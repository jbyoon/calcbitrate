#! /usr/bin/env python

# codec = "H264,MJPEG"
# Resolution = "1920x1080,1280x720,800x450,640x360,480x270"
# quality = "highest,high,medium,low,lowest"
# qual100 = [1..100]
# fps == [1..30]
# H264 VBR only
def createtestconfig(codec, resolution, quality, fps, codecsetting=True,
  collect=-1, warmup=-1, cgitime=-1):
  EOL = "\n"
  base_str = "/uapi-cgi/param.cgi?action=update&group=ENCODER.Ch0.Videocodec.St0"
  outstr = ""
  for cod in codec:
    codec_base_str = base_str+"."+cod
    qnode = "qvalue" if cod == "h264" else "quality"
    if codecsetting == True:
      outstr += "# codec setting"+EOL
      outstr += "CGI_SET-->"+base_str+"&standard="+cod+EOL
      if cod == "h264":
        outstr += "CGI_SET-->"+codec_base_str+"&bitratectrl=vbr&pcount=29"+EOL
      outstr += "# common setting"+EOL
    for res in resolution:
      res_str = "&resolution="+res
      for f in fps:
        fps_str = "&maxfps="+str(f)
        for qv in quality:
          qv_str = "&"+qnode+"="+str(qv)
          cgi_query = codec_base_str+res_str+fps_str+qv_str+EOL
          outstr += "CGI_SET-->"+cgi_query
          capture_str = "T_%s_%s_%s_%s" % (cod,res,f,qv)
          time_opt  = (','+str(collect)) if collect>=0 else ''
          time_opt += (','+str(warmup)) if warmup>=0 else ''
          time_opt += (','+str(cgitime)) if cgitime>=0 else ''
          outstr += "VIEW-->"+capture_str+time_opt+EOL
  return outstr


def printheader(cnt):
  header  = "#######################\n"
  header += "##total : %3d test case\n" % cnt
  header += "#######################\n"
  header += '#VIEW-->filename[,collect,warmup,cgiwait]\n'
  return header

def maxfull(img1080p):
  codec=["h264"]
  quality=["highest","high","normal","low","lowest"]
  res=["1920x1080","1280x720","800x450","640x360","480x270"]
  if img1080p==False :
    del res[0]
  fps=[30,24,18,12,6]
  cnt=len(quality)*len(res)*len(fps)
  out=createtestconfig(codec,res,quality,fps)
  codec=["mjpeg"]
  quality=[100,75,50,1]
  res=["1920x1080","1280x720","800x450","640x360","480x270"]
  fps=[30,24,18,12,6]
  cnt+=len(quality)*len(res)*len(fps)
  out+=createtestconfig(codec,res,quality,fps)
  header = printheader(cnt)
  return header+out

def full(img1080p):
  codec=["h264"]
  quality=["highest","normal","lowest"]
  res=["1920x1080","1280x720","800x450","640x360","480x270"]
  if img1080p==False :
    del res[0]
  fps=[30,24,18,12,6]
  cnt=len(quality)*len(res)*len(fps)
  out=createtestconfig(codec,res,quality,fps)
  codec=["mjpeg"]
  quality=[100,75,50,1]
  res=["1920x1080","1280x720","800x450","640x360","480x270"]
  if img1080p==False :
    del res[0]
  fps=[30,24,18,12,6]
  cnt+=len(quality)*len(res)*len(fps)
  out+=createtestconfig(codec,res,quality,fps)
  header = printheader(cnt)
  return header+out

def oneframe(img1080p):
  codec=["h264"]
  quality=["highest","normal","lowest"]
  res=["1920x1080","1280x720","800x450","640x360","480x270"]
  if img1080p==False :
    del res[0]
  fps=[1]
  cnt=len(quality)*len(res)*len(fps)
  out=createtestconfig(codec,res,quality,fps)
  codec=["mjpeg"]
  quality=[100,75,50,1]
  res=["1920x1080","1280x720","800x450","640x360","480x270"]
  if img1080p==False :
    del res[0]
  fps=[1]
  cnt+=len(quality)*len(res)*len(fps)
  out+=createtestconfig(codec,res,quality,fps)
  header = printheader(cnt)
  return header+out

def simple(img1080p):
  codec=["h264"]
  quality=["highest","lowest"]
  res=["1280x720"]
  fps=[30,6]
  out=createtestconfig(codec,res,quality,fps)
  codec=["mjpeg"]
  quality=[75]
  res=["800x450"]
  fps=[30,6]
  out+=createtestconfig(codec,res,quality,fps)
  return out

def update_profile_config(sc=10,wu=10,col=30):
  configname = 'profile_config.txt'
  f = open(fname,'w+')
  f.write(str)
  f.close()

def tycofullset(img1080p=True):
  out = ""
  codec=["h264"]
  quality=["highest","high","normal","low","lowest"]
  res=["1920x1080","1280x720","800x450","640x360","480x270"]
  if img1080p==False :
    del res[0]
  fps=[30,24,18,12,6,1]
  cnt=len(quality)*len(res)*len(fps)
  first = True
  for r in res:
    reschanged = True
    for f in fps:
      for q in quality:
        out+=createtestconfig(codec,[r],[q],[f],codecsetting=first,
          collect=(30 if f==1 else 10),
          warmup=(10 if f==1 else 5),
          cgitime=(15 if reschanged else 0))
        first = False
        reschanged = False

  codec=["mjpeg"]
  quality=[100,75,50,1]
  res=["1920x1080","1280x720","800x450","640x360","480x270"]
  if img1080p==False :
    del res[0]
  fps=[30,24,18,12,6,1]
  cnt+=len(quality)*len(res)*len(fps)
  first = True
  for r in res:
    reschanged = True
    for f in fps:
      for q in quality:
        out+=createtestconfig(codec,[r],[q],[f],codecsetting=first,
          collect=10,warmup=5,cgitime=(15 if reschanged else 0))
        first = False
        reschanged = False
  fname = 'Test_script.txt'+('' if img1080p else '.720p')+'.tyco_full'
  WriteFile(fname,printheader(cnt)+out)


def WriteFile(fname,str):
  f = open(fname,'w+')
  f.write(str)
  f.close()

############
##### main
WriteFile("Test_script.txt.one",oneframe(img1080p=True))
WriteFile("Test_script.txt.simple",simple(img1080p=True))
WriteFile("Test_script.txt.full",full(img1080p=True))
WriteFile("Test_script.txt.maxfull",maxfull(img1080p=True))

WriteFile("Test_script.txt.720p.one",oneframe(img1080p=False))
WriteFile("Test_script.txt.720p.full",full(img1080p=False))
WriteFile("Test_script.txt.720p.maxfull",maxfull(img1080p=False))

tycofullset(img1080p=True)
tycofullset(img1080p=False)
