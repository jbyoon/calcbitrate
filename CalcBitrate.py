# -*- coding: utf-8 -*-

import time
import urllib2
import subprocess
import os
import os.path
import glob
import sys
import time

###############################################################
# CalcBitrate
APP_VERSION = 1.0

# ���� �������� config ���Ͽ��� �о���δ�.
IP_ADDR = None
PATH_PROFILES = None
PATH_RESULT = None
PATH_SIMPLE_VIEWER = None
TIME_SETTING_COMPLETE = None
TIME_WARM_UP = None
TIME_COLLECTING = None
NUM_OF_CHANNEL = None
SAVE_STREAM = 1
g_stime = []
ROOT_USERNAME = 'admin'
ROOT_PASSWORD = 'admin'
DEFAULT_PROFILE_NAME = 'profile_default.txt'
FILENAME_CONFIG = 'profile_config.txt'

filelist = []
filelistForScript = []
fpath0 = ''
fpath1 = ''
fpath2 = ''
fpath3 = ''

pre_str = ''

def traverseForScript(_dir):
    global filelistForScript
    # init
    filelistForScript = []
    for obj in glob.glob(_dir + '\*_script.txt'):
        print 'Insert Script : ' + obj
        filelistForScript.append(obj)

def GetViewCount(fname):
    count = 0
    fread = open(fname, 'r');
    for l in fread.readlines():
        if(l.split('-->')[0] =='VIEW'):
            count +=1
    return count
    
def ExecScript(_num):
    global filelistForScript
    filelistForScript.sort()
    for fpath in filelistForScript:
        view_count = GetViewCount(fpath)
        print 'Script Name : ', fpath,'VIEW count:',view_count
        fread = open(fpath, 'r')
        line = fread.readline()
        count = 0
        while line:
            if not line.rfind('#') == -1:
                line = fread.readline()
                continue
            param = line.split('-->')
            if param[0] == 'CGI_SET':
                print 'CMD : CGI_SET'
                ExecParamCGI(param[1], 'SET', None)
            elif param[0] == 'CGI_GET':
                print 'CMD : CGI_GET'
                ExecParamCGI(param[1], 'GET', param[2])
            elif param[0] == 'VIEW':
                print 'Viewer Start:',count,'/',view_count
                PrintExpectedTime(g_stime,count+1,view_count)
                try:
                    count += 1
                    #new VIEW syntax
                    #VIEW-->T_h264_1920x1080_1_normal[,TIME_COLLECTING,TIME_WARM_UP,TIME_SETTING_COMPLETE]
                    vparam = param[1].split(',')
                    prefilename = vparam[0] if len(vparam) > 1 else param[1][:-1]
                    collect = int(vparam[1]) if len(vparam) > 1 else TIME_COLLECTING
                    warmup = int(vparam[2]) if len(vparam) > 2 else TIME_WARM_UP
                    cgitime = int(vparam[3]) if len(vparam) > 3 else TIME_SETTING_COMPLETE
                    print 'collect:',collect,'warmup:',warmup,'cgitime:',cgitime
                    CollectStatFromSimpleViewer(prefilename + '_Test#_%d'%_num, NUM_OF_CHANNEL, collect, warmup, cgitime)
#                    CollectStatFromSimpleViewer(param[1][:-1] + '_Test#_%d'%_num, NUM_OF_CHANNEL)
                except Exception as inst:
                    print 'Exception occured in this test.'
                    print type(inst)     # the exception instance
                    print inst.args      # arguments stored in .args
                    print inst     
                    pass
            elif param[0] == 'SLEEP':
                print 'Sleep : %d'%int(param[1])
                time.sleep(int(param[1]))
            else:
                if len(line.strip()) > 0:
                    print '[ERROR] Script line is strange.... check plz.'
            line = fread.readline()
        
        fread.close()
            
    
def traverseForData(_dir):
    global filelist
    for obj in glob.glob(_dir + '\*_perf.txt'):
        print 'Insert Data: ' + obj
        filelist.append(obj)

def calForMax_Min_Avg_Mid():
    global filelist
    filelist.sort()
    strTmp=''
    ind = 0

    global fpath0, fpath1, fpath2, fpath3

    for fpath in filelist:
        strTmp = fpath
        strTmp = strTmp.replace("_perf.txt", "")
        ind = ind + 1
        print 'str:' + strTmp
        
        if (ind == 4):
            ind = 0
            fpath0 = open(fpath, 'r')
            print 'new:' + strTmp[:-2]
            writeInOneFile(strTmp[:-2])
        elif (ind == 1):
            fpath1 = open(fpath, 'r')
        elif (ind == 2):
            fpath2 = open(fpath, 'r')
        elif (ind == 3):
            fpath3 = open(fpath, 'r')
            
def writeInOneFile(nowpath): 
    print 'Start Write One file\n'
    global fpath0, fpath1, fpath2, fpath3
    data_storage = {}
    ind = 0
    fw = open(nowpath + '_All_Result.txt', 'w')
    sample = "Num    ch0    ch1    ch2    ch3    All\n"
    fw.write(sample)
    while 1:
        line0 = fpath0.readline()
        line1 = fpath1.readline()
        line2 = fpath2.readline()
        line3 = fpath3.readline()
        
        if not (line0 and line1 and line2 and line3):
            print 'Write fin.'
            fpath0.close()
            fpath1.close()
            fpath2.close()
            fpath3.close()
            

            maxValue = [-1,-1,-1,-1,-1]
            minValue = [-1,-1,-1,-1,-1]
            avgValue = [-1,-1,-1,-1,-1]
            
            midValue = []
            for data_ind in data_storage:
                if (maxValue[4] == -1 and minValue[4] == -1):
                    maxValue = data_storage[data_ind]
                    minValue = data_storage[data_ind]
                    avgValue = data_storage[data_ind]
                    
                    midValue.append( int(data_storage[data_ind][4]) ) 
                else:
                    midValue.append( int(data_storage[data_ind][4]) )
                    
                    ## max
                    if (maxValue[4] < data_storage[data_ind][4]):
                        maxValue = data_storage[data_ind]
                        
                    ## min
                    if (minValue[4] > data_storage[data_ind][4]):
                        minValue = data_storage[data_ind]
                        
                    ## avg.
                    avgValue[0] = int( int(avgValue[0] + data_storage[data_ind][0]) / 2 )
                    avgValue[1] = int( int(avgValue[1] + data_storage[data_ind][1]) / 2 )
                    avgValue[2] = int( int(avgValue[2] + data_storage[data_ind][2]) / 2 )
                    avgValue[3] = int( int(avgValue[3] + data_storage[data_ind][3]) / 2 )
                    avgValue[4] = avgValue[0] + avgValue[1] + avgValue[2] + avgValue[3]
                    
            midValue.sort()
                
            fw.write('\n===================================\n')
            fw.write('\nsample num : %d\n'%len(data_storage))
            fw.write('\nMax Result : ' + str(maxValue[0]) + '\t' + str(maxValue[1]) + '\t' + str(maxValue[2]) + '\t' + str(maxValue[3]) + '\t' + str(maxValue[4]) )
            fw.write('\nMin Result : ' + str(minValue[0]) + '\t' + str(minValue[1]) + '\t' + str(minValue[2]) + '\t' + str(minValue[3]) + '\t' + str(minValue[4]) )
#            fw.write('\nAvg Result : ' + str(avgValue[0]) + '\t' + str(avgValue[1]) + '\t' + str(avgValue[2]) + '\t' + str(avgValue[3]) + '\t' + str(avgValue[4]) )
            fw.write('\nMid Result (All Channel) : ' + str(midValue[ (len(data_storage) / 2) ]))
            fw.close()
            break
        
        ##
        lv0 = line0.split('\t')
        lv1 = line1.split('\t')
        lv2 = line2.split('\t')
        lv3 = line3.split('\t')
        lv0_int = int( float(lv0[0]) )
        lv1_int = int( float(lv1[0]) )
        lv2_int = int( float(lv2[0]) )
        lv3_int = int( float(lv3[0]) )
        sample = "%d    %d    %d    %d    %d    %d\n" %(ind, lv0_int, lv1_int, lv2_int, lv3_int, (lv0_int + lv1_int + lv2_int + lv3_int) )
        
        data_storage[ind] = [ lv0_int, lv1_int, lv2_int, lv3_int, (lv0_int + lv1_int + lv2_int + lv3_int) ]
        
        ind = ind + 1
        fw.write(sample)

def ExecParamCGI(_theurl, _case, _case_param):
    global pre_str
    time.sleep(5)
    
    while 1:
        pagehandle = MakeHttpReq(_theurl)
        page = pagehandle.read()
        print page
        
        if _case == 'SET':
            res = page.split('|')
            pre_str = _theurl
            if res[1] == 'OK':            
                break
        elif _case == 'GET':
            res = page.split('=')

            # check 'y' or 'n'            
            if ( str(res[1][:1]) == str(_case_param[:1]) ) :
                print '[INFORM] CGI Setting Really Succ!'
                break
            else:
                print '[INFORM] makehttpReq ReTry : GET Setting value is strange...'
                pagehandle = MakeHttpReq(pre_str)
                page = pagehandle.read()
                print 'Pre_theurl : ', pre_str
                print page
                time.sleep(10)

def MakeHttpReq(shortUrl,bPrint = True):
    theurl = 'http://%s%s' % (IP_ADDR, shortUrl)
    theurl = theurl.strip()             #�¿� ��� ó��
    theurl = theurl.replace(' ', '%20') # ���ڿ� ���� ��� ó�� (GET ������� HttpReq �ϱ⶧����)
    if(bPrint): print 'URL : ', theurl
    
    chk = 1
    while chk :
        try:
            # code from http://www.voidspace.org.uk/python/articles/authentication.shtml
            passman = urllib2.HTTPPasswordMgrWithDefaultRealm()
            passman.add_password(None, theurl, ROOT_USERNAME, ROOT_PASSWORD)
            authhandler = urllib2.HTTPBasicAuthHandler(passman)
            opener = urllib2.build_opener(authhandler)
            urllib2.install_opener(opener)
            pagehandle = urllib2.urlopen(theurl, None, 20)
            if(bPrint): print '[INFORM] makehttpReq succ'
            return pagehandle 
            
        except:
            print '[INFORM] makehttpReq ReTry & sleep 5sec : ', theurl
            time.sleep(5)
            chk = 1    
    
def findNearestInfo(info,t):
    t/=10000000
    for i in range(len(info)):
        timetick,datastr = info[i]
        if(t<=timetick):
            i-=1
            if (i<0):i=0
            timetick,datastr = info[i]
            return datastr
    return info[-1][1]  #last parameter

temp_result = """
cpu.user-mode=25.3
cpu.system-mode=29.7
cpu.nice=0.0
cpu.idle=45.1
cpu.io=0.0
memory.used=61712
memory.free=76716
memory.shared=0
memory.buffers=0
memory.cached=27180
temperature.celsius=40.0
temperature.fahrenheit=104.0
temperature.ptz1-celsius=0.0
temperature.ptz1-fahrenheit=32.0
temperature.ptz2-celsius=0.0
temperature.ptz2-fahrenheit=32.0
network.interface=eth0
network.address=192.168.66.225
network.subnet=255.255.0.0
network.gateway=192.168.0.1
network.dns1=0.0.0.0
network.dns2=0.0.0.0
stream[0].type=H264
stream[0].resolution=1920x1080
stream[0].fps=18
stream[0].ratecontrol=VBR
stream[0].bitrate=1833056
stream[0].gop=30
stream[0].profile=high profile
stream[1].type=H264
stream[1].resolution=800x450
stream[1].fps=7
stream[1].ratecontrol=VBR
stream[1].bitrate=339912
stream[1].gop=30
stream[1].profile=high profile
stream[2].type=MJPEG
stream[2].resolution=1280x720
stream[2].fps=3
stream[2].bitrate=1916312
stream[2].gop=0
time.btime=1736406203
time.uptime=15289
network.nbrofchannel=0
"""

def getCPUFPSBitrateFromInfo(istr,ch):
    if istr == None:
        return -1,-1,-1
    l = istr.split("\r\n")
    parsed = {}
    for item in l:
        pair = item.split("=")
        if(len(pair)==2):parsed[pair[0]]= pair[1]
    cpu = float(parsed.get("cpu.user-mode",0))+float(parsed.get("cpu.system-mode",0))
    fps = float(parsed.get("stream[%d].fps"%ch,0))
    bps = float(parsed.get("stream[%d].bitrate"%ch,0))
    
    return cpu,fps,bps

    #return float(parsed["cpu.user-mode"])+float(parsed["cpu.system-mode"]),float(parsed["stream[%d].fps"%ch]),float(parsed["stream[%d].bitrate"%ch])

import math
def mean(values):
  if len(values) == 0:
    return None
  return sum(values, 0.0) / len(values)


def standardDeviation(values, option):
  if len(values) < 2:
    return None

  sd = 0.0
  sum = 0.0
  meanValue = mean(values)

  for i in range(0, len(values)):
    diff = values[i] - meanValue
    sum += diff * diff

  sd = math.sqrt(sum / (len(values) - option))
  return sd

def GetMinMaxAvgSTDEV(l):
    return min(l),max(l),float(sum(l))/len(l),standardDeviation(l,1)

def GetMinMaxAvgSTDEVbpsToKbps(l):
    return min(l)/1000.0,max(l)/1000.0,float(sum(l))/len(l)/1000.0,standardDeviation(l,1)/1000.0


def processStatFile(srcfile,resultfile,ch,info,_warmup):
    fSrc = open(srcfile)
    data= eval('['+fSrc.read()+']')
    fSrc.close()
    fResult = open(resultfile,'w')
    fResult2 = open(resultfile+'.frameinterval.txt','w')

    #TIME_WARM_UP 
    firsttime = data[0][2]
    startidx =0
    for i in range(len(data)-1):
        if firsttime + _warmup*10000000 < data[i][2]:
            startidx = i
            break
    
    
    n = 0
    fps = 0
    bps = 0
    fpss=[]
    bpss=[]
    data = data[startidx:] #clip away first part
    print "clipping:" , startidx
    if(len(data)==0):
        fResult.close()
        fResult2.close()
        return
    first = data[0][0]
    
    dfpss = []
    dbpss = []
    dcpus = []
    for i in range(len(data)-1):
        
    #    if dd[i+1][0] - dd[i][0] > 500000 :
    #        print dd[i+1][0] - dd[i][0],i
        fResult2.write("%d\r\n"%(data[i+1][0]-data[i][0]))
        elapsed = data[i][0]-first
        if n*10000000 <= elapsed and elapsed <(n+1)*10000000:
            fps+=1
            bps+=data[i][1]*8
        else:
            fpss.append(fps)
            bpss.append(bps)
            dcpu , dfps,dbps = getCPUFPSBitrateFromInfo ( findNearestInfo(info,data[i][2]) ,ch)
            dfpss.append(dfps)
            dbpss.append(dbps)
            dcpus.append(dcpu)
            fResult.write("%d,%d,%f,%f,%f\r\n"%(fps,bps, dcpu , dfps,dbps  )  )
            n+=1
            fps = 1
            bps = data[i][1]*8


    fResult.write("client fps -- min:%d max:%d avg:%f STDEV:%f\r\n"%(min(fpss),max(fpss),float(sum(fpss))/len(fpss),standardDeviation(fpss,1)))
    fResult.write("client bps -- min:%d max:%d avg:%f STDEV:%f\r\n"%(min(bpss),max(bpss),float(sum(bpss))/len(bpss),standardDeviation(bpss,1)))
    fResult.write("device CPU -- min:%d max:%d avg:%f STDEV:%f\r\n"%(min(dcpus),max(dcpus),float(sum(dcpus))/len(dcpus),standardDeviation(dcpus,1)))
    fResult.write("device fps -- min:%d max:%d avg:%f STDEV:%f\r\n"%(min(dfpss),max(dfpss),float(sum(dfpss))/len(dfpss),standardDeviation(dfpss,1)))
    fResult.write("device bps -- min:%d max:%d avg:%f STDEV:%f\r\n"%(min(dbpss),max(dbpss),float(sum(dbpss))/len(dbpss),standardDeviation(dbpss,1)))
    fResult.write("%.0f,%.0f,%.2f,%.2f, %.0f,%.0f,%.2f,%.2f, %.2f,%.2f,%.2f,%.2f, %.0f,%.0f,%.2f,%.2f, %.0f,%.0f,%.2f,%.2f"%(GetMinMaxAvgSTDEV(fpss)+GetMinMaxAvgSTDEVbpsToKbps(bpss)+GetMinMaxAvgSTDEV(dcpus)+GetMinMaxAvgSTDEV(dfpss)+GetMinMaxAvgSTDEVbpsToKbps(dbpss)))

    fResult.close()
    fResult2.close()


def processSnapshotInfo(srcfile,resultfile,info,_warmup):
    fSrc = open(srcfile)
    data= eval('['+fSrc.read()+']')
    fSrc.close()
    fResult = open(resultfile+'.snapshot.log','w')

    #TIME_WARM_UP 
    firsttime = data[0][2]
    startidx =0
    for i in range(len(data)-1):
        if firsttime + _warmup*10000000 < data[i][2]:
            startidx = i
            break
    
    
    data = data[startidx:] #clip away first part
    print "clipping:" , startidx
    if(len(data)==0):
        fResult.close()
        return
    first = data[0][0]
    n=0
    dfpss = []
    dbpss = []
    dcpus = []
    for i in range(len(data)-1):
        
    #    if dd[i+1][0] - dd[i][0] > 500000 :
    #        print dd[i+1][0] - dd[i][0],i
        elapsed = data[i][0]-first
        if n*10000000 <= elapsed and elapsed <(n+1)*10000000:
            pass
        else:
            dcpu , dfps,dbps = getCPUFPSBitrateFromInfo ( findNearestInfo(info,data[i][2]) ,2)
            dfpss.append(dfps)
            dbpss.append(dbps)
            dcpus.append(dcpu)
            n+=1
            fResult.write("%f,%f,%f\r\n"%(dcpu , dfps,dbps  )  )

    fResult.write("cpu min,max,avg,stdv,device snapshot fps min,max,avg,stdv,device snapshot kbps,min,max,avg,stdv\r\n");
    fResult.write("%.2f,%.2f,%.2f,%.2f, %.0f,%.0f,%.2f,%.2f, %.0f,%.0f,%.2f,%.2f"%(GetMinMaxAvgSTDEV(dcpus)+GetMinMaxAvgSTDEV(dfpss)+GetMinMaxAvgSTDEVbpsToKbps(dbpss)))

    fResult.close()

def AddInfoToInfoFile(file,ch):
    #query setting fps,bitrate and save the values
    f = open(file,"a")
    pagehandle = MakeHttpReq("/uapi-cgi/param.cgi?action=list&group=encoder.ch0.videocodec.st%d.h264"%ch)
    page = pagehandle.read()
    f.write("\n%s"%page)
    f.close()

def CollecDeviceInfo(duration):
    current = time.time()
    start = current
    end = current+duration
    iter = 1
    info=[]
    while True:
        pagehandle = MakeHttpReq("/uapi-cgi/status.cgi",False)
        if pagehandle != None:
            page = pagehandle.read()
            info.append((current,page))
        #info.append((current,temp_result))
        current = time.time()
        wait = start+iter-current
        #print("%d,%f"%(iter,wait))
        if(wait>0):time.sleep(wait)
        iter += 1
        if(current>end):
            return info
        

def CollectStatFromSimpleViewer(_profileFilename, _NumOfChannel, _collect, _warmup, _cgitime):
    processId={}
    for i in range(0, _NumOfChannel):
        MakeXmlForSimpleViewer(_profileFilename, i)
    
    time.sleep(_cgitime)
        
    for i in range(0, _NumOfChannel):
        processId[i] = LaunchSimpleViewer(i)
    info = CollecDeviceInfo( _warmup + _collect)
    
    for i in range(0, _NumOfChannel):
        KillSimpleViewer(processId[i])

    for i in range(0, _NumOfChannel):
        filenameToSaveStatistics = GetStatisticsFileName( _profileFilename + '_ch_' + str(i) )
        processStatFile(filenameToSaveStatistics+".txt",filenameToSaveStatistics+".log",i,info, _warmup)
        if i==0:
            processSnapshotInfo(filenameToSaveStatistics+".txt",filenameToSaveStatistics+".log",info, _warmup)
        filenameToSaveStatisticsInfo = GetStreamInfoFileName(_profileFilename + '_ch_' + str(i))
        AddInfoToInfoFile(filenameToSaveStatisticsInfo,i)       

    return



def ConvProfilePathToFileName(profileFileName):
    ''' ����̺� �̸��� Ȯ���ڸ� ����, ���� �̸����� '\'�� '_'�� �ٲ۴�.'''
    root, ext = os.path.splitext(profileFileName)
    ext
    head, tail = os.path.splitdrive(root)
    head
    filename = tail.replace('\\', '_')
    return filename

def GetStatisticsFileName(profileFileName):
    filename = PATH_RESULT + '\\' + ConvProfilePathToFileName(profileFileName) + '_perf.txt'
    return filename

def GetStreamInfoFileName(profileFileName):
    filename = PATH_RESULT + '\\' + ConvProfilePathToFileName(profileFileName) + '_strm_info.txt'
    return filename

def GetBmpFileName(profileFileName):
    filename = PATH_RESULT + '\\' + ConvProfilePathToFileName(profileFileName) + '.bmp'
    return filename

def GetCpuStatisticsFileName(profileFileName):
    filename = PATH_RESULT + '\\' + ConvProfilePathToFileName(profileFileName) + '_cpu.txt'
    return filename

def GetTotalStatisticsFileName():
    filename = PATH_RESULT + '\\' + '1_total_perf.txt'
    return filename

def GetTotalStatisticsFileNameForChannel(_channel):
    filename = PATH_RESULT + '\\' + '1_total_perf_' + _channel + '.txt'
    return filename


def MakeXmlForSimpleViewer(_profileFileName, _channel):
    filenameToSaveStatistics = GetStatisticsFileName( _profileFileName + '_ch_' + str(_channel) )
    filenameToSaveStreamInfo = GetStreamInfoFileName( _profileFileName + '_ch_' + str(_channel) )
    filenameToSaveStatisticsBmp = GetBmpFileName( _profileFileName + '_ch_' + str(_channel) )

    streamname = ['ufirststream','usecondstream'][_channel]
    
    xmludp = r'''<root>
          <APPSetting>
            <SourceManager Path="Sources">
              <SourceFactorySetting>
                <SourceFactory Id="0" Description="RTSP Source" SourceType="1114373"/>
              </SourceFactorySetting>
              <SourceSetting>
                <Source Id="0" SrcFacId="0" ColorFormat="3" RTSPURL="rtsp://%s/%s" SESSION="tcp" WEBURL="https://%s" ID="root" PW="pass" 
                bSaveToBmp="0" TotalFramesToSave="30" bSaveYuv420p="0" PathToSaveBmp="C:\ForDevelopment\dqa\Encoder_profile_NVC4000L\DecodedBmp"
                        bSaveStatistics="1" timeWarmup="%d" timeCollecting="%d" bSaveStream="%d"
                        filenameToSaveStatistics="%s"
                        filenameToSaveStreamInfo="%s"
                        filenameToSaveStatisticsBmp="%s"
                />
              </SourceSetting>
            </SourceManager>
            <FrameManager FrameType="3" Path="Frames">
              <EningSetting>
                <Engine Id="0" SourceId="0"/>
              </EningSetting>
              <NVViewManager Path="NVViews"/>
            </FrameManager>
            <SimpleNVView TestValue="4"/>
          </APPSetting>
        </root>
        ''' % (IP_ADDR,streamname,IP_ADDR, TIME_WARM_UP, TIME_COLLECTING, SAVE_STREAM, filenameToSaveStatistics.encode('cp949'), filenameToSaveStreamInfo.encode('cp949'), filenameToSaveStatisticsBmp.encode('cp949'))
    xmlaxis = r'''<root>
          <APPSetting>
            <SourceManager Path="Sources">
              <SourceFactorySetting>
                <SourceFactory Id="0" Description="RTSP Source" SourceType="1114373"/>
              </SourceFactorySetting>
              <SourceSetting>
                <Source Id="0" SrcFacId="0" ColorFormat="3" RTSPURL="rtsp://%s/axis-media/media.amp?streamprofile=Quality&amp;fps=30" SESSION="tcp" WEBURL="https://%s" ID="root" PW="pass" 
                bSaveToBmp="0" TotalFramesToSave="30" bSaveYuv420p="0" PathToSaveBmp="C:\ForDevelopment\dqa\Encoder_profile_NVC4000L\DecodedBmp"
                        bSaveStatistics="1" timeWarmup="%d" timeCollecting="%d"
                        filenameToSaveStatistics="%s"
                        filenameToSaveStreamInfo="%s"
                        filenameToSaveStatisticsBmp="%s"
                />
              </SourceSetting>
            </SourceManager>
            <FrameManager FrameType="3" Path="Frames">
              <EningSetting>
                <Engine Id="0" SourceId="0"/>
              </EningSetting>
              <NVViewManager Path="NVViews"/>
            </FrameManager>
            <SimpleNVView TestValue="4"/>
          </APPSetting>
        </root>
        ''' % (IP_ADDR,IP_ADDR, TIME_WARM_UP, TIME_COLLECTING, filenameToSaveStatistics.encode('cp949'), filenameToSaveStreamInfo.encode('cp949'), filenameToSaveStatisticsBmp.encode('cp949'))

    xml = xmludp
    #print xml
    
    xmlFilename = PATH_SIMPLE_VIEWER + '\\' + 'NVFrameSetting_profile_ch_%d.xml' %(_channel)
    f=open(xmlFilename.encode('cp949'), 'w')
    f.write(xml)
    f.close()
    
    return

def LaunchSimpleViewer(_channel):
    programPath = PATH_SIMPLE_VIEWER + '\\' + 'VideoMeter.exe NVFrameSetting_profile_ch_%d.xml' %(_channel)
    p = subprocess.Popen(programPath.encode('cp949'), cwd=PATH_SIMPLE_VIEWER.encode('cp949'))
        
    return p

def KillSimpleViewer(p):
    p.terminate()
    time.sleep(1)
    assert p.poll() != None

    return


def GetConfig():
    with open(FILENAME_CONFIG) as f:
        configs = f.read()
    
    config_dict = {}
    key=''
    value=''
    for line in configs.split('\n'):
        if line:
            try:
                config, comment = line.split('#', 1); comment
            except ValueError:
                config = line
            try:
                key, value = config.split('=')
                key = key.strip()
                value = value.strip()
                config_dict[key] = value
            except ValueError:
                pass
    
    global IP_ADDR; global PATH_PROFILES; global PATH_RESULT; global PATH_SIMPLE_VIEWER; global TIME_SETTING_COMPLETE; global TIME_WARM_UP; global TIME_COLLECTING; global NUM_OF_CHANNEL;global SAVE_STREAM;
    try:
        IP_ADDR = config_dict['IP_ADDR']
        PATH_PROFILES = config_dict['PATH_PROFILES']
        PATH_RESULT = config_dict['PATH_RESULT']
        PATH_SIMPLE_VIEWER = config_dict['PATH_SIMPLE_VIEWER']
        TIME_SETTING_COMPLETE = int(config_dict['TIME_SETTING_COMPLETE'])
        TIME_WARM_UP = int(config_dict['TIME_WARM_UP'])
        TIME_COLLECTING = int(config_dict['TIME_COLLECTING'])
        NUM_OF_CHANNEL = int(config_dict['NUM_OF_CHANNEL'])
        SAVE_STREAM = int(config_dict['SAVE_STREAM'])
    except KeyError as err:
        print 'Parameter is not setting... :  %s.., Exit.' % str(err)
        return False
                
    print '== [Config Res] start =='
    print 'IP Addr : %s' % (IP_ADDR)
    print 'Profiles path : %s' % (PATH_PROFILES)
    print 'Result path : %s' % (PATH_RESULT)
    print 'Simple viewer path : %s' % (PATH_SIMPLE_VIEWER)
    print 'setting-complete time : %s' % (TIME_SETTING_COMPLETE)
    print 'warm-up time : %s' % (TIME_WARM_UP)
    print 'collecting time : %s' % (TIME_COLLECTING)
    print '== [Config Res] end == \n'
        
    return True

def Main():
    if GetConfig() == False:
        return
    
    # The number of test.
    for i in range(0, 1):
        ## Setup Scripts
        print '[Script] Setup'
        traverseForScript('Scripts')
        
        ## Exec Scripts
        print '[Script] Exec'
        ExecScript(i)
        
        ## Calc Max, Min, Avg, Min
        print '[Calc] start'
        traverseForData('Results')
        calForMax_Min_Avg_Mid()
    print 'End of Operation'


def PrintExpectedTime(stime,count,view_count):
    etime = time.time()
    sec = int(etime - stime)
    esec = int(sec*view_count/float(count))
    rsec = esec-sec
    print "Used time      : %d hours %d minutes %d seconds" % (sec/3600,(sec/60)%60,sec%60)
    print "Expectinig time: %d hours %d minutes %d seconds" % (esec/3600,(esec/60)%60,esec%60)
    print "Remained time  : %d hours %d minutes %d seconds" % (rsec/3600,(rsec/60)%60,rsec%60)

def PrintTime(stime):
    etime = time.time()
    sec = int(etime - stime)
    print "Total time: %d hours %d minutes %d seconds" % (sec/3600,(sec/60)%60,sec%60)

if __name__ == '__main__':
    print "Version:", APP_VERSION
    g_stime = time.time()
    Main()
    PrintTime(g_stime)


