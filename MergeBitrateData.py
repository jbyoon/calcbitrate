#! /usr/bin/env python

import os,sys
import subprocess
import operator
import fnmatch

def findfiles(pattern, path):
  result = []
  for root, dirs, files in os.walk(path):
    for d in dirs:
      rs = findfiles(pattern, d)
      if len(rs) > 0:
        result.append(rs)
    for name in files:
      if fnmatch.fnmatch(name, pattern):
          result.append(os.path.join(root, name))
  return result

# ls = findfiles('*.txt', 'back/i610-M022-fps1/')
# print ls
# print len(ls)

def mergefiledata(data_str,path='.'):
  proc=subprocess.Popen(['grep','-r',data_str,path], stdout=subprocess.PIPE)
  #proc=subprocess.Popen(['ls','-a'], stdout=subprocess.PIPE)
  out, err = proc.communicate()
#  print out, err
  data = []
  for l in out.splitlines():
    if len(l)>0:
#      print l.split('/')
      item = l.split('/')[-1].split('_')
      #print item
      (codec,res,fps,quality) = item[1:5]
      #print codec,res,fps,quality
      (minfps,maxfps,avg) = ':'.join(item[9].split()[3:6]).split(':')[1:6:2]
      avg = str(round(float(avg),2))
      #print minfps,maxfps,avg
      data.append([codec,res,fps,quality,minfps,maxfps,avg])

  a=sorted(data,key=operator.itemgetter(3))
  b=sorted(a,key=operator.itemgetter(2))
  c=sorted(b,key=operator.itemgetter(1))
  fd=sorted(c,key=operator.itemgetter(0))
  return fd

def getfps():
  header = ["codec","resolution","fps","quality", "min fps", "max fps", "avg fps"]
  out = ','.join(header) + "\n"
  for x in mergefiledata('client fps','back'):
    out += ','.join(x) +"\n"
  return out

def getbps():
  header = ["codec","resolution","fps","quality", "min bps", "max bps", "avg bps"]
  out = ','.join(header) + "\n"
  for x in mergefiledata('client bps','back'):
    out += ','.join(x) +"\n"
  return out

def getminmaxavg(text, fpath, unit=1):
  proc=subprocess.Popen(['grep', text, fpath], stdout=subprocess.PIPE)
  out, err = proc.communicate()
  (mind,maxd,avg) = ':'.join(out.split()[3:6]).split(':')[1:6:2]
  #print avg, type(avg)
  v = float(avg)
  avg = "{:.2f}".format(v) if v < 100 else str(int(v)/unit)
  return mind,maxd,avg

def gettesttime(path):
  f = open(path,'r')
  l = len(f.readlines())
  count = l - 5  # last 5 lines are information
  return count

def getdata(path):
  data = []
  for fpath in findfiles('*.txt.log', path):
    try:
      fname=os.path.basename(fpath)
      (codec,res,fps,quality) = fname.split('_')[1:5]
      minfps,maxfps,avgfps = getminmaxavg('client fps', fpath)
      minbps,maxbps,avgbps = getminmaxavg('client bps', fpath,unit=1000)
      testtime = str(gettesttime(fpath))
#      data.append([codec,res,fps,quality,minfps,maxfps,avgfps,minbps,maxbps,avgbps,testtime,os.path.dirname(fpath)])
      data.append([codec,res,fps,quality,minfps,maxfps,
        avgfps,minbps,maxbps,avgbps,testtime,fpath])
    except: # If file has not any contents, pass it
      pass
  a=sorted(data,key=operator.itemgetter(3))
  b=sorted(a,key=operator.itemgetter(2))
  c=sorted(b,key=operator.itemgetter(1))
  fd=sorted(c,key=operator.itemgetter(0))
  return fd

def printcsv(path):
  data = getdata(path)
  header = ["codec","resolution","fps","quality", "min fps", "max fps",
    "avg fps", "min kbps", "max kbps", "avg kbps", "time sec", "path"]
  out = ','.join(header) + "\n"
  for x in data:
    out += ','.join(x) +"\n"
  print out

printcsv(sys.argv[1])
