Bitrate Calculator
==================

### 목적
IP 카메라의 Bitrate,FPS를 지정한 조건에 따라 자동으로 추출한다.
** 지정할 수 있는 조건:**

* Codec
  * h264 (VBR only)
  * mjpeg
* Resolution
  * 1920x1080,1280x720,800x450,640x360,480x270
* quality
 * highest,high,normal,low,lowest # H264
  * [1 ~ 100] # MJPEG
* FPS
  * [1 ~ 30]

### 전제조건
* OS: Windows7 (XP? not tested)
* Prerequisite APP:
 * Pythone 2.7 
   
### 사용법
Windows command line에서  
`#> python CalcBitate.py`

### 환경파일(Input)
`$ROOT\profile_config.txt` 파일에 전체적인 동작에 대한 정보가 있다. 아래내용참조

	## profile_config.txt
	## Profile Config
	IP_ADDR = 192.168.241.24
	PATH_PROFILES = C:\test\bw-calc-py          # Profile이 담긴 폴더 
	PATH_RESULT = C:\test\bw-calc-py\Results    # 결과가 저장될 폴더
	PATH_SIMPLE_VIEWER = C:\test\bw-calc-py\SimpleViewer  # SimpleViewer 프로그램의 위치
	## period = TIME_SETTING_COMPLETE+TIME_WARM_UP+TIME_COLLECTING+(6?)
	## ex) 36 seconds
	TIME_SETTING_COMPLETE = 10      # 코덱설정을 바꾼 후 SimpleViewer를 띄우기 전에 기다릴 시간 (초)
	TIME_WARM_UP = 10               # SimpleViewer가 시작하고 정상 동작하기까지 기다리는 시간 (초) 
	TIME_COLLECTING = 10            # SimpleViewer가 데이타를 모으는 시간 (초)
	NUM_OF_CHANNEL = 1              # SimpleViewer가 실행할 Channel 갯수
	SAVE_STREAM = 0                 # Raw Stream 저장유무

`$ROOT\Scripts\Test_script.txt` 이 파일에 테스트 항목이 있다.

	# Test_script.txt
	# basic setup
	#CGI_SET-->/uapi-cgi/param.cgi?action=update&group=ENCODER.Ch0.Videocodec.St1&enable=no
	#CGI_SET-->/uapi-cgi/param.cgi?action=update&group=ENCODER.Ch0.Videocodec.St0.H264&bitratectrl=vbr&maxfps=30&profile=HiP
	#each step
	#CGI_SET-->/uapi-cgi/param.cgi?action=update&group=ENCODER.Ch0.Videocodec.St0.H264&resolution=1920x1080&qvalue=highest
	VIEW-->1920_1080_highest
	CGI_SET-->/uapi-cgi/param.cgi?action=update&group=ENCODER.Ch0.Videocodec.St0.H264&resolution=1920x1080&qvalue=normal
	VIEW-->1920_1080_medium
	CGI_SET-->/uapi-cgi/param.cgi?action=update&group=ENCODER.Ch0.Videocodec.St0.H264&resolution=1920x1080&qvalue=lowest
	VIEW-->1920_1080_lowest
	CGI_SET-->/uapi-cgi/param.cgi?action=update&group=ENCODER.Ch0.Videocodec.St0.H264&resolution=1280x720&qvalue=highest
	VIEW-->1280_720_highest

### 결과파일(Output)

'$ROOT\Results\' 폴더에 결과 파일이 생성된다.

	*.bmp # snapshot
	*_perf.txt.h264 # h264 raw file
	*_perf.txt.txt  # framerate lists
	*_perf.txt.log  # framerate 결과를 정리한 파일
	*_strm_info.txt # 해당 stream 정보
	*_perf.txt.log **이 파일들의 결과를 모아서 엑셀로 정리한다**

Python Scripts & Application
----------------------------

### VideoMeter.exe(old SimpleViewer)
UMF Framework를 이용한 RTSP Streamer 기반의 RTSP View 프로그램을 수정하여, 접속한 스트림의 Bitrate,FPS,Frame information 등 다양한 정보를 출력/저장하도록 변경되었다.
현재, 프로그램의 argument로 config file를 입력받는데, 지금은 **출력파일의 path정보 이외는 사용되고 있지않다**. 원래 목적에서 변경이 많이 되었으니, 주의해야한다.
실행파일 이름을 VideoMeter.exe로 변경하였다.

### CalcBitrates.py
전체 프로젝트의 메일 프로그램이다. VideoMeter.exe를 이용하여 bitrate,fps를 정보를 저장하고, 이를 파싱하여 최종 결과파일(*.txt.log)을 만들어된다.

### MergeBitrateData.py
최종결과파일(*_perf.txt.log)을 이용하여 엑셀포맷에 해당하는 파일을 만든다.

	사용법:
  #> pythone MergeBitrateData.py test_folder > output.csv

### MergeBitrateData2.py
MergeBitrateData.py와 같은 목적이나, 계산 결과값이 이상하여 다시 작성했음(JB)

	사용법:
	#> python MergeBitrateData2.py test_folder > output.csv

### GenTestScript.py
Tyco 전용 TestScript.txt를 생성한는 스크립트이다. 실행하면 아래 파일들이 생성된다. 생성된 파일을 Test_script.txt로 변경하여 사용하면 된다.

	Test_script.txt.one
	Test_script.txt.simple
	Test_script.txt.one
	Test_script.txt.maxfull
	Test_script.txt.tyco\_full
	Test_script.txt.720p.* 

### complexity.py
RTSP Stream, Video File, Image File에서 복잡도를 추출하는 스크립트

	사용법:
	#> python complexity.py -r rtspurl
	#> python complexity.py -v videofile
	#> python complexity.py -f test-*.bmp

### motion.py
Image File들에서 움직임비율을 추출하는 스크립트

	사용법:
	#> python motion.py test-*.bmp
    
Program Design Brief
--------------------
* CalcBitate.py가 실행되면, profile_config.txt 파일을 읽어 기본동작에 대한 정보수집
* loop start
 * Test_script.txt의 CGI_SET 항목을 실행
 * wait for TIME_SETTING_COMPLETE
 * VideoMeter.exe 실행
 * wait for TIME_WARM_UP
 * VideoMeter.exe는 TIME_COLLECTING 시간만큼 *_perf.txt.txt 파일에 정보를 저장한다.
 * CalcBitate.py는 TIME_COLLECTING 이 지난후, VideoMeter.exe 프로세서를 강제 종료시킨다. **중요!!!**
 * *_perf.txt.txt 파일을 읽어, *_perf.txt.log를 생성한다.
* loop end


Complexity vs MotionRate
------------------------

코덱에 따른 압축률(bitrate)은 영상의 복잡도와 움직에 가장 민감하게 연동된다. 코덱제품의 비트레이트를 측정함에 있어 이 두가지 조건에 대한 기준점을 수치화하여 테스트를 하여야 그 결과를 신뢰할수 있다.

### 복잡도(Complexity)
UDP 는 복잡도를 아래와 같이 정의한다.
카메라의 최고 품질의 Raw Data Image 1280x720 영상을 JPEG 표준 품질(75%) 로 압축한 파일크기를 영상면적의 제곱근(sqrt(1280x720))으로 나눈값.

	표준품질(75%) JPEG DQT Value
	{0: array('b', [8, 6, 6, 7, 6, 5, 8, 7, 7, 7, 9, 9, 8, 10, 12, 20, 13, 12, 11, 11, 12, 25, 18, 19, 15, 20, 29, 26, 31, 30, 29, 26, 28, 28, 32, 36, 46, 39, 32, 34, 44, 35, 28, 28, 40, 55, 41, 44, 48, 49, 52, 52, 52, 31, 39, 57, 61, 56, 50, 60, 46, 51, 52, 50]), 1: array('b', [9, 9, 9, 12, 11, 12, 24, 13, 13, 24, 50, 33, 28, 33, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50])}

	복잡도에 따른 값의 대략적인 분포
	백지영상은       : 20 근방
	단순한 영상      : 50 ~ 80
	일반적인 영상    : 80 ~ 110
	조금 복잡한 영상 : 100 ~ 130
	아주 복잡한 영상 : 130 ~
	개발자 작업 환경을 찍으면 100정도 나옴. (천장의 하얀색이 영향)
	연구소전체를 천장에서 찍으면 120~130 정도 나옴.(많은 파티션들을 복잡하게 보이는것 같음)

### 움직임량(MotionRate)
UDP 는 움직임량을 아래와 같이 정의한다.
1초동안 움직인 각 영상차들 누적 영상 대비 전체 영상의 비율

**Complexity와 MotionRate는 VideoMeter.exe의 메뉴에서 활성화하여 직접 실시간으로 계측할수 있다.**

----

Test Information
----------------

### Tyco Bandwidth Calculator
과거 테스트동영상을 참조하여 보니, Complexity 118, MotionRate 40% 로 테스트를 하였음을 알수 있었다.  
`#>python complexity.py -v tyco_test.mp4`  
`#>python motion.py tyco_test*.bmp`  
Tyco에 새로운 펌웨어(1.2.3), HW(Mini Bullet)의 BandWidth 결과를 전달해야하므로, 과거와 동일한 조건에서 테스트를 진행하였다.

### Test_script.txt
과거의 SimpleViewerWin32.exe가 Low FPS(<4fps)에서 동작이 불안하였다. 이로인해 테스트조건을 상당히 길게하였으나, 버그를 수정하여 30초 정도만 데이트를 수집하도록 하였다. 그 외에 다른 fps는 10초면 충분하다.

과거 profile_config.txt

	TIME_SETTING_COMPLETE = 60 # 코덱설정을 바꾼 후 SimpleViewer를 띄우기 전에 기다릴 시간 (초)
	TIME_WARM_UP = 60          # SimpleViewer가 시작하고 정상 동작하기까지 기다리는 시간 (초) 
	TIME_COLLECTING = 360      # SimpleViewer가 데이타를 모으는 시간 (초)

현재 profile_config.txt

	TIME_SETTING_COMPLETE = 10 # 코덱설정을 바꾼 후 SimpleViewer를 띄우기 전에 기다릴 시간 (초)
	TIME_WARM_UP = 10          # SimpleViewer가 시작하고 정상 동작하기까지 기다리는 시간 (초) 
	TIME_COLLECTING = 10       # SimpleViewer가 데이타를 모으는 시간 (초)

참고로 Test_script.txt의 VIEW 명령에 추가적인 옵션을 통해 위 세가지 시간을 개별 조절할 수 있다. 이 옵션이 없으면, profile_config의 값을 사용한다.

	#Sample Test_script.txt
	#VIEW-->filename, TIME_COLLECTING, TIME_WARM_UP, TIME_SETTING_COMPLETE
	CGI_SET-->/uapi-cgi/param.cgi?action=update&group=ENCODER.Ch0.Videocodec.St0.h264&resolution=1920x1080&maxfps=30&qvalue=highest
	VIEW-->T_h264_1920x1080_30_highest,10,5,15
	CGI_SET-->/uapi-cgi/param.cgi?action=update&group=ENCODER.Ch0.Videocodec.St0.h264&resolution=1920x1080&maxfps=30&qvalue=high
	VIEW-->T_h264_1920x1080_30_high,10,5,0

### UDP QA Test Conditions

* Comlexity : 90(보통), 120(복잡)
* MotionRate : 0%(고정), 10%(보통움직임), 40%(과도한 움직임)
* Codec : h264 (VBR only), MJPEG
* Resolution : 1920x1080, 1280x720, 800x450, 640x360, 480x270
* Quality : 
  * H264 : highest,high,normal,low,lowest
  * MJPEG : 1, 50, 75, 100
* FPS : 1, 7, 15, 30


----

Test Case Example
-----------------
1. Windows에 Python2.7 설치  
	1.1 (optional) install [PIL](http://effbot.org/downloads/PIL-1.1.7.win32-py2.7.exe)
 
2. 본 Repository에 있는 Binary를 다운 받는다.  
	`#> wget https://bitbucket.org/jbyoon/calcbitrate/get/f47f98930990.zip`

3. profile_config.txt에서 아래 부분을 해당 상황에 맞게 수정한다.

	`IP_ADDR = 192.168.241.24 # Target Camera IP`  
	`PATH_PROFILES = C:\test\bw-calc-py	# Profile이 담긴 폴더`  
	`PATH_RESULT = C:\test\bw-calc-py\Results	# 결과가 저장될 폴더`  
	`PATH_SIMPLE_VIEWER = C:\test\bw-calc-py\SimpleViewer  # SimpleViewer 프로그램의 위치`  
  `**PATH_RESULT 폴더가 없는 경우, 수동으로 꼭 생성해야한다.**`

4. Scripts/Test_script.txt를 테스트 하고자 하는 Script 파일로 교체한다.  
	`#> copy Test_script.txt.tyco_full Test_script.txt`

5. Webpage에서 테스트하고자 하는 카메라의 설정을 아래와 같이 변경한다.

	`Codec : H264`  
	`profile : High`  
	`Bitrate Control : VBR`  
	`GOP : 30`  
	`Resolution : 1280x720`  
	`FPS : 30`

6. VideoMeter.exe를 이용하여, Complexity와 MotionRate를 측정하면서, 테스트 하고자 하는 영상 조건으로 카메라 구도를 설정한다.

7. 위조건을 모두 결정한 후, CalcBitrate.py를 실행한다.  
	`#> python CalcBitrate.py`

8. (tyco full test시 2시간 소요) 테스트가 끝나면, PATH_RESULT 폴더를 다른 곳으로 복사한다.  
	`#> xcopy Results backup\tyco-i610`
	`#> del Results\*.*`

9. 모든 데이터 취득 작업이 완료된후, 최종 결과파일을 만들어 낸다.  
	`#> python MergeBitrate2.py backup\tyco-i610 > tyco-i610.csv`




