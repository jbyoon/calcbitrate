#! /usr/bin/env python

import os,sys
import subprocess
import operator
import fnmatch

###############################################################################
# Merge From Raw File (*_perf.txt.txt)

####################################
# raw data file format
# (rtsp time:FILETIME, bitrate, client time:FILETIME) # sec = FILETIME/1000000
#
def loadrawdata(path):
  f = open(path,'r')
  data= eval('['+f.read()+']')
  f.close()
  count = len(data)
  if count > 5:
    del data[0:5]

  for i in xrange(len(data)):
    data[i] = (data[i][0],data[i][1]*8,data[i][2])
#  print data
#  print count,len(data)
  return data

def findfiles(pattern, path):
  result = []
  for root, dirs, files in os.walk(path):
    for d in dirs:
      rs = findfiles(pattern, d)
      if len(rs) > 0:
        result.append(rs)
    for name in files:
      if fnmatch.fnmatch(name, pattern):
          result.append(os.path.join(root, name))
  return result

def avgsdvlist(data):
  count = len(data)
  s2,s = 0,0
  for e in data:
      s += e
      s2 += e * e
  avg = s/count
  vr = float(s2)/count-avg*avg
  return avg, round(vr**0.5,2)

# bits per sec
def calcbps_gop(data):
  count = len(data)
  bitrates = zip(*data)[1]
  exavg = (sum(bitrates)/count)
  exavg,exsdv = avgsdvlist(bitrates)
  avgx2 = exavg*2
#  print "ex:",exavg*8,exsdv*8
  if exsdv > exavg/2: # it has gop
    gops,avgs=[],[]
    firsti = -1
    first = 0
    for i,b in enumerate(bitrates):
      if b > avgx2:
        if firsti < 0:
          first =i
        if firsti >= 0:
          gops.append(i-firsti)
          avgs.append(sum(bitrates[firsti:i])/(i-firsti))
        firsti = i
    gopsize = sum(gops)/len(gops)
    last = (count/gopsize)*gopsize - 1
    totalgop = last/gopsize
  else:
    last = count-1

  timeindex = 2 # RTSP Time
#  timeindex = 2 # Client Time
#  print "RTSP:",data[last][timeindex]-data[0][0],"Client:",data[last][2]-data[0][2]
  duration = data[last][timeindex]-data[0][timeindex]
  # time base
  tavg, sdv = avgsdvlist(bitrates[first:last])
#  print first,last,tavg
  # gop base
  avg, sdv = avgsdvlist(avgs)
#  print gopsize, last+1, exavg, avg, sdv
  return avg, int(sdv)

def makesecdata(data,index):
  secdata = []
  bpsdata = zip(zip(*data)[1],zip(*data)[index])
  basetm = bpsdata[0][1]
  bps = bpsdata[0][0]
  fps=0 # ignore the first frame
  nsec = 1
  for br,tm in bpsdata[1:]:
    bps += br
    fps += 1
    if (tm-basetm) >= nsec*10000000:
      secdata.append((fps,bps))
      bps,fps,nsec = 0,0,nsec+1
  return secdata

# bits per sec
def calcfpsbps_time(data):
  count = len(data)
  bitrates = zip(*data)[1]
  last = count-1
  timeindex = 0 # 0 RTSP Time, 2 Client Time
#  print "RTSP:",data[last][timeindex]-data[0][0],"Client:",data[last][2]-data[0][2]
#  duration = data[last][timeindex]-data[0][timeindex]
#  sec = float(duration)/10000000
#  avgbps = int(sum(bitrates)/sec)
#  avgfps = round(count/sec,2)

  secdata = makesecdata(data,timeindex)
  secdata = secdata[4:] # remove first 4 seconds
  sec = float(len(secdata))
  # fps,bps
  sb = sorted(secdata,key=operator.itemgetter(1))
  minbps,maxbps = sb[0][1],sb[-1][1]
  avgbps = int(sum(zip(*secdata)[1])/sec)
  sf = sorted(secdata,key=operator.itemgetter(0))
  minfps,maxfps = sf[0][0],sf[-1][0]
  avgfps = sum(zip(*secdata)[0])/sec
  return minbps/1000,maxbps/1000,avgbps/1000,minfps,maxfps,avgfps,round(sec,2)

def getfpsbps(fpath):
  fname=os.path.basename(fpath)
  (codec,res,fps,quality) = fname.split('_')[1:5]
  rawdata=loadrawdata(fpath)
#  return [codec,res,fps,quality,calcfpsbps_time(rawdata),fname]
  minbps,maxbps,avgkbps,minfps,maxfps,avgfps,sec,=calcfpsbps_time(rawdata)
  return [codec,res,fps,quality,
    str(minfps),str(maxfps),str(avgfps),str(minbps),str(maxbps),str(avgkbps),
    str(sec),fpath]

def calcfpsbps(path="."):
  header = ["codec","resolution","fps","quality",
  "min fps","max fps","avg fps",
  "min kbps","max kbps","avg kbps", "sec", "path"]
  out = ','.join(header) + "\n"
  data=[]
  for fpath in findfiles('*.txt.txt', path):
    data.append(getfpsbps(fpath))
  a=sorted(data,key=operator.itemgetter(3))
  b=sorted(a,key=operator.itemgetter(2))
  c=sorted(b,key=operator.itemgetter(1))
  fd=sorted(c,key=operator.itemgetter(0))
  for x in fd:
    out += ','.join(x) +"\n"
  if len(fd) == 0:
    print "No files\n"
  print out

#d=loadrawdata('back/M022/D-full-10-0/T_h264_1280x720_30_high_Test#_0_ch_0_perf.txt.txt')
#print getfpsbps('back/M022/D-full-10-0/T_h264_1920x1080_30_lowest_Test#_0_ch_0_perf.txt.txt')
#calcfpsbps('back/M022/D-full-10-0')
#calcfpsbps('back/M022/i610-M022-rec60sec')
calcfpsbps(sys.argv[1])

