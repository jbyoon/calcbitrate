#include "stdafx.h"	//Commoninterface refer to upper folder stdafx.
#include "UMFTracer.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>


#pragma		warning( disable :4996 )

#define UMF_TRACE_BUFFER_LEN	1024
#define UMF_LINE_NO_NONE		-1
#define MAX_MODULE_NAME			32
#define	MAX_LOGMODULE_CNT		64
#define	DEFAUL_LOG_FILENAME		L"UMF_Debug_Log.txt"


typedef struct{
	WCHAR				name[MAX_MODULE_NAME];
	eUMF_TRACE_LEVEL	level;
	ULONG				option[UMF_TRACE_LEVEL_MAX];
	FILE *				file;
	WCHAR				filename[MAX_PATH];
	FUNC_ON_EVENTLOG	notifier;
}UMF_LOGINFO;

//Initialize Global LOG Information
static BOOL			s_bWin7;
static int			s_nLogInfoCnt;
static UMF_LOGINFO	s_LogInfo[MAX_LOGMODULE_CNT]; 


inline UMF_LOGINFO* GetModuleLogInfo(WCHAR*	szModuleName)
{
	UMF_LOGINFO *pLogInfo = NULL;
	if(szModuleName == NULL){
		pLogInfo	= &s_LogInfo[0];
	}else{
		for( int i = 1 ; i < s_nLogInfoCnt ; i++){
			if(wcsncmp(s_LogInfo[i].name, szModuleName, MAX_MODULE_NAME) == 0){
				pLogInfo	= &s_LogInfo[i];
				break;
			}
		}
	}

	return pLogInfo;
}


inline void	SetDefulatLogInfo(UMF_LOGINFO *pUMFLogInfo)
{
	pUMFLogInfo->level	= UMF_TRACE_LEVEL_WARNING;
	pUMFLogInfo->option[UMF_TRACE_LEVEL_CRITICAL]	= UMF_TRACE_OUTPUT_MESSAGGEBOX|UMF_TRACE_DEFAULT_OPTION;
	pUMFLogInfo->option[UMF_TRACE_LEVEL_ERROR]		= UMF_TRACE_OUTPUT_MESSAGGEBOX|UMF_TRACE_DEFAULT_OPTION;
	pUMFLogInfo->option[UMF_TRACE_LEVEL_WARNING]	= UMF_TRACE_OUTPUT_LOG|UMF_TRACE_DEFAULT_OPTION;
	pUMFLogInfo->option[UMF_TRACE_LEVEL_VERBOSE]	= UMF_TRACE_OUTPUT_LOG|UMF_TRACE_DEFAULT_OPTION;

	if(s_bWin7){
		GetEnvironmentVariable(L"TEMP", pUMFLogInfo->filename, MAX_PATH );
		wcscat(pUMFLogInfo->filename, L"\\");
		wcscat(pUMFLogInfo->filename, DEFAUL_LOG_FILENAME);
	}else{
		wcscpy(pUMFLogInfo->filename, L"C:\\");
		wcscat(pUMFLogInfo->filename, DEFAUL_LOG_FILENAME);
	}

	pUMFLogInfo->notifier	= NULL;
}


BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	//Get temporary file for Windows7 does not have permission write 
	//something so temporary directory need 
	if(DLL_PROCESS_ATTACH == ul_reason_for_call){
		OSVERSIONINFO ovi;
		ovi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
	    GetVersionEx(&ovi);
		s_bWin7 = (ovi.dwMajorVersion >= 6)?TRUE:FALSE;
				
		//Set Global setting
		s_nLogInfoCnt		= 0;
		memset(s_LogInfo[0].name, 0, MAX_MODULE_NAME);
		SetDefulatLogInfo(&s_LogInfo[s_nLogInfoCnt]);
		s_nLogInfoCnt		= 1;
	}

	return TRUE;
}


void	WINAPI	UMFSetTraceModule(WCHAR *szModuleName)
{
	if(szModuleName == NULL) return;

	//find same module name 
	for( int i = 1 ; i < s_nLogInfoCnt ; i++){
		if(wcsncmp(s_LogInfo[i].name, szModuleName, MAX_MODULE_NAME) == 0) return;
	}

	if(s_nLogInfoCnt+1 > MAX_LOGMODULE_CNT) return;
	
	wcsncpy(s_LogInfo[s_nLogInfoCnt].name, szModuleName, MAX_MODULE_NAME); 
	s_LogInfo[s_nLogInfoCnt].name[MAX_MODULE_NAME-1] = 0;

	SetDefulatLogInfo(&s_LogInfo[s_nLogInfoCnt]);
	s_nLogInfoCnt++;
}


void	WINAPI	UMFSetTraceLevelEx(WCHAR *szModuleName, eUMF_TRACE_LEVEL eTraceLevel)
{
	UMF_LOGINFO *pLogInfo = GetModuleLogInfo(szModuleName);
	if(NULL == pLogInfo) return;
	pLogInfo->level	= eTraceLevel;
}


eUMF_TRACE_LEVEL	WINAPI	UMFGetTraceLevelEx(WCHAR *szModuleName)
{
	UMF_LOGINFO *pLogInfo = GetModuleLogInfo(szModuleName);
	if(NULL == pLogInfo) return UMF_TRACE_LEVEL_MAX;
	return pLogInfo->level;
}


void	WINAPI	UMFSetTraceOptionEx(WCHAR *szModuleName, eUMF_TRACE_LEVEL eTraceLevel, ULONG TraceOption)
{
	UMF_LOGINFO *pLogInfo = GetModuleLogInfo(szModuleName);
	if(NULL == pLogInfo) return;

	pLogInfo->option[eTraceLevel] = TraceOption;
}


ULONG	WINAPI	UMFGetTraceOptionEx(WCHAR *szModuleName, eUMF_TRACE_LEVEL eTraceLevel)
{
	UMF_LOGINFO *pLogInfo = GetModuleLogInfo(szModuleName);
	if(NULL == pLogInfo) return UMF_TRACE_OUTPUT_SLIENT;

	return pLogInfo->option[eTraceLevel];
}


BOOL	WINAPI	UMFSetOutputFileNameEx(WCHAR *szModuleName, WCHAR *file, BOOL truncate)
{
	UMF_LOGINFO *pLogInfo = GetModuleLogInfo(szModuleName);
	if(NULL == pLogInfo) return UMF_TRACE_OUTPUT_SLIENT;

	if ( (file && ! wcscmp(pLogInfo->filename, file)) || (file == NULL) ) {
		return FALSE;
	}

	if (pLogInfo->file) {
		fclose(pLogInfo->file);
		pLogInfo->file = NULL;
	}

	wcscpy(pLogInfo->filename, file);
	pLogInfo->file = _wfopen(pLogInfo->filename, truncate ? L"w" : L"a");
	
	return (BOOL)pLogInfo->file;
}


void	WINAPI	UMFSetOutputEventLogEx(WCHAR *szModuleName, FUNC_ON_EVENTLOG pFunc)
{
	UMF_LOGINFO *pLogInfo = GetModuleLogInfo(szModuleName);
	if(NULL == pLogInfo) return ;

	pLogInfo->notifier	= pFunc;
}


inline	void	OutputFileWrite(UMF_LOGINFO *pLogInfo, WCHAR *szBuffer)
{
	//Open if not open before
	if (!pLogInfo->file) {
		pLogInfo->file = _wfopen(pLogInfo->filename, L"w");
		if(!pLogInfo->file) return;
	}

	fprintf(pLogInfo->file, "%S\n", szBuffer);
	fflush(pLogInfo->file);
}


WCHAR*	s_LogLevel[4] = {{L"[CRITICAL]"}, {L"[ERROR]"}, {L"[WARNING]"} , {L"[VERVOSE]"}};


void	WINAPI	UMFTraceEX(eUMF_TRACE_LEVEL eLevel, WCHAR *szModuleName, WCHAR* szFile, int nLine, WCHAR* szFunc, WCHAR* lpszFormat, ...)
{
	UMF_LOGINFO *pLogInfo = GetModuleLogInfo(szModuleName);
	if(NULL == pLogInfo) return;
	
	if(pLogInfo->level < eLevel) return;
	if(UMF_TRACE_OUTPUT_SLIENT == pLogInfo->option[eLevel]) return;

	int nBuf = 0;
	WCHAR buffer[UMF_TRACE_BUFFER_LEN] = {0,};
	ULONG DisplayOption = pLogInfo->option[eLevel];
	
	BOOL bNeedNewLine = FALSE;

	int	bufferSize	= UMF_TRACE_BUFFER_LEN;
	int bufferUsed	= 0;
	int bufferLeft	= bufferSize - bufferUsed;
	int perElementBufferUsed;


	// Write filename, line number, function and time. 
	if(DisplayOption & UMF_TRACE_DISPLAY_LEVEL) {
		WCHAR	*szLogLevel = s_LogLevel[eLevel];
		perElementBufferUsed = _snwprintf(&buffer[bufferUsed], bufferLeft, L"%s ", szLogLevel); // C4996
		
		bufferUsed += perElementBufferUsed;
		bufferLeft -= perElementBufferUsed;
	}

	if(DisplayOption & UMF_TRACE_DISPLAY_FILENAME) {
		if(szFile != NULL){
			perElementBufferUsed = _snwprintf(&buffer[bufferUsed], bufferLeft, L"filename: %s ", szFile); // C4996
			if((perElementBufferUsed < 0)) goto OUTPUT_EXIT;

			bufferUsed += perElementBufferUsed;
			bufferLeft -= perElementBufferUsed;

			bNeedNewLine = TRUE;
		}
	}
	if(DisplayOption & UMF_TRACE_DISPLAY_LINE) {
		if(nLine != UMF_LINE_NO_NONE){
			perElementBufferUsed = _snwprintf(&buffer[bufferUsed], bufferLeft, L"line: %d ", nLine); // C4996
			if((perElementBufferUsed < 0)) goto OUTPUT_EXIT;

			bufferUsed += perElementBufferUsed;
			bufferLeft -= perElementBufferUsed;

			bNeedNewLine = TRUE;
		}
	}
	if(DisplayOption & UMF_TRACE_DISPLAY_FUNC) {
		if(szFunc != NULL){
			perElementBufferUsed = _snwprintf(&buffer[bufferUsed], bufferLeft, L"%s() ", szFunc); // C4996
			if((perElementBufferUsed < 0)) goto OUTPUT_EXIT;

			bufferUsed += perElementBufferUsed;
			bufferLeft -= perElementBufferUsed;

			bNeedNewLine = TRUE;
		}
	}
	if(DisplayOption & UMF_TRACE_DISPLAY_TIME) {
		SYSTEMTIME stTime;
		GetSystemTime(&stTime);

		perElementBufferUsed = _snwprintf(&buffer[bufferUsed], bufferLeft, L"%02d:%02d:%02d.%04d", 
			stTime.wHour, stTime.wMinute, stTime.wSecond, stTime.wMilliseconds); // C4996
		if((perElementBufferUsed < 0)) goto OUTPUT_EXIT;

		bufferUsed += perElementBufferUsed;
		bufferLeft -= perElementBufferUsed;

		bNeedNewLine = TRUE;
	}

	if(bNeedNewLine){
		perElementBufferUsed = 1;
		wcscat(&buffer[bufferUsed], L"\n");
		bufferUsed += perElementBufferUsed;
		bufferLeft -= perElementBufferUsed;
	}

	//ModuleName at the beginning of the string
	if(pLogInfo->name[0] != NULL){
		perElementBufferUsed = _snwprintf(&buffer[bufferUsed], bufferLeft, L"[%s] ", pLogInfo->name); 
		if((perElementBufferUsed < 0)) goto OUTPUT_EXIT;

		bufferUsed += perElementBufferUsed;
		bufferLeft -= perElementBufferUsed;
	}
		
	//Last 
	va_list args;
    va_start(args, lpszFormat);
	perElementBufferUsed = _vsnwprintf(&buffer[bufferUsed], bufferLeft, lpszFormat, args);
	va_end(args);

OUTPUT_EXIT:

	// Output Message
	OutputDebugStringW(buffer);
	
	if(DisplayOption & UMF_TRACE_OUTPUT_FILE ) {
		OutputFileWrite(pLogInfo, buffer);
	}

	if(DisplayOption & UMF_TRACE_OUTPUT_EVENTLOG) {
		if(pLogInfo->notifier){
			(*pLogInfo->notifier)(eLevel, buffer);
		}
	}

	if(DisplayOption & UMF_TRACE_OUTPUT_MESSAGGEBOX) {
		MessageBoxW(NULL, buffer, L"UMF ERROR" ,MB_OK);
	}


	if(DisplayOption & UMF_TRACE_OUTPUT_CONSOLE_STDOUT) {
		fputws(buffer, stdout);
	}

	if(DisplayOption & UMF_TRACE_OUTPUT_CONSOLE_STDERR) {
		fputws(buffer, stderr);
	}

	
}

