#include "Stdafx.h"
#include <stdio.h>
#include <math.h>

int get_u1(unsigned char *data, int datalen, int bitoffset)
{
	int dataidx = (bitoffset / 8);
	int data_bitoffset = bitoffset % 8;

	if(datalen<=dataidx){
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("get_u1 data Error\n"));
		return -1;
	}

	if(data[dataidx] & (0x80 >> (data_bitoffset)))
		return 1;
	return 0;
}

int get_ue(unsigned char *data, int datalen, int bitoffset, int *bitlength)
{

	int dataidx = (bitoffset / 8);
	int data_bitoffset = bitoffset % 8;
	int check_cnt = 0;
	int val = 1;
	int i;

	if(datalen<=dataidx){
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("get_ue data Error\n"));
		return -1;
	}

	while(bitoffset < datalen * 8){
		if(data[dataidx] & (0x80 >> (data_bitoffset))){
			bitoffset++;
			data_bitoffset++;
			if(data_bitoffset>=8){
				data_bitoffset=0;
				dataidx++;
			}
			break;
		}else
			check_cnt++;

		bitoffset++;
		data_bitoffset++;
		if(data_bitoffset>=8){
			data_bitoffset=0;
			dataidx++;
		}
	}

	val = (int)pow(2, (double)check_cnt);

	for(i=0;i<check_cnt;i++){
		char temp = data[dataidx] & (0x80 >> (data_bitoffset));
		if(temp){
			val += (int)pow(2, (double)(check_cnt-1-i));
		}

		bitoffset++;
		data_bitoffset++;
		if(data_bitoffset>=8){
			data_bitoffset=0;
			dataidx++;
		}
	}

	*bitlength = bitoffset;

	return val-1;
}

int get_se(unsigned char *data, int datalen, int bitoffset, int *_bitlength)
{
	int val;

	val = get_ue(data, datalen, bitoffset, _bitlength);
	val = (int)pow(-1, (double)(val+1)) * (int)ceil((double)(val/2));

	return val;
}

UINT ParseH264Sps(BYTE *pSps, UINT uDataLen, UINT *puWidth, UINT *puHeight)
{
	int bitoffset = 0, len=0, val=0;	
	int profile_idc;
	int i, sps_len = 0;
	int chroma_format_idc = 0;

	uDataLen *= 8;

	//profile_idc 8bit
	profile_idc = pSps[0];
	bitoffset += 8;

	//constraint_set_flag 4bit
	bitoffset += 4;

	//reserved_zero_4bit
	bitoffset += 4;

	//level_idc 8bit
	bitoffset += 8;

	//seq_parameter_set_id ue(v)
	val = get_ue(pSps, uDataLen, bitoffset, &len);
	bitoffset = len;

	if(profile_idc==100 || profile_idc==110 || profile_idc==122 ||
		profile_idc==244 || profile_idc==44 || profile_idc==83	|| profile_idc==86){
			//chroma_format_idc ue(v)		
			chroma_format_idc = get_ue(pSps, uDataLen, bitoffset, &len);
			bitoffset = len;

			if(val==3){
				//separate_colour_plane_flag u(1)
				bitoffset += 1;
			}

			//bit_depth_luma_minus8 ue(v)
			val = get_ue(pSps, uDataLen, bitoffset, &len);
			bitoffset = len;

			//bit_depth_chroma_minus8 ue(v)
			val = get_ue(pSps, uDataLen, bitoffset, &len);
			bitoffset = len;

			//qpprime_y_zero_transform_bypass_flag u(1)
			bitoffset += 1;

			//seq_scaling_matriz_present_flag u(1)
			int seq_scaling_matriz_present_flag;
			seq_scaling_matriz_present_flag = get_u1(pSps, uDataLen, bitoffset);
			bitoffset += 1;

			if(seq_scaling_matriz_present_flag){
				int seq_scaling_list_present_flag;
				for(i=0;i<((chroma_format_idc!=3)?8:12);i++){
					//seq_scaling_list_present_flag[i] u(1)
					seq_scaling_list_present_flag = get_u1(pSps, uDataLen, bitoffset);
					bitoffset += 1;
					if(seq_scaling_list_present_flag){
						if(i<6){
							//scaling_list(ScalingList4x4[i], 16, UseDefaultScalingMatrix4x4Flag[i])	?????
							;
						}else{
							//scaling_list(ScalingList8x8[i], 64, UseDefaultScalingMatrix8x8Flag[i-6])	?????
							;
						}
					}
				}
			}		
	}

	//log2_max_frame_num_minus4 ue(v)
	val = get_ue(pSps, uDataLen, bitoffset, &len);
	bitoffset = len;

	//pic_order_cnt_type ue(v)
	val = get_ue(pSps, uDataLen, bitoffset, &len);
	bitoffset = len;

	if(val==0){
		//log2_max_pic_order_cnt_lsb_minus4 ue(v)
		val = get_ue(pSps, uDataLen, bitoffset, &len);
		bitoffset = len;
	}else if(val==1){
		//delta_pic_order_always_zero_flag u(1)
		bitoffset += 1;

		//offset_for_non_ref_pic se(v)
		val = get_se(pSps, uDataLen, bitoffset, &len);
		bitoffset = len;

		//offset_for_top_to_bottom_field se(v)
		val = get_se(pSps, uDataLen, bitoffset, &len);
		bitoffset = len;

		//num_ref_frames_in_pic_order_cnt_cycle ue(v)
		int num_ref_frames_in_pic_order_cnt_cycle;
		num_ref_frames_in_pic_order_cnt_cycle = get_ue(pSps, uDataLen, bitoffset, &len);
		bitoffset = len;

		for(i=0;i<num_ref_frames_in_pic_order_cnt_cycle;i++){
			//offset_for_ref_frame[i] se(v)
			val = get_se(pSps, uDataLen, bitoffset, &len);
			bitoffset = len;
		}
	}

	//num_ref_frames ue(v)
	val = get_ue(pSps, uDataLen, bitoffset, &len);
	bitoffset = len;

	//gaps_in_frame_num_value_allowed_flag u(1)
	bitoffset += 1;

	//pic_width_in_mbs_minus1 ue(v)
	val = get_ue(pSps, uDataLen, bitoffset, &len);
	bitoffset = len;
	*puWidth = (val+1) * 16;

	//pic_height_in_map_units_minus1 ue(v)
	val = get_ue(pSps, uDataLen, bitoffset, &len);
	bitoffset = len;	
	*puHeight = (val+1) * 16;

	//frame_mbs_only_flag u(1)
	int frame_mbs_only_flag = get_u1(pSps, uDataLen, bitoffset);
	bitoffset += 1;

	if(!frame_mbs_only_flag){
		//mb_adaptive_frame_field_flag u(1)
		bitoffset += 1;
	}

	//direct_8x8_inference_flag u(1)
	bitoffset += 1;

	//frame_cropping_flag u(1)
	int frame_cropping_flag = get_u1(pSps, uDataLen, bitoffset);
	bitoffset += 1;

	if(frame_cropping_flag){
		//frame_crop_left_offset ue(v)
		int frame_crop_left_offset = get_ue(pSps, uDataLen, bitoffset, &len);
		bitoffset = len;

		//frame_crop_right_offset ue(v)
		int frame_crop_right_offset = get_ue(pSps, uDataLen, bitoffset, &len);
		bitoffset = len;

		*puWidth -= ((frame_crop_left_offset+frame_crop_right_offset)*2);

		//frame_crop_top_offset ue(v)
		int frame_crop_top_offset = get_ue(pSps, uDataLen, bitoffset, &len);
		bitoffset = len;

		//frame_crop_bottom_offset ue(v)
		int frame_crop_bottom_offset = get_ue(pSps, uDataLen, bitoffset, &len);
		bitoffset = len;

		*puHeight -= ((frame_crop_top_offset+frame_crop_bottom_offset)*2*(2-frame_mbs_only_flag));
	}

	//vui_parameters_present_flag u(1)
	bitoffset += 1;

	sps_len = bitoffset/8 +1;
	if(bitoffset%8)
		sps_len++;

	return sps_len;
}
