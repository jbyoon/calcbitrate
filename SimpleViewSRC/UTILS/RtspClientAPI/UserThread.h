#ifndef __USER_THREAD_H__
#define __USER_THREAD_H__

enum ThreadState {
	TS_STOP		= 1,
	TS_RUN		= 2
};

struct THREAD_PARAM_SET {
	VOID *pParam1;
	VOID *pParam2;
	VOID *pParam3;
	VOID *pParam4;
};

typedef UINT (__stdcall *PTHREAD_FUNC) (VOID *);

class CUserThread  
{
public:
	CUserThread();
	virtual ~CUserThread();

public:
	BOOL	StartThread(PTHREAD_FUNC pFunc, PVOID pParam1, PVOID pParam2, PVOID pParam3);
	VOID	StopThread(BOOL bWait,  UINT uTime, BOOL bEvent);
	VOID	ReleaseHandle();
	UINT	GetState() { return m_uThreadState; }
	HANDLE	GetKillHandle() { return m_hKill; }
	HANDLE	GetThreadHandle() { return m_hThread; }

private:
	struct THREAD_PARAM_SET m_ParamSet;
	UINT	m_uThreadState;

	HANDLE	m_hThread;
	HANDLE	m_hKill;
};

#endif // __USER_THREAD_H__
