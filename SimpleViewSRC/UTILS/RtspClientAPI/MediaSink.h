#ifndef __MEDIA_SINK_H__
#define __MEDIA_SINK_H__

#include <UsageEnvironment.hh>
#include <liveMedia.hh>

#define MEDIA_SINK_MAX_VIDEO_FRAME_SIZE		(1024*1024)

struct BUFFER_INFO {
	UINT uBufferSize;
	UINT uFrameSize;
	UINT uOffset;
	BYTE* pBuffer;
	struct timeval timeStamp;

	VOID *pInstance;
	BOOL (*MediaCallbackFunc)( struct BUFFER_INFO* pInfo);
};

class CVideoSink: public MediaSink {
public:
	CVideoSink(UsageEnvironment& env, struct BUFFER_INFO *pInfo);
	~CVideoSink();

protected:
	virtual Boolean continuePlaying();
	static void afterGettingFrame(VOID* clientData, 
		unsigned frameSize, 
		unsigned numTruncatedBytes, 
		struct timeval presentationTime, 
		unsigned durationInMicroseconds);
	virtual void afterGettingFrame1(unsigned frameSize, struct timeval presentationTime);

private:
	struct BUFFER_INFO* m_pVideoBuffer;
};

#define MEDIA_SINK_MAX_AUDIO_FRAME_SIZE		8*1024

class CAudioSink: public MediaSink {
public:
	CAudioSink(UsageEnvironment& env, struct BUFFER_INFO *pInfo);
	~CAudioSink();

protected:

	virtual Boolean continuePlaying();
	static void afterGettingFrame(VOID* clientData, 
		unsigned frameSize,
		unsigned numTruncatedBytes,
		struct timeval presentationTime,
		unsigned durationInMicroseconds);
	virtual void afterGettingFrame1(unsigned frameSize, struct timeval presentationTime);

private:
	struct BUFFER_INFO* m_pAudioBuffer;
};

#define MEDIA_SINK_MAX_META_FRAME_SIZE		128*1024

class CMetaSink: public MediaSink {
public:
	CMetaSink(UsageEnvironment& env, struct BUFFER_INFO *pInfo);
	~CMetaSink();

protected:

	virtual Boolean continuePlaying();
	static void afterGettingFrame(VOID* clientData, 
		unsigned frameSize,
		unsigned numTruncatedBytes,
		struct timeval presentationTime,
		unsigned durationInMicroseconds);
	virtual void afterGettingFrame1(unsigned frameSize, struct timeval presentationTime);

private:
	struct BUFFER_INFO* m_pMetaBuffer;
};

#endif // __MEDIA_SINK_H__
