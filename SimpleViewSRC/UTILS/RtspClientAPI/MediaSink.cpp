#include "Stdafx.h"
#include "MediaSink.h"

////////////////////////////////////////////////////////////////////////////////////////////
// CVideoSink
//
CVideoSink::CVideoSink(UsageEnvironment& env, BUFFER_INFO *pInfo)
: MediaSink(env), m_pVideoBuffer(pInfo)
{
}

CVideoSink::~CVideoSink() 
{
}

Boolean CVideoSink::continuePlaying() 
{
	if (m_pVideoBuffer->uOffset + MEDIA_SINK_MAX_VIDEO_FRAME_SIZE + 256 >= m_pVideoBuffer->uBufferSize) {
		m_pVideoBuffer->uOffset = 0;
	}

	m_pVideoBuffer->uOffset += 256; // for H.264 NAL header

	fSource->getNextFrame(m_pVideoBuffer->pBuffer + m_pVideoBuffer->uOffset, 
		MEDIA_SINK_MAX_VIDEO_FRAME_SIZE,
		afterGettingFrame, 
		this, 
		onSourceClosure, 
		this);

	return True;
}

void CVideoSink::afterGettingFrame(VOID* clientData, 
								unsigned frameSize,
								unsigned numTruncatedBytes,
								struct timeval presentationTime,
								unsigned durationInMicroseconds) 
{
	CVideoSink* videoSink = (CVideoSink*)clientData;
	videoSink->afterGettingFrame1(frameSize, presentationTime);
}

void CVideoSink::afterGettingFrame1(unsigned frameSize, struct timeval presentationTime) 
{
	m_pVideoBuffer->uFrameSize = frameSize;
	m_pVideoBuffer->timeStamp = presentationTime;
	m_pVideoBuffer->MediaCallbackFunc(m_pVideoBuffer);
	m_pVideoBuffer->uOffset += frameSize;
	continuePlaying();
}

////////////////////////////////////////////////////////////////////////////////////////////
// CAudioSink
//
CAudioSink::CAudioSink(UsageEnvironment& env, BUFFER_INFO *pInfo) 
: MediaSink(env), m_pAudioBuffer(pInfo)
{
}

CAudioSink::~CAudioSink()
{
}

Boolean CAudioSink::continuePlaying() 
{
	if (m_pAudioBuffer->uOffset + MEDIA_SINK_MAX_AUDIO_FRAME_SIZE >= m_pAudioBuffer->uBufferSize) {
		m_pAudioBuffer->uOffset = 0;
	}

	fSource->getNextFrame(m_pAudioBuffer->pBuffer + m_pAudioBuffer->uOffset,  
		MEDIA_SINK_MAX_AUDIO_FRAME_SIZE,
		afterGettingFrame, 
		this, 
		onSourceClosure, 
		this);

	return True;
}

void CAudioSink::afterGettingFrame(VOID* clientData, 
								unsigned frameSize,
								unsigned numTruncatedBytes,
								struct timeval presentationTime,
								unsigned durationInMicroseconds) 
{
	CAudioSink* audioSink = (CAudioSink*)clientData;
	audioSink->afterGettingFrame1(frameSize, presentationTime);
}

void CAudioSink::afterGettingFrame1(unsigned frameSize, struct timeval presentationTime) 
{
	m_pAudioBuffer->uFrameSize = frameSize;
	m_pAudioBuffer->timeStamp = presentationTime;
	m_pAudioBuffer->MediaCallbackFunc(m_pAudioBuffer);
	m_pAudioBuffer->uOffset += frameSize;
	continuePlaying();
}

////////////////////////////////////////////////////////////////////////////////////////////
// CMetaSink
//
CMetaSink::CMetaSink(UsageEnvironment& env, BUFFER_INFO *pInfo)
: MediaSink(env), m_pMetaBuffer(pInfo)
{
}

CMetaSink::~CMetaSink() 
{
}

Boolean CMetaSink::continuePlaying() 
{
	if (m_pMetaBuffer->uOffset + MEDIA_SINK_MAX_META_FRAME_SIZE >= m_pMetaBuffer->uBufferSize) {
		m_pMetaBuffer->uOffset = 0;
	}

	fSource->getNextFrame(m_pMetaBuffer->pBuffer + m_pMetaBuffer->uOffset, 
		MEDIA_SINK_MAX_META_FRAME_SIZE,
		afterGettingFrame, 
		this, 
		onSourceClosure, 
		this);

	return True;
}

void CMetaSink::afterGettingFrame(VOID* clientData, unsigned frameSize,
                            unsigned numTruncatedBytes,
                            struct timeval presentationTime,
                            unsigned durationInMicroseconds) 
{
	CMetaSink* metaSink = (CMetaSink*)clientData;
	metaSink->afterGettingFrame1(frameSize, presentationTime);
}

void CMetaSink::afterGettingFrame1(unsigned frameSize, struct timeval presentationTime) 
{
	m_pMetaBuffer->uFrameSize = frameSize;
	m_pMetaBuffer->timeStamp = presentationTime;
	m_pMetaBuffer->MediaCallbackFunc(m_pMetaBuffer);
	m_pMetaBuffer->uOffset += frameSize;
	continuePlaying();
}
