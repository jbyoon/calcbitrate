#ifndef __RTSP_RECEIVER_H__
#define __RTSP_RECEIVER_H__

#include "RtspClient.h"
#include "CapQueueLock.h"

#define RTSPRCV_MAX_STREAM_NUM				3	// video, audio, meta
#define	RTSPRCV_VIDEO_BUFFER_SIZE			(6*1024*1024)
#define	RTSPRCV_AUDIO_BUFFER_SIZE			(128*1024)
#define	RTSPRCV_META_BUFFER_SIZE			(512*1024)

#define RTSPRCV_VIDEO_BUFFER_NUM			6
#define RTSPRCV_AUDIO_BUFFER_NUM			8

class CRtspReceiver
{
public:
	CRtspReceiver();
	~CRtspReceiver();

	BOOL Init();
	VOID Release();
	BOOL InitSession(CHAR *pszUrl, CHAR *pszSession, CHAR *pszUsername, CHAR *pszPassword);
	VOID ReleaseSession();
	BOOL StartSession();
	VOID StopSession();
	
	struct STATISTICS* GetVideoStatistics() { return m_rtspSession.GetVideoStatistics(); }
	struct STATISTICS* GetAudioStatistics() { return m_rtspSession.GetAudioStatistics(); }
	BOOL GetVideoInfo(CHAR *pszCodec, UINT *puWidth, UINT *puHeight);
	BOOL GetAudioInfo(CHAR *pszCodec, UINT *puSampleBits, UINT *puSampleRate);
	UINT GetVideoIndex();
	UINT GetAudioIndex();
	BOOL GetJpegResolution(BYTE *pImg, UINT uSize, UINT *puWidth, UINT *puHeight);
	BOOL GetVideoData(BYTE **ppData, UINT *puFrameSize, struct timeval *pTimeStamp);
	BOOL GetAudioData(BYTE **ppData, UINT *puFrameSize, struct timeval *pTimeStamp);
	BOOL GetMetaData(BYTE **ppData, UINT *puFrameSize, struct timeval *pTimeStamp);
	CCapQueueLock<struct BUFFER_INFO, 32> *GetVideoQueue() { return &m_videoQue; }
	CCapQueueLock<struct BUFFER_INFO, 32> *GetAudioQueue() { return &m_audioQue; }
	CCapQueueLock<struct BUFFER_INFO, 32> *GetMetaQueue() { return &m_metaQue; }
	HANDLE GetVideoEvent() { return m_hVideoEvent; }
	HANDLE GetAudioEvent() { return m_hAudioEvent; }
	HANDLE GetMetaEvent() { return m_hMetaEvent; }
	CRtspClient *GetRtspClient() { return &m_rtspSession; }

	static BOOL ProcessVideoDataWrapper(struct BUFFER_INFO *pInfo);
	BOOL ProcessVideoData(struct BUFFER_INFO *pInfo);
	static BOOL ProcessAudioDataWrapper(struct BUFFER_INFO *pInfo);
	BOOL ProcessAudioData(struct BUFFER_INFO *pInfo);
	static BOOL ProcessMetaDataWrapper(struct BUFFER_INFO *pInfo);
	BOOL ProcessMetaData(struct BUFFER_INFO *pInfo);

	BOOL IsAlive();
	BOOL IsKeyFrameArrived(UINT uOffset);
	BOOL IsKeyFrameArrived() { return m_bKeyFrameArrived; }
	UINT IsParseJpeg() { return m_bIsParseJpeg; }

	VOID SetAlive(BOOL bAlive) { m_bAlive = bAlive; }
	VOID SetAliveCheckTime(UINT uTime) { m_rtspSession.SetAliveCheckTime(uTime); }
	VOID SetKeyFrame(BOOL bKeyFrame) { m_bKeyFrameArrived = bKeyFrame; }
	VOID ParseJpegDone() { m_bIsParseJpeg = 1; }
	BOOL ParseVideoParams();

private:
	CRtspClient m_rtspSession;

	struct BUFFER_INFO m_videoBuf;
	struct BUFFER_INFO m_audioBuf;
	struct BUFFER_INFO m_metaBuf;

	CCapQueueLock<struct BUFFER_INFO, 32> m_videoQue;
	CCapQueueLock<struct BUFFER_INFO, 32> m_audioQue;
	CCapQueueLock<struct BUFFER_INFO, 32> m_metaQue;

	CHAR m_szUrl[128];
	CHAR m_szSession[16];
	CHAR m_szUsername[32];
	CHAR m_szPassword[32];

	BYTE *m_pVideoBuffer;
	BYTE *m_pAudioBuffer;
	BYTE *m_pMetaBuffer;

	BYTE m_pVideoParam[2][256];
	UINT m_uVideoParamSize[2];

	UINT m_uVideoIndex;
	UINT m_uAudioIndex;

	BOOL m_bKeyFrameArrived;
	BOOL m_bAlive;
	BOOL m_bIsParseJpeg;

	HANDLE m_hVideoEvent;
	HANDLE m_hAudioEvent;
	HANDLE m_hMetaEvent;

	UINT m_uWidth;
	UINT m_uHeight;
};

#endif // __RTSP_RECEIVER_H__
