#include "Stdafx.h"
#include "RtspClient.h"

CRtspClient::CRtspClient()
	: m_pEnv(NULL), m_pScheduler(NULL), 
	m_pRtspClient(NULL), m_pMediasession(NULL), 
	m_pVideoSink(NULL), m_pAudioSink(NULL), m_pMetaSink(NULL), 
	m_pStatisticsTask(NULL),
	m_pVideoBuffer(NULL), m_pAudioBuffer(NULL), m_pMetaBuffer(NULL), 
	m_uHttpPortNum(0), m_bIsTcp(FALSE), m_bIsMulticast(FALSE),
	m_bStopLoop(0), 
	m_pSdpDescription(NULL),
	m_uInterval(1000),
	m_uLastVideoSrTime(0), m_uLastAudioSrTime(0), m_uLastMetaSrTime(0),
	m_uUpdateCount(0), m_uAudioSampleBits(0), m_uAudioSampleRate(0), m_bIsWait(0), m_uAliveCheckCount(10)
{
	memset(m_szVideoCodec, 0, sizeof(m_szVideoCodec));
	memset(m_szMpeg4Config, 0, sizeof(m_szMpeg4Config));
	memset(m_szH264SpropParameterSets, 0, sizeof(m_szH264SpropParameterSets));
	memset(m_szAudioCodec, 0, sizeof(m_szAudioCodec));
}

CRtspClient::~CRtspClient()
{
}

BOOL CRtspClient::Init(CHAR *pszUrl, 
					  CHAR *pszUserName, 
					  CHAR *pszPassword, 
					  BOOL bIsTcp, 
					  BOOL bIsMulticast, 
					  BUFFER_INFO* pVideoBuffer, 
					  BUFFER_INFO* pAudioBuffer, 
					  BUFFER_INFO* pMetaBuffer)
{
	memset(m_szUrl, 0, sizeof(m_szUrl));
	memset(m_szUserName, 0, sizeof(m_szUserName));
	memset(m_szPassword, 0, sizeof(m_szPassword));

	strncpy(m_szUrl, pszUrl, sizeof(m_szUrl)-1);
	strncpy(m_szUserName, pszUserName, sizeof(m_szUserName)-1);
	strncpy(m_szPassword, pszPassword, sizeof(m_szPassword)-1);
	m_bIsTcp = bIsTcp;
	m_bIsMulticast = bIsMulticast;
	m_pVideoBuffer = pVideoBuffer;
	m_pAudioBuffer = pAudioBuffer;
	m_pMetaBuffer = pMetaBuffer;

	m_pScheduler = BasicTaskScheduler::createNew();
	if (!m_pScheduler) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Cannot create task scheduler\n"));
		return FALSE;
	}
	
	m_pEnv = BasicUsageEnvironment::createNew(*m_pScheduler);
	if (!m_pEnv) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Cannot create usage environment\n"));
		return FALSE;
	}

	if (!CreateClient()) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("CreateClient failed\n"));
		return FALSE;
	}

	if (!Describe()) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Describe failed\n"));
		return FALSE;
	}

	if (!SetupStream()) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("SetupStream failed\n"));
		return FALSE;
	}

	m_uUpdateCount = 0;
	m_nConnectTestTime = GetTickCount();

	m_pStatisticsTask = m_pEnv->taskScheduler().scheduleDelayedTask(
		m_uInterval*1000, 
		(TaskFunc*)StatisticsMeasurementStub, 
		(VOID*)this);

	if (!m_pStatisticsTask) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Cannot start statistics_measurement\n"));
		return FALSE;
	}

	return TRUE;
}

VOID CRtspClient::Release()
{
	if (m_pStatisticsTask) {
		m_pEnv->taskScheduler().unscheduleDelayedTask(m_pStatisticsTask);
		m_pStatisticsTask = NULL;
	}

	if (m_pRtspClient) {
		m_pRtspClient->teardownMediaSession(*m_pMediasession);
	}

	if (m_pVideoSink) {
		Medium::close(m_pVideoSink);
		m_pVideoSink = NULL;
	}

	if (m_pAudioSink) {
		Medium::close(m_pAudioSink);
		m_pAudioSink = NULL;
	}
	
	if (m_pMetaSink) {
		Medium::close(m_pMetaSink);
		m_pMetaSink = NULL;
	}

	if (m_pMediasession) {
		Medium::close(m_pMediasession);
		m_pMediasession = NULL;
	}

	if (m_pSdpDescription) {
		delete m_pSdpDescription;
		m_pSdpDescription = NULL;
	}

	if (m_pRtspClient) {
		Medium::close(m_pRtspClient);
		m_pRtspClient = NULL;
	}

	if (m_pEnv) {
		m_pEnv->reclaim();
		m_pEnv = NULL;
	}

	if (m_pScheduler) {
		delete m_pScheduler;
		m_pScheduler = NULL;
	}
}

BOOL CRtspClient::StartPlaying()
{
	MediaSubsessionIterator iter(*m_pMediasession);
	MediaSubsession *pSubsession;

	while ((pSubsession = iter.next()) != NULL) {
		if (strcmp("video", pSubsession->mediumName()) == 0) {
			m_pVideoSink = new CVideoSink(*m_pEnv, m_pVideoBuffer);
			if (!m_pVideoSink) {
				UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Cannot create video sink\n"));
				return FALSE;
			}

			if (!m_pVideoSink->startPlaying(*pSubsession->readSource(), 
				SubsessionAfterPlaying, 
				pSubsession)) {
				UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Video startPlaying failed\n"));
				return FALSE;
			}
		} else if (strcmp("audio", pSubsession->mediumName()) == 0) {
			m_pAudioSink = new CAudioSink(*m_pEnv, m_pAudioBuffer);
			if (!m_pAudioSink) {
				UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Cannot create audio sink\n"));
				return FALSE;
			}

			if (!m_pAudioSink->startPlaying(*pSubsession->readSource(), 
				SubsessionAfterPlaying, 
				pSubsession)) {
				UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Audio startPlaying failed\n"));
				return FALSE;
			}
		} else if (strcmp("metadata", pSubsession->mediumName()) == 0 || strcmp("event", pSubsession->mediumName()) == 0) {
			m_pMetaSink = new CMetaSink(*m_pEnv, m_pMetaBuffer);
			if (!m_pMetaSink) {
				UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Cannot create meta sink\n"));
				return FALSE;
			}

			if (!m_pMetaSink->startPlaying(*pSubsession->readSource(), 
				SubsessionAfterPlaying, 
				pSubsession)) {
				UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Meta startPlaying failed\n"));
				return FALSE;
			}
		}
	}

	if (!m_pRtspClient->playMediaSession(*m_pMediasession)) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("playMediaSession failed\n"));
		return FALSE;
	}

	m_bStopLoop = 0;
	m_bIsWait = 1;

	if (!m_userThread.StartThread(ThreadEventLoopStub, this, NULL, NULL)) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("StartThread failed\n"));
		return FALSE;
	}

	return TRUE;
}

VOID CRtspClient::StopPlaying()
{
	int rs = 0;

	if (m_bIsWait) {
		m_bStopLoop = 1;
		m_bIsWait = 0;

		m_userThread.StopThread(TRUE, 5000, TRUE);
	}
}

BOOL CRtspClient::IsAlive()
{
	/* [ADD] ldsung 2012.03.20 */
	int nTestTime = GetTickCount();

	nTestTime = nTestTime - m_nConnectTestTime;
	if(nTestTime > (int)((m_uAliveCheckCount * 1) * 1000))
		return FALSE;

	return m_uUpdateCount > m_uAliveCheckCount ? FALSE : TRUE;
}

BOOL CRtspClient::CreateClient()
{
	m_pRtspClient = RTSPClient::createNew(*m_pEnv, 0, NULL, m_uHttpPortNum);
	if (!m_pRtspClient) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("CRtspClient::createNew failed\n"));
		return FALSE;
	}

	return TRUE;
}

BOOL CRtspClient::Describe()
{
	m_pSdpDescription = m_pRtspClient->describeWithPassword(m_szUrl, 
		m_szUserName, 
		m_szPassword, 
		False);

	if (!m_pSdpDescription) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("describeWithPassword failed\n"));
		return FALSE;
	}

	return TRUE;
}

BOOL CRtspClient::SetupStream()
{
	m_pMediasession = MediaSession::createNew(*m_pEnv, m_pSdpDescription);
	if (!m_pMediasession) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("MediaSession::createNew failed\n"));
		return FALSE;
	}

	MediaSubsessionIterator iter(*m_pMediasession);
	MediaSubsession *pSubsession;

	while ((pSubsession = iter.next()) != NULL) {
		if (!pSubsession->initiate()) {
			UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Subsession initiate failed. medium name[%s]\n"), pSubsession->mediumName());
			continue;
			return FALSE;
		}

		if (pSubsession->clientPortNum() == 0) {
			continue; // port # was not set
		}

		UMFTrace(UMF_TRACE_LEVEL_VERBOSE, _T("medium name[%s], rtp port[%d], rtcp port[%d]\n"), 
			pSubsession->mediumName(), 
			pSubsession->clientPortNum(), 
			pSubsession->clientPortNum()+1);

		if (strcmp("video", pSubsession->mediumName()) == 0) {
			if (!pSubsession->codecName()) {
				continue;
			}

			strncpy(m_szVideoCodec, pSubsession->codecName(), sizeof(m_szVideoCodec));

			if (strcmp("H264", m_szVideoCodec) == 0) {
				if (pSubsession->fmtp_spropparametersets())
					strncpy(m_szH264SpropParameterSets, pSubsession->fmtp_spropparametersets(), sizeof(m_szH264SpropParameterSets));
			} else if (strcmp("MP4V-ES", m_szVideoCodec) == 0) {
				if (pSubsession->fmtp_config())
					strncpy(m_szMpeg4Config, pSubsession->fmtp_config(), sizeof(m_szMpeg4Config));
			}

			UMFTrace(UMF_TRACE_LEVEL_VERBOSE, _T("video: codec[%s], config[%s], sprop_parameter_sets[%s]\n"), 
				m_szVideoCodec, m_szMpeg4Config, m_szH264SpropParameterSets);
		} else if (strcmp("audio", pSubsession->mediumName()) == 0) {
			if (!pSubsession->codecName()) {
				continue;
			}

			strncpy(m_szAudioCodec, pSubsession->codecName(), sizeof(m_szAudioCodec));

			if (strcmp("L8", m_szAudioCodec) == 0) {
				m_uAudioSampleBits = 8;
			} else if (strcmp("L16", m_szAudioCodec) == 0) {
				m_uAudioSampleBits = 16;
			} else if (strcmp("PCMU", m_szAudioCodec) == 0) {
				m_uAudioSampleBits = 8;
			} else if (strcmp("PCMA", m_szAudioCodec) == 0) {
				m_uAudioSampleBits = 8;
			}

			m_uAudioSampleRate = pSubsession->rtpTimestampFrequency();

			UMFTrace(UMF_TRACE_LEVEL_VERBOSE, _T("audio: codec[%s] sample_bits[%d], sample_rate[%d]\n"), 
				m_szAudioCodec, m_uAudioSampleBits, m_uAudioSampleRate);
		}

		if (!m_pRtspClient->setupMediaSubsession(*pSubsession, False, m_bIsMulticast?False:m_bIsTcp, m_bIsMulticast)) {
			UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("setupMediaSubsession failed\n"));
			return FALSE;
		}
	}

	return TRUE;
}

VOID CRtspClient::SubsessionAfterPlaying(VOID* client_data) 
{
	// this function is not called.
}

VOID CRtspClient::StatisticsMeasurementStub(VOID* client_data)
{
	static_cast<CRtspClient*>(client_data)->StatisticsMeasurement();
}

VOID CRtspClient::StatisticsMeasurement()
{
	MediaSubsessionIterator iter(*m_pMediasession);
	MediaSubsession *pSubsession;
	RTPSource* pRtpSource;
	RTCPInstance* pRtcpInstance;
	UINT uCurSrTime;
	UINT uUpdated = 0;

	while ((pSubsession = iter.next()) != NULL) {
		if (strcmp("video", pSubsession->mediumName()) != 0 &&
			strcmp("audio", pSubsession->mediumName()) != 0) {
			continue; // process later.
		}

		pRtpSource = pSubsession->rtpSource();
		pRtcpInstance = pSubsession->rtcpInstance();
		RTPReceptionStatsDB::Iterator statsIter(pRtpSource->receptionStatsDB());
		// Assume that there's only one SSRC source (usually the case):
		RTPReceptionStats* stats = statsIter.next(TRUE);
		if (!stats) continue;
		struct timeval srTime = stats->lastReceivedSR_time();

		if (stats != NULL) {
			uCurSrTime = srTime.tv_sec*1000 + srTime.tv_usec/1000;

			if (strcmp("video", pSubsession->mediumName()) == 0) {
				if (m_uLastVideoSrTime != uCurSrTime) {
					uUpdated = 1;
				}
				m_uLastVideoSrTime = uCurSrTime;

				m_videoStats.uCurNumPacketReceived = 
					stats->totNumPacketsReceived() - m_videoStats.uTotalNumPacketReceived;
				m_videoStats.uCurNumPacketExpected = 
					stats->totNumPacketsExpected() - m_videoStats.uTotalNumPacketExpected;
				m_videoStats.uCurKbytesReceived = 
					(UINT)(stats->totNumKBytesReceived() - m_videoStats.uTotalKbytesReceived);

				m_videoStats.uTotalNumPacketReceived = stats->totNumPacketsReceived();
				m_videoStats.uTotalNumPacketExpected = stats->totNumPacketsExpected();
				m_videoStats.uTotalKbytesReceived = (UINT)stats->totNumKBytesReceived();
			} else if (strcmp("audio", pSubsession->mediumName()) == 0) {
				if (m_uLastAudioSrTime != uCurSrTime) {
					uUpdated = 1;
				}
				m_uLastAudioSrTime = uCurSrTime;

				m_audioStats.uCurNumPacketReceived = 
					stats->totNumPacketsReceived() - m_audioStats.uTotalNumPacketReceived;
				m_audioStats.uCurNumPacketExpected = 
					stats->totNumPacketsExpected() - m_audioStats.uTotalNumPacketExpected;
				m_audioStats.uCurKbytesReceived = 
					(UINT)(stats->totNumKBytesReceived() - m_audioStats.uTotalKbytesReceived);

				m_audioStats.uTotalNumPacketReceived = stats->totNumPacketsReceived();
				m_audioStats.uTotalNumPacketExpected = stats->totNumPacketsExpected();
				m_audioStats.uTotalKbytesReceived = (UINT)stats->totNumKBytesReceived();
			} else if (strcmp("metadata", pSubsession->mediumName()) == 0) {
				if (m_uLastMetaSrTime != uCurSrTime) {
					uUpdated = 1;
				}
				m_uLastMetaSrTime = uCurSrTime;

				m_metaStats.uCurNumPacketReceived = 
					stats->totNumPacketsReceived() - m_metaStats.uTotalNumPacketReceived;
				m_metaStats.uCurNumPacketExpected = 
					stats->totNumPacketsExpected() - m_metaStats.uTotalNumPacketExpected;
				m_metaStats.uCurKbytesReceived = 
					(UINT)(stats->totNumKBytesReceived() - m_metaStats.uTotalKbytesReceived);

				m_metaStats.uTotalNumPacketReceived = stats->totNumPacketsReceived();
				m_metaStats.uTotalNumPacketExpected = stats->totNumPacketsExpected();
				m_metaStats.uTotalKbytesReceived = (UINT)stats->totNumKBytesReceived();
			}
		}
	}

	if (uUpdated) {
		/* [ADD] ldsung 2012.03.20 */
		m_nConnectTestTime = GetTickCount();
		m_uUpdateCount = 0;
	} else {
		m_uUpdateCount++;
	}

	m_pEnv->taskScheduler().rescheduleDelayedTask(
		m_pStatisticsTask, 
		m_uInterval*1000,
		StatisticsMeasurementStub, 
		this);
}

UINT WINAPI CRtspClient::ThreadEventLoopStub(VOID *arg)
{
	struct THREAD_PARAM_SET *pParam = (struct THREAD_PARAM_SET*)arg;

	return static_cast<CRtspClient*>(pParam->pParam2)->ThreadEventLoop();
}

UINT WINAPI CRtspClient::ThreadEventLoop()
{
	UMFTrace(UMF_TRACE_LEVEL_VERBOSE, _T("EventLoop started\n"));
	m_pEnv->taskScheduler().doEventLoop((char*)&m_bStopLoop);
	UMFTrace(UMF_TRACE_LEVEL_VERBOSE, _T("EventLoop stopped\n"));

	return 0;
}
