#include "Stdafx.h"
#include "RtspReceiver.h"
#include <Base64.hh>

UINT ParseH264Sps(BYTE *pSps, UINT uDataLen, UINT *puWidth, UINT *puHeight);
UINT ParseMpeg4Vol(BYTE *pData, UINT uSkipBits, UINT *puWidth, UINT *puHeight);

int htoi(char *s)
{
	char *t=s; 
	int x=0, y=1; 
	if(*s=='0' && (s[1]=='x' || s[1]=='X')) 
		t+=2; 
	s+=strlen(s); 
	while(t<=--s) { 
		if('0' <= *s && *s <= '9') 
			x+=y*(*s - '0'); 
		else if('a' <= *s && *s <= 'f') 
			x+=y*(*s - 'a' + 10); 
		else if('A' <= *s && *s <= 'F') 
			x+=y*(10 + *s - 'A'); 
		else 
			return -1; /* invalid input! */ 
		y<<=4; 
	}

	return x;
}

CRtspReceiver::CRtspReceiver()
	: m_pVideoBuffer(NULL), m_pAudioBuffer(NULL), m_pMetaBuffer(NULL), 
	m_uVideoIndex(0), m_uAudioIndex(0), m_bKeyFrameArrived(0), m_bAlive(FALSE), m_bIsParseJpeg(0),
	m_hVideoEvent(NULL), m_hAudioEvent(NULL), m_hMetaEvent(NULL), m_uWidth(0), m_uHeight(0)
{
	memset(m_szUrl, 0, sizeof(m_szUrl));
	memset(m_szSession, 0, sizeof(m_szSession));
	memset(m_szUsername, 0, sizeof(m_szUsername));
	memset(m_szPassword, 0, sizeof(m_szPassword));
	memset(&m_videoBuf, 0, sizeof(m_videoBuf));
	memset(&m_audioBuf, 0, sizeof(m_audioBuf));
	memset(&m_metaBuf, 0, sizeof(m_metaBuf));
}

CRtspReceiver::~CRtspReceiver()
{
}

BOOL CRtspReceiver::Init()
{
	m_pVideoBuffer = new BYTE[RTSPRCV_VIDEO_BUFFER_SIZE];
	if (!m_pVideoBuffer) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Cannot create video buffer\n"));
		return FALSE;
	}

	m_pAudioBuffer = new BYTE[RTSPRCV_AUDIO_BUFFER_SIZE];
	if (!m_pAudioBuffer) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Cannot create audio buffer\n"));
		return FALSE;
	}

	m_pMetaBuffer = new BYTE[RTSPRCV_META_BUFFER_SIZE];
	if (!m_pMetaBuffer) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Cannot create meta buffer\n"));
		return FALSE;
	}

	m_hVideoEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	m_hAudioEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	m_hMetaEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	
	return TRUE;
}

void CRtspReceiver::Release()
{
	if (m_hVideoEvent) {
		CloseHandle(m_hVideoEvent);
		m_hVideoEvent = NULL;
	}

	if (m_hAudioEvent) {
		CloseHandle(m_hAudioEvent);
		m_hAudioEvent = NULL;
	}

	if (m_hMetaEvent) {
		CloseHandle(m_hMetaEvent);
		m_hMetaEvent = NULL;
	}

	if (m_pVideoBuffer) {
		delete[] m_pVideoBuffer;
		m_pVideoBuffer = NULL;
	}

	if (m_pAudioBuffer) {
		delete[] m_pAudioBuffer;
		m_pAudioBuffer = NULL;
	}

	if (m_pMetaBuffer) {
		delete[] m_pMetaBuffer;
		m_pMetaBuffer = NULL;
	}
}

BOOL CRtspReceiver::InitSession(CHAR *pszUrl, CHAR *pszSession, CHAR *pszUsername, CHAR *pszPassword)
{
	BOOL bIsTcp = FALSE;
	BOOL bIsMulticast = FALSE;
	m_bAlive = FALSE;
	m_bKeyFrameArrived = FALSE;
	m_bIsParseJpeg = FALSE;
	m_uWidth = 0;
	m_uHeight = 0;

	strncpy(m_szUrl, pszUrl, sizeof(m_szUrl)-1);
	strncpy(m_szSession, pszSession, sizeof(m_szSession)-1);
	strncpy(m_szUsername, pszUsername, sizeof(m_szUsername)-1);
	strncpy(m_szPassword, pszPassword, sizeof(m_szPassword)-1);

	if (strcmp("tcp", m_szSession) == 0) {
		bIsTcp = TRUE;
	} else if (strcmp("multicast", m_szSession) == 0) {
		bIsMulticast = TRUE;
	}

	m_videoBuf.pBuffer = m_pVideoBuffer;
	m_videoBuf.uBufferSize = RTSPRCV_VIDEO_BUFFER_SIZE;
	m_videoBuf.MediaCallbackFunc = ProcessVideoDataWrapper;
	m_videoBuf.pInstance = this;

	m_audioBuf.pBuffer = m_pAudioBuffer;
	m_audioBuf.uBufferSize = RTSPRCV_AUDIO_BUFFER_SIZE;
	m_audioBuf.MediaCallbackFunc = ProcessAudioDataWrapper;
	m_audioBuf.pInstance = this;

	m_metaBuf.pBuffer = m_pMetaBuffer;
	m_metaBuf.uBufferSize = RTSPRCV_META_BUFFER_SIZE;
	m_metaBuf.MediaCallbackFunc = ProcessMetaDataWrapper;
	m_metaBuf.pInstance = this;

	if (!m_rtspSession.Init(m_szUrl, 
			m_szUsername, 
			m_szPassword, 
			bIsTcp, 
			bIsMulticast, 
			&m_videoBuf, 
			&m_audioBuf,
			&m_metaBuf)) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("RtspClient init failed\n"));
		return FALSE;
	}

	if (!ParseVideoParams()) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("RtspClient video param parsing failed\n"));
		return FALSE;
	}

	return TRUE;
}

VOID CRtspReceiver::ReleaseSession()
{
	m_rtspSession.Release();
}

BOOL CRtspReceiver::StartSession()
{
	if (!m_rtspSession.StartPlaying()) {
		return FALSE;
	}

	m_bAlive = TRUE;

	return TRUE;
}

VOID CRtspReceiver::StopSession()
{
	m_rtspSession.StopPlaying();
}

BOOL CRtspReceiver::IsAlive()
{
	return m_bAlive ? m_rtspSession.IsAlive():m_bAlive;
}

BOOL CRtspReceiver::ProcessVideoDataWrapper(struct BUFFER_INFO *pInfo)
{
	return static_cast<CRtspReceiver*>(pInfo->pInstance)->ProcessVideoData(pInfo);
}

BOOL CRtspReceiver::ProcessVideoData(struct BUFFER_INFO *pInfo)
{
	//UMFTrace(UMF_TRACE_LEVEL_VERBOSE, _T("Video: size[%d], offset[%d]\n"), pInfo->uFrameSize, pInfo->uOffset);

	if (strcmp("H264", m_rtspSession.GetVideoCodec()) == 0) {
		BYTE *pBuf = pInfo->pBuffer+pInfo->uOffset;

		if (pBuf[0] != 0x00 || pBuf[1] != 0x00 || pBuf[2] != 0x00 || pBuf[3] != 0x01)
		{
			CHAR pStartBytes[4] = { 0x00, 0x00, 0x00, 0x01 };

			if ((pBuf[0]&0x1f) == 7 || (pBuf[0]&0x1f) == 8) return 0; // Get SPS/PPS from SDP description

			if ((pBuf[0]&0x1f) == 5)
			{
				UINT uOffset = 0;
				
				pInfo->uOffset -= (m_uVideoParamSize[0]+m_uVideoParamSize[1]+12);
				pInfo->uFrameSize += (m_uVideoParamSize[0]+m_uVideoParamSize[1]+12);
				
				pBuf = pInfo->pBuffer+pInfo->uOffset;
				memcpy(pBuf, pStartBytes, sizeof(pStartBytes));
				uOffset += sizeof(pStartBytes);
				memcpy(pBuf+uOffset, m_pVideoParam[0], m_uVideoParamSize[0]);
				uOffset += m_uVideoParamSize[0];
				memcpy(pBuf+uOffset, pStartBytes, sizeof(pStartBytes));
				uOffset += sizeof(pStartBytes);
				memcpy(pBuf+uOffset, m_pVideoParam[1], m_uVideoParamSize[1]);
				uOffset += m_uVideoParamSize[1];
				memcpy(pBuf+uOffset, pStartBytes, sizeof(pStartBytes));
			}
			else
			{
				pInfo->uOffset -= sizeof(pStartBytes);
				pInfo->uFrameSize += sizeof(pStartBytes);

				pBuf = pInfo->pBuffer+pInfo->uOffset;
				memcpy(pBuf, pStartBytes, sizeof(pStartBytes));
			}
		}
	} else if (strcmp("JPEG", m_rtspSession.GetVideoCodec()) == 0) {
		if (0 == m_uWidth && 0 == m_uHeight) {
			if (!GetJpegResolution(pInfo->pBuffer+pInfo->uOffset, pInfo->uFrameSize, &m_uWidth, &m_uHeight)) {
				UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Getting jpeg resolution failed\n"));
				return FALSE;
			}
		}
	}

	if (!IsKeyFrameArrived(pInfo->uOffset)) {
		UMFTrace(UMF_TRACE_LEVEL_VERBOSE, _T("Key frame does not arrived\n"));
		return FALSE;
	}

	if (!m_videoQue.Push(pInfo)) {
		UMFTrace(UMF_TRACE_LEVEL_VERBOSE, _T("Push failed (video)\n"));
		m_videoQue.RemoveAll();
		SetKeyFrame(FALSE);
		return FALSE;
	}

	SetEvent(m_hVideoEvent);

	return TRUE;
}

BOOL CRtspReceiver::ProcessAudioDataWrapper(struct BUFFER_INFO *pInfo)
{
	return static_cast<CRtspReceiver*>(pInfo->pInstance)->ProcessAudioData(pInfo);
}

BOOL CRtspReceiver::ProcessAudioData(struct BUFFER_INFO *pInfo)
{
	//UMFTrace(UMF_TRACE_LEVEL_VERBOSE, _T("Audio: size[%d], offset[%d]\n"), pInfo->uFrameSize, pInfo->uOffset);

	if (!m_audioQue.Push(pInfo)) {
		UMFTrace(UMF_TRACE_LEVEL_VERBOSE, _T("Push failed (audio)\n"));
		m_audioQue.RemoveAll();
		return FALSE;
	}

	SetEvent(m_hAudioEvent);

	return TRUE;
}

BOOL CRtspReceiver::ProcessMetaDataWrapper(struct BUFFER_INFO *pInfo)
{
	return static_cast<CRtspReceiver*>(pInfo->pInstance)->ProcessMetaData(pInfo);
}

BOOL CRtspReceiver::ProcessMetaData(struct BUFFER_INFO* pInfo)
{
	//UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Meta : size[%d], offset[%d]\n"), pInfo->uFrameSize, pInfo->uOffset);

	if (!m_metaQue.Push(pInfo)) {
		UMFTrace(UMF_TRACE_LEVEL_VERBOSE, _T("Push failed (meta)\n"));
		m_metaQue.RemoveAll();
		return FALSE;
	}

	SetEvent(m_hMetaEvent);

	return TRUE;
}

BOOL CRtspReceiver::IsKeyFrameArrived(UINT uOffset)
{
	BYTE *pBuff = m_pVideoBuffer + uOffset;

	if (!m_rtspSession.GetVideoCodec()) {
		return FALSE;
	}
	
	if (strcmp("H264", m_rtspSession.GetVideoCodec()) == 0) {
		if ((pBuff[4]&0x1F) == 5 || (pBuff[4]&0x1F) == 7) { // IDR/SPS
			m_bKeyFrameArrived = 1;
		}
	} else if (strcmp("MP4V-ES", m_rtspSession.GetVideoCodec()) == 0) {
		if (pBuff[0] == 0x00 && pBuff[1] == 0x00 && pBuff[2] == 0x01 && pBuff[3] == 0xb0) {
			m_bKeyFrameArrived = 1;
		}
	} else if (strcmp("JPEG", m_rtspSession.GetVideoCodec()) == 0) {
		m_bKeyFrameArrived = 1;
	}

	return m_bKeyFrameArrived;
}

BOOL CRtspReceiver::GetVideoInfo(CHAR *pszCodec, UINT *puWidth, UINT *puHeight)
{
	if (pszCodec) {
		if (strcmp("H264", m_rtspSession.GetVideoCodec()) == 0) {
			strcpy(pszCodec, "h264");
		} else if (strcmp("MP4V-ES", m_rtspSession.GetVideoCodec()) == 0) {
			strcpy(pszCodec, "mpeg4");
		} else if (strcmp("JPEG", m_rtspSession.GetVideoCodec()) == 0) {
			strcpy(pszCodec, "jpeg");
		} else {
			return FALSE;
		}
	}

	if (puWidth) *puWidth = m_uWidth;
	if (puHeight) *puHeight = m_uHeight;

	return TRUE;
}

BOOL CRtspReceiver::GetAudioInfo(CHAR *pszCodec, UINT *puSampleBits, UINT *puSampleRate)
{
	if (pszCodec) {
		if (strcmp("L8", m_rtspSession.GetAudioCodec()) == 0 ||
				strcmp("L16", m_rtspSession.GetAudioCodec()) == 0) {
			strcpy(pszCodec, "pcm");
		} else if (strcmp("PCMU", m_rtspSession.GetAudioCodec()) == 0) {
			strcpy(pszCodec, "ulaw");
		} else if (strcmp("PCMA", m_rtspSession.GetAudioCodec()) == 0) {
			strcpy(pszCodec, "alaw");
		} else {
			return FALSE;
		}
	}

	if (puSampleBits) *puSampleBits = m_rtspSession.GetAudioSampleBits();
	if (puSampleRate) *puSampleRate = m_rtspSession.GetAudioSampleRate();

	return TRUE;
}

UINT CRtspReceiver::GetVideoIndex()
{
	UINT uIndex = m_uVideoIndex;

	if (++m_uVideoIndex >= RTSPRCV_VIDEO_BUFFER_NUM) m_uVideoIndex = 0;

	return uIndex;
}

UINT CRtspReceiver::GetAudioIndex()
{
	UINT uIndex = m_uAudioIndex;

	if (++m_uAudioIndex >= RTSPRCV_AUDIO_BUFFER_NUM) m_uAudioIndex = 0;

	return uIndex;
}

BOOL CRtspReceiver::GetJpegResolution(BYTE *pImg, UINT uSize, UINT *puWidth, UINT *puHeight)
{
	//Check for valid JPEG image
	UINT i = 0;	// Keeps track of the position within the file
	if(pImg[i] == 0xFF && pImg[i+1] == 0xD8 && pImg[i+2] == 0xFF && pImg[i+3] == 0xE0) {
		i += 4;
		// Check for valid JPEG header (null terminated JFIF)
		if(pImg[i+2] == 'J' && pImg[i+3] == 'F' && pImg[i+4] == 'I' && pImg[i+5] == 'F' && pImg[i+6] == 0x00) {
			//Retrieve the block length of the first block since the first block will not contain the size of file
			USHORT uBlockLength = pImg[i] * 256 + pImg[i+1];
			while(i<uSize)  {
				i += uBlockLength;	//Increase the file index to get to the next block
				if(i >= uSize) return FALSE;	//Check to protect against segmentation faults
				if(pImg[i] != 0xFF) return FALSE;	//Check that we are truly at the start of another block
				if(pImg[i+1] == 0xC0) { 	//0xFFC0 is the "Start of frame" marker which contains the file size
					//The structure of the 0xFFC0 block is quite simple [0xFFC0][ushort length][uchar precision][ushort x][ushort y]
					*puHeight = pImg[i+5]*256 + pImg[i+6];
					*puWidth = pImg[i+7]*256 + pImg[i+8];
					return TRUE;
				} else {
					i += 2;		//Skip the block marker
					uBlockLength = pImg[i] * 256 + pImg[i+1];	//Go to the next block
				}
			}
			return FALSE;	//If this point is reached then no size was found
		}
		else return FALSE;	//Not a valid JFIF string
	}
	else return FALSE;	//Not a valid SOI header
}

BOOL CRtspReceiver::ParseVideoParams()
{
	UINT i;
	CHAR szParams[512];

	if (strcmp("H264", m_rtspSession.GetVideoCodec()) == 0) {
		CHAR *pszToken;
		BYTE *pDecoded;
		UINT uSize;

		strcpy(szParams, m_rtspSession.GetH264SpropParameterSets());

		pszToken = strtok(szParams, ",");
		if (!pszToken) return FALSE;
		pDecoded = base64Decode(pszToken, uSize);
		ParseH264Sps(&pDecoded[1], uSize-1, &m_uWidth, &m_uHeight);
		memcpy(m_pVideoParam[0], pDecoded, uSize);
		m_uVideoParamSize[0] = uSize;
		delete[] pDecoded;

		pszToken = strtok(NULL, ",");
		if (!pszToken) return -1;
		pDecoded = base64Decode(pszToken, uSize);
		memcpy(m_pVideoParam[1], pDecoded, uSize);
		m_uVideoParamSize[1] = uSize;
		delete[] pDecoded;
	} else if (strcmp("MP4V-ES", m_rtspSession.GetVideoCodec()) == 0) {
		strcpy(szParams, m_rtspSession.GetMpeg4Config());

		for (i=0; i<strlen(szParams)/2; i++) {
			CHAR szStr[3] = {0,};
			strncpy(szStr, &szParams[i*2], 2);
			m_pVideoParam[0][i] = (unsigned char)htoi(szStr);
		}
		ParseMpeg4Vol(m_pVideoParam[0], 0, &m_uWidth, &m_uHeight);
	}

	return TRUE;
}

BOOL CRtspReceiver::GetVideoData(BYTE **ppData, UINT *puFrameSize, struct timeval *pTimeStamp)
{
	struct BUFFER_INFO info;

	if (!m_videoQue.Pop(&info)) {
		return FALSE;
	}

	*ppData = info.pBuffer + info.uOffset;
	*puFrameSize = info.uFrameSize;
	*pTimeStamp = info.timeStamp;

	return TRUE;
}

BOOL CRtspReceiver::GetAudioData(BYTE **ppData, UINT *puFrameSize, struct timeval *pTimeStamp)
{
	struct BUFFER_INFO info;

	if (!m_audioQue.Pop(&info)) {
		return FALSE;
	}

	*ppData = info.pBuffer + info.uOffset;
	*puFrameSize = info.uFrameSize;
	*pTimeStamp = info.timeStamp;

	return TRUE;
}

BOOL CRtspReceiver::GetMetaData(BYTE **ppData, UINT *puFrameSize, struct timeval *pTimeStamp)
{
	struct BUFFER_INFO info;
		
	if (!m_metaQue.Pop(&info)) {
		return FALSE;
	}

	*ppData = info.pBuffer + info.uOffset;
	*puFrameSize = info.uFrameSize;
	*pTimeStamp = info.timeStamp;

	return TRUE;
}
