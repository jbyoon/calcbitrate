// RtspClientAPI.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "RTSPClientAPI.h"

extern "C" IRTSPClientAPI* WINAPI CreateRTSPClientAPI()
{
	return (IRTSPClientAPI*) new CRTSPClientAPI();
}

CRTSPClientAPI::CRTSPClientAPI()
{
}

BOOL CRTSPClientAPI::Init()
{
	return m_rtspRcv.Init();
}

VOID CRTSPClientAPI::Release()
{
	m_rtspRcv.Release();
}

BOOL CRTSPClientAPI::Connect(CHAR *pszUrl, CHAR *pszSession, CHAR *pszUsername, CHAR *pszPassword)
{
	if (!m_rtspRcv.InitSession(pszUrl, pszSession, pszUsername, pszPassword)) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("InitSession failed\n"));
		return FALSE;
	}

	if (!m_rtspRcv.StartSession()) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("StartSession failed\n"));
		return FALSE;
	}

	return TRUE;
}

VOID CRTSPClientAPI::Disconnect()
{
	m_rtspRcv.StopSession();
	m_rtspRcv.ReleaseSession();
}

HANDLE CRTSPClientAPI::GetEvent(UINT uType)
{
	switch (uType) {
	case RTSP_MT_VIDEO:
		return m_rtspRcv.GetVideoEvent();
	case RTSP_MT_AUDIO:
		return m_rtspRcv.GetAudioEvent();
	case RTSP_MT_META:
		return m_rtspRcv.GetMetaEvent();
	}

	return NULL;
}

BOOL CRTSPClientAPI::GetMediaData(UINT uType, BYTE **ppData, UINT *puFrameSize, struct timeval *pTimeStamp)
{
	switch (uType) {
	case RTSP_MT_VIDEO:
		if (!m_rtspRcv.GetVideoData(ppData, puFrameSize, pTimeStamp)) {
			UMFTrace(UMF_TRACE_LEVEL_VERBOSE, _T("GetVideoData failed\n"));
			return FALSE;
		}
		break;
	case RTSP_MT_AUDIO:
		if (!m_rtspRcv.GetAudioData(ppData, puFrameSize, pTimeStamp)) {
			UMFTrace(UMF_TRACE_LEVEL_VERBOSE, _T("GetAudioData failed\n"));
			return FALSE;
		}
		break;
	case RTSP_MT_META:
		if (!m_rtspRcv.GetMetaData(ppData, puFrameSize, pTimeStamp)) {
			UMFTrace(UMF_TRACE_LEVEL_VERBOSE, _T("GetMetaData failed\n"));
			return FALSE;
		}
		break;
	default:
		return FALSE;
	}

	return TRUE;
}

BOOL CRTSPClientAPI::GetParam(CHAR *pszCmd, VOID *pParam, UINT uLen)
{
	if (strcmp("Alive", pszCmd) == 0) {
		BOOL bAlive = m_rtspRcv.IsAlive();
		if (sizeof(bAlive) > uLen) return FALSE;

		CopyMemory(pParam, &bAlive, sizeof(bAlive));
	} else if (strcmp("VideoCodec", pszCmd) == 0) {
		CHAR szCodec[8];
		if (sizeof(szCodec) > uLen) return FALSE;
		
		if (!m_rtspRcv.GetVideoInfo(szCodec, NULL, NULL)) {
			return FALSE;
		}
		CopyMemory(pParam, szCodec, sizeof(szCodec));
	} else if (strcmp("Width", pszCmd) == 0) {
		UINT uWidth;
		if (sizeof(uWidth) > uLen) return FALSE;
		
		if (!m_rtspRcv.GetVideoInfo(NULL, &uWidth, NULL)) {
			return FALSE;
		}
		CopyMemory(pParam, &uWidth, sizeof(uWidth));
	} else if (strcmp("Height", pszCmd) == 0) {
		UINT uHeight;
		if (sizeof(uHeight) > uLen) return FALSE;
		
		if (!m_rtspRcv.GetVideoInfo(NULL, NULL, &uHeight)) {
			return FALSE;
		}
		CopyMemory(pParam, &uHeight, sizeof(uHeight));
	} else if (strcmp("AudioCodec", pszCmd) == 0) {
		CHAR szCodec[8];
		if (sizeof(szCodec) > uLen) return FALSE;
		
		if (!m_rtspRcv.GetAudioInfo(szCodec, NULL, NULL)) {
			return FALSE;
		}
		CopyMemory(pParam, szCodec, sizeof(szCodec));
	} else if (strcmp("SampleRate", pszCmd) == 0) {
		UINT uSampleRate;
		if (sizeof(uSampleRate) > uLen) return FALSE;
		
		if (!m_rtspRcv.GetAudioInfo(NULL, NULL, &uSampleRate)) {
			return FALSE;
		}
		CopyMemory(pParam, &uSampleRate, sizeof(uSampleRate));
	} else if (strcmp("SampleBits", pszCmd) == 0) {
		UINT uSampleBits;
		if (sizeof(uSampleBits) > uLen) return FALSE;
		
		if (!m_rtspRcv.GetAudioInfo(NULL, &uSampleBits, NULL)) {
			return FALSE;
		}
		CopyMemory(pParam, &uSampleBits, sizeof(uSampleBits));
	}

	return TRUE;
}

BOOL CRTSPClientAPI::SetParam(CHAR *pszCmd, VOID *pParam)
{
	if (strcmp("AliveCheckTime", pszCmd) == 0) {
		m_rtspRcv.SetAliveCheckTime(*(UINT*)pParam);
	}

	return TRUE;
}
