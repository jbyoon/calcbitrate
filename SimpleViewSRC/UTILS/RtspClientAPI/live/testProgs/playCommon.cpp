/**********
This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the
Free Software Foundation; either version 2.1 of the License, or (at your
option) any later version. (See <http://www.gnu.org/copyleft/lesser.html>.)

This library is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
more details.

You should have received a copy of the GNU Lesser General Public License
along with this library; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**********/
// Copyright (c) 1996-2007, Live Networks, Inc.  All rights reserved
// A common framework, used for the "openRTSP" and "playSIP" applications
// Implementation

/*
	2007-10-26	jinohan

*/

#include "playCommon.hh"

#include <BasicUsageEnvironment.hh>
#include <GroupsockHelper.hh>

#if defined(__WIN32__) || defined(_WIN32)
#pragma message("################################################### ")
#pragma message(" DEFINE!! defined(__WIN32__) || defined(_WIN32) ")
#pragma message("################################################### ")
#else
#pragma message("################################################### ")
#pragma message(" NOT DEFINE!! defined(__WIN32__) || defined(_WIN32) ")
#pragma message("################################################### ")
#endif

#if defined(__WIN32__) || defined(_WIN32)
#define		snprintf	_snprintf
#else
#include <signal.h>
#define		USE_SIGNALS		1
#endif

////////////////////////////////////////////////////////////////////
// DECLARE EXPORT FUNCTION PROTOTYPE 


////////////////////////////////////////////////////////////////////
// DECLARE LOCAL FUNCTION PROTOTYPE 

int		_parsingusage( int _argc, char** _argv );
void	_usage();

void	_setupStreams();
void	_startPlayingStreams();

void	_tearDownStreams();
void	_closeMediaSinks();
void	_subsessionAfterPlaying( void* _pclientdata );
void	_subsessionByeHandler(void* clientData);
void	_sessionAfterPlaying(void* clientData = NULL);
void	_sessionTimerHandler(void* clientData);
void	_shutdown(int exitCode = 1);
void	_signalHandlerShutdown(int sig);
void	_checkForPacketArrival(void* clientData);
void	_checkInterPacketGaps(void* clientData);
void	_beginQOSMeasurement();
void	_printQOSData( int exitCode );

////////////////////////////////////////////////////////////////////
// DEFINE GLOBAL VARIABLE

extern	Boolean		gfIsAllowProxyServers;			// in openRTSP.cpp
extern  Boolean		gfIsControlConnectionUsesTCP;	// in openRTSP.cpp
extern  Boolean		gfIsSupportCodecSelection;		// in openRTSP.cpp
extern  char const* gpszClientProtocolName;			// in openRTSP.cpp

char const*			gpszProgName;
UsageEnvironment*	gpcEnv;
Medium*				gpcOurClient = NULL;
MediaSession*		gpcSession = NULL;

TaskToken			gtaskSessionTimer = NULL;
TaskToken			gtaskArrivalCheckTimer = NULL;
TaskToken			gtaskInterPacketGapCheckTimer = NULL;
TaskToken			gtaskQOSMeasurementTimer = NULL;

Boolean				gfIsCreateReceivers = True;

Boolean				gfIsOutputQuickTimeFile = False;
Boolean				gfIsGenerateMP4Format = False;
QuickTimeFileSink*	gpcQTOut = NULL;

Boolean				gfIsOutputAVIFile = False;
AVIFileSink*		gpcAVIOut = NULL;

Boolean				gfIsAudioOnly = False;
Boolean				gfIsVideoOnly = False;

char const*			gpszSingleMediumName = NULL;

int					gnVerbosityLevel = 0;

double				gdblEndTime		= 0;
double				gdblEndTimeSlop = -1.0;		// extra seconds to play at the end

unsigned 			gnInterPacketGapMaxTime = 0;
unsigned			gnTotNumPacketsReceived = ~0; // used if checking inter-packet gaps

Boolean				gfIsPlayContinuously = False;

int					gnSimpleRTPoffsetArg = -1;
Boolean				gfIsSendOptionsRequest = True;
Boolean				gfIsSendOptionsRequestOnly = False;
Boolean				gfIsOneFilePerFrame = False;
Boolean				gfIsNotifyOnPacketArrival = False;

Boolean				gfIsStreamUsingTCP = False;
portNumBits			gnTunnelOverHTTPPortNum = 0;		// export to openRTSP.cpp

char*				gpszUsername = NULL;
char*				gpszPassword = NULL;

char*				gpszProxyServerName = NULL;
unsigned short		gnProxyServerPortNum = 0;

unsigned char		gcbDesiredAudioRTPPayloadFormat = 0;
char*				gpszMIMESubtype = NULL;

unsigned short		gnMovieWidth = 240; // default
Boolean				gfIsMovieWidthOptionSet = False;
unsigned short		gnMovieHeight = 180; // default
Boolean				gfIsMovieHeightOptionSet = False;
unsigned			gnMovieFPS = 15; // default
Boolean				gfIsMovieFPSOptionSet = False;

char*				gpszFileNamePrefix = "";

unsigned			gnFileSinkBufferSize = 20000;
unsigned			gnSocketInputBufferSize = 0;

Boolean				gfIsPacketLossCompensate = False;
Boolean				gfIsSyncStreams = False;
Boolean				gfIsGenerateHintTracks = False;
unsigned			gnQOSMeasurementIntervalMS = 0;		// 0(milisec) means: Don't output QOS data
unsigned			gnStatusCode = 0;					// export to openRTSP.cpp

struct timeval		gtStartTime;

//
char				gszClientIPAddress[255] = {0,};
unsigned short		gnClientDesiredPortNum = 0;
char				gszServerURL[255] = {0,};

////////////////////////////////////////////////////////////////////
// IMPLEMENT EXPORT FUNCTION



////////////////////////////////////////////////////////////////////
// IMPLEMENT LOCAL FUNCTION

int main(int _argc, char** _argv)
{
	Boolean		fret;

	// Begin by setting up our usage environment:
	TaskScheduler* pcScheduler = BasicTaskScheduler::createNew();
	gpcEnv = BasicUsageEnvironment::createNew( *pcScheduler );

	gettimeofday( &gtStartTime, NULL );

#ifdef USE_SIGNALS
	// Allow ourselves to be shut down gracefully by a SIGHUP or a SIGUSR1:
	signal(SIGHUP,  _signalHandlerShutdown);
	signal(SIGUSR1, _signalHandlerShutdown);
#endif

#if 1
	////
	gpszProgName = "rtspclient";
	strcpy( gszServerURL, "rtsp://192.168.18.117:554/firstvideostream" );

#else
	gpszProgName = _argv[0];

	///////////////////////////////////////////////////////////////////
	// parsing global define variable
	int	nret;
	nret = _parsingusage( _argc, _argv );
#endif

	/*
	IP Adapter(IP Address)가 여러 개 있는 경우 그 중에 하나를 선택한다.
	*/
	// extern netAddressBits ReceivingInterfaceAddr; in GroupsockHelper.hh
	NetAddressList	cNetAddressList( gszClientIPAddress );
	ReceivingInterfaceAddr = *(unsigned*)( cNetAddressList.firstAddress()->data() );


	///////////////////////////////////////////////////////////////////
	// Create our client object:	in openRTSP.cpp
	gpcOurClient = createClient( *gpcEnv, gnVerbosityLevel, gpszProgName );
	if( NULL == gpcOurClient ) {
		*gpcEnv << "Failed to create " << gpszClientProtocolName
				<< " client: " << gpcEnv->getResultMsg() << "\n";
		_shutdown();
	}


	///////////////////////////////////////////////////////////////////
	// Begin by sending an "OPTIONS" command:	in openRTSP.cpp
	if( gfIsSendOptionsRequest ) {
		char* optionsResponse
			= getOptionsResponse( gpcOurClient, gszServerURL, gpszUsername, gpszPassword );

		if( NULL == optionsResponse ) {
			*gpcEnv << gpszClientProtocolName << " \"OPTIONS\" request failed: "
					<< gpcEnv->getResultMsg() << "\n";
		}
		else {
			*gpcEnv << gpszClientProtocolName << " \"OPTIONS\" request returned: "
					<< optionsResponse << "\n";
		}

		delete[] optionsResponse;

		if( gfIsSendOptionsRequestOnly ) {
			_shutdown();
		}
	}

	
	///////////////////////////////////////////////////////////////////
	// Open the URL, to get a SDP description:	in openRTSP.cpp
	char* sdpDescription
		= getSDPDescriptionFromURL( gpcOurClient, 
									gszServerURL, 
									gpszUsername, gpszPassword,
									gpszProxyServerName, 
									gnProxyServerPortNum,
								    gnClientDesiredPortNum );
	if( NULL == sdpDescription ) {
		*gpcEnv << "Failed to get a SDP description from URL \"" << gszServerURL
				<< "\": " << gpcEnv->getResultMsg() << "\n";
		_shutdown();
	}

	*gpcEnv << "Opened URL \"" << gszServerURL
			<< "\", returning a SDP description:\n" << sdpDescription << "\n";


	///////////////////////////////////////////////////////////////////////////////
	// Create a media session object from this SDP description:
	gpcSession = MediaSession::createNew( *gpcEnv, sdpDescription );
	delete[] sdpDescription;

	if( gpcSession == NULL ) {
		*gpcEnv << "Failed to create a MediaSession object from the SDP description: " << gpcEnv->getResultMsg() << "\n";
		_shutdown();
	}
	else
	if( !gpcSession->hasSubsessions() ) {
		*gpcEnv << "This session has no media subsessions (i.e., \"m=\" lines)\n";
		_shutdown();
	}

	///////////////////////////////////////////////////////////////////////////////
	// Then, setup the "RTPSource"s for the session:
	MediaSubsessionIterator		cSubSessionIter(*gpcSession);
	MediaSubsession*			pcSubsession;

	Boolean	fIsMadeProgress = False;

	char const* pszSingleMediumToTest = gpszSingleMediumName;
	while( (pcSubsession = cSubSessionIter.next()) != NULL ) {
		// If we've asked to receive only a single medium, then check this now:
		if( NULL != pszSingleMediumToTest ) {
			if( 0 != strcmp(pcSubsession->mediumName(), pszSingleMediumToTest) ) {
				*gpcEnv << "Ignoring \"" << pcSubsession->mediumName()
						<< "/" << pcSubsession->codecName()
						<< "\" subsession, because we've asked to receive a single " 
						<< gpszSingleMediumName
						<< " session only\n";
				continue;
			}
			else {
				// Receive this subsession only
				pszSingleMediumToTest = "xxxxx";
				
				// this hack ensures that we get only 1 subsession of this type
			}
		}

		if( 0 != gnClientDesiredPortNum ) {
			pcSubsession->setClientPortNum( gnClientDesiredPortNum );
			gnClientDesiredPortNum += 2;
		}

		if( gfIsCreateReceivers ) {
			if( !pcSubsession->initiate(gnSimpleRTPoffsetArg) ) {
				*gpcEnv << "Unable to create receiver for \"" << pcSubsession->mediumName()
						<< "/" << pcSubsession->codecName()
						<< "\" subsession: " << gpcEnv->getResultMsg() << "\n";
			}
			else {
				*gpcEnv << "Created receiver for \"" << pcSubsession->mediumName()
						<< "/" << pcSubsession->codecName()
						<< "\" subsession (client ports " << pcSubsession->clientPortNum()
						<< "-" << pcSubsession->clientPortNum()+1 << ")\n";
				
				fIsMadeProgress = True;

				if( NULL != pcSubsession->rtpSource() ) {
					// Because we're saving the incoming data, rather than playing
					// it in real time, allow an especially large time threshold
					// (1 second) for reordering misordered incoming packets:
					unsigned const thresh = 1000000; // 1 second 
					pcSubsession->rtpSource()->setPacketReorderingThresholdTime(thresh);

					if( 0 < gnSocketInputBufferSize ) {
						// Set the RTP source's input buffer size as specified:
						int socketNum = pcSubsession->rtpSource()->RTPgs()->socketNum();
						unsigned curBufferSize = getReceiveBufferSize( *gpcEnv, socketNum );
						unsigned newBufferSize = setReceiveBufferTo( *gpcEnv, 
																	 socketNum, 
																	 gnSocketInputBufferSize );
						
						*gpcEnv << "Changed socket receive buffer size for the \""
								<< pcSubsession->mediumName()
								<< "/" << pcSubsession->codecName()
								<< "\" subsession from "
								<< curBufferSize << " to "
								<< newBufferSize << " bytes\n";
					}//if( 0 < gnSocketInputBufferSize )
				}//if( NULL != pcSubsession->rtpSource() )
			}
		}//if( gfIsCreateReceivers )
		else {
			if( 0 == pcSubsession->clientPortNum() ) {
				*gpcEnv << "No client port was specified for the \""
						<< pcSubsession->mediumName()
						<< "/" << pcSubsession->codecName()
						<< "\" subsession.  (Try adding the \"-p <portNum>\" option.)\n";
			}
			else {
				fIsMadeProgress = True;
			}
		}//if( gfIsCreateReceivers )
	}//while( (pcSubsession = cSubSessionIter.next()) != NULL )

	if( !fIsMadeProgress ) {
		_shutdown();
	}

	//////////////////////////////////////////////////////////////////////
	// Perform additional 'setup' on each subsession, before playing them:
	_setupStreams();

	//////////////////////////////////////////////////////////////////////
	// Create output files:
	if( gfIsCreateReceivers ) {

		// Create a "QuickTimeFileSink", to write to 'stdout':
		if( gfIsOutputQuickTimeFile ) {
			gpcQTOut = QuickTimeFileSink::createNew( *gpcEnv,
													 *gpcSession,
													 "stdout",
													 gnFileSinkBufferSize,
													 gnMovieWidth, gnMovieHeight, gnMovieFPS,
												     gfIsPacketLossCompensate,
													 gfIsSyncStreams,
													 gfIsGenerateHintTracks,
													 gfIsGenerateMP4Format );
			if( gpcQTOut == NULL ) {
				*gpcEnv << "Failed to create QuickTime file sink for stdout: " << gpcEnv->getResultMsg();
				_shutdown();
			}

			gpcQTOut->startPlaying(_sessionAfterPlaying, NULL);
		}
		else
		// Create an "AVIFileSink", to write to 'stdout':
		if( gfIsOutputAVIFile ) {
			gpcAVIOut = AVIFileSink::createNew( *gpcEnv, 
												*gpcSession, 
												"testtest.avi",
												//"stdout",
											    gnFileSinkBufferSize,
												gnMovieWidth, gnMovieHeight, gnMovieFPS,
												gfIsPacketLossCompensate );
			if( gpcAVIOut == NULL ) {
				*gpcEnv << "Failed to create AVI file sink for stdout: " << gpcEnv->getResultMsg();
				_shutdown();
			}

			gpcAVIOut->startPlaying(_sessionAfterPlaying, NULL);
		}
		else {
			// Create and start "FileSink"s for each subsession:
			fIsMadeProgress = False;

			cSubSessionIter.reset();
			while( (pcSubsession = cSubSessionIter.next()) != NULL ) {
				if (pcSubsession->readSource() == NULL) continue; // was not initiated
	
				// Create an output file for each desired stream:
				char outFileName[1000];
				if( gpszSingleMediumName == NULL ) {
					// Output file name is
					//     "<filename-prefix><medium_name>-<codec_name>-<counter>"
					static unsigned streamCounter = 0;
					snprintf( outFileName, sizeof outFileName, "%s%s-%s-%d",
							  gpszFileNamePrefix, pcSubsession->mediumName(),
							  pcSubsession->codecName(), ++streamCounter );
				}
				else {
					sprintf(outFileName, "stdout");
				}
				
				FileSink* fileSink;
				if( strcmp(pcSubsession->mediumName(), "audio" ) == 0 &&
					(strcmp(pcSubsession->codecName(), "AMR") == 0 ||
					strcmp(pcSubsession->codecName(), "AMR-WB") == 0) ) {
					
					// For AMR audio streams, we use a special sink that inserts AMR frame hdrs:
					fileSink = AMRAudioFileSink::createNew( *gpcEnv, 
															outFileName,
															gnFileSinkBufferSize, gfIsOneFilePerFrame );
				}
				else
				if( strcmp(pcSubsession->mediumName(), "video") == 0 &&
					(strcmp(pcSubsession->codecName(), "H264") == 0)) {
					
					// For H.264 video stream, we use a special sink that insert start_codes:
					fileSink = H264VideoFileSink::createNew( *gpcEnv, 
						                                     outFileName,
															 gnFileSinkBufferSize, gfIsOneFilePerFrame );
				}
				else {
					// Normal case:
					fileSink = FileSink::createNew( *gpcEnv, 
													outFileName,
													gnFileSinkBufferSize, gfIsOneFilePerFrame );
				}
				
				pcSubsession->sink = fileSink;
				if( pcSubsession->sink == NULL ) {
					*gpcEnv << "Failed to create FileSink for \"" << outFileName
					<< "\": " << gpcEnv->getResultMsg() << "\n";
				}
				else {
					if( gpszSingleMediumName == NULL ) {
						*gpcEnv << "Created output file: \"" << outFileName << "\"\n";
					}
					else {
						*gpcEnv << "Outputting data from the \"" << pcSubsession->mediumName()
								<< "/" << pcSubsession->codecName()
								<< "\" subsession to 'stdout'\n";
					}
					
					if (strcmp(pcSubsession->mediumName(), "video") == 0 &&
						strcmp(pcSubsession->codecName(), "MP4V-ES") == 0 &&
						pcSubsession->fmtp_config() != NULL) {
						// For MPEG-4 video RTP streams, the 'config' information
						// from the SDP description contains useful VOL etc. headers.
						// Insert this data at the front of the output file:
						unsigned configLen;
						unsigned char* configData
								= parseGeneralConfigStr(pcSubsession->fmtp_config(), configLen);
						struct timeval timeNow;
						
						gettimeofday(&timeNow, NULL);
						fileSink->addData(configData, configLen, timeNow);
						delete[] configData;
					}

					pcSubsession->sink->startPlaying( *(pcSubsession->readSource()),
													  _subsessionAfterPlaying,
												      pcSubsession );
	  
					// Also set a handler to be called if a RTCP "BYE" arrives
					// for this subsession:
					if( pcSubsession->rtcpInstance() != NULL ) {
						pcSubsession->rtcpInstance()->setByeHandler( _subsessionByeHandler,
																	 pcSubsession);
					}
					fIsMadeProgress = True;
				}
			}//while( (pcSubsession = cSubSessionIter.next()) != NULL )
			
			if (!fIsMadeProgress) _shutdown();
		}// Create and start "FileSink"s for each subsession:
	}//if( gfIsCreateReceivers )

	// Finally, start playing each subsession, to start the data flow:
	_startPlayingStreams();
	
	gpcEnv->taskScheduler().doEventLoop(); // does not return
	
	return 0; // only to prevent compiler warning
}

void _usage()
{
	*gpcEnv << "Usage: " << gpszProgName
	<< " [-p <startPortNum>] [-r|-q|-4|-i] [-a|-v] [-V] [-e <endTime>] [-E <max-inter-packet-gap-time> [-c] [-s <offset>] [-n] [-O]"
	<< (gfIsControlConnectionUsesTCP ? " [-t|-T <http-port>]" : "")
	<< " [-u <username> <password>"
	<< (gfIsAllowProxyServers ? " [<proxy-server> [<proxy-server-port>]]" : "")
	<< "]" << (gfIsSupportCodecSelection ? " [-A <audio-codec-rtp-payload-format-code>|-D <mime-subtype-name>]" : "")
	<< " [-w <width> -h <height>] [-f <frames-per-second>] [-y] [-H] [-Q [<measurement-interval>]] [-F <filename-prefix>] [-b <file-sink-buffer-size>] [-B <input-socket-buffer-size>] [-I <input-interface-ip-address>] [-m] <url> (or " << gpszProgName << " -o [-V] <url>)\n";
	//##### Add "-R <dest-rtsp-url>" #####

	// clear variable and exit program!!
	_shutdown();
}

int _parsingusage( int _argc, char** _argv )
{
	// unfortunately we can't use getopt() here, as Windoze doesn't have it
	while( 2 < _argc ) {
		char* const opt = _argv[1];
		if( '-' != opt[0] ) {
			_usage();
		}

		switch( opt[1] ) {
			// specify the client start port number
			case 'p': {
				int portArg;

				if( 1 != sscanf(_argv[2], "%d", &portArg ) ) {
					_usage();
				}

				if( portArg <= 0 || portArg >= 65536 || portArg & 1) {
					*gpcEnv << "bad port number: " << portArg << " (must be even, and in the range (0,65536))\n";
					_usage();
				}

				gnClientDesiredPortNum = (unsigned short)portArg;
				++_argv; --_argc;
				break;
			}

			// do not receive data (instead, just 'play' the stream(s))
			case 'r': { 
				gfIsCreateReceivers = False;
				break;
			}

			// output a QuickTime file (to stdout)
			case 'q': { 
				gfIsOutputQuickTimeFile = True;
				break;
			}

			// output a 'mp4'-format file (to stdout)
			case '4': { 
				gfIsOutputQuickTimeFile = True;
				gfIsGenerateMP4Format = True;
				break;
			}

			// output an AVI file (to stdout)
			case 'i': { 
				gfIsOutputAVIFile = True;
				break;
			}

			// specify input interface... 
			case 'I': { 
				NetAddressList	cNetAddressList( _argv[2] );
				if( 0 == cNetAddressList.numAddresses() ) {
					*gpcEnv << "Failed to find network address for \"" << _argv[2] << "\"";
					break;
				}
				strcpy( gszClientIPAddress, _argv[2] );
				++_argv; --_argc;
				break;
			}

			// receive/record an audio stream only
			case 'a': {
				gfIsAudioOnly = True;
				gpszSingleMediumName = "audio";
				break;
			}

			// receive/record a video stream only
			case 'v': { 
				gfIsVideoOnly = True;
				gpszSingleMediumName = "video";
				break;
			}

			// verbose output
			case 'V': { 
				gnVerbosityLevel = 1;
				break;
			}

			// specify end time, or how much to delay after end time
			case 'e': { 
				float arg;
				if (sscanf(_argv[2], "%g", &arg) != 1) {
					_usage();
				}

				// not "arg<0", in case _argv[2] was "-0"
				// a 'negative' argument was specified; use this for "endTimeSlop":
				if (_argv[2][0] == '-') { 
					gdblEndTime = 0; // use whatever's in the SDP
					gdblEndTimeSlop = -arg;
				}
				else {
					gdblEndTime = arg;
					gdblEndTimeSlop = 0;
				}
				++_argv; --_argc;
				break;
			}

			// specify maximum number of seconds to wait for packets:
			case 'E': { 
				if (sscanf(_argv[2], "%u", &gnInterPacketGapMaxTime) != 1) {
					_usage();
				}
				++_argv; --_argc;
				break;
			}

			// play continuously
			case 'c': { 
				gfIsPlayContinuously = True;
				break;
			}

			// specify an offset to use with "SimpleRTPSource"s
			case 's': { 
				if (sscanf(_argv[2], "%d", &gnSimpleRTPoffsetArg) != 1) {
					_usage();
				}
				
				if (gnSimpleRTPoffsetArg < 0) {
					*gpcEnv << "offset argument to \"-s\" must be >= 0\n";
					_usage();
				}
				++_argv; --_argc;
				break;
			}

			// Don't send an "OPTIONS" request before "DESCRIBE"
			case 'O': { 
				gfIsSendOptionsRequest = False;
				break;
			}

			// Send only the "OPTIONS" request to the server
			case 'o': { 
				gfIsSendOptionsRequestOnly = True;
				break;
			}

			// output multiple files - one for each frame
			case 'm': { 
				gfIsOneFilePerFrame = True;
				break;
			}

			// notify the user when the first data packet arrives
			case 'n': { 
				gfIsNotifyOnPacketArrival = True;
				break;
			}

			// stream RTP and RTCP over the TCP 'control' connection
			case 't': {
				if (gfIsControlConnectionUsesTCP) {
					gfIsStreamUsingTCP = True;
				}
				else {
					_usage();
				}
				break;
			}

			// stream RTP and RTCP over a HTTP connection
			case 'T': {
				if (gfIsControlConnectionUsesTCP) {
					if (_argc > 3 && _argv[2][0] != '-') {
						// The next argument is the HTTP server port number:
						if( (sscanf(_argv[2], "%hu", &gnTunnelOverHTTPPortNum) == 1) && 
							(gnTunnelOverHTTPPortNum > 0) ) {
							++_argv; --_argc;
							break;
						}
					}
				}

				// If we get here, the option was specified incorrectly:
				_usage();
				break;
			}

			// specify a username and password
			case 'u': { 
				gpszUsername = _argv[2];
				gpszPassword = _argv[3];
				_argv+=2; _argc-=2;
				
				if( gfIsAllowProxyServers && _argc > 3 && _argv[2][0] != '-' ) {
					// The next argument is the name of a proxy server:
					gpszProxyServerName = _argv[2];
					++_argv; --_argc;

					if (_argc > 3 && _argv[2][0] != '-') {
						// The next argument is the proxy server port number:
						if (sscanf(_argv[2], "%hu", &gnProxyServerPortNum) != 1) {
							_usage();
						}
						++_argv; --_argc;
					}
				}
				break;
			}

			// specify a desired audio RTP payload format
			case 'A': { 
				unsigned formatArg;
				if( sscanf(_argv[2], "%u", &formatArg ) != 1 || formatArg >= 96 ) {
					_usage();
				}
				
				gcbDesiredAudioRTPPayloadFormat = (unsigned char)formatArg;
				++_argv; --_argc;
				break;
			}

			// specify a MIME subtype for a dynamic RTP payload type
			case 'D': { 
				gpszMIMESubtype = _argv[2];
				if( gcbDesiredAudioRTPPayloadFormat == 0 ) {
					gcbDesiredAudioRTPPayloadFormat = 96;
				}
				++_argv; --_argc;
				break;
			}

			// specify a width (pixels) for an output QuickTime or AVI movie
			case 'w': { 
				if( sscanf(_argv[2], "%hu", &gnMovieWidth) != 1 ) {
					_usage();
				}
				
				gfIsMovieWidthOptionSet = True;
				++_argv; --_argc;
				break;
			}

			// specify a height (pixels) for an output QuickTime or AVI movie
			case 'h': { 
				if( sscanf(_argv[2], "%hu", &gnMovieHeight) != 1 ) {
					_usage();
				}
				
				gfIsMovieHeightOptionSet = True;
				++_argv; --_argc;
				break;
			}

			// specify a frame rate (per second) for an output QT or AVI movie
			case 'f': { 
				if( sscanf(_argv[2], "%u", &gnMovieFPS) != 1 ) {
					_usage();
				}
				
				gfIsMovieFPSOptionSet = True;
				++_argv; --_argc;
				break;
			}

			// specify a prefix for the audio and video output files
			case 'F': { 
				gpszFileNamePrefix = _argv[2];
				++_argv; --_argc;
				break;
			}

			// specify the size of buffers for "FileSink"s
			case 'b': { 
				if( sscanf(_argv[2], "%u", &gnFileSinkBufferSize) != 1 ) {
					_usage();
				}
				++_argv; --_argc;
				break;
			}

			// specify the size of input socket buffers
			case 'B': { 
				if( sscanf(_argv[2], "%u", &gnSocketInputBufferSize) != 1 ) {
					_usage();
				}
				++_argv; --_argc;
				break;
			}

			// Note: The following option is deprecated, and may someday be removed:
			// try to compensate for packet loss by repeating frames
			case 'l': { 
				gfIsPacketLossCompensate = True;
				break;
			}

			// synchronize audio and video streams
			case 'y': { 
				gfIsSyncStreams = True;
				break;
			}

			// generate hint tracks (as well as the regular data tracks)
			case 'H': { 
				gfIsGenerateHintTracks = True;
				break;
			}

			// output QOS measurements
			case 'Q': { 
				gnQOSMeasurementIntervalMS = 1000; // default: 1 second

				if( _argc > 3 && _argv[2][0] != '-' ) {
					// The next argument is the measurement interval,
					// in multiples of 100 ms
					if( sscanf(_argv[2], "%u", &gnQOSMeasurementIntervalMS) != 1 ) {
						_usage();
					}
					
					gnQOSMeasurementIntervalMS *= 100;
					++_argv; --_argc;
				}
				break;
				}

			default: {
				_usage();
				break;
			}
		}//switch( opt[1] )

		++_argv; --_argc;
	}// while( 2 < _argc )

	if( 2 != _argc ) {
		_usage();
	}

	if( gfIsOutputQuickTimeFile && gfIsOutputAVIFile ) {
		*gpcEnv << "The -i and -q (or -4) flags cannot both be used!\n";
		_usage();
	}

	Boolean fIsOutFile = ( gfIsOutputQuickTimeFile || gfIsOutputAVIFile );
	if( fIsOutFile ) {
		if( !gfIsCreateReceivers ) {
			*gpcEnv << "The -r and -q (or -4 or -i) flags cannot both be used! \n";
			_usage();
		}

		if( !gfIsMovieWidthOptionSet ) {
			*gpcEnv << "Warning: The -q, -4 or -i option was used, but not -w.  Assuming a video width of "
					<< gnMovieWidth << " pixels\n";
		}

		if( !gfIsMovieHeightOptionSet ) {
			*gpcEnv << "Warning: The -q, -4 or -i option was used, but not -h.  Assuming a video height of "
					<< gnMovieHeight << " pixels\n";
		}
  
		if( !gfIsMovieFPSOptionSet ) {
			*gpcEnv << "Warning: The -q, -4 or -i option was used, but not -f.  Assuming a video frame rate of "
					<< gnMovieFPS << " frames-per-second\n";
		}
	}//if( fIsOutFile )
  
	if( gfIsAudioOnly && gfIsVideoOnly ) {
		*gpcEnv << "The -a and -v flags cannot both be used!\n";
		_usage();
	}

	if( gfIsSendOptionsRequestOnly && !gfIsSendOptionsRequest ) {
		*gpcEnv << "The -o and -O flags cannot both be used!\n";
		_usage();
	}

	if( 0 < gnTunnelOverHTTPPortNum ) {
		if( gfIsStreamUsingTCP ) {
			*gpcEnv << "The -t and -T flags cannot both be used!\n";
			_usage();
		}
		else {
			gfIsStreamUsingTCP = True;
		}
	}

	if( !gfIsCreateReceivers && gfIsNotifyOnPacketArrival ) {
		*gpcEnv << "Warning: Because we're not receiving stream data, the -n flag has no effect\n";
	}

	if( 0 > gdblEndTimeSlop ) {
		// This parameter wasn't set, so use a default value.
		// If we're measuring QOS stats, then don't add any slop, to avoid
		// having 'empty' measurement intervals at the end.
		gdblEndTimeSlop = gnQOSMeasurementIntervalMS > 0 ? 0.0 : 5.0;
	}

	strcpy( gszServerURL, _argv[1] );

	return 0;
}

void _setupStreams()
{
	MediaSubsessionIterator		cSubSessionIter(*gpcSession);
	MediaSubsession*			pcSubsession;
	
	Boolean	fIsMadeProgress = False;

	while( NULL != (pcSubsession = cSubSessionIter.next()) ) {
		if( 0 == pcSubsession->clientPortNum() ) {
			continue; // port # was not set
		}

		// in openRTSP.cpp
		if( !clientSetupSubsession(gpcOurClient, pcSubsession, gfIsStreamUsingTCP) ) {
			*gpcEnv << "Failed to setup \"" << pcSubsession->mediumName()
					<< "/" << pcSubsession->codecName()
					<< "\" subsession: " << gpcEnv->getResultMsg() << "\n";
		}
		else {
			*gpcEnv << "Setup \"" << pcSubsession->mediumName()
					<< "/" << pcSubsession->codecName()
					<< "\" subsession (client ports " << pcSubsession->clientPortNum()
					<< "-" << pcSubsession->clientPortNum()+1 << ")\n";
			fIsMadeProgress = True;
		}
	}

	if( !fIsMadeProgress ) _shutdown();
}

void _startPlayingStreams()
{
	// refer to openRTSP.cpp
	if( !clientStartPlayingSession(gpcOurClient, gpcSession) ) {
		*gpcEnv << "Failed to start playing session: " << gpcEnv->getResultMsg() << "\n";
		_shutdown();
	}
	else {
		*gpcEnv << "Started playing session\n";
	}

	if( gnQOSMeasurementIntervalMS > 0 ) {
		// Begin periodic QOS measurements:
		_beginQOSMeasurement();
	}

	// Figure out how long to delay (if at all) before shutting down, or
	// repeating the playing
	Boolean timerIsBeingUsed = False;
	double totalEndTime		 = gdblEndTime;

	if( gdblEndTime == 0 ) {
		gdblEndTime = gpcSession->playEndTime(); // use SDP end time
	}

	if( gdblEndTime > 0 ) {
		double const maxDelayTime
			= (double)( ((unsigned)0x7FFFFFFF)/1000000.0 );

		if( gdblEndTime > maxDelayTime ) {
			*gpcEnv << "Warning: specified end time " << gdblEndTime
					<< " exceeds maximum " << maxDelayTime
					<< "; will not do a delayed shutdown\n";
			gdblEndTime = 0.0;
		}
		else {
			timerIsBeingUsed = True;
			totalEndTime	 = gdblEndTime + gdblEndTimeSlop;

			int uSecsToDelay = (int)(totalEndTime*1000000.0);
		
			gtaskSessionTimer = gpcEnv->taskScheduler().scheduleDelayedTask(
									uSecsToDelay,
									(TaskFunc*)_sessionTimerHandler,
									(void*)NULL);
		}
	}

	char const* actionString
			= gfIsCreateReceivers ? "Receiving streamed data" : "Data is being streamed";

	if( timerIsBeingUsed ) {
		*gpcEnv << actionString
				<< " (for up to " << totalEndTime
				<< " seconds)...\n";
	}
	else {
#ifdef USE_SIGNALS
		pid_t ourPid = getpid();
		*gpcEnv << actionString
				<< " (signal with \"kill -HUP " << (int)ourPid
				<< "\" or \"kill -USR1 " << (int)ourPid
				<< "\" to terminate)...\n";
#else
		*gpcEnv << actionString << "...\n";
#endif
	}

	// Watch for incoming packets (if desired):
	_checkForPacketArrival(NULL);
	_checkInterPacketGaps(NULL);
}

void _tearDownStreams()
{
	if( gpcSession == NULL ) return;

	clientTearDownSession(gpcOurClient, gpcSession);
}

void _closeMediaSinks()
{
	Medium::close(gpcQTOut);
	Medium::close(gpcAVIOut);

	if (gpcSession == NULL) return;
	MediaSubsessionIterator iter(*gpcSession);
	MediaSubsession* subsession;

	while( (subsession = iter.next()) != NULL ) {
		Medium::close(subsession->sink);
		subsession->sink = NULL;
	}
}

void _subsessionAfterPlaying( void* _pclientdata )
{
	// Begin by closing this media subsession's stream:
	MediaSubsession* subsession = (MediaSubsession*)_pclientdata;
	
	Medium::close(subsession->sink);
	subsession->sink = NULL;

	// Next, check whether *all* subsessions' streams have now been closed:
	MediaSession& session = subsession->parentSession();
	MediaSubsessionIterator iter(session);

	while( (subsession = iter.next()) != NULL ) {
		if( subsession->sink != NULL ) {
			return; // this subsession is still active
		}
	}

	// All subsessions' streams have now been closed
	_sessionAfterPlaying();
}

void _subsessionByeHandler(void* clientData)
{
	struct timeval timeNow;
	gettimeofday(&timeNow, NULL);
	unsigned secsDiff = timeNow.tv_sec - gtStartTime.tv_sec;

	MediaSubsession* subsession = (MediaSubsession*)clientData;
	*gpcEnv << "Received RTCP \"BYE\" on \"" << subsession->mediumName()
			<< "/" << subsession->codecName()
			<< "\" subsession (after " << secsDiff
			<< " seconds)\n";

	// Act now as if the subsession had closed:
	_subsessionAfterPlaying(subsession);
}

void _sessionAfterPlaying( void* /*clientData*/ )
{
	if( !gfIsPlayContinuously ) {
		_shutdown(0);
	}
	else {
		// We've been asked to play the stream(s) over again:
		_startPlayingStreams();
	}
}

void _sessionTimerHandler( void* /*clientData*/ )
{
	gtaskSessionTimer = NULL;
	_sessionAfterPlaying();
}

class qosMeasurementRecord
{
public:
	qosMeasurementRecord(struct timeval const& startTime, RTPSource* src)
			: fSource(src), fNext(NULL),
			  kbits_per_second_min(1e20), kbits_per_second_max(0),
			  kBytesTotal(0.0),
			  packet_loss_fraction_min(1.0), packet_loss_fraction_max(0.0),
			  totNumPacketsReceived(0), totNumPacketsExpected(0)
	{
		measurementEndTime = measurementStartTime = startTime;

		RTPReceptionStatsDB::Iterator statsIter(src->receptionStatsDB());
		
		// Assume that there's only one SSRC source (usually the case):
		RTPReceptionStats* stats = statsIter.next(True);
		if (stats != NULL) {
			kBytesTotal = stats->totNumKBytesReceived();
			totNumPacketsReceived = stats->totNumPacketsReceived();
			totNumPacketsExpected = stats->totNumPacketsExpected();
		}
	}
	
	virtual ~qosMeasurementRecord() { delete fNext; }
	void periodicQOSMeasurement(struct timeval const& timeNow);

public:
	RTPSource* fSource;
	qosMeasurementRecord* fNext;

public:
	struct timeval measurementStartTime, measurementEndTime;
	double kbits_per_second_min, kbits_per_second_max;
	double kBytesTotal;
	double packet_loss_fraction_min, packet_loss_fraction_max;
	unsigned totNumPacketsReceived, totNumPacketsExpected;
};

static qosMeasurementRecord* gpcQOSRecordHead = NULL;

static void     _periodicQOSMeasurement(void* clientData); // forward
static unsigned gnNextQOSMeasurementUSecs;

static void _scheduleNextQOSMeasurement()
{
	gnNextQOSMeasurementUSecs += gnQOSMeasurementIntervalMS * 1000;

	struct timeval timeNow;
	gettimeofday(&timeNow, NULL);
	
	unsigned timeNowUSecs = timeNow.tv_sec*1000000 + timeNow.tv_usec;
	unsigned usecsToDelay = gnNextQOSMeasurementUSecs < timeNowUSecs ? 0
						  : gnNextQOSMeasurementUSecs - timeNowUSecs;

	gtaskQOSMeasurementTimer = 
		gpcEnv->taskScheduler().scheduleDelayedTask( usecsToDelay, 
													 (TaskFunc*)_periodicQOSMeasurement, 
													 (void*)NULL );
}

static void _periodicQOSMeasurement(void* /*clientData*/)
{
	struct timeval timeNow;
	gettimeofday(&timeNow, NULL);

	for( qosMeasurementRecord* qosRecord = gpcQOSRecordHead;
         qosRecord != NULL; 
		 qosRecord = qosRecord->fNext ) {
		qosRecord->periodicQOSMeasurement(timeNow);
	}

	// Do this again later:
	_scheduleNextQOSMeasurement();
}

void qosMeasurementRecord::periodicQOSMeasurement( struct timeval const& _timeNow )
{
	unsigned secsDiff  = _timeNow.tv_sec - measurementEndTime.tv_sec;
	int		 usecsDiff = _timeNow.tv_usec - measurementEndTime.tv_usec;
	double   timeDiff  = secsDiff + usecsDiff/1000000.0;
	
	measurementEndTime = _timeNow;

	RTPReceptionStatsDB::Iterator statsIter(fSource->receptionStatsDB());
	
	// Assume that there's only one SSRC source (usually the case):
	RTPReceptionStats* stats = statsIter.next(True);
	if( stats != NULL ) {
		double kBytesTotalNow = stats->totNumKBytesReceived();
		double kBytesDeltaNow = kBytesTotalNow - kBytesTotal;
		
		kBytesTotal = kBytesTotalNow;

		double kbpsNow = (timeDiff == 0.0) ? 0.0 : 8*kBytesDeltaNow/timeDiff;
		if (kbpsNow < 0.0) kbpsNow = 0.0; // in case of roundoff error
		if (kbpsNow < kbits_per_second_min) kbits_per_second_min = kbpsNow;
		if (kbpsNow > kbits_per_second_max) kbits_per_second_max = kbpsNow;

		unsigned totReceivedNow = stats->totNumPacketsReceived();
		unsigned totExpectedNow = stats->totNumPacketsExpected();
		unsigned deltaReceivedNow = totReceivedNow - totNumPacketsReceived;
		unsigned deltaExpectedNow = totExpectedNow - totNumPacketsExpected;
    
		totNumPacketsReceived = totReceivedNow;
		totNumPacketsExpected = totExpectedNow;

		double lossFractionNow = (deltaExpectedNow == 0) ? 
								 0.0 : 1.0 - deltaReceivedNow/(double)deltaExpectedNow;

		//if (lossFractionNow < 0.0) lossFractionNow = 0.0; //reordering can cause
		if( lossFractionNow < packet_loss_fraction_min ) {
			packet_loss_fraction_min = lossFractionNow;
		}
		
		if (lossFractionNow > packet_loss_fraction_max) {
			packet_loss_fraction_max = lossFractionNow;
		}
	}
}

void _beginQOSMeasurement()
{
	// Set up a measurement record for each active subsession:
	struct timeval startTime;
	gettimeofday(&startTime, NULL);
	
	gnNextQOSMeasurementUSecs = startTime.tv_sec*1000000 + startTime.tv_usec;
	qosMeasurementRecord* qosRecordTail = NULL;
	
	MediaSubsessionIterator iter(*gpcSession);
	MediaSubsession* subsession;

	while( (subsession = iter.next()) != NULL ) {
		RTPSource* src = subsession->rtpSource();
		if( src == NULL ) continue;

		qosMeasurementRecord* qosRecord = new qosMeasurementRecord(startTime, src);
		if (gpcQOSRecordHead == NULL) gpcQOSRecordHead = qosRecord;
		if (qosRecordTail != NULL) qosRecordTail->fNext = qosRecord;
		qosRecordTail  = qosRecord;
	}

	// Then schedule the first of the periodic measurements:
	_scheduleNextQOSMeasurement();
}

void _printQOSData( int exitCode )
{
	if( exitCode != 0 && gnStatusCode == 0 ) gnStatusCode = 2;
  
	*gpcEnv << "begin_QOS_statistics\n";
	*gpcEnv << "server_availability\t" << (gnStatusCode == 1 ? 0 : 100) << "\n";
	*gpcEnv << "stream_availability\t" << (gnStatusCode == 0 ? 100 : 0) << "\n";

	// Print out stats for each active subsession:
	qosMeasurementRecord* curQOSRecord = gpcQOSRecordHead;
	if( gpcSession != NULL ) {
		MediaSubsessionIterator iter(*gpcSession);
		MediaSubsession* subsession;
		
		while( (subsession = iter.next()) != NULL ) {
			RTPSource* src = subsession->rtpSource();
			if( src == NULL ) continue;

			*gpcEnv << "subsession\t" << subsession->mediumName()
					<< "/" << subsession->codecName() << "\n";
      
			unsigned numPacketsReceived = 0, numPacketsExpected = 0;

			if( curQOSRecord != NULL ) {
				numPacketsReceived = curQOSRecord->totNumPacketsReceived;
				numPacketsExpected = curQOSRecord->totNumPacketsExpected;
			}
			
			*gpcEnv << "num_packets_received\t" << numPacketsReceived << "\n";
			*gpcEnv << "num_packets_lost\t" << numPacketsExpected - numPacketsReceived << "\n";
      
			if( curQOSRecord != NULL ) {
				unsigned secsDiff = curQOSRecord->measurementEndTime.tv_sec
								  - curQOSRecord->measurementStartTime.tv_sec;
				
				int usecsDiff = curQOSRecord->measurementEndTime.tv_usec
						      - curQOSRecord->measurementStartTime.tv_usec;
				
				double measurementTime = secsDiff + usecsDiff/1000000.0;
	
				*gpcEnv << "elapsed_measurement_time\t" << measurementTime << "\n";
				*gpcEnv << "kBytes_received_total\t" << curQOSRecord->kBytesTotal << "\n";
				*gpcEnv << "measurement_sampling_interval_ms\t" << gnQOSMeasurementIntervalMS << "\n";
	
				if( curQOSRecord->kbits_per_second_max == 0 ) {
					// special case: we didn't receive any data:
					*gpcEnv <<  "kbits_per_second_min\tunavailable\n"
								"kbits_per_second_ave\tunavailable\n"
								"kbits_per_second_max\tunavailable\n";
				}
				else {
					*gpcEnv << "kbits_per_second_min\t" << curQOSRecord->kbits_per_second_min << "\n";
					*gpcEnv << "kbits_per_second_ave\t"
							<< (measurementTime == 0.0 ? 0.0 : 8*curQOSRecord->kBytesTotal/measurementTime) << "\n";
					*gpcEnv << "kbits_per_second_max\t" << curQOSRecord->kbits_per_second_max << "\n";
				}
	
				*gpcEnv << "packet_loss_percentage_min\t" << 100*curQOSRecord->packet_loss_fraction_min << "\n";
				
				double packetLossFraction = numPacketsExpected == 0 ? 1.0
										  : 1.0 - numPacketsReceived/(double)numPacketsExpected;
	
				if (packetLossFraction < 0.0) packetLossFraction = 0.0;

				*gpcEnv << "packet_loss_percentage_ave\t" << 100*packetLossFraction << "\n";
				*gpcEnv << "packet_loss_percentage_max\t"
						<< (packetLossFraction == 1.0 ? 100.0 : 100*curQOSRecord->packet_loss_fraction_max) << "\n";
	
				RTPReceptionStatsDB::Iterator statsIter(src->receptionStatsDB());
				
				// Assume that there's only one SSRC source (usually the case):
				RTPReceptionStats* stats = statsIter.next(True);

				if( stats != NULL ) {
					*gpcEnv << "inter_packet_gap_ms_min\t" << stats->minInterPacketGapUS()/1000.0 << "\n";
					
					struct timeval totalGaps = stats->totalInterPacketGaps();
					double totalGapsMS = totalGaps.tv_sec*1000.0 + totalGaps.tv_usec/1000.0;
					unsigned totNumPacketsReceived = stats->totNumPacketsReceived();
					
					*gpcEnv << "inter_packet_gap_ms_ave\t"
							<< (totNumPacketsReceived == 0 ? 0.0 : totalGapsMS/totNumPacketsReceived) << "\n";
					*gpcEnv << "inter_packet_gap_ms_max\t" << stats->maxInterPacketGapUS()/1000.0 << "\n";
				}
				
				curQOSRecord = curQOSRecord->fNext;
			}
		}
	}    

	*gpcEnv << "end_QOS_statistics\n";

	delete gpcQOSRecordHead;
}

void _shutdown(int exitCode)
{
	if( gpcEnv != NULL ) {
		gpcEnv->taskScheduler().unscheduleDelayedTask(gtaskSessionTimer);
		gpcEnv->taskScheduler().unscheduleDelayedTask(gtaskArrivalCheckTimer);
		gpcEnv->taskScheduler().unscheduleDelayedTask(gtaskInterPacketGapCheckTimer);
		gpcEnv->taskScheduler().unscheduleDelayedTask(gtaskQOSMeasurementTimer);
	}

	if( gnQOSMeasurementIntervalMS > 0 ) {
		_printQOSData(exitCode);
	}

	// Close our output files:
	_closeMediaSinks();

	// Teardown, then shutdown, any outstanding RTP/RTCP subsessions
	_tearDownStreams();
	Medium::close(gpcSession);

	// Finally, shut down our client:
	Medium::close(gpcOurClient);

	// Adios...
	exit(exitCode);
}

#ifdef USE_SIGNALS
void _signalHandlerShutdown( int /*sig*/ )
{
	*gpcEnv << "Got shutdown signal\n";
	_shutdown(0);
}
#endif

void _checkForPacketArrival( void* /*clientData*/ )
{
	// we're not checking 
	if( !gfIsNotifyOnPacketArrival ) return;

	// Check each subsession, to see whether it has received data packets:
	unsigned numSubsessionsChecked = 0;
	unsigned numSubsessionsWithReceivedData = 0;
	unsigned numSubsessionsThatHaveBeenSynced = 0;

	MediaSubsessionIterator iter(*gpcSession);
	MediaSubsession* subsession;
	
	while( (subsession = iter.next()) != NULL ) {
		RTPSource* src = subsession->rtpSource();
		if( src == NULL ) continue;
		
		++numSubsessionsChecked;

		if( src->receptionStatsDB().numActiveSourcesSinceLastReset() > 0 ) {
			// At least one data packet has arrived
			++numSubsessionsWithReceivedData;
		}
		
		if (src->hasBeenSynchronizedUsingRTCP()) {
			++numSubsessionsThatHaveBeenSynced;
		}
	}

	unsigned numSubsessionsToCheck = numSubsessionsChecked;

	// Special case for "QuickTimeFileSink"s and "AVIFileSink"s:
	// They might not use all of the input sources:
	if( gpcQTOut != NULL ) {
		numSubsessionsToCheck = gpcQTOut->numActiveSubsessions();
	}
	else
	if( gpcAVIOut != NULL ) {
		numSubsessionsToCheck = gpcAVIOut->numActiveSubsessions();
	}

	Boolean notifyTheUser;
	if( !gfIsSyncStreams ) {
		notifyTheUser = numSubsessionsWithReceivedData > 0; // easy case
	}
	else {
		notifyTheUser = (numSubsessionsWithReceivedData   >= numSubsessionsToCheck) && 
						(numSubsessionsThatHaveBeenSynced == numSubsessionsChecked);
		// Note: A subsession with no active sources is considered to be synced
	}

	if( notifyTheUser ) {
		struct timeval timeNow;
		gettimeofday(&timeNow, NULL);
		
		char timestampStr[100];
		
		sprintf(timestampStr, "%ld%03ld", timeNow.tv_sec, timeNow.tv_usec/1000);
		*gpcEnv << (gfIsSyncStreams ? "Synchronized d" : "D")
				<< "ata packets have begun arriving [" << timestampStr << "]\007\n";
		return;
	}

	// No luck, so reschedule this check again, after a delay:
	int uSecsToDelay = 100000; // 100 ms
	gtaskArrivalCheckTimer = 
		gpcEnv->taskScheduler().scheduleDelayedTask( uSecsToDelay,
													 (TaskFunc*)_checkForPacketArrival,
													 NULL );
}

void _checkInterPacketGaps( void* /*clientData*/ )
{
	// we're not checking 
	if( gnInterPacketGapMaxTime == 0 ) return;

	// Check each subsession, counting up how many packets have been received:
	unsigned newTotNumPacketsReceived = 0;

	MediaSubsessionIterator iter(*gpcSession);
	MediaSubsession* subsession;

	while( (subsession = iter.next()) != NULL ) {
		RTPSource* src = subsession->rtpSource();
		if(src == NULL) continue;
    
		newTotNumPacketsReceived += src->receptionStatsDB().totNumPacketsReceived();
	}

  
	if( newTotNumPacketsReceived == gnTotNumPacketsReceived ) {
		// No additional packets have been received since the last time we
		// checked, so end this stream:
		
		*gpcEnv << "Closing session, because we stopped receiving packets.\n";
		gtaskInterPacketGapCheckTimer = NULL;
		
		_sessionAfterPlaying();
	}
	else {
		gnTotNumPacketsReceived = newTotNumPacketsReceived;
		
		// Check again, after the specified delay: 
		gtaskInterPacketGapCheckTimer = 
			gpcEnv->taskScheduler().scheduleDelayedTask( gnInterPacketGapMaxTime * 1000000,
														 (TaskFunc*)_checkInterPacketGaps,
														 NULL);
	}
}
