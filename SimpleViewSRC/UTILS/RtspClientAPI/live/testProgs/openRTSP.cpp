/**********
This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the
Free Software Foundation; either version 2.1 of the License, or (at your
option) any later version. (See <http://www.gnu.org/copyleft/lesser.html>.)

This library is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
more details.

You should have received a copy of the GNU Lesser General Public License
along with this library; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**********/
// Copyright (c) 1996-2007, Live Networks, Inc.  All rights reserved
// A RTSP client test program that opens a RTSP URL argument,
// and extracts the data from each incoming RTP stream.

/*
	2007-10-26	jinohan

*/

#include "playCommon.hh"

Boolean		gfIsAllowProxyServers        = False;
Boolean		gfIsControlConnectionUsesTCP = True;
Boolean		gfIsSupportCodecSelection    = False;
char const* gpszClientProtocolName       = "RTSP";

Medium* createClient( UsageEnvironment& _cEnv,
                      int				_nVerbosityLevel, 
					  char const*		_pszApplicationName )
{
	extern portNumBits gnTunnelOverHTTPPortNum;

	return RTSPClient::createNew( _cEnv, _nVerbosityLevel, _pszApplicationName, gnTunnelOverHTTPPortNum );
}

char* getOptionsResponse( Medium*		_pcClient, 
						  char const*	_pszUrl,
						  char*         _pszUsername, 
						  char*			_pszPassword )
{
	RTSPClient* rtspClient = (RTSPClient*)_pcClient;
	return rtspClient->sendOptionsCmd( _pszUrl, _pszUsername, _pszPassword );
}

char* getSDPDescriptionFromURL( Medium*			_pcClient,
								char const*		_pszUrl,
								char const*		_pszUsername,
								char const*		_pszPassword,

								char const*		/*proxyServerName*/,
								unsigned short	/*proxyServerPortNum*/,
								unsigned short	/*clientStartPort*/ )
{
	RTSPClient* rtspClient = (RTSPClient*)_pcClient;

	char* result;
	if( _pszUsername != NULL && _pszPassword != NULL ) {
		result = rtspClient->describeWithPassword( _pszUrl, _pszUsername, _pszPassword );
	}
	else {
		result = rtspClient->describeURL(_pszUrl);
	}

	extern unsigned gnStatusCode;
	gnStatusCode = rtspClient->describeStatus();

	return result;
}

Boolean clientSetupSubsession( Medium*			_pcClient,
							   MediaSubsession*	_pcSubsession,
							   Boolean			_fStreamUsingTCP )
{
	if( _pcClient == NULL || _pcSubsession == NULL ) return False;
  
	RTSPClient* rtspClient = (RTSPClient*)_pcClient;
	
	return rtspClient->setupMediaSubsession( *_pcSubsession, False, _fStreamUsingTCP );
}

Boolean clientStartPlayingSession( Medium*			_pcClient,
								   MediaSession*	_pcSession )
{
	if( _pcClient == NULL || _pcSession == NULL ) return False;

	RTSPClient* rtspClient = (RTSPClient*)_pcClient;

	return rtspClient->playMediaSession( *_pcSession );
}

Boolean clientTearDownSession( Medium*			_pcClient,
							   MediaSession*	_pcSession )
{
	if( _pcClient == NULL || _pcSession == NULL ) return False;
	
	RTSPClient* rtspClient = (RTSPClient*)_pcClient;
	
	return rtspClient->teardownMediaSession( *_pcSession );
}
