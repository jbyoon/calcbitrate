#ifndef __RTSP_CLIENT_API_IMPL_H__
#define __RTSP_CLIENT_API_IMPL_H__

#include "RtspReceiver.h"
#include "IRTSPClientAPI.h"

class CRTSPClientAPI : public IRTSPClientAPI
{
public:
	CRTSPClientAPI();
	virtual ~CRTSPClientAPI(){};

	BOOL	Init();
	VOID	Release();
	BOOL	Connect(CHAR *pszUrl, CHAR *pszSession, CHAR *pszUsername, CHAR *pszPassword);
	VOID	Disconnect();
	HANDLE	GetEvent(UINT uType);
	BOOL	GetMediaData(UINT uType, BYTE **ppData, UINT *puFrameSize, struct timeval *pTimeStamp);
	BOOL	GetParam(CHAR *pszCmd, VOID *pParam, UINT uLen);
	BOOL	SetParam(CHAR *pszCmd, VOID *pParam);

	VOID	DestroySelf() { delete this; }

private:
	CRtspReceiver m_rtspRcv;
};

#endif	//__RTSP_CLIENT_API_IMPL_H__