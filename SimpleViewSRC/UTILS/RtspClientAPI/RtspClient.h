#ifndef __RTSP_CLIENT_H__
#define __RTSP_CLIENT_H__

#include <BasicUsageEnvironment.hh>
#include <liveMedia.hh>
#include "MediaSink.h"
#include "UserThread.h"

#define RTSP_CLIENT_URL_LENGTH			128
#define RTSP_CLIENT_USERNAME_LENGTH		32
#define RTSP_CLIENT_PASSWORD_LENGTH		32

struct STATISTICS
{
	UINT uTotalNumPacketReceived;
	UINT uTotalNumPacketExpected;
	UINT uTotalKbytesReceived;

	UINT uCurNumPacketReceived;
	UINT uCurNumPacketExpected;
	UINT uCurKbytesReceived;
};

class CRtspClient
{
public:
	CRtspClient();
	~CRtspClient();

public:
	BOOL Init(CHAR *pszUrl, 
		CHAR *pszUserName, 
		CHAR *pszPassword, 
		BOOL bIsTcp, 
		BOOL bIsMulticast, 
		BUFFER_INFO* pVideoBuffer, 
		BUFFER_INFO* pAudioBuffer, 
		BUFFER_INFO* pMetaBuffer);
	VOID Release();
	BOOL StartPlaying();
	VOID StopPlaying();
	BOOL IsAlive();

	CHAR* GetVideoCodec() { return m_szVideoCodec; }
	CHAR* GetMpeg4Config() { return m_szMpeg4Config; }
	CHAR* GetH264SpropParameterSets() { return m_szH264SpropParameterSets; }
	CHAR* GetAudioCodec() { return m_szAudioCodec; }
	UINT GetAudioSampleBits() { return m_uAudioSampleBits; }
	UINT GetAudioSampleRate() { return m_uAudioSampleRate; }

	VOID SetAliveCheckTime(UINT uTime) { m_uAliveCheckCount = uTime; }

	struct STATISTICS* GetVideoStatistics() { return &m_videoStats; }
	struct STATISTICS* GetAudioStatistics() { return &m_audioStats; }
	struct STATISTICS* GetMetaStatistics() { return &m_metaStats; }

private:
	BOOL CreateClient();
	BOOL Describe();
	BOOL SetupStream();

	static VOID SubsessionAfterPlaying(VOID *pClientData);
	static VOID StatisticsMeasurementStub(VOID *pClientData);
	VOID StatisticsMeasurement();
	static UINT WINAPI ThreadEventLoopStub(VOID *pParam);
	UINT WINAPI ThreadEventLoop();

private:
	UsageEnvironment *m_pEnv;
	TaskScheduler *m_pScheduler;
	RTSPClient *m_pRtspClient;
	MediaSession *m_pMediasession;
	CVideoSink *m_pVideoSink;
	CAudioSink *m_pAudioSink;
	CMetaSink *m_pMetaSink;
	TaskToken m_pStatisticsTask;
	CUserThread m_userThread;
	BUFFER_INFO *m_pVideoBuffer;
	BUFFER_INFO *m_pAudioBuffer;
	BUFFER_INFO *m_pMetaBuffer;
	STATISTICS m_videoStats;
	STATISTICS m_audioStats;
	STATISTICS m_metaStats;

	UINT m_uHttpPortNum;
	BOOL m_bIsTcp;
	BOOL m_bIsMulticast;
	BOOL m_bStopLoop;
	CHAR *m_pSdpDescription;
	CHAR m_szUrl[RTSP_CLIENT_URL_LENGTH];
	CHAR m_szUserName[RTSP_CLIENT_USERNAME_LENGTH];
	CHAR m_szPassword[RTSP_CLIENT_PASSWORD_LENGTH];
	UINT m_uInterval;	// msec
	UINT m_uLastVideoSrTime;	// msec
	UINT m_uLastAudioSrTime;	// msec
	UINT m_uLastMetaSrTime;	// msec
	UINT m_uUpdateCount;

	/* [ADD] ldsung 2012.03.20 */
	int	m_nConnectTestTime;

	CHAR m_szVideoCodec[16];
	CHAR m_szMpeg4Config[128];
	CHAR m_szH264SpropParameterSets[512];
	CHAR m_szAudioCodec[16];
	UINT m_uAudioSampleBits;
	UINT m_uAudioSampleRate;
	BOOL m_bIsWait;
	UINT m_uAliveCheckCount;
};

#endif // __RTSP_CLIENT_H__
