#include "Stdafx.h"
#include <stdio.h>
#include <stdlib.h>

#define S_ERR				-1
#define MARKER_BIT			1
#define	BIT_COUNT_INC(a)	nBitCount += a;
#define	BIT_COUNT_DEC(a)	nBitCount -= a;
#define GET_BITS(data,bits) GetBits(pData+nBitCount/8, nBitCount%8, bits);nBitCount+=bits
#define GET_BITS_AND_PRINT_AND_IF_ERROR_RETURN(data, bits, code, code_ref, hex_print) { \
	code=GET_BITS(data,bits); \
	if (code!=code_ref) { \
		BIT_COUNT_DEC(bits); \
		return S_ERR; \
	} \
}
#define GET_BITS_AND_PRINT_AND_IF_ERROR_NOT_RETURN(data, bits, code, code_ref, hex_print) { \
	code=GET_BITS(data,bits); \
	if (code!=code_ref) { \
		BIT_COUNT_DEC(bits); \
	} \
}
#define GET_BITS_AND_PRINT(data, bits, code, hex_print) { \
	code=GET_BITS(data,bits); \
}

int		g_nFrameSize;


#define VIDEO_OBJECT_START_CODE					0x100 // THROUGH 111F
#define VIDEO_OBJECT_LAYER_START_CODE			0x120 // THROUGH 112F
//RESERVED 130 THROUGH 1AF
#define VISUAL_OBJECT_SEQUENCE_START_CODE		0x1B0
#define VISUAL_OBJECT_SEQUENCE_END_CODE			0x1B1
#define USER_DATA_START_CODE					0x1B2
#define GROUP_OF_VOP_START_CODE					0x1B3
#define VIDEO_SESSION_ERROR_CODE				0x1B4
#define VISUAL_OBJECT_START_CODE				0x1B5
#define VOP_START_CODE							0x1B6
//RESERVED 1B7-1B9
#define FBA_OBJECT_START_CODE					0x1BA
#define FBA_OBJECT_PLANE_START_CODE				0x1BB
#define MESH_OBJECT_START_CODE					0x1BC
#define MESH_OBJECT_PLANE_START_CODE			0x1BD
#define STILL_TEXTURE_OBJECT_START_CODE			0x1BE
#define TEXTURE_SPATIAL_LAYER_START_CODE		0x1BF
#define TEXTURE_SNR_LAYER_START_CODE			0x1C0
#define TEXTURE_TILE_START_CODE					0x1C1
#define TEXTURE_SHAPE_LAYER_START_CODE			0x1C2
#define STUFFING_START_CODE						0x1C3
//RESERVED 1C4-1C5
//SYSTEM START CODES (SEE NOTE) 1C6 THROUGH 1FF


int GetBits(unsigned char *pData, unsigned int uSkipBits, unsigned int uBits)
{
	if (pData==NULL || uBits==0) {
		return 0;
	}

	int nByteCnt=0;
	int	nReadData=0;
	int nSkipData=0xffffffff;
	unsigned char cData;
	if (uSkipBits) {
		uBits += uSkipBits;
	}
	for (unsigned int i=0; i<uBits; i++) {
		cData = *(pData+nByteCnt);
		nReadData |= (cData>>(7-(i%8)))<<(uBits-1-i);
		if (uSkipBits) {
			if (i<uSkipBits) {
				nSkipData &= ~(1<<(uBits-1-i));
			}
			nReadData = nReadData&nSkipData;
		} 
		if ((i%8)==7) {
			nByteCnt++;
		}
	}

	return nReadData;
}

int NeedBits(unsigned int uValue)
{
	unsigned int	uMaxBit=0;

	for (unsigned int i=0; i<sizeof(unsigned int)*8; i++) {
		if (uValue & (1<<i)) {
			uMaxBit = i;
		}
	}
	return (uMaxBit+1);
}

int InStartCode(unsigned int uCode)
{
	if (uCode>=0x100&&uCode<=0x12F) {
		return 1;
	}
	if (uCode>=0x1B0&&uCode<=0x1B6) {
		return 1;
	}
	if (uCode>=0x1BA&&uCode<=0x1C3) {
		return 1;
	}
	return 0;
}

int NextBits(unsigned char* pData, unsigned int uSkipBits, unsigned int uFindData, unsigned int uFindSize)
{
	int		nBitCount=uSkipBits;
	unsigned int	uData;

	uData = GET_BITS(pData, uFindSize);
	while (uData!=uFindData) {
		BIT_COUNT_DEC(32);
		BIT_COUNT_INC(1);

		if (nBitCount>=g_nFrameSize) {
			return S_ERR;				
		}

		uData = GET_BITS(pData, uFindSize);
	}

	return nBitCount;
}

int NextStartCode(unsigned char* pData, unsigned int uSkipBits, unsigned int* puStartCode)
{
	int		nBitCount=uSkipBits;
	unsigned int	uNextStartCode;

	uNextStartCode = GET_BITS(pData, 32);
	while (!InStartCode(uNextStartCode)) {
		BIT_COUNT_DEC(32);
		BIT_COUNT_INC(1);

		if (nBitCount>=g_nFrameSize) {
			return S_ERR;				
		}

		uNextStartCode = GET_BITS(pData, 32);
	}
	*puStartCode = uNextStartCode;

	return nBitCount;
} 

int _UserData(unsigned char* pData, unsigned int uSkipBits)
{
	int		nBitCount=uSkipBits;
	unsigned int	uUserDataStartCode;
	unsigned int	uNextBits;

	uUserDataStartCode = GET_BITS(pData, 32);
	if (uUserDataStartCode!=USER_DATA_START_CODE) {
		BIT_COUNT_DEC(32);
		return S_ERR;
	}

	GET_BITS_AND_PRINT(pData,24,uNextBits,1);
	while (uNextBits!=0x000001) {
		int nUserData;
		GET_BITS_AND_PRINT(pData,8,nUserData,1);
		GET_BITS_AND_PRINT(pData,24,uNextBits,1);
	}
	BIT_COUNT_DEC(24);

	return nBitCount;
}

int _VideoSignalType(unsigned char* pData, unsigned int uSkipBits)
{
	int		nBitCount=uSkipBits; 
	unsigned int	uVideoSignalType;
	unsigned int	uVideoFormat;
	unsigned int	uVideoRange;
	unsigned int	uColourDescription;
	unsigned int	uColourPrimaries;
	unsigned int	uTransferCharacteristics;
	unsigned int	uMatrixCoefficients;

	GET_BITS_AND_PRINT(pData,1,uVideoSignalType,0);
	if (uVideoSignalType) {
		GET_BITS_AND_PRINT(pData,3,uVideoFormat,1);
		GET_BITS_AND_PRINT(pData,1,uVideoRange,1);
		GET_BITS_AND_PRINT(pData,1,uColourDescription,1);
		if (uColourDescription) {
			GET_BITS_AND_PRINT(pData,8,uColourPrimaries,1);
			GET_BITS_AND_PRINT(pData,8,uTransferCharacteristics,1);
			GET_BITS_AND_PRINT(pData,8,uMatrixCoefficients,1);
		}
	}

	return nBitCount;
}

int _VideoObjectLayer(unsigned char* pData, unsigned int uSkipBits, unsigned int *puWidth, unsigned int *puHeight)
{
	int		nBitCount=uSkipBits;
	unsigned int	uShortVideoHeader;
	unsigned int	uVideoObjectLayerStartCode;
	unsigned int	uRandomAccessibleVol;
	unsigned int	uVideoObjectTypeIndication;
	unsigned int	uIsObjectLayerIdentifier;
	unsigned int	uVideoObjectLayerVerid = 0;
	unsigned int	uVideoObjectLayerPriority;
	unsigned int	uAspectRatioInfo;
	unsigned int	uParWidth;
	unsigned int	uParHeight;
	unsigned int	uVolControlParameters;
	unsigned int	uChromaFormat;
	unsigned int	uLowDelay;
	unsigned int	uVbvParameters;
	unsigned int	uMarkerBit;
	unsigned int	uFirstHalfBitRate;
	unsigned int	uLatterHalfBitRate;
	unsigned int	uFirstHalfVbvBufferSize;
	unsigned int	uLatterHalfVbvBufferSize;
	unsigned int	uFirstHalfVbvOccupancy;
	unsigned int	uLatterHalfVbvOccupancy; 
	unsigned int	uVideoObjectLayerShape;
	unsigned int	uVideoObjectLayerShapeExtension;
	unsigned int	uVopTimeIncrementResolution;
	unsigned int	uFixedVopRate;
	unsigned int	uFixedVopTimeIncrement;
	unsigned int	uVideoObjectLayerWidth;
	unsigned int	uVideoObjectLayerHeight;
	unsigned int	uInterlaced;
	unsigned int	uObmcDisable;

	nBitCount = NextBits(pData+nBitCount/8, nBitCount%8, VIDEO_OBJECT_LAYER_START_CODE, 32);
	if (nBitCount>0) { // next_bits()==video_object_layer_start_code
		BIT_COUNT_DEC(32);
		uShortVideoHeader = 0;
		GET_BITS_AND_PRINT_AND_IF_ERROR_RETURN(pData,32,uVideoObjectLayerStartCode,VIDEO_OBJECT_LAYER_START_CODE,1);
		GET_BITS_AND_PRINT(pData,1,uRandomAccessibleVol,1);
		GET_BITS_AND_PRINT(pData,8,uVideoObjectTypeIndication,1);
		GET_BITS_AND_PRINT(pData,1,uIsObjectLayerIdentifier,0);
		if (uIsObjectLayerIdentifier) {
			GET_BITS_AND_PRINT(pData,4,uVideoObjectLayerVerid,1);
			GET_BITS_AND_PRINT(pData,3,uVideoObjectLayerPriority,1);
		}
		/* 
		aspect_ratio_info pixel aspect ratios
		0000 Forbidden
		0001 1:1 (Square)
		0010 12:11 (625-type for 4:3 picture)
		0011 10:11 (525-type for 4:3 picture)
		0100 16:11 (625-type stretched for 16:9 picture)
		0101 40:33 (525-type stretched for 16:9 picture)
		0110-1110 Reserved
		1111 extended PAR 
		*/
		enum AspectRatioInfo {
			ARI_Forbidden = 0x00,
			ARI_Square = 0x01,
			ARI_625For43 = 0x02,
			ARI_525For43 = 0x03,
			ARI_625For169 = 0x04,
			ARI_525For169 = 0x05,
			ARI_ExtendedPAR = 0x0F
		};
		GET_BITS_AND_PRINT(pData,4,uAspectRatioInfo,1);
		if (uAspectRatioInfo==ARI_ExtendedPAR) {
			GET_BITS_AND_PRINT(pData,8,uParWidth,0);
			GET_BITS_AND_PRINT(pData,8,uParHeight,0);
		}
		GET_BITS_AND_PRINT(pData,1,uVolControlParameters,1);
		if (uVolControlParameters) {
			GET_BITS_AND_PRINT(pData,2,uChromaFormat,1);
			GET_BITS_AND_PRINT(pData,1,uLowDelay,1);
			GET_BITS_AND_PRINT(pData,1,uVbvParameters,1);
			if (uVbvParameters) {
				GET_BITS_AND_PRINT(pData,15,uFirstHalfBitRate,1);
				GET_BITS_AND_PRINT_AND_IF_ERROR_RETURN(pData,1,uMarkerBit,MARKER_BIT,0);
				GET_BITS_AND_PRINT(pData,15,uLatterHalfBitRate,1);
				GET_BITS_AND_PRINT_AND_IF_ERROR_RETURN(pData,1,uMarkerBit,MARKER_BIT,0);
				GET_BITS_AND_PRINT(pData,15,uFirstHalfVbvBufferSize,1);
				GET_BITS_AND_PRINT_AND_IF_ERROR_RETURN(pData,1,uMarkerBit,MARKER_BIT,0);
				GET_BITS_AND_PRINT(pData,3,uLatterHalfVbvBufferSize,1);
				GET_BITS_AND_PRINT(pData,11,uFirstHalfVbvOccupancy,1);
				GET_BITS_AND_PRINT_AND_IF_ERROR_RETURN(pData,1,uMarkerBit,MARKER_BIT,0);
				GET_BITS_AND_PRINT(pData,15,uLatterHalfVbvOccupancy,1);
				GET_BITS_AND_PRINT_AND_IF_ERROR_RETURN(pData,1,uMarkerBit,MARKER_BIT,0);
			}

		}
		/* 
		Video Object Layer shape type
		Shape format Meaning
		00 rectangular
		01 binary
		10 binary only
		11 grayscale 
		*/
		enum VideoObjectLayerShape {
			VOLS_Rectangular = 0x00,
			VOLS_Binary = 0x01,
			VOLS_BinaryOnly = 0x02,
			VOLS_Grayscale = 0x03
		};
		GET_BITS_AND_PRINT(pData,2,uVideoObjectLayerShape,1);
		if (uVideoObjectLayerShape==VOLS_Grayscale&&uVideoObjectLayerVerid!=0x01) {
			GET_BITS_AND_PRINT(pData,4,uVideoObjectLayerShapeExtension,1);
		}
		GET_BITS_AND_PRINT_AND_IF_ERROR_RETURN(pData,1,uMarkerBit,MARKER_BIT,0);
		GET_BITS_AND_PRINT(pData,16,uVopTimeIncrementResolution,1);
		GET_BITS_AND_PRINT_AND_IF_ERROR_RETURN(pData,1,uMarkerBit,MARKER_BIT,0);
		GET_BITS_AND_PRINT(pData,1,uFixedVopRate,1);
		if (uFixedVopRate) {
			//1~16bit
			unsigned int uNeedBits;
			uNeedBits=NeedBits(uVopTimeIncrementResolution);
			GET_BITS_AND_PRINT(pData,uNeedBits,uFixedVopTimeIncrement,1);
		}
		if (uVideoObjectLayerShape!=VOLS_BinaryOnly) {
			if (uVideoObjectLayerShape==VOLS_Rectangular) {
				GET_BITS_AND_PRINT_AND_IF_ERROR_RETURN(pData,1,uMarkerBit,MARKER_BIT,0);
				GET_BITS_AND_PRINT(pData,13,uVideoObjectLayerWidth,0);
				GET_BITS_AND_PRINT_AND_IF_ERROR_RETURN(pData,1,uMarkerBit,MARKER_BIT,0);
				GET_BITS_AND_PRINT(pData,13,uVideoObjectLayerHeight,0);
				GET_BITS_AND_PRINT_AND_IF_ERROR_RETURN(pData,1,uMarkerBit,MARKER_BIT,0);
				*puWidth = uVideoObjectLayerWidth;
				*puHeight = uVideoObjectLayerHeight;
			}
			GET_BITS_AND_PRINT(pData,1,uInterlaced,1);
			GET_BITS_AND_PRINT(pData,1,uObmcDisable,1);
			/*
			ISO/IEC_14496-2_2001(E)-Character_PDF_document.pdf
			Page 36

			...

			VideoObjectPlane()
			에서 VOP로부터 Image Width,Height를 구하는 루틴을 구현하자.
			*/

		}


	}
	else {
		return S_ERR;
	}

	return nBitCount;
}

int _VisualObject(unsigned char* pData, unsigned int uSkipBits, unsigned int *puWidth, unsigned int *puHeight)
{
	int		nBitCount=uSkipBits;
	unsigned int	uUserDataStartCode;
	unsigned int	uVisualObjectStartCode;
	unsigned int	uIsVisualObjectIdentifier;
	unsigned int	uVisualObjectVerid;
	unsigned int	uVisualObjectPriority;
	unsigned int	uVisualObjectType;
	unsigned int	uVideoObjectStartCode;
	unsigned int	uNextBits;
	//unsigned int	uNextStartCode;

	GET_BITS_AND_PRINT(pData,32,uVisualObjectStartCode,1);
	GET_BITS_AND_PRINT(pData,1,uIsVisualObjectIdentifier,0);
	if (uIsVisualObjectIdentifier) {
		GET_BITS_AND_PRINT(pData,4,uVisualObjectVerid,1);
		GET_BITS_AND_PRINT(pData,3,uVisualObjectPriority,1);
	}

	/*
	Meaning of visual object type
	code 	visual object type
	0000 	reserved
	0001 	video ID
	0010 	still texture ID
	0011 	mesh ID
	0100 	FBA ID
	0101 	3D mesh ID
	01101 	reserved
	: :
	: :
	1111 	reserved
	*/
	enum VisualObjectType {
		VOT_VideoID = 0x01,
		VOT_StillTextureID = 0x02,
		VOT_MeshID = 0x03,
		VOT_FBAID = 0x04,
		VOT_3DMeshID = 0x05
	};
	GET_BITS_AND_PRINT(pData,4,uVisualObjectType,1);
	if (uVisualObjectType==VOT_VideoID||uVisualObjectType==VOT_StillTextureID) {
		nBitCount = _VideoSignalType(pData+nBitCount/8, nBitCount%8);
	}

	// next_start_code()
	// uNextStartCode 는 사용되지 않음.
	/*nBitCount = NextStartCode(pData+nBitCount/8, nBitCount%8, &uNextStartCode);
	if (nBitCount<0) {
		return S_ERR;
	}
	BIT_COUNT_DEC(32);*/

	GET_BITS_AND_PRINT_AND_IF_ERROR_NOT_RETURN(pData,32,uUserDataStartCode,USER_DATA_START_CODE,1);
	while (uUserDataStartCode==USER_DATA_START_CODE) {
		nBitCount = _UserData(pData+nBitCount/8, nBitCount%8);
		if (nBitCount==S_ERR) {
			return S_ERR;
		}
		GET_BITS_AND_PRINT_AND_IF_ERROR_NOT_RETURN(pData,32,uUserDataStartCode,USER_DATA_START_CODE,1);
	}

	if (uVisualObjectType==VOT_VideoID) {
		int i;
		for (i=0; i<128; i++) {
			uVideoObjectStartCode = GET_BITS(pData, i);
			if (uVideoObjectStartCode == VIDEO_OBJECT_START_CODE) {
				break;
			}
		}
		BIT_COUNT_DEC(i);
		GET_BITS_AND_PRINT_AND_IF_ERROR_RETURN(pData,i,uVideoObjectStartCode,VIDEO_OBJECT_START_CODE,1);
		nBitCount = _VideoObjectLayer(pData+nBitCount/8, nBitCount%8, puWidth, puHeight);
	}
	else if (uVisualObjectType==VOT_StillTextureID) {
		//_StilTextureObject();
	}
	else if (uVisualObjectType==VOT_MeshID) {
		//_MeshObject();
	}
	else if (uVisualObjectType==VOT_FBAID) {
		//_FBAObject();
	}
	else if (uVisualObjectType==VOT_3DMeshID) {
		//_3DMeshObject();
	}

	GET_BITS_AND_PRINT(pData,24,uNextBits,1);
	if (uNextBits!=0x000001) {
		//next_start_code();
	}

	return nBitCount;
}

UINT ParseMpeg4Vol(BYTE *pData, UINT uSkipBits, UINT *puWidth, UINT *puHeight)
{
	int		nBitCount=uSkipBits;
	unsigned int	uVisualObjectSequenceStartCode;
	//unsigned int	uVisualObjectSequenceEndCode;
	unsigned int	uProfileAndLevelIndication;
	unsigned int	uUserDataStartCode;

	//do {
	GET_BITS_AND_PRINT_AND_IF_ERROR_RETURN(pData,32,uVisualObjectSequenceStartCode,VISUAL_OBJECT_SEQUENCE_START_CODE,1);
	//if (uVisualObjectSequenceStartCode!=VISUAL_OBJECT_SEQUENCE_START_CODE) {
	//BIT_COUNT_DEC(32);
	//BIT_COUNT_INC(1);
	//	if (nBitCount>=g_nFrameSize) {
	//		return S_ERR;				
	//	}
	//	continue;
	//}
	GET_BITS_AND_PRINT(pData,8,uProfileAndLevelIndication,1);
	GET_BITS_AND_PRINT_AND_IF_ERROR_NOT_RETURN(pData,32,uUserDataStartCode,USER_DATA_START_CODE,1);
	while (uUserDataStartCode==USER_DATA_START_CODE) {
		nBitCount = _UserData(pData+nBitCount/8, nBitCount%8);
		if (nBitCount==S_ERR) {
			return S_ERR;
		}
		GET_BITS_AND_PRINT_AND_IF_ERROR_NOT_RETURN(pData,32,uUserDataStartCode,USER_DATA_START_CODE,1);
	}


	nBitCount = _VisualObject(pData+nBitCount/8, nBitCount%8, puWidth, puHeight);

	//uVisualObjectSequenceEndCode = GET_BITS(pData, 32);
	//if (uVisualObjectSequenceEndCode!=VISUAL_OBJECT_SEQUENCE_END_CODE) {
	//BIT_COUNT_DEC(32);
	//}

	//} while (uVisualObjectSequenceEndCode!=VISUAL_OBJECT_SEQUENCE_END_CODE);

	return nBitCount;
}
