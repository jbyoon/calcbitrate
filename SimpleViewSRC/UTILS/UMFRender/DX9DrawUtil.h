#ifndef __DX9_DRAW_UTILS_H__
#define __DX9_DRAW_UTILS_H__

#include "IUMFDrawUtil.h"
#include "D3DFont/d3dfont.h"
#include "DrawRecorder.h"

#define BRUSH_TEXTURESIZE			16.0f
class CDX9DrawUtil : public IUMFDrawUtil
{
public:
	enum { MAX_VERTEX	= 200*160*6};

	

	CDX9DrawUtil();
	~CDX9DrawUtil();
	
	BOOL			Init(IUMFRender *pUMFRender);
	void			Destroy();
	
	eUMFRenderType	GetType(){return UMF_DX9Render;}

	//Canvas Rect
	void SetCanvasRect(RECT CanvasRect){m_CanvasRect = CanvasRect;}
	SIZE GetCanvasSize(){
		SIZE CanvasSize = {m_CanvasRect.right - m_CanvasRect.left, m_CanvasRect.bottom - m_CanvasRect.top};
		return CanvasSize;
	}

	//Font 
	BOOL	InitFont(UINT index, const WCHAR* strFontName, DWORD dwHeight, DWORD dwFlags);
	BOOL	GetTextExtent(UINT index, const WCHAR* strText, SIZE* pSize );

	//Record Draw item
	DWORD	Push(UMFDRAWRECORD *pDrawItem){return m_DrawRecoder.Push(pDrawItem);}
	void	Clear(){ m_DrawRecoder.Clear();}

	//Image 
	IUMFImage*	CreateUMFImage(USHORT width, USHORT height, eUMF_COLORFORMAT colorformat);
	

	//Render 
	BOOL	Commit();

	void	DestroySelf(){delete this;}

	
private:

	BOOL	DrawRecord(UMFDRAWRECORD* pRecord);
	UINT	FillRectListVertex(RECT *pRc, UINT nRectsCnt, D3DCOLOR color);
	UINT	FillLineStripVertex(POINT *pPts, UINT nPtCnt, D3DCOLOR color);
	BOOL	DrawPolygonVCA(POINT *pPts, UINT nPtCnt, D3DCOLOR color, eUMFBRUSHTYPE BrushType);
	INT		TriangulateWrapper(POINT* pPts, UINT nPtCnt, D3DCOLOR color);
	BOOL	DrawPolygonCircle(POINT Center, double Radious, D3DCOLOR color);
	BOOL	DrawArc(POINT Center, double Radious, unsigned short Width, double StartRadius, double FinishRadius, D3DCOLOR color);
	BOOL	DrawTexture(RECT TargetRect, LPDIRECT3DTEXTURE9 pTexture);
	BOOL	DrawCurve(POINT *pPts, UINT nPtCnt, D3DCOLOR color);
// 	BOOL	InitBrush();
	BOOL	ReInitFont(UINT index);
	void	DeleteFont(UINT index);


	BOOL				m_bInit;
	BOOL				m_bReInit;
	CDX9Render*			m_pUMFRender;
	LPDIRECT3DDEVICE9	m_pd3dDevice;

	CD3DFont*			m_pFont[MAX_FONT_INDEX];
	RECT				m_CanvasRect;

	TCHAR				m_tcsFontName[128];
	
	LPDIRECT3DVERTEXBUFFER9 m_pRectVB;		// For CUSTOMVERTEX
	LPDIRECT3DVERTEXBUFFER9 m_pPolygonVB;	// For CUSTOMVERTEX1

	UINT m_nNextVertexDataRect;
	UINT m_nNextVertexData;
	
	IDirect3DTexture9*		m_pHatchedBrushTexture;
	
	CDrawRecorder		m_DrawRecoder;
};


#endif