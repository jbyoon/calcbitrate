#pragma once
#include "IUMFDrawUtil.h"

class CDrawRecorder
{
public:
	enum {	
		MAX_NUM_RECORDS		= 1024,
		MAX_TEXT_BUFFER		= 1024*20,
		MAX_POLYGON_BUFFER	= 512,
		MAX_LINEPOINTS_BUFFER = 1024*10
	};

	CDrawRecorder(){
		//m_pRecords						= new UMFDRAWRECORD[MAX_NUM_RECORDS];
		m_pTexts[0]						= new TCHAR[MAX_TEXT_BUFFER];
		m_pTexts[MAX_NUM_RECORDS]		= m_pTexts[0] + MAX_TEXT_BUFFER;
		m_pPolygonPoints[0]				= new POINT[MAX_POLYGON_BUFFER];
		m_pPolygonPoints[MAX_NUM_RECORDS]= m_pPolygonPoints[0] + MAX_POLYGON_BUFFER;
		m_pLinePoints[0]				= new POINT[MAX_LINEPOINTS_BUFFER];
		m_pLinePoints[MAX_NUM_RECORDS]	= m_pLinePoints[0] + MAX_LINEPOINTS_BUFFER;
		Clear();
	}

	~CDrawRecorder(){
		//if ( m_pRecords ) delete[] m_pRecords;
		if ( m_pTexts[0] ) delete[] m_pTexts[0];
		if ( m_pPolygonPoints[0] ) delete[] m_pPolygonPoints[0];
		if ( m_pLinePoints[0] ) delete[] m_pLinePoints[0];
	}
	

	DWORD	Push(UMFDRAWRECORD *pDrawItem){

		if (m_nNumRecords >= MAX_NUM_RECORDS){ return -1;}

		m_pRecords[m_nNumRecords] = *pDrawItem;

		// Some drawing types used pointers, we need to copy all these to the buffers
		switch ( pDrawItem->drawType )
		{
		case UMF_DRT_TEXT:
			{
				TCHAR *pTextNext = m_pTexts[m_nNumTexts] + lstrlen( pDrawItem->text.pText ) + 1; // do not forget the last '\0'

				// Check text buffer overflow
				if ( pTextNext > m_pTexts[MAX_NUM_RECORDS] ) return -1;
					
				// Copy text to the text buffer
				lstrcpy( m_pTexts[m_nNumTexts], pDrawItem->text.pText );
				// Change the text pointer inside saved record
				m_pRecords[m_nNumRecords].text.pText = m_pTexts[m_nNumTexts];
				// Set the text pointer for next one
				m_pTexts[m_nNumTexts+1] = pTextNext;	
				m_nNumTexts++;
			}
			break;

		case UMF_DRT_POLYGON:
			{
				POINT *pPolygonNext = m_pPolygonPoints[m_nNumPolygons] + pDrawItem->polygon.nPt;

				// Check polygon points buffer overflow
				if ( pPolygonNext > m_pPolygonPoints[MAX_NUM_RECORDS] ) return -1;

				// Copy points to the polygon points buffer
				memcpy( m_pPolygonPoints[m_nNumPolygons], pDrawItem->polygon.pPts, sizeof(POINT) * pDrawItem->polygon.nPt );
				// Change the pt pointer inside saved record
				m_pRecords[m_nNumRecords].polygon.pPts = m_pPolygonPoints[m_nNumPolygons];
				// Set the polygon points pointer for next one
				m_pPolygonPoints[m_nNumPolygons+1] = pPolygonNext;
				m_nNumPolygons++;
			}
			break;

		case UMF_DRT_LINESTRIP:
			{
				POINT *pLinePointsNext = m_pLinePoints[m_nNumLines] + pDrawItem->pts.nPt;

				// Check linepoints buffer overflow
				if ( pLinePointsNext > m_pLinePoints[MAX_NUM_RECORDS] ) return -1;
				
				// Copy points to the linepoints buffer
				memcpy( m_pLinePoints[m_nNumLines], pDrawItem->pts.pPts, sizeof(POINT) * pDrawItem->pts.nPt );
				// Change the pt pointer inside saved record
				m_pRecords[m_nNumRecords].pts.pPts = m_pLinePoints[m_nNumLines];
				// Set the linepoints pointer for next one
				m_pLinePoints[m_nNumLines+1] = pLinePointsNext;
				m_nNumLines++;
			}
			break;
		}
		m_nNumRecords++;
		return m_nNumRecords;
	}

	void	Clear(){
		m_nNumRecords	= 0;
		m_nNumPolygons	= 0;
		m_nNumTexts		= 0;
		m_nNumLines		= 0;
	}

	DWORD	GetRecordCnt(){return m_nNumRecords;}
	UMFDRAWRECORD* GetRecords(){return m_pRecords;}


private:
	//UMFDRAWRECORD		*m_pRecords;
	UMFDRAWRECORD		m_pRecords[MAX_NUM_RECORDS];
	TCHAR				*m_pTexts[MAX_NUM_RECORDS+1];
	POINT				*m_pPolygonPoints[MAX_NUM_RECORDS+1];
	POINT				*m_pLinePoints[MAX_NUM_RECORDS+1];

	INT					m_nNumTexts;
	INT					m_nNumPolygons;
	INT					m_nNumRecords;
	INT					m_nNumLines;
};
