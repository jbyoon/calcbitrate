#ifndef __UMF_RENDER_INTERFACE_H__
#define __UMF_RENDER_INTERFACE_H__

#include "UMFBase.h"

typedef enum{
	UMF_DX9Render = 1
}eUMFRenderType;


typedef enum{
	UMFRenderChangeClientSIZE = 1,
	UMFRenderChangeMonitor	= 2,
	UMFRenderClearDevice	= 3,
}eUMFRenderParam;


class IUMFRender
{
public:
	
	virtual BOOL			Open(HWND hWnd) = 0;
	virtual void			Close() = 0;
	virtual eUMFRenderType	GetType() = 0;

	//Return ImageBuffer index
	virtual DWORD	CreateImageBuf(DWORD width, DWORD height, eUMF_COLORFORMAT SurfaceColorFormat) = 0;
	//Destory
	virtual void	DestroyImageBuf(DWORD ImageBufIndex) = 0;

	virtual HRESULT	DrawImage(DWORD ImageBufIndex, PBYTE pImage, eUMF_COLORFORMAT ImageColorFormat, RECT TargetRect) = 0;

	virtual UINT	GetImageSize(DWORD EndId, DWORD ImageBufIndex, eUMF_COLORFORMAT Imageformat) = 0;
	
	virtual HRESULT	Present(RECT ClientRect) = 0;
	virtual HRESULT	Present(RECT srcRect, RECT destRect) = 0;

	virtual BOOL	GetParam(DWORD type, void *unKnown) = 0;
	virtual BOOL	SetParam(DWORD type, void *unKnown) = 0;

	virtual void	DestroySelf() = 0;
};

extern "C" IUMFRender* WINAPI CreateUMFRender(eUMFRenderType type);

#endif