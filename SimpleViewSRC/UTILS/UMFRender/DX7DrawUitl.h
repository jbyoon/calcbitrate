#ifndef __DX7_DRAW_UTILS_H__
#define __DX7_DRAW_UTILS_H__

#include "UMFDrawUtil.h"

class CDX7Render;

class CDX7DrawUitl : public CUMFDrawUtil
{
public:
	DX7DrawUitl();
	~DX7DrawUitl();
	
	BOOL Init(IUMFRender *pUMFRender);

	//Font 
	BOOL SetFontSize(int size);
	
	//Render 
	BOOL Commit();

private:

	BOOL	InitBrush();
	BOOL	InitFont();

	BOOL			m_bInit;
	RECT			m_CanvasRect;
	CDX7Render*		m_pDX9Render;
	CDrawRecoder*	m_pDrawRecorder;
};


#endif