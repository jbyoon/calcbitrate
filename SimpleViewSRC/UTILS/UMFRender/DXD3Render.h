#pragma once

#include "d3d.h"
#include "UMFStructures.h"

typedef enum SurfaceType{
	SURFACETYPE_INVALID=0,
	SURFACETYPE_AGPTEXTURE=1,
	SURFACETYPE_VIDMEMTEXTURE=2,
	SURFACETYPE_3DCANVAS=3,
	SURFACETYPE_PRIMARY=4,
	SURFACETYPE_MANAGEDTEXTURE=5,
};

typedef struct 
{
	DWORD		width;
	DWORD		height;
	UMF_COLORFORMAT format;
	LPDIRECTDRAWSURFACE7	pddsImageBuffer;
	DWORD		ref_count;
}IMAGEBUFFER;


UMF_COLORFORMAT GetColorFormat3DSupportFromBitInfoHeader(BITMAPINFOHEADER *pbm);

class CDXD3Render
{

public:
	enum {MAX_IMAGEBUFFER = 16};

	CDXD3Render(void);
	~CDXD3Render(void);

	BOOL	Create();
	void	Destroy();

	//Return ImageBuffer index
	DWORD	CreateImageBuf(DWORD width, DWORD height, UMF_COLORFORMAT SurfaceColorFormat);
	//Destory
	void	DestroyImageBuf(DWORD ImageBufIndex);

	BOOL	Recreate();
	void	SetClipperWnd(HWND hwnd){if(m_pcClipper) m_pcClipper->SetHWnd(0, hwnd); m_hWnd = hwnd;}
	LPDIRECTDRAWSURFACE7	GetD3BackBuf(){return m_pddsBackBuffer;}
	UINT					GetImageSize(DWORD EndId, DWORD ImageBufIndex, UMF_COLORFORMAT Imageformat);

	HRESULT	DrawImage(DWORD ImageBufIndex, PBYTE pImage, UMF_COLORFORMAT ImageColorFormat, RECT TargetRect);
	
	HRESULT	Present(RECT ClientRect);
	HRESULT	Present(RECT srcRect, RECT destRect);

private:

	BOOL					m_bCreate;
	HWND					m_hWnd;
	LPDIRECTDRAW7			m_pDD;
	LPDIRECTDRAWSURFACE7	m_pddsPrimary;
	LPDIRECTDRAWCLIPPER		m_pcClipper;
	LPDIRECTDRAWSURFACE7	m_pddsBackBuffer;
	LPDIRECT3DDEVICE7		m_pd3dDevice;

	DWORD					m_TotalImageBufCnt;
	IMAGEBUFFER				m_ImageBuf[MAX_IMAGEBUFFER];
	
	void InitColorConversionFunctions();
	BOOL ConvertColorFormat( BYTE* pSrc, UMF_COLORFORMAT CFSource, SIZE szSource, BYTE* pDst, UMF_COLORFORMAT CFDest, DWORD dwDstPitch, DWORD dwScale_factor);

	BOOL	AdapterEnumCallback( GUID* pGUID, LPSTR strDesc,LPSTR strName, HMONITOR hmonitor);
	static BOOL WINAPI AdapterEnumCallbackWrapper( GUID* pGUID, LPSTR strDesc,LPSTR strName, VOID* lpContext, HMONITOR hmonitor);
};

