#pragma once

#include "UMFBase.h"

class IUMFRender;

typedef struct{
	DWORD		EngId;
	RECT		TargetRect;
}VIEWINFO;

typedef struct{
	DWORD		ViewIndex;
	eUMF_COLORFORMAT	ImageColorFormat;
	DWORD		BufIndex;
}VIEWENGINFO;


class CMultiViewRender
{
public:
	enum {UNDEFINED_INDEX = 0xFFFFFFFF, MAX_RENDER = 16};
	typedef enum {VIEW_1, VIEW_4, VIEW_9, VIEW_16} VIEW_MODE;

	CMultiViewRender(void);
	~CMultiViewRender(void);

	BOOL	Setup(HWND hWnd, UINT m_MaxEng = MAX_RENDER);
	void	Endup();

	void	SetViewMode(VIEW_MODE mode){if(m_bSetup){m_CurViewMode = mode;Split();DrawBackGround();}}
	BOOL	SetViewIndexToEngId(DWORD ViewIndex, DWORD EngId);

	VIEW_MODE GetViewMode(){return m_CurViewMode;}
	UINT	  GetImageSize(DWORD EngId);

	void	OnChangeClientRect(RECT *pRect = NULL);
//	void	OnChangeMonitorResolution();

	BOOL	ChangeVCASourceInfo(DWORD EngId, DWORD width, DWORD height, eUMF_COLORFORMAT ColorFormat);
	BOOL	ChangeVCASourceInfo(DWORD EngId, BITMAPINFOHEADER *pbm);

	BOOL	RenderImage(DWORD EngId, BYTE *pImage);
	BOOL	Present(DWORD EngId);

	void	OnEraseBackGround();
	
	VIEWINFO*		GetViewInfoByIndex(DWORD ViewIndex);
	VIEWINFO*		GetViewInfoByEngId(DWORD EngId);
	VIEWENGINFO*	GetViewEngineInfo(DWORD EngId);
	
	IUMFRender*	GetUMFRender(){return m_pUmfRender;}

private:

	BOOL			m_bSetup;
	HWND			m_hWnd;
	UINT			m_MaxEng;
	UINT			m_MaxViewIndex;
	
	VIEW_MODE		m_CurViewMode;
	RECT			m_ClientRect;
	RECT			m_ScreenRect;
	void			Split();	
	void			DrawBackGround();
	
	IUMFRender*		m_pUmfRender;
		
	VIEWINFO		m_ViewInfo[MAX_RENDER];	
	VIEWENGINFO		m_ViewEngInfo[MAX_RENDER];	

	void			DestroyViewEngInfo(DWORD EngId);
	
	CRITICAL_SECTION m_CS;
};

DWORD GetMaxIndexChannel(CMultiViewRender::VIEW_MODE mode);