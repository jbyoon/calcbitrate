#ifndef __UMF_DRAW_UTILS_H__
#define __UMF_DRAW_UTILS_H__

typedef enum eUMFBRUSHTYPE
{
	BRT_SOLID		= 0,
	BRT_HATCH,
};

class IUMFRender;
class CDrawRecorder;

class CUMFDrawUtil
{
public:
	CUMFDrawUtil();
	~CUMFDrawUtil();
	
	virtual BOOL Init(IUMFRender *pUMFRender);

	virtual void SetCanvasRect(RECT CanvasRect){m_CanvasRect = CanvasRect;}
	virtual SIZE GetCanvasSize(){
		SIZE CanvasSize = {m_CanvasRect.right - m_CanvasRect.left, m_CanvasRect.bottom - m_CanvasRect.top};
		return CanvasSize;
	}

	virtual BOOL DrawRect(RECT* pDstRect, COLORREF Color, BYTE Alpha, eUMFBRUSHTYPE BrushType);
	virtual BOOL DrawRectList(INT nRectCnt, RECT* pDstRects, COLORREF Color, BYTE Alpha, eUMFBRUSHTYPE BrushType);
	virtual BOOL DrawPolygon(INT PtCnt, POINT *pPoints, COLORREF Color, BYTE Alpha, eUMFBRUSHTYPE BrushType);
	virtual BOOL DrawLineList(INT PtCnt, POINT *pPoints, COLORREF Color, BYTE Alpha);
	
	virtual BOOL DrawCircle(POINT Center, double Radious, BOOL bFill, COLORREF Color, BYTE Alpha);
	virtual BOOL DrawCircularArc(POINT Center, double Radious, unsigned short Width, double StartRadius, double FinishRadius, COLORREF Color, BYTE Alpha);

	//Font 
	virtual BOOL SetFontSize(int size) { return FALSE; }
	virtual BOOL DrawText(POINT Start, TCHAR* szText, COLORREF Color, BYTE Alpha);
	virtual BOOL GetTextExtent( const TCHAR* strText, SIZE* pSize ) = 0;

	//Render 
	virtual BOOL Commit() { return FALSE; }

protected:
	RECT			m_CanvasRect;
	IUMFRender*		m_pUMFRender;
	CDrawRecorder*	m_pDrawRecorder;
};


#endif