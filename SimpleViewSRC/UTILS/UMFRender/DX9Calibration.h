#ifndef __DX9_CALIBRATION_H__
#define __DX9_CALIBRATION_H__

#include <d3d9.h>
#include <d3dx9.h>

typedef struct
{
//	CD3DFile* pFileObject;
	D3DXMATRIX matObjectModel;

	D3DXVECTOR3 vecRot;
	D3DXVECTOR3 vecPos;
	D3DXVECTOR3 vecPickPos;
	D3DXVECTOR2 vecLogicalScreenPos;

	bool		bDragging;
	bool		bHighlighted;
	float		fScale;

	float		fMaxZ, fMinZ;
	float		fMaxX, fMinX;
	float		fMaxY, fMinY;

}ObjectModel;

#define MAXNUM_OBJECTS 6
#define NUM_COLOURS 6


typedef struct {
	double fHeight;
	double fTilt;
	double fRoll;
	double fFOV;
	double fPan;
} CAMERA_CALIB_INFO;

typedef void(*_calib_callback_fxn)(void* /* _usr_info */, CAMERA_CALIB_INFO );

struct CUSTOMVERTEX {
	FLOAT x, y, z;
	FLOAT nx, ny, nz;
	FLOAT tu, tv;
};
#define D3DFVF_CUSTOMVERTEX (D3DFVF_XYZ|D3DFVF_NORMAL|D3DFVF_TEX1)

struct CUSTOMVERTEX1 {
	FLOAT x, y, z;
	D3DCOLOR color;
	D3DCOLOR specular;
	FLOAT tu, tv;

};
#define D3DFVF_CUSTOMVERTEX1 (D3DFVF_XYZ|D3DFVF_DIFFUSE|D3DFVF_SPECULAR|D3DFVF_TEX1)

#define	 HUMAN_HEIGHT	1.8f;

#define	_MAXWIDTH_D3DCANVAS			(1024)		// 0	current display mode resolution
#define	_MAXHEIGHT_D3DCANVAS		(1024)		// 0	current display mode resolution

class CDX9Calibration
{
public:
	CDX9Calibration();
	~CDX9Calibration();
	
	BOOL	Init(LPDIRECT3DDEVICE9 pd3dDevice, RECT ClientRect);
	void	Close();
	void	OnChangeClientRect(RECT ClientRect);
	void	SetInitialCameraParams(CAMERA_CALIB_INFO& calibInfo);
	void	SetCameraParams(CAMERA_CALIB_INFO& calibInfo, BOOL bCentre);
	void	RenderCalibrationGrid();
	void	RegisterCalibInfoCallback(_calib_callback_fxn, void* _usr_info);

	void	MouseOver(POINT screenCoord);
	void	PickGrid(POINT screenCoord);
	void	DragGrid(POINT screenCoord);
	void	EndGridDrag();
	void	BeginRuler(POINT screenCoord);
	void	EndRuler();
	void	ZoomGrid(POINT screenCoord, long zDelta);

	void	InsertObjectModel(POINT screenCoord);

	void	EnableTransparentGrid(BOOL bEnable);
	void	EnableCalibDrawSky(BOOL bEnable);

	float	GetRulerLength();

	void	SetCanvasRect(RECT CanvasRect){m_CanvasRect = CanvasRect;}
	SIZE	GetCanvasSize(){
		SIZE CanvasSize = {m_CanvasRect.right - m_CanvasRect.left, m_CanvasRect.bottom - m_CanvasRect.top};
		return CanvasSize;
	}
	
private:
	void	InitCalibInfoVars();
	BOOL	InitDrawCalib();
	BOOL	InitGroundVertexTexture();
	void	DrawGroundPlane();
	void	DrawPerson(int iModelIdx);
	void	SetupMatricies();
	void	SetupModelMatrix(int iModelIdx);
	void	HoldModelPositions_Begin();
	void	HoldModelPositions_End();

	D3DXVECTOR3		UnprojectXplane(POINT screenCoord, int modelIdx = -1, float fXPlane = 0.0f, float *pfDist2Plane = NULL);
	D3DXVECTOR3		UnprojectYplane(POINT screenCoord, int modelIdx = -1, float fYPlane = 0.0f, float *pfDist2Plane = NULL);
	D3DXVECTOR3		UnprojectZplane(POINT screenCoord, int modelIdx = -1, float fZPlane = 0.0f, float *pfDist2Plane = NULL);
	D3DXVECTOR3		UnprojectZplane(D3DXVECTOR3 logicalCoord, int modelIdx = -1, float fZPlane = 0.0f, float *pfDist2Plane = NULL);
	BOOL			_insertObjectModel(D3DXVECTOR3 pos);

	ObjectModel*	PickObject(POINT screenCoord, D3DXVECTOR3 *pPickedPos);
	void			ClipCoords(POINT &screenCoord);

	BOOL	InitRulerVertexTexture();
	void	DrawRuler();

	BOOL	InitHorizonVertex();

	void	UpdateCalibInfo();	


private:
	BOOL			m_bInited;
	LPDIRECT3DDEVICE9 m_pd3dDevice;
	SIZE			m_csVideoSize; /// native video dimensions
	RECT			m_ClientRect;
	RECT			m_CanvasRect;
	BOOL			m_bTransparentGrid;
	BOOL			m_bSkyEnabled;

	CAMERA_CALIB_INFO	m_CalibInfo;

	ID3DXMesh		*m_pSphereMesh;
	ID3DXMesh		*m_pCylinderMesh;
	ID3DXMesh		*m_pConeMesh;
	D3DMATERIAL9	m_materialPerson[MAXNUM_OBJECTS];

	float			m_fHumanHeight;

	LPDIRECT3DVERTEXBUFFER9 m_pGroundVB;
	IDirect3DTexture9*		m_pGroundTexture;
	D3DLIGHT9				m_light;

	LPDIRECT3DVERTEXBUFFER9 m_pRulerVB;
	IDirect3DTexture9*		m_pRulerTexture;
	BOOL					m_bRulerEnabled;
	D3DXVECTOR3				m_vRulerStartPos, m_vRulerEndPos;

	LPDIRECT3DVERTEXBUFFER9 m_pHorizonVB;

	ObjectModel m_objectModels[MAXNUM_OBJECTS];
	int			m_iNumObjects;

	static D3DCOLORVALUE	m_colourLUT[NUM_COLOURS];
	D3DXMATRIX	m_matView;
	D3DXMATRIX	m_matView_noTrans;
	D3DXMATRIX	m_matProjModel, m_matProjModel_Clipped;

	D3DXVECTOR3 m_vecGridRot;
	BOOL		m_bParamsSet;

	BOOL		m_bTiltingGrid;
	POINT		m_tiltStartPos;

	_calib_callback_fxn m_calib_callback_fxn;
	void*				m_callback_usr_info;
};


#endif // __DX9_CALIBRATION_H__