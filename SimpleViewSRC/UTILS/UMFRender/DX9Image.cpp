#include "StdAfx.h"
#include "DX9Render.h"
#include "DX9Image.h"
#include "UMFColorFormat.h"



#define	 INITGUID
#include <guiddef.h>
DEFINE_GUID(IID_IDirect3DTexture9, 0x85c31227, 0x3de5, 0x4f00, 0x9b, 0x3a, 0xf1, 0x1a, 0xc3, 0x8c, 0x18, 0xb5);



#define CHECK_INIT_RETUNR_FALSE() if(!m_bInit){\
	UMFTrace(UMF_TRACE_LEVEL_WARNING, _T(" CDX9Image does not Init Before\n\n"));\
	return FALSE;\
} 

#define CHECK_INIT_RETUNR() if(!m_bInit){\
	UMFTrace(UMF_TRACE_LEVEL_WARNING, _T(" CDX9Image does not Init Before\n\n"));\
	return ;\
} 


CDX9Image::CDX9Image(void)
{
	m_bInit			= FALSE;
	m_usWidth		= 0;
	m_usHeight		= 0;
	m_ColorFormat	= UMF_NONE;
}


CDX9Image::~CDX9Image(void)
{
	if(m_bInit)Destroy();
}


BOOL	CDX9Image::Init(IUMFRender *pUMFRender, unsigned short width, unsigned short height, eUMF_COLORFORMAT colorformat)
{
	if(!pUMFRender){
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T(" CDX9Image::Init Fail pUMFRender not assigned \n\n"));
		return FALSE;
	}
	if(pUMFRender->GetType() != UMF_DX9Render){
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T(" CDX9Image::Init Fail Support DX9 Render \n"));
		return FALSE;
	}
	
	LPDIRECT3D9			pD3D;
	HRESULT		hr;

	CDX9Render *pDX9Render = (CDX9Render *)pUMFRender;
	pD3D		= pDX9Render->GetD3D();
	m_pd3dDevice	= pDX9Render->GetD3dDevice();

	if(m_bInit){
		UMFTrace(UMF_TRACE_LEVEL_VERBOSE, _T(" CDX9Image::Init before reinit\n"));
		Destroy();
	}
	
	D3DFORMAT format;
	D3DDISPLAYMODE d3ddm;
	memset(&d3ddm, 0 ,sizeof(D3DDISPLAYMODE));
	pD3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &d3ddm);

	format = Direct3DFindColorFormat(pD3D, colorformat, d3ddm.Format);
	if(D3DFMT_UNKNOWN == format){
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Can not create Colorformat[%d]\n"),colorformat);
		return FALSE;
	}

	//TODO.. D3DFORMAT to ColorFormat....
	LPDIRECT3DSURFACE9 pddsImageBuffer;
	hr = m_pd3dDevice->CreateOffscreenPlainSurface(width,height,format,D3DPOOL_DEFAULT, &pddsImageBuffer, NULL);
	if( FAILED(hr) ){
        UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Failed to create picture surface. (hr=0x%lx)"), hr);
        return FALSE;
    }

	hr = m_pd3dDevice->CreateTexture(width, height, 1, 
		D3DUSAGE_RENDERTARGET, format, D3DPOOL_DEFAULT ,&m_pTexture, NULL);
    if( FAILED(hr)){
        UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Failed to create texture. (hr=0x%lx)"), hr);
		pddsImageBuffer->Release();
        return FALSE;
    }

	m_usWidth		= width;
	m_usHeight		= height;
	m_ColorFormat	= colorformat;
	m_pddsImageBuf	= pddsImageBuffer;

	m_bInit			= TRUE;
	return m_bInit;
}


void	CDX9Image::Destroy()
{
	if(m_bInit){
		if(m_pddsImageBuf){
			m_pddsImageBuf->Release();
			m_pddsImageBuf=NULL;
		}
		m_ColorFormat	= UMF_NONE;
		m_bInit			= FALSE;
	}
}


HDC		CDX9Image::GetDC()
{
	CHECK_INIT_RETUNR_FALSE();
	
	HDC hdc;
	HRESULT hr;
	hr = m_pddsImageBuf->GetDC(&hdc);
	if( FAILED(hr) ){
        UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Failed can not Get DC . (hr=0x%lx)"), hr);
        return NULL;
    }

	return hdc;
}


void	CDX9Image::ReleaseDC(HDC hdc)
{
	CHECK_INIT_RETUNR();
	m_pddsImageBuf->ReleaseDC(hdc);
}


BYTE*	CDX9Image::GetBuf(DWORD *pPitch)
{
	CHECK_INIT_RETUNR_FALSE();

	D3DLOCKED_RECT D3DRect;
	HRESULT	hr;
	hr =  m_pddsImageBuf->LockRect(&D3DRect, NULL, 0);
    if( FAILED(hr) ){
        UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("%s:%d (hr=0x%0lX)"), __FUNCTION__, __LINE__, hr);
        return NULL;
    }

	*pPitch = D3DRect.Pitch;
	return (PBYTE)D3DRect.pBits; 
}


void	CDX9Image::ReleaseBuf()
{
	CHECK_INIT_RETUNR();
	m_pddsImageBuf->UnlockRect();
}


void	CDX9Image::Update()
{
	CHECK_INIT_RETUNR();

	HRESULT hr;
	LPDIRECT3DSURFACE9 pD3DTexSurf;
	RECT Rect = {0,0,	m_usWidth, m_usHeight};

	hr = m_pTexture->GetSurfaceLevel(0, &pD3DTexSurf);
    if( FAILED(hr) ){
        UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("%s:%d (hr=0x%0lX)"), __FUNCTION__, __LINE__, hr);
		return ;
    }
	
	hr = m_pd3dDevice->StretchRect(m_pddsImageBuf, &Rect, pD3DTexSurf, &Rect, D3DTEXF_LINEAR);
	if( FAILED(hr) ){
        UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Error StretchRect (hr=0x%0lX)\n"), hr);
		pD3DTexSurf->Release();
		return ;
    }

	pD3DTexSurf->Release();
}

