#pragma once

#include "IUMFDrawUtil.h"
#include "D3DFont/d3dfont.h"


class CDX9Image : public IUMFImage
{
public:
	CDX9Image();
	~CDX9Image();

	BOOL	Init(IUMFRender *pUMFRender, USHORT width, USHORT height, eUMF_COLORFORMAT Color);
	void	Destroy();
	
	eUMFRenderType		GetType()	{ return UMF_DX9Render;}
	eUMF_COLORFORMAT	GetColorFormat()	{ return m_ColorFormat;}

	HDC		GetDC();
	void	ReleaseDC(HDC hdc);

	BYTE*	GetBuf(DWORD *pPitch);
	void	ReleaseBuf();

	LPDIRECT3DSURFACE9	GetSurface(){return m_pddsImageBuf;}
	LPDIRECT3DTEXTURE9	GetTexture(){return m_pTexture;}
	void	Update();

	void	DestroySelf(){delete this;}
	
private:

	BOOL				m_bInit;
	LPDIRECT3DDEVICE9	m_pd3dDevice;
	LPDIRECT3DSURFACE9	m_pddsImageBuf;
	LPDIRECT3DTEXTURE9	m_pTexture;

	USHORT				m_usWidth;
	USHORT				m_usHeight;
	eUMF_COLORFORMAT	m_ColorFormat;

};
