#pragma once

#include <d3d9.h>
#include "IUMFRender.h"


typedef struct 
{
	DWORD		width;
	DWORD		height;
	eUMF_COLORFORMAT format;
	LPDIRECT3DSURFACE9 pddsImageBuffer;
	DWORD		ref_count;
}IMAGEBUFFER;


class CDX9Render : public IUMFRender
{

public:
	enum {MAX_IMAGEBUFFER = 16};

	CDX9Render(void);
	~CDX9Render(void);

	BOOL	Open(HWND hWnd);
	void	Close();

	eUMFRenderType	GetType(){return UMF_DX9Render;}


	//Return ImageBuffer index
	DWORD	CreateImageBuf(DWORD width, DWORD height, eUMF_COLORFORMAT SurfaceColorFormat);
	//Destory
	void	DestroyImageBuf(DWORD ImageBufIndex);

	LPDIRECT3DTEXTURE9	GetD3BackBuf(){return m_pMainTexture;}
	UINT				GetImageSize(DWORD EndId, DWORD ImageBufIndex, eUMF_COLORFORMAT Imageformat);

	HRESULT	DrawImage(DWORD ImageBufIndex, PBYTE pImage, eUMF_COLORFORMAT ImageColorFormat, RECT TargetRect);
	
	HRESULT	Present(RECT ClientRect);
	HRESULT	Present(RECT srcRect, RECT destRect);
	void	DestroySelf() { delete this; }

	LPDIRECT3DDEVICE9	GetD3dDevice() { return	m_pd3dDevice; }
	LPDIRECT3D9			GetD3D() { return m_pD3D; }

	BOOL	GetParam(DWORD type, void *unKnown){return FALSE;}
	BOOL	SetParam(DWORD type, void *unKnown);

private:
	BOOL					m_bCreate;
	HWND					m_hWnd;

	LPDIRECT3D9				m_pD3D;
	LPDIRECT3DDEVICE9       m_pd3dDevice; 
	
	LPDIRECT3DTEXTURE9      m_pMainTexture;
	LPDIRECT3DVERTEXBUFFER9	m_pMainVertex;
	D3DPRESENT_PARAMETERS	m_D3DPP;
	
	DWORD					m_TotalImageBufCnt;
	IMAGEBUFFER				m_ImageBuf[MAX_IMAGEBUFFER];

	BOOL					DrawImageTexture(RECT t,RECT s,DWORD color,float zValue);
	BOOL					ReCreateDevice(DWORD width, DWORD height);
	BOOL					Clear();
};

D3DFORMAT Direct3DFindColorFormat(LPDIRECT3D9 pD3D, eUMF_COLORFORMAT src_format, D3DFORMAT target);

