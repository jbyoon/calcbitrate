#include "StdAfx.h"
#include <d3dx9.h>
#include "DX9Render.h"
#include "DX9Calibration.h"
#include "Resource.h"


#ifndef _SAFE_RELEASE
#define _SAFE_RELEASE(c) if(c){c->Release();c=NULL;}else{}
#endif

/* maximum valid light range */
#define D3DLIGHT_RANGE_MAX      ((float)sqrt(FLT_MAX))

/// different colour materials
D3DCOLORVALUE CDX9Calibration::m_colourLUT[NUM_COLOURS] = { 
	{0.2f, 0.2f, 0.8f, 1.0f },
	{0.8f, 0.2f, 0.2f, 1.0f },
	{0.2f, 0.8f, 0.2f, 1.0f },
	{0.2f, 0.8f, 0.8f, 1.0f },
	{0.8f, 0.8f, 0.2f, 1.0f },
	{0.8f, 0.2f, 0.8f, 1.0f }
}; 

CDX9Calibration::CDX9Calibration()
{
	m_bInited			= FALSE;
	m_bTransparentGrid	= FALSE;
	m_bSkyEnabled		= FALSE;
	m_pd3dDevice		= NULL;
	m_pGroundTexture	= NULL;
	m_pGroundVB			= NULL;
	SetRectEmpty(&m_ClientRect);

	m_pSphereMesh		= NULL;
	m_pCylinderMesh		= NULL;
	m_pConeMesh			= NULL;

	m_fHumanHeight	= 0;
	m_pRulerTexture = NULL;
	m_pRulerVB		= NULL;
	m_iNumObjects	= 0;

	m_pHorizonVB	= NULL;

	m_csVideoSize = CSize(_MAXWIDTH_D3DCANVAS, _MAXHEIGHT_D3DCANVAS);
}

CDX9Calibration::~CDX9Calibration()
{
	Close();
}


//////////////////////////////////////////////////////////////////////////
// Public Member Functions
//////////////////////////////////////////////////////////////////////////

BOOL CDX9Calibration::Init(LPDIRECT3DDEVICE9 pd3dDevice, RECT ClientRect)
{
	if(!pd3dDevice) return FALSE;
	m_pd3dDevice = pd3dDevice;
	if(IsRectEmpty(&ClientRect)) return FALSE;
	if(TRUE == m_bInited) {
		Close();
		m_bInited = FALSE;
	}

	m_ClientRect = ClientRect;

	InitCalibInfoVars();
	if(FALSE == InitDrawCalib()) {
		Close();
		return FALSE;
	}

	m_bInited = TRUE;

	return TRUE;
}

void CDX9Calibration::Close()
{
	_SAFE_RELEASE(m_pGroundTexture);
	_SAFE_RELEASE(m_pGroundVB);
	_SAFE_RELEASE(m_pSphereMesh);
	_SAFE_RELEASE(m_pCylinderMesh);
	_SAFE_RELEASE(m_pConeMesh);
	_SAFE_RELEASE(m_pRulerTexture);
	_SAFE_RELEASE(m_pRulerVB);
	_SAFE_RELEASE(m_pHorizonVB);
}

void CDX9Calibration::OnChangeClientRect(RECT ClientRect)
{
	if(FALSE == m_bInited) return;
	if(IsRectEmpty(&ClientRect)) return;
	m_ClientRect = ClientRect;
}

void CDX9Calibration::SetInitialCameraParams(CAMERA_CALIB_INFO& calibInfo )
{
	if(FALSE == m_bInited) return;
	if(TRUE == m_bParamsSet) return;

	m_bParamsSet = TRUE;

	SetCameraParams(calibInfo, TRUE);
}


void CDX9Calibration::SetCameraParams(CAMERA_CALIB_INFO& calibInfo, BOOL bCentre )
{
	if(FALSE == m_bInited) return;
	if (!bCentre) {
		HoldModelPositions_Begin();
	}

	m_CalibInfo = calibInfo;

	m_vecGridRot.x = (float)D3DXToRadian(m_CalibInfo.fTilt);
	m_vecGridRot.z = (float)D3DXToRadian(m_CalibInfo.fPan);
	m_vecGridRot.y = (float)D3DXToRadian(m_CalibInfo.fRoll);

	SetupMatricies();


	if (bCentre) {	
		/// centre the models to nice position
		unsigned int w = m_ClientRect.right - m_ClientRect.left;
		unsigned int h = m_ClientRect.bottom - m_ClientRect.top;
		//m_GetClientWindowSize(&w, &h);

		POINT centrePoint;
		if (m_CalibInfo.fTilt >= 0.0f && m_CalibInfo.fTilt <= 60.0f) {
			centrePoint.x = w/2;
			centrePoint.y = h-100;
		} else if (m_CalibInfo.fTilt > 60.0f && m_CalibInfo.fTilt < 120.0f) {
			centrePoint.x = w/2;
			centrePoint.y = h/2;
		} else if (m_CalibInfo.fTilt < 0.0f) {
			centrePoint.x = w/2;
			centrePoint.y = h-1;
		} else if (m_CalibInfo.fTilt <= 180.0f && m_CalibInfo.fTilt >= 120.0f) {
			centrePoint.x = w/2;
			centrePoint.y = 100;
		} else if (m_CalibInfo.fTilt > 180.0f) {
			centrePoint.x = w/2;
			centrePoint.y = 1;
		} else {
			centrePoint.x = w/2;
			centrePoint.y = h/2;
		}

		D3DXVECTOR3 v = UnprojectZplane(centrePoint, -1);

		m_iNumObjects = 2;

		m_objectModels[0].vecPos.x = v.x - 1.0f;
		m_objectModels[0].vecPos.y = v.y - 1.0f;

		m_objectModels[1].vecPos.x = v.x + 1.0f;
		m_objectModels[1].vecPos.y = v.y + 1.0f;

	} else {// make sure they not on the edge

		HoldModelPositions_End();
//		CheckModelPositions();
	}
}


void CDX9Calibration::RenderCalibrationGrid()
{
	if(FALSE == m_bInited) return;

	if( SUCCEEDED( m_pd3dDevice->BeginScene() ) ) {
		unsigned int w, h;

		w	= m_ClientRect.right - m_ClientRect.left;
		h	= m_ClientRect.bottom - m_ClientRect.top;

		// DWORD stateHandle;
		IDirect3DStateBlock9* pStateBlock = NULL;

		/// save the current device state
		m_pd3dDevice->CreateStateBlock( D3DSBT_ALL, &pStateBlock);

		m_pd3dDevice->Clear( 0, NULL, D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER, D3DCOLOR_RGBA(0,0,0,1), 1.0f, 0 );

		//	m_pd3dDevice->SetRenderState( D3DRS_ANTIALIAS, D3DANTIALIAS_SORTINDEPENDENT);
		//	m_pd3dDevice->SetRenderState( D3DRS_EDGEANTIALIAS, TRUE);

		m_pd3dDevice->SetRenderState( D3DRS_LIGHTING, FALSE);

		m_pd3dDevice->SetRenderState( D3DRS_ALPHABLENDENABLE,	TRUE );
		m_pd3dDevice->SetRenderState( D3DRS_SRCBLEND,			D3DBLEND_SRCALPHA );
		m_pd3dDevice->SetRenderState( D3DRS_DESTBLEND,		D3DBLEND_INVSRCALPHA );
		m_pd3dDevice->SetRenderState( D3DRS_ALPHATESTENABLE,	FALSE );
		m_pd3dDevice->SetRenderState( D3DRS_ALPHAREF,			0 );

		/// �ø������ ����. �ﰢ���� �ո�, �޸��� ��� �������Ѵ�.
		m_pd3dDevice->SetRenderState( D3DRS_CULLMODE,			D3DCULL_NONE);
		m_pd3dDevice->SetRenderState( D3DRS_FILLMODE,			D3DFILL_SOLID);
		//	m_pd3dDevice->SetRenderState( D3DRS_NORMALIZENORMALS,	TRUE);	/// need this because scaling screws up normals
		m_pd3dDevice->SetRenderState( D3DRS_ZENABLE,			TRUE );

		D3DVIEWPORT9 Viewport =  { 0, 0, w, h, 0.0f, 1.0f };
		m_pd3dDevice->SetViewport( &Viewport );

		SetupMatricies();

		m_pd3dDevice->SetTransform( D3DTS_PROJECTION, (D3DMATRIX *)&m_matProjModel );
		m_pd3dDevice->SetTransform( D3DTS_VIEW, (D3DMATRIX *)&m_matView );

		DrawGroundPlane();

		if (m_bRulerEnabled)
			DrawRuler();

		m_pd3dDevice->SetRenderState( D3DRS_LIGHTING,			TRUE);
		m_pd3dDevice->SetRenderState( D3DRS_SPECULARENABLE,	FALSE );
		m_pd3dDevice->SetRenderState( D3DRS_AMBIENT,			D3DCOLOR_COLORVALUE(0.4f,0.4f,0.4f,0xff) );
		m_pd3dDevice->SetRenderState( D3DRS_CULLMODE,			D3DCULL_CW);	// modified (conrad)
		m_pd3dDevice->SetRenderState( D3DRS_NORMALIZENORMALS,	TRUE);	/// need this because scaling screws up normals
		m_pd3dDevice->SetLight(0, &m_light);
		m_pd3dDevice->LightEnable(0, TRUE);


		for (int i = m_iNumObjects-1; i >= 0; i--) {
			//DrawObject(i);
			DrawPerson(i);
		}

		/// restore previous state
		pStateBlock->Apply();

		pStateBlock->Release();

		m_pd3dDevice->Clear( 0, NULL, D3DCLEAR_ZBUFFER, D3DCOLOR_RGBA(0,0,0,1), 1.0f, 0 );

		m_pd3dDevice->EndScene();
	}
}


void CDX9Calibration::RegisterCalibInfoCallback(_calib_callback_fxn _fxn, void* _usr_info)
{
	m_calib_callback_fxn = _fxn;
	m_callback_usr_info	= _usr_info;
}


void CDX9Calibration::MouseOver(POINT screenCoord)
{
	if(FALSE == m_bInited) return;

	if (m_bRulerEnabled) {
		float fDist2Plane;
		D3DXVECTOR3 v = UnprojectZplane(screenCoord, -1, 0, &fDist2Plane);

		if (fDist2Plane >= 0.0f && fDist2Plane <= 1.0f) {
			m_vRulerEndPos = v;
		}
	}

	for (int i = 0; i < m_iNumObjects; i++) {
		m_objectModels[i].bHighlighted = false;
	}


	D3DXVECTOR3 vPickedPos;
	ObjectModel* pObjPicked = PickObject(screenCoord, &vPickedPos);

	if (pObjPicked) {
		pObjPicked->bHighlighted = true;
	}	
}

void CDX9Calibration::PickGrid(POINT screenCoord)
{
	if(FALSE == m_bInited) return;

	D3DXVECTOR3 vPickedPos;
	ObjectModel* pObjPicked = PickObject(screenCoord, &vPickedPos);

	if (pObjPicked)
	{
		D3DXVECTOR4 out;
		D3DXVec3Transform(&out, &vPickedPos, &pObjPicked->matObjectModel); // project picked coords from model space to view space
		pObjPicked->vecPickPos = D3DXVECTOR3(out.x, out.y, out.z) - pObjPicked->vecPos; // get offset from current object position
		pObjPicked->bDragging = true;
		m_bTiltingGrid = FALSE;

	}
	else
	{
		m_tiltStartPos = screenCoord;
		m_bTiltingGrid = TRUE;
	}
}


void CDX9Calibration::DragGrid(POINT screenCoord)
{
	if(FALSE == m_bInited) return;

	D3DXVECTOR3 v, v2;
	float fDist2Plane;
	ObjectModel* pObj = m_objectModels;


	int i = 0;
	for (i = 0; i < m_iNumObjects; i++, pObj++)
	{
		if (pObj->bDragging)
		{
			ClipCoords(screenCoord);
			v = UnprojectZplane(screenCoord, -1, pObj->vecPickPos.z, &fDist2Plane);

			if (fDist2Plane >= 0.0f && fDist2Plane <= 1.0f)
			{
				pObj->vecPos.y = v.y - pObj->vecPickPos.y;
				pObj->vecPos.x = v.x - pObj->vecPickPos.x;
			}
			break;
		}
	}

	if (m_bTiltingGrid && i == m_iNumObjects )
	{	
		HoldModelPositions_Begin();

		unsigned int w = m_ClientRect.right - m_ClientRect.left;
		unsigned int h = m_ClientRect.bottom - m_ClientRect.top;

		float fang = float(m_tiltStartPos.y - screenCoord.y)*(float)D3DXToRadian(m_CalibInfo.fFOV)/float(h);
		m_vecGridRot.x += fang;
		m_vecGridRot.x = (float)min(D3DXToRadian(90.0f + m_CalibInfo.fFOV/2), max(D3DXToRadian(- m_CalibInfo.fFOV/2), m_vecGridRot.x));


		/*//no more panning with mouse :( 

		fang = float(m_tiltStartPos.x - screenCoord.x)*D3DXToRadian(m_CalibInfo.fFOV*float(m_csVideoSize.cx)/float(m_csVideoSize.cy))/float(w);

		m_vecGridRot.z += fang;
		// wrap the rotation thru 360deg
		while (m_vecGridRot.z > 2*D3DX_PI)
		m_vecGridRot.z -= 2*D3DX_PI;
		while (m_vecGridRot.z < 0)
		m_vecGridRot.z += 2*D3DX_PI;
		*/

		m_tiltStartPos = screenCoord;

		m_CalibInfo.fTilt = D3DXToDegree(m_vecGridRot.x);
		m_CalibInfo.fPan = D3DXToDegree(m_vecGridRot.z);

		UpdateCalibInfo();

//		CheckModelPositions();

		HoldModelPositions_End();
	}
}

void CDX9Calibration::EndGridDrag()
{
	if(FALSE == m_bInited) return;

	for (int i = 0; i < m_iNumObjects; i++)
	{
		m_objectModels[i].bDragging = false;
	}

	m_bTiltingGrid = FALSE;
}

void CDX9Calibration::BeginRuler( POINT screenCoord )
{
	if(FALSE == m_bInited) return;

	float fDist2Plane = 0.0f;
	D3DXVECTOR3 v = UnprojectZplane(screenCoord, -1, 0, &fDist2Plane);

	if (fDist2Plane >= 0.0f && fDist2Plane <= 1.0f)
	{
		m_vRulerStartPos = v;
		m_vRulerEndPos = v;
		m_bRulerEnabled = TRUE;
	}
}

void CDX9Calibration::EndRuler()
{
	if(FALSE == m_bInited) return;

	m_bRulerEnabled = FALSE;
}

void CDX9Calibration::ZoomGrid(POINT screenCoord, long zDelta)
{
	if(FALSE == m_bInited) return;

	HoldModelPositions_Begin();

	if (zDelta > 0)
		m_CalibInfo.fHeight /= 1.05f;
	else if (zDelta < 0)
		m_CalibInfo.fHeight *= 1.05f;

	m_CalibInfo.fHeight = min(200.0f, max(0.2f, m_CalibInfo.fHeight));

	HoldModelPositions_End();

	//	CheckModelPositions();

		UpdateCalibInfo();
}

void CDX9Calibration::InsertObjectModel(POINT screenCoord)
{
	if(FALSE == m_bInited) return;

	D3DXVECTOR3 v;
	float fDist2Plane;
	v = UnprojectZplane(screenCoord, -1, m_fHumanHeight/2, &fDist2Plane);

	if (fDist2Plane >= 0.0f && fDist2Plane <= 1.0f)
	{

		if(_insertObjectModel(v))
			m_objectModels[m_iNumObjects-1].bHighlighted = true; // max him highlighted
	}

}



void CDX9Calibration::EnableTransparentGrid(BOOL bEnable)
{
	m_bTransparentGrid = bEnable;
}

void CDX9Calibration::EnableCalibDrawSky(BOOL bEnable)
{
	m_bSkyEnabled = bEnable;
}


//////////////////////////////////////////////////////////////////////////
// Private Member Functions
//////////////////////////////////////////////////////////////////////////

void CDX9Calibration::ClipCoords(POINT &screenCoord)
{
	LONG w = m_ClientRect.right - m_ClientRect.left;
	LONG h = m_ClientRect.bottom - m_ClientRect.top;

	screenCoord.x = min(w-1, max(0, screenCoord.x));
	screenCoord.y = min(h-1, max(0, screenCoord.y));
}

void CDX9Calibration::DrawGroundPlane()
{
	D3DXMATRIX matLocal;

	D3DCAPS9 Caps;
	m_pd3dDevice->GetDeviceCaps(&Caps);
	m_pd3dDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_ANISOTROPIC);
	m_pd3dDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_ANISOTROPIC);
	m_pd3dDevice->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR);
	m_pd3dDevice->SetSamplerState(0, D3DSAMP_MAXANISOTROPY, Caps.MaxAnisotropy);

	if (m_bSkyEnabled)
	{
		// draw shit horizon line instead

		D3DXMATRIX matRotation, matProj;
		D3DXMatrixRotationX(&matRotation, m_vecGridRot.x);
		D3DXMatrixIdentity(&matLocal);

		m_pd3dDevice->SetTexture( 0, NULL );

		m_pd3dDevice->SetTransform(D3DTS_VIEW, (D3DMATRIX *) &m_matView_noTrans);
		m_pd3dDevice->SetTransform(D3DTS_WORLD, (D3DMATRIX *) &matLocal);

		//m_pd3dDevice->DrawPrimitive( D3DPT_LINESTRIP, D3DFVF_LVERTEX , m_vHorizonVerticies, 5, 0);
		m_pd3dDevice->SetStreamSource(0, m_pHorizonVB, 0, sizeof(CUSTOMVERTEX1));
		m_pd3dDevice->SetFVF(D3DFVF_CUSTOMVERTEX1);
		m_pd3dDevice->DrawPrimitive(D3DPT_LINESTRIP, 0, 5);

		m_pd3dDevice->SetTransform(D3DTS_VIEW, (D3DMATRIX *) &m_matView);
	}

	// wrap texture to repeate grid
	m_pd3dDevice->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
	m_pd3dDevice->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);

 	if (m_pGroundTexture)
	{
		m_pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
		m_pd3dDevice->SetRenderState(D3DRS_TEXTUREFACTOR, m_bTransparentGrid?  0x40000000 : 0xFF000000 ); // set total alpha for this texture
		m_pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
		m_pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE );
		m_pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_TFACTOR );

		m_pd3dDevice->SetTexture( 0, m_pGroundTexture );

		for (int i = -25; i < 25; i++)
		{
			for (int j = -25; j < 25; j++)
			{
				D3DXMatrixTranslation(&matLocal, i*20.0f, j*20.0f, 0.0f);
				m_pd3dDevice->SetTransform(D3DTS_WORLD, (D3DMATRIX *) &matLocal);

				m_pd3dDevice->SetStreamSource(0, m_pGroundVB, 0, sizeof(CUSTOMVERTEX));
				m_pd3dDevice->SetFVF(D3DFVF_CUSTOMVERTEX);
				m_pd3dDevice->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 4);
			}
		}
 	}
}

void CDX9Calibration::InitCalibInfoVars()
{
	m_CalibInfo.fHeight	= 10.0;
	m_CalibInfo.fFOV	= 40.0;
	m_CalibInfo.fTilt	= 30.0;
	m_CalibInfo.fPan	= 0.0;
	m_CalibInfo.fRoll	= 0.0;
}

BOOL CDX9Calibration::InitDrawCalib()
{
	m_fHumanHeight = HUMAN_HEIGHT;

	SetupMatricies();

	m_bParamsSet = FALSE;
	m_bTiltingGrid = FALSE;
	m_bSkyEnabled = FALSE;

	m_vecGridRot = D3DXVECTOR3((float)D3DXToRadian(m_CalibInfo.fTilt), 0.0f, (float)D3DXToRadian(m_CalibInfo.fPan));

	m_bRulerEnabled = FALSE;
	m_calib_callback_fxn = NULL;
	m_callback_usr_info = NULL;

	ZeroMemory(m_objectModels, sizeof(m_objectModels));

	_insertObjectModel(D3DXVECTOR3(0, 0, 0));
	_insertObjectModel(D3DXVECTOR3(0, 0, 0));


	///// sphere
	if (S_OK != D3DXCreateSphere(m_pd3dDevice, 0.1f, 10, 10, &m_pSphereMesh, 0)) return FALSE;

	///// cylinder
	if (S_OK != D3DXCreateCylinder(m_pd3dDevice, 0.1f, 0.1f, 1.0f, 10, 5, &m_pCylinderMesh, 0)) return FALSE;

	/// cone
	if (S_OK != D3DXCreateCylinder(m_pd3dDevice, 0.15f, 0.25f, 1.0f, 10, 5, &m_pConeMesh, 0)) return FALSE;

	//// light
	D3DXVECTOR4 lightDir(-0.5f, 0.5f, -1.0f, 0.0f);

	ZeroMemory(&m_light, sizeof(D3DLIGHT9));
	m_light.Type			= D3DLIGHT_DIRECTIONAL;
	m_light.Diffuse.r	= m_light.Specular.r = 1.0f;
	m_light.Diffuse.g	= m_light.Specular.g = 1.0f;
	m_light.Diffuse.b	= m_light.Specular.b = 1.0f;
	m_light.Position.x	= 0.0f;
	m_light.Position.y	= 0.0f;
	m_light.Position.z	= 0.0f;
	m_light.Direction.x	= lightDir.x;
	m_light.Direction.y	= lightDir.y;
	m_light.Direction.z	= lightDir.z;

	// Don't attenuate.
	m_light.Attenuation0 = 1.0f; 
	m_light.Range        = D3DLIGHT_RANGE_MAX;

	if(FALSE == InitGroundVertexTexture()) return FALSE;
	if(FALSE == InitRulerVertexTexture()) return FALSE;
	if(FALSE == InitHorizonVertex()) return FALSE;

	return TRUE;
}

BOOL CDX9Calibration::InitGroundVertexTexture()
{
	// Init Ground Texture Vertices
	CUSTOMVERTEX aGroundVertices[4] = 
	{
		{-10.0f,  10.0f, 0.0f, 0.0f, 0.0f, 1.0f, -1.0f,  1.0f},
		{ 10.0f,  10.0f, 0.0f, 0.0f, 0.0f, 1.0f,  1.0f,  1.0f},
		{-10.0f, -10.0f, 0.0f, 0.0f, 0.0f, 1.0f, -1.0f, -1.0f},
		{ 10.0f, -10.0f, 0.0f, 0.0f, 0.0f, 1.0f,  1.0f, -1.0f},
	};

	UINT nVertexCnt = sizeof(aGroundVertices) / sizeof(aGroundVertices[0]);

	if( FAILED( m_pd3dDevice->CreateVertexBuffer( nVertexCnt*sizeof(CUSTOMVERTEX),
		0, D3DFVF_CUSTOMVERTEX,
		D3DPOOL_DEFAULT, &m_pGroundVB, NULL ) ) ) {
			return FALSE;
	}

	VOID* pVertices;
	if( FAILED( m_pGroundVB->Lock( 0, sizeof(aGroundVertices), (void**)&pVertices, 0 ) ) )
		return FALSE;
	memcpy( pVertices, aGroundVertices, sizeof(aGroundVertices) );
	m_pGroundVB->Unlock();


	// Init Ground Texture
	HRESULT hr;

	hr = D3DXCreateTextureFromResourceEx(
		m_pd3dDevice, 
		g_hInstance,
		MAKEINTRESOURCE(IDB_GROUND),
		D3DX_DEFAULT, 
		D3DX_DEFAULT,
		1, 
		0, 
		D3DFMT_A8R8G8B8, D3DPOOL_MANAGED, D3DX_FILTER_BOX|D3DX_FILTER_DITHER,
		0, D3DCOLOR_ARGB(0xff, 0x00,0xff,0x01), 0, 0, &m_pGroundTexture);

	if(FAILED(hr)) return FALSE;

	return TRUE;
}

BOOL CDX9Calibration::InitRulerVertexTexture()
{
	HRESULT hr;

	hr = D3DXCreateTextureFromResource(m_pd3dDevice, g_hInstance, MAKEINTRESOURCE(IDB_RULER), &m_pRulerTexture);
	if(FAILED(hr)) return FALSE;

	hr =  m_pd3dDevice->CreateVertexBuffer( 
		4 * sizeof(CUSTOMVERTEX),
		0, D3DFVF_CUSTOMVERTEX,
		D3DPOOL_DEFAULT, &m_pRulerVB, NULL );

	if(FAILED(hr)) return FALSE;

	return TRUE;
}


void CDX9Calibration::DrawPerson(int iModelIdx)
{
	D3DXMATRIX matModelRotation, matTranslation, matScaling, matRotation;
	D3DXMATRIX matLocal, matModel;

	SetupModelMatrix(iModelIdx);

	matModel = m_objectModels[iModelIdx].matObjectModel;

	if ( m_objectModels[iModelIdx].bHighlighted)
	{
		m_materialPerson[iModelIdx].Emissive.r = min(0.3f, m_materialPerson[iModelIdx].Emissive.r+0.05f);
		m_materialPerson[iModelIdx].Emissive.g = min(0.3f, m_materialPerson[iModelIdx].Emissive.g+0.05f);
		m_materialPerson[iModelIdx].Emissive.b = min(0.3f, m_materialPerson[iModelIdx].Emissive.b+0.05f);
	}
	else
	{
		m_materialPerson[iModelIdx].Emissive.r = max(0.0f, m_materialPerson[iModelIdx].Emissive.r-0.05f);
		m_materialPerson[iModelIdx].Emissive.g = max(0.0f, m_materialPerson[iModelIdx].Emissive.g-0.05f);
		m_materialPerson[iModelIdx].Emissive.b = max(0.0f, m_materialPerson[iModelIdx].Emissive.b-0.05f);
	}

	m_pd3dDevice->SetMaterial(&m_materialPerson[iModelIdx]);
	m_pd3dDevice->SetTexture( 0, NULL );

	/// head
	D3DXMatrixScaling(&matScaling, 1.5f, 1.5f, 1.5f); 
	D3DXMatrixTranslation(&matTranslation, 0, 0, m_fHumanHeight-0.125f);
	matLocal = matScaling*matTranslation;                                                                           
	m_pd3dDevice->SetTransform(D3DTS_WORLD, (D3DMATRIX *) &(matLocal*matModel));
	m_pSphereMesh->DrawSubset(0);


	/// feet
	float legangle = D3DXToRadian(8);
	float leglength = 0.85f;
	float feetpos = 0.17f;
	float legwidth = 0.9f;

	D3DXMatrixScaling(&matScaling, legwidth, legwidth, legwidth); 
	D3DXMatrixTranslation(&matTranslation, -feetpos, 0.0f, 0.1f);
	matLocal = matScaling*matTranslation;                                                                           
	m_pd3dDevice->SetTransform(D3DTS_WORLD, (D3DMATRIX *) &(matLocal*matModel));
	m_pSphereMesh->DrawSubset(0);

	D3DXMatrixTranslation(&matTranslation, feetpos, 0.0f, 0.1f);
	matLocal = matScaling*matTranslation;                                                                           
	m_pd3dDevice->SetTransform(D3DTS_WORLD, (D3DMATRIX *) &(matLocal*matModel));
	m_pSphereMesh->DrawSubset(0);

	/// legs
	D3DXMatrixScaling(&matScaling, legwidth, legwidth, leglength); 
	D3DXMatrixTranslation(&matTranslation, feetpos, 0.0f, 0.1f);
	D3DXMatrixRotationY(&matRotation, -legangle);
	matLocal = matScaling*matRotation*matTranslation;                                                                           
	m_pd3dDevice->SetTransform(D3DTS_WORLD, (D3DMATRIX *) &(matLocal*matModel));
	m_pCylinderMesh->DrawSubset(0);

	D3DXMatrixTranslation(&matTranslation, -feetpos, 0.0f, 0.1f);
	D3DXMatrixRotationY(&matRotation, legangle);
	matLocal = matScaling*matRotation*matTranslation;                                                                           
	m_pd3dDevice->SetTransform(D3DTS_WORLD, (D3DMATRIX *) &(matLocal*matModel));
	m_pCylinderMesh->DrawSubset(0);

	/// arms & hands
	float armangle = D3DXToRadian(13);
	float armlength = 0.55f;
	float handpos	= 0.3f;
	float handheight = 0.9f;
	float armwidth = 0.6f;

	D3DXMatrixScaling(&matScaling, armwidth, armwidth, armwidth); 
	D3DXMatrixTranslation(&matTranslation, -handpos, 0.0f, handheight);
	matLocal = matScaling*matTranslation;                                                                           
	m_pd3dDevice->SetTransform(D3DTS_WORLD, (D3DMATRIX *) &(matLocal*matModel));
	m_pSphereMesh->DrawSubset(0);

	D3DXMatrixTranslation(&matTranslation, handpos, 0.0f, handheight);
	matLocal = matScaling*matTranslation;                                                                           
	m_pd3dDevice->SetTransform(D3DTS_WORLD, (D3DMATRIX *) &(matLocal*matModel));
	m_pSphereMesh->DrawSubset(0);

	D3DXMatrixScaling(&matScaling, armwidth, armwidth, armlength); 
	D3DXMatrixTranslation(&matTranslation, handpos, 0.0f, handheight);
	D3DXMatrixRotationY(&matRotation, -armangle);
	matLocal = matScaling*matRotation*matTranslation;                                                                           
	m_pd3dDevice->SetTransform(D3DTS_WORLD, (D3DMATRIX *) &(matLocal*matModel));
	m_pCylinderMesh->DrawSubset(0);

	D3DXMatrixTranslation(&matTranslation, -handpos, 0.0f, handheight);
	D3DXMatrixRotationY(&matRotation, armangle);
	matLocal = matScaling*matRotation*matTranslation;                                                                           
	m_pd3dDevice->SetTransform(D3DTS_WORLD, (D3DMATRIX *) &(matLocal*matModel));
	m_pCylinderMesh->DrawSubset(0);

	/// body
	D3DXMatrixScaling(&matScaling, 1.0f, 0.5f, 0.65f); 
	D3DXMatrixTranslation(&matTranslation, 0.0f, 0.0f, 0.8f);
	matLocal = matScaling*matTranslation;                                                                           
	m_pd3dDevice->SetTransform(D3DTS_WORLD, (D3DMATRIX *) &(matLocal*matModel));
	m_pConeMesh->DrawSubset(0);

	D3DXMatrixScaling(&matScaling, 1.0f, 0.5f, 0.05f); 
	D3DXMatrixTranslation(&matTranslation, 0.0f, 0.0f, 1.5f);
	D3DXMatrixRotationY(&matRotation, D3DX_PI);
	matLocal = matScaling*matRotation*matTranslation;                                                                           
	m_pd3dDevice->SetTransform(D3DTS_WORLD, (D3DMATRIX *) &(matLocal*matModel));
	m_pConeMesh->DrawSubset(0);

	/* uncomment this for a female version ;)

	/// titties
	D3DXMatrixScaling(&matScaling, 1.0f, 1.0f, 1.0f); 
	D3DXMatrixTranslation(&matTranslation, -0.1f, -0.1f, 1.3f);
	matLocal = matScaling*matTranslation;                                                                           
	m_pd3dDevice->SetTransform(D3DTS_WORLD, (D3DMATRIX *) &(matLocal*matModel));
	m_pSphereMesh->DrawSubset(0);

	D3DXMatrixScaling(&matScaling, 1.0f, 1.0f, 1.0f);
	D3DXMatrixTranslation(&matTranslation, 0.1f, -0.1f, 1.3f);
	matLocal = matScaling*matTranslation;                                                                           
	m_pd3dDevice->SetTransform(D3DTS_WORLD, (D3DMATRIX *) &(matLocal*matModel));
	m_pSphereMesh->DrawSubset(0);

	*/
}


//////////////////////////////// Ruler ///////////////////////////////////

float CDX9Calibration::GetRulerLength()
{
	if (m_bRulerEnabled)
	{
		float dx = m_vRulerStartPos.x - m_vRulerEndPos.x;
		float dy = m_vRulerStartPos.y - m_vRulerEndPos.y;
		return sqrt(dx*dx + dy*dy);
	}
	else
		return -1.0f;
}

void CDX9Calibration::DrawRuler()
{
	D3DXMATRIX matLocal;

	float length = GetRulerLength() * 2.5f;

	float x1 = m_vRulerStartPos.x;
	float y1 = m_vRulerStartPos.y;
	float x2 = m_vRulerEndPos.x;
	float y2 = m_vRulerEndPos.y;

	float fTheta = atan2( (float)(y2-y1), (float)(x2-x1) );
	float fx = sin(fTheta);
	float fy = cos(fTheta);
	float fxOff, fyOff;

	fxOff = 0.2f * fx;
	fyOff = 0.2f * fy;

	CUSTOMVERTEX aRulerVertices[4] = 
	{
		{ x1 - fxOff, y1 + fyOff, 0.0005f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f},
		{ x2 - fxOff, y2 + fyOff, 0.0005f, 0.0f, 0.0f, 1.0f, length,  0.0f},
		{ x1 + fxOff, y1 - fyOff, 0.0005f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f},
		{ x2 + fxOff, y2 - fyOff, 0.0005f, 0.0f, 0.0f, 1.0f, length, 1.0f},
	};

	VOID* pVertices;
	if( FAILED( m_pRulerVB->Lock( 0, sizeof(aRulerVertices), (void**)&pVertices, 0 ) ) )
		return;
	memcpy( pVertices, aRulerVertices, sizeof(aRulerVertices) );
	m_pRulerVB->Unlock();

	D3DXMatrixIdentity(&matLocal);
	m_pd3dDevice->SetTransform( D3DTS_WORLD, (D3DMATRIX *)&matLocal );

	//	m_pd3dDevice->SetMaterial(&m_materialPerson[0]);
	m_pd3dDevice->SetTexture( 0, m_pRulerTexture );

	m_pd3dDevice->SetStreamSource(0, m_pRulerVB, 0, sizeof(CUSTOMVERTEX));
	m_pd3dDevice->SetFVF(D3DFVF_CUSTOMVERTEX);
	m_pd3dDevice->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);
	//m_pd3dDevice->DrawPrimitive(D3DPT_TRIANGLESTRIP, D3DFVF_VERTEX, vRulerVerticies, 4, 0);
}

BOOL CDX9Calibration::InitHorizonVertex()
{
	// Init Ground Texture Vertices
	CUSTOMVERTEX1 aHorizonVerticies[5] = 
	{
		{-510.0f, 510.0f, 0.0f, 0xFF00FF00, 0, 0.0f, 0.0f},
		{ 510.0f, 510.0f, 0.0f, 0xFF00FF00, 0, 0.0f, 0.0f},
		{ 510.0f,-510.0f, 0.0f, 0xFF00FF00, 0, 0.0f, 0.0f},
		{-510.0f,-510.0f, 0.0f, 0xFF00FF00, 0, 0.0f, 0.0f},
		{-510.0f, 510.0f, 0.0f, 0xFF00FF00, 0, 0.0f, 0.0f}
	};

	UINT nVertexCnt = sizeof(aHorizonVerticies) / sizeof(aHorizonVerticies[0]);

	if( FAILED( m_pd3dDevice->CreateVertexBuffer( nVertexCnt*sizeof(CUSTOMVERTEX1),
		0, D3DFVF_CUSTOMVERTEX1,
		D3DPOOL_DEFAULT, &m_pHorizonVB, NULL ) ) ) {
			return FALSE;
	}

	VOID* pVertices;
	if( FAILED( m_pHorizonVB->Lock( 0, sizeof(aHorizonVerticies), (void**)&pVertices, 0 ) ) )
		return FALSE;
	memcpy( pVertices, aHorizonVerticies, sizeof(aHorizonVerticies) );
	m_pHorizonVB->Unlock();

	return TRUE;
}

void CDX9Calibration::SetupMatricies()
{
	D3DXMATRIX matTranslation, matModelRotationTilt, matModelTiltLevel, matModelRotationPan, matModelRotationRoll;

	// Left Hand version? or Right Hand version
	D3DXMatrixPerspectiveFovRH( &m_matProjModel, D3DX_PI*((float)m_CalibInfo.fFOV)/180.0f, float(m_csVideoSize.cy)/float(m_csVideoSize.cx), 1.0f, 1000.0f );
	D3DXMatrixPerspectiveFovRH( &m_matProjModel_Clipped, D3DX_PI*((float)m_CalibInfo.fFOV)/180.0f, float(m_csVideoSize.cy)/float(m_csVideoSize.cx), 1.0f, 510.0f );

	D3DXMatrixTranslation(&matTranslation, 0, 0, -((float)m_CalibInfo.fHeight));
	D3DXMatrixRotationX(&matModelRotationTilt, m_vecGridRot.x);
	D3DXMatrixRotationX(&matModelTiltLevel, -D3DX_PI/2);
	D3DXMatrixRotationY(&matModelRotationPan, m_vecGridRot.z);
	D3DXMatrixRotationZ(&matModelRotationRoll, m_vecGridRot.y);

	m_matView_noTrans = matModelTiltLevel*matModelRotationPan*matModelRotationTilt*matModelRotationRoll;
	m_matView = matTranslation*m_matView_noTrans;
}

void CDX9Calibration::SetupModelMatrix(int iModelIdx)
{
	D3DXMATRIX matRotationY, matTranslation, matModelTranslation, matTranslation2;

	D3DXMatrixRotationY(&matRotationY, m_objectModels[iModelIdx].vecRot.z);
	D3DXMatrixTranslation(&matTranslation, 0, 0, -m_fHumanHeight/2);
	D3DXMatrixTranslation(&matTranslation2, 0, 0, m_fHumanHeight/2);

	matRotationY = matTranslation*matRotationY*matTranslation2;

	D3DXMatrixTranslation(&matModelTranslation, m_objectModels[iModelIdx].vecPos.x, m_objectModels[iModelIdx].vecPos.y, 0);


	m_objectModels[iModelIdx].matObjectModel = matRotationY*matModelTranslation;

}

BOOL CDX9Calibration::_insertObjectModel(D3DXVECTOR3 pos)
{
	if (m_iNumObjects < MAXNUM_OBJECTS)
	{

		m_objectModels[m_iNumObjects].fMinX = -0.35f;
		m_objectModels[m_iNumObjects].fMaxX = 0.35f;
		m_objectModels[m_iNumObjects].fMinY = -0.15f;
		m_objectModels[m_iNumObjects].fMaxY = 0.15f;
		m_objectModels[m_iNumObjects].fMinZ = 0.0f;
		m_objectModels[m_iNumObjects].fMaxZ = m_fHumanHeight;

		memset(&m_materialPerson[m_iNumObjects], 0, sizeof(D3DMATERIAL9));
		m_materialPerson[m_iNumObjects].Specular.r = 1.0f;
		m_materialPerson[m_iNumObjects].Specular.g = 1.0f;
		m_materialPerson[m_iNumObjects].Specular.b = 1.0f;
		m_materialPerson[m_iNumObjects].Specular.a = 0.0f;
		m_materialPerson[m_iNumObjects].Power = 40.0f;
		m_materialPerson[m_iNumObjects].Diffuse = m_colourLUT[m_iNumObjects%NUM_COLOURS];
		m_materialPerson[m_iNumObjects].Ambient = m_colourLUT[m_iNumObjects%NUM_COLOURS];

		m_objectModels[m_iNumObjects].vecPos = pos;
		m_objectModels[m_iNumObjects].vecPos.z = 0;


		m_iNumObjects++;

		return TRUE;
	}

	return FALSE;
}

void CDX9Calibration::HoldModelPositions_Begin()
{
	D3DXMATRIX matTransform;
	D3DXVECTOR4 out;
	//POINT screenCoord;

	SetupMatricies();

	for (int i = 0; i < m_iNumObjects; i++)
	{
		matTransform = m_matView*m_matProjModel;

		D3DXVec3Transform(&out, &m_objectModels[i].vecPos, &matTransform);

		out.x /= out.w;
		out.y /= out.w;

		m_objectModels[i].vecLogicalScreenPos.x = out.x;
		m_objectModels[i].vecLogicalScreenPos.y = out.y;

	}
}


void CDX9Calibration::HoldModelPositions_End()
{

	D3DXVECTOR3 v,v2;

	SetupMatricies();
	float fDist2Plane;

	for (int i = 0; i < m_iNumObjects; i++)
	{

		v.x = m_objectModels[i].vecLogicalScreenPos.x;
		v.y = m_objectModels[i].vecLogicalScreenPos.y;
		v.z = 0;

		v2 = UnprojectZplane(v, -1, 0, &fDist2Plane);

		if (fDist2Plane >= 0.0f && fDist2Plane <= 1.0f)
		{
			m_objectModels[i].vecPos = v2;
		}
	}
}


D3DXVECTOR3 CDX9Calibration::UnprojectZplane(D3DXVECTOR3 logicalCoord, int modelIdx, float fZPlane, float *pfDist2Plane)
{

	D3DXMATRIX matTransform;  // complete transform
	D3DXMATRIX matInvTransform;
	float fDet;
	D3DXVECTOR3 v;
	D3DXVECTOR4 unprojnear, unprojfar, direction;
//	unsigned int w, h;

	if (modelIdx == -1)
		matTransform = m_matView*m_matProjModel_Clipped;
	else
	{
		matTransform = m_objectModels[modelIdx].matObjectModel*m_matView*m_matProjModel_Clipped;
	}	

//	m_GetClientWindowSize(&w, &h);

	/// convert to logical coords
	v.x = logicalCoord.x;
	v.y = logicalCoord.y;
	v.z = 0.0f;

	/// inverse transform matrix for unprojection
	D3DXMatrixInverse(&matInvTransform, &fDet, &matTransform);

	//// near clipping plane unprojection 
	D3DXVec3Transform(&unprojnear, &v, &matInvTransform);

	unprojnear.x /= unprojnear.w;
	unprojnear.y /= unprojnear.w;
	unprojnear.z /= unprojnear.w;

	/// far clipping plane projection
	v.z = 1.0f;
	D3DXVec3Transform(&unprojfar, &v, &matInvTransform);

	unprojfar.x /= unprojfar.w;
	unprojfar.y /= unprojfar.w;
	unprojfar.z /= unprojfar.w;

	/// find ray intersection point with plane z = fZPlane
	direction = unprojfar - unprojnear;

	float t = (fZPlane-unprojnear.z)/direction.z;
	float X = unprojnear.x + t*direction.x;
	float Y = unprojnear.y + t*direction.y;

	if (pfDist2Plane)
		*pfDist2Plane = t;


	return D3DXVECTOR3(X, Y, fZPlane);
}

D3DXVECTOR3 CDX9Calibration::UnprojectZplane(POINT screenCoord, int modelIdx, float fZPlane, float *pfDist2Plane)
{
	D3DXVECTOR3 v;

	//m_GetClientWindowSize(&w, &h);
	unsigned int w = m_ClientRect.right - m_ClientRect.left;
	unsigned int h = m_ClientRect.bottom - m_ClientRect.top;

	/// convert to logical coords
	v.x = 2.0f*(float)screenCoord.x/(float)w - 1.0f;
	v.y = -2.0f*(float)screenCoord.y/(float)h + 1.0f;
	v.z = 0.0f;

	return UnprojectZplane(v, modelIdx, fZPlane, pfDist2Plane);

}


D3DXVECTOR3 CDX9Calibration::UnprojectYplane(POINT screenCoord, int modelIdx, float fYPlane, float *pfDist2Plane)
{

	D3DXMATRIX matTransform;  // complete transform
	D3DXMATRIX matInvTransform;
	float fDet;
	D3DXVECTOR3 v;
	D3DXVECTOR4 unprojnear, unprojfar, direction;


	if (modelIdx == -1)
		matTransform = m_matView*m_matProjModel_Clipped;
	else
		matTransform = m_objectModels[modelIdx].matObjectModel*m_matView*m_matProjModel_Clipped;


	//m_GetClientWindowSize(&w, &h);
	unsigned int w = m_ClientRect.right - m_ClientRect.left;
	unsigned int h = m_ClientRect.bottom - m_ClientRect.top;

	/// convert to logical coords
	v.x = 2.0f*(float)screenCoord.x/(float)w - 1.0f;
	v.y = -2.0f*(float)screenCoord.y/(float)h + 1.0f;
	v.z = 0.0f;

	/// inverse transform matrix for unprojection
	D3DXMatrixInverse(&matInvTransform, &fDet, &matTransform);

	//// near clipping plane unprojection 
	D3DXVec3Transform(&unprojnear, &v, &matInvTransform);

	unprojnear.x /= unprojnear.w;
	unprojnear.y /= unprojnear.w;
	unprojnear.z /= unprojnear.w;

	/// far clipping plane projection
	v.z = 1.0f;
	D3DXVec3Transform(&unprojfar, &v, &matInvTransform);

	unprojfar.x /= unprojfar.w;
	unprojfar.y /= unprojfar.w;
	unprojfar.z /= unprojfar.w;

	/// find ray intersection point with man plane (y = fYPlane)
	direction = unprojfar - unprojnear;

	float t = (fYPlane-unprojnear.y)/direction.y;
	float X = unprojnear.x + t*direction.x;
	float Z = unprojnear.z + t*direction.z;

	if (pfDist2Plane)
		*pfDist2Plane = t;


	return D3DXVECTOR3(X, fYPlane, Z);

}

D3DXVECTOR3 CDX9Calibration::UnprojectXplane(POINT screenCoord, int modelIdx, float fXPlane, float *pfDist2Plane)
{

	D3DXMATRIX matTransform;  // complete transform
	D3DXMATRIX matInvTransform;
	float fDet;
	D3DXVECTOR3 v;
	D3DXVECTOR4 unprojnear, unprojfar, direction;

	if (modelIdx == -1)
		matTransform = m_matView*m_matProjModel_Clipped;
	else
		matTransform = m_objectModels[modelIdx].matObjectModel*m_matView*m_matProjModel_Clipped;


	//m_GetClientWindowSize(&w, &h);
	unsigned int w = m_ClientRect.right - m_ClientRect.left;
	unsigned int h = m_ClientRect.bottom - m_ClientRect.top;

	/// convert to logical coords
	v.x = 2.0f*(float)screenCoord.x/(float)w - 1.0f;
	v.y = -2.0f*(float)screenCoord.y/(float)h + 1.0f;
	v.z = 0.0f;

	/// inverse transform matrix for unprojection
	D3DXMatrixInverse(&matInvTransform, &fDet, &matTransform);

	//// near clipping plane unprojection 
	D3DXVec3Transform(&unprojnear, &v, &matInvTransform);

	/// unhomo divide
	unprojnear.x /= unprojnear.w;
	unprojnear.y /= unprojnear.w;
	unprojnear.z /= unprojnear.w;

	/// far clipping plane projection
	v.z = 1.0f;
	D3DXVec3Transform(&unprojfar, &v, &matInvTransform);

	/// unhomo divide
	unprojfar.x /= unprojfar.w;
	unprojfar.y /= unprojfar.w;
	unprojfar.z /= unprojfar.w;

	/// find ray intersection point with man plane (x = fXPlane)
	direction = unprojfar - unprojnear;

	float t = (fXPlane-unprojnear.x)/direction.x;
	float Y = unprojnear.y + t*direction.y;
	float Z = unprojnear.z + t*direction.z;

	if (pfDist2Plane)
		*pfDist2Plane = t;

	return D3DXVECTOR3(fXPlane, Y, Z);

}


ObjectModel* CDX9Calibration::PickObject(POINT screenCoord, D3DXVECTOR3 *pPickedPos)
{
	D3DXVECTOR3 v, vPicked;
	ObjectModel* pObj = m_objectModels;
	ObjectModel* pObjPicked = NULL;
	float fDist2PlaneMin = 1.0f + FLT_EPSILON; //FLT_MAX;
	float fDist2Plane;

	/// try pick object from any bounding box face
	for (int i = 0; i < m_iNumObjects; i++, pObj++)
	{	
		v = UnprojectXplane(screenCoord, i, pObj->fMinX, &fDist2Plane);
		if (v.y > pObj->fMinY && v.y < pObj->fMaxY && v.z > pObj->fMinZ && v.z < pObj->fMaxZ && fDist2Plane < fDist2PlaneMin && fDist2Plane >= 0.0f)
		{
			fDist2PlaneMin = fDist2Plane;
			pObjPicked = pObj;
			vPicked = v;
		}

		v = UnprojectXplane(screenCoord, i, pObj->fMaxX,  &fDist2Plane);
		if (v.y > pObj->fMinY && v.y < pObj->fMaxY && v.z > pObj->fMinZ && v.z < pObj->fMaxZ && fDist2Plane < fDist2PlaneMin  && fDist2Plane >= 0.0f)
		{
			fDist2PlaneMin = fDist2Plane;
			pObjPicked = pObj;
			vPicked = v;
		}

		v = UnprojectYplane(screenCoord, i, pObj->fMinY,  &fDist2Plane);
		if (v.x > pObj->fMinX && v.x < pObj->fMaxX && v.z > pObj->fMinZ && v.z < pObj->fMaxZ && fDist2Plane < fDist2PlaneMin  && fDist2Plane >= 0.0f)
		{
			fDist2PlaneMin = fDist2Plane;
			pObjPicked = pObj;
			vPicked = v;
		}

		v = UnprojectYplane(screenCoord, i, pObj->fMaxY,  &fDist2Plane);
		if (v.x > pObj->fMinX && v.x < pObj->fMaxX && v.z > pObj->fMinZ && v.z < pObj->fMaxZ && fDist2Plane < fDist2PlaneMin  && fDist2Plane >= 0.0f)
		{
			fDist2PlaneMin = fDist2Plane;
			pObjPicked = pObj;
			vPicked = v;
		}

		v = UnprojectZplane(screenCoord, i, pObj->fMinZ,  &fDist2Plane);
		if (v.x > pObj->fMinX && v.x < pObj->fMaxX && v.y > pObj->fMinY && v.y < pObj->fMaxY && fDist2Plane < fDist2PlaneMin  && fDist2Plane >= 0.0f)
		{
			fDist2PlaneMin = fDist2Plane;
			pObjPicked = pObj;
			vPicked = v;
		}

		v = UnprojectZplane(screenCoord, i, pObj->fMaxZ,  &fDist2Plane);
		if (v.x > pObj->fMinX && v.x < pObj->fMaxX && v.y > pObj->fMinY && v.y < pObj->fMaxY && fDist2Plane < fDist2PlaneMin  && fDist2Plane >= 0.0f)
		{
			fDist2PlaneMin = fDist2Plane;
			pObjPicked = pObj;
			vPicked = v;
		}
	}

	if (pPickedPos)
		*pPickedPos = vPicked;

	return pObjPicked;

}


void CDX9Calibration::UpdateCalibInfo()
{
// inform callback
	if (m_calib_callback_fxn) {
		m_calib_callback_fxn(m_callback_usr_info, m_CalibInfo);
	}
}

