#include "StdAfx.h"
#include "UMFDrawUtil.h"
#include "DrawRecorder.h"

#ifndef TRACE
#include <atlbase.h>
#define TRACE AtlTrace
#endif


CUMFDrawUtil::CUMFDrawUtil()
{
	m_pUMFRender		= NULL;
	m_pDrawRecorder		= new CDrawRecorder();
}

CUMFDrawUtil::~CUMFDrawUtil()
{
	if(m_pDrawRecorder) delete m_pDrawRecorder;
}

BOOL CUMFDrawUtil::Init(IUMFRender *pUMFRender)
{
	m_pUMFRender = pUMFRender;
	return TRUE;
}

//Orient to Canvas x,y
BOOL CUMFDrawUtil::DrawRect(RECT* pDstRect, COLORREF Color, BYTE Alpha, eUMFBRUSHTYPE BrushType)
{
	if(!m_pDrawRecorder) return FALSE;

	return DrawRectList(1, pDstRect, Color, Alpha, BrushType);
}


BOOL CUMFDrawUtil::DrawRectList(INT nRectCnt, RECT* pDstRects, COLORREF Color, BYTE Alpha, eUMFBRUSHTYPE BrushType)
{
	if(!m_pDrawRecorder) return FALSE;

	CDrawRecorder::DRAWRECORD Item;
	DWORD Result;

	Item.drawType	= CDrawRecorder::UMF_DRT_RECTLIST;
	Item.Color		= Color;
	Item.Alpha		= Alpha;
	Item.BrushType	= BrushType;
	Item.rects.pRects	= pDstRects;
	Item.rects.nRect	= nRectCnt;

	Result = m_pDrawRecorder->Push(&Item);
	Commit();

//	if(Result == -1) return FALSE;

	return TRUE;
}

BOOL CUMFDrawUtil::DrawPolygon(INT PtCnt, POINT *pPoints, COLORREF Color, BYTE Alpha, eUMFBRUSHTYPE BrushType)
{
	if(!m_pDrawRecorder) return FALSE;

	CDrawRecorder::DRAWRECORD Item;
	DWORD Result;

	Item.drawType	= CDrawRecorder::UMF_DRT_POLYGON;
	Item.Color		= Color;
	Item.Alpha		= Alpha;
	Item.BrushType	= BrushType;
	Item.polygon.pPts	= pPoints;
	Item.polygon.nPt	= PtCnt;
	
	Result = m_pDrawRecorder->Push(&Item);
	if(Result == -1) Commit();

	Result = m_pDrawRecorder->Push(&Item);

	return (Result != -1);
}

BOOL CUMFDrawUtil::DrawLineList(INT PtCnt, POINT *pPoints, COLORREF Color, BYTE Alpha)
{
	if(!m_pDrawRecorder) return FALSE;

	CDrawRecorder::DRAWRECORD Item;
	DWORD Result;

	Item.drawType	= CDrawRecorder::UMF_DRT_LINESTRIP;
	Item.Color		= Color;
	Item.Alpha		= Alpha;
	Item.pts.pPts	= pPoints;
	Item.pts.nPt	= PtCnt;

	Result = m_pDrawRecorder->Push(&Item);
	if(Result == -1) Commit();

	Result = m_pDrawRecorder->Push(&Item);

	return (Result != -1);
}

BOOL CUMFDrawUtil::DrawCircle(POINT Center, double Radious, BOOL bFill, COLORREF Color, BYTE Alpha)
{
	if(!m_pDrawRecorder) return FALSE;
	
	CDrawRecorder::DRAWRECORD Item;
	DWORD Result;

	Item.drawType	= CDrawRecorder::UMF_DRT_CIRCLE;
	Item.Color		= Color;
	Item.Alpha		= Alpha;
	Item.circle.bFill		= bFill;
	Item.circle.fRadius		= Radious;
	Item.circle.ptCentre	= Center;

	Result = m_pDrawRecorder->Push(&Item);
	Commit();

	return TRUE;
}

BOOL CUMFDrawUtil::DrawCircularArc(POINT Center, double Radious, unsigned short Width, double StartRadius, double FinishRadius, COLORREF Color, BYTE Alpha)
{
	if(!m_pDrawRecorder) return FALSE;
	
	CDrawRecorder::DRAWRECORD Item;
	DWORD Result;


	Item.drawType	= CDrawRecorder::UMF_DRT_CIRCULARARC;
	Item.Color		= Color;
	Item.Alpha		= Alpha;
	Item.circularArc.fRadius		= Radious;
	Item.circularArc.ptCentre		= Center;
	Item.circularArc.dStartRadius	= StartRadius;
	Item.circularArc.dFinishRadius	= FinishRadius;
	Item.circularArc.usWidth		= Width;

	Result = m_pDrawRecorder->Push(&Item);
	Commit();

	return TRUE;
}

//Font 
BOOL CUMFDrawUtil::DrawText(POINT Start, TCHAR* szText, COLORREF Color, BYTE Alpha)
{
	if(!m_pDrawRecorder) return FALSE;

	CDrawRecorder::DRAWRECORD Item;
	DWORD Result;

	Item.drawType	= CDrawRecorder::UMF_DRT_TEXT;
	Item.text.pt	= Start;
	Item.text.pText	= szText;
	Item.Color		= Color;
	Item.Alpha		= Alpha;

	Result = m_pDrawRecorder->Push(&Item);
	if(Result == -1) Commit();

	Result = m_pDrawRecorder->Push(&Item);

	return (Result != -1);
}

