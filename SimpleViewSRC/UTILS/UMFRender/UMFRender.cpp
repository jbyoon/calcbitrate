// UMFRender.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "DX9Render.h"
#include "DX9DrawUtil.h"
#include "DX9Image.h"


extern "C" IUMFRender* WINAPI CreateUMFRender(eUMFRenderType type)
{
	if(UMF_DX9Render != type) return NULL;
	return new CDX9Render();
}

extern "C" IUMFDrawUtil* WINAPI CreateUMFDrawUtil(DWORD type)
{
	if(UMF_DX9Render != type) return NULL;
	return new CDX9DrawUtil();
}

