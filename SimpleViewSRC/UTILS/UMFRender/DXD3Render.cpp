#include "StdAfx.h"
#include "DXD3Render.h"
#include "colorconversion.h"


#pragma comment( lib, "./render/colorconversion" )
#pragma comment( lib, "dxguid" )
#pragma comment( lib, "ddraw" )


#define INIT_DIRECTDRAW_STRUCT(x) (ZeroMemory(&x, sizeof(x)), x.dwSize=sizeof(x))

#ifndef SAFE_RELEASE
#define SAFE_RELEASE(c) if(c){c->Release();c=NULL;}else{}
#endif

#ifndef SAFE_RELEASE2
#define SAFE_RELEASE2(c) if(c){UINT count=c->Release();c=NULL;TRACE(_T("%s RefCount After Release :%d \n"), #c, count);}else{}
#endif


static DDPIXELFORMAT DDPF_RGBA16={
	sizeof(DDPIXELFORMAT),
	DDPF_RGB|DDPF_ALPHAPIXELS ,
	0,
	16,
	0x00000F00,0x000000F0,0x0000000F,0x0000F000,
};

static DDPIXELFORMAT DDPF_RGB32={
	sizeof(DDPIXELFORMAT),
	DDPF_RGB,
	0,
	32,
	0x00FF0000,0x0000FF00,0x000000FF,0x00000000,
};

static DDPIXELFORMAT DDPF_RGB24={
	sizeof(DDPIXELFORMAT),
	DDPF_RGB,
	0,
	24,
	0x00FF0000,0x0000FF00,0x000000FF,0x00000000,
};

static DDPIXELFORMAT DDPF_RGB565={
	sizeof(DDPIXELFORMAT),
	DDPF_RGB,
	0,
	16,
	0x0000F800,0x000007E0,0x0000001F,0x00000000,
};

static DDPIXELFORMAT DDPF_RGB555={
	sizeof(DDPIXELFORMAT),
	DDPF_RGB,
	0,
	16,
	0x00007C00,0x000003E0,0x0000001F,0x00000000,
};

static DDPIXELFORMAT DDPF_YUY2= {
	sizeof(DDPIXELFORMAT),
	DDPF_FOURCC,
	MAKEFOURCC('Y','U','Y','2'),
	0,
	0,0,0,0
};

static DDPIXELFORMAT DDPF_YV12= {
	sizeof(DDPIXELFORMAT),
	DDPF_FOURCC,
	MAKEFOURCC('Y','V','1','2'),
	0,
	0,0,0,0
};

static DDPIXELFORMAT DDPF_IYUV= {
	sizeof(DDPIXELFORMAT),
	DDPF_FOURCC,
	MAKEFOURCC('I','Y','U','V'),
	0,
	0,0,0,0
};


UMF_COLORFORMAT GetColorFormat3DSupportFromBitInfoHeader(BITMAPINFOHEADER *pbm)
{

	UMF_COLORFORMAT format = UMF_NONE;
	if (MAKEFOURCC('Y','U','Y','2') == pbm->biCompression) {
		format = UMF_YUY2;
	} else if(MAKEFOURCC('Y','V','1','2') == pbm->biCompression) {
		//format = UMF_YV12;
		format = UMF_RGB555;
	}else if(MAKEFOURCC('I','Y','U','V') == pbm->biCompression) {
		format = UMF_YUV420;
	} else { //RGB 
		if(pbm->biBitCount == 32){
			format = UMF_RGBX32;
		}else if(pbm->biBitCount == 24){
			format = UMF_RGB24;
		}else if(pbm->biBitCount == 16){
			format = UMF_RGB565;
		}else if(pbm->biBitCount == 15){
			format = UMF_RGB555;
		}
	}
	return format;
}


CDXD3Render::CDXD3Render(void)
{
	m_pDD			= 0;
	m_pddsPrimary	= 0;
	m_pddsBackBuffer= 0;
	m_pd3dDevice	= 0;
	m_pcClipper		= 0;

	m_TotalImageBufCnt = 0;
	memset(m_ImageBuf, 0, sizeof(m_ImageBuf));
	InitColorConversionFunctions();
	m_bCreate	= FALSE;
	m_hWnd		= NULL;
}


CDXD3Render::~CDXD3Render(void)
{
	if(m_bCreate){
		Destroy();
	}
}

BOOL WINAPI CDXD3Render::AdapterEnumCallbackWrapper( GUID* pGUID, LPSTR strDesc,LPSTR strName, VOID* lpContext, HMONITOR hmonitor)
{
	return ((CDXD3Render*)lpContext)->AdapterEnumCallback(pGUID,strDesc,strName,hmonitor);
}

HRESULT WINAPI EnumZBufferCallback( DDPIXELFORMAT* pddpf, VOID* pddpfDesired )
{
    // For this tutorial, we are only interested in z-buffers, so ignore any
    // other formats (e.g. DDPF_STENCILBUFFER) that get enumerated. An app
    // could also check the depth of the z-buffer (16-bit, etc,) and make a
    // choice based on that, as well. For this tutorial, we'll take the first
    // one we get.
    if( pddpf->dwFlags == DDPF_ZBUFFER )
    {
        memcpy( pddpfDesired, pddpf, sizeof(DDPIXELFORMAT) );
         // Return with D3DENUMRET_CANCEL to end the search.
        return D3DENUMRET_CANCEL;
    }
     // Return with D3DENUMRET_OK to continue the search.
    return D3DENUMRET_OK;
}

//lParam is Pointer to DDPIXELFORMAT or Pointer to LPDIRECT3DDEIVCE7
LPDIRECTDRAWSURFACE7 CreateCustomSurface( LPDIRECTDRAW7 pDD,SurfaceType surfaceType,void *lpParam, DWORD dwWidth,DWORD dwHeight)
{
	HRESULT hr;

	LPDIRECTDRAWSURFACE7  pddsTexture=NULL;
	DDSURFACEDESC2 ddsd;

	INIT_DIRECTDRAW_STRUCT(ddsd);
	switch(surfaceType){
	case SURFACETYPE_AGPTEXTURE:{
		ddsd.dwFlags         = DDSD_CAPS|DDSD_HEIGHT|DDSD_WIDTH|DDSD_PIXELFORMAT;
		ddsd.dwWidth         = dwWidth;
		ddsd.dwHeight        = dwHeight;
		ddsd.ddpfPixelFormat = *(DDPIXELFORMAT*)lpParam;
		ddsd.ddsCaps.dwCaps  = DDSCAPS_TEXTURE|DDSCAPS_VIDEOMEMORY|DDSCAPS_NONLOCALVIDMEM;
		break;}
	case SURFACETYPE_VIDMEMTEXTURE:{
		ddsd.dwFlags         = DDSD_CAPS|DDSD_HEIGHT|DDSD_WIDTH|DDSD_PIXELFORMAT;
		ddsd.dwWidth         = dwWidth;
		ddsd.dwHeight        = dwHeight;
		ddsd.ddpfPixelFormat = *(DDPIXELFORMAT*)lpParam;
		ddsd.ddsCaps.dwCaps  = DDSCAPS_TEXTURE|DDSCAPS_VIDEOMEMORY|DDSCAPS_LOCALVIDMEM ;
		break;}
	case SURFACETYPE_3DCANVAS:{
		DDSURFACEDESC2	ddsdesc;
		INIT_DIRECTDRAW_STRUCT(ddsdesc);
		pDD->GetDisplayMode(&ddsdesc);
		
		ddsd.dwFlags        = DDSD_WIDTH | DDSD_HEIGHT | DDSD_CAPS;
		ddsd.ddsCaps.dwCaps = DDSCAPS_VIDEOMEMORY | DDSCAPS_3DDEVICE|DDSCAPS_LOCALVIDMEM ;//DDSCAPS_OFFSCREENPLAIN
		ddsd.dwWidth  = ddsdesc.dwWidth;
		ddsd.dwHeight = ddsdesc.dwHeight;
		break;}
	case SURFACETYPE_PRIMARY:{
		ddsd.dwFlags        = DDSD_CAPS;
		ddsd.ddsCaps.dwCaps = DDSCAPS_PRIMARYSURFACE;
		break;}
	case SURFACETYPE_MANAGEDTEXTURE:{
		ddsd.dwFlags         = DDSD_CAPS|DDSD_HEIGHT|DDSD_WIDTH|DDSD_TEXTURESTAGE|DDSD_PIXELFORMAT;
		ddsd.ddsCaps.dwCaps  = DDSCAPS_TEXTURE;
		ddsd.dwWidth         = dwWidth;
		ddsd.dwHeight        = dwHeight;
		ddsd.ddpfPixelFormat = *(DDPIXELFORMAT*)lpParam;
		ddsd.ddsCaps.dwCaps2 = DDSCAPS2_TEXTUREMANAGE;
		break;}

	/*case SURFACETYPE_MANAGEDTEXTURE:{
		ddsd.dwFlags = DDSD_CAPS | DDSD_HEIGHT | DDSD_WIDTH | DDSD_TEXTURESTAGE | DDSD_PIXL
									}*/

	default:
		return NULL;
	}

	if( FAILED( hr = pDD->CreateSurface( &ddsd, &pddsTexture, NULL ) ) )
		return NULL;

	//special case...
	if(surfaceType==SURFACETYPE_3DCANVAS){
		//create 3d Device..

		LPDIRECT3D7 pD3D;
		if(!FAILED(hr = pDD->QueryInterface( IID_IDirect3D7, (VOID**)&pD3D ))){
			//enumerate z buffer format..
			DDPIXELFORMAT pixFormat;
			pD3D->EnumZBufferFormats(IID_IDirect3DHALDevice,EnumZBufferCallback,&pixFormat);

			DDSURFACEDESC2	ddsdesc;
			INIT_DIRECTDRAW_STRUCT(ddsdesc);
			pDD->GetDisplayMode(&ddsdesc);

			ddsd.dwFlags		= DDSD_CAPS|DDSD_WIDTH|DDSD_HEIGHT|DDSD_PIXELFORMAT;
			ddsd.ddsCaps.dwCaps = DDSCAPS_ZBUFFER|DDSCAPS_VIDEOMEMORY;
			ddsd.dwWidth		= ddsdesc.dwWidth;
			ddsd.dwHeight		= ddsdesc.dwHeight;
			ddsd.ddpfPixelFormat= pixFormat;

			LPDIRECTDRAWSURFACE7  pddsZBuffer=NULL;
			if( FAILED( hr = pDD->CreateSurface( &ddsd, &pddsZBuffer, NULL ) ) )
				return 0;
			if( FAILED( hr = pddsTexture->AddAttachedSurface( pddsZBuffer ) ) )
				return 0;

			SAFE_RELEASE2(pddsZBuffer);

			if(!FAILED(hr = pD3D->CreateDevice(IID_IDirect3DHALDevice,pddsTexture,(LPDIRECT3DDEVICE7 * )lpParam ))){
				pD3D->Release();
				(*(LPDIRECT3DDEVICE7* )lpParam)->SetRenderState( D3DRENDERSTATE_ZENABLE, TRUE );
				(*(LPDIRECT3DDEVICE7* )lpParam)->Clear(0,NULL,D3DCLEAR_ZBUFFER ,0,1.0,0);
				return pddsTexture;
			}
			 
			pD3D->Release();
		}
		pddsTexture->Release();
		return NULL;
	}

	return pddsTexture;
}


#define OutRefrenceCount(c) if(c){c->AddRef();UINT count=c->Release();TRACE(#c":RefCount:%d\n",count);}else{TRACE(#c":RefCount: is NULL\n");}
BOOL CDXD3Render::AdapterEnumCallback( GUID* pGUID, LPSTR strDesc,LPSTR strName, HMONITOR hmonitor)
{
	HRESULT       hr;

	LPDIRECTDRAW7 pDD  = NULL;

	if(!FAILED(hr = DirectDrawCreateEx( pGUID, (VOID**)&pDD, IID_IDirectDraw7, NULL ))){
		pDD->SetCooperativeLevel( NULL, DDSCL_NORMAL);
		
		LPDIRECTDRAWSURFACE7	pddsPrimary		= NULL;
		LPDIRECTDRAWSURFACE7	pddsBackBuffer	= NULL;
		LPDIRECTDRAWSURFACE7	pddsImageBuffer = NULL;
		LPDIRECT3DDEVICE7		pD3DDevice		= NULL;
		LPDIRECTDRAWSURFACE7	pddsZBuffer		= NULL;
		
		pddsPrimary    = CreateCustomSurface(pDD,SURFACETYPE_PRIMARY,NULL,0,0);
		if(!pddsPrimary) goto EXIT;
		pddsBackBuffer = CreateCustomSurface(pDD,SURFACETYPE_3DCANVAS,&pD3DDevice,0,0);
		if(!pddsBackBuffer) goto EXIT;

		
		m_pDD			= pDD;
		m_pddsPrimary	= pddsPrimary;
		m_pddsBackBuffer= pddsBackBuffer;
				
		m_pd3dDevice	= pD3DDevice;

		m_pDD->CreateClipper(0,&m_pcClipper,NULL);
		m_pddsPrimary->SetClipper( m_pcClipper );
		m_bCreate	= TRUE;
		
		return 0;//stop enumerate..
		
EXIT:
		SAFE_RELEASE(pDD);
		SAFE_RELEASE(pddsPrimary);
		SAFE_RELEASE(pddsImageBuffer);
		SAFE_RELEASE(pddsBackBuffer);
		SAFE_RELEASE(pD3DDevice);

		pDD->Release();	
	}
		 
	return DDENUMRET_OK; 
}


BOOL	CDXD3Render::Create()
{
	TRACE(_T("CD3DRenderer::Setup()\n"));
	DirectDrawEnumerateExA(	CDXD3Render::AdapterEnumCallbackWrapper, 
							this, 
							DDENUM_ATTACHEDSECONDARYDEVICES |
							DDENUM_DETACHEDSECONDARYDEVICES |
							DDENUM_NONDISPLAYDEVICES );
	return m_bCreate;
}

void	CDXD3Render::Destroy()
{
	SAFE_RELEASE(m_pddsBackBuffer);
	SAFE_RELEASE(m_pddsPrimary);
	SAFE_RELEASE(m_pd3dDevice);
	SAFE_RELEASE(m_pcClipper);	

	SAFE_RELEASE(m_pDD);

	for(DWORD i = 0 ; i < m_TotalImageBufCnt ; i++)DestroyImageBuf(i);
	
	m_TotalImageBufCnt = 0;
	m_bCreate = FALSE;
	TRACE(_T("CD3DRenderer::Endup()\n"));
}


DWORD CDXD3Render::CreateImageBuf(DWORD width, DWORD height, UMF_COLORFORMAT colorformat)
{
	//1. Find Same buffer 
	DWORD i;
	for(i = 0 ; i < m_TotalImageBufCnt ; i++){
		if((width == m_ImageBuf[i].width) && 
			(height == m_ImageBuf[i].height) && 
			(colorformat == m_ImageBuf[i].format)){
				m_ImageBuf[i].ref_count++;
				return i;
		}
	}

	// Find empty buffer
	DWORD FoundEmptyIndex = -1;
	for(i = 0 ; i < m_TotalImageBufCnt ; i++){
		if((m_ImageBuf[i].ref_count == 0) && (m_ImageBuf[i].pddsImageBuffer == NULL)){
			FoundEmptyIndex = i;
		}
	}

	if(-1 == FoundEmptyIndex){
		if((m_TotalImageBufCnt+1) >= MAX_IMAGEBUFFER){
			TRACE(_T("Current ImageBufCnt [%d] Exceed MAX Buf [%d]\n"),m_TotalImageBufCnt, MAX_IMAGEBUFFER);
			return -1;
		}
		FoundEmptyIndex = m_TotalImageBufCnt;
	}

	DDPIXELFORMAT pixelFormat;
	LPDIRECTDRAWSURFACE7	pddsImageBuffer;

	if(colorformat==UMF_RGB565)pixelFormat=DDPF_RGB565;
	else if(colorformat==UMF_RGB555)pixelFormat=DDPF_RGB555;
	else if(colorformat==UMF_RGBX32)pixelFormat=DDPF_RGB32;
	else if(colorformat==UMF_YUY2)pixelFormat=DDPF_YUY2;
	else if(colorformat==UMF_YV12)pixelFormat=DDPF_YV12; 
	else if(colorformat==UMF_YUV420)pixelFormat=DDPF_IYUV; 
	else{
		TRACE(_T("CDXD3Render does not support ColorFormat %d\n "), colorformat);
		return FALSE;
	}

	pddsImageBuffer = CreateCustomSurface(m_pDD,SURFACETYPE_VIDMEMTEXTURE,&pixelFormat,width, height);
	if(!pddsImageBuffer) return -1;

	m_ImageBuf[FoundEmptyIndex].width = width;
	m_ImageBuf[FoundEmptyIndex].height = height;
	m_ImageBuf[FoundEmptyIndex].format = colorformat;
	m_ImageBuf[FoundEmptyIndex].pddsImageBuffer = pddsImageBuffer;
	m_ImageBuf[FoundEmptyIndex].ref_count = 1;
	m_TotalImageBufCnt++;
	return FoundEmptyIndex;
}


void	CDXD3Render::DestroyImageBuf(DWORD ImageBufIndex)
{
	if(ImageBufIndex >= m_TotalImageBufCnt){
		TRACE(_T("Image Buffer[%d] not found TotalImageBuf [%d] \n"),ImageBufIndex, m_TotalImageBufCnt);
		return;
	}

	if((m_ImageBuf[ImageBufIndex].ref_count-1) == 0){
		SAFE_RELEASE(m_ImageBuf[ImageBufIndex].pddsImageBuffer);
		memset(&m_ImageBuf[ImageBufIndex], 0, sizeof(IMAGEBUFFER));
	}
}


BOOL	CDXD3Render::Recreate()
{
	Destroy();
	Create();

	//Recreate ImageBuf..
	return TRUE;
}

UINT	CDXD3Render::GetImageSize(DWORD EndId, DWORD ImageBufIndex, UMF_COLORFORMAT Imageformat)
{
	double bytePerPixel = 0;
	if(Imageformat==UMF_YV12){
		bytePerPixel = 3/2;
	}else{
		bytePerPixel = 4/2;
	}

	return (UINT)(m_ImageBuf[ImageBufIndex].width * m_ImageBuf[ImageBufIndex].height * bytePerPixel);
}

HRESULT	CDXD3Render::DrawImage(DWORD ImageBufIndex, PBYTE pImage, UMF_COLORFORMAT Imageformat, RECT TargetRect)
{
	if(!m_bCreate) return S_FALSE;

	if(ImageBufIndex >= m_TotalImageBufCnt){
		TRACE(_T("Image Buffer[%d] not found TotalImageBuf [%d] \n"),ImageBufIndex, m_TotalImageBufCnt);
		return S_FALSE;
	}

	DDSURFACEDESC2 desc={0,};
	HRESULT hr;
	desc.dwSize=sizeof(desc);
	SIZE	size = {m_ImageBuf[ImageBufIndex].width,m_ImageBuf[ImageBufIndex].height};
	RECT	SrcRect = {0,0,m_ImageBuf[ImageBufIndex].width,m_ImageBuf[ImageBufIndex].height};
	LPDIRECTDRAWSURFACE7 pddsImageBuffer = m_ImageBuf[ImageBufIndex].pddsImageBuffer; 
	
	hr = pddsImageBuffer->Lock(NULL,&desc,DDLOCK_WAIT ,NULL);
	if( FAILED(hr) ) return hr;
	ConvertColorFormat(pImage, Imageformat ,size, (BYTE*)desc.lpSurface ,m_ImageBuf[ImageBufIndex].format, desc.lPitch, 1);
	pddsImageBuffer->Unlock(NULL);

	hr = m_pddsBackBuffer->Blt(&TargetRect, pddsImageBuffer, &SrcRect, DDBLT_ASYNC |DDBLT_DONOTWAIT ,NULL);
	
	return S_OK;
}


HRESULT	CDXD3Render::Present(RECT srcRect, RECT destRect)
{
	if(!m_bCreate) return S_FALSE;
	HRESULT hr;
	hr = m_pddsPrimary->Blt(&destRect,m_pddsBackBuffer,&srcRect,DDBLT_ASYNC |DDBLT_DONOTWAIT,NULL);
	return hr;
}


HRESULT	CDXD3Render::Present(RECT ClientRect)
{
	if(!m_bCreate) return S_FALSE;
	if(!m_hWnd) return S_FALSE;
	
	HRESULT hr;
	RECT rect = ClientRect;
	ClientToScreen( m_hWnd, (POINT*)&rect.left );
	ClientToScreen( m_hWnd, (POINT*)&rect.right );

	hr = m_pddsPrimary->Blt(&rect,m_pddsBackBuffer,&ClientRect,DDBLT_ASYNC |DDBLT_DONOTWAIT,NULL);
	return hr;
}

void CDXD3Render::InitColorConversionFunctions()
{
	BOOL sse2_support = IsSSE2Supported();

	if( sse2_support ) {
		cc_YUY2_To_RGB32			= YUY2_To_RGB32			;
		cc_YUY2_To_RGB32_Resize_2	= YUY2_To_RGB32_Resize_2_SSE;//YUY2_To_RGB32_Resize_2;	
		cc_YUY2_To_RGB555			= YUY2_To_RGB555		;	
		cc_YUY2_To_RGB555_Resize_2	= YUY2_To_RGB555_Resize_2_2;
		cc_YUY2_To_RGB565			= YUY2_To_RGB565			;
		cc_YUY2_To_RGB565_Resize_2	= YUY2_To_RGB565_Resize_2_2;
		//cc_YUY2_To_RGB565_Resize_2	= YUY2_To_RGB565_Resize_2_SSE;
	}else{
		cc_YUY2_To_RGB32			= YUY2_To_RGB32_SSE			;
		cc_YUY2_To_RGB32_Resize_2	= YUY2_To_RGB32_Resize_2_SSE;	
		cc_YUY2_To_RGB555			= YUY2_To_RGB555_SSE		;	
		cc_YUY2_To_RGB555_Resize_2	= YUY2_To_RGB555_Resize_2_SSE;
		cc_YUY2_To_RGB565			= YUY2_To_RGB565_SSE		;	
		cc_YUY2_To_RGB565_Resize_2	= YUY2_To_RGB565_Resize_2_SSE;
	}
}


BOOL CDXD3Render::ConvertColorFormat( BYTE*	 _pSrc,
							    UMF_COLORFORMAT _eCFSource,
								SIZE		 _sizeSource,
								BYTE*		 _pDst,
								UMF_COLORFORMAT _eCFDest,
								DWORD		 _dwDstPitch,
								DWORD		 _dwScale_factor )
{
	if( _pSrc == NULL ) return FALSE;

	//_pDst의 BGR565는 모두 BGRX로 바뀔 예정
	const DWORD pixel_bytes[] = { 0,2,4,3,2,2 };	// bps
	//ErrDbg(1, "_ConvertColorFormat _pSrc %Xh, _pDst %Xh \r\n", _pSrc, _pDst);

	if( _eCFDest == _eCFSource )
	{
		if( _dwScale_factor == 1 ) {
			HSH_CopySameFormatImage( _pDst, _pSrc, _dwDstPitch, _sizeSource.cx, _sizeSource.cy, pixel_bytes[_eCFSource] );
			return TRUE;
		}
	}

	if( _eCFSource != UMF_YUY2 &&
		_eCFSource != UMF_YV12 &&
		_eCFSource != UMF_YUV420    ) { return FALSE; }

	switch( _eCFSource )
	{
	case UMF_YUY2:
		if( _eCFDest == UMF_YUY2 )
		{
			if( _dwScale_factor == 1 ) { HSH_CopyYUYVImage   ( _pDst, _pSrc, _dwDstPitch, _sizeSource.cx, _sizeSource.cy ); }
			else
			if( _dwScale_factor == 2 ) { HSH_CopyYUYVImageBy2( _pDst, _pSrc, _dwDstPitch, _sizeSource.cx, _sizeSource.cy ); }
			else
			if( _dwScale_factor == 4 ) { HSH_CopyYUYVImageBy4( _pDst, _pSrc, _dwDstPitch, _sizeSource.cx, _sizeSource.cy ); }
			else { return FALSE; }
		}
		else
		if( _eCFDest == UMF_RGB555 )
		{
			if( _dwScale_factor == 1 ) {
				if( _sizeSource.cx % 8 ) YUY2_To_RGB555_SSE(_pSrc, _pDst, _dwDstPitch, _sizeSource.cx, _sizeSource.cy );
				else					 cc_YUY2_To_RGB555( _pSrc, _pDst, _dwDstPitch, _sizeSource.cx, _sizeSource.cy );
			}
			else
			if( _dwScale_factor == 2 ) {
				cc_YUY2_To_RGB555_Resize_2( _pSrc, _pDst, _dwDstPitch, _sizeSource.cx, _sizeSource.cy );
			}
			else { return FALSE; }
		}
		else
		if( _eCFDest == UMF_RGB565 )
		{
			if( _dwScale_factor == 1 ) {
				if( _sizeSource.cx % 8 ) YUY2_To_RGB565_SSE( _pSrc, _pDst, _dwDstPitch, _sizeSource.cx, _sizeSource.cy );
				else					 cc_YUY2_To_RGB565( _pSrc, _pDst, _dwDstPitch, _sizeSource.cx, _sizeSource.cy );
			}
			else
			if( _dwScale_factor == 2 ) {
				cc_YUY2_To_RGB565_Resize_2( _pSrc, _pDst, _dwDstPitch, _sizeSource.cx, _sizeSource.cy );
			}
			else { return FALSE; }
		}
		else
		if( _eCFDest == UMF_RGBX32 )
		{
			if( _dwScale_factor == 1 ) {
				if( _sizeSource.cx % 8 ) YUY2_To_RGB32_SSE( _pSrc, _pDst, _dwDstPitch, _sizeSource.cx, _sizeSource.cy );
				else					 cc_YUY2_To_RGB32( _pSrc, _pDst, _dwDstPitch, _sizeSource.cx, _sizeSource.cy );
			}
			else
			if( _dwScale_factor == 2 ) {
				cc_YUY2_To_RGB32_Resize_2(_pSrc,_pDst,_dwDstPitch,_sizeSource.cx,_sizeSource.cy);
			}
			else { return FALSE; }
		}
		else { return FALSE; }
		break;

	case UMF_YUV420:
		if( _eCFDest == UMF_RGB565 )
		{
			//포인터에 대한 추가적인 변수가 필요하다..
			BYTE* u_src = _pSrc+_sizeSource.cx * _sizeSource.cy;
			BYTE* v_src = _pSrc+_sizeSource.cx * _sizeSource.cy * 5 / 4;
			if( _dwScale_factor == 1 ) {
				YUY12_To_RGB565_mmx(_pDst,_dwDstPitch,_pSrc,u_src,v_src,_sizeSource.cx,_sizeSource.cx/2,_sizeSource.cx,_sizeSource.cy);
			}
			else
			if( _dwScale_factor == 2 ) {
				YUY12_To_RGB565_mmx_resize_2(_pDst,_dwDstPitch,_pSrc,u_src,v_src,_sizeSource.cx,_sizeSource.cx/2,_sizeSource.cx,_sizeSource.cy);
			}
			else { return FALSE; }
		}
		else { return FALSE; }
		break;

	case UMF_YV12:
		if( _eCFDest == UMF_RGB565 )
		{
			//포인터에 대한 추가적인 변수가 필요하다..
			BYTE* u_src = _pSrc+_sizeSource.cx * _sizeSource.cy;
			BYTE* v_src = _pSrc+_sizeSource.cx * _sizeSource.cy * 5 / 4;
			if( _dwScale_factor == 1 ) {
				YUY12_To_RGB565_mmx(_pDst,_dwDstPitch,_pSrc,u_src,v_src,_sizeSource.cx,_sizeSource.cx/2,_sizeSource.cx,_sizeSource.cy);
			}
			else
			if( _dwScale_factor== 2 ) {
				YUY12_To_RGB565_mmx_resize_2(_pDst,_dwDstPitch,_pSrc,u_src,v_src,_sizeSource.cx,_sizeSource.cx/2,_sizeSource.cx,_sizeSource.cy);
			}
			else{ return FALSE; }
		}
		else { return FALSE; }
		break;
	}//switch( _eCFSource )

	return TRUE;
}
