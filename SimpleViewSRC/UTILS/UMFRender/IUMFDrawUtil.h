#ifndef __UMF_DRAW_UTILS_INTERFACE_H__
#define __UMF_DRAW_UTILS_INTERFACE_H__
#include "IUMFRender.h"

typedef enum eUMFDRAWRECORDTYPE{ 
	UMF_DRT_LINESTRIP = 1, 
	UMF_DRT_TEXT, 
	UMF_DRT_RECTLIST, 
	UMF_DRT_POLYGON, 
	UMF_DRT_CIRCLE, 
	UMF_DRT_CIRCULARARC,
	UMF_DRT_IMAGE,
	UMF_DRT_CURVE,
};

typedef enum eUMFBRUSHTYPE {
	UMF_BRT_SOLID		= 0,
	UMF_BRT_HATCH,
};


// Font creation flags
#define UMF_FONT_BOLD        0x0001
#define UMF_FONT_ITALIC      0x0002
#define UMF_FONT_ZENABLE     0x0004


class IUMFImage	//Surface default 32bit ARGB to draw device.
{
public:
	virtual BOOL	Init(IUMFRender *pUMFRender, USHORT width, USHORT height, eUMF_COLORFORMAT colorformat)	= 0;
	virtual void	Destroy() = 0;
	
	virtual eUMFRenderType		GetType()	= 0;
	virtual eUMF_COLORFORMAT	GetColorFormat()	= 0;	

	virtual HDC		GetDC() = 0;
	virtual void	ReleaseDC(HDC hdc) = 0;


	virtual BYTE*	GetBuf(DWORD *pPitch) = 0;
	virtual void	ReleaseBuf() = 0;

	virtual void	DestroySelf() = 0;
};


typedef struct 
{
	eUMFDRAWRECORDTYPE drawType;

	COLORREF Color;
	BYTE Alpha;
	eUMFBRUSHTYPE  BrushType;

	union {
		//POLYGON
		struct {
			ULONG  nPt;
			POINT* pPts;
		}polygon;

		//RECTs
		struct {
			ULONG nRect;
			RECT* pRects;
		}rects;

		struct {
			ULONG  nPt;
			POINT* pPts;
		}pts;

		//Text
		struct {
			ULONG	nIndex;	//Font index
			WCHAR*	pText;
			POINT	pt;		// start XY
			float	fScale;
		}text;

		//Circle
		struct {
			POINT	ptCentre;		// centre of circle
			double	fRadius;		// radius
			BOOL	bFill;
		}circle;

		struct {
			POINT			ptCentre;		// centre of circle
			double			fRadius;		// radius
			unsigned short	usWidth;		// The width of the arc
			double			dStartRadius;
			double			dFinishRadius;
		}circularArc;

		struct {
			RECT			rcTarget;
			IUMFImage*		pImage;
		}image;

		struct {
			ULONG	nPt;
			POINT	pPts[4];
		}curve;

	};
}UMFDRAWRECORD;


class IUMFDrawUtil
{
public:
	enum { MAX_FONT_INDEX	= 16 };

	virtual BOOL	Init(IUMFRender *pUMFRender) = 0;
	virtual void	Destroy() = 0;

	virtual eUMFRenderType	GetType() = 0;
	
	virtual void	SetCanvasRect(RECT CanvasRect) = 0;
	virtual SIZE	GetCanvasSize() = 0;

	virtual BOOL	InitFont(UINT index, const WCHAR* strFontName, DWORD dwHeight, DWORD dwFlags) = 0;
	virtual BOOL	GetTextExtent(UINT index, const WCHAR* strText, SIZE* pSize ) = 0;

	virtual DWORD	Push(UMFDRAWRECORD *pDrawItem) = 0;
	virtual void	Clear()	= 0;

	virtual BOOL	Commit() = 0;

	virtual void	DestroySelf() = 0;

	virtual IUMFImage*	CreateUMFImage(USHORT width, USHORT height, eUMF_COLORFORMAT colorformat) = 0;
};

extern "C" IUMFDrawUtil*	WINAPI CreateUMFDrawUtil(DWORD type);

#endif