#pragma once

#include <d3d9.h>
#include "UMFColorFormat.h"

typedef enum SurfaceType{
	SURFACETYPE_INVALID=0,
	SURFACETYPE_AGPTEXTURE=1,
	SURFACETYPE_VIDMEMTEXTURE=2,
	SURFACETYPE_3DCANVAS=3,
	SURFACETYPE_PRIMARY=4,
	SURFACETYPE_MANAGEDTEXTURE=5,
};

typedef struct 
{
	DWORD		width;
	DWORD		height;
	UMF_COLORFORMAT format;
	LPDIRECTDRAWSURFACE7	pddsImageBuffer;
	DWORD		ref_count;
}DX7IMAGEBUFFER;

COLORFORMAT GetColorFormat3DSupportFromBitInfoHeader(BITMAPINFOHEADER *pbm);


class CDXD3Render
{

public:
	enum {MAX_IMAGEBUFFER = 16};

	CDXD3Render(void);
	~CDXD3Render(void);

	BOOL	Create(HWND hWnd);
	void	Destroy();

	//Return ImageBuffer index
	DWORD	CreateImageBuf(DWORD width, DWORD height, COLORFORMAT SurfaceColorFormat);
	//Destory
	void	DestroyImageBuf(DWORD ImageBufIndex);

	BOOL	Recreate();

	LPDIRECT3D9				GetD3D(){return m_pD3D;}
	LPDIRECT3DTEXTURE9		GetD3BackBuf(){return m_pMainTexture;}
	
	UINT					GetImageSize(DWORD EndId, DWORD ImageBufIndex, COLORFORMAT Imageformat);

	HRESULT	DrawImage(DWORD ImageBufIndex, PBYTE pImage, COLORFORMAT ImageColorFormat, RECT TargetRect);
	
	HRESULT	Present(RECT ClientRect);
	HRESULT	Present(RECT srcRect, RECT destRect);

private:

	BOOL					m_bCreate;
	HWND					m_hWnd;

	LPDIRECT3D9				m_pD3D;
	LPDIRECT3DDEVICE9       m_pd3dDevice; 
	RECT					m_rcMonitor;

	LPDIRECT3DTEXTURE9      m_pMainTexture;
	LPDIRECT3DVERTEXBUFFER9	m_pMainVertext;
	

	DWORD					m_TotalImageBufCnt;
	IMAGEBUFFER				m_ImageBuf[MAX_IMAGEBUFFER];

	BOOL					DrawImageTexture(RECT t,RECT s,DWORD color,float zValue);
};

