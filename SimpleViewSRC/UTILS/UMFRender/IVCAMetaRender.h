#ifndef __UMF_RENDER_H__
#define __UMF_RENDER_H__

#include "UMFBase.h"

eUMF_COLORFORMAT GetColorFormat3DSupportFromBitInfoHeader(BITMAPINFOHEADER *pbm);


class IUMFRender
{
public:
	virtual BOOL	Open(HWND hWnd) = 0;
	virtual void	Close() = 0;

	//Return ImageBuffer index
	virtual DWORD	CreateImageBuf(DWORD width, DWORD height, eUMF_COLORFORMAT SurfaceColorFormat) = 0;
	//Destory
	virtual void	DestroyImageBuf(DWORD ImageBufIndex) = 0;

	virtual HRESULT	DrawImage(DWORD ImageBufIndex, PBYTE pImage, eUMF_COLORFORMAT ImageColorFormat, RECT TargetRect) = 0;

	virtual UINT	GetImageSize(DWORD EndId, DWORD ImageBufIndex, eUMF_COLORFORMAT Imageformat) = 0;
	
	virtual HRESULT	Present(RECT ClientRect) = 0;
	virtual HRESULT	Present(RECT srcRect, RECT destRect) = 0;

	virtual void DestroySelf() = 0;

//	virtual BOOL	GetParam() = 0;
};

IUMFRender* CreateUMFRender();

#endif