#ifndef __UMF_COLOR_FORMAT_H__
#define __UMF_COLOR_FORMAT_H__

typedef enum{
	CF_NOTDEFINED = 0,
	CF_YUY2,
	CF_BGRX,	//rgb32
	CF_BGR,		//rgb24
	CF_BGR565,
	CF_BGR555,
	CF_YV12,
	CF_IYUV
}COLORFORMAT;

#ifndef MAKEFOURCC
#define MAKEFOURCC(ch0, ch1, ch2, ch3)                              \
                ((DWORD)(BYTE)(ch0) | ((DWORD)(BYTE)(ch1) << 8) |   \
                ((DWORD)(BYTE)(ch2) << 16) | ((DWORD)(BYTE)(ch3) << 24 ))

#endif 

inline COLORFORMAT GetColorFormatFromBitInfoHeader(BITMAPINFOHEADER *pbm)
{
	COLORFORMAT format = CF_NOTDEFINED;
	if (MAKEFOURCC('Y','U','Y','2') == pbm->biCompression) {
		format = CF_YUY2;
	} else if(MAKEFOURCC('Y','V','1','2') == pbm->biCompression) {
		format = CF_YV12;
	}else if(MAKEFOURCC('I','Y','U','V') == pbm->biCompression) {
		format = CF_IYUV;
	} else { //RGB 
		if(pbm->biBitCount == 32){
			format = CF_BGRX;
		}else if(pbm->biBitCount == 24){
			format = CF_BGR;
		}else if(pbm->biBitCount == 16){
			format = CF_BGR565;
		}else if(pbm->biBitCount == 15){
			format = CF_BGR555;
		}
	}
	return format;
}

#endif