#include "StdAfx.h"

#include <afxwin.h>
#include "DXSDK/Include/d3dx9.h"
#include "DX9Render.h"
#include "DX9DrawUtil.h"
#include "triangulation.h"
#include "DX9Image.h"
#include "resource.h"

// COLORREF = 0x00bbggrr
// D3DCOLOR = 0xaarrggbb

// D3DCOLOR -> COLORREF

#ifndef TRACE
#include <atlbase.h>
#define TRACE AtlTrace
#endif

#pragma comment( lib, "../../Lib/DXSDK/LIB/D3dx9" )

#ifndef D3DCOLORTOCOLORREF
#define D3DCOLORTOCOLORREF( color ) (COLORREF)((color & 0xFF000000) | ((color & 0xFF0000) >> 16) | (color & 0xFF00) | ((color & 0xFF) << 16)     )
#endif

// COLORREF -> D3DCOLOR
#ifndef COLORREFTOD3DCOLOR
#define COLORREFTOD3DCOLOR( color )  (D3DCOLOR)((color & 0xFF000000) | ((color & 0xFF) << 16)     | (color & 0xFF00) | ((color & 0xFF0000) >> 16) ) 
#endif

#define		RADIUS_STEP				0.1
#define		MAXCOUNT_POLYGONEDGE	40

struct CUSTOMVERTEX
{
public:
	CUSTOMVERTEX() {};
	CUSTOMVERTEX( FLOAT fx, FLOAT fy, FLOAT fz, FLOAT frhw, D3DCOLOR color )
	{
		x = fx;
		y = fy;
		z = fz;
		rhw = frhw;
		diffuse = color;
	}

public:
	FLOAT       x,y,z;      // vertex untransformed position
	FLOAT       rhw;        // eye distance
	D3DCOLOR    diffuse;    // diffuse color
};

#define D3DFVF_CUSTOMVERTEX (D3DFVF_XYZRHW|D3DFVF_DIFFUSE)

struct CUSTOMVERTEX1
{
public:
	CUSTOMVERTEX1() {};
	CUSTOMVERTEX1( FLOAT fx, FLOAT fy, FLOAT fz, FLOAT frhw, D3DCOLOR color )
	{
		x = fx;
		y = fy;
		z = fz;
		rhw = frhw;
		diffuse = color;
		tu = 1.0f;
		tv = 1.0f;
	}

public:
	FLOAT       x,y,z;      // vertex untransformed position
	FLOAT       rhw;        // eye distance
	D3DCOLOR    diffuse;    // diffuse color
	FLOAT       tu, tv;     // texture relative coordinates
};

#define D3DFVF_CUSTOMVERTEX1 (D3DFVF_XYZRHW|D3DFVF_DIFFUSE|D3DFVF_TEX1)


CUSTOMVERTEX g_aRectsVertex[CDX9DrawUtil::MAX_VERTEX];
CUSTOMVERTEX1 g_aPolygonsVertex[CDX9DrawUtil::MAX_VERTEX];


void SetVertexZValue( CUSTOMVERTEX* _aptVertex, int indexFrom, int count, FLOAT _foZValue )
{
	for( int i = indexFrom; i < indexFrom + count; i++ ) {
		_aptVertex[i].z = _foZValue;
	}
}

void SetVertexColorValue( CUSTOMVERTEX* _aptVertex, int indexFrom, int count, DWORD _dwColor )
{
	for( int i = indexFrom; i < indexFrom + count; i++ ) {
		_aptVertex[i].diffuse = _dwColor;
	}
}

void EnableAlpha(LPDIRECT3DDEVICE9 pd3dDevice)
{
	pd3dDevice->SetTexture( 0, NULL );

	pd3dDevice->SetRenderState( D3DRS_ALPHABLENDENABLE,	TRUE );
	pd3dDevice->SetRenderState( D3DRS_SRCBLEND,			D3DBLEND_SRCALPHA );
	pd3dDevice->SetRenderState( D3DRS_DESTBLEND,		D3DBLEND_INVSRCALPHA );
	pd3dDevice->SetRenderState( D3DRS_ALPHATESTENABLE,	TRUE );
	pd3dDevice->SetRenderState( D3DRS_ALPHAREF,			0x08 );
	pd3dDevice->SetRenderState( D3DRS_ALPHAFUNC,		D3DCMP_GREATEREQUAL );
	pd3dDevice->SetRenderState( D3DRS_FILLMODE,			D3DFILL_SOLID );
	pd3dDevice->SetRenderState( D3DRS_CULLMODE,			D3DCULL_CCW );
	pd3dDevice->SetRenderState( D3DRS_ZENABLE,			TRUE );
}

BOOL UpdateBrushAlpha( IDirect3DSurface9* pd3dSurface, eUMFBRUSHTYPE brushType, BYTE _cbAlpha )
{
	D3DLOCKED_RECT D3DRect;

	unsigned char ucAlpha = 0xF0;
	DWORD dwPitch;

	if( SUCCEEDED(pd3dSurface->LockRect(&D3DRect, NULL, NULL)) )
	{
		D3DSURFACE_DESC desc;
		pd3dSurface->GetDesc(&desc);

		BYTE *pLine = (BYTE*)D3DRect.pBits;	// D3DFMT_X8R8G8B8
		dwPitch = D3DRect.Pitch;

		for( int y = 0; y < 16; y++, pLine += dwPitch )
		{
			unsigned int *pWord = (unsigned int *)pLine;

			// Modify the alpha slightly coz the textures don't show up so well
			unsigned int uiAlpha = _cbAlpha;

			uiAlpha = min( uiAlpha + 64, 255 );

			uiAlpha <<= 24;

			for( int x = 0; x < 16; x++, pWord++ )
			{
				// Simple test - if byte 0 is != 0 then we change the alpha
				// otherwise we assume it's totally transparent
				if( *pWord & 0x00FFFFFF )
				{
					unsigned int zwordb = *pWord;
					*pWord = (*pWord & 0x00FFFFFF) + uiAlpha;
					unsigned int zworda = *pWord;
					int zzz = 0;
					//*pWord = *pWord & 0x00FFFFFF;
				}
				else
				{
					*pWord = 0x00000000;
					int ssa = 0;
				}
			}
		}
		pd3dSurface->UnlockRect();
		
	} else {
		return FALSE;
	}

	return TRUE;
}


CDX9DrawUtil::CDX9DrawUtil()
{
	m_bInit		= FALSE;
	m_bReInit	= FALSE;	
	m_pd3dDevice = NULL;
	m_pRectVB  = NULL;
	m_pPolygonVB = NULL;
	m_nNextVertexDataRect = 0;
	m_nNextVertexData = 0;
	m_pHatchedBrushTexture = NULL;
	memset(m_pFont, 0, sizeof(CD3DFont*)* MAX_FONT_INDEX);
	_tcscpy_s(m_tcsFontName, sizeof(m_tcsFontName), _T("Arial"));
}

CDX9DrawUtil::~CDX9DrawUtil()
{
	Destroy();
}

BOOL CDX9DrawUtil::Init(IUMFRender *pUMFRender)
{
	if(!pUMFRender) return FALSE;
	if(pUMFRender->GetType() != UMF_DX9Render) return FALSE;


	m_pUMFRender = (CDX9Render *)pUMFRender;
	m_pd3dDevice = m_pUMFRender->GetD3dDevice();
	if(m_pd3dDevice == NULL) return FALSE;
	
	BOOL bReInit = FALSE;

	//Recreate all things
	if(m_bInit){
		m_bReInit	= TRUE;
		bReInit		= TRUE;
		Destroy();
	}
	
	// Init VertexBuffer for RectList
	if( FAILED( m_pd3dDevice->CreateVertexBuffer( 
		MAX_VERTEX*sizeof(CUSTOMVERTEX),
		D3DUSAGE_WRITEONLY,
		D3DFVF_CUSTOMVERTEX,
		D3DPOOL_MANAGED, // D3DPOOL_DEFAULT ?
		&m_pRectVB, NULL ) ) )
	{
		m_bInit = FALSE;
		goto LCleanReturn;
	}

	// Init VertexBuffer for PolygonVCA
	if( FAILED( m_pd3dDevice->CreateVertexBuffer( 
		MAX_VERTEX*sizeof(CUSTOMVERTEX1),
		D3DUSAGE_WRITEONLY,
		D3DFVF_CUSTOMVERTEX1,
		D3DPOOL_MANAGED, // D3DPOOL_DEFAULT ?
		&m_pPolygonVB, NULL ) ) )
	{
		m_bInit = FALSE;
		goto LCleanReturn;
	}

	// Init Brush
 	HRESULT hr;
	hr = D3DXCreateTextureFromResource(m_pd3dDevice, AfxFindResourceHandle(MAKEINTRESOURCE(IDB_HATCH), _T("BITMAP")), MAKEINTRESOURCE(IDB_HATCH), &m_pHatchedBrushTexture);
	if(FAILED(hr)){
		m_bInit = FALSE;
		goto LCleanReturn;
	}

	m_bInit = TRUE;
	if(!bReInit){
		if(!InitFont(0,NULL,13,0)) {	// temporarily default font height
			m_bInit = FALSE;
			goto LCleanReturn;
		}
	}

	return TRUE;

LCleanReturn:
	Destroy();

	return FALSE;
}


void CDX9DrawUtil::Destroy()
{
	for(UINT i = 0; i< MAX_FONT_INDEX ; i++){
		if(m_bReInit){
			if(m_pFont[i])	 ReInitFont(i);
		}else{
			DeleteFont(i);
		}
	}
	
	if(m_pRectVB) {
		m_pRectVB->Release();
		m_pRectVB = NULL;
	}

	if(m_pPolygonVB) {
		m_pPolygonVB->Release();
		m_pPolygonVB = NULL;
	}

 	if(m_pHatchedBrushTexture) {
 		m_pHatchedBrushTexture->Release();
 		m_pHatchedBrushTexture = NULL;
 	}

	m_bReInit	= FALSE;
	m_bInit		= FALSE;	
}


BOOL CDX9DrawUtil::InitFont(UINT index, const WCHAR* strFontName, DWORD dwHeight, DWORD dwFlags)
{
	if(m_bInit == FALSE) return FALSE;
	if(index > MAX_FONT_INDEX) return FALSE;

	DeleteFont(index);
	
	m_pFont[index] = new CD3DFont(strFontName?strFontName:m_tcsFontName, dwHeight, dwFlags);

	if(FAILED(m_pFont[index]->InitDeviceObjects( m_pd3dDevice ))) {
		DeleteFont(index);
		return FALSE;
	}

	if(FAILED(m_pFont[index]->RestoreDeviceObjects())) {
		DeleteFont(index);
		return FALSE;
	}

	return TRUE;
}

void CDX9DrawUtil::DeleteFont(UINT index)
{
	if(m_pFont[index]) {
		m_pFont[index]->InvalidateDeviceObjects();
		m_pFont[index]->DeleteDeviceObjects();
		delete m_pFont[index];
		m_pFont[index] = NULL;
	}
}

BOOL CDX9DrawUtil::ReInitFont(UINT index)
{
	if(NULL == m_pFont[index]) return FALSE;

	m_pFont[index]->InvalidateDeviceObjects();
	m_pFont[index]->DeleteDeviceObjects();

	if(FAILED(m_pFont[index]->InitDeviceObjects( m_pd3dDevice ))) {
		DeleteFont(index);
		return FALSE;
	}

	if(FAILED(m_pFont[index]->RestoreDeviceObjects())) {
		DeleteFont(index);
		return FALSE;
	}

	return TRUE;
}


BOOL CDX9DrawUtil::GetTextExtent(UINT index, const WCHAR* strText, SIZE* pSize )
{
	if(m_bInit == FALSE) return FALSE;
	if(index > MAX_FONT_INDEX) return FALSE;
	if(!m_pFont[index]) return FALSE;

	HRESULT hResult = m_pFont[index]->GetTextExtent(strText, pSize);

	if(SUCCEEDED(hResult)) return TRUE;

	return FALSE;
}

BOOL CDX9DrawUtil::Commit()
{
	if(m_pd3dDevice == NULL)
		return FALSE;
	EnableAlpha(m_pd3dDevice);
	m_pd3dDevice->SetRenderState( D3DRS_FILLMODE,			D3DFILL_SOLID );
	m_pd3dDevice->SetRenderState( D3DRS_CULLMODE,			D3DCULL_CCW );

	UMFDRAWRECORD* paRecords = m_DrawRecoder.GetRecords();
	
	if(SUCCEEDED( m_pd3dDevice->BeginScene()))
	{
		m_nNextVertexDataRect = 0;
		m_nNextVertexData = 0;
		for(int i=0; i<(int)m_DrawRecoder.GetRecordCnt(); i++)
		{
			DrawRecord(&paRecords[i]);
		}
		m_pd3dDevice->EndScene();
		m_DrawRecoder.Clear();
	}

	return TRUE;
}


BOOL CDX9DrawUtil::DrawRecord(UMFDRAWRECORD* pRecord)
{
	D3DCOLOR color = COLORREFTOD3DCOLOR( (pRecord->Color | (pRecord->Alpha<<8*3)));
	eUMFBRUSHTYPE BrushType = pRecord->BrushType;

	if(pRecord->drawType == UMF_DRT_TEXT) {
		if(pRecord->text.nIndex > MAX_FONT_INDEX) return FALSE;
		if(NULL == m_pFont[pRecord->text.nIndex]) return FALSE;
		
		m_pFont[pRecord->text.nIndex]->DrawText( 
			(FLOAT)(m_CanvasRect.left + pRecord->text.pt.x),
			(FLOAT)(m_CanvasRect.top + pRecord->text.pt.y), 
			color,
			pRecord->text.pText );

	} else if(pRecord->drawType == UMF_DRT_RECTLIST) {
		RECT *pRects = pRecord->rects.pRects;
		UINT nRectsCnt = pRecord->rects.nRect;
		
		ULONG n = FillRectListVertex(pRects, nRectsCnt, color);
		if(n == 0) return FALSE;

		m_pd3dDevice->SetStreamSource( 0, m_pRectVB, m_nNextVertexDataRect - (n*sizeof(CUSTOMVERTEX)), sizeof(CUSTOMVERTEX) );
		m_pd3dDevice->SetFVF( D3DFVF_CUSTOMVERTEX );
		m_pd3dDevice->DrawPrimitive( D3DPT_TRIANGLELIST, 0, nRectsCnt*2 );

	} else if(pRecord->drawType == UMF_DRT_LINESTRIP) {
		//DrawPrimitive...
		POINT *pPts = pRecord->pts.pPts;
		UINT nPtCnt = pRecord->pts.nPt;

		UINT n = FillLineStripVertex(pPts, nPtCnt, color);

		m_pd3dDevice->SetStreamSource( 0, m_pRectVB, m_nNextVertexDataRect - (nPtCnt*sizeof(CUSTOMVERTEX)), sizeof(CUSTOMVERTEX) );
		m_pd3dDevice->SetFVF( D3DFVF_CUSTOMVERTEX );
		m_pd3dDevice->DrawPrimitive( D3DPT_LINESTRIP, 0, n );

	} else if(pRecord->drawType == UMF_DRT_CIRCLE) {
		if(!DrawPolygonCircle(pRecord->circle.ptCentre, pRecord->circle.fRadius, color)) {
			return FALSE;
		}

	} else if(pRecord->drawType == UMF_DRT_CIRCULARARC) {
		if(!DrawArc(pRecord->circularArc.ptCentre, 
					pRecord->circularArc.fRadius, 
					pRecord->circularArc.usWidth, 
					pRecord->circularArc.dStartRadius, 
					pRecord->circularArc.dFinishRadius, 
					color)) {
			return FALSE;
		}
		return FALSE;

	} else if(pRecord->drawType == UMF_DRT_POLYGON) {
		POINT *pPts = pRecord->pts.pPts;
		UINT nPtCnt = pRecord->pts.nPt;

		if(!DrawPolygonVCA(pPts, nPtCnt, color, BrushType)) {
			return FALSE;
		}
	}else if(pRecord->drawType == UMF_DRT_IMAGE){

		CDX9Image *pImage = (CDX9Image *)pRecord->image.pImage;
		RECT	TargetRect = pRecord->image.rcTarget;
		pImage->Update();
		DrawTexture(TargetRect, pImage->GetTexture());
/*		
		if(!pImage) return FALSE;
		LPDIRECT3DTEXTURE9	pTexture = pImage->GetTexture();
		DrawTexture(TargetRect, pTexture);

		LPDIRECT3DSURFACE9	pD3DSurf = pImage->GetSurface();
		LPDIRECT3DSURFACE9	backBuffer;
		if(!pD3DSurf) return FALSE;
		if( FAILED(m_pd3dDevice->GetBackBuffer(0,0,D3DBACKBUFFER_TYPE_MONO, &backBuffer )) ){
			return FALSE;
		}else{
			m_pd3dDevice->SetRenderState(D3DRS_ALPHABLENDENABLE,TRUE);
			m_pd3dDevice->SetRenderState(D3DRS_SRCBLEND,D3DBLEND_SRCALPHA);
			m_pd3dDevice->SetRenderState(D3DRS_DESTBLEND,D3DBLEND_INVSRCALPHA);

			
		
			m_pd3dDevice->SetRenderState( D3DRS_ALPHATESTENABLE,	TRUE );
			m_pd3dDevice->SetRenderState( D3DRS_ALPHAREF,			0x08 );
			m_pd3dDevice->SetRenderState( D3DRS_ALPHAFUNC,			D3DCMP_GREATEREQUAL );
			m_pd3dDevice->SetRenderState( D3DRS_FILLMODE,			D3DFILL_SOLID );
			m_pd3dDevice->SetRenderState( D3DRS_CULLMODE,			D3DCULL_CCW );
			m_pd3dDevice->SetRenderState( D3DRS_ZENABLE,			TRUE );

			
			m_pd3dDevice->StretchRect(pD3DSurf, NULL, backBuffer, &TargetRect, D3DTEXF_LINEAR);
		}

//		pTexture->Release();
*/
	}else if(pRecord->drawType == UMF_DRT_CURVE){
		DrawCurve(pRecord->curve.pPts, pRecord->curve.nPt, color);
	}

	return TRUE;
}


BOOL CDX9DrawUtil::DrawTexture(RECT	TargetRect, LPDIRECT3DTEXTURE9 pTexture)
{
	HRESULT hr;
	RECT rc;

	rc.left = m_CanvasRect.left	+ TargetRect.left;
	rc.top	= m_CanvasRect.top	+ TargetRect.top;
	rc.right= m_CanvasRect.left + TargetRect.right;
	rc.bottom=m_CanvasRect.top + TargetRect.bottom;

	
	// 3DCanvas의 어디로 옮길것인지 결정한다.
	int x1 = rc.left+2;
	int y1 = rc.top+2;
	int x2 = rc.right-2;
	int y2 = rc.bottom-2;

	float fx1 = (float)x1 - 0.5f;
	float fy1 = (float)y1 - 0.5f;
	float fx2 = (float)x2 - 0.5f;
	float fy2 = (float)y2 - 0.5f;

	// 입력Texture 좌표.. 일단 pixel좌표를 쓴다.
	float u1 = 0;//(float)rcSrc.left;
	float v1 = 0;//(float)rcSrc.top;
	float u2 = 1;//(float)rcSrc.right;
	float v2 = 1;//(float)rcSrc.bottom;

	
	// Determine the size of data to be moved into the vertex buffer.
	UINT nSizeOfData = 4 * sizeof(CUSTOMVERTEX1);
    // No overwrite will be used if the vertices can fit into 
    //   the space remaining in the vertex buffer.
    DWORD dwLockFlags = D3DLOCK_NOOVERWRITE;
    // Check to see if the entire vertex buffer has been used up yet.
	UINT nSizeOfVB = MAX_VERTEX*sizeof(CUSTOMVERTEX1);
    if( m_nNextVertexData > nSizeOfVB - nSizeOfData )
    {
        // No space remains. Start over from the beginning 
        //   of the vertex buffer.
        dwLockFlags = D3DLOCK_DISCARD;
        m_nNextVertexData = 0;
    }

	// Lock the vertex buffer.
	CUSTOMVERTEX1	*p_vertices;
	hr = m_pPolygonVB->Lock((UINT)m_nNextVertexData, nSizeOfData, (VOID **)(&p_vertices), dwLockFlags);
    if( FAILED(hr) ){
        TRACE(_T("%s:%d (hr=0x%0lX)"), __FUNCTION__, __LINE__, hr);
		goto EXIT;
    }


    /* -0.5f is a "feature" of DirectX and it seems to apply to Direct3d also */
    /* http://www.sjbrown.co.uk/2003/05/01/fix-directx-rasterisation/ */
    p_vertices[0].x       = fx1;       // left
    p_vertices[0].y       = fy2;       // top
    p_vertices[0].z       = 0.0f;
    p_vertices[0].diffuse = D3DCOLOR_ARGB(255, 255, 255, 255);
    p_vertices[0].rhw     = 1.0f;
    p_vertices[0].tu      = u1;
    p_vertices[0].tv      = v2;

    p_vertices[1].x       = fx1;    // right
    p_vertices[1].y       = fy1;       // top
    p_vertices[1].z       = 0.0f;
    p_vertices[1].diffuse = D3DCOLOR_ARGB(255, 255, 255, 255);
    p_vertices[1].rhw     = 1.0f;
    p_vertices[1].tu      = u1;
    p_vertices[1].tv      = v1;

    p_vertices[2].x       = fx2;    // right
    p_vertices[2].y       = fy2;   // bottom
    p_vertices[2].z       = 0.0f;
    p_vertices[2].diffuse = D3DCOLOR_ARGB(255, 255, 255, 255);
    p_vertices[2].rhw     = 1.0f;
    p_vertices[2].tu      = u2;
    p_vertices[2].tv      = v2;

    p_vertices[3].x       = fx2;       // left
    p_vertices[3].y       = fy1;   // bottom
    p_vertices[3].z       = 0.0f;
    p_vertices[3].diffuse = D3DCOLOR_ARGB(255, 255, 255, 255);
    p_vertices[3].rhw     = 1.0f;
    p_vertices[3].tu      = u2;
    p_vertices[3].tv      = v1;

    hr= m_pPolygonVB->Unlock();
	m_nNextVertexData += nSizeOfData;
	if( FAILED(hr) ){
        TRACE(_T("%s:%d (hr=0x%0lX)"), __FUNCTION__, __LINE__, hr);
	    goto EXIT;
    }

	// Draw the front and back faces of the cube using texture 1
	m_pd3dDevice->SetTexture( 0, pTexture );

	m_pd3dDevice->SetTextureStageState( 0, D3DTSS_ALPHAARG1,	D3DTA_TEXTURE);
	m_pd3dDevice->SetTextureStageState( 0, D3DTSS_ALPHAOP,	D3DTOP_SELECTARG1);

	m_pd3dDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_pd3dDevice->SetRenderState( D3DRS_SRCBLEND,			D3DBLEND_SRCALPHA );
	m_pd3dDevice->SetRenderState( D3DRS_DESTBLEND,			D3DBLEND_INVSRCALPHA );

	
	// Render the vertex buffer contents
    hr = m_pd3dDevice->SetStreamSource(0, m_pPolygonVB, m_nNextVertexData - sizeof(CUSTOMVERTEX1)*4, sizeof(CUSTOMVERTEX1));
    if( FAILED(hr) ){
        TRACE(_T("%s:%d (hr=0x%0lX)"), __FUNCTION__, __LINE__, hr);
		goto EXIT;
    }

    // we use FVF instead of vertex shader
    hr = m_pd3dDevice->SetFVF(D3DFVF_CUSTOMVERTEX1);
    if( FAILED(hr) ){
        TRACE(_T("%s:%d (hr=0x%0lX)"), __FUNCTION__, __LINE__, hr);
		goto EXIT;
    }

    // draw rectangle
    hr = m_pd3dDevice->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);
    if( FAILED(hr) ){
        TRACE(_T("%s:%d (hr=0x%0lX)"), __FUNCTION__, __LINE__, hr);
		goto EXIT;
    }

	return TRUE;

EXIT :
	return FALSE;
}


BOOL CDX9DrawUtil::DrawPolygonVCA(POINT *pPts, UINT nPtCnt, D3DCOLOR color, eUMFBRUSHTYPE BrushType)
{
	//JL : Changed this from ULONG to work in the test (n < 0)
	LONG n, i;

	n = TriangulateWrapper(pPts, nPtCnt, color);
	if(n < 0) 
	{
		return FALSE;
	}
//	DbgMsg(_T("Triangulated points= %d\n"), n);

	// See if we have to apply a texture
	// DWORD stateHandle;
	IDirect3DStateBlock9* pStateBlock = NULL;
	if( UMF_BRT_SOLID != BrushType )
	{
		// save the current device state
		m_pd3dDevice->CreateStateBlock( D3DSBT_ALL, &pStateBlock);
		EnableAlpha(m_pd3dDevice);
		
	    m_pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);

		IDirect3DSurface9* pd3dSurface = NULL;
		m_pHatchedBrushTexture->GetSurfaceLevel(0, &pd3dSurface);
		UpdateBrushAlpha( pd3dSurface, BrushType, (BYTE)(color >> 24) );
		pd3dSurface->Release();

		m_pd3dDevice->SetSamplerState( 0, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP  );
		m_pd3dDevice->SetSamplerState( 0, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP  );
		m_pd3dDevice->SetTexture( 0, m_pHatchedBrushTexture );

		// Add offset for correct texel-pixel mapping
		CUSTOMVERTEX1* vertexs;
		m_pPolygonVB->Lock( m_nNextVertexData - sizeof(CUSTOMVERTEX1)*n, sizeof(CUSTOMVERTEX1)*n, (void**)&vertexs, 0 );
		for( i = 0; i < n; i++ )
		{
			CUSTOMVERTEX1& v = vertexs[i];
			v.tu = v.x/BRUSH_TEXTURESIZE;
			v.tv = v.y/BRUSH_TEXTURESIZE;

			v.x -= 0.5f;
			v.y -= 0.5f;
		}
		m_pPolygonVB->Unlock();
	}

	m_pd3dDevice->SetStreamSource(0, m_pPolygonVB, m_nNextVertexData - sizeof(CUSTOMVERTEX1)*n, sizeof(CUSTOMVERTEX1));
	m_pd3dDevice->SetFVF(D3DFVF_CUSTOMVERTEX1);
	//JL TODO: Change the Triangulate function so that we can call DrawPrimitive only once. 
	//That means setting up the vertex buffer with the correct vertices in the Triangulate function.
	//For now we are drawing each triangle at a time.
//	m_pd3dDevice->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, n-2);
 	for(i = 0; i < n/4; i++) {
 		m_pd3dDevice->DrawPrimitive(D3DPT_TRIANGLELIST, i*4, 1);
 	}

	// BW - if we drew with a brush, then draw a line without texture round the edge to box it all in...
	if( UMF_BRT_SOLID != BrushType )
	{
		m_pd3dDevice->SetTexture( 0, NULL );
//		m_pd3dDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);

		int n = 0;
		// Rescale the original co-ordinates into line segments

		// Determine the size of data to be moved into the vertex buffer.
		UINT nSizeOfData = (nPtCnt+1) * sizeof(CUSTOMVERTEX1);
		// No overwrite will be used if the vertices can fit into 
		//   the space remaining in the vertex buffer.
		DWORD dwLockFlags = D3DLOCK_NOOVERWRITE;
		// Check to see if the entire vertex buffer has been used up yet.
		UINT nSizeOfVB = MAX_VERTEX*sizeof(CUSTOMVERTEX1);
		if( m_nNextVertexData > nSizeOfVB - nSizeOfData )
		{
			// No space remains. Start over from the beginning 
			//   of the vertex buffer.
			dwLockFlags = D3DLOCK_DISCARD;
			m_nNextVertexData = 0;
		}

		CUSTOMVERTEX1* v;
		m_pPolygonVB->Lock( (UINT)m_nNextVertexData, nSizeOfData, (void**)&v, dwLockFlags );
		for( ULONG j = 0; j < nPtCnt; j++ )
		{
			v[n].x = (float)(pPts[j].x + m_CanvasRect.left);
			v[n].y = (float)(pPts[j].y + m_CanvasRect.top);
			v[n].z = 0;
			v[n].rhw = 1;
			v[n].diffuse = color;
			n++;
		}

		// Connect the last segment
		v[n].x = v[0].x;
		v[n].y = v[0].y;
		v[n].z = 0;
		v[n].rhw = 1;
		v[n].diffuse = color;

		m_pPolygonVB->Unlock();
		m_nNextVertexData += nSizeOfData;

		m_pd3dDevice->SetStreamSource( 0, m_pPolygonVB, m_nNextVertexData - nSizeOfData, sizeof(CUSTOMVERTEX1) );
		m_pd3dDevice->SetFVF( D3DFVF_CUSTOMVERTEX1 );
		m_pd3dDevice->DrawPrimitive( D3DPT_LINESTRIP, 0, n );

		// restore previous state
		pStateBlock->Apply();
		pStateBlock->Release();
	}

	return TRUE;
}


UINT CDX9DrawUtil::FillRectListVertex(RECT *pRects, UINT nRectsCnt, D3DCOLOR color)
{
	ULONG n = 0;

	if( nRectsCnt*6 >= CDX9DrawUtil::MAX_VERTEX ) return 0;

	SIZE srcSize = { (m_CanvasRect.right-m_CanvasRect.left), (m_CanvasRect.bottom-m_CanvasRect.top)};
	
	for( ULONG i=0; i < nRectsCnt; i++ )
	{
		//clip rect..			
		RECT rc = *pRects;
		rc.left   = max( 0, rc.left );
		rc.top    = max( 0, rc.top );
		rc.right  = min( srcSize.cx, rc.right );
		rc.bottom = min( srcSize.cy, rc.bottom );

		// Triangle Vertext를 만든다.
		// 뿌릴 사각형을 삼각형 두개로 표현한다.
		FLOAT fPtX = (FLOAT)(m_CanvasRect.left + rc.left);
		FLOAT fPtY = (FLOAT)(m_CanvasRect.top + rc.top);
		g_aRectsVertex[n+0] = CUSTOMVERTEX( fPtX,  fPtY, 0.0f, 1.0f, color );

		fPtX = (FLOAT)(m_CanvasRect.left + rc.right);
		fPtY = (FLOAT)(m_CanvasRect.top + rc.bottom);
		g_aRectsVertex[n+1] = CUSTOMVERTEX( fPtX,  fPtY, 0.0f, 1.0f, color );

		fPtX = (FLOAT)(m_CanvasRect.left + rc.left);
		fPtY = (FLOAT)(m_CanvasRect.top + rc.bottom);
		g_aRectsVertex[n+2] = CUSTOMVERTEX( fPtX,  fPtY, 0.0f, 1.0f, color );

		fPtX = (FLOAT)(m_CanvasRect.left + rc.left);
		fPtY = (FLOAT)(m_CanvasRect.top + rc.top);
		g_aRectsVertex[n+3] = CUSTOMVERTEX( fPtX,  fPtY, 0.0f, 1.0f, color );

		fPtX = (FLOAT)(m_CanvasRect.left + rc.right);
		fPtY = (FLOAT)(m_CanvasRect.top + rc.top);
		g_aRectsVertex[n+4] = CUSTOMVERTEX( fPtX,  fPtY, 0.0f, 1.0f, color );

		fPtX = (FLOAT)(m_CanvasRect.left + rc.right);
		fPtY = (FLOAT)(m_CanvasRect.top + rc.bottom);
		g_aRectsVertex[n+5] = CUSTOMVERTEX( fPtX,  fPtY, 0.0f, 1.0f, color );

		n += 6;
		pRects++;
	}

	// Determine the size of data to be moved into the vertex buffer.
	UINT nSizeOfData = n * sizeof(CUSTOMVERTEX);
    // No overwrite will be used if the vertices can fit into 
    //   the space remaining in the vertex buffer.
    DWORD dwLockFlags = D3DLOCK_NOOVERWRITE;
    // Check to see if the entire vertex buffer has been used up yet.
	UINT nSizeOfVB = MAX_VERTEX*sizeof(CUSTOMVERTEX);
    if( m_nNextVertexDataRect > nSizeOfVB - nSizeOfData )
    {
        // No space remains. Start over from the beginning 
        //   of the vertex buffer.
        dwLockFlags = D3DLOCK_DISCARD;
        m_nNextVertexDataRect = 0;
    }

	CUSTOMVERTEX* v;
	if( FAILED( m_pRectVB->Lock( m_nNextVertexDataRect, nSizeOfData, (void**)&v, dwLockFlags ) ) )
		return 0;
	memcpy( v, g_aRectsVertex, sizeof(CUSTOMVERTEX)*n );
	m_pRectVB->Unlock();
	m_nNextVertexDataRect += nSizeOfData;

	return n;
}

UINT CDX9DrawUtil::FillLineStripVertex(POINT *pPts, UINT nPtCnt, D3DCOLOR color)
{
	if( nPtCnt >= CDX9DrawUtil::MAX_VERTEX ) return 0;

	UINT n = 0;
	// assume that src size is always same as dest rect size.
	SIZE srcSize = { (m_CanvasRect.right-m_CanvasRect.left), (m_CanvasRect.bottom-m_CanvasRect.top)};

	// Determine the size of data to be moved into the vertex buffer.
	UINT nSizeOfData = nPtCnt * sizeof(CUSTOMVERTEX);
    // No overwrite will be used if the vertices can fit into 
    //   the space remaining in the vertex buffer.
    DWORD dwLockFlags = D3DLOCK_NOOVERWRITE;
    // Check to see if the entire vertex buffer has been used up yet.
	UINT nSizeOfVB = MAX_VERTEX*sizeof(CUSTOMVERTEX);
    if( m_nNextVertexDataRect > nSizeOfVB - nSizeOfData )
    {
        // No space remains. Start over from the beginning 
        //   of the vertex buffer.
        dwLockFlags = D3DLOCK_DISCARD;
        m_nNextVertexDataRect = 0;
    }

	CUSTOMVERTEX* v;
	m_pRectVB->Lock( m_nNextVertexDataRect, nSizeOfData, (void**)&v, dwLockFlags );
	for( UINT i=0; i < nPtCnt; i++ ) {
		//clip rect..
		POINT pt = pPts[i];
		pt.x = max( 0, pt.x );
		pt.x = min( srcSize.cx, pt.x );
		pt.y = max( 0, pt.y );
		pt.y = min( srcSize.cy, pt.y );
		FLOAT fPtX = (FLOAT)(m_CanvasRect.left + pt.x);
		FLOAT fPtY = (FLOAT)(m_CanvasRect.top + pt.y);

		v[n] = CUSTOMVERTEX( fPtX,  fPtY, 0.0f, 1.0f, color );

		n++;
		//		pPt++;
	} 
	m_pRectVB->Unlock();
	m_nNextVertexDataRect += nSizeOfData;

	return n-1;
}

INT CDX9DrawUtil::TriangulateWrapper( POINT* pPts, UINT nPtCnt, D3DCOLOR color )
{
	unsigned j;
	unsigned tcount;
	Vector2dVector polygonpt, triangleresult;

	int trW = m_CanvasRect.right  - m_CanvasRect.left;
	int trH = m_CanvasRect.bottom - m_CanvasRect.top;
	ULONG n = 0;

	FLOAT fPtX = 0.0f, fPtY = 0.0f;

	if ( nPtCnt > 3 )
	{
		for ( j = 0; j < nPtCnt; j++ )
			polygonpt.push_back( Vector2d( (float) pPts[j].x,  (float) pPts[j].y ) );

		if ( !Triangulate::Process( polygonpt, triangleresult ) )
		{
			return -1;
		}

		tcount = (unsigned)triangleresult.size()/3;  
	}
	else if ( nPtCnt == 3 )
	{
		for ( j = 0; j < 3; j++ )
			triangleresult.push_back( Vector2d( (float) pPts[j].x, (float) pPts[j].y ) );

		tcount = 1;
	}
	else
		return 0;

//	DbgMsg(_T("tcount= %d\n"), tcount);

	if( tcount*4 >= CDX9DrawUtil::MAX_VERTEX ) return 0;

	for ( j = 0; j < tcount ; j++ )
	{

		const Vector2d &p0 = triangleresult[j*3+0];
		const Vector2d &p1 = triangleresult[j*3+1];
		const Vector2d &p2 = triangleresult[j*3+2];

		POINT pt0, pt1, pt2;
		pt0.x = (LONG) p0.GetX();
		pt0.y = (LONG) p0.GetY();

		pt1.x = (LONG) p1.GetX();
		pt1.y = (LONG) p1.GetY();

		pt2.x = (LONG) p2.GetX();
		pt2.y = (LONG) p2.GetY();

		pt0.x = max( 0, pt0.x );
		pt0.x = min( trW, pt0.x );
		pt0.y = max( 0, pt0.y );
		pt0.y = min( trH, pt0.y );

		pt1.x = max( 0, pt1.x );
		pt1.x = min( trW, pt1.x );
		pt1.y = max( 0, pt1.y );
		pt1.y = min( trH, pt1.y );

		pt2.x = max( 0, pt2.x );
		pt2.x = min( trW, pt2.x );
		pt2.y = max( 0, pt2.y );
		pt2.y = min( trH, pt2.y );

		fPtX = (FLOAT)(pt0.x + m_CanvasRect.left);
		fPtY = (FLOAT)(pt0.y + m_CanvasRect.top);	//0x01000000
		g_aPolygonsVertex[n+0] = CUSTOMVERTEX1( fPtX,  fPtY, 0.0f, 1.0f, color);
//		DbgMsg(_T("(%3d, %3d), (%3f, %3f)\n"), pt0.x, pt0.y, fPtX, fPtY);
	
		fPtX = (FLOAT)(pt1.x + m_CanvasRect.left);
		fPtY = (FLOAT)(pt1.y + m_CanvasRect.top);
		g_aPolygonsVertex[n+1] = CUSTOMVERTEX1( fPtX,  fPtY, 0.0f, 1.0f, color);
//		DbgMsg(_T("(%3d, %3d), (%3f, %3f)\n"), pt1.x, pt1.y, fPtX, fPtY);
	
		fPtX = (FLOAT)(pt2.x + m_CanvasRect.left);
		fPtY = (FLOAT)(pt2.y + m_CanvasRect.top);
		g_aPolygonsVertex[n+2] = CUSTOMVERTEX1( fPtX,  fPtY, 0.0f, 1.0f, color);
//		DbgMsg(_T("(%3d, %3d), (%3f, %3f)\n"), pt2.x, pt2.y, fPtX, fPtY);
	
		fPtX = (FLOAT)(pt0.x + m_CanvasRect.left);
		fPtY = (FLOAT)(pt0.y + m_CanvasRect.top);
		g_aPolygonsVertex[n+3] = CUSTOMVERTEX1( fPtX,  fPtY, 0.0f, 1.0f, color);
//		DbgMsg(_T("(%3d, %3d), (%3f, %3f)\n"), pt0.x, pt0.y, fPtX, fPtY);
	
		n += 4;
	}

	// Determine the size of data to be moved into the vertex buffer.
	UINT nSizeOfData = n * sizeof(CUSTOMVERTEX1);
    // No overwrite will be used if the vertices can fit into 
    //   the space remaining in the vertex buffer.
    DWORD dwLockFlags = D3DLOCK_NOOVERWRITE;
    // Check to see if the entire vertex buffer has been used up yet.
	UINT nSizeOfVB = MAX_VERTEX*sizeof(CUSTOMVERTEX1);
    if( m_nNextVertexData > nSizeOfVB - nSizeOfData )
    {
        // No space remains. Start over from the beginning 
        //   of the vertex buffer.
        dwLockFlags = D3DLOCK_DISCARD;
        m_nNextVertexData = 0;
    }

    // Lock the vertex buffer.
	CUSTOMVERTEX1* v;
	if( FAILED( m_pPolygonVB->Lock( (UINT)m_nNextVertexData, nSizeOfData, (void**)&v, dwLockFlags ) ) )
		return 0;
	memcpy( v, g_aPolygonsVertex, sizeof(CUSTOMVERTEX1)*n );
	m_pPolygonVB->Unlock();
	m_nNextVertexData += nSizeOfData;

	return n;
}


BOOL CDX9DrawUtil::DrawPolygonCircle(POINT Center, double Radious, D3DCOLOR color)
{
	int  i;
	int  t1;
	POINT p1,p2;
	POINT centre = Center;
	double radius = Radious;
	//BOOL fill =  pRecord->circle.bFill;
	SIZE srcSize = { (m_CanvasRect.right-m_CanvasRect.left), (m_CanvasRect.bottom-m_CanvasRect.top)};
	int srW = m_CanvasRect.right-m_CanvasRect.left;
	int srH = m_CanvasRect.bottom-m_CanvasRect.top;

	ULONG nPt;
	POINT pPts[MAXCOUNT_POLYGONEDGE];

	// Do not clip it, sometimes it won't draw the zone/counter nodes when the zone/counter is too close to the edge
	//				if ((centre.x-radius)<0 || (centre.x+radius)>srW || (centre.y-radius)<0 ||(centre.y+radius)>srH)
	//					return -1;
	//memcpy( &tRecord, pRecord, sizeof( UMFDRAWRECORD ) );
	// 		tRecord.pts.pPts = &ptPolygonList;
	// 		tRecord.pts.nPt	= 
	//tRecord.polygons.nPolygon = 1;


	// first 3 points
	{
		p1.x = (LONG)(centre.x - radius);
		p1.y = (LONG)centre.y;
		pPts[0] = p1;

		i = (int)(centre.x - radius + 1);
		p1.x = i;
		p2.x = i;
		t1 = (int)sqrt(radius*radius-(i-centre.x)*(i-centre.x));
		p1.y = centre.y - t1;
		p2.y = centre.y + t1;
		pPts[1] = p1;
		pPts[2] = p2;
		nPt = 3;

		if ( ( p1.x != p2.x ) || ( p1.y != p2.y ) )
			DrawPolygonVCA( pPts, nPt, color, UMF_BRT_SOLID);
	}

	// Middle part
	nPt = 4;
	pPts[2] = p1;
	pPts[3] = p2;
	for (i = (int) (centre.x - radius + 2); i < centre.x + radius; i++)
	{	
		p1.x = i;
		p2.x = i;
		t1 = (int) sqrt(radius*radius-(i-centre.x)*(i-centre.x));
		p1.y = centre.y - t1;
		p2.y = centre.y + t1;

		pPts[0] = pPts[3];
		pPts[1] = pPts[2];

		pPts[2] = p1;
		pPts[3] = p2;
		DrawPolygonVCA( pPts, nPt, color, UMF_BRT_SOLID);
		//_DrawPolygonVCA( _rc3DCanvas, _foZValue, 1, &tRecord );
		//					m_DrawPolygonList( _rc3DCanvas, _aptDrawRecords->srcSize, &ptPolygonList, 1, alpha, _aptDrawRecords->colorValue - (alpha<<24) );
	}

	// last 3 points
	{
		p1.x = (LONG) (centre.x + radius);
		p1.y = centre.y;
		pPts[2] = p1;

		i = (int) (centre.x + radius - 1);
		p1.x = i;
		p2.x = i;
		t1 = (int) sqrt(radius*radius-(i-centre.x)*(i-centre.x));
		p1.y = centre.y - t1;
		p2.y = centre.y + t1;
		pPts[0] = p2;
		pPts[1] = p1;
		nPt = 3;

		if ( ( p1.x != p2.x ) || ( p1.y != p2.y ) )
			DrawPolygonVCA( pPts, nPt, color, UMF_BRT_SOLID);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	// first 3 points
	{
		p1.x = centre.x;
		p1.y = (LONG) (centre.y - radius);
		pPts[0] = p1;

		i =  (int) (centre.y - radius + 1);
		p1.y = i;
		p2.y = i;
		t1 = (int) sqrt(radius*radius-(i-centre.y)*(i-centre.y));
		p1.x = centre.x - t1;
		p2.x = centre.x + t1;
		pPts[1] = p1;
		pPts[2] = p2;
		nPt = 3;
		if ( ( p1.x != p2.x ) || ( p1.y != p2.y ) )
			DrawPolygonVCA( pPts, nPt, color, UMF_BRT_SOLID);
		//					m_DrawPolygonList( _rc3DCanvas, _aptDrawRecords->srcSize, &ptPolygonList, 1, alpha, _aptDrawRecords->colorValue - (alpha<<24) );
	}

	// Middle part
	nPt = 4;
	pPts[2] = p1;
	pPts[3] = p2;
	for (i = (int)(centre.y - radius + 2); i < centre.y + radius; i++)
	{	
		p1.y = i;
		p2.y = i;
		t1 = (int) sqrt(radius*radius-(i-centre.y)*(i-centre.y));
		p1.x = centre.x - t1;
		p2.x = centre.x + t1;

		pPts[0] = pPts[3];
		pPts[1] = pPts[2];

		pPts[2] = p1;
		pPts[3] = p2;
		DrawPolygonVCA( pPts, nPt, color, UMF_BRT_SOLID);
		//					m_DrawPolygonList( _rc3DCanvas, _aptDrawRecords->srcSize, &ptPolygonList, 1, alpha, _aptDrawRecords->colorValue - (alpha<<24) );
	}
	// last 3 points
	{
		p1.x = centre.x;
		p1.y = (LONG) (centre.y + radius);
		pPts[2] = p1;

		i =  (int) (centre.y - radius + 1);
		p1.y = i;
		p2.y = i;
		t1 = (int) sqrt(radius*radius-(i-centre.y)*(i-centre.y));
		p1.x = centre.x - t1;
		p2.x = centre.x + t1;
		pPts[1] = p1;
		pPts[2] = p2;
		nPt = 3;
		if ( ( p1.x != p2.x ) || ( p1.y != p2.y ) )
			DrawPolygonVCA( pPts, nPt, color, UMF_BRT_SOLID);
		//					m_DrawPolygonList( _rc3DCanvas, _aptDrawRecords->srcSize, &ptPolygonList, 1, alpha, _aptDrawRecords->colorValue - (alpha<<24) );
	}

	return TRUE;
};

BOOL	CDX9DrawUtil::DrawArc(POINT Center, double Radious, unsigned short Width, double StartRadius, double FinishRadius, D3DCOLOR color)
{
	POINT		   centre  = Center;
	double		   radius  = Radious;
	double		   dStartRadius  = StartRadius;
	double		   dFinishRadius = FinishRadius;
	unsigned short usWidth = Width;
	unsigned short usHalfWidth = (unsigned short) ((float)(usWidth - 1)*0.5);

	int srW = m_CanvasRect.right-m_CanvasRect.left;
	int srH = m_CanvasRect.bottom-m_CanvasRect.top;

	ULONG nPt;
	POINT pPts[MAXCOUNT_POLYGONEDGE];

	double dCurrentRadius = dStartRadius;

	if ((centre.x-radius)<0 || (centre.x+radius)>srW || (centre.y-radius)<0 ||(centre.y+radius)>srH) {
		return FALSE;
	}

	nPt = 4;

	while(dCurrentRadius + RADIUS_STEP < dFinishRadius)
	{
		pPts[0].x = (LONG) (( radius - usHalfWidth ) * cos(dCurrentRadius) + centre.x);
		pPts[0].y = (LONG) (-( radius - usHalfWidth ) * sin(dCurrentRadius) + centre.y);		//-- "-" suggests the screen coordinates is different.

		pPts[1].x = (LONG) (( radius + usHalfWidth ) * cos(dCurrentRadius) + centre.x);
		pPts[1].y = (LONG) (-( radius + usHalfWidth ) * sin(dCurrentRadius) + centre.y);

		pPts[2].x = (LONG) (( radius + usHalfWidth ) * cos(dCurrentRadius + RADIUS_STEP) + centre.x);
		pPts[2].y = (LONG) (-( radius + usHalfWidth ) * sin(dCurrentRadius + RADIUS_STEP) + centre.y);

		pPts[3].x = (LONG) (( radius - usHalfWidth ) * cos(dCurrentRadius + RADIUS_STEP) + centre.x);
		pPts[3].y = (LONG) (-( radius - usHalfWidth ) * sin(dCurrentRadius + RADIUS_STEP) + centre.y);

		DrawPolygonVCA( pPts, nPt, color, UMF_BRT_SOLID);
		//					m_DrawPolygonList( _rc3DCanvas, _aptDrawRecords->srcSize, &ptPolygonList, 1, alpha, _aptDrawRecords->colorValue - (alpha<<24) );

		dCurrentRadius += RADIUS_STEP;
	}
#if 1
	pPts[0].x = (LONG) (( radius - usHalfWidth ) * cos(dCurrentRadius) + centre.x);
	pPts[0].y = (LONG) (-( radius - usHalfWidth ) * sin(dCurrentRadius) + centre.y);

	pPts[1].x = (LONG) (( radius + usHalfWidth ) * cos(dCurrentRadius) + centre.x);
	pPts[1].y = (LONG) (-( radius + usHalfWidth ) * sin(dCurrentRadius) + centre.y);

	pPts[2].x = (LONG) (( radius + usHalfWidth ) * cos(dFinishRadius) + centre.x);
	pPts[2].y = (LONG) (-( radius + usHalfWidth ) * sin(dFinishRadius) + centre.y);

	pPts[3].x = (LONG) (( radius - usHalfWidth ) * cos(dFinishRadius) + centre.x);
	pPts[3].y = (LONG) (-( radius - usHalfWidth ) * sin(dFinishRadius) + centre.y);

	DrawPolygonVCA( pPts, nPt, color, UMF_BRT_SOLID);
	//				m_DrawPolygonList( _rc3DCanvas, _aptDrawRecords->srcSize, &ptPolygonList, 1, alpha, _aptDrawRecords->colorValue - (alpha<<24) );
#endif
	return TRUE;
}


IUMFImage*	CDX9DrawUtil::CreateUMFImage(USHORT width, USHORT height, eUMF_COLORFORMAT colorformat)
{
	if(m_bInit == FALSE) return FALSE;

	CDX9Image* pCDX9Image = new CDX9Image();

	if(!pCDX9Image->Init(m_pUMFRender , width, height, colorformat)){
		pCDX9Image->DestroySelf();
		return NULL;
	}

	return pCDX9Image;
}


CUSTOMVERTEX g_aCurveVertex[CDX9DrawUtil::MAX_VERTEX];

BOOL	CDX9DrawUtil::DrawCurve(POINT *pPts, UINT nPtCnt, D3DCOLOR color)
{
	ULONG i;
	ULONG LineCnt = 20*(nPtCnt-1);

	// assume that src size is always same as dest rect size.
	SIZE srcSize = { (m_CanvasRect.right-m_CanvasRect.left), (m_CanvasRect.bottom-m_CanvasRect.top)};

	for( i = 0 ; i < nPtCnt ;i++){
		g_aCurveVertex[i].diffuse = color;
		g_aCurveVertex[i].rhw	= 1.0f;
		pPts[i].x = max( 0, pPts[i].x );
		pPts[i].x = min( srcSize.cx, pPts[i].x );
		pPts[i].y = max( 0, pPts[i].y );
		pPts[i].y = min( srcSize.cy, pPts[i].y );
		g_aCurveVertex[i].x		= (FLOAT)pPts[i].x + m_CanvasRect.left;
		g_aCurveVertex[i].y		= (FLOAT)pPts[i].y + m_CanvasRect.top;
		g_aCurveVertex[i].z		= 0;
	}
	
	// Determine the size of data to be moved into the vertex buffer.
	UINT nSizeOfData = LineCnt * sizeof(CUSTOMVERTEX);
    // No overwrite will be used if the vertices can fit into 
    //   the space remaining in the vertex buffer.
    DWORD dwLockFlags = D3DLOCK_NOOVERWRITE;
    // Check to see if the entire vertex buffer has been used up yet.
	UINT nSizeOfVB = MAX_VERTEX*sizeof(CUSTOMVERTEX);
    if( m_nNextVertexDataRect > nSizeOfVB - nSizeOfData )
    {
        // No space remains. Start over from the beginning 
        //   of the vertex buffer.
        dwLockFlags = D3DLOCK_DISCARD;
        m_nNextVertexDataRect = 0;
    }

	CUSTOMVERTEX* v;
	if( FAILED( m_pRectVB->Lock( m_nNextVertexDataRect, nSizeOfData, (void**)&v, dwLockFlags ) ) )
		return 0;
	
	m_nNextVertexDataRect += nSizeOfData;
	//현재 4개 기준으로 되어 있다.
	for( i = 0 ; i < LineCnt/3 ;i++){
		v[i].diffuse				= color;
		v[i].rhw					= 1.0f;
		v[i+(LineCnt/3)].diffuse	= color;
		v[i+(LineCnt/3)].rhw		= 1.0f;
		v[i+(LineCnt/3)*2].diffuse	= color;
		v[i+(LineCnt/3)*2].rhw		= 1.0f;


		float S =  (float)i / (LineCnt/3);
		D3DXVec3CatmullRom( (D3DXVECTOR3 *)&v[i], (D3DXVECTOR3 *)&g_aCurveVertex[0], 
			(D3DXVECTOR3 *)&g_aCurveVertex[0], (D3DXVECTOR3 *)&g_aCurveVertex[1], (D3DXVECTOR3 *)&g_aCurveVertex[2], S );
		D3DXVec3CatmullRom( (D3DXVECTOR3 *)&v[i+(LineCnt/3)], (D3DXVECTOR3 *)&g_aCurveVertex[0], (D3DXVECTOR3 *)&g_aCurveVertex[1], 
			(D3DXVECTOR3 *)&g_aCurveVertex[2], (D3DXVECTOR3 *)&g_aCurveVertex[3], S );
		D3DXVec3CatmullRom( (D3DXVECTOR3 *)&v[i+(LineCnt/3)*2], (D3DXVECTOR3 *)&g_aCurveVertex[1], (D3DXVECTOR3 *)&g_aCurveVertex[2], 
			(D3DXVECTOR3 *)&g_aCurveVertex[3], (D3DXVECTOR3 *)&g_aCurveVertex[3], S );
	}

	m_pRectVB->Unlock();

	m_pd3dDevice->SetStreamSource( 0, m_pRectVB, m_nNextVertexDataRect - nSizeOfData, sizeof(CUSTOMVERTEX) );
	m_pd3dDevice->SetFVF( D3DFVF_CUSTOMVERTEX );
	m_pd3dDevice->DrawPrimitive( D3DPT_LINESTRIP, 0, LineCnt-1);
	
	return TRUE;
}

