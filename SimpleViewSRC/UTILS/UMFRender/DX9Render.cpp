#include "StdAfx.h"
#include "DX9Render.h"


#ifndef TRACE
#include <atlbase.h>
#define TRACE AtlTrace
#endif

//#pragma comment( lib, "dxguid" )
#pragma comment( lib, "D3D9" )
#pragma comment( lib, "winmm" )


#define INIT_DIRECTDRAW_STRUCT(x) (ZeroMemory(&x, sizeof(x)), x.dwSize=sizeof(x))

#ifndef SAFE_RELEASE
#define SAFE_RELEASE(c) if(c){c->Release();c=NULL;}else{}
#endif

#ifndef SAFE_RELEASE2
#define SAFE_RELEASE2(c) if(c){UINT count=c->Release();c=NULL;TRACE(_T("%s RefCount After Release :%d \n"), #c, count);}else{}
#endif


#ifndef __MAX
#define	__MAX(A,B) ((A>B)?A:B)
#endif

typedef struct
{
    FLOAT       x,y,z;      // vertex untransformed position
    FLOAT       rhw;        // eye distance
    D3DCOLOR    diffuse;    // diffuse color
    FLOAT       tu, tv;     // texture relative coordinates
} CUSTOMVERTEX;

#define D3DFVF_CUSTOMVERTEX (D3DFVF_XYZRHW|D3DFVF_DIFFUSE|D3DFVF_TEX1)


eUMF_COLORFORMAT GetColorFormat3DSupportFromBitInfoHeader(BITMAPINFOHEADER *pbm)
{

	eUMF_COLORFORMAT format = UMF_NONE;
	if (MAKEFOURCC('Y','U','Y','2') == pbm->biCompression) {
		format = UMF_YUY2;
	} else if(MAKEFOURCC('Y','V','1','2') == pbm->biCompression) {
		format = UMF_YV12;
		//format = UMF_RGB555;
	}else if(MAKEFOURCC('I','Y','U','V') == pbm->biCompression) {
		format = UMF_YUV420;
	} else { //RGB 
		if(pbm->biBitCount == 32){
			format = UMF_RGBA32;
		}else if(pbm->biBitCount == 24){
			format = UMF_RGB24;
		}else if(pbm->biBitCount == 16){
			format = UMF_RGB565;
		}else if(pbm->biBitCount == 15){
			format = UMF_RGB555;
		}
	}
	return format;
}


D3DFORMAT Direct3DVoutSelectFormat( LPDIRECT3D9 pD3D, D3DFORMAT target, const D3DFORMAT *formats, size_t count)
{
    size_t c;

    for( c=0; c<count; ++c ){
        HRESULT hr;
        D3DFORMAT format = formats[c];
        /* test whether device can create a surface of that format */
        hr = pD3D->CheckDeviceFormat(D3DADAPTER_DEFAULT,D3DDEVTYPE_HAL, target, 0, D3DRTYPE_SURFACE, format);
        if( SUCCEEDED(hr) ){
            /* test whether device can perform color-conversion
            ** from that format to target format
            */
            hr = pD3D->CheckDeviceFormatConversion(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL,format, target);
        }
        if( SUCCEEDED(hr) ){
            // found a compatible format
            switch( format ){
                case D3DFMT_UYVY:
                    TRACE(_T("selected surface pixel format is UYVY"));
                    break;
                case D3DFMT_YUY2:
                    TRACE(_T("selected surface pixel format is YUY2"));
                    break;
                case D3DFMT_X8R8G8B8:
                    TRACE(_T("selected surface pixel format is X8R8G8B8"));
                    break;
                case D3DFMT_A8R8G8B8:
                    TRACE(_T("selected surface pixel format is A8R8G8B8"));
                    break;
                case D3DFMT_R8G8B8:
                    TRACE(_T("selected surface pixel format is R8G8B8"));
                    break;
                case D3DFMT_R5G6B5:
                    TRACE(_T("selected surface pixel format is R5G6B5"));
                    break;
                case D3DFMT_X1R5G5B5:
                    TRACE(_T("selected surface pixel format is X1R5G5B5"));
                    break;
                default:
                    TRACE(_T("selected surface pixel format is 0x%0X"), format);
                    break;
            }
            return format;
        }
        else if( D3DERR_NOTAVAILABLE != hr ){
            TRACE(_T("Could not query adapter supported formats. (hr=0x%lX)"), hr);
            break;
        }
    }
    return D3DFMT_UNKNOWN;
}


D3DFORMAT Direct3DFindColorFormat(LPDIRECT3D9 pD3D, eUMF_COLORFORMAT src_format, D3DFORMAT target)
{
	switch(src_format){
		case UMF_YV12:
		case UMF_YUV420:{
			static const D3DFORMAT formats[] =
                    { D3DFMT_YUY2, D3DFMT_UYVY, D3DFMT_X8R8G8B8, D3DFMT_A8R8G8B8, D3DFMT_R5G6B5, D3DFMT_X1R5G5B5 };
			return Direct3DVoutSelectFormat(pD3D, target, formats, sizeof(formats)/sizeof(D3DFORMAT));
		}
		case UMF_YUY2:{
			static const D3DFORMAT formats[] =
                { D3DFMT_YUY2, D3DFMT_UYVY, D3DFMT_X8R8G8B8, D3DFMT_A8R8G8B8, D3DFMT_R5G6B5, D3DFMT_X1R5G5B5 };
            return Direct3DVoutSelectFormat(pD3D, target, formats, sizeof(formats)/sizeof(D3DFORMAT));
		}
		case UMF_RGB555:{
            static const D3DFORMAT formats[] =
                { D3DFMT_X1R5G5B5 };
            return Direct3DVoutSelectFormat(pD3D, target, formats, sizeof(formats)/sizeof(D3DFORMAT));
        }
        case UMF_RGB565:{
            static const D3DFORMAT formats[] =
                { D3DFMT_R5G6B5 };
            return Direct3DVoutSelectFormat(pD3D, target, formats, sizeof(formats)/sizeof(D3DFORMAT));
        }
        case UMF_RGB24:{
            static const D3DFORMAT formats[] =
                { D3DFMT_R8G8B8, D3DFMT_X8R8G8B8, D3DFMT_A8R8G8B8 };
            return Direct3DVoutSelectFormat(pD3D, target, formats, sizeof(formats)/sizeof(D3DFORMAT));
        }
        case UMF_RGBA32:
        {
            static const D3DFORMAT formats[] =
                { D3DFMT_A8R8G8B8, D3DFMT_X8R8G8B8 };
            return Direct3DVoutSelectFormat(pD3D, target, formats, sizeof(formats)/sizeof(D3DFORMAT));
        }
        default:
        {
            /* use display default format */
            D3DDISPLAYMODE d3ddm;
            HRESULT hr = pD3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &d3ddm );
            if( SUCCEEDED(hr)){
                /*
                ** some professional cards could use some advanced pixel format as default,
                ** make sure we stick with chromas that we can handle internally
                */
                switch( d3ddm.Format )
                {
                    case D3DFMT_R8G8B8:
                    case D3DFMT_X8R8G8B8:
                    case D3DFMT_A8R8G8B8:
                    case D3DFMT_R5G6B5:
                    case D3DFMT_X1R5G5B5:
                        TRACE(_T("defaulting to adapter pixel format"));
                        return Direct3DVoutSelectFormat(pD3D, target, &d3ddm.Format, 1);
                    default:
                    {
                        /* if we fall here, that probably means that we need to render some YUV format */
                        static const D3DFORMAT formats[] =
                            { D3DFMT_X8R8G8B8, D3DFMT_A8R8G8B8, D3DFMT_R5G6B5, D3DFMT_X1R5G5B5 };
                        TRACE(_T("defaulting to built-in pixel format"));
                        return Direct3DVoutSelectFormat(pD3D, target, formats, sizeof(formats)/sizeof(D3DFORMAT));
                    }
                }
            }
        }
    }
    return D3DFMT_UNKNOWN;
}


static BOOL Direct3DFillPresentationParameters(LPDIRECT3D9 pD3D, HWND hWnd, D3DPRESENT_PARAMETERS* pD3DPP)
{
    D3DDISPLAYMODE d3ddm;
    HRESULT hr;

    hr = pD3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &d3ddm );
    if( FAILED(hr))
    {
       TRACE( _T("Could not read adapter display mode. (hr=0x%lX)"), hr);
       return FALSE;
    }
    
    ZeroMemory( pD3DPP, sizeof(D3DPRESENT_PARAMETERS) );
    pD3DPP->Flags                  = D3DPRESENTFLAG_VIDEO;
    pD3DPP->Windowed               = TRUE;
    pD3DPP->hDeviceWindow          = hWnd;
    pD3DPP->BackBufferWidth        = d3ddm.Width;
    pD3DPP->BackBufferHeight       = d3ddm.Height;
    pD3DPP->SwapEffect             = D3DSWAPEFFECT_DISCARD;
    pD3DPP->MultiSampleType        = D3DMULTISAMPLE_NONE;
    pD3DPP->PresentationInterval   = D3DPRESENT_INTERVAL_DEFAULT;
    pD3DPP->BackBufferFormat       = d3ddm.Format;
    pD3DPP->BackBufferCount        = 1;
    pD3DPP->EnableAutoDepthStencil = TRUE;
	pD3DPP->AutoDepthStencilFormat = D3DFMT_D24S8; 

	
	const unsigned i_adapter_count = pD3D->GetAdapterCount();
    for( unsigned i = 1; i < i_adapter_count; i++ ){
        hr = pD3D->GetAdapterDisplayMode(i, &d3ddm );
        if( FAILED(hr) )
            continue;
        pD3DPP->BackBufferWidth  = __MAX(pD3DPP->BackBufferWidth,  d3ddm.Width);
        pD3DPP->BackBufferHeight = __MAX(pD3DPP->BackBufferHeight, d3ddm.Height);
    }

	return TRUE;
}


BOOL Direct3DCreateMainTexture(LPDIRECT3DDEVICE9 pd3dDevice, D3DPRESENT_PARAMETERS* pD3DPP, LPDIRECT3DTEXTURE9* pddsMainTexture, LPDIRECT3DVERTEXBUFFER9 *pddVertext)
{

    HRESULT hr;

    /*
    ** Create a texture for use when rendering a scene
    ** for performance reason, texture format is identical to backbuffer
    ** which would usually be a RGB format
    */
    hr = pd3dDevice->CreateTexture(pD3DPP->BackBufferWidth, pD3DPP->BackBufferHeight, 1, 
		D3DUSAGE_RENDERTARGET, pD3DPP->BackBufferFormat, D3DPOOL_DEFAULT,pddsMainTexture, NULL);
    if( FAILED(hr)){
        TRACE(_T("Failed to create texture. (hr=0x%lx)"), hr);
        return FALSE;
    }

	hr = pd3dDevice->CreateVertexBuffer(sizeof(CUSTOMVERTEX)*4,
            D3DUSAGE_DYNAMIC|D3DUSAGE_WRITEONLY,
            D3DFVF_CUSTOMVERTEX,
            D3DPOOL_DEFAULT,
            pddVertext,
            NULL);
    if( FAILED(hr) )
    {
        TRACE(_T("Failed to create vertex buffer. (hr=0x%lx)"), hr);
        return FALSE;
    }


    // Texture coordinates outside the range [0.0, 1.0] are set
    // to the texture color at 0.0 or 1.0, respectively.
    pd3dDevice->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
    pd3dDevice->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);

    // Set linear filtering quality
    pd3dDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
    pd3dDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);

    // set maximum ambient light
    pd3dDevice->SetRenderState(D3DRS_AMBIENT, D3DCOLOR_XRGB(255,255,255));

    // Turn off culling
    pd3dDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);

    // Turn off the zbuffer
    pd3dDevice->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);

    // Turn off lights
    pd3dDevice->SetRenderState(D3DRS_LIGHTING, FALSE);

    // Enable dithering
    pd3dDevice->SetRenderState(D3DRS_DITHERENABLE, TRUE);

    // disable stencil
    pd3dDevice->SetRenderState(D3DRS_STENCILENABLE, FALSE);

    // manage blending
    pd3dDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
    pd3dDevice->SetRenderState(D3DRS_SRCBLEND,D3DBLEND_SRCALPHA);
    pd3dDevice->SetRenderState(D3DRS_DESTBLEND,D3DBLEND_INVSRCALPHA);
    pd3dDevice->SetRenderState(D3DRS_ALPHATESTENABLE,TRUE);
    pd3dDevice->SetRenderState(D3DRS_ALPHAREF, 0x10);
    pd3dDevice->SetRenderState(D3DRS_ALPHAFUNC,D3DCMP_GREATER);

    // Set texture states
    pd3dDevice->SetTextureStageState(0, D3DTSS_COLOROP,D3DTOP_MODULATE);
    pd3dDevice->SetTextureStageState(0, D3DTSS_COLORARG1,D3DTA_TEXTURE);
    pd3dDevice->SetTextureStageState(0, D3DTSS_COLORARG2,D3DTA_DIFFUSE);

    // turn off alpha operation
    pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_DISABLE);

    return TRUE;
}


CDX9Render::CDX9Render(void)
{
	m_bCreate		= FALSE;
	m_hWnd			= NULL;

	m_pD3D			= NULL;
	m_pd3dDevice	= NULL; 

	m_pMainTexture	= NULL;
	m_pMainVertex	= NULL;
	memset(&m_D3DPP, 0, sizeof(D3DPRESENT_PARAMETERS));
		
	m_TotalImageBufCnt	= 0;

	memset(m_ImageBuf, 0, sizeof(m_ImageBuf));
}


CDX9Render::~CDX9Render(void)
{
	if(m_bCreate){
		Close();
	}
}


BOOL	CDX9Render::Open(HWND hWnd)
{
	if(m_bCreate){
		TRACE(_T("CDX9Render::Create before\n"));
		return TRUE;
	}
	
	HRESULT	hr;

	m_pD3D	= Direct3DCreate9( D3D_SDK_VERSION );
	if(!m_pD3D){
		TRACE(_T("Can not Create Direct3DCreate9 Ver [%d]  \n"), D3D_SDK_VERSION);
		return FALSE;
	}
	
	
	Direct3DFillPresentationParameters(m_pD3D, hWnd, &m_D3DPP);
	
	HMONITOR hMonitor = MonitorFromWindow(m_hWnd, MONITOR_DEFAULTTONEAREST);
	DWORD i, nAdapter = D3DADAPTER_DEFAULT;
	for( i = 0; i < m_pD3D->GetAdapterCount() ; i++ ){
		if(m_pD3D->GetAdapterMonitor(i) == hMonitor)nAdapter = i;
    }
	TRACE(_T("Open\n"));
	TRACE(_T("create D3D device! adapter %d\n"), nAdapter );


	hr = m_pD3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd, 
			D3DCREATE_HARDWARE_VERTEXPROCESSING|D3DCREATE_MULTITHREADED, &m_D3DPP, &m_pd3dDevice);
	if( FAILED(hr) )
    {
		hr = m_pD3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd, 
			D3DCREATE_SOFTWARE_VERTEXPROCESSING|D3DCREATE_MULTITHREADED, &m_D3DPP, &m_pd3dDevice);
		if( FAILED(hr) ){
		   TRACE(_T("Could not create the D3D device! (hr=0x%lX)"), hr);
		   goto FAILED;
		}
    }

	if(!Direct3DCreateMainTexture(m_pd3dDevice, &m_D3DPP, &m_pMainTexture, &m_pMainVertex)){
		m_D3DPP.BackBufferHeight = m_D3DPP.BackBufferWidth = 0;
		goto FAILED;
	}
	
	m_bCreate		= TRUE;
	m_hWnd			= hWnd;

	Clear();

FAILED:
	if(!m_bCreate){
		Close();
	}

	return m_bCreate;
}

void	CDX9Render::Close()
{
	SAFE_RELEASE(m_pMainTexture);
	SAFE_RELEASE(m_pMainVertex);
	SAFE_RELEASE(m_pd3dDevice);	

	SAFE_RELEASE(m_pD3D);


	for(DWORD i = 0 ; i < m_TotalImageBufCnt ; i++)DestroyImageBuf(i);
	
	m_TotalImageBufCnt = 0;
	m_bCreate	= FALSE;
	m_hWnd		= NULL;
	TRACE(_T("CD3DRenderer::Endup()\n"));
}


BOOL	CDX9Render::Clear()
{
	if(!m_bCreate){
		TRACE(_T("CDX9Render::Clear did not create before\n"));
		return FALSE;
	}
	
	RECT rc;
	HRESULT	hr;
	m_pd3dDevice->Clear(0, NULL, D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER,  0, 1.0, 0);

	LPDIRECT3DSURFACE9 pD3DTexSurf;
	rc.left		= 0;
	rc.top		= 0;
	rc.right	= m_D3DPP.BackBufferWidth;
	rc.bottom	= m_D3DPP.BackBufferHeight;
		
	hr = m_pMainTexture->GetSurfaceLevel(0, &pD3DTexSurf);
    if( FAILED(hr) ){
		TRACE(_T("CDX9Render::Clear can not get Surface\n"));
		return FALSE;
    }

	m_pd3dDevice->ColorFill(pD3DTexSurf, &rc, RGB(0,0,0));
	pD3DTexSurf->Release();

	return TRUE;
}


BOOL	CDX9Render::ReCreateDevice(DWORD width, DWORD height)
{
	if(!m_bCreate){
		TRACE(_T("CDX9Render::ReCreateDevice did not create before\n"));
		return FALSE;
	}
	HRESULT	hr;

	m_bCreate = FALSE;

	m_D3DPP.BackBufferWidth		= width;
	m_D3DPP.BackBufferHeight	= height;

	//need update monitor
//1 Clean up
	SAFE_RELEASE(m_pMainTexture);
	SAFE_RELEASE(m_pMainVertex);

	SAFE_RELEASE(m_pd3dDevice);	
	
	DWORD			TotalImageBufCntTemp;
	IMAGEBUFFER		ImageBufTemp[MAX_IMAGEBUFFER];
	memcpy(ImageBufTemp, m_ImageBuf, sizeof(IMAGEBUFFER)*MAX_IMAGEBUFFER);
	TotalImageBufCntTemp	= m_TotalImageBufCnt;

	for(DWORD i = 0 ; i < m_TotalImageBufCnt ; i++)DestroyImageBuf(i);
	m_TotalImageBufCnt = 0;


	
//2. ReCreate 
	//Get Adapter 
	//MonitorFromWindow(m_hWnd, )
	HMONITOR hMonitor = MonitorFromWindow(m_hWnd, MONITOR_DEFAULTTONEAREST);
	DWORD i, nAdapter = D3DADAPTER_DEFAULT;
	for( i = 0; i < m_pD3D->GetAdapterCount() ; i++ ){
		if(m_pD3D->GetAdapterMonitor(i) == hMonitor)nAdapter = i;
    }
	TRACE(_T("ReCreate\n"));
	TRACE(_T("create D3D device! adapter %d\n"), nAdapter );


	hr = m_pD3D->CreateDevice(nAdapter, D3DDEVTYPE_HAL, m_hWnd, 
			D3DCREATE_HARDWARE_VERTEXPROCESSING|D3DCREATE_MULTITHREADED, &m_D3DPP, &m_pd3dDevice);
	if( FAILED(hr) )
    {
		hr = m_pD3D->CreateDevice(nAdapter, D3DDEVTYPE_HAL, m_hWnd, 
			D3DCREATE_SOFTWARE_VERTEXPROCESSING|D3DCREATE_MULTITHREADED, &m_D3DPP, &m_pd3dDevice);
		if( FAILED(hr) ){
		   TRACE(_T("Could not create the D3D device! (hr=0x%lX)"), hr);
		   goto FAILED;
		}
    }

	if(!Direct3DCreateMainTexture(m_pd3dDevice, &m_D3DPP, &m_pMainTexture, &m_pMainVertex)){
		TRACE(_T("Could not create MainTexture [%d:%d]"), width, height);
		m_D3DPP.BackBufferHeight = m_D3DPP.BackBufferWidth = 0;
		goto FAILED;
	}

	//must set m_bCreate  before call CreateImage  
	m_bCreate = TRUE;

	for(DWORD i = 0 ; i < TotalImageBufCntTemp ; i++){
		CreateImageBuf(ImageBufTemp[i].width, ImageBufTemp[i].height, ImageBufTemp[i].format);
	}

	Clear();

FAILED:
	if(!m_bCreate){

		SAFE_RELEASE(m_pMainTexture);
		SAFE_RELEASE(m_pMainVertex);

		m_D3DPP.BackBufferHeight= 0;
		m_D3DPP.BackBufferWidth	= 0;

		SAFE_RELEASE(m_pd3dDevice);	
		for(DWORD i = 0 ; i < m_TotalImageBufCnt ; i++)DestroyImageBuf(i);

		memset(&m_D3DPP, 0, sizeof(m_D3DPP));
		m_TotalImageBufCnt = 0;

	}

	return m_bCreate;
}


DWORD CDX9Render::CreateImageBuf(DWORD width, DWORD height, eUMF_COLORFORMAT colorformat)
{
	if(!m_bCreate)
		return -1;

	//1. Find Same buffer 
	DWORD i;
	D3DFORMAT format;
	HRESULT hr;
	for(i = 0 ; i < m_TotalImageBufCnt ; i++){
		if((width == m_ImageBuf[i].width) && 
			(height == m_ImageBuf[i].height) && 
			(colorformat == m_ImageBuf[i].format)){
				m_ImageBuf[i].ref_count++;
				return i;
		}
	}

	// Find empty buffer
	DWORD FoundEmptyIndex = -1;
	for(i = 0 ; i < m_TotalImageBufCnt ; i++){
		if((m_ImageBuf[i].ref_count == 0) && (m_ImageBuf[i].pddsImageBuffer == NULL)){
			FoundEmptyIndex = i;
		}
	}

	if(-1 == FoundEmptyIndex){
		if((m_TotalImageBufCnt+1) >= MAX_IMAGEBUFFER){
			TRACE(_T("Current ImageBufCnt [%d] Exceed MAX Buf [%d]\n"),m_TotalImageBufCnt, MAX_IMAGEBUFFER);
			return -1;
		}
		FoundEmptyIndex = m_TotalImageBufCnt;
	}

	D3DDISPLAYMODE d3ddm;
	memset(&d3ddm, 0 ,sizeof(D3DDISPLAYMODE));
	m_pD3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &d3ddm);

	format = Direct3DFindColorFormat(m_pD3D, colorformat, d3ddm.Format);
	if(D3DFMT_UNKNOWN == format){
		TRACE(_T("Can not create Colorformat[%d]\n"),colorformat);
		return -1;
	}

	//TODO.. D3DFORMAT to ColorFormat....
	LPDIRECT3DSURFACE9 pddsImageBuffer;
	hr = m_pd3dDevice->CreateOffscreenPlainSurface(width,height,format,D3DPOOL_DEFAULT, &pddsImageBuffer, NULL);
	if( FAILED(hr) ){
        TRACE(_T("Failed to create picture surface. (hr=0x%lx)"), hr);
        return -1;
    }

	m_ImageBuf[FoundEmptyIndex].width	= width;
	m_ImageBuf[FoundEmptyIndex].height	= height;
	m_ImageBuf[FoundEmptyIndex].format	= colorformat;
	m_ImageBuf[FoundEmptyIndex].pddsImageBuffer = pddsImageBuffer;
	m_ImageBuf[FoundEmptyIndex].ref_count = 1;
	m_TotalImageBufCnt++;
	return FoundEmptyIndex;
}


void	CDX9Render::DestroyImageBuf(DWORD ImageBufIndex)
{
	if(ImageBufIndex >= m_TotalImageBufCnt){
		TRACE(_T("Image Buffer[%d] not found TotalImageBuf [%d] \n"),ImageBufIndex, m_TotalImageBufCnt);
		return;
	}

	if((m_ImageBuf[ImageBufIndex].ref_count-1) == 0){
		SAFE_RELEASE(m_ImageBuf[ImageBufIndex].pddsImageBuffer);
		memset(&m_ImageBuf[ImageBufIndex], 0, sizeof(IMAGEBUFFER));
	}
}


UINT	CDX9Render::GetImageSize(DWORD EndId, DWORD ImageBufIndex, eUMF_COLORFORMAT Imageformat)
{
	double bytePerPixel = GetUMFColorByte(Imageformat);
	return (UINT)(m_ImageBuf[ImageBufIndex].width * m_ImageBuf[ImageBufIndex].height * bytePerPixel);
}


void ConvertYV12ToYUY2(BYTE* pSrc, int nWidth, int nHeight, BYTE* pDst, int nDstPitch)
{
	BYTE* pSrcY = pSrc;
	BYTE* pSrcU = pSrc + (nWidth * nHeight);
	BYTE* pSrcV = pSrc + (nWidth * nHeight) + (nWidth * nHeight) / 4;

	BYTE* pDest = pDst;

	int idxY=0, idxU=0, idxV=0;
	int idxEvenU=0, idxEvenV=0;
	int nLinesizeYUY2 = nWidth*2;
	int nHeightYV12 = nHeight;

	for(int h=0; h<nHeight; h++)
	{
		idxU = idxEvenU ;
		idxV = idxEvenV;

		for(int w=0; w<nLinesizeYUY2; w+=4)
		{
			if((h%2)==0) {
				pDest[w+0] = pSrcY[idxY];// Y1
				pDest[w+1] = pSrcU[idxU];	// U
				pDest[w+2] = pSrcY[idxY+1];	// Y2
				pDest[w+3] = pSrcV[idxV];	// V

				idxY += 2;
				idxU++;
				idxV++;
			} else {
				pDest[w+0] = pSrcY[idxY];// Y1
				pDest[w+1] = pSrcU[idxU];	// U
				pDest[w+2] = pSrcY[idxY+1];	// Y2
				pDest[w+3] = pSrcV[idxV];	// V

				idxY += 2;
				idxU++;
				idxV++;
			}
		}

		if((h%2)==0) {
			idxEvenU += (nWidth/2);
			idxEvenV += (nWidth/2);
		} 

		pDest += nDstPitch;
	}
}


HRESULT	CDX9Render::DrawImage(DWORD ImageBufIndex, PBYTE pImage, eUMF_COLORFORMAT Imageformat, RECT TargetRect)
{
	if(!m_bCreate) return S_FALSE;

	if(ImageBufIndex >= m_TotalImageBufCnt){
		TRACE(_T("Image Buffer[%d] not found TotalImageBuf [%d] \n"),ImageBufIndex, m_TotalImageBufCnt);
		return S_FALSE;
	}

	// check if device is still available
	HRESULT hr;
    hr = m_pd3dDevice->TestCooperativeLevel();
    if( FAILED(hr) ){
/*	TODO.. Implement
        if( (D3DERR_DEVICENOTRESET != hr)||(VLC_SUCCESS != Direct3DVoutResetDevice(p_vout)) )
        {
            // device is not usable at present (lost device, out of video mem ?)
            return S_FALSE;
        }
*/

    }

	//1. Draw image to IMAGEBUFFER 
	D3DLOCKED_RECT D3DRect;
	LPDIRECT3DSURFACE9 pD3DSurf = m_ImageBuf[ImageBufIndex].pddsImageBuffer;
	if(!pD3DSurf){
		TRACE(_T("Image Buffer[%d] not found assigned Image buffer \n"),ImageBufIndex);
		return S_FALSE;
	}

	LPDIRECT3DSURFACE9 pD3DTexSurf;

	RECT SrcRect;
	SrcRect.left	= SrcRect.top = 0;
	SrcRect.right	= m_ImageBuf[ImageBufIndex].width;	 
	SrcRect.bottom	= m_ImageBuf[ImageBufIndex].height;


    /* Lock the surface to get a valid pointer to the picture buffer */
    hr =  pD3DSurf->LockRect(&D3DRect, NULL, 0);
    if( FAILED(hr) ){
        TRACE(_T("%s:%d (hr=0x%0lX)"), __FUNCTION__, __LINE__, hr);
        return S_FALSE;
    }

	float BytePerPixel = GetUMFColorByte(Imageformat);
	if( (Imageformat == UMF_YUV420) || (Imageformat == UMF_YV12) ) {
		//ASSERT(m_ImageBuf[ImageBufIndex].format == UMF_YUY2);

		// YV12 To YUY2
		__try{
		ConvertYV12ToYUY2(
			pImage, 
			m_ImageBuf[ImageBufIndex].width, 
			m_ImageBuf[ImageBufIndex].height, 
			(PBYTE)D3DRect.pBits, 
			D3DRect.Pitch);
		}
		__except(EXCEPTION_CONTINUE_EXECUTION){
		}
	} else {
		/* fill in buffer info in first plane */
		for(ULONG i = 0 ; i < m_ImageBuf[ImageBufIndex].height ; i++){
			memcpy((PBYTE)D3DRect.pBits + i* D3DRect.Pitch, 
				pImage + (i* (int)(m_ImageBuf[ImageBufIndex].width*BytePerPixel)), 
				(int)(m_ImageBuf[ImageBufIndex].width*BytePerPixel));
		}
	}
    
	hr = pD3DSurf->UnlockRect();
    if( FAILED(hr) ){
        TRACE(_T("%s:%d (hr=0x%0lX)"), __FUNCTION__, __LINE__, hr);
        return S_FALSE;
    }
	

	//2. Draw to ImageBuffer to Texture
	hr = m_pMainTexture->GetSurfaceLevel(0, &pD3DTexSurf);
    if( FAILED(hr) ){
        TRACE(_T("%s:%d (hr=0x%0lX)"), __FUNCTION__, __LINE__, hr);
		goto EXIT;
    }
	
	hr = m_pd3dDevice->StretchRect(pD3DSurf, &SrcRect, pD3DTexSurf, &TargetRect, D3DTEXF_LINEAR);
	if( FAILED(hr) ){
        TRACE(_T("Error StretchRect (hr=0x%0lX)\n"), hr);
		pD3DTexSurf->Release();
		goto EXIT;
    }

	pD3DTexSurf->Release();
	
	//3. Draw to Texture to Device
	CUSTOMVERTEX            *p_vertices;
	hr = m_pMainVertex->Lock(0, 0, (VOID **)(&p_vertices), D3DLOCK_DISCARD);
    if( FAILED(hr) ){
        TRACE(_T("%s:%d (hr=0x%0lX)"), __FUNCTION__, __LINE__, hr);
		goto EXIT;
    }

	 /* Setup vertices */
	float f_width  = (float)m_D3DPP.BackBufferWidth;
	float f_height = (float)m_D3DPP.BackBufferHeight;

    /* -0.5f is a "feature" of DirectX and it seems to apply to Direct3d also */
    /* http://www.sjbrown.co.uk/2003/05/01/fix-directx-rasterisation/ */
    p_vertices[0].x       = -0.5f;       // left
    p_vertices[0].y       = -0.5f;       // top
    p_vertices[0].z       = 0.0f;
    p_vertices[0].diffuse = D3DCOLOR_ARGB(255, 255, 255, 255);
    p_vertices[0].rhw     = 1.0f;
    p_vertices[0].tu      = 0.0f;
    p_vertices[0].tv      = 0.0f;

    p_vertices[1].x       = f_width - 0.5f;    // right
    p_vertices[1].y       = -0.5f;       // top
    p_vertices[1].z       = 0.0f;
    p_vertices[1].diffuse = D3DCOLOR_ARGB(255, 255, 255, 255);
    p_vertices[1].rhw     = 1.0f;
    p_vertices[1].tu      = 1.0f;
    p_vertices[1].tv      = 0.0f;

    p_vertices[2].x       = f_width - 0.5f;    // right
    p_vertices[2].y       = f_height - 0.5f;   // bottom
    p_vertices[2].z       = 0.0f;
    p_vertices[2].diffuse = D3DCOLOR_ARGB(255, 255, 255, 255);
    p_vertices[2].rhw     = 1.0f;
    p_vertices[2].tu      = 1.0f;
    p_vertices[2].tv      = 1.0f;

    p_vertices[3].x       = -0.5f;       // left
    p_vertices[3].y       = f_height - 0.5f;   // bottom
    p_vertices[3].z       = 0.0f;
    p_vertices[3].diffuse = D3DCOLOR_ARGB(255, 255, 255, 255);
    p_vertices[3].rhw     = 1.0f;
    p_vertices[3].tu      = 0.0f;
    p_vertices[3].tv      = 1.0f;

    hr= m_pMainVertex->Unlock();
	if( FAILED(hr) ){
        TRACE(_T("%s:%d (hr=0x%0lX)"), __FUNCTION__, __LINE__, hr);
	    goto EXIT;
    }

	// Begin the scene
    hr = m_pd3dDevice->BeginScene();
	if( FAILED(hr) ){
        TRACE(_T("%s:%d (hr=0x%0lX)"), __FUNCTION__, __LINE__, hr);
        goto EXIT;
    }


    // Setup our texture. Using textures introduces the texture stage states,
    // which govern how textures get blended together (in the case of multiple
    // textures) and lighting information. In this case, we are modulating
    // (blending) our texture with the diffuse color of the vertices.
    hr = m_pd3dDevice->SetTexture(0, m_pMainTexture);
	if( FAILED(hr) ){
        TRACE(_T("%s:%d (hr=0x%0lX)"), __FUNCTION__, __LINE__, hr);
		m_pd3dDevice->EndScene();
        goto EXIT;
    }

    
    // Render the vertex buffer contents
    hr = m_pd3dDevice->SetStreamSource(0, m_pMainVertex, 0, sizeof(CUSTOMVERTEX));
    if( FAILED(hr) ){
        TRACE(_T("%s:%d (hr=0x%0lX)"), __FUNCTION__, __LINE__, hr);
		m_pd3dDevice->EndScene();
        goto EXIT;
    }

    // we use FVF instead of vertex shader
    hr = m_pd3dDevice->SetFVF(D3DFVF_CUSTOMVERTEX);
    if( FAILED(hr) ){
        TRACE(_T("%s:%d (hr=0x%0lX)"), __FUNCTION__, __LINE__, hr);
		m_pd3dDevice->EndScene();
        goto EXIT;
    }

    // draw rectangle
    hr = m_pd3dDevice->DrawPrimitive(D3DPT_TRIANGLEFAN, 0, 2);
    if( FAILED(hr) ){
        TRACE(_T("%s:%d (hr=0x%0lX)"), __FUNCTION__, __LINE__, hr);
		m_pd3dDevice->EndScene();
        goto EXIT;
    }

    // End the scene
    hr = m_pd3dDevice->EndScene();
    if( FAILED(hr) ){
        TRACE(_T("%s:%d (hr=0x%0lX)"), __FUNCTION__, __LINE__, hr);
        goto EXIT;
    }

/*
	//3. Draw Texture to Device
	hr = m_pd3dDevice->BeginScene();
	if( FAILED(hr) ){
        TRACE(_T("%s:%d (hr=0x%0lX)"), __FUNCTION__, __LINE__, hr);
        return S_FALSE;
    }

	m_pd3dDevice->SetRenderState( D3DRS_ALPHABLENDENABLE, FALSE );
	DrawImageTexture(TargetRect,TargetRect,255,0);

	m_pd3dDevice->EndScene();
*/
EXIT:
	return hr;
}


HRESULT	CDX9Render::Present(RECT srcRect, RECT destRect)
{
	if(!m_bCreate) return S_FALSE;
		    
    HRESULT hr = m_pd3dDevice->Present(&srcRect, &destRect, NULL, NULL);
    if( FAILED(hr))
        TRACE(_T("%s:%d (hr=0x%0lX)"), __FUNCTION__, __LINE__, hr);

	return hr;
}


HRESULT	CDX9Render::Present(RECT ClientRect)
{
	if(!m_bCreate) return S_FALSE;
	if(!m_hWnd) return S_FALSE;

	HRESULT hr;

	hr = m_pd3dDevice->Present(&ClientRect, &ClientRect, NULL, NULL);
    if( FAILED(hr))
        TRACE(_T("%s:%d (hr=0x%0lX)"), __FUNCTION__, __LINE__, hr);
	
	return hr;
}



BOOL	CDX9Render::SetParam(DWORD type, void *unKnown)
{
	if(!m_bCreate) return FALSE;

	if(UMFRenderChangeClientSIZE == type){
		if(m_hWnd){
			RECT rt;
			GetClientRect(m_hWnd,&rt);

			if((UINT)rt.right > m_D3DPP.BackBufferWidth || (UINT)rt.bottom > m_D3DPP.BackBufferHeight){
				BOOL bCreate = ReCreateDevice(rt.right, rt.bottom);
				return bCreate;
			}
		}
	}

	if(UMFRenderChangeMonitor == type){
		BOOL bCreate = ReCreateDevice(m_D3DPP.BackBufferWidth, m_D3DPP.BackBufferHeight);
		return bCreate;

	}

	if(UMFRenderClearDevice == type){
		Clear();
	}

	return FALSE;
}

#if 0
BOOL CDX9Render::DrawImageTexture(RECT t,RECT s,DWORD color,float zValue)
{
	HRESULT hr;
	
	CUSTOMVERTEX   *p_vertices;

	RECT rcMonitor={0,0,m_rcMonitor.right-m_rcMonitor.left,m_rcMonitor.bottom-m_rcMonitor.top};
//	if(HasRectsNoIntersection(t,rcMonitor))return FALSE;

	int t_width =t.right-t.left;
	int t_height=t.bottom-t.top;

	int monitor_width =rcMonitor.right-rcMonitor.left;
	int monitor_height=rcMonitor.bottom-rcMonitor.top;

	//Setup the Vertex Coordinate and Texture Coordinate
	int x1 = t.left  -rcMonitor.left;
	int y1 = t.top   -rcMonitor.top ;
	int x2 = t.right -rcMonitor.left;
	int y2 = t.bottom-rcMonitor.top ;

	int xorg=x1;
	int yorg=y1; 

	//texture ��ǥ.. �ϴ� pixel��ǥ�� ����.
	float u1=(float)s.left,v1=(float)s.top,u2=(float)s.right,v2=(float)s.bottom;

	float fx1=(float)x1-0.5f;
	float fy1=(float)y1-0.5f;
	float fx2=(float)x2-0.5f;
	float fy2=(float)y2-0.5f;
	
	normalface[0].dvSX = fx1; normalface[0].dvSY = fy2;
	normalface[1].dvSX = fx1; normalface[1].dvSY = fy1;
	normalface[2].dvSX = fx2; normalface[2].dvSY = fy2;
	normalface[3].dvSX = fx2; normalface[3].dvSY = fy1;

	normalface[0].dvTU = 0; normalface[0].dvTV = 1;
	normalface[1].dvTU = 0; normalface[1].dvTV = 0;
	normalface[2].dvTU = 1; normalface[2].dvTV = 1;
	normalface[3].dvTU = 1; normalface[3].dvTV = 0;

	normalface[0].dcColor=color;
	normalface[1].dcColor=color;
	normalface[2].dcColor=color;
	normalface[3].dcColor=color;

	normalface[0].dvSZ=	zValue;
	normalface[1].dvSZ=	zValue;
	normalface[2].dvSZ=	zValue;
	normalface[3].dvSZ=	zValue;

	
    // Draw the front and back faces of the cube using texture 1
	hr=	m_pd3dDevice->SetTexture(0,m_pMainTexture);
	hr=	m_pd3dDevice->SetSamplerState(0,D3DSAMP_MINFILTER,D3DTEXF_POINT);
	hr=	m_pd3dDevice->SetSamplerState(0,D3DSAMP_MAGFILTER,D3DTEXF_POINT);


    m_pd3dDevice->SetFVF( D3DFVF_FONT2DVERTEX );
	m_pd3dDevice->SetPixelShader(NULL);

	m_pd3dDevice->SetRenderState( D3DRS_ALPHABLENDENABLE, FALSE );
	hr=	m_pd3dDevice->DrawPrimitiveUP( D3DPT_TRIANGLESTRIP, 4, normalface,  sizeof(D3DTLVERTEX)); 
	m_pd3dDevice->SetTexture( 0, NULL);

	return 0;
}

#endif