#pragma once

#include "CommonInterface/IBaseUMFrame.h"

typedef struct 
{
	eFRAMETYPE		FrameType;
	TCHAR			szPath[MAX_PATH];

	CBaseUMFStorage	FrameSet;
}FRAME_INFO;


class CFrameBaseMgr 
{
public:
	enum { MAXIMUM_VIEW = 4 };
	CFrameBaseMgr();
	virtual ~CFrameBaseMgr(){}
	
	virtual BOOL	Setup(TCHAR* szFrameFolderPath, IBaseUMFStorage* pFrameXMLStrg);
	virtual void	Endup(){}

	virtual IBaseUMFrame*	CreateFrame(eFRAMETYPE FrameType, ULONG FrameFlag, IBaseUMFContainer* pContainer) = 0;

protected:
	BOOL			m_bSetup;
	FRAME_INFO		m_FrameInfo;
	BOOL			ParseFrameXml(PXML pInFrameXML);
};
