#include "stdafx.h"
#include "SourceDllMgr.h"
#include "XMLUtils.h"

//////////////////////////////////////////////////////////////////////////
CSourceDllMgr::CSourceDllMgr(void)
{
	m_Status = CREATED;

 	m_nSrcFacHandleCnt = 0;
 	memset(m_hMMSrcFacArray, 0, sizeof(m_hMMSrcFacArray));
	
}


CSourceDllMgr::~CSourceDllMgr(void)
{
	Endup();
}


BOOL	CSourceDllMgr::Setup(TCHAR *szSourceFolderPath, IBaseUMFStorage* pSrcMgrXMLStrg)
{
	if(!CSourceBaseMgr::Setup(szSourceFolderPath, pSrcMgrXMLStrg)){
		return FALSE;
	}

	if(!LoadSrcFacDlls(szSourceFolderPath)) {
		return FALSE;
	}

	m_Status = READY;
	return TRUE;
}


void	CSourceDllMgr::Endup()
{
	UINT i = 0;
	CSourceBaseMgr::Endup();
	
	for(i=0; i < m_nSrcFacHandleCnt; i++) {
		if(m_hMMSrcFacArray[i]) {
			FreeLibrary(m_hMMSrcFacArray[i]);
			m_hMMSrcFacArray[i]= NULL;
		}
	}
	m_nSrcFacHandleCnt = 0;
}


IBaseUMFSourceFactory*	CSourceDllMgr::CreateSrcFac(SRCTYPE SrcType)
{
	HMODULE hModule = GetMatchedSrcFacModule(SrcType);
	if(!hModule) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, 
			_T("Can not find source factory in source module folder [SrcFacType= %d]\n"), SrcType);
		return NULL;
	}

	IBaseUMFSourceFactory* pMMSourceFactory = NULL;
	IBaseUMFSourceFactory* (FAR WINAPI*_CreateSourceFactory)();
	FARPROC test_proc = GetProcAddress(hModule, "CreateSourceFactory");
	if (!test_proc) return NULL;

	*(FARPROC*)&_CreateSourceFactory = test_proc;
	return  _CreateSourceFactory();
}


HMODULE	CSourceDllMgr::GetMatchedSrcFacModule(SRCTYPE srcType)
{
	SRCTYPE (FAR WINAPI*_GetSourceType)();

	for(UINT i=0; i < m_nSrcFacHandleCnt; i++) {
		FARPROC test_proc = GetProcAddress(m_hMMSrcFacArray[i], "GetSourceType");
		if(!test_proc) continue;
		_GetSourceType = (SRCTYPE (FAR WINAPI*)())(test_proc);

		SRCTYPE _srcType = _GetSourceType();
		if(_srcType == srcType) {
			return m_hMMSrcFacArray[i];
		}
	}

	return NULL;
}


BOOL	CSourceDllMgr::LoadSrcFacDlls(TCHAR *szSourcePath)
{
	WIN32_FIND_DATA FindFileData;
	HANDLE hFind = INVALID_HANDLE_VALUE;

	TCHAR szFindFileFilter[MAX_PATH];
	_tcscpy_s(szFindFileFilter, szSourcePath);
	_tcscat_s(szFindFileFilter, _T("\\*"));

	// Look up "szSourcePath"
	hFind = FindFirstFile(szFindFileFilter, &FindFileData);
	if (hFind == INVALID_HANDLE_VALUE){
		UMFTrace(UMF_TRACE_LEVEL_WARNING, 
			_T("Invalid Module Path handle. Error is %u\n"), GetLastError());
		return FALSE;
	} 

	// Load Source Factory Handles
	HMODULE hSrcFacModule = NULL;
	while(1){
		_stprintf_s(szFindFileFilter, _T("%s\\%s"), szSourcePath, FindFileData.cFileName);

		hSrcFacModule = LoadSrcFacDllModule(szFindFileFilter);
		if(hSrcFacModule && IsValidSrcFacModule(hSrcFacModule)) {
			m_hMMSrcFacArray[m_nSrcFacHandleCnt] = hSrcFacModule;
			m_nSrcFacHandleCnt++;
		}

		if(m_nSrcFacHandleCnt >= UMF_MAXIMUM_SOURCE_FACTORY){
			UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Too many Source Module dlls in %s\n"), szSourcePath);
			break;
		}
		if(FindNextFile(hFind, &FindFileData) == 0) break;
	}

	if(m_nSrcFacHandleCnt == 0) return FALSE;

	return TRUE;
}

HMODULE	CSourceDllMgr::LoadSrcFacDllModule(TCHAR *szSrcDllFileName)
{
	if(NULL == _tcsstr(szSrcDllFileName, _T(".dll"))) {
		return NULL;
	}

	HMODULE hLib = NULL;
	hLib = LoadLibrary(szSrcDllFileName);
	if(NULL == hLib) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, 
			_T("Source [%s] can not loaded. Loadlibrary GetLastError [%d] \n"), 
			szSrcDllFileName, GetLastError());
		return NULL;
	}

	return hLib;
}

// Assume that 
// the module which has GetSourceType and CreateSourceFactory function 
// is a valid source module.
BOOL	CSourceDllMgr::IsValidSrcFacModule(HMODULE hModule)
{
	FARPROC test_proc = GetProcAddress(hModule, "GetSourceType");
	FARPROC test_proc1 = GetProcAddress(hModule, "CreateSourceFactory");
	if(!test_proc || !test_proc1) {
		return FALSE;
	}
	return TRUE;
}

