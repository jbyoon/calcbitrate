#pragma once

#include "FrameBaseMgr.h"

class CFrameStaticMgr : public CFrameBaseMgr
{
public:
	BOOL			Setup(TCHAR* szFrameFolderPath, IBaseUMFStorage* pFrameXMLStrg);
	void			Endup(){}

	IBaseUMFrame*	CreateFrame(eFRAMETYPE FrameType, ULONG FrameFlag, IBaseUMFContainer* pContainer);
};
