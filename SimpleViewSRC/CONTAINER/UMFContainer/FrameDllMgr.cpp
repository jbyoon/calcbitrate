#include "stdafx.h"
#include "FrameDllMgr.h"
#include "XMLUtils.h"


//////////////////////////////////////////////////////////////////////////
CFrameDllMgr::CFrameDllMgr(void)
{
 	m_nFrameModuleHandleCnt = 0;
 	memset(m_hFrameModuleArray, 0, sizeof(m_hFrameModuleArray));
	m_bSetup = FALSE;
}


CFrameDllMgr::~CFrameDllMgr(void)
{
	Endup();
}


BOOL	CFrameDllMgr::Setup(TCHAR* szFrameFolderPath, IBaseUMFStorage* pFrameXMLStrg)
{
	if(!CFrameBaseMgr::Setup(szFrameFolderPath, pFrameXMLStrg)){
		return FALSE;
	}
		
	if(!LoadFrameDlls(szFrameFolderPath)) {
		return FALSE;
	}

	m_bSetup = TRUE;
	return TRUE;
}


void	CFrameDllMgr::Endup()
{
	CFrameBaseMgr::Endup();

	UINT i = 0;
	for(i=0; i < m_nFrameModuleHandleCnt; i++) {
		if(m_hFrameModuleArray[i]){
			FreeLibrary(m_hFrameModuleArray[i]);
			m_hFrameModuleArray[i]= NULL;
		}
	}
	m_nFrameModuleHandleCnt = 0;
}


IBaseUMFrame*	CFrameDllMgr::CreateFrame(eFRAMETYPE FrameType, ULONG FrameFlag, IBaseUMFContainer* pContainer)
{
	if(!m_bSetup) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, 
			_T("CFrameDllMgr::CreateFrame() failed: Not initialized yet\n"));
		return FALSE;
	}

	eUMF_STATUS status = UMF_OK;

	if(m_FrameInfo.FrameType != FrameType) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, 
			_T("Frame Type [%d] was not found in App Xml file\n"), FrameType);
		return FALSE;
	}

	// search view module with "FrameType"
	HMODULE hModule = GetMatchedFrameModule(m_FrameInfo.FrameType);
	if(!hModule) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, 
			_T("Frame Type [%d] was not found in view module folder\n"), m_FrameInfo.FrameType);
		return FALSE;
	}

	IBaseUMFrame* pFrame = NULL;
	if(!TryToCreateFrame(hModule, &pFrame)) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Failed to create view: FrameType [%d]\n"), FrameType);
		return FALSE;
	}

	if(!pFrame->Open(FrameFlag, (IBaseUMFStorage*)&(m_FrameInfo.FrameSet), pContainer)) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Failed to open view: FrameType [%d]\n"), FrameType);
		return FALSE;
	}

	return pFrame;
}


HMODULE	CFrameDllMgr::GetMatchedFrameModule(eFRAMETYPE FrameType)
{
	eFRAMETYPE (FAR WINAPI*_GetFrameType)();

	for(UINT i=0; i < m_nFrameModuleHandleCnt; i++) {
		FARPROC test_proc = GetProcAddress(m_hFrameModuleArray[i], "GetFrameType");
		if(!test_proc) continue;
		*(FARPROC*)&_GetFrameType = test_proc;

		eFRAMETYPE _FrameType = _GetFrameType();
		if(_FrameType == FrameType) {
			return m_hFrameModuleArray[i];
		}
	}

	return NULL;
}


BOOL	CFrameDllMgr::TryToCreateFrame(HMODULE hFrameModule, IBaseUMFrame** ppFrame)
{
	IBaseUMFrame* pFrame = NULL;
	IBaseUMFrame* (FAR WINAPI*_CreateUMFrame)();
	FARPROC test_proc = GetProcAddress(hFrameModule, "CreateUMFrame");
	if (!test_proc) return FALSE;

	*(FARPROC*)&_CreateUMFrame = test_proc;
	pFrame = _CreateUMFrame();
	if(!pFrame) return FALSE;

	*ppFrame = pFrame;

	return TRUE;
}


BOOL	CFrameDllMgr::LoadFrameDlls(TCHAR *szFrameFolderPath)
{
	WIN32_FIND_DATA FindFileData;
	HANDLE hFind = INVALID_HANDLE_VALUE;

	TCHAR szFindFileFilter[MAX_PATH];
	_tcscpy_s(szFindFileFilter, szFrameFolderPath);
	_tcscat_s(szFindFileFilter, _T("\\*"));

	// Look up "Frame\*.*"
	hFind = FindFirstFile(szFindFileFilter, &FindFileData);
	if (hFind == INVALID_HANDLE_VALUE){
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Invalid Module Path handle. Error is %u\n"), GetLastError());
		return FALSE;
	} 

	// Load Frame Module Handles
	HMODULE hFrameModule = NULL;
	while(1){
		_stprintf_s(szFindFileFilter, _T("%s\\%s"), szFrameFolderPath, FindFileData.cFileName);

		hFrameModule = LoadFrameDllModule(szFindFileFilter);
		if(hFrameModule && IsValidFrameModule(hFrameModule)) {
			m_hFrameModuleArray[m_nFrameModuleHandleCnt] = hFrameModule;
			m_nFrameModuleHandleCnt++;
		}

		if(m_nFrameModuleHandleCnt >= UMF_MAXIMUM_VIEW){
			UMFTrace(UMF_TRACE_LEVEL_WARNING, 
				_T("Too many Frame Module dlls in %s. Max Frame Module= %d\n"), 
				szFrameFolderPath,
				UMF_MAXIMUM_VIEW);
			break;
		}
		if(FindNextFile(hFind, &FindFileData) == 0) break;
	}

	if(m_nFrameModuleHandleCnt == 0) return FALSE;

	return TRUE;
}


HMODULE	CFrameDllMgr::LoadFrameDllModule(TCHAR *szDllFileName)
{
	if(NULL == _tcsstr(szDllFileName, _T(".dll"))) {
		return NULL;
	}

	HMODULE hLib = NULL;
	hLib = LoadLibrary(szDllFileName);
	if(NULL == hLib) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, 
			_T("File [%s] LoadConfModule Can not Loadlibrary GetLastError [%d] \n"),
			szDllFileName, 
			GetLastError());
		return NULL;
	}

	return hLib;
}


// Assume that 
// the module which has GetFrameType and CreateUMFrame function 
// is a valid view module.
BOOL	CFrameDllMgr::IsValidFrameModule(HMODULE hModule)
{
	FARPROC test_proc = GetProcAddress(hModule, "GetFrameType");
	FARPROC test_proc1 = GetProcAddress(hModule, "CreateUMFrame");
	if(!test_proc || !test_proc1) {
		return FALSE;
	}
	return TRUE;
}

