// ContainerDlg.cpp : implementation file
//

#include "stdafx.h"
#include "UMFContainer.h"

//#ifdef _DEBUG
//#define new DEBUG_NEW
//#endif

#ifdef USE_STATIC_LIB

#include "FrameStaticMgr.h"
#include "SourceStaticMgr.h"

#else

#include "FrameDllMgr.h"
#include "SourceDllMgr.h"

#endif


CUMFContainer::CUMFContainer()
{
	m_bSetup		= FALSE;
	m_pUMFApp		= NULL;
	m_pCurUMFrame	= NULL;

#ifdef USE_STATIC_LIB
	m_pSourceMgr	= new CSourceStaticMgr();
	m_pFrameMgr		= new CFrameStaticMgr();
#else
	m_pSourceMgr	= new CSourceDllMgr();
	m_pFrameMgr		= new CFrameDllMgr();
#endif
}

CUMFContainer::~CUMFContainer()
{	
	Close();

	delete m_pSourceMgr;
	delete m_pFrameMgr;
}


BOOL CUMFContainer::LoadAppSetting(LPTSTR szAppXmlPath)
{
	BOOL bRet = FALSE;
	TCHAR szXmlFileName[MAX_PATH];
	if (szAppXmlPath == NULL || '\0' == szAppXmlPath[0]) {
		_tcscpy_s(szXmlFileName, _T("UMFAppSetting.xml"));
		bRet = m_AppSettingStorage.Load(szXmlFileName);
	}else if('<' == szAppXmlPath[0]) {
		bRet = m_AppSettingStorage.LoadXML(szAppXmlPath);
	}else{
		_tcscpy_s(szXmlFileName, szAppXmlPath);
		bRet = m_AppSettingStorage.Load(szXmlFileName);
	}

	if (FALSE == bRet) {
		TCHAR szErr[MAX_PATH];
		_stprintf_s(szErr, _T("Failed to load App setting XML File: \"%s\"\n"), szXmlFileName);
		UMFTrace(UMF_TRACE_LEVEL_ERROR, szErr);
		return FALSE;
	}

	return TRUE;;
}

BOOL CUMFContainer::SaveAppSetting(LPTSTR szAppXmlPath)
{
	BOOL bRet = FALSE;
	TCHAR szXmlFileName[MAX_PATH];
	if (szAppXmlPath == NULL || '\0' == szAppXmlPath[0]) {
		_tcscpy_s(szXmlFileName, _T("UMFAppSetting.xml"));
	}else{
		_tcscpy_s(szXmlFileName, szAppXmlPath);
	}
	bRet = m_AppSettingStorage.Save(szXmlFileName);

	if (FALSE == bRet) {
		TCHAR szErr[MAX_PATH];
		_stprintf_s(szErr, _T("Failed to save App setting XML File: \"%s\"\n"), szXmlFileName);
		UMFTrace(UMF_TRACE_LEVEL_ERROR, szErr);
		return FALSE;
	}
	
	return TRUE;
}

BOOL CUMFContainer::Open(IBaseUMFApp* pUMFApp)
{
	BOOL bRet	= FALSE;

	// Init Source Manager
	TCHAR* szSrcModuleFolder = m_AppSettingStorage.GetSrcModuleFolderPath();
	bRet = m_pSourceMgr->Setup(szSrcModuleFolder, m_AppSettingStorage.GetSrcMgrSetting());
	if(bRet == FALSE) {
		UMFTrace(UMF_TRACE_LEVEL_ERROR, _T("Failed to initialize Source Manager\n"));
		return FALSE;
	}

	// Init Frame Manager
	TCHAR* szFrameModuleFolder = m_AppSettingStorage.GetFrameModuleFolderPath();
	bRet = m_pFrameMgr->Setup(szFrameModuleFolder, m_AppSettingStorage.GetFrameMgrSetting());
	if(bRet == FALSE) {
		UMFTrace(UMF_TRACE_LEVEL_ERROR, _T("Failed to initialize Frame Manager\n"));
		return FALSE;
	}

	eFRAMETYPE eFrameType = (eFRAMETYPE)m_AppSettingStorage.GetFrameType();
	m_pCurUMFrame = m_pFrameMgr->CreateFrame(eFrameType, m_AppSettingStorage.GetFrameFlag(), this);
	if(!m_pCurUMFrame) {
		UMFTrace(UMF_TRACE_LEVEL_ERROR, _T("Failed to create Frame [FrameType= %d]\n"), 
			m_AppSettingStorage.GetFrameType());
		return FALSE;
	}

	m_pUMFApp	= pUMFApp; 
	m_bSetup	= TRUE;
	return TRUE;
}


void CUMFContainer::Close()
{
	if(m_bSetup){
		if(m_pCurUMFrame){
			m_pCurUMFrame->DestroySelf();
			m_pCurUMFrame = NULL;
		}

		m_pFrameMgr->Endup();
		m_pSourceMgr->Endup();
		m_bSetup = NULL;
	}
}


BOOL	CUMFContainer::Show(BOOL bShow)
{
	if(!m_pCurUMFrame)return FALSE;
	return m_pCurUMFrame->Show(bShow);
}