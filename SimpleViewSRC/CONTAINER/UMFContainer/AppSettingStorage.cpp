#include "stdafx.h"
#include <assert.h>
#include "AppSettingStorage.h"
#include "XMLUtils.h"


CAppSettingStorage::CAppSettingStorage()
{
	CoInitialize(NULL);

	m_pXMLDoc		= NULL;
	m_bLoaded		= FALSE;
	memset(m_szAppSetStrgPath, 0, sizeof(m_szAppSetStrgPath));
}

CAppSettingStorage::~CAppSettingStorage()
{
	if(m_pXMLDoc) {
		IXMLDOMDocument2*	pXMLDoc = (IXMLDOMDocument2*)m_pXMLDoc;
		pXMLDoc->Release();
		m_pXMLDoc = NULL;
	}

	CoUninitialize();
}

BOOL FileExist(TCHAR* pPathName)
{
	HANDLE hFind;
	WIN32_FIND_DATA fd;

	if ((hFind = FindFirstFile (pPathName, &fd)) != INVALID_HANDLE_VALUE) {
		FindClose (hFind);
		return TRUE;
	}
	return FALSE;
}

BOOL	CAppSettingStorage::Load(TCHAR* szFilenameOrXml, eXmlType type)
{
	if( XMLFILE == type ) {
		if( !FileExist( szFilenameOrXml ) ) {
			UMFTrace(UMF_TRACE_LEVEL_ERROR, _T("Cannot find App Setting XML file \" %s \""), szFilenameOrXml);
			return FALSE;
		}
	}

	IXMLDOMDocument2*	pXMLDoc;// = new IXMLDOMDocument2;

	HRESULT hr;
	hr = CoCreateInstance(__uuidof(DOMDocument) , NULL, CLSCTX_INPROC_SERVER, __uuidof(IXMLDOMDocument2), (void**)&pXMLDoc); 
	if(S_OK != hr){
		UMFTrace(UMF_TRACE_LEVEL_ERROR, _T("Failed to initialize IXMLDOMDocument2 object."));
		return FALSE;
	}
	m_pXMLDoc = pXMLDoc;

	pXMLDoc->resolveExternals = VARIANT_TRUE;
	pXMLDoc->async = VARIANT_FALSE;
	pXMLDoc->validateOnParse = VARIANT_TRUE;

	VARIANT_BOOL varRet = VARIANT_FALSE;
	if(XMLFILE == type) {
		_variant_t varXml(szFilenameOrXml);
		varRet = pXMLDoc->load(szFilenameOrXml);
	} else if(XMLBUFFER == type) {
		varRet = pXMLDoc->loadXML(szFilenameOrXml);
	} else {
		assert(0);
	}

	if (varRet != VARIANT_TRUE) {
		MSXML2::IXMLDOMParseErrorPtr pErr = pXMLDoc->parseError;

		TCHAR	szLine[MAX_PATH];
		TCHAR	szResult[MAX_PATH];

		_stprintf_s(szLine, _countof(szLine), _T(" ( line %u, column %u )"), pErr->Getline(), pErr->Getlinepos());
		
		// Return validation results in message to the user.
		if (pErr->errorCode != S_OK) {
			_stprintf_s(szResult, _countof(szResult), _T("Validation failed on \n====================="));
			_stprintf_s(szResult, _countof(szResult), _T("%s\nReason: %s\nSource: %s\n"), szResult, 
				(TCHAR*)pErr->Getreason(), (TCHAR*)pErr->GetsrcText());
			UMFTrace(UMF_TRACE_LEVEL_ERROR, szResult);
		}

		return FALSE;
	}

	IXMLDOMNodeListPtr	pRootXMLList;
	IXMLDOMElementPtr	pRootXML;
	bool bRet = true;


	try{
		pRootXMLList	= pXMLDoc->documentElement->getElementsByTagName(_T("APPSetting"));
		pRootXML		= pRootXMLList->item[0];

		{
			IXMLDOMNodeListPtr	pChildListXML;
			IXMLDOMElementPtr	pChildXML;
			pChildListXML	= pRootXML->getElementsByTagName(_T("SourceManager"));

			pChildXML		= pChildListXML->item[0];
			GetAttrValueString(pChildXML, _T("Path"), m_szSrcModuleFolder, MAX_PATH);

			IXMLDOMNode* pNode;
			pChildListXML->get_item(0, &pNode);
			SrcMgrSetting.SetXML((PXML)pNode);
		}

		{
			IXMLDOMNodeListPtr	pChildListXML;
			IXMLDOMElementPtr	pChildXML;
			pChildListXML	= pRootXML->getElementsByTagName(_T("FrameManager"));

			pChildXML		= pChildListXML->item[0];
			GetAttrValueString(pChildXML, _T("Path"), m_szFrameModuleFolder, MAX_PATH);

			m_FrameType		= GetAttrValueNum(pChildXML, _T("FrameType"));
			m_FrameFlag		= GetAttrValueNum(pChildXML, _T("FrameFlag"));
			
			IXMLDOMNode* pNode;
			pChildListXML->get_item(0, &pNode);
			FrameMgrSetting.SetXML((PXML)pNode);
		}
	} catch(...) {
		bRet = false;
	}

	if(bRet){
		m_bLoaded		= TRUE;
	}

	SetAppSettingFilePath(szFilenameOrXml);
	
	return TRUE;
}

BOOL	CAppSettingStorage::LoadXML(TCHAR* szXML)
{
	return Load(szXML, XMLBUFFER);
}

BOOL	CAppSettingStorage::Save(TCHAR* szFilename)
{
	if(FALSE == m_bLoaded) {
		return FALSE;
	}

	HRESULT hr;

	IXMLDOMDocument2*	pXMLDoc = (IXMLDOMDocument2*)m_pXMLDoc;

	if (NULL == szFilename) {
		hr = pXMLDoc->save(m_szAppSetStrgPath);
	} else {
		hr = pXMLDoc->save(szFilename);
	}

	if(S_OK != hr){
		return FALSE;
	}

	m_bLoaded = FALSE;

	return TRUE;
}

BOOL	CAppSettingStorage::SetAppSettingFilePath(TCHAR* szFilePath)
{
	if(NULL == szFilePath) return FALSE;
	_tcsncpy_s(m_szAppSetStrgPath, _countof(m_szAppSetStrgPath), szFilePath, _tcslen(szFilePath));
	return TRUE;
}

TCHAR*	CAppSettingStorage::GetAppSettingFilePath()
{
	return m_szAppSetStrgPath;
}

BOOL	CAppSettingStorage::SetSrcMgrSetting(IBaseUMFStorage* pSrcMgrSetting)
{
	if(FALSE == m_bLoaded) {
		return FALSE;
	}

	PXML pXml = pSrcMgrSetting->GetXML();
	if(!pXml) return FALSE;

	SrcMgrSetting.SetXML(pXml);

	return TRUE;
}

IBaseUMFStorage*	CAppSettingStorage::GetSrcMgrSetting()
{
	if(FALSE == m_bLoaded) {
		return NULL;
	}

	return (IBaseUMFStorage*)&SrcMgrSetting;
}

BOOL	CAppSettingStorage::SetFrameMgrSetting(IBaseUMFStorage* pFrameMgrSetting)
{
	if(FALSE == m_bLoaded) {
		return FALSE;
	}

	PXML pXml = pFrameMgrSetting->GetXML();
	if(!pXml) return FALSE;

	FrameMgrSetting.SetXML(pXml);

	return TRUE;
}

IBaseUMFStorage*	CAppSettingStorage::GetFrameMgrSetting()
{
	if(FALSE == m_bLoaded) {
		return NULL;
	}

	return (IBaseUMFStorage*)&FrameMgrSetting;
}

#if 0
BOOL	CAppSettingStorage::SetSrcSetting(IBaseUMFStorage* pSrcSetting)
{
	if(FALSE == m_bLoaded) {
		return FALSE;
	}

	PXML pXml = pSrcSetting->GetXML();
	if(!pXml) return FALSE;

	if(!SrcSetting.SetXML(pXml)) return FALSE;

	return TRUE;
}

IBaseUMFStorage*	CAppSettingStorage::GetSrcSetting()
{
	if(FALSE == m_bLoaded) {
		return NULL;
	}

	return (IBaseUMFStorage*)&SrcSetting;
}

BOOL	CAppSettingStorage::SetSrcFacSetting(IBaseUMFStorage* pSrcFacSetting)
{
	if(FALSE == m_bLoaded) {
		return FALSE;
	}

	PXML pXml = pSrcFacSetting->GetXML();
	if(!pXml) return FALSE;

	if(!SrcFacSetting.SetXML(pXml)) return FALSE;

	return TRUE;
}

IBaseUMFStorage*	CAppSettingStorage::GetSrcFacSetting()
{
	if(FALSE == m_bLoaded) {
		return NULL;
	}

	return (IBaseUMFStorage*)&SrcFacSetting;
}

BOOL	CAppSettingStorage::SetEngineSetting(IBaseUMFStorage* pEngSetting)
{
	if(FALSE == m_bLoaded) {
		return FALSE;
	}

	PXML pXml = pEngSetting->GetXML();
	if(!pXml) return FALSE;

	if(!EngineSetting.SetXML(pXml)) return FALSE;

	return TRUE;
}

IBaseUMFStorage*	CAppSettingStorage::GetEngineSetting()
{
	if(FALSE == m_bLoaded) {
		return NULL;
	}

	return (IBaseUMFStorage*)&EngineSetting;
}

BOOL	CAppSettingStorage::SetFrameSetting(IBaseUMFStorage* pFrameSetting)
{
	if(FALSE == m_bLoaded) {
		return FALSE;
	}

	PXML pXml = pFrameSetting->GetXML();
	if(!pXml) return FALSE;

	if(!FrameSetting.SetXML(pXml)) return FALSE;

	return TRUE;
}

IBaseUMFStorage*	CAppSettingStorage::GetFrameSetting()
{
	if(FALSE == m_bLoaded) {
		return NULL;
	}

	return (IBaseUMFStorage*)&FrameSetting;
}
#endif