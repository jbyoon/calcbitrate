#include "stdafx.h"
#include "SourceBaseMgr.h"
#include "XMLUtils.h"
#include <assert.h>


//////////////////////////////////////////////////////////////////////////
CSourceBaseMgr::CSourceBaseMgr(void)
{
	m_Status = CREATED;

	m_nSrcFacCnt	= 0;
	m_nSourceCnt	= 0;

 	m_nSrcInfoCnt	= 0;
 	m_nSrcFacInfoCnt= 0;
}


BOOL	CSourceBaseMgr::Setup(TCHAR* szSrcFolderPath, IBaseUMFStorage* pSrcMgrXMLStrg)
{
	if((NULL == szSrcFolderPath) || (NULL == pSrcMgrXMLStrg)) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Invalid Parameter\n"));
		return FALSE;
	}

	PXML pSrcMgrXML = pSrcMgrXMLStrg->GetXML();
	if(!pSrcMgrXML) {
		return FALSE;
	}

	// Parse Source Manager XML
	if(!ParseSrcMgrXml(pSrcMgrXML)) {
		return FALSE;
	}

	return TRUE;
}


void	CSourceBaseMgr::Endup()
{
	UINT i = 0;

	for(i=0; i<m_nSourceCnt; i++) {
		if(m_aSource[i]) m_aSource[i]->DestroySelf();
	}
	m_nSourceCnt = 0;

	for(i=0; i<m_nSrcFacCnt; i++) {
		if(m_aSrcFac[i].pSrcFactory) m_aSrcFac[i].pSrcFactory->DestroySelf();
	}
	ZeroMemory(m_aSrcFac, sizeof(m_aSrcFac));
	m_nSrcFacCnt = 0;

	m_nSrcFacInfoCnt = 0;
//	ZeroMemory(m_aSrcFactoryInfo, sizeof(m_aSrcFactoryInfo));
	m_nSrcInfoCnt = 0;
//	ZeroMemory(m_aSrcInfo, sizeof(m_aSrcInfo));
}


eUMF_STATUS	CSourceBaseMgr::GetSource(UINT uSrcFacId, UINT uSrcId, IBaseUMFSource** ppSource)
{
	eUMF_STATUS status = UMF_OK;
	IBaseUMFSource* pSource = NULL;

	// are there pre-created source ?
	pSource = GetCreatedSource(uSrcId);
	if(pSource) {
		*ppSource = pSource;
		return UMF_OK;
	}
	

	// create new source from here
	// search SourceSetting XML with "uSrcId"
	SRC_INFO* pSrcInfo = NULL;
	for(UINT i=0; i<m_nSrcInfoCnt; i++) {
		if(m_aSrcInfo[i].Id == uSrcId) {
			pSrcInfo = &m_aSrcInfo[i];
			break;
		}
	}

	if(pSrcInfo != NULL) {	// case that there is source info
		// create source using the source info
		pSource = CreateSource(pSrcInfo->SrcFacId);
		if(!pSource) {
			UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Failed to create source by Source Id= %d\n"), pSrcInfo->Id);
			return UMF_ERR_FAILED;
		}

		// Init the Source
		CBaseUMFStorage* pSrcSet = &(pSrcInfo->SrcSet);
		status = pSource->Open(pSrcInfo->Id, (IBaseUMFStorage*)pSrcSet);
		if(status != UMF_OK) {
			pSource->DestroySelf();
			UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Failed to open source by Source Id= %d\n"), pSrcInfo->Id);
			return UMF_ERR_FAILED;	// Source Open Error
		}
		m_aSource[m_nSourceCnt] = pSource;
		m_nSourceCnt++;

	} else {	// case of No source info in xml
		if(uSrcFacId == UMF_ID_NONE) {
			UMFTrace(UMF_TRACE_LEVEL_ERROR,
				_T("Failed to create source.\
				   \nThere is no source info in setting xml file [Source Id= %d]\
				   \nPlease check the setting xml file\n"), 
				uSrcId);
			return UMF_ERR_FAILED;
		}
		// create source using the factory which has source Id = "uSrcFacId"
		pSource = CreateSource(uSrcFacId);
		if(!pSource) {
			UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Failed to create source by Source Factory Id= %d\n"), uSrcFacId);
			return UMF_ERR_FAILED;
		}

		// ShowConfigDlg(pXmlElement)
		// m_aSrcInfo[m_nSrcInfoCnt].Id = uSrcId;
		// add new infor to m_aSrcInfo
		// m_aSrcInfo[0].SrcSet.SetXML(pXmlElement)
		// m_nSrcInfoCnt++;
		// pSrcInfo = 

		// Init the Source
		CBaseUMFStorage* pSrcSet = &(pSrcInfo->SrcSet);
		status = pSource->Open(pSrcInfo->Id, (IBaseUMFStorage*)pSrcSet);
		if(status != UMF_OK) {
			pSource->DestroySelf();
			UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Failed to open source by Source Id= %d\n"), pSrcInfo->Id);
			return UMF_ERR_FAILED;	// Source Open Error
		}
		m_aSource[m_nSourceCnt] = pSource;
		m_nSourceCnt++;
	}

	*ppSource = pSource;
	
	return UMF_OK;
}

eUMF_STATUS	CSourceBaseMgr::ReleaseSource(IBaseUMFSource* pSource)
{
	UINT i, j;
	for(i=0; i<m_nSourceCnt; i++) {
		if(pSource == m_aSource[i]) {
			if(m_aSource[i]) m_aSource[i]->DestroySelf();
			for(j=i; j<m_nSourceCnt-1; j++) {
				m_aSource[j] = m_aSource[j+1];
			}
			m_nSourceCnt--;
			return UMF_OK;
		}
	}

//	Check_DeleteNotUsedFactory();
	
	return UMF_ERR_FAILED;
}

IBaseUMFSource*	CSourceBaseMgr::CreateSource(UINT uSrcFacId)
{
	if(m_Status != READY) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Source Manager not initialized yet\n)"));
		return NULL;
	}

	if(m_nSourceCnt >= UMF_MAXIMUM_SOURCE) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Can't create source more than %d\n"), UMF_MAXIMUM_SOURCE);
		return NULL;
	}

	SRC_FAC_INFO* pSrcFacInfo = NULL;
	// GetMatchedSrcFacInfo (Key : SrcFacId)
	for(UINT j=0; j<m_nSrcFacInfoCnt; j++) {
		if(uSrcFacId == m_aSrcFactoryInfo[j].Id) {
			pSrcFacInfo = &m_aSrcFactoryInfo[j];
			break;
		}
	}

	if(!pSrcFacInfo) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Source factory [Id=%d] was not found in App Xml file\n"), uSrcFacId);
		return NULL; // can't find source information from AppXML
	}

	eUMF_STATUS status = UMF_OK;

	// Search PreCreated Source Factory (if it is not found, create it)
	IBaseUMFSourceFactory* pSrcFac = NULL;
	if(!GetPreCreatedSrcFacById(pSrcFacInfo->Id, &pSrcFac)) {

		pSrcFac = CreateSrcFac(pSrcFacInfo->SrcType);
		if(!pSrcFac) {
			UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Failed to create source factory: SrcFacType [%d]\n"), 
				pSrcFacInfo->SrcType);
			return NULL;
		}

		// Set up the created source factory
		CBaseUMFStorage* pSrcFacSet = &(pSrcFacInfo->SrcFacSet);
		status = pSrcFac->Open(pSrcFacInfo->Id, (IBaseUMFStorage*)pSrcFacSet);
		if(status != UMF_OK) {
			UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Failed to open source factory by SrcFacId= %d\n"), 
				pSrcFacInfo->Id);
			return NULL;
		}

		m_aSrcFac[m_nSrcFacCnt].pSrcFactory = pSrcFac;
		m_aSrcFac[m_nSrcFacCnt].Id = pSrcFacInfo->Id;
		m_nSrcFacCnt++;
	}

	// Create a Source
	IBaseUMFSource* pSource = pSrcFac->CreateUMFSource();
	if(!pSource) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Failed to create source factory [SrcFacId= %d]\n"), 
			pSrcFacInfo->Id);
		return NULL;
	}

	return pSource;
}




HWND	CSourceBaseMgr::ShowConfigDlg(UINT SrcId, HWND hParentWnd)
{
	UINT i = 0;
	SRCTYPE srcType = UMF_ST_NONE;
	for(i=0; i<m_nSourceCnt; i++) {
		if(SrcId == m_aSource[i]->GetSourceId()) {
			srcType = m_aSource[i]->GetSourceType();
			break;
		}
	}

	if(srcType == UMF_ST_NONE) {
		UMFTrace(UMF_TRACE_LEVEL_ERROR, 
			_T("Can not find source module to show configuration dialog [Source Id= %d]\n"), SrcId);
		return NULL;
	}

	SRC_INFO* pSrcInfo = NULL;
	for(UINT i=0; i<m_nSrcInfoCnt; i++) {
		if(m_aSrcInfo[i].Id == SrcId) {
			pSrcInfo = &m_aSrcInfo[i];
			break;
		}
	}

	if(!pSrcInfo) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, 
			_T("Can not find source info in setting xml file [Source Id= %d]\n"), SrcId);
		return NULL;
	}

	for(i=0; i<m_nSrcFacCnt; i++) {
		SRCTYPE srcTypeFac = m_aSrcFac[i].pSrcFactory->GetSourceType();
		if(srcType == srcTypeFac) {
			return m_aSrcFac[i].pSrcFactory->ShowConfigDlg(SrcId, hParentWnd, (IBaseUMFStorage*)&pSrcInfo->SrcSet);
		}
	}

	return NULL;
}


eUMF_STATUS	CSourceBaseMgr::GetSrcFacList(UINT uSrcFacIdx, UINT* uSrcFacId, SRCTYPE* SrcType, TCHAR* szDescStr, UINT ccDescStrMaxSize)
{
	if(!uSrcFacId || !SrcType || !szDescStr) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Invalid Parameter\n"));
		return UMF_ERR_INVALID_PARAMS;
	}

	if(uSrcFacIdx >= m_nSrcInfoCnt) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Exceeded Max Index of Source Factory Set\n"));
		return UMF_ERR_FAILED;
	}

	SRC_FAC_INFO* pSrcFacInfo = NULL;
	pSrcFacInfo = GetSrcFacInfo(uSrcFacIdx);
	if(!pSrcFacInfo) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, 
			_T("Can not find source factory info in setting xml file [Source Factory Id= %d]\n"), uSrcFacIdx);
		return UMF_ERR_FAILED; // can't find source information from AppXML
	}

	*uSrcFacId	= pSrcFacInfo->Id;
	*SrcType	= pSrcFacInfo->SrcType;
	_tcscpy_s(szDescStr, ccDescStrMaxSize, pSrcFacInfo->szDesc);

	return UMF_OK;
}

eUMF_STATUS	CSourceBaseMgr::GetSourceSettings(UINT uSrcId, IBaseUMFStorage **ppXmlStorage)
{
	SRC_INFO* pSrcInfo = NULL;
	for(UINT i=0; i<m_nSrcInfoCnt; i++) {
		if(m_aSrcInfo[i].Id == uSrcId) {
			pSrcInfo = &m_aSrcInfo[i];
			break;
		}
	}

	if(!pSrcInfo) return UMF_ERR_FAILED;

	*ppXmlStorage = (IBaseUMFStorage*) &(pSrcInfo->SrcSet);

	return UMF_OK;
}

BOOL	CSourceBaseMgr::ParseSrcMgrXml(PXML pInSrcMgrXML)
{
	IXMLDOMElementPtr SrcMgrXML, SrcFacXML, pSrcFacXMLChild;
	IXMLDOMNode*	pSrcMgrXML = (IXMLDOMNode*)pInSrcMgrXML;
	SrcMgrXML = (IXMLDOMNodePtr)pSrcMgrXML;
	BOOL bRet = TRUE;
	

	try{
		TCHAR* szXml = (TCHAR*)SrcMgrXML->xml;

		IXMLDOMNodeListPtr	pSrcFacListXML;
		pSrcFacListXML	= SrcMgrXML->getElementsByTagName(_T("SourceFactorySetting"));
		IXMLDOMNode* pSrcFacNode;
		pSrcFacListXML->get_item(0, &pSrcFacNode);
		if(!pSrcFacNode) {
			UMFTrace(UMF_TRACE_LEVEL_WARNING, 
				_T("Can not find \"SourceFactorySetting\" in setting xml file\n"));
			return FALSE;
		}

		IXMLDOMNodeListPtr	pSrcListXML;
		pSrcListXML	= SrcMgrXML->getElementsByTagName(_T("SourceSetting"));
		IXMLDOMNode* pSrcNode;
		pSrcListXML->get_item(0, &pSrcNode);
		if(!pSrcFacNode) {
			UMFTrace(UMF_TRACE_LEVEL_WARNING, 
				_T("Can not find \"SourceSetting\" in setting xml file\n"));
			return FALSE;
		}

		szXml = (TCHAR*)pSrcNode->xml;

		// Parse Source Factory XML
		if(!ParseSrcFacXml((PXML)pSrcFacNode)) {
			UMFTrace(UMF_TRACE_LEVEL_WARNING, 
				_T("Failed to parse source factory info in setting xml file\n"));
			return FALSE;
		}

		// Parse Source XML
		if(!ParseSrcXml((PXML)pSrcNode)) {
			UMFTrace(UMF_TRACE_LEVEL_WARNING, 
				_T("Failed to parse source info in setting xml file\n"));
			return FALSE;
		}

	}catch(...){
		bRet = FALSE;
	}

	return bRet;
}


BOOL	CSourceBaseMgr::ParseSrcFacXml(PXML pInSrcFacXML)
{
	IXMLDOMElementPtr SrcFacXML, pSrcFacXMLChild;
	IXMLDOMElement*	pSrcFacXML = (IXMLDOMElement*)pInSrcFacXML;
	SrcFacXML = (IXMLDOMElementPtr)pSrcFacXML;
	int i;
	BOOL bRet = TRUE;

	try{
		TCHAR* szXml = (TCHAR*)SrcFacXML->xml;
		i = 0;
		for_ChildNodeEnum(SrcFacXML, pSrcFacXMLChild){
			szXml = (TCHAR*)pSrcFacXMLChild->xml;
			m_aSrcFactoryInfo[i].Id = GetAttrValueNum(pSrcFacXMLChild, _T("Id"));
			m_aSrcFactoryInfo[i].SrcType = (SRCTYPE)GetAttrValueNum(pSrcFacXMLChild, _T("SourceType"));
			GetAttrValueString(pSrcFacXMLChild, _T("Description"), m_aSrcFactoryInfo[i].szDesc, _countof(m_aSrcFactoryInfo[i].szDesc));

			IXMLDOMNode* pNode;
			pSrcFacXMLChild->raw_cloneNode(VARIANT_TRUE, &pNode);
			m_aSrcFactoryInfo[i].SrcFacSet.SetXML((PXML)pNode);

 			i++;
 		}
	}catch(...){
		bRet = FALSE;
	}
	
	m_nSrcFacInfoCnt = i;

	return bRet;
}

BOOL	CSourceBaseMgr::ParseSrcXml(PXML pInSrcXML)
{
	IXMLDOMElementPtr SrcXML, pSrcXMLChild;
	IXMLDOMElement*	pSrcXML = (IXMLDOMElement*)pInSrcXML;
	SrcXML = (IXMLDOMElementPtr)pSrcXML;
	int i;
	BOOL bRet = TRUE;

	try{
		TCHAR* szXml = (TCHAR*)SrcXML->xml;
		i = 0;

		IXMLDOMNode* pSrcXMLChild = 0;
		HRESULT _hr = SrcXML->get_firstChild(&pSrcXMLChild);
		if (FAILED(_hr)) assert(0);

		while (pSrcXMLChild) {
			TCHAR* szXml = pSrcXMLChild->xml;

			m_aSrcInfo[i].Id	= GetAttrValueNum(pSrcXMLChild, _T("Id"));
			m_aSrcInfo[i].SrcFacId	= (INT)GetAttrValueNum(pSrcXMLChild, _T("SrcFacId"));
			m_aSrcInfo[i].SrcSet.SetXML((PXML)pSrcXMLChild);

			_hr = pSrcXMLChild->get_nextSibling(&pSrcXMLChild);
			if (FAILED(_hr)) assert(0);
			i++;
		}

	}catch(...){
		bRet = FALSE;
	}


	m_nSrcInfoCnt = i;

	return bRet;
}

IBaseUMFSource*	CSourceBaseMgr::GetCreatedSource(UINT SrcId)
{
	if(m_Status != READY) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Not Initialized\n"));
		return NULL;
	}

	for(UINT i=0; i<m_nSourceCnt; i++) {
		if(SrcId == m_aSource[i]->GetSourceId()) {
			return m_aSource[i];
		}
	}
	return NULL;
}

BOOL	CSourceBaseMgr::GetPreCreatedSrcFacById(UINT uSrcFacId, IBaseUMFSourceFactory** ppSrcFac)
{
	for(UINT i=0; i<m_nSrcFacCnt; i++) {
		if(m_aSrcFac[i].Id == uSrcFacId) {
			*ppSrcFac = m_aSrcFac[i].pSrcFactory;
			return TRUE;
		}
	}

	*ppSrcFac = NULL;
	return FALSE;
}


SRC_FAC_INFO*	CSourceBaseMgr::GetSrcFacInfo(UINT uSrcFacId)
{

	SRC_FAC_INFO* pSrcFacInfo = NULL;
	for(UINT j=0; j<m_nSrcFacInfoCnt; j++) {
		if(uSrcFacId == m_aSrcFactoryInfo[j].Id) {
			pSrcFacInfo = &m_aSrcFactoryInfo[j];
			break;
		}
	}
	return pSrcFacInfo;
}

