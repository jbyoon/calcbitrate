#pragma once

#include "CommonInterface/IBaseUMFSource.h"
#include "CommonInterface/IBaseUMFSourceMgr.h"

// Store SourceFactorySetting in UMFAppSetting.xml
struct SRC_FAC_INFO
{
	UINT		Id;
	SRCTYPE		SrcType;
	TCHAR		szDesc[MAX_PATH];
	
	CBaseUMFStorage	SrcFacSet;
};

// Store SourceSetting in UMFAppSetting.xml
struct SRC_INFO
{
	UINT	Id;
	INT		SrcFacId;

	CBaseUMFStorage	SrcSet;
};

struct SRC_FAC
{
	DWORD	Id;
	IBaseUMFSourceFactory*	pSrcFactory;
};


class CSourceBaseMgr : public IBaseUMFSourceMgr
{
public:
	CSourceBaseMgr();
	virtual ~CSourceBaseMgr(){}

	//implemnet by parent
	virtual BOOL	Setup(TCHAR* szSrcFolderPath, IBaseUMFStorage* pSrcMgrXMLStrg);
	virtual void	Endup();
	virtual IBaseUMFSourceFactory*	CreateSrcFac(SRCTYPE SrcType){return NULL;}


	eUMF_STATUS		GetSource(UINT uSrcFacId, UINT uSrcId, IBaseUMFSource** ppSource);
	eUMF_STATUS		ReleaseSource(IBaseUMFSource* pSource);

	HWND			ShowConfigDlg(UINT SrcId, HWND hParentWnd);

	eStatus			GetStatus() { return m_Status; }
	UINT			GetSrcFacCnt() { return m_nSrcInfoCnt; }
	eUMF_STATUS		GetSrcFacList(UINT uSrcFacIdx, UINT* uSrcFacId, SRCTYPE* SrcType, TCHAR* szDescStr, UINT ccDescStrMaxSize);

	eUMF_STATUS		GetSourceSettings(UINT uSrcId, IBaseUMFStorage **ppXmlStorage);


protected:

	eStatus			m_Status;
	
	UINT			m_nSrcFacCnt;
	SRC_FAC			m_aSrcFac[UMF_MAXIMUM_SOURCE_FACTORY];	// only containing created source factory instances
 
	DWORD			m_nSourceCnt;	
	IBaseUMFSource*	m_aSource[UMF_MAXIMUM_SOURCE];	// only containing created source instances

	UINT			m_nSrcFacInfoCnt;
	SRC_FAC_INFO	m_aSrcFactoryInfo[UMF_MAXIMUM_SOURCE_FACTORY];

	UINT			m_nSrcInfoCnt;
	SRC_INFO		m_aSrcInfo[UMF_MAXIMUM_SOURCE];

	
	BOOL			ParseSrcMgrXml(PXML pInSrcMgrXML);
	BOOL			ParseSrcFacXml(PXML pInSrcFacXML);
	BOOL			ParseSrcXml(PXML pInSrcXML);

	IBaseUMFSource*	CreateSource(UINT uSrcFacId);
	IBaseUMFSource*	GetCreatedSource(UINT SrcId);
	SRC_FAC_INFO*	GetSrcFacInfo(UINT uSrcFacId);
	BOOL			GetPreCreatedSrcFacById(UINT uSrcFacId, IBaseUMFSourceFactory** ppSrcFac);
	
};
