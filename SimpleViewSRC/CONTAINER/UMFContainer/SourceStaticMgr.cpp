#include "stdafx.h"
#include "SourceStaticMgr.h"
#include "XMLUtils.h"
#include "StaticLoadConfigure.h"

//////////////////////////////////////////////////////////////////////////
CSourceStaticMgr::~CSourceStaticMgr()
{
	Endup();
}


BOOL	CSourceStaticMgr::Setup(TCHAR *szSourceFolderPath, IBaseUMFStorage* pSrcMgrXMLStrg)
{
	if(!CSourceBaseMgr::Setup(szSourceFolderPath, pSrcMgrXMLStrg)){
		return FALSE;
	}
	m_Status		= READY;
	return TRUE;
}


void	CSourceStaticMgr::Endup()
{
	CSourceBaseMgr::Endup();
}



IBaseUMFSourceFactory*	CSourceStaticMgr::CreateSrcFac(SRCTYPE SrcType)
{
	
	for(UINT i=0; i < MAXNVSOURCEODULE_CNT ; i++) {
		if(g_STATICSOURCEModule[i].Type == SrcType) {
			return (IBaseUMFSourceFactory*)g_STATICSOURCEModule[i].Func();
		}
	}

	UMFTrace(UMF_TRACE_LEVEL_ERROR, _T("CSourceStaticMgr Source factory was not Support Type [%d]\n"), SrcType);
	// Create a Source Factory
	return NULL;
}