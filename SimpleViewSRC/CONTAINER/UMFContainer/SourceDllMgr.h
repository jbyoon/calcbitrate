#pragma once

#include "SourceBaseMgr.h"

class CSourceDllMgr : public CSourceBaseMgr
{
public:
	CSourceDllMgr();
	~CSourceDllMgr();

	BOOL		Setup(TCHAR* szSrcFolderPath, IBaseUMFStorage* pSrcMgrXMLStrg);
	void		Endup();

	IBaseUMFSourceFactory*	CreateSrcFac(SRCTYPE SrcType);
	
private:

	UINT		m_nSrcFacHandleCnt;
	HMODULE		m_hMMSrcFacArray[UMF_MAXIMUM_SOURCE_FACTORY];
	
	BOOL		LoadSrcFacDlls(TCHAR* szSrcFolderPath);
	HMODULE		LoadSrcFacDllModule(TCHAR *szSrcDllFileName);
	BOOL		IsValidSrcFacModule(HMODULE hModule);

	BOOL		GetPreCreatedSrcFacById(UINT uSrcFacId, IBaseUMFSourceFactory** ppSrcFac);
	HMODULE		GetMatchedSrcFacModule(SRCTYPE SrcType);
	BOOL		CreateSrcFac(HMODULE hSrcFacModule, IBaseUMFSourceFactory** ppSrcFac);

};
