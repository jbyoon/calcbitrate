#pragma once

#include "SourceBaseMgr.h"

class CSourceStaticMgr : public CSourceBaseMgr
{
public:
	CSourceStaticMgr(){}
	~CSourceStaticMgr();

	BOOL			Setup(TCHAR* szSrcFolderPath, IBaseUMFStorage* pSrcMgrXMLStrg);
	void			Endup();

	IBaseUMFSourceFactory*	CreateSrcFac(SRCTYPE SrcType);
};
