#include "stdafx.h"
#include "FrameStaticMgr.h"
#include "XMLUtils.h"

//define FRAME module
#pragma comment( lib, "../../LIB/Frames/sNVFrame" )

BOOL		CFrameStaticMgr::Setup(TCHAR* szFrameFolderPath, IBaseUMFStorage* pFrameXMLStrg)
{
	if(!CFrameBaseMgr::Setup(szFrameFolderPath, pFrameXMLStrg)){
		return FALSE;
	}
	
	if(m_FrameInfo.FrameType != GetFrameType()){
		UMFTrace(UMF_TRACE_LEVEL_ERROR, _T("CFrameStaticMgr does not support Type[%d] Only support [%d]\n"),
			m_FrameInfo.FrameType, GetFrameType());
		return FALSE;
	}

	m_bSetup = TRUE;
	return TRUE;
}


IBaseUMFrame*	CFrameStaticMgr::CreateFrame(eFRAMETYPE FrameType, ULONG FrameFlag, IBaseUMFContainer* pContainer)
{
	if(!m_bSetup) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, 
			_T("CFrameMgr::CreateFrame() failed: Not initialized yet\n"));
		return FALSE;
	}

	eUMF_STATUS status = UMF_OK;

	if(m_FrameInfo.FrameType != FrameType) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, 
			_T("Frame Type [%d] was not found in App Xml file\n"), FrameType);
		return FALSE;
	}
	
	IBaseUMFrame* pFrame = CreateUMFrame();;
	if(!pFrame) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Failed to open view: FrameType [%d]\n"), FrameType);
		return FALSE;
	}

	if(!pFrame->Open(FrameFlag, (IBaseUMFStorage*)&(m_FrameInfo.FrameSet), pContainer)) {
		pFrame->DestroySelf();
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Failed to open view: FrameType [%d]\n"), FrameType);
		return FALSE;
	}

	return pFrame;
}
