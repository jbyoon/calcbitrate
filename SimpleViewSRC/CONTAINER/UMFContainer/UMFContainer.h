// ContainerDlg.h : header file
//

#pragma once

#include "AppSettingStorage.h"
#include "SourceBaseMgr.h"
#include "FrameBaseMgr.h"
#include "CommonInterface/IBaseUMFContainer.h"

// CUMFContainer
class CUMFContainer : public IBaseUMFContainer
{
// Construction
public:
	CUMFContainer();	// standard constructor
	~CUMFContainer();

	BOOL	LoadAppSetting(LPTSTR szAppXmlPath = NULL);
	BOOL	SaveAppSetting(LPTSTR szAppXmlPath = NULL);

	BOOL	Open(IBaseUMFApp* pUMFApp);
	void	Close();
	void	DestorySelf(){delete this;}

	BOOL	Show(BOOL bShow);

	//IBaseUMFContainer interface
	IBaseUMFSourceMgr*	GetSourceMgr(){return m_pSourceMgr;}
	LONG				SendUMFCommand(ULONG nEngId, wchar_t* szCmd, wchar_t* szSubCmd, wchar_t* szValue, void* pParam){return FALSE;}
	void				OnUMFEvent(ULONG nEngId, wchar_t* szEvent, wchar_t* szSubEvent, wchar_t* szValue){}
	void				OnInternalUMFEvent() {}

	HWND				GetContainerHWnd(){return NULL;}

	IBaseUMFrame*		GetCurUMFrame(){return m_pCurUMFrame;}

//<JL>
	LONG CreateMMProc(LPCTSTR szName, LPCTSTR szDLLFile) {return 0;}
	LONG DestroyMMProc(LONG iMMProcID) {return 0;}
	LONG ConnectMMProcs(LONG iSourceID, LONG iDestID) {return 0;}
	LONG DisconnectMMProcs(LONG iSourceID, LONG iDestID) {return 0;}
	LONG PositionMMProc(LONG iMMProcID, double dRelPosX, double dRelPosY, double dRelWidth, double dRelHeight) {return 0;}
//</JL>
private:
	
	BOOL				m_bSetup;
	IBaseUMFApp*		m_pUMFApp;

	CAppSettingStorage	m_AppSettingStorage;
	CSourceBaseMgr		*m_pSourceMgr;
	CFrameBaseMgr		*m_pFrameMgr;

	IBaseUMFrame*		m_pCurUMFrame;

};
