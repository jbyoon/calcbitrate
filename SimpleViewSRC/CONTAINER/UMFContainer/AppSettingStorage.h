#pragma once

#include "CommonInterface/IBaseUMFStorage.h"

class CAppSettingStorage
{
public:
	typedef enum { 
		XMLFILE		= 1,
		XMLBUFFER	= 2
	} eXmlType;

	CAppSettingStorage(void);
	~CAppSettingStorage(void);

	BOOL		Load(TCHAR* szFilenameOrXml, eXmlType type=XMLFILE);
	BOOL		LoadXML(TCHAR* szXML);
	BOOL		Save(TCHAR* szFilename=NULL);

	BOOL		SetAppSettingFilePath(TCHAR* szFilePath);
	TCHAR*		GetAppSettingFilePath();

	TCHAR*		GetSrcModuleFolderPath() { return m_szSrcModuleFolder;}
	TCHAR*		GetFrameModuleFolderPath() { return m_szFrameModuleFolder;}
	DWORD		GetFrameType() { return m_FrameType;}
	DWORD		GetFrameFlag() { return m_FrameFlag;}

	BOOL				SetSrcMgrSetting(IBaseUMFStorage* pSrcMgrSetting);
	IBaseUMFStorage*	GetSrcMgrSetting();

	BOOL				SetFrameMgrSetting(IBaseUMFStorage* pFrameMgrSetting);
	IBaseUMFStorage*	GetFrameMgrSetting();

private:
	PXML		m_pXMLDoc;
	BOOL		m_bLoaded;

	TCHAR		m_szAppSetStrgPath[MAX_PATH];
	DWORD		m_FrameType;
	DWORD		m_FrameFlag;
	TCHAR		m_szSrcModuleFolder[MAX_PATH];
	TCHAR		m_szFrameModuleFolder[MAX_PATH];

	CBaseUMFStorage	SrcMgrSetting;
	CBaseUMFStorage	FrameMgrSetting;
};
