#include "stdafx.h"
#include "FrameBaseMgr.h"
#include "XMLUtils.h"

CFrameBaseMgr::CFrameBaseMgr()
{
	m_bSetup	= FALSE;
	m_FrameInfo.FrameType	= UMF_VT_NONE;
}

BOOL	CFrameBaseMgr::Setup(TCHAR* szFrameFolderPath, IBaseUMFStorage* pFrameXMLStrg)
{
	if( (NULL == szFrameFolderPath) || (NULL == pFrameXMLStrg) ) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Invalid Parameter\n"));
		return FALSE;
	}

	PXML pFrameXML = pFrameXMLStrg->GetXML();
	if(!pFrameXML) {
		return FALSE;
	}

	// Parse Frame XML
	if(!ParseFrameXml(pFrameXML)) {
		return FALSE;
	}

	m_FrameInfo.FrameSet.SetXML((PXML)pFrameXML);
	m_bSetup	= TRUE;
	return TRUE;
}


BOOL	CFrameBaseMgr::ParseFrameXml(PXML pInFrameXML)
{
	IXMLDOMElementPtr FrameXML, pFrameXMLChild;
	IXMLDOMNode*	pFrameXML = (IXMLDOMNode*)pInFrameXML;
	FrameXML = (IXMLDOMElementPtr)pFrameXML;
	BOOL bRet = TRUE;

	try{
		TCHAR* szXml = (TCHAR*)FrameXML->xml;
		m_FrameInfo.FrameType = (eFRAMETYPE)GetAttrValueNum(FrameXML, _T("FrameType"));
		
	}catch(...){
		bRet = FALSE;
	}
	
	return bRet;
}
