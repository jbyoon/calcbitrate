#pragma once

#include "FrameBaseMgr.h"

class CFrameDllMgr : public CFrameBaseMgr
{
public:
	CFrameDllMgr();
	~CFrameDllMgr();

	BOOL			Setup(TCHAR* szFrameFolderPath, IBaseUMFStorage* pFrameXMLStrg);
	void			Endup();

	IBaseUMFrame*	CreateFrame(eFRAMETYPE FrameType, ULONG FrameFlag, IBaseUMFContainer* pContainer);


private:

	UINT		m_nFrameModuleHandleCnt;
	HMODULE		m_hFrameModuleArray[MAXIMUM_VIEW];

	BOOL		LoadFrameDlls(TCHAR* szFrameFolderPath);
	HMODULE		LoadFrameDllModule(TCHAR *szDllFileName);
	BOOL		IsValidFrameModule(HMODULE hModule);
	HMODULE		GetMatchedFrameModule(eFRAMETYPE FrameType);
	BOOL		TryToCreateFrame(HMODULE hFrameModule, IBaseUMFrame** ppFrame);
};
