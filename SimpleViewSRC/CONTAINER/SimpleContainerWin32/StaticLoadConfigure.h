#ifndef __STATIC_LOAD_CONFIGURE__
#define __STATIC_LOAD_CONFIGURE__

//define FRAME module
#pragma comment( lib, "../../LIB/Frames/sNVFrame" )

#define MAXNVSOURCEODULE_CNT	2

#pragma comment( lib, "../../LIB/Sources/sCompFileVideoSource" )
#pragma comment( lib, "../../LIB/Sources/sRTSPSource" )


extern "C" IBaseUMFSourceFactory* CreateCompFileSourceFactory();
extern "C" IBaseUMFSourceFactory* CreateRTSPSourceFactory();

typedef struct{
	SRCTYPE		Type;
	FARPROC		Func;
}STATICSOURCEModule;

STATICSOURCEModule  g_STATICSOURCEModule[MAXNVSOURCEODULE_CNT] ={
	{UMF_ST_VID_COMPFILE,(FARPROC)CreateCompFileSourceFactory},
	{UMF_ST_VID_RTSP|UMF_ST_AUD_RTSP|UMF_ST_META_VCA|UMF_ST_RAW_RTSP,(FARPROC)CreateRTSPSourceFactory}

};


#endif