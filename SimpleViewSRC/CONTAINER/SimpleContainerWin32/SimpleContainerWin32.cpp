// SimpleContainerWin32.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "..\UMFContainer\UMFContainer.h"

HINSTANCE g_UMFInstance;
HINSTANCE GetUMFStaticInstance(){return g_UMFInstance;}

int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);
 
	g_UMFInstance = hInstance;
//	UMFSetTraceOption(NULL, UMF_TRACE_DISPLAY_ALL);
//	UMFSetDebugFileName(NULL, TRUE);
	CUMFContainer UMFContainer;	
	
	if(lpCmdLine[0] == 0) lpCmdLine = NULL;
	if(!UMFContainer.LoadAppSetting(lpCmdLine)) {
		return 0;
	}

	if(!UMFContainer.Open(NULL)){
		return 0;
	}

	UMFContainer.Show(TRUE);

	
	return 0;
}
