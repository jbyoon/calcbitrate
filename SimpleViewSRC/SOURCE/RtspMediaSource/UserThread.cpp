#include "stdafx.h"
#include <process.h>

#include "UserThread.h"

CUserThread::CUserThread()
{
	m_hThread = NULL;
	m_hKill = NULL;
	m_uThreadState = TS_STOP;
}

CUserThread::~CUserThread()
{
	StopThread(TRUE, 1, FALSE);
}

BOOL CUserThread::StartThread(PTHREAD_FUNC pFunc, PVOID pParam1, PVOID pParam2, PVOID pParam3)
{
	if (m_uThreadState == TS_RUN) {
		return FALSE;
	}

	m_hKill = CreateEvent(NULL, FALSE, FALSE, NULL);
	if (!m_hKill) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("CUserThread::StartThread: create kill event failed\n"));
		return FALSE;
	}
	
	m_ParamSet.pParam1 = this;
	m_ParamSet.pParam2 = pParam1;
	m_ParamSet.pParam3 = pParam2;
	m_ParamSet.pParam4 = pParam3;

	m_hThread = (HANDLE)_beginthreadex(NULL, 0, pFunc, &m_ParamSet, 0, NULL);
	if (!m_hThread) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("CUserThread::StartThread: create thread failed\n"));
		return FALSE;
	}

	m_uThreadState = TS_RUN;

	return TRUE;
}

VOID CUserThread::StopThread(BOOL bWait, UINT uTime, BOOL bEvent)
{
	if (m_uThreadState == TS_STOP) {
		return;
	}

	if (m_hKill && m_hThread && bWait) {
		if (bEvent) {
			SetEvent(m_hKill);
		}
		if (uTime > 0) {
			UINT uExitCode;
			GetExitCodeThread(m_hThread, (LPDWORD)&uExitCode);
			if (WaitForSingleObject(m_hThread, uTime) != WAIT_OBJECT_0) {
				TerminateThread(m_hThread, uExitCode);
			}
		}else{
			WaitForSingleObject(m_hThread, INFINITE);
		}
	}
	ReleaseHandle();

	m_uThreadState = TS_STOP;
}

VOID CUserThread::ReleaseHandle()
{
	if (m_uThreadState == TS_STOP) {
		return;
	}

	if (m_hKill && m_hThread) {
		CloseHandle(m_hThread);
		m_hThread = NULL;
		CloseHandle(m_hKill);
		m_hKill = NULL;
	}

	m_uThreadState = TS_STOP;
}
