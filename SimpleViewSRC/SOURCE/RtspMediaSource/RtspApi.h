// RtspApi.h: interface for the CRtspApi class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_RTSPAPI_H__D87F9FFD_B7C1_4632_B50E_0E9705F43080__INCLUDED_)
#define AFX_RTSPAPI_H__D87F9FFD_B7C1_4632_B50E_0E9705F43080__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "./library/CRtspClient.hpp"

class AFX_EXT_CLASS CRtspApi  
{
public:
	CRtspApi();
	virtual ~CRtspApi();

	int Init();
	int DeInit();
	int SetQue(int iVideoCnt, int iAudioCnt);
	int Connect(char * strUrl, bool bLogin, char * strId, char * strPw, HANDLE _heventVQue, HANDLE _heventAQue, int iQueSize/*1~128*/ );
	int Disconnect();
	int Pause();
	int Resume();
	int GetVideoQueCount();
	int GetAudioQueCount();
	int GetVideo();
	int GetAudio();
	int GetSPS(char * strSPS, int iLen);
	int GetPPS(char * strPPS, int iLen);
	int GetWidth();
	int GetHeight();
	char * GetCodec();
	HWND GetConnectionEventHandel();
	
	unsigned char * m_pVideoStream;
	unsigned char * m_pAudioStream;	

private:
	CRtspClient * m_pcRtspStream;
	HANDLE m_hEventErr;
	unsigned int m_iVideoStreamLen;
	unsigned int m_iAudioStreamLen;
};

#endif // !defined(AFX_RTSPAPI_H__D87F9FFD_B7C1_4632_B50E_0E9705F43080__INCLUDED_)
