#ifndef __CHttpClient__
#define __CHttpClient__

#include <Wininet.h>
#pragma comment(lib, "Wininet.lib")

#include "CQueue.hpp"
#include "nvccommon.h"

#ifdef __USE_CUSTOM_DSP_HDR__
#include "nvchdr.h"
#endif

/*
	Windows Internet API (Wininet) for Client Side
*/

/*
	 This is Basic HTTP Client Class
	 If you only want to use for HTTP-API communication (get/setting configuration using HTTP-API), please use this class instead of CHttpClient.
	 CHTTPClient class requires much more memory (over 60MB) due to the static video/audio que!!!
*/
class CHttpClientBasic
{
public: 
	CHttpClientBasic();
	~CHttpClientBasic();

	HRESULT SetupLastErrorEvent( HANDLE _hErrorEvent );
	DWORD   GetLastError( OUT LPTSTR _lpszErrName );

	HRESULT Init();
	HRESULT	Deinit();
	BOOL    IsInit() { return m_hInet ? TRUE : FALSE; };

	HANDLE Connect( LPTSTR	_lpszIP,
					INT		_nPort,
					LPTSTR	_lpszUsername,
					LPTSTR	_lpszPassword );
	HRESULT Disconnect( HANDLE _hSession );

	/*
		CGI request "POST" method
		_lpszObject : "/nvc-cgi/admin/param.cgi"
	*/
	HANDLE	OpenRequest( HANDLE _hSession,
						  BOOL	_fIsHTTPS,
						  BOOL	_fIsIgnoreCA,
						  LPTSTR _lpszObject,
						  BOOL	_fUseCache = TRUE);

	/*
		CGI request "POST" method
		_lpszOption : "action=xxx&group=yyy&..."
	*/
	BOOL	SendRequest( HANDLE			_hOpenRequest,
						 LPTSTR			_lpszOption,
						 IN OUT BYTE*	_lpcbResponse,
						 IN OUT DWORD*	_lpdwResponseLength );

	BOOL	SendRequest_Content_Type_APP_Form(	HANDLE			_hOpenRequest,
												LPTSTR			_lpszOption,
												IN OUT BYTE*	_lpcbResponse,
												IN OUT DWORD*	_lpdwResponseLength );

	HRESULT CloseRequest( HANDLE _hOpenRequest );

protected:
	HANDLE		m_eventError;
	DWORD		m_dwLastErrorCode;
	TCHAR		m_szLastErrorName[64];

private:
	HINTERNET	m_hInet;
};

/*	
	This is the extended HTTP class, which could also be used for audio/video streaming using HTTP
	Actually m_cVQue and m_cAQue is class for static que, which requires over 60MBytes memory!!!
*/

class CHttpClient : public CHttpClientBasic
{
public:
	CHttpClient();
	~CHttpClient();

	BOOL	AVStreamRequest( HANDLE _hOpenRequest, LPTSTR _lpszOption, HANDLE _hVQueEvent, HANDLE _hAQueEvent );
	BOOL	AVStreamCancel();

public:	
	CQue	m_cVQue;
	CQue	m_cAQue;

protected:
	VOID	_CreateQueryThreadAndEvent( ULONG _ulContextPtr );
	VOID	_DestroyQueryThreadAndEvent();
	static LPTHREAD_START_ROUTINE _lpthreadQuery( LPVOID _pParam );

	HANDLE		m_eventVQue;
	HANDLE		m_eventAQue;

private:
	HANDLE		m_eventStartQuery;
	HANDLE		m_threadQuery;
	DWORD		m_dwthreadQuery;
	HINTERNET	m_hAVStreamOpenRequest;
};

#endif //#ifndef __CHttpClient__
