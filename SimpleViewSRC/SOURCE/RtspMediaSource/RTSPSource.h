// RTSPSource.h: interface for the CRTSPSource class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_RTSPSource_H__88592B80_ADEE_4847_8DC8_03475EEDF54A__INCLUDED_)
#define AFX_RTSPSource_H__88592B80_ADEE_4847_8DC8_03475EEDF54A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "RTSPClientAPI/IRTSPClientAPI.h"
#include "UserThread.h"
#include "CapQueueLock.h"
#include <winsock.h>

typedef struct
{
	TCHAR	szURL[MAX_PATH];
	TCHAR	szSession[16];
	TCHAR	szID[IDPWMAXLENGTH];
	TCHAR	szPW[IDPWMAXLENGTH];
	UINT	uAliveCheckTime;
	BOOL    bForceContinue;
	BOOL	bCreateConnectionLog;
	TCHAR	pathConnectionLog[MAX_PATH];
	TCHAR	szWEBURL[MAX_PATH];// sean
} RTSPSOURCEINFO;

struct MEDIA_INFO {
	UINT uMediaType;
	UINT uFrameSize;
	BYTE* pBuffer;
	struct timeval timeStamp;
};

#define UMF_ST_MEDIA_RTSP (UMF_ST_VID_RTSP | UMF_ST_AUD_RTSP | UMF_ST_META_VCA)

class CRTSPSource  : public IBaseUMFSource
{
public:
	CRTSPSource();
	virtual ~CRTSPSource(){Close();}

	// IBaseUMFSource Implementation
	SRCTYPE		GetSourceType();
	eStatus		GetStatus() { return m_Status; }

	eUMF_STATUS	Open(UINT SourceId, IBaseUMFStorage *pXmlStorage);
	void		Close();

	eUMF_STATUS	Start();
	void		Stop();

	UINT		GetSourceId() { return m_SourceID; }
	PXML		GetSourceInfo();

	HANDLE		GetEvent(){ return m_hEvent; }
	eUMF_STATUS	GetMMData(SRCTYPE* pType, BYTE **pMMData, UINT* pMMDataLen, FILETIME *timestamp);
	eUMF_STATUS	ReleaseMMData();

	void		DestroySelf() { delete this; }
	//~IBaseUMFSource Implementation

private:
	BOOL			RtspInit();
	BOOL			MakeThread();
	eUMF_STATUS		LogCreate();
	eUMF_STATUS		LogConnected(BOOL bConnected);
	eUMF_STATUS		LogDisconnected();
	eUMF_STATUS		LogNoVideo(LPVOID _logvidcap, LPVOID _lognvcencoder, LPVOID _logrtspstreamer, DWORD _lenvid, DWORD _lennvcen, DWORD _lenrtsp);
	eUMF_STATUS		LogNoVideoForPing();

	static UINT WINAPI ThreadProcMediaDataWrapper(LPVOID pParam);
	UINT WINAPI ThreadProcMediaData(LPVOID pParam);

	static UINT WINAPI ThreadCheckConnectionWrapper(LPVOID pParam);
	UINT WINAPI ThreadCheckConnection(LPVOID pParam);
	
private:
	IBaseUMFStorage*	m_pXmlStorage;		// Input Xml
	PXML				m_pSrcInfoXMLDoc;	// Output Xml

	IRTSPClientAPI*		m_pRTSPClientAPI;
	RTSPSOURCEINFO		m_rtspInfoSource;

	CUserThread			m_mediaThread;
	CUserThread			m_checkThread;
	CCapQueueLock<struct MEDIA_INFO, 64>	m_mmQue;

	eStatus		m_Status;
	DWORD		m_SourceID;
	HANDLE		m_hEvent;

	CHAR		m_szVideoCodec[8];
	UINT		m_uWidth;
	UINT		m_uHeight;

	CHAR		m_szAudioCodec[8];
	UINT		m_uSampleRate;
	UINT		m_uSampleBits;

	BOOL		m_bForceContinue;
	BOOL		m_bReconnectFlag;

	HANDLE		m_hConnectionLog;
	TCHAR		m_filenameConnectionLog[MAX_PATH];
	BOOL		m_bConnected;
	LONGLONG	m_timeLastDataRecieved;
};


class CRTSPSourceFactory : public IBaseUMFSourceFactory
{
public:
	CRTSPSourceFactory();


	// IBaseUMFSourceFactory Implementation
	SRCTYPE		GetSourceType();
	eUMF_STATUS	Open(UINT SrcFactoryId, IBaseUMFStorage *pXmlStorage);
	void		Close();	

	IBaseUMFSource* 	CreateUMFSource();

	HWND		ShowConfigDlg(UINT SrcId, HWND hParentWnd, IBaseUMFStorage *pXmlStorage);
	eUMF_STATUS	ApplyCurSetting(IBaseUMFStorage *pXmlStorage);

	void		DestroySelf() {delete this;}
	//~IBaseUMFSourceFactory Implementation


	BOOL	OnWinMsg(UINT uMsg, WPARAM wParam, LPARAM lParam);


	static BOOL	CALLBACK DialogProcWrap(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);
private:
	
	BOOL			m_bSetup;
	DWORD			m_SrcFactoryId;

	HWND			m_hSourceSetDlg;

	DWORD			m_SourceID;
	IBaseUMFStorage*m_pXmlStorage;

	RTSPSOURCEINFO	m_rtspInfoSource;
};


#endif // !defined(AFX_RTSPSource_H__88592B80_ADEE_4847_8DC8_03475EEDF54A__INCLUDED_)
