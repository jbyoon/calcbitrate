// RTSPSource.cpp: implementation of the CRTSPSource class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "RTSPSource.h"
#include <malloc.h>

#include "XMLUtils.h"
#include "UMFUtil.h"
#include "resource.h"


// sean for cgi
#include <Wininet.h>
#pragma comment(lib, "Wininet.lib")

// sean for ping <start>
#include <stdio.h> 
#include <errno.h>   /* errno */ 
#include <string.h>  /* strerror */ 
  
#ifdef WIN32 
#define popen _popen  
#define pclose _pclose 
#endif 

// sean for ping <end>

#ifndef TRACE
#include <atlbase.h>
#define TRACE	ATLTRACE
#endif

/*
#ifdef _DEBUG
#pragma comment( lib, "../../LIB/RTSPClientAPI/sRTSPClientAPI_d" )
#else
#pragma comment( lib, "../../LIB/RTSPClientAPI/sRTSPClientAPI" )
#endif
*/
//#pragma comment( lib, "../../LIB/RTSPClientAPI/RTSPClientAPI" )

CHAR szWEBURL[MAX_PATH];//sean
wchar_t szWEBURL_wchar[MAX_PATH];//sean
VOID SystemResReturn(CHAR* cmd, CHAR* szretrun, INT len);//sean

BOOL	GetRtspSourceInfo(PXML pRtspSourceInfoXMLElement, RTSPSOURCEINFO *pRtspSourceInfo)
{

	IXMLDOMNode*		pSourceXML = (IXMLDOMNode*)pRtspSourceInfoXMLElement;
	try {
		TCHAR* pXml = pSourceXML->xml;

		GetAttrValueString(pSourceXML, _T("RTSPURL"), pRtspSourceInfo->szURL, _countof(pRtspSourceInfo->szURL));
		GetAttrValueString(pSourceXML, _T("SESSION"), pRtspSourceInfo->szSession, _countof(pRtspSourceInfo->szSession));
		GetAttrValueString(pSourceXML, _T("ID"), pRtspSourceInfo->szID, _countof(pRtspSourceInfo->szID));
		GetAttrValueString(pSourceXML, _T("PW"), pRtspSourceInfo->szPW, _countof(pRtspSourceInfo->szPW));
		pRtspSourceInfo->uAliveCheckTime = GetAttrValueNum(pSourceXML, _T("AliveCheckTime"));
		if(pRtspSourceInfo->uAliveCheckTime == 0) pRtspSourceInfo->uAliveCheckTime = 10;
		pRtspSourceInfo->bForceContinue = GetAttrValueNum(pSourceXML, _T("ForceContinue")); // 0=FALSE, 1=TRUE
		pRtspSourceInfo->bCreateConnectionLog = GetAttrValueNum(pSourceXML, _T("CreateConnectionLog"));	// 0=FALSE, 1=TRUE
		GetAttrValueString(pSourceXML, _T("PathConnectionLog"), pRtspSourceInfo->pathConnectionLog, _countof(pRtspSourceInfo->pathConnectionLog));

		GetAttrValueString(pSourceXML, _T("WEBURL"), pRtspSourceInfo->szWEBURL, _countof(pRtspSourceInfo->szWEBURL));// sean
	} catch (...) {
		return FALSE;
	}
    return TRUE;
}

BOOL	SetRtspSourceInfo(PXML pRtspSourceInfoXMLElement, RTSPSOURCEINFO *pRtspSourceInfo)
{
	IXMLDOMElementPtr	pEngineXML;
	IXMLDOMElement*		pRawEngineXML = (IXMLDOMElement*)pRtspSourceInfo;
	try	{	
		pEngineXML = (IXMLDOMElementPtr)pRawEngineXML;
		SetAttrValueString(pEngineXML, _T("RTSPURL"), pRtspSourceInfo->szURL);
		SetAttrValueString(pEngineXML, _T("SESSION"), pRtspSourceInfo->szSession);
		SetAttrValueString(pEngineXML, _T("ID"), pRtspSourceInfo->szID);
		SetAttrValueString(pEngineXML, _T("PW"), pRtspSourceInfo->szPW);
		SetAttrValueNum(pEngineXML, _T("AliveCheckTime"), pRtspSourceInfo->uAliveCheckTime);
	} catch (...) {
		return FALSE;	
	}
	return TRUE;
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
CRTSPSource::CRTSPSource() :
	m_pXmlStorage(NULL),
	m_pSrcInfoXMLDoc(NULL),
	m_pRTSPClientAPI(NULL),
	m_Status(INIT),
	m_hEvent(NULL),
	m_uWidth(0),
	m_uHeight(0),
	m_uSampleRate(0),
	m_uSampleBits(0),
	m_hConnectionLog(INVALID_HANDLE_VALUE),
	m_bConnected(FALSE)
{	
	ZeroMemory(m_szVideoCodec, sizeof(m_szVideoCodec));
	ZeroMemory(m_szAudioCodec, sizeof(m_szAudioCodec));
	m_bForceContinue = FALSE;
	m_bReconnectFlag = FALSE;
}

BOOL CRTSPSource::RtspInit()
{
	CHAR szURL[MAX_PATH];
	CHAR szSession[16];
	CHAR szID[IDPWMAXLENGTH];
	CHAR szPW[IDPWMAXLENGTH];

	m_uWidth = 0;
	m_uHeight = 0;

	const TCHAR* RTSPCLIENTDLL = _T("RTSPClientAPI.dll");

	if (!m_pRTSPClientAPI){
		HMODULE	hModule = LoadUMFLibrary((TCHAR*)RTSPCLIENTDLL);
		UMF_CREATE_INSTANCE(m_pRTSPClientAPI, hModule, IRTSPClientAPI,CreateRTSPClientAPI);
		if(!m_pRTSPClientAPI){
			if(TRUE != m_bForceContinue) {
				UMFTrace(UMF_TRACE_LEVEL_ERROR, _T("Cannot find %s\n"), RTSPCLIENTDLL);
			}
			return FALSE;
		}
	}

	if (!m_pRTSPClientAPI->Init()) {
		if(TRUE != m_bForceContinue) {
			UMFTrace(UMF_TRACE_LEVEL_ERROR, _T("Out of Memory. %s failed to allocate media buffers\n"), RTSPCLIENTDLL);
		}
		return FALSE;
	}

	WideCharToMultiByte(CP_ACP, 0, m_rtspInfoSource.szURL, -1, szURL, sizeof(szURL), NULL, NULL);
	WideCharToMultiByte(CP_ACP, 0, m_rtspInfoSource.szSession, -1, szSession, sizeof(szSession), NULL, NULL);
	WideCharToMultiByte(CP_ACP, 0, m_rtspInfoSource.szID, -1, szID, sizeof(szURL), NULL, NULL);
	WideCharToMultiByte(CP_ACP, 0, m_rtspInfoSource.szPW, -1, szPW, sizeof(szURL), NULL, NULL);
	WideCharToMultiByte(CP_ACP, 0, m_rtspInfoSource.szWEBURL, -1, szWEBURL, sizeof(szWEBURL), NULL, NULL);

	wsprintfW(szWEBURL_wchar, _T("%s"), szWEBURL);

	TRACE("test url : %s",m_rtspInfoSource.szURL);
	TRACE("test url : %s",szURL);
	TRACE("test url : %s",m_rtspInfoSource.szWEBURL);
	TRACE("test url : %s",szWEBURL);
	m_pRTSPClientAPI->SetParam("AliveCheckTime", &m_rtspInfoSource.uAliveCheckTime);

	if (!m_pRTSPClientAPI->Connect(szURL, szSession, szID, szPW)) {
		if(TRUE != m_bForceContinue) {
			UMFTrace(UMF_TRACE_LEVEL_ERROR, _T("Cannot connect to %s\n"), m_rtspInfoSource.szURL);
		}
		LogConnected(FALSE);
		//return FALSE;
	} else {
		LogConnected(TRUE);
	}

	return TRUE;
}

eUMF_STATUS CRTSPSource::LogCreate()
{
	TCHAR pathCreate[MAX_PATH];
	_tcscpy_s(pathCreate, MAX_PATH-1, m_rtspInfoSource.pathConnectionLog);
	if (!CreateDirectory(pathCreate, NULL)) {
		if (GetLastError()!=ERROR_ALREADY_EXISTS) {
			return UMF_ERR_FAILED;
		}
	}

	TCHAR rtspurl[MAX_PATH];
	_tcscpy_s(rtspurl, MAX_PATH-1, m_rtspInfoSource.szURL);
	for (size_t i=0; i<_tcslen(rtspurl); i++) {
		if (rtspurl[i]==_T(':') || rtspurl[i]==_T('/')) {
			rtspurl[i] = _T('_');
		}
	}
	wsprintf(pathCreate, _T("%s\\%s"), m_rtspInfoSource.pathConnectionLog, rtspurl);
	if (!CreateDirectory(pathCreate, NULL)) {
		if (GetLastError()!=ERROR_ALREADY_EXISTS) {
			return UMF_ERR_FAILED;
		}
	}

	SYSTEMTIME localTime;
	GetLocalTime(&localTime);

	TCHAR filename[MAX_PATH];
	wsprintf(filename, _T("%s\\%04d_%02d_%02d_%02d_%02d_connection_log.txt"), pathCreate, localTime.wYear, localTime.wMonth, localTime.wDay, localTime.wHour, localTime.wMinute);
	_tcscpy_s(m_filenameConnectionLog, MAX_PATH-1, filename);

	m_hConnectionLog = CreateFile(m_filenameConnectionLog, GENERIC_WRITE, FILE_SHARE_READ, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if (m_hConnectionLog == INVALID_HANDLE_VALUE) {
		return UMF_ERR_FAILED;
	}

	CloseHandle(m_hConnectionLog);

	return UMF_OK;
}

eUMF_STATUS CRTSPSource::LogConnected(BOOL bConnected)
{
	m_hConnectionLog = CreateFile(m_filenameConnectionLog, GENERIC_WRITE, FILE_SHARE_READ, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if (m_hConnectionLog == INVALID_HANDLE_VALUE) {
		return UMF_ERR_FAILED;
	}

	SYSTEMTIME localTime;
	GetLocalTime(&localTime);

	if (SetFilePointer(m_hConnectionLog, 0, NULL, FILE_END) == INVALID_SET_FILE_POINTER) {
		return UMF_ERR_FAILED;
	}

	char msg[MAX_PATH];
	DWORD written = 0;
	sprintf_s(msg, MAX_PATH-1, "%04d/%02d/%02d %02d:%02d:%02d %s\n", localTime.wYear, localTime.wMonth, localTime.wDay, localTime.wHour, localTime.wMinute, localTime.wSecond, bConnected ? "connected ---" : "connection failed");
	WriteFile(m_hConnectionLog, msg, strlen(msg), &written, NULL);

	CloseHandle(m_hConnectionLog);

	m_bConnected = bConnected;
	
	//sean. m_timeLastDataRecieved = GetTickCount64();
	m_timeLastDataRecieved = (ULONGLONG) GetTickCount();


	return UMF_OK;
}

eUMF_STATUS CRTSPSource::LogDisconnected()
{
	m_bConnected = FALSE;

	m_hConnectionLog = CreateFile(m_filenameConnectionLog, GENERIC_WRITE, FILE_SHARE_READ, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if (m_hConnectionLog == INVALID_HANDLE_VALUE) {
		return UMF_ERR_FAILED;
	}

	SYSTEMTIME localTime;
	GetLocalTime(&localTime);

	if (SetFilePointer(m_hConnectionLog, 0, NULL, FILE_END) == INVALID_SET_FILE_POINTER) {
		return UMF_ERR_FAILED;
	}

	char msg[MAX_PATH];
	DWORD written = 0;
	sprintf_s(msg, MAX_PATH-1, "%04d/%02d/%02d %02d:%02d:%02d disconnected\n", localTime.wYear, localTime.wMonth, localTime.wDay, localTime.wHour, localTime.wMinute, localTime.wSecond);
	WriteFile(m_hConnectionLog, msg, strlen(msg), &written, NULL);

	CloseHandle(m_hConnectionLog);

	return UMF_OK;
}

eUMF_STATUS CRTSPSource::LogNoVideo(LPVOID _logvidcap, LPVOID _lognvcencoder, LPVOID _logrtspstreamer, DWORD _lenvid, DWORD _lennvcen, DWORD _lenrtsp)
{
	m_hConnectionLog = CreateFile(m_filenameConnectionLog, GENERIC_WRITE, FILE_SHARE_READ, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if (m_hConnectionLog == INVALID_HANDLE_VALUE) {
		return UMF_ERR_FAILED;
	}

	if (SetFilePointer(m_hConnectionLog, 0, NULL, FILE_END) == INVALID_SET_FILE_POINTER) {
		return UMF_ERR_FAILED;
	}

	DWORD written = 0;

	// log vidcap
	WriteFile(m_hConnectionLog, "\n[VidCap Log] start\n", lstrlen(L"\n[VidCap Log] start\n"), &written, NULL);
	WriteFile(m_hConnectionLog, _logvidcap, _lenvid, &written, NULL);

	// log nvcencoder
	WriteFile(m_hConnectionLog, "\n[NVCEncoder Log] start\n", lstrlen(L"\n[NVCEncoder Log] start\n"), &written, NULL);
	WriteFile(m_hConnectionLog, _lognvcencoder, _lennvcen, &written, NULL);

	// log rtspstreamer
	WriteFile(m_hConnectionLog, "\n[RTSPStreamer Log] start\n", lstrlen(L"\n[RTSPStreamer Log] start\n"), &written, NULL);
	WriteFile(m_hConnectionLog, _logrtspstreamer, _lenrtsp, &written, NULL);
	

	// ping
	WriteFile(m_hConnectionLog, "\n[Ping Test] start\n", lstrlen(L"\n[Ping Test] start\n"), &written, NULL);

	CHAR szcmd[MAX_PATH];
	CHAR szcmdResult[1024*20];

	memset(szcmdResult, 0, sizeof(szcmdResult));
	sprintf_s(szcmd, MAX_PATH-1, "ping -n 20 %s", szWEBURL);
	SystemResReturn(szcmd, szcmdResult, 1024*20);

	TRACE("*** Ping Result : %s\n", szcmdResult);
	WriteFile(m_hConnectionLog, szcmdResult, strlen(szcmdResult), &written, NULL);

	WriteFile(m_hConnectionLog, "\n================================================================================================\n", lstrlen(L"\n================================================================================================\n"), &written, NULL);
	CloseHandle(m_hConnectionLog);



	return UMF_OK;
}


UINT WINAPI CRTSPSource::ThreadCheckConnectionWrapper(LPVOID pParam)
{
	struct THREAD_PARAM_SET *pParamSet = (struct THREAD_PARAM_SET*)pParam;
	return static_cast<CRTSPSource*>(pParamSet->pParam2)->ThreadCheckConnection(pParam);
}

UINT WINAPI CRTSPSource::ThreadCheckConnection(LPVOID pParam)
{
	struct THREAD_PARAM_SET *pParamSet = (struct THREAD_PARAM_SET*)pParam;
	BOOL bAlive = FALSE;
	HANDLE hEvent = ((CUserThread*)pParamSet->pParam1)->GetKillHandle();

	CHAR szURL[MAX_PATH];
	CHAR szSession[16];
	CHAR szID[IDPWMAXLENGTH];
	CHAR szPW[IDPWMAXLENGTH];

	while (1) {
		UINT uRs = WaitForSingleObject(hEvent, 1000);
		if (WAIT_OBJECT_0 == uRs) {
			break;
		} else if (WAIT_TIMEOUT == uRs) {
			m_pRTSPClientAPI->GetParam("Alive", &bAlive, sizeof(bAlive));

			if (!bAlive) {
				WideCharToMultiByte(CP_ACP, 0, m_rtspInfoSource.szURL, -1, szURL, sizeof(szURL), NULL, NULL);
				WideCharToMultiByte(CP_ACP, 0, m_rtspInfoSource.szSession, -1, szSession, sizeof(szSession), NULL, NULL);
				WideCharToMultiByte(CP_ACP, 0, m_rtspInfoSource.szID, -1, szID, sizeof(szURL), NULL, NULL);
				WideCharToMultiByte(CP_ACP, 0, m_rtspInfoSource.szPW, -1, szPW, sizeof(szURL), NULL, NULL);

				m_pRTSPClientAPI->Disconnect();
				LogDisconnected();

				m_mmQue.RemoveAll();

				if (!m_pRTSPClientAPI->Connect(szURL, szSession, szID, szPW)) {
					LogConnected(FALSE);
					continue;
				}
				LogConnected(TRUE);
				
				m_bReconnectFlag = TRUE;

				if (!m_pRTSPClientAPI->GetParam("VideoCodec", m_szVideoCodec, sizeof(m_szVideoCodec))) {
					UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("GetParam(VideoCodec) failed\n"));
					continue;
				}

				if (!m_pRTSPClientAPI->GetParam("Width", &m_uWidth, sizeof(m_uWidth))) {
					UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("GetParam(Width) failed\n"));
					continue;
				}

				if (!m_pRTSPClientAPI->GetParam("Height", &m_uHeight, sizeof(m_uHeight))) {
					UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("GetParam(Height) failed\n"));
					continue;
				}

				if (!m_pRTSPClientAPI->GetParam("AudioCodec", m_szAudioCodec, sizeof(m_szAudioCodec))) {
					UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("GetParam(AudioCodec) failed\n"));
					continue;
				}

				if (!m_pRTSPClientAPI->GetParam("SampleRate", &m_uSampleRate, sizeof(m_uSampleRate))) {
					UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("GetParam(SampleRate) failed\n"));
					continue;
				}

				if (!m_pRTSPClientAPI->GetParam("SampleBits", &m_uSampleBits, sizeof(m_uSampleBits))) {
					UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("GetParam(SampleBits) failed\n"));
					continue;
				}
			}
		}
	}

	return 0;
}

UINT WINAPI CRTSPSource::ThreadProcMediaDataWrapper(LPVOID pParam)
{
	struct THREAD_PARAM_SET *pParamSet = (struct THREAD_PARAM_SET*)pParam;
	return static_cast<CRTSPSource*>(pParamSet->pParam2)->ThreadProcMediaData(pParam);
}

UINT WINAPI CRTSPSource::ThreadProcMediaData(LPVOID pParam)
{
	struct THREAD_PARAM_SET *pParamSet = (struct THREAD_PARAM_SET*)pParam;
	struct MEDIA_INFO tMediaInfo;
	BOOL bRun = TRUE;
	HANDLE hEvents[4] = { ((CUserThread*)pParamSet->pParam1)->GetKillHandle(),
		m_pRTSPClientAPI->GetEvent(RTSP_MT_VIDEO),
		m_pRTSPClientAPI->GetEvent(RTSP_MT_AUDIO),
		m_pRTSPClientAPI->GetEvent(RTSP_MT_META)
	};
	//sean. LONGLONG timeCurrent = GetTickCount64();
	LONGLONG timeCurrent = (ULONGLONG) GetTickCount();


	HINTERNET	hInet_nvc = NULL;
	HRESULT	hRet = NO_ERROR;

	HINTERNET   hHttp_nvc = NULL;
	HINTERNET	hRequest = NULL;
	LPTSTR	apszMIME[] = { _T("text/*"), NULL };

	// 4. prepare
	DWORD	dwTotalReadedBytes1 = 0;
	DWORD	dwTotalReadedBytes2 = 0;
	DWORD	dwTotalReadedBytes3 = 0;
	DWORD	dwTotalQueryLength = 0;
	DWORD	dwQueryLength;
	DWORD	dwReadedBytes;
	
	BYTE* pcbSyntax1;
	BYTE* pcbSyntax2;
	BYTE* pcbSyntax3;
	BYTE*   lpcbResponse;

	#define	_MAXLEN_SYNTAXBUFF_CMD		(1024)
	pcbSyntax1 = (PBYTE)new TCHAR[_MAXLEN_SYNTAXBUFF_CMD];
	pcbSyntax2 = (PBYTE)new TCHAR[_MAXLEN_SYNTAXBUFF_CMD];
	pcbSyntax3 = (PBYTE)new TCHAR[_MAXLEN_SYNTAXBUFF_CMD];



	while (bRun) {
		UINT uRs = WaitForMultipleObjects(4, hEvents, FALSE, 1000);
		
		switch (uRs) {
		case WAIT_OBJECT_0:
			bRun = FALSE;
			break;
		case WAIT_OBJECT_0+1:
			tMediaInfo.uMediaType = RTSP_MT_VIDEO;
			//sean. m_timeLastDataRecieved = GetTickCount64();
			m_timeLastDataRecieved = (ULONGLONG)GetTickCount();
			//TRACE("Video!");
			break;
		case WAIT_OBJECT_0+2:
			tMediaInfo.uMediaType = RTSP_MT_AUDIO;
			break;
		case WAIT_OBJECT_0+3:
			tMediaInfo.uMediaType = RTSP_MT_META;
			break;
		case WAIT_TIMEOUT:
			//sean. timeCurrent = GetTickCount64();
			timeCurrent = (ULONGLONG)GetTickCount();
			if (timeCurrent - m_timeLastDataRecieved > 5000) {
				//sean. m_timeLastDataRecieved = GetTickCount64();
				m_timeLastDataRecieved = (ULONGLONG)GetTickCount();
				if (m_bConnected) {

					TRACE("no video time >= 5s");
					LogNoVideoForPing();
//////////////////////////////////////////
	// sean for cgi start.

	// 초기화!
	if(hInet_nvc) { InternetCloseHandle(hInet_nvc); hInet_nvc = NULL;}
	if(hHttp_nvc) {InternetCloseHandle(hHttp_nvc); hHttp_nvc = NULL;}
	dwTotalReadedBytes1 = 0;
	dwTotalQueryLength = 0;

	for(int chk = 0; ; chk++)
	{
		if (szWEBURL[chk] == NULL) {szWEBURL_wchar[chk] = NULL; break;}
		szWEBURL_wchar[chk] = szWEBURL[chk];		
	}
	TRACE(_T("WEBURL : %s"),  szWEBURL_wchar);

	// ******** 1. internet open
	hInet_nvc = NULL;
	hRet = NO_ERROR;
	do {
		TRACE("[**CGI**] Try Internet Open!");
		hInet_nvc = InternetOpen( _T("CLIENT_NVC"), INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, 0 );
		
		if( NULL == hInet_nvc ) { hRet = -1; break; }
	}while(0);
	
	if( NO_ERROR == hRet ) {;}
	else 
	{
		if(hInet_nvc) { InternetCloseHandle(hInet_nvc); }
		TRACE("[**CGI**] Internet Close!");
		hInet_nvc = NULL;
	}
	
	// ******** 2. internet connection
	hHttp_nvc = NULL;
	do {
		TRACE("[**CGI**] Try InternetConnect!");
		hHttp_nvc = InternetConnect(hInet_nvc, szWEBURL_wchar, 80, _T("root"), _T("pass"), INTERNET_SERVICE_HTTP, 0, NULL); //_T("192.168.73.225"), 80, _T("root"), _T("pass"), INTERNET_SERVICE_HTTP, 0, NULL);
		if( NULL == hHttp_nvc ) {
			hRet = -2;
			break;
		}
	}while(0);

	if( NO_ERROR == hRet ) {
		;
	}
	else {
		if(hHttp_nvc) {InternetCloseHandle(hHttp_nvc);}
		TRACE("[**CGI**] Internet Close!");
		hHttp_nvc = NULL;
	}

	// ******** 3. Http Open request
	hRequest = NULL;
	hRequest = HttpOpenRequest( hHttp_nvc,
								_T("POST"),				//org:"POST", [modify] if .fcgi then "GET"
								_T("/nvc-cgi/admin/debug.cgi"),
								NULL,
								NULL,
								(LPCTSTR*)apszMIME,
								0,
								NULL );

	if( NULL == hRequest ) {
		TRACE("[**CGI**] Http Open Request fail!");
	}
	else
	{
		TRACE("[**CGI**] Http Open Request succ!");
	}

	// 4. 나머지
	// ******** 4. send request
	//hRet = HttpSendRequest( hRequest, L"Content-Type: application/x-www-form-urlencoded", lstrlen(L"Content-Type: application/x-www-form-urlencoded"), L"action=query", lstrlen(L"action=query") );

	//**** vidcapd
	hRet = HttpSendRequest( hRequest, NULL, 0, (LPVOID)"action=update&group=debug&command=cat /var/run/vidcapd_status", lstrlen(L"action=update&group=debug&command=cat /var/run/vidcapd_status") );
	if(hRet == FALSE)
	{
		TRACE("[**CGI**] Http Send Request fail!");
	}
	else
	{
		TRACE("[**CGI**] Http Send Request succ!");

		memset(pcbSyntax1, 0, sizeof(pcbSyntax1));
		lpcbResponse = pcbSyntax1;
		do {

			TRACE("[**CGI**] Call InternetQueryDataAvailable()!");
			InternetQueryDataAvailable( hRequest, &dwQueryLength, 0, 0 );
			dwTotalQueryLength += dwQueryLength;

			if( 0 != dwQueryLength)
			{
				lpcbResponse = lpcbResponse + dwTotalReadedBytes1;
				TRACE("[**CGI**] Call InternetReadFile()!");
				InternetReadFile( hRequest, lpcbResponse, dwQueryLength, &dwReadedBytes );
				dwTotalReadedBytes1 += dwReadedBytes;
				if (0 == dwReadedBytes)
				{
					break;
				}

			}else
			{
				break;
			}
			
		
		}while(1);
		TRACE("[**CGI**] vidcapd Result \n%s ", pcbSyntax1);
	}

	//**** nvcencoder
	hRequest = NULL;
	hRequest = HttpOpenRequest( hHttp_nvc,
								_T("POST"),				//org:"POST", [modify] if .fcgi then "GET"
								_T("/nvc-cgi/admin/debug.cgi"),
								NULL,
								NULL,
								(LPCTSTR*)apszMIME,
								0,
								NULL );

	if( NULL == hRequest ) {
		TRACE("[**CGI**] Http Open Request fail!");
	}
	else
	{
		TRACE("[**CGI**] Http Open Request succ!");
	}


	// 초기화 start
	dwTotalReadedBytes2 = 0;
	dwTotalQueryLength = 0;
	// 초기화 end

	hRet = HttpSendRequest( hRequest, NULL, 0, (LPVOID)"action=update&group=debug&command=cat /var/run/nvcencoderd_status", lstrlen(L"action=update&group=debug&command=cat /var/run/nvcencoderd_status") );
	if(hRet == FALSE)
	{
		TRACE("[**CGI**] Http Send Request fail!");
	}
	else
	{
		TRACE("[**CGI**] Http Send Request succ!");

		memset(pcbSyntax2, 0, sizeof(pcbSyntax2));
		lpcbResponse = pcbSyntax2;
		do {

			TRACE("[**CGI**] Call InternetQueryDataAvailable()!");
			InternetQueryDataAvailable( hRequest, &dwQueryLength, 0, 0 );
			dwTotalQueryLength += dwQueryLength;

			if( 0 != dwQueryLength)
			{
				lpcbResponse = lpcbResponse + dwTotalReadedBytes2;
				TRACE("[**CGI**] Call InternetReadFile()!");
				InternetReadFile( hRequest, lpcbResponse, dwQueryLength, &dwReadedBytes );
				dwTotalReadedBytes2 += dwReadedBytes;
				if (0 == dwReadedBytes)
				{
					break;
				}

			}else
			{
				break;
			}
		}while(1);
		TRACE("[**CGI**] nvcencoder Result \n%s ", pcbSyntax2);
	}


	//**** rtspstreamer
	hRequest = NULL;
	hRequest = HttpOpenRequest( hHttp_nvc,
								_T("POST"),				//org:"POST", [modify] if .fcgi then "GET"
								_T("/nvc-cgi/admin/debug.cgi"),
								NULL,
								NULL,
								(LPCTSTR*)apszMIME,
								0,
								NULL );

	if( NULL == hRequest ) {
		TRACE("[**CGI**] Http Open Request fail!");
	}
	else
	{
		TRACE("[**CGI**] Http Open Request succ!");
	}


	// 초기화 start
	dwTotalReadedBytes3 = 0;
	dwTotalQueryLength = 0;
	// 초기화 end

	hRet = HttpSendRequest( hRequest, NULL, 0, (LPVOID)"action=update&group=debug&command=cat /var/run/rtspstreamerd_status", lstrlen(L"action=update&group=debug&command=cat /var/run/rtspstreamerd_status") );
	if(hRet == FALSE)
	{
		TRACE("[**CGI**] Http Send Request fail!");
	}
	else
	{
		TRACE("[**CGI**] Http Send Request succ!");

		memset(pcbSyntax3, 0, sizeof(pcbSyntax3));
		lpcbResponse = pcbSyntax3;
		do {

			TRACE("[**CGI**] Call InternetQueryDataAvailable()!");
			InternetQueryDataAvailable( hRequest, &dwQueryLength, 0, 0 );
			dwTotalQueryLength += dwQueryLength;

			if( 0 != dwQueryLength)
			{
				lpcbResponse = lpcbResponse + dwTotalReadedBytes3;
				TRACE("[**CGI**] Call InternetReadFile()!");
				InternetReadFile( hRequest, lpcbResponse, dwQueryLength, &dwReadedBytes );
				dwTotalReadedBytes3 += dwReadedBytes;
				if (0 == dwReadedBytes)
				{
					break;
				}

			}else
			{
				break;
			}
		}while(1);
		TRACE("[**CGI**] rtspstreamer Result \n%s ", pcbSyntax3);
	}


	// sean for cgi end.
//////////////////////////////////////////
					LogNoVideo(pcbSyntax1, pcbSyntax2, pcbSyntax3, dwTotalReadedBytes1, dwTotalReadedBytes2, dwTotalReadedBytes3);
				}
			}
			break;
		}

		if (RUN == m_Status) {
			while (m_pRTSPClientAPI->GetMediaData(tMediaInfo.uMediaType, &tMediaInfo.pBuffer, &tMediaInfo.uFrameSize, &tMediaInfo.timeStamp)) {
				//Check Kill Event 
				if(WAIT_OBJECT_0 == WaitForSingleObject(hEvents[0], 0)){
					UMFTrace(UMF_TRACE_LEVEL_VERBOSE, _T("End of ThreadProcMediaData thread\n"));
					break;
				}

				if (!m_mmQue.Push(&tMediaInfo)) {
					m_mmQue.RemoveAll();
					m_mmQue.Push(&tMediaInfo);
				}

				SetEvent(m_hEvent);
			}
		}			
	}

	return 0;
}

BOOL CRTSPSource::MakeThread()
{
	if (!m_checkThread.StartThread(ThreadProcMediaDataWrapper, this, NULL, NULL)) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Media thread creation failed\n"));
		return FALSE;
	}

	if (!m_mediaThread.StartThread(ThreadCheckConnectionWrapper, this, NULL, NULL)) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Checking connection thread creation failed\n"));
		return FALSE;
	}

	return TRUE;
}

SRCTYPE	CRTSPSource::GetSourceType()
{
	return UMF_ST_RAW_RTSP;
}

eUMF_STATUS	CRTSPSource::Open(UINT SourceId, IBaseUMFStorage *pXmlStorage)
{
	if (INIT != m_Status) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("CRTSPSource : Already Opened \n"));
		Close();
	}
	
	m_pRTSPClientAPI = NULL;

	memset(&m_rtspInfoSource, 0 , sizeof(m_rtspInfoSource));

	if (!pXmlStorage->GetXML()) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("CRtspSource : Cannot get RtspSource info from Source XML element 0\n"));
		return UMF_ERR_XML_PARSING;		
	}
	
	if (!GetRtspSourceInfo(pXmlStorage->GetXML(), &m_rtspInfoSource)) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("CRtspSource : Cannot get RtspSource info from Source XML element 1\n"));
		return UMF_ERR_XML_PARSING;
	}
	m_bForceContinue = m_rtspInfoSource.bForceContinue;

	if (m_rtspInfoSource.bCreateConnectionLog) {
		eUMF_STATUS status = LogCreate();
		if (status < 0) {
			return status;
		}
	}

	m_hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	BOOL bRTSPInited = RtspInit();
	if (!bRTSPInited) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("[%s] Failed to init RTSPClientAPI\n"), m_rtspInfoSource.szURL);
		if(TRUE != m_bForceContinue) return UMF_ERR_INIT;
	}

//	Sleep(3000);

	if( bRTSPInited ) {
		const int nMaxTry = 4;     // if fps is 1, it can exceed 1 seconds
		const int nInterval = 500; // 0.5 sec
		for( int nTry = 0; (0 == m_uWidth) || (0 == m_uHeight) || (0 == strlen(m_szVideoCodec)); nTry++ ) {
			if (!m_pRTSPClientAPI->GetParam("VideoCodec", m_szVideoCodec, sizeof(m_szVideoCodec))) {
				UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("GetParam(VideoCodec) failed\n"));
				//return UMF_ERR_NOT_INITIALIZED;
			}

			if (!m_pRTSPClientAPI->GetParam("Width", &m_uWidth, sizeof(m_uWidth))) {
				UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("GetParam(Width) failed\n"));
				//return UMF_ERR_NOT_INITIALIZED;
			}

			if (!m_pRTSPClientAPI->GetParam("Height", &m_uHeight, sizeof(m_uHeight))) {
				UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("GetParam(Height) failed\n"));
				//return UMF_ERR_NOT_INITIALIZED;
			}
			if(nMaxTry <= nTry) {
				UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("RTSP source failed to retreive Width or Height for %d secs\n"), (nMaxTry*nInterval)/1000);
				if(m_bForceContinue) break;
				else return UMF_ERR_NOT_INITIALIZED;
			}
			Sleep(nInterval);
		}

		if (!m_pRTSPClientAPI->GetParam("AudioCodec", m_szAudioCodec, sizeof(m_szAudioCodec))) {
			UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("GetParam(AudioCodec) failed\n"));
			//return UMF_ERR_NOT_INITIALIZED;
		}

		if (!m_pRTSPClientAPI->GetParam("SampleRate", &m_uSampleRate, sizeof(m_uSampleRate))) {
			UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("GetParam(SampleRate) failed\n"));
			//return UMF_ERR_NOT_INITIALIZED;
		}

		if (!m_pRTSPClientAPI->GetParam("SampleBits", &m_uSampleBits, sizeof(m_uSampleBits))) {
			UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("GetParam(SampleBits) failed\n"));
			//return UMF_ERR_NOT_INITIALIZED;
		}

		if (!MakeThread()) {
			UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("CRtspSource : Can not Make Thread\n"));
			if(TRUE != m_bForceContinue) return UMF_ERR_INIT;
		}
	}

	m_Status = IBaseUMFSource::OPEN;
	m_SourceID = SourceId;
	m_pXmlStorage = pXmlStorage;

	return UMF_OK;
}

void CRTSPSource::Close()
{
	DWORD lpExitCode = STILL_ACTIVE;

	m_checkThread.StopThread(TRUE, 3000, TRUE);
	m_mediaThread.StopThread(TRUE, 3000, TRUE);

	if (m_hEvent) CloseHandle(m_hEvent);

	if (m_pRTSPClientAPI) {
		m_pRTSPClientAPI->Disconnect();
		m_pRTSPClientAPI->Release();
		m_pRTSPClientAPI->DestroySelf();
		m_pRTSPClientAPI = NULL;
		LogDisconnected();
	}

	m_Status = INIT;
}

eUMF_STATUS	CRTSPSource::Start()
{
	if (OPEN != m_Status){
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("CRTSPSource : Start Fail Status [%d]\n"), m_Status);
		return UMF_ERR_NOT_READY;
	}
	m_Status = RUN;

	return UMF_OK;
}

void CRTSPSource::Stop()
{
	if (RUN != m_Status) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("CRTSPSource : Stop Fail Status [%d]\n"), m_Status);
		return;
	}
	m_Status = OPEN;
}

PXML CRTSPSource::GetSourceInfo()
{
	IXMLDOMElementPtr	SourceXML;
	IXMLDOMNode*		pSourceXML = (IXMLDOMNode*)m_pXmlStorage->GetXML();
	TCHAR szVideoCodec[8], szAudioCodec[8];

	MultiByteToWideChar(CP_ACP, 0, m_szVideoCodec, -1, szVideoCodec, sizeof(szVideoCodec));
	MultiByteToWideChar(CP_ACP, 0, m_szAudioCodec, -1, szAudioCodec, sizeof(szAudioCodec));

	try{
		SetAttrValueString(pSourceXML, _T("VideoCodec"), szVideoCodec);
		SetAttrValueNum(pSourceXML, _T("ImageSizeW"), m_uWidth);
		SetAttrValueNum(pSourceXML, _T("ImageSizeH"), m_uHeight);
		SetAttrValueString(pSourceXML, _T("AudioCodec"), szAudioCodec);
		SetAttrValueNum(pSourceXML, _T("AudioSampleRate"), m_uSampleRate);
		SetAttrValueNum(pSourceXML, _T("AudioSampleBits"), m_uSampleBits);
//		SetAttrValueNum(pSourceXML, _T("FrameRate"), 30);
	}catch(...)
	{
		return NULL;
	}

	m_pSrcInfoXMLDoc = pSourceXML;

	return m_pSrcInfoXMLDoc;
}


eUMF_STATUS CRTSPSource::GetMMData(SRCTYPE* pType, BYTE **pMMData, UINT* pMMDataLen, FILETIME *timestamp)
{
	struct MEDIA_INFO tMediaInfo;
	CustomMetaHdr *pCustomHdr;
	CHAR szVideoCodec[8], szAudioCodec[8];
	eUMF_STATUS eStatus = UMF_OK;

	if (RUN != m_Status) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("CRTSPSource : GetRawImage not Status [%d]\n"), m_Status);
		return UMF_ERR_NOT_READY;
	}

	if (!m_mmQue.Pop(&tMediaInfo)) {
		return UMF_ERR_NOT_ENOUGH_DATA;
	}

	*pType = UMF_ST_RAW_RTSP;
	*pMMData = tMediaInfo.pBuffer;
	*pMMDataLen = tMediaInfo.uFrameSize;
	TimevalToFileTime(&tMediaInfo.timeStamp, timestamp);

	switch (tMediaInfo.uMediaType) {
	case RTSP_MT_VIDEO:
		*pType |= UMF_ST_VID_RTSP;
		if (!m_pRTSPClientAPI->GetParam("VideoCodec", szVideoCodec, sizeof(szVideoCodec))) {
			UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("GetParam(VideoCodec) failed\n"));
			return UMF_ERR_NOT_INITIALIZED;
		}

		if (strcmp(szVideoCodec, m_szVideoCodec) != 0) {
			eStatus = UMF_CHNAGE_SOURCE;
		}
		break;
	case RTSP_MT_AUDIO:
		*pType |= UMF_ST_AUD_RTSP;
		if (!m_pRTSPClientAPI->GetParam("AudioCodec", szAudioCodec, sizeof(szAudioCodec))) {
			UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("GetParam(AudioCodec) failed\n"));
			return UMF_ERR_NOT_INITIALIZED;
		}

		if (strcmp(szAudioCodec, m_szAudioCodec) != 0) {
			eStatus = UMF_CHNAGE_SOURCE;
		}
		break;
	case RTSP_MT_META:
		pCustomHdr = (CustomMetaHdr*)*pMMData;
		if (VCA_MODULE_ID == pCustomHdr->customid) {
			*pType |= UMF_ST_META_VCA;
		} else if (OFD_MODULE_ID == pCustomHdr->customid) {
			*pType |= UMF_ST_META_MD;
		} else {
			UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Unknown Meta Type\n"));
			return UMF_ERR_UNSUPPORTED;
		}
		break;
	default:
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Unknown Type\n"));
		return UMF_ERR_UNSUPPORTED;
	}

	if(m_bReconnectFlag == TRUE)
	{
		m_bReconnectFlag = FALSE;
		eStatus = UMF_CHNAGE_SOURCE;
	}
	return eStatus;
}

eUMF_STATUS	CRTSPSource::ReleaseMMData()
{
	return UMF_OK;
}

//CRTSPSourceFactory class

CRTSPSourceFactory::CRTSPSourceFactory()
{
	m_bSetup		= FALSE;
	m_hSourceSetDlg	= NULL;
	memset(&m_rtspInfoSource, 0, sizeof(m_rtspInfoSource));
}


IBaseUMFSource*	CRTSPSourceFactory::CreateUMFSource()
{
	if(!m_bSetup){
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("CRTSPSourceFactory not setup before \n"));
		return NULL;
	}
	return new CRTSPSource();
}

SRCTYPE	CRTSPSourceFactory::GetSourceType()
{
	return UMF_ST_MEDIA_RTSP | UMF_ST_RAW_RTSP;
}

eUMF_STATUS CRTSPSourceFactory::Open(UINT SrcFactoryId, IBaseUMFStorage *pXmlStorage)
{
	memset(&m_rtspInfoSource, 0, sizeof(m_rtspInfoSource));
	m_hSourceSetDlg	= NULL;
	m_SrcFactoryId	= SrcFactoryId;
	m_bSetup		= TRUE;

	return UMF_OK;
}


void	CRTSPSourceFactory::Close()
{
	if(m_hSourceSetDlg){
		DestroyWindow(m_hSourceSetDlg);
		m_hSourceSetDlg	= NULL;
	}
	m_bSetup		= FALSE;
}
	

BOOL	CALLBACK CRTSPSourceFactory::DialogProcWrap(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	CRTSPSourceFactory *pRTSPSourceFactory = (CRTSPSourceFactory *) (GetWindowLongPtr(hwndDlg, DWLP_USER));
	if(pRTSPSourceFactory) return pRTSPSourceFactory->OnWinMsg(uMsg, wParam, lParam);
	return FALSE;
}

extern HINSTANCE g_UMFInstance;
HWND	CRTSPSourceFactory::ShowConfigDlg(UINT SrcId, HWND hParentWnd, IBaseUMFStorage *pXmlStorage)
{
	if(!m_bSetup){
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("CRTSPSourceFactory did not Setup before \n"));
		return NULL;
	}


	if(IsWindow(m_hSourceSetDlg)) {
		ShowWindow(m_hSourceSetDlg, SW_SHOW);
		SetFocus(m_hSourceSetDlg);
		return m_hSourceSetDlg;
	}

	m_hSourceSetDlg = CreateDialog(g_UMFInstance, MAKEINTRESOURCE(IDD_DIALOG_FILESOURCE), hParentWnd, DialogProcWrap);
	if(!m_hSourceSetDlg){
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("CRTSPSourceFactory : Can not Create Dialog\n"));
		return NULL;
	}
	SetWindowLongPtr(m_hSourceSetDlg, DWLP_USER, (LONG)this);

	m_SourceID		= SrcId;
	m_pXmlStorage	= pXmlStorage;
	
	ShowWindow(m_hSourceSetDlg, SW_SHOW);

	return m_hSourceSetDlg;
}


eUMF_STATUS	CRTSPSourceFactory::ApplyCurSetting(IBaseUMFStorage *pXmlStorage)
{
// 	if(!m_bSetup || !m_hSourceSetDlg){
// 		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("CRTSPSource did not open before \n"));
// 		return FALSE;
// 	}
// 
// 	GetDlgItemText(m_hSourceSetDlg, IDC_ED_RAWFILEPATH, m_FileInfoSource.szFilePath, MAX_PATH);
// 	m_FileInfoSource.uIFrameRate100 = GetDlgItemInt(m_hSourceSetDlg, IDC_EDIT_FPS, NULL ,FALSE);
// 
// 	return SetFileSourceInfo(m_pXmlStorage->GetXML(), &m_FileInfoSource);
	return UMF_OK;
}


BOOL	CRTSPSourceFactory::OnWinMsg(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	if(WM_COMMAND == uMsg){
		if (LOWORD(wParam) == IDC_BTN_FILEOPEN){
			TCHAR szFilePath[MAX_PATH] = {0,};
			OPENFILENAME OFN;
			memset(&OFN,0,sizeof(OPENFILENAME));

			OFN.lStructSize = sizeof(OPENFILENAME);
			OFN.hwndOwner	= m_hSourceSetDlg;
			OFN.lpstrFilter	= _T("Raw Video File(*.raw)\0*.raw\0");
			OFN.lpstrFile	= szFilePath;
			OFN.nMaxFile	= MAX_PATH;

			if (0!=GetOpenFileName(&OFN)){
				SetDlgItemText(m_hSourceSetDlg, IDC_ED_RAWFILEPATH, szFilePath);
			}
			return TRUE;
		}
		switch(LOWORD(wParam)) {
		case IDC_BTN_APPLY: 
//			ApplyCurSetting();
			MessageBox(m_hSourceSetDlg, _T("Configuration Saved"), _T("Notice"), MB_OK);
			return TRUE;
		case IDC_BTN_CLOSE:
		case IDCANCEL:
			DestroyWindow(m_hSourceSetDlg);
			return TRUE;
		}
	}
	return FALSE;
}


VOID SystemResReturn(CHAR* cmd, CHAR* szretrun, INT len)//sean
{
	FILE	*fp = NULL;
	size_t	readSize = 0;

	fp = popen((CHAR*)cmd, "r");
	if(!fp){;}

	readSize = fread( (void*)szretrun, sizeof(CHAR), len-1, fp );
	if( readSize == 0 )
	{
		pclose(fp);
		szretrun = "[SystemResReturn]_ERROR";
	}
	pclose(fp);
	szretrun[readSize]=0;

}

eUMF_STATUS CRTSPSource::LogNoVideoForPing()
{
	CHAR szcmd[MAX_PATH];
	CHAR szcmdResult[1024*20];

	memset(szcmdResult, 0, sizeof(szcmdResult));
	sprintf_s(szcmd, MAX_PATH-1, "ping -n 20 %s", szWEBURL);
	SystemResReturn(szcmd, szcmdResult, 1024*20);

	TRACE("*** Ping Result : %s\n", szcmdResult);

	m_hConnectionLog = CreateFile(m_filenameConnectionLog, GENERIC_WRITE, FILE_SHARE_READ, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if (m_hConnectionLog == INVALID_HANDLE_VALUE) {
		return UMF_ERR_FAILED;
	}

	SYSTEMTIME localTime;
	GetLocalTime(&localTime);

	if (SetFilePointer(m_hConnectionLog, 0, NULL, FILE_END) == INVALID_SET_FILE_POINTER) {
		return UMF_ERR_FAILED;
	}

	char msg[MAX_PATH];
	DWORD written = 0;
	sprintf_s(msg, MAX_PATH-1, "%04d/%02d/%02d %02d:%02d:%02d no video for 5 seconds\n", localTime.wYear, localTime.wMonth, localTime.wDay, localTime.wHour, localTime.wMinute, localTime.wSecond);
	WriteFile(m_hConnectionLog, msg, strlen(msg), &written, NULL);


	// ping
	WriteFile(m_hConnectionLog, "\n[Ping Test] start\n", lstrlen(L"\n[Ping Test] start\n"), &written, NULL);


	WriteFile(m_hConnectionLog, szcmdResult, strlen(szcmdResult), &written, NULL);

	CloseHandle(m_hConnectionLog);

	return UMF_OK;
}