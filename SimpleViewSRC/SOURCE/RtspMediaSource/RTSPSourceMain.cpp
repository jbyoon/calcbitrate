// FileVideoSource.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "RTSPSource.h"

//#include <crtdbg.h>

#ifdef _MANAGED
#pragma managed(push, off)
#endif

#ifndef _LIB
HINSTANCE g_UMFInstance;
BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
//	_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
//	_CrtSetBreakAlloc(663);
	if(DLL_PROCESS_ATTACH == ul_reason_for_call) g_UMFInstance = hModule;
    return TRUE;
}

extern "C" IBaseUMFSourceFactory* WINAPI CreateSourceFactory()
{
	return (IBaseUMFSourceFactory *) new CRTSPSourceFactory();
}

extern "C" SRCTYPE	WINAPI GetSourceType()
{
	return UMF_ST_MEDIA_RTSP | UMF_ST_RAW_RTSP;
}

#else

extern "C" IBaseUMFSourceFactory* CreateRTSPSourceFactory()
{
	return (IBaseUMFSourceFactory *) new CRTSPSourceFactory();
}


#endif


#ifdef _MANAGED
#pragma managed(pop)
#endif

