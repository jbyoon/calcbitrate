#ifndef __CAP_QUEUE_LOCK_H__
#define __CAP_QUEUE_LOCK_H__

#include "PUBLIB2\CapQueue.h"

template<class T,int MaxQueueSize>
class CCapQueueLock
{
public:
	ULONG GetCount()
	{
		return m_CapQueue.GetCount();
	}

	BOOL Push(T* item)
	{
		EnterCriticalSection(&m_CS);
		BOOL bRs = m_CapQueue.Push(item);
		LeaveCriticalSection(&m_CS);

		return bRs;
	}

	BOOL Pop(T* item)
	{
		EnterCriticalSection(&m_CS);
		BOOL bRs = m_CapQueue.Pop(item);
		LeaveCriticalSection(&m_CS);
		
		return bRs;
	}

	BOOL PopPtr(T** item)
	{
		EnterCriticalSection(&m_CS);
		BOOL bRs = m_CapQueue.PopPtr(item);
		LeaveCriticalSection(&m_CS);

		return bRs;
	}

	BOOL Peek(T* item)
	{
		return m_CapQueue.Peek(item);
	}

	BOOL PeekSpecific(T* item, ULONG Num)
	{
		return m_CapQueue.PeekSpecific(item, Num);
	}

	void RemoveAll()
	{
		EnterCriticalSection(&m_CS);
		m_CapQueue.RemoveAll();
		LeaveCriticalSection(&m_CS);
	}

	void SaveIndex()
	{
		EnterCriticalSection(&m_CS);
		m_CapQueue.SaveIndex();
		LeaveCriticalSection(&m_CS);
	}

	void RestoreIndex()
	{
		EnterCriticalSection(&m_CS);
		m_CapQueue.RestoreIndex();
		LeaveCriticalSection(&m_CS);
	}

	BOOL HasIt()	{return m_CapQueue.HasIt();}
	BOOL IsEmpty()	{return m_CapQueue.IsEmpty();}
	BOOL IsFull()	{return m_CapQueue.IsFull();}

	void ShowInternal()
	{
		m_CapQueue.ShowInternal();
	}

	CCapQueueLock() { 
		InitializeCriticalSection(&m_CS); 
		m_CapQueue.RemoveAll();	
	};
	virtual ~CCapQueueLock() {
		DeleteCriticalSection(&m_CS);
	};

private:
	CCapQueue<T, MaxQueueSize>	m_CapQueue;
	CRITICAL_SECTION m_CS;
};

#endif
