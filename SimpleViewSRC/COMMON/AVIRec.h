// AVIRec.h: interface for the CAVIRec class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AVIREC_H__5A2C5CB5_C19C_40B7_B260_AB662102E28F__INCLUDED_)
#define AFX_AVIREC_H__5A2C5CB5_C19C_40B7_B260_AB662102E28F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <Vfw.h>


class CAVIRec  
{
public:
	CAVIRec();
	virtual ~CAVIRec();

	BOOL Open(TCHAR* lpFilename, BITMAPINFOHEADER* pbih, DWORD fccHandler, WAVEFORMATEX *pwfx, ULONG uRate);
	void Close();
	BOOL WriteVideo(DWORD frameNo, LPVOID buf, LONG sizeImage, BOOL bKeyFrame);
	BOOL WriteAudio(DWORD frameNo, LPVOID buf, LONG size);
	BOOL WriteCaption(DWORD frameNo, LPVOID buf, LONG sizeCaption, BOOL bKeyFrame);
	BOOL WriteAudio(DWORD frameNo, LPVOID buf, LONG sizePacket, BOOL bKeyFrame);
	BOOL IsOpened() {return m_bOpened;}
	
private:
	void DiagMsg(HRESULT errCode);

	PAVIFILE			m_aviFile;
	PAVISTREAM			m_strmVideo;
	PAVISTREAM			m_strmAudio;
	PAVISTREAM			m_strmCaption;

	BOOL				m_bOpened;
};


//make folder base day and time 
class CAVIFolderRec
{
public:
	CAVIFolderRec();
	
	BOOL Open(TCHAR* szFolder, BITMAPINFOHEADER* pbih, DWORD fccHandler, WAVEFORMATEX *pwfx, ULONG uRate, TCHAR* szAppendName, ULONG ChunkTime = 600);
	void Close(){
		m_AVIRec.Close();
		m_bOpened	= FALSE;
	}

	void SetAppendName(TCHAR* szAppendName);

	BOOL WriteVideo(DWORD frameNo, LPVOID buf, LONG sizeImage, BOOL bKeyFrame, BOOL* bReopen);
	BOOL WriteAudio(DWORD frameNo, LPVOID buf, LONG size){
		return m_AVIRec.WriteAudio(frameNo, buf, size);
	}
	BOOL WriteCaption(DWORD frameNo, LPVOID buf, LONG sizeCaption, BOOL bKeyFrame){
		return m_AVIRec.WriteCaption(frameNo, buf, sizeCaption, bKeyFrame);
	}
	BOOL WriteAudio(DWORD frameNo, LPVOID buf, LONG sizePacket, BOOL bKeyFrame){
		return m_AVIRec.WriteAudio(frameNo, buf, sizePacket, bKeyFrame);
	}
	BOOL IsOpened() {return m_bOpened;}

private:

	CAVIRec			m_AVIRec;
	BOOL			m_bOpened;
	TCHAR			m_szFolder[MAX_PATH];
	TCHAR			m_szAppendName[MAX_PATH];

	BOOL			CreateAVIFolder(SYSTEMTIME* pSystemTime, TCHAR *szFolderPath);
	BOOL			GetCurFileName(SYSTEMTIME* pSystemTime, TCHAR *szFilePath);

	SYSTEMTIME		m_CurSystemTime;
	TCHAR			m_szCurFolder[MAX_PATH];
	
	ULONG			m_FileChunkTime;
	ULONG			m_StartTick;

	BITMAPINFOHEADER m_BitmpaInfoHeader;
	DWORD			m_fccHandler;
	WAVEFORMATEX	m_WaveFormat;
	ULONG			m_uRate;
};

#endif // !defined(AFX_AVIREC_H__5A2C5CB5_C19C_40B7_B260_AB662102E28F__INCLUDED_)
