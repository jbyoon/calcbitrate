#define MAXCMD_STR_LEN 32

#ifdef CMD_TYPE_NAME
	#define DEFINE_UMF_CMD(CMD, index)  CMD = index,
	typedef enum {
		#include CMD_HEADER
		CMD_TYPE_END
	}CMD_TYPE_NAME;
#endif


#ifdef CMD_STRING_NAME
#undef DEFINE_UMF_CMD
	#define DEFINE_UMF_CMD(CMD, index) L#CMD,
	static wchar_t	CMD_STRING_NAME[CMD_TYPE_END][MAXCMD_STR_LEN]={
		#include CMD_HEADER
	};
#endif


#ifdef CMD_FUNCTION_NAME
inline CMD_TYPE_NAME CMD_FUNCTION_NAME(LPCTSTR szCmd)
{
	for(int i = 0; i < CMD_TYPE_END; i++)
		if(!_wcsicmp(CMD_STRING_NAME[i], szCmd))
			return (CMD_TYPE_NAME)i;
	return CMD_TYPE_END;
}
#endif
