// This is a part of the UDP Multimedia Framework.
// Copyright (C) UDP Technology
// All rights reserved.
//
// [How To Use]
// 1. Include UMFTrace.h to source code or Stdafx.h (EX. #include "CommonInterface/UMFTrace.h" )
// 2. Set trace option at the beginning of whole program.
//		EX. UMFSetTraceOption(NULL, UMF_TRACE_DISPLAY_ALL);  <-- at the beginning of main() function or InitInstance() of MFC
// 3. If you will use UMF_TRACE_WRITE_LOG flag, please add UMFSetDebugFileName() function.
// 4. Select UMFTrace series according to the number of parameter to pass. 
//		EX. case of 0 parameter : UMFTrace0(UMF_TRACE_LEVEL_ERROR,_T("Failed to call function"));
//		EX. case of 2 parameter : UMFTrace2(UMF_TRACE_LEVEL_LOG,_T("CurTime=%2d %2d"), nHour, nMin);
// Please visit PC Card Team in UDP R&D lab for further information.

#ifndef __UMF_TRACE_H__
#define __UMF_TRACE_H__

//#define UMF_TRACE_DISABLE		// uncomment or add to project setting(C++/Preprocessor) to disable UMFTrace

typedef enum{
	UMF_TRACE_LEVEL_CRITICAL 		= 0,	//ASSERT
	UMF_TRACE_LEVEL_ERROR 			= 1,	//MessageBox 
	UMF_TRACE_LEVEL_WARNING 		= 2,	
	UMF_TRACE_LEVEL_LOG 			= 4,		
}eUMF_TRACE_LEVEL;


//TRACE Display option.
enum{
	UMF_TRACE_DISPLAY_ERROR 		= 0x00000001,	
	UMF_TRACE_DISPLAY_WARNING 		= 0x00000002,	
	UMF_TRACE_DISPLAY_LOG			= 0x00000004,
	
	UMF_TRACE_DISPLAY_FILENAME 		= 0x00000010,	
	UMF_TRACE_DISPLAY_LINE 			= 0x00000020,
	UMF_TRACE_DISPLAY_FUNC 			= 0x00000040,

	UMF_TRACE_WRITE_LOG				= 0x00000100
};

void				UMFSetTraceLevel(eUMF_TRACE_LEVEL eTraceLevel);
eUMF_TRACE_LEVEL	UMFGetTraceLevel();

void	UMFSetTraceOption(TCHAR* szModuleName, ULONG uTraceDisplayOption);
ULONG	UMFGetTraceOption();

void	UMFSetDebugFileName(TCHAR *file, BOOL truncate);

void	UMFTrace(eUMF_TRACE_LEVEL Level, LPCTSTR lpszFormat, ...);
void	UMFTraceEX(eUMF_TRACE_LEVEL Level, TCHAR* szFile, int nLine, TCHAR* szFunc, LPCTSTR lpszFormat, ...);


#ifdef _UNICODE 
	#define WIDEN(x)		L ## x 
	#define WIDEN2(x)       WIDEN(x) 
	#define __WFILE__		WIDEN2(__FILE__)
	#define __WFUNC__		WIDEN2(__FUNCTION__) 
#else 
	#define __WFILE__		__FILE__ 
	#define __WFUNCTION__	__FUNCTION__ 
#endif

#ifndef UMF_TRACE_DISABLE

	#define UMFTrace0(level, format) \
		UMFTraceEX(level, __WFILE__, __LINE__, __WFUNC__, _T("%s"), format)

	#define UMFTrace1(level, format, a1) \
		UMFTraceEX(level, __WFILE__, __LINE__, __WFUNC__, format, a1)

	#define UMFTrace2(level, format, a1, a2) \
		UMFTraceEX(level, __WFILE__, __LINE__, __WFUNC__, format, a1, a2)

	#define UMFTrace3(level, format, a1, a2, a3) \
		UMFTraceEX(level, __WFILE__, __LINE__, __WFUNC__, format, a1, a2, a3)

	#define UMFTrace4(level, format, a1, a2, a3, a4) \
		UMFTraceEX(level, __WFILE__, __LINE__, __WFUNC__, format, a1, a2, a3, a4)
#else

	#define UMFTrace0(level, format)
	#define UMFTrace1(level, format, a1)
	#define UMFTrace2(level, format, a1, a2)
	#define UMFTrace3(level, format, a1, a2, a3)
	#define UMFTrace4(level, format, a1, a2, a3, a4)

#endif

#endif // !__UMF_TRACE_H__
