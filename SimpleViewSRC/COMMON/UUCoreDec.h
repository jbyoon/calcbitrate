#ifndef __UU_CORE_DECODER_H__
#define __UU_CORE_DECODER_H__

#include <mmsystem.h>
#include "UUCore/UUCoreAPI.h"
#include "UMFTracer/UMFTracer.h"
#include <TCHAR.h>

class CUUCoreDec
{
public:
	CUUCoreDec();
	~CUUCoreDec();

	BOOL		Init();
	BOOL		GetVersion(ULONG Version[4]);
	BOOL		GetModulePath(TCHAR* szModulePath);

	BOOL		OpenVideoDecompressor(ULONG ulCodecType = UU_CODEC_X264);
	void		CloseVideoCodec();

	BOOL		OpenAudioDecompressor(ULONG ulCodecType = UU_CODEC_MP2);
	void		CloseAudioCodec();

	BOOL		SetVideoDecompressParam(ULONG ulWidth, ULONG ulHeight, ULONG ulColorSpace);
	BOOL		SetAudioDecompressParam(ULONG ulBitrate, ULONG ulSamplerate, ULONG ulChannels);

	BOOL		VideoDecompress(DecodedVideo* pDecodedData, int* pDecodedLength, 
								BYTE* pCompressedData, int nCompressedLength, BOOL	bDeinterlaced);

	BOOL		AudioDecompress(SHORT* pDecodedData, int* pDecodedDataBufLength, int* pDecodedLength,
								BYTE* pCompressedData, int nCompressedLength);

private:
	BOOL		m_bInit;

	HMODULE		m_hDLLUucore;
	IUucore*	m_pIUucore;

	HANDLE		m_hVideoCodec;
	DWORD		m_VideoCodeId;
	
	HANDLE		m_hAudioCodec;
	DWORD		m_AudioCodeId;

	IUucore*	CreateUUCoreInstance();

};

#endif	//#ifndef __UU_CORE_DECODER_H__
