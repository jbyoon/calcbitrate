#include "StdAfx.h"
#include "UMFDrawUtil.h"
#include "UMFUtil.h"


#ifdef _LIB	//static library
	#ifdef _DEBUG
		#pragma comment( lib, "../../LIB/UMFRender/sUMFRender_d" )
	#else
		#pragma comment( lib, "../../LIB/UMFRender/sUMFRender" )
	#endif
#else
	//#pragma comment( lib, "../../LIB/UMFRender/UMFRender" )
#endif


#ifndef TRACE
#include <atlbase.h>
#define TRACE AtlTrace
#endif


CUMFDrawUtil::CUMFDrawUtil()
{
	m_pUMFRender	= NULL;
	m_pUMFDrawUtil	= NULL;	
}

CUMFDrawUtil::~CUMFDrawUtil()
{
	if(m_pUMFDrawUtil){
		m_pUMFDrawUtil->DestroySelf();
		m_pUMFDrawUtil = NULL;
	}
}

BOOL CUMFDrawUtil::Init(IUMFRender *pUMFRender)
{
	if(!pUMFRender){
		UMFTrace(UMF_TRACE_LEVEL_CRITICAL, _T("CUMFDrawUtil::Init Failed pUMFRender not assigned \n"));
		return FALSE;
	}

	m_pUMFRender	= pUMFRender;
	
	HMODULE hModule = LoadUMFLibrary(_T("UMFRender.dll"));
	if(!hModule){
		UMFTrace(UMF_TRACE_LEVEL_CRITICAL, _T("Can not Load UMFRender.dll\n"));
		return FALSE;
	}

	IUMFDrawUtil* (FAR WINAPI*_CreateUMFDrawUtil)(DWORD type);
	FARPROC test_proc=GetProcAddress(hModule, "CreateUMFDrawUtil");
	if (test_proc) {
		*(FARPROC*)&_CreateUMFDrawUtil=test_proc;
	}
	m_pUMFDrawUtil	= _CreateUMFDrawUtil(UMF_DX9Render);

	if(!m_pUMFDrawUtil->Init(pUMFRender)){
		UMFTrace(UMF_TRACE_LEVEL_CRITICAL, _T("CUMFDrawUtil::Init Failed\n"));
		return FALSE;
	}
	return TRUE;
}


BOOL CUMFDrawUtil::ReInit(IUMFRender *pUMFRender)
{
	if(!m_pUMFDrawUtil){
		UMFTrace(UMF_TRACE_LEVEL_CRITICAL, _T("CUMFDrawUtil::ReInit Failed before Init\n"));
		return FALSE;
	}

	if(!m_pUMFDrawUtil->Init(pUMFRender)){
		UMFTrace(UMF_TRACE_LEVEL_CRITICAL, _T("CUMFDrawUtil::Init Failed\n"));
		return FALSE;
	}
	return TRUE;
}


void CUMFDrawUtil::SetCanvasRect(RECT CanvasRect)
{
	if(!m_pUMFDrawUtil) return ;
	m_pUMFDrawUtil->SetCanvasRect(CanvasRect);
}

SIZE CUMFDrawUtil::GetCanvasSize()
{
	if(!m_pUMFDrawUtil){
		SIZE temp={0,};
		return temp;
	}

	return m_pUMFDrawUtil->GetCanvasSize();
}

//Orient to Canvas x,y
BOOL CUMFDrawUtil::DrawRect(RECT rc, COLORREF Color, BYTE Alpha, eUMFBRUSHTYPE BrushType)
{
	if(!m_pUMFDrawUtil) return FALSE;

	POINT pts[4];
	
	//	Assemble the 4 boundary lines of a bounding box
	pts[0].x = rc.left;
	pts[0].y = rc.top;

	pts[1].x = rc.right;
	pts[1].y = rc.top;

	pts[2].x = rc.right;
	pts[2].y = rc.bottom;

	pts[3].x = rc.left;
	pts[3].y = rc.bottom;

	UMFDRAWRECORD Item;

	Item.drawType	= UMF_DRT_POLYGON;
	Item.polygon.nPt	= 4;
	Item.polygon.pPts	= pts;
	
	Item.BrushType	= BrushType;
	Item.Color		= Color;
	Item.Alpha		= Alpha;

	return InsertItem(&Item);
}


BOOL CUMFDrawUtil::DrawRectList(INT nRectCnt, RECT* pDstRects, COLORREF Color, BYTE Alpha, eUMFBRUSHTYPE BrushType)
{
	if(!m_pUMFDrawUtil) return FALSE;

	UMFDRAWRECORD Item;
	
	Item.drawType	= UMF_DRT_RECTLIST;
	Item.Color		= Color;
	Item.Alpha		= Alpha;
	Item.BrushType	= BrushType;
	Item.rects.pRects	= pDstRects;
	Item.rects.nRect	= nRectCnt;

	//Commit rect list use external memory so it 
	if(InsertItem(&Item)){
		return m_pUMFDrawUtil->Commit();	
	}

	return FALSE;
}

BOOL CUMFDrawUtil::DrawPolygon(INT PtCnt, POINT *pPoints, COLORREF Color, BYTE Alpha, eUMFBRUSHTYPE BrushType)
{
	if(!m_pUMFDrawUtil) return FALSE;

	UMFDRAWRECORD Item;

	Item.drawType	= UMF_DRT_POLYGON;
	Item.Color		= Color;
	Item.Alpha		= Alpha;
	Item.BrushType	= BrushType;
	Item.polygon.pPts	= pPoints;
	Item.polygon.nPt	= PtCnt;
	
	return InsertItem(&Item);
}

BOOL CUMFDrawUtil::DrawLineList(INT PtCnt, POINT *pPoints, COLORREF Color, BYTE Alpha)
{
	if(!m_pUMFDrawUtil) return FALSE;

	UMFDRAWRECORD Item;

	Item.drawType	= UMF_DRT_LINESTRIP;
	Item.Color		= Color;
	Item.Alpha		= Alpha;
	Item.pts.pPts	= pPoints;
	Item.pts.nPt	= PtCnt;

	return InsertItem(&Item);
}

BOOL CUMFDrawUtil::DrawCurve(INT PtCnt, POINT *pPoints, COLORREF Color, BYTE Alpha)
{
	if(!m_pUMFDrawUtil) return FALSE;
	if(4 != PtCnt) return FALSE;

	UMFDRAWRECORD Item;
	memset(&Item, 0, sizeof(UMFDRAWRECORD));

	Item.drawType	= UMF_DRT_CURVE;
	Item.Color		= Color;
	Item.Alpha		= Alpha;
	Item.curve.nPt	= PtCnt;

	memcpy(Item.curve.pPts, pPoints, sizeof(POINT)*PtCnt);
	
	return InsertItem(&Item);
}

BOOL CUMFDrawUtil::DrawCircle(POINT Center, double Radious, BOOL bFill, COLORREF Color, BYTE Alpha)
{
	if(!m_pUMFDrawUtil) return FALSE;
	
	UMFDRAWRECORD Item;
	
	Item.drawType	= UMF_DRT_CIRCLE;
	Item.Color		= Color;
	Item.Alpha		= Alpha;
	Item.circle.bFill		= bFill;
	Item.circle.fRadius		= Radious;
	Item.circle.ptCentre	= Center;

	return InsertItem(&Item);
}

BOOL CUMFDrawUtil::DrawCircularArc(POINT Center, double Radious, unsigned short Width, double StartRadius, double FinishRadius, COLORREF Color, BYTE Alpha)
{
	if(!m_pUMFDrawUtil) return FALSE;
	
	UMFDRAWRECORD Item;
	
	Item.drawType	= UMF_DRT_CIRCULARARC;
	Item.Color		= Color;
	Item.Alpha		= Alpha;
	Item.circularArc.fRadius		= Radious;
	Item.circularArc.ptCentre		= Center;
	Item.circularArc.dStartRadius	= StartRadius;
	Item.circularArc.dFinishRadius	= FinishRadius;
	Item.circularArc.usWidth		= Width;

	return InsertItem(&Item);
}

BOOL CUMFDrawUtil::InitFont(UINT index, const WCHAR* strFontName, DWORD dwHeight, DWORD dwFlags)
{
	if(!m_pUMFDrawUtil) return FALSE;
	return m_pUMFDrawUtil->InitFont(index, strFontName, dwHeight, dwFlags);
}

BOOL CUMFDrawUtil::GetTextExtent(UINT index, const WCHAR* strText, SIZE* pSize)
{
	if(!m_pUMFDrawUtil) return FALSE;
	return m_pUMFDrawUtil->GetTextExtent(index, strText, pSize);

}

//Font 
BOOL CUMFDrawUtil::DrawText(UINT index, POINT Start, WCHAR* szText, COLORREF Color, BYTE Alpha)
{
	if(!m_pUMFDrawUtil) return FALSE;

	UMFDRAWRECORD Item;

	Item.drawType	= UMF_DRT_TEXT;
	Item.text.nIndex= index;
	Item.text.pt	= Start;
	Item.text.pText	= szText;
	Item.Color		= Color;
	Item.Alpha		= Alpha;

	return InsertItem(&Item);
}


BOOL CUMFDrawUtil::DrawBoundingBox(RECT rc, COLORREF Color, BYTE Alpha)
{
	POINT pts[5];
	
	//	Assemble the 4 boundary lines of a bounding box
	pts[0].x = rc.left;
	pts[0].y = rc.top;

	pts[1].x = rc.right;
	pts[1].y = rc.top;

	pts[2].x = rc.right;
	pts[2].y = rc.bottom;

	pts[3].x = rc.left;
	pts[3].y = rc.bottom;

	pts[4].x = rc.left;
	pts[4].y = rc.top;


	UMFDRAWRECORD Item;

	Item.drawType	= UMF_DRT_LINESTRIP;
	Item.pts.nPt	= 5;
	Item.pts.pPts	= pts;
	Item.Color		= Color;
	Item.Alpha		= Alpha;

	return InsertItem(&Item);
}

BOOL CUMFDrawUtil::DrawImage(IUMFImage* pImage, RECT DstRects, BYTE Alpha)
{
	UMFDRAWRECORD Item;

	Item.drawType	= UMF_DRT_IMAGE;

	Item.image.pImage	= pImage;
	Item.image.rcTarget	= DstRects;
	Item.Alpha		= Alpha;

	return InsertItem(&Item);
}


IUMFImage*	CUMFDrawUtil::CreateUMFImage(USHORT width, USHORT height, eUMF_COLORFORMAT colorformat)
{
	if(!m_pUMFDrawUtil) return NULL;	
	return m_pUMFDrawUtil->CreateUMFImage(width, height, colorformat);
}

BOOL CUMFDrawUtil::Commit()
{
	if(!m_pUMFDrawUtil) return FALSE;
	return m_pUMFDrawUtil->Commit();

}

BOOL CUMFDrawUtil::InsertItem(UMFDRAWRECORD* pItem)
{
	DWORD Result;
	Result = m_pUMFDrawUtil->Push(pItem);
	if(Result == -1){
		Commit();
		Result = m_pUMFDrawUtil->Push(pItem);
	}
	return (Result != -1);
}

