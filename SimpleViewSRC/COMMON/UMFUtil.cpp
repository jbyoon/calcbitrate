#include "stdafx.h"
#include "UMFUtil.h"
#include <mmsystem.h>
#include "UUCore/UUCoreAPI.h"
#include "PUBLIB/INC/VCA5CoreLib.h"
#include "UMFTracer/UMFTracer.h"
#pragma comment( lib, "../../LIB/UMFTracer/UMFTracer" )

#define NSPERSEC	(10000000UI64)
#define FACTOR		(0x19db22a45262800UI64)

static ULONGLONG g_ullOffsetFrom1601To1970 = 0;
static void CalcOffsetFrom1601To1970()
{
	SYSTEMTIME bp;
	if ( g_ullOffsetFrom1601To1970 == 0 ) {
		bp.wYear		= 1970;
		bp.wMonth		= 1;
		bp.wDay			= 1;
		bp.wDayOfWeek	= 4;
		bp.wHour		= 0;
		bp.wMinute		= 0;
		bp.wSecond		= 0;
		bp.wMilliseconds = 0;
		
		SystemTimeToFileTime(&bp, (FILETIME *)&g_ullOffsetFrom1601To1970);
		FileTimeToLocalFileTime((FILETIME *)&g_ullOffsetFrom1601To1970, (FILETIME *)&g_ullOffsetFrom1601To1970);
	}
}

void TimevalToFileTime(struct timeval *pTimeval, FILETIME *pFileTime)
{
	CalcOffsetFrom1601To1970();
	ULONGLONG x = pTimeval->tv_sec * NSPERSEC + pTimeval->tv_usec * 10;
	x += g_ullOffsetFrom1601To1970;
	pFileTime->dwHighDateTime = (ULONG)(x >> 32);
	pFileTime->dwLowDateTime = (ULONG)x;
}

void FileTimeToTimeval(FILETIME *pFileTime, struct timeval *pTimeval)
{
	CalcOffsetFrom1601To1970();
	ULONGLONG ul64FileTime = 0;

	ul64FileTime |= pFileTime->dwHighDateTime;
	ul64FileTime <<= 32;
	ul64FileTime |= pFileTime->dwLowDateTime;

	ul64FileTime -= g_ullOffsetFrom1601To1970;
	pTimeval->tv_sec = (long)(ul64FileTime / 10000000);
	pTimeval->tv_usec = (long)((ul64FileTime % 10000000) / 10);
}

BOOL	GetPreDefPathFromApp(TCHAR *szDllName, TCHAR *szFullPath)
{
	//If use AxUMF, then First load AXUMF 
	//and get module to get DLL path or get default dll path
	HMODULE hModule = GetModuleHandle(_T("AxUMF.ocx"));

	BOOL* (FAR WINAPIV*_GetDLLPath)(wchar_t* szDllName, wchar_t* szPath);
	FARPROC test_proc = GetProcAddress(hModule,("GetDLLPath"));	//Current process module
	if(test_proc){
		*(FARPROC*)& _GetDLLPath = test_proc;
		if(_GetDLLPath(szDllName, szFullPath)){
			UMFTrace(UMF_TRACE_LEVEL_VERBOSE, _T("Load Name [%s] Path [%s]\n"), szDllName, szFullPath);
			return TRUE;	
		}else{
			UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Fail Get path by name [%s]\n"), szDllName);
		}
	}else{
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Fail get GetDLLPath\n"));
	}

	return FALSE;
}

void ShowGetLastErrorMessage(const TCHAR* pAdditional)
{
	DWORD dwError = GetLastError();

	HLOCAL hlocal = NULL;   

	DWORD systemLocale = MAKELANGID(LANG_NEUTRAL, SUBLANG_NEUTRAL);

	BOOL fOk = FormatMessage(
		FORMAT_MESSAGE_FROM_SYSTEM | 
		FORMAT_MESSAGE_IGNORE_INSERTS |
		FORMAT_MESSAGE_ALLOCATE_BUFFER, 
		NULL, 
		dwError, 
		systemLocale, 
		(PTSTR) &hlocal, 
		0, 
		NULL);
	
	if (!fOk) {
		HMODULE hDll = LoadLibraryEx(_T("netmsg.dll"), NULL, DONT_RESOLVE_DLL_REFERENCES );

		if (hDll != NULL) {
			fOk = FormatMessage( FORMAT_MESSAGE_FROM_HMODULE | 
				FORMAT_MESSAGE_IGNORE_INSERTS |
				FORMAT_MESSAGE_ALLOCATE_BUFFER,
				hDll, 
				dwError, 
				systemLocale,
				(PTSTR) &hlocal, 
				0,
				NULL );
			FreeLibrary(hDll);
		}
	}

	if (fOk && (hlocal != NULL)) {
		UMFTrace(UMF_TRACE_LEVEL_ERROR, _T("%s : %s"), (PCTSTR)LocalLock(hlocal), pAdditional);
		//MessageBox( hWnd, (PCTSTR) LocalLock(hlocal), _T("Error"), MB_OK );
		LocalFree(hlocal);
	} else {
		UMFTrace(UMF_TRACE_LEVEL_ERROR, _T("No text found for this error number."));
		//MessageBox( hWnd, _T("No text found for this error number."), _T("Error"), MB_OK );
	}
}

// If bDirectLoad = TRUE, then same as LoadLibrary
HMODULE	LoadUMFLibrary(TCHAR *szDllName, BOOL bDirectLoad)
{
	TCHAR szDllPath[MAX_PATH];
	if(bDirectLoad){
		_tcscpy_s(szDllPath, szDllName);
	}else{
		if(!GetPreDefPathFromApp(szDllName, szDllPath)){
			TCHAR szAbsoluteDllPath[MAX_PATH] = {0,};
			GetCurrentDirectory(sizeof(szAbsoluteDllPath), szAbsoluteDllPath);
			_stprintf_s(szDllPath, _countof(szDllPath), _T("%s\\%s"), szAbsoluteDllPath, szDllName);
		}
	}

	HMODULE hMod = LoadLibrary(szDllPath);
	if( NULL ==  hMod ) ShowGetLastErrorMessage(szDllName);
	return hMod;
}

eUMF_COLORFORMAT	UUCoreColor2UMFColorFormat(DWORD UUCoreColor)
{
	switch(UUCoreColor){
	case UU_CSP_YVU420: return UMF_YV12;
	case UU_CSP_YUY2:	return UMF_YUY2;
	case UU_CSP_RGB24:	return UMF_RGB24;	
	case UU_CSP_RGB32:	return UMF_RGBA32;	
	case UU_CSP_RGB565:	return UMF_RGB565;	
	}
	return UMF_NONE;
}

DWORD UMFColorFormat2UUCoreColor(eUMF_COLORFORMAT eColorFormat)
{
	switch(eColorFormat){
	case UMF_YV12:		return UU_CSP_YVU420;
	case UMF_YUY2:		return UU_CSP_YUY2;
	case UMF_RGB24:		return UU_CSP_RGB24;	
	case UMF_RGBA32:	return UU_CSP_RGB32;	
	case UMF_RGB565:	return UU_CSP_RGB565;	
	}
	return UU_CSP_NONE;
}

DWORD	GetCodecIdFromStreamType(TCHAR *pszCodec)
{
	if(wcscmp(_T("jpeg"), pszCodec) == 0) return UU_CODEC_MJPEG;
	else if(wcscmp(_T("mpeg2"), pszCodec) == 0) return UU_CODEC_MPEG2;
	else if(wcscmp(_T("mpeg4"), pszCodec) == 0) return UU_CODEC_MPJ4;
	else if(wcscmp(_T("h264"), pszCodec) == 0) return UU_CODEC_X264;
	else if(wcscmp(_T("ulaw"), pszCodec) == 0) return UU_CODEC_ULAW;

	return UU_CODEC_NONE;
}

DWORD	UUCoreColor2VCA5ColorFormat(DWORD	UUCoreColor)
{
	switch(UUCoreColor){
	case UU_CSP_YVU420: return VCA5_COLOR_FORMAT_YV12;
	case UU_CSP_YUY2:	return VCA5_COLOR_FORMAT_YUY2;
	case UU_CSP_RGB24:	return VCA5_COLOR_FORMAT_RGB24;	
	case UU_CSP_RGB565:	return VCA5_COLOR_FORMAT_RGB16;	
	}
	return 0;
}

DWORD	UMFColorFormatVCA5ColorFormat(eUMF_COLORFORMAT eColorFormat)
{
	switch(eColorFormat){
	case UMF_YV12: return VCA5_COLOR_FORMAT_YV12;
	case UMF_YUY2:	return VCA5_COLOR_FORMAT_YUY2;
	case UMF_RGB24:	return VCA5_COLOR_FORMAT_RGB24;	
	case UMF_RGB565:	return VCA5_COLOR_FORMAT_RGB16;	
	}
	return 0;
}