#include "StdAfx.h"
#include "UMFRender/IUMFRender.h"
#include "MultiViewRender.h"
#include "UMFUtil.h"
#include "UMFTracer/UMFTracer.h"

#ifdef _LIB	//static library
	#ifdef _DEBUG
		#pragma comment( lib, "../../LIB/UMFRender/sUMFRender_d" )
	#else
		#pragma comment( lib, "../../LIB/UMFRender/sUMFRender" )
	#endif
#else
	//#pragma comment( lib, "../../LIB/UMFRender/UMFRender" )
#endif

#ifndef TRACE
#include <atlbase.h>
#define TRACE AtlTrace
#endif

#ifndef MAKEFOURCC
#define MAKEFOURCC(ch0, ch1, ch2, ch3)                              \
                ((DWORD)(BYTE)(ch0) | ((DWORD)(BYTE)(ch1) << 8) |   \
                ((DWORD)(BYTE)(ch2) << 16) | ((DWORD)(BYTE)(ch3) << 24 ))

#endif 


#ifndef SAFE_DELETE
#define SAFE_DELETE(c) if(c){delete c;c=NULL;}else{}
#endif

#ifndef SAFE_RELEASE
#define SAFE_RELEASE(c) if(c){c->Release();c=NULL;}else{}
#endif

#define CHECK_ENGID_CONDITION(EngID)\
	if(!m_bSetup){\
		TRACE(_T("CMultiViewRender::ChangeSourceInfo not setup\n"));\
		return FALSE;\
	}\
	if(EngID >= MAX_RENDER){\
		TRACE(_T("CMultiViewRender::ChangeSourceInfo index [%d] exceed MaxEng[%d]\n"), EngID,m_MaxEng);\
		return FALSE;\
	}

#define CHECK_VIEWINDEX_CONDITION(ViewIndex)\
	if(!m_bSetup){\
		TRACE(_T("CMultiViewRender::ChangeSourceInfo not setup\n"));\
		return FALSE;\
	}\
	if(ViewIndex >= m_MaxViewIndex){\
		TRACE(_T("CMultiViewRender::ChangeSourceInfo View Index [%d] exceed MaxView Index [%d]\n"), ViewIndex,m_MaxViewIndex);\
		return FALSE;\
	}


DWORD GetMaxIndexChannel(CMultiViewRender::VIEW_MODE mode)
{
	if(CMultiViewRender::VIEW_1 == mode)	return 1;
	else if(CMultiViewRender::VIEW_4 == mode)return 4;
	else if(CMultiViewRender::VIEW_9 == mode)return 9;
	else if(CMultiViewRender::VIEW_16 == mode)return 16;
	return 0;
}

inline eUMF_COLORFORMAT GetColorFormatFromBitInfoHeader(BITMAPINFOHEADER *pbm)
{
	eUMF_COLORFORMAT format = UMF_NONE;
	if (MAKEFOURCC('Y','U','Y','2') == pbm->biCompression) {
		format = UMF_YUY2;
	} else if(MAKEFOURCC('Y','V','1','2') == pbm->biCompression) {
		format = UMF_YUV420;
	}else if(MAKEFOURCC('I','Y','U','V') == pbm->biCompression) {
		format = UMF_YUV420;
	} else { //RGB 
		if(pbm->biBitCount == 32){
			format = UMF_RGBA32;
		}else if(pbm->biBitCount == 24){
			format = UMF_RGB24;
		}else if(pbm->biBitCount == 16){
			format = UMF_RGB565;
		}else if(pbm->biBitCount == 15){
			format = UMF_RGB555;
		}
	}
	return format;
}

CMultiViewRender::CMultiViewRender(void)
{
	m_bSetup		= FALSE;
	m_hWnd			= NULL;
	m_CurViewMode	= VIEW_4;
	m_MaxEng		= 0;
	m_MaxViewIndex	= 0;

	m_pUmfRender	= NULL;
	memset(m_ViewInfo, 0 ,sizeof(m_ViewInfo));
	memset(m_ViewEngInfo, 0 ,sizeof(m_ViewEngInfo));


	for(DWORD i = 0 ; i < MAX_RENDER ; i++){
		m_ViewEngInfo[i].ViewIndex	= UNDEFINED_INDEX;
		m_ViewEngInfo[i].BufIndex	= UNDEFINED_INDEX;
		m_ViewInfo[i].EngId			= UNDEFINED_INDEX;
	}

	InitializeCriticalSection(&m_CS); 
}


CMultiViewRender::~CMultiViewRender(void)
{
	if(m_bSetup)Endup();
	DeleteCriticalSection(&m_CS); 
}


BOOL	CMultiViewRender::Setup(HWND hWnd, UINT MaxEng)
{
	if(m_bSetup){
		TRACE(_T("CMultiViewRender Setup before \n"));
		return TRUE;
	}

	HMODULE hModule = LoadUMFLibrary(_T("UMFRender.dll"));
	if(!hModule){
		UMFTrace(UMF_TRACE_LEVEL_CRITICAL, _T("Can not Load UMFRender.dll\n"));
		return FALSE;
	}

	IUMFRender* (FAR WINAPI*_CreateUMFRender)(DWORD type);
	FARPROC test_proc=GetProcAddress(hModule, "CreateUMFRender");
	if (test_proc) {
		*(FARPROC*)&_CreateUMFRender=test_proc;
	}
	m_pUmfRender = _CreateUMFRender(UMF_DX9Render);

	if(!m_pUmfRender->Open(hWnd)){
		MessageBox(NULL, _T("Can not Create DXD3Render \n"), _T("ERROR"), MB_OK);
		goto FAILED; 
	}

	m_hWnd		= hWnd;
	m_MaxEng	= MaxEng;
	m_bSetup	= TRUE;

	OnChangeClientRect();

	m_hMonitor = MonitorFromWindow(m_hWnd, MONITOR_DEFAULTTONEAREST);
	return TRUE;

FAILED:
	SAFE_DELETE(m_pUmfRender);
	return FALSE;
}


void	CMultiViewRender::Endup()
{
	if(m_bSetup){
		for(UINT i = 0 ; i < m_MaxEng ; i++){
			DestroyViewEngInfo(i);	
		}	

		m_pUmfRender->DestroySelf();
		m_pUmfRender = NULL;
		
		m_hWnd		= NULL;
		m_MaxEng	= 0;
		m_bSetup	= FALSE;
	}
}

UINT	CMultiViewRender::GetImageSize(DWORD EngId)
{
	CHECK_ENGID_CONDITION(EngId);
	if(UNDEFINED_INDEX == m_ViewEngInfo[EngId].BufIndex){
		TRACE(_T("Buf[%d] not assigned imagebuffer\n"), EngId);
		return FALSE;
	}

	DWORD ImageBufIndex = m_ViewEngInfo[EngId].BufIndex;
	eUMF_COLORFORMAT Imageformat = m_ViewEngInfo[EngId].ImageColorFormat;
	return m_pUmfRender->GetImageSize(EngId, ImageBufIndex, Imageformat);
}


BOOL	CMultiViewRender::OnChangeClientRect(RECT *pRect)
{
	if(!m_bSetup)return FALSE;
	BOOL bChange;
	RECT rect;
	if(pRect == NULL){
		GetClientRect( m_hWnd, &rect);
	}else{
		rect = *pRect;
	}

	EnterCriticalSection(&m_CS); 
	bChange = m_pUmfRender->SetParam((DWORD)UMFRenderChangeClientSIZE,NULL);
	LeaveCriticalSection(&m_CS);

	m_ClientRect = rect;
	Split();
//	DrawBackGround();

	return bChange;
}


BOOL	CMultiViewRender::ChangeSourceInfo(DWORD EngId, BITMAPINFOHEADER *pbm)
{
	CHECK_ENGID_CONDITION(EngId);
	EnterCriticalSection(&m_CS); 
	DWORD	ViewIndex = m_ViewEngInfo[EngId].ViewIndex ;
	DestroyViewEngInfo(EngId);	
	
	eUMF_COLORFORMAT format = GetColorFormatFromBitInfoHeader(pbm);
	DWORD ImageIndex = m_pUmfRender->CreateImageBuf(pbm->biWidth, pbm->biHeight, format);
	if(ImageIndex == -1){
		TRACE(_T("Can not Create ImageBuf retry RGB565\n"));

		//format = UMF_RGB565;
		ImageIndex = m_pUmfRender->CreateImageBuf(pbm->biWidth, pbm->biHeight, UMF_RGB565);
		if(ImageIndex == -1){
			TRACE(_T("Can not Create ImageBuf\n"));
			LeaveCriticalSection(&m_CS);
			return FALSE;
		}
	}

	m_ViewEngInfo[EngId].ImageColorFormat	= format;
	m_ViewEngInfo[EngId].Width				= pbm->biWidth;
	m_ViewEngInfo[EngId].Height				= pbm->biHeight;
	m_ViewEngInfo[EngId].BufIndex			= ImageIndex;
	m_ViewEngInfo[EngId].ViewIndex			= ViewIndex;
	LeaveCriticalSection(&m_CS);
	return TRUE;
}

BOOL	CMultiViewRender::ChangeSourceInfo(DWORD EngId, DWORD width, DWORD height, eUMF_COLORFORMAT format)
{
	CHECK_ENGID_CONDITION(EngId);

	DWORD	ViewIndex = m_ViewEngInfo[EngId].ViewIndex ;
	DestroyViewEngInfo(EngId);
	if( (width == 0) || (height == 0) || (format == UMF_NONE)) {
		TRACE(_T("Source width [%d] height[%d] format [%d] does not support!\n"), width, height, format);
		return FALSE;
	}
	
	if(m_pUmfRender == NULL)
		return FALSE;
	EnterCriticalSection(&m_CS); 
	DWORD ImageIndex = m_pUmfRender->CreateImageBuf(width, height, format);
	if(ImageIndex == -1){
		TRACE(_T("Can not Create ImageBuf retry RGB565\n"));

		//format = UMF_RGB565;
		ImageIndex = m_pUmfRender->CreateImageBuf(width, height, UMF_RGB565);
		if(ImageIndex == -1){
			TRACE(_T("Can not Create ImageBuf\n"));
			LeaveCriticalSection(&m_CS);
			return FALSE;
		}
	}

	m_ViewEngInfo[EngId].ImageColorFormat	= format;
	m_ViewEngInfo[EngId].Width				= width;
	m_ViewEngInfo[EngId].Height				= height;
	m_ViewEngInfo[EngId].BufIndex			= ImageIndex;
	m_ViewEngInfo[EngId].ViewIndex			= ViewIndex;
	LeaveCriticalSection(&m_CS);
	return TRUE;
}

VIEWINFO*	CMultiViewRender::GetViewInfoByIndex(DWORD Index)
{
	CHECK_VIEWINDEX_CONDITION(Index);
	return &m_ViewInfo[Index];
}

VIEWINFO*	CMultiViewRender::GetViewInfoByEngId(DWORD EngId)
{
	CHECK_ENGID_CONDITION(EngId);
	if(UNDEFINED_INDEX == m_ViewEngInfo[EngId].ViewIndex) return NULL;
	return &m_ViewInfo[m_ViewEngInfo[EngId].ViewIndex];
}


DWORD		CMultiViewRender::GetViewIndexByEngId(DWORD EngId)
{
	CHECK_ENGID_CONDITION(EngId);
	return m_ViewEngInfo[EngId].ViewIndex;
}


VIEWENGINFO* CMultiViewRender::GetViewEngineInfo(DWORD EngId)
{
	CHECK_ENGID_CONDITION(EngId);
	return &m_ViewEngInfo[EngId];
}


BOOL	CMultiViewRender::RenderImage(DWORD EngId, BYTE *pImage)
{
	CHECK_ENGID_CONDITION(EngId);
	if(UNDEFINED_INDEX == m_ViewEngInfo[EngId].BufIndex){
		TRACE(_T("Buf[%d] not assigned imagebuffer\n"), EngId);
		return FALSE;
	}

	if(UNDEFINED_INDEX == m_ViewEngInfo[EngId].ViewIndex){
		TRACE(_T("Eng[%d] not assigned Viewindex\n"), EngId);
		return FALSE;
	}
	
	EnterCriticalSection(&m_CS); 

	RECT rect = m_ViewInfo[m_ViewEngInfo[EngId].ViewIndex].TargetRect;
	m_pUmfRender->DrawImage(m_ViewEngInfo[EngId].BufIndex, pImage, m_ViewEngInfo[EngId].ImageColorFormat, rect);

//	m_pVMRRender[EngId]->DrawMetaData(m_pUmfRender, rect, pMetadata, nLength);
//	m_pUmfRender->Present(rect);
	LeaveCriticalSection(&m_CS);
	return TRUE;
}

BOOL	CMultiViewRender::Present(DWORD EngId)
{
	RECT rect = m_ViewInfo[m_ViewEngInfo[EngId].ViewIndex].TargetRect;
	if(rect.right == 0 || rect.bottom ==0) return FALSE;

	m_pUmfRender->Present(rect);
	return TRUE;
}


void	CMultiViewRender::OnEraseBackGround()
{
	if(!m_bSetup)return ;
	EnterCriticalSection(&m_CS); 
	m_pUmfRender->SetParam(UMFRenderClearDevice, NULL);
	LeaveCriticalSection(&m_CS);
}


void	CMultiViewRender::DrawBackGround()
{
	//Draw Logo or Background

 	HDC hDC = GetDC(m_hWnd);
// 	
// 	EnterCriticalSection(&m_CS); 
 	FillRect(hDC, &m_ClientRect, (HBRUSH)GetStockObject(BLACK_BRUSH));
 	ReleaseDC(m_hWnd, hDC);
// 
 //	OnEraseBackGround();
// 	LeaveCriticalSection(&m_CS);

}


BOOL	CMultiViewRender::SetViewIndexToEngId(DWORD ViewIndex, DWORD EngId)
{
	CHECK_ENGID_CONDITION(EngId);	
	CHECK_VIEWINDEX_CONDITION(ViewIndex);
	
	DWORD PreEngId = m_ViewInfo[ViewIndex].EngId;
	if(PreEngId < MAX_RENDER) m_ViewEngInfo[PreEngId].ViewIndex = UNDEFINED_INDEX;

	m_ViewInfo[ViewIndex].EngId		= EngId;
	m_ViewEngInfo[EngId].ViewIndex	= ViewIndex;	

	return TRUE;
}

void	CMultiViewRender::Split()
{
	UINT	i, MaxUICh, Mod;
	UINT	width, height, unit_w, unit_h;
	RECT	rect;
	width	= m_ClientRect.right - m_ClientRect.left;
	height	= m_ClientRect.bottom - m_ClientRect.top;

	if(VIEW_1 == m_CurViewMode){
		unit_w	= width;
		unit_h	= height;
		MaxUICh = 1;
		Mod = 1;
	}else if(VIEW_4 == m_CurViewMode){
		MaxUICh = 4;
		unit_w= width/2;
		unit_h= height/2;
		Mod = 2;
	}else if(VIEW_9 == m_CurViewMode){
		MaxUICh = 9;
		unit_w= width/3;
		unit_h= height/3;
		Mod = 3;
	}else if(VIEW_16 == m_CurViewMode){
		MaxUICh = 16;
		unit_w= width/4;
		unit_h= height/4;
		Mod = 4;
	}

	m_MaxViewIndex = MaxUICh;
	for(i = 0 ; i < m_MaxViewIndex ; i++){
		rect.left	= m_ClientRect.left + (unit_w*(i%Mod));
		rect.top	= m_ClientRect.top + (unit_h*(i/Mod));
		rect.right	= rect.left + unit_w;
		rect.bottom	= rect.top + unit_h;
		m_ViewInfo[i].TargetRect	= rect;
		m_ViewInfo[i].EngId			= i;
		m_ViewEngInfo[i].ViewIndex	= i;			
	}

	for(i = m_MaxViewIndex ; i < m_MaxEng ; i++){
		m_ViewEngInfo[i].ViewIndex = UNDEFINED_INDEX;
	}
}


void	CMultiViewRender::DestroyViewEngInfo(DWORD EngId)
{
	if(EngId >= m_MaxEng){
		TRACE(_T("CMultiViewRender::ChangeSourceInfo index [%d] exceed MaxEng[%d]\n"), EngId,m_MaxEng);
		return ;
	}

	m_pUmfRender->DestroyImageBuf(m_ViewEngInfo[EngId].BufIndex);
	memset(&(m_ViewEngInfo[EngId]), 0, sizeof(VIEWENGINFO));
	m_ViewEngInfo[EngId].BufIndex = -1;
}


BOOL	CMultiViewRender::CheckChangeViewMonitor(BOOL *bChanged)
{
	HMONITOR	hMonitor = 	MonitorFromWindow(m_hWnd, MONITOR_DEFAULTTONEAREST);
	if(m_hMonitor != hMonitor){
		m_hMonitor = hMonitor;
		*bChanged = TRUE;
		return TRUE;
	}

	*bChanged = FALSE;
	return TRUE;
}


BOOL	CMultiViewRender::ChangeViewMonitor() 
{
	if(!m_pUmfRender) return FALSE;
	
	EnterCriticalSection(&m_CS); 
	BOOL bChange = m_pUmfRender->SetParam((DWORD)UMFRenderChangeMonitor,NULL);
	LeaveCriticalSection(&m_CS);

	return bChange;
}

DWORD	CMultiViewRender::GetEngByPos(POINT pt)
{
	UINT	i, MaxUICh;

	if(VIEW_1 == m_CurViewMode){
		MaxUICh = 1;
	}else if(VIEW_4 == m_CurViewMode){
		MaxUICh = 4;
	}else if(VIEW_9 == m_CurViewMode){
		MaxUICh = 9;
	}else if(VIEW_16 == m_CurViewMode){
		MaxUICh = 16;
	}
	
	for(i = 0 ; i < MaxUICh ; i++){
		if(PtInRect(&m_ViewInfo[i].TargetRect, pt))	return m_ViewInfo[i].EngId;
	}
	
	return UNDEFINED_INDEX;
}

