#ifndef __UMF_UTILS_H__
#define __UMF_UTILS_H__

#include <winsock.h>
#include "UMFBase.h"

BOOL		GetPreDefPathFromApp(TCHAR *szDllName, TCHAR *szFullPath);
HMODULE		LoadUMFLibrary(TCHAR *szDllName, BOOL bDirectLoad = FALSE);
void		TimevalToFileTime(struct timeval *pTimeval, FILETIME *pFileTime);
void		FileTimeToTimeval(FILETIME *pFileTime, struct timeval *pTimeval);

eUMF_COLORFORMAT	UUCoreColor2UMFColorFormat(DWORD UUCoreColor);
DWORD				UMFColorFormat2UUCoreColor(eUMF_COLORFORMAT eColorFormat);
DWORD				GetCodecIdFromStreamType(TCHAR *pszCodec);

DWORD				UUCoreColor2VCA5ColorFormat(DWORD	UUCoreColor);
DWORD				UMFColorFormatVCA5ColorFormat(eUMF_COLORFORMAT eColorFormat);

void				ShowGetLastErrorMessage(const TCHAR* pAdditional);


#define		UMF_CREATE_INSTANCE(INSTHANCE, MODULE, TYPE, FUNC_NAME)\
do{\
	TYPE* (FAR WINAPI*_##FUNC_NAME)();\
	FARPROC test_proc=GetProcAddress(MODULE, #FUNC_NAME);\
	if (test_proc) {\
		*(FARPROC*)&_##FUNC_NAME=test_proc;\
		INSTHANCE = _##FUNC_NAME();\
	}\
}while(0)\



#endif