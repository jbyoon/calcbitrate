#include "stdafx.h"	//Commoninterface refer to upper folder stdafx.
#include "UMFTrace.h"
#include <stdio.h>
#include <assert.h>

#pragma		warning( disable :4996 )

#define UMF_TRACE_BUFFER_LEN	1024
#define UMF_MAX_LEN				256
#define UMF_LINE_NO_NONE		-1

#ifdef _DEBUG
static eUMF_TRACE_LEVEL g_CurrentTraceLevel = UMF_TRACE_LEVEL_WARNING;
#else
static eUMF_TRACE_LEVEL g_CurrentTraceLevel = UMF_TRACE_LEVEL_ERROR;
#endif

static ULONG g_ulTraceDisplayOption = 0x000000FF;
static TCHAR g_szModuleName[UMF_TRACE_BUFFER_LEN] = {0,};
static TCHAR g_szLogFileName[UMF_MAX_LEN] = _T(".\\UMF_Debug_Log.txt");
static FILE *g_fpLogFile = NULL;

// #include <locale.h>
// 
// #ifdef _DEBUG
// _tsetlocale(LC_ALL, _T("korean"));
// #endif

void UMFSetTraceLevel(eUMF_TRACE_LEVEL eTraceLevel)
{
	g_CurrentTraceLevel = eTraceLevel;
}

eUMF_TRACE_LEVEL UMFGetTraceLevel()
{
	return g_CurrentTraceLevel;
}

void UMFSetTraceOption(TCHAR* szModuleName, ULONG uTraceDisplayOption)
{
	if(szModuleName) {
		_tcscpy_s(g_szModuleName, _countof(g_szModuleName), szModuleName);
	}
	g_ulTraceDisplayOption = uTraceDisplayOption;
}

ULONG UMFGetTraceOption()
{
	return g_ulTraceDisplayOption;
}

void UMFSetDebugFileName(TCHAR *file, BOOL truncate)
{
	if ( file && !_tcscmp(g_szLogFileName, file)) {
		return;
	}

	if (g_fpLogFile) {
		fclose(g_fpLogFile);
		g_fpLogFile = NULL;
	}

	if (file) {
		_tcscpy_s(g_szLogFileName, _countof(g_szLogFileName), file);
	}

	if (*g_szLogFileName) {
		g_fpLogFile = _tfopen(g_szLogFileName, (truncate) ? _T("w") : _T("a"));
	}
}

static void UMFShowMessage(eUMF_TRACE_LEVEL Level, TCHAR* szBuffer)
{
	if(UMF_TRACE_LEVEL_CRITICAL == Level) {
		assert(FALSE);
	} else if(UMF_TRACE_LEVEL_ERROR ==  Level) {
#if defined(_WIN32) || defined(_WIN64)
		MessageBox(GetActiveWindow(), szBuffer, _T("Error"), MB_OK|MB_ICONERROR);
#else
		_tprintf(_T("%s"), szBuffer);
#endif
	} else {
		OutputDebugString(szBuffer);
	}

	if(g_ulTraceDisplayOption & UMF_TRACE_WRITE_LOG) {
		//UMFSetDebugFileName(NULL, TRUE);
		if (g_fpLogFile) {
#if defined(UNICODE) || defined(_UNICODE)
			fprintf(g_fpLogFile, "%S\n", szBuffer);
#else
			fprintf(g_fpLogFile, "%s\n", szBuffer);
#endif
			fflush(g_fpLogFile);
			//	fclose(g_fpLogFile);
		}
	}
}

void UMFTrace(eUMF_TRACE_LEVEL eLevel, LPCTSTR lpszFormat, ...)
{
	if(g_CurrentTraceLevel < eLevel) return;

	BOOL bSwitch = g_ulTraceDisplayOption && eLevel;
	if(FALSE == bSwitch) return;

	int nBuf;
	TCHAR szBuffer[UMF_TRACE_BUFFER_LEN] = {0,};

	// Append ModuleName at the beginning of the string
	if(_tcslen(g_szModuleName)) _stprintf_s(szBuffer, _T("[%s] %s "), g_szModuleName, szBuffer);


	// Write lpszFormat
	va_list args;
	va_start(args, lpszFormat);

	nBuf = _vstprintf_s(szBuffer, lpszFormat, args);
	assert(nBuf >= 0);

	va_end(args);

	// Show message
	UMFShowMessage(eLevel, szBuffer);
}

void UMFTraceEX(eUMF_TRACE_LEVEL eLevel, TCHAR* szFile, int nLine, TCHAR* szFunc, LPCTSTR lpszFormat, ...)
{
	if(g_CurrentTraceLevel < eLevel) return;

	BOOL bSwitch = g_ulTraceDisplayOption && eLevel;
	if(FALSE == bSwitch) return;

	int nBuf;
	TCHAR szBuffer[UMF_TRACE_BUFFER_LEN] = {0,};

	// Append ModuleName at the beginning of the string
	if(_tcslen(g_szModuleName)) _stprintf_s(szBuffer, _T("[%s] %s "), g_szModuleName, szBuffer);


	// Write lpszFormat
	va_list args;
    va_start(args, lpszFormat);

	nBuf = _vstprintf_s(szBuffer, lpszFormat, args);
	assert(nBuf >= 0);

	va_end(args);


	_tcscat_s(szBuffer, _countof(g_szModuleName), _T("\t--> "));

	// Write filename, line number, and function name
	if(g_ulTraceDisplayOption & UMF_TRACE_DISPLAY_FILENAME) {
		if(szFile != NULL)
			_stprintf_s(szBuffer, _T("%s filename: %s "), szBuffer, szFile);
	}
	if(g_ulTraceDisplayOption & UMF_TRACE_DISPLAY_LINE) {
		if(nLine != UMF_LINE_NO_NONE)
			_stprintf_s(szBuffer, _T("%s line: %d "), szBuffer, nLine);
	}
	if(g_ulTraceDisplayOption & UMF_TRACE_DISPLAY_FUNC) {
		if(szFunc != NULL)
			_stprintf_s(szBuffer, _T("%s %s() "), szBuffer, szFunc);
	}
	_tcscat_s(szBuffer, _countof(g_szModuleName), _T("\n"));
	
	// Show message
	UMFShowMessage(eLevel, szBuffer);
}

