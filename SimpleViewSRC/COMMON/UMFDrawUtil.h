#ifndef __UMF_DRAW_UTILS_H__
#define __UMF_DRAW_UTILS_H__

#include "UMFRender/IUMFDrawUtil.h"

class CUMFDrawUtil
{
public:
	CUMFDrawUtil();
	~CUMFDrawUtil();
	
	BOOL Init(IUMFRender *pUMFRender);
	BOOL ReInit(IUMFRender *pUMFRender);

	void SetCanvasRect(RECT CanvasRect);
	SIZE GetCanvasSize();

	BOOL DrawRect(RECT rc, COLORREF Color, BYTE Alpha, eUMFBRUSHTYPE BrushType);
	BOOL DrawBoundingBox(RECT rc, COLORREF Color, BYTE Alpha);
	BOOL DrawPolygon(INT PtCnt, POINT *pPoints, COLORREF Color, BYTE Alpha, eUMFBRUSHTYPE BrushType);
	BOOL DrawLineList(INT PtCnt, POINT *pPoints, COLORREF Color, BYTE Alpha);
	BOOL DrawCurve(INT PtCnt, POINT *pPoints, COLORREF Color, BYTE Alpha);
	
	BOOL DrawCircle(POINT Center, double Radious, BOOL bFill, COLORREF Color, BYTE Alpha);
	BOOL DrawCircularArc(POINT Center, double Radious, unsigned short Width, double StartRadius, double FinishRadius, COLORREF Color, BYTE Alpha);

	//Font 
	BOOL InitFont(UINT index, const WCHAR* strFontName, DWORD dwHeight, DWORD dwFlags);
	BOOL GetTextExtent(UINT index, const WCHAR* strText, SIZE* pSize);
	BOOL DrawText(UINT index, POINT Start, WCHAR* szText, COLORREF Color, BYTE Alpha);
	
	//RectList : Undefined region (like as blob map, waterlevel occupacy)
	BOOL DrawRectList(INT nRectCnt, RECT* pDstRects, COLORREF Color, BYTE Alpha, eUMFBRUSHTYPE BrushType);
	BOOL DrawImage(IUMFImage* pImage, RECT DstRects, BYTE Alpha);

	//
	IUMFImage*	CreateUMFImage(USHORT width, USHORT height, eUMF_COLORFORMAT colorformat);


	//Render 
	BOOL Commit();

	
private:
	RECT			m_CanvasRect;
	IUMFRender*		m_pUMFRender;
	IUMFDrawUtil*	m_pUMFDrawUtil;

	BOOL	InsertItem(UMFDRAWRECORD* pItem);
};


#endif