// AVIRec.cpp: implementation of the CAVIRec class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "AVIRec.h"

#pragma comment( lib, "Vfw32")
#pragma warning(disable:4996)

void CAVIRec::DiagMsg(HRESULT errCode)
{
	LPSTR strErr = 0;

	switch(errCode) {
	case AVIERR_UNSUPPORTED:	strErr = "AVIERR_UNSUPPORTED";	break;
	case AVIERR_BADFORMAT:		strErr = "AVIERR_BADFORMAT";	break;
	case AVIERR_MEMORY:			strErr = "AVIERR_MEMORY";	break;
	case AVIERR_INTERNAL:		strErr = "AVIERR_INTERNAL";	break;
	case AVIERR_BADFLAGS:		strErr = "AVIERR_BADFLAGS";	break;
	case AVIERR_BADPARAM:		strErr = "AVIERR_BADPARAM";	break;
	case AVIERR_BADSIZE:		strErr = "AVIERR_BADSIZE";	break;
	case AVIERR_BADHANDLE:		strErr = "AVIERR_BADHANDLE";	break;
	case AVIERR_FILEREAD:		strErr = "AVIERR_FILEREAD";	break;
	case AVIERR_FILEWRITE:		strErr = "AVIERR_FILEWRITE";	break;
	case AVIERR_FILEOPEN:		strErr = "AVIERR_FILEOPEN";	break;
	case AVIERR_COMPRESSOR:		strErr = "AVIERR_COMPRESSOR";	break;
	case AVIERR_NOCOMPRESSOR:	strErr = "AVIERR_NOCOMPRESSOR";	break;
	case AVIERR_READONLY:		strErr = "AVIERR_READONLY";	break;
	case AVIERR_NODATA:			strErr = "AVIERR_NODATA";	break;
	case AVIERR_BUFFERTOOSMALL:	strErr = "AVIERR_BUFFERTOOSMALL";	break;
	case AVIERR_CANTCOMPRESS:	strErr = "AVIERR_CANTCOMPRESS";	break;
	case AVIERR_USERABORT:		strErr = "AVIERR_USERABORT";	break;
	case AVIERR_ERROR:			strErr = "AVIERR_ERROR";	break;
	default:	strErr = "Unknown error";		break;
	}
}

BOOL CreateVideoStream(PAVIFILE aviFile,FOURCC fccHandler,BITMAPINFOHEADER* pbih,PAVISTREAM * ppavi, ULONG uRate)
{
	if(pbih==NULL)return FALSE;

	AVISTREAMINFO		strmInfoVideo;
	HRESULT hr;

	ZeroMemory(&strmInfoVideo, sizeof(strmInfoVideo));
	strmInfoVideo.fccType					= streamtypeVIDEO;
	strmInfoVideo.fccHandler				= fccHandler;
	strmInfoVideo.dwScale					= 100;
	strmInfoVideo.dwRate					= uRate;
	strmInfoVideo.dwStart					= 0;
	strmInfoVideo.dwLength					= 0;
	strmInfoVideo.dwInitialFrames			= 0;
	strmInfoVideo.dwSuggestedBufferSize 	= 0;
	strmInfoVideo.dwQuality 				= -1;
	strmInfoVideo.dwSampleSize				= 0;
	strmInfoVideo.dwEditCount				= 0;
	strmInfoVideo.dwFormatChangeCount		= 0;
	strmInfoVideo.dwSampleSize				= 0;
	SetRect(&strmInfoVideo.rcFrame, 0, 0, (int)pbih->biWidth, (int)pbih->biHeight);

	PAVISTREAM			strmVideo;
	hr = AVIFileCreateStream(aviFile, &strmVideo, &strmInfoVideo);
	if (hr != 0) {
		AVIFileRelease(aviFile);
		return FALSE;
	} 

	hr = AVIStreamSetFormat(strmVideo, 0, pbih,pbih->biSize);
	if (hr != 0) { 
		AVIStreamRelease(strmVideo); 
		return FALSE; 
	}
	*ppavi=strmVideo;
	return TRUE;
}

BOOL CreateAudioStream(PAVIFILE aviFile,WAVEFORMATEX* pwf,PAVISTREAM * ppavi)
{
	if(pwf==NULL)return FALSE;
	if(pwf->wFormatTag==0)return FALSE;

	AVISTREAMINFO		strmInfoAudio;
	HRESULT hr;

	ZeroMemory(&strmInfoAudio, sizeof(strmInfoAudio));
	
	strmInfoAudio.fccType					= streamtypeAUDIO;
	strmInfoAudio.fccHandler				= 0;
	strmInfoAudio.dwScale					= 1;
	strmInfoAudio.dwRate					= pwf->nSamplesPerSec;
	strmInfoAudio.dwSuggestedBufferSize 	= 0;
	strmInfoAudio.dwSampleSize				= 0;
	strmInfoAudio.dwStart					= 0;
	strmInfoAudio.dwLength					= 0;
	strmInfoAudio.dwInitialFrames			= 0;
	strmInfoAudio.dwQuality 				= -1;
	strmInfoAudio.dwEditCount				= 0;
	strmInfoAudio.dwFormatChangeCount		= 0;
	
	PAVISTREAM aviStream;
	hr = AVIFileCreateStream(aviFile, &aviStream, &strmInfoAudio); 
	if (hr != 0) {
		return FALSE; 
	} 

	hr = AVIStreamSetFormat(aviStream, 0, pwf,sizeof(WAVEFORMATEX)+pwf->cbSize); 
	if (hr != 0) { 
		AVIStreamRelease(aviStream); 
		return FALSE; 
	} 
	*ppavi=aviStream;
	return TRUE;
	
}

BOOL CreateTextStream(PAVIFILE aviFile,PAVISTREAM * ppavi, ULONG uRate)
{
	AVISTREAMINFO		strmInfoCaption;
	BITMAPINFOHEADER	bihCaption;
	HRESULT hr;

	ZeroMemory(&bihCaption, sizeof(bihCaption));
	bihCaption.biSize = 4;
	ZeroMemory(&strmInfoCaption, sizeof(strmInfoCaption));
	strmInfoCaption.fccType 				= streamtypeTEXT;
	strmInfoCaption.fccHandler				= 0x2e565247;
	strmInfoCaption.dwScale 				= 100;
	strmInfoCaption.dwRate					= uRate;
	strmInfoCaption.dwStart 				= 0;
	strmInfoCaption.dwLength				= 0;
	strmInfoCaption.dwInitialFrames 		= 0;
	strmInfoCaption.dwSuggestedBufferSize	= 0;
	strmInfoCaption.dwQuality				= -1;
	strmInfoCaption.dwSampleSize			= 0;
	strmInfoCaption.dwEditCount 			= 0;
	strmInfoCaption.dwFormatChangeCount 	= 0;
	strmInfoCaption.dwSampleSize			= 0;
	SetRect(&strmInfoCaption.rcFrame, 0, 0, 0, 0); 

	PAVISTREAM	strmCaption;
	hr = AVIFileCreateStream(aviFile, &strmCaption, &strmInfoCaption); 
	if (hr != 0) {
		return FALSE; 
	} 

	hr = AVIStreamSetFormat(strmCaption, 0, &bihCaption, sizeof(bihCaption.biSize)); 
	if (hr != 0) { 
		AVIStreamRelease(strmCaption); 
		return FALSE; 
	} 
	*ppavi=strmCaption;
	return TRUE;
}

CAVIRec::CAVIRec()
{
	m_bOpened = FALSE;
	m_strmVideo = NULL;
	m_strmAudio = NULL;
	m_strmCaption = NULL;
}

CAVIRec::~CAVIRec()
{
	Close();
}


BOOL CAVIRec::Open(TCHAR* lpFilename, BITMAPINFOHEADER* pbih,  DWORD fccHandler, WAVEFORMATEX *pwfx, ULONG uRate)
{
//	OutputDebugString("CAVIRec::Open\n");
	if (m_bOpened)return FALSE;

	CoInitialize(NULL);
	HRESULT hr; 
	hr = AVIFileOpen(&m_aviFile, lpFilename, OF_WRITE | OF_CREATE, NULL); 
	if (hr != 0) {
		DiagMsg(hr);
		return FALSE;
	}

	if(!CreateVideoStream(m_aviFile,fccHandler,pbih,&m_strmVideo, uRate))m_strmVideo=NULL;
	if (pwfx) {
		if(!CreateAudioStream(m_aviFile, pwfx, &m_strmAudio)) m_strmAudio=NULL;
	}
	if(!CreateTextStream (m_aviFile,&m_strmCaption, uRate))m_strmCaption=NULL;
	
	if(m_strmVideo==NULL){
		if(m_strmCaption)AVIStreamRelease(m_strmCaption);
		if(m_strmAudio)AVIStreamRelease(m_strmAudio);
		AVIFileRelease(m_aviFile);
		return FALSE;
	}

	m_bOpened = TRUE;

	return TRUE;
}

void CAVIRec::Close()
{
	if (m_bOpened) {
		if (m_strmCaption) {
			AVIStreamRelease(m_strmCaption); 
			m_strmCaption = NULL;
		}

		if (m_strmVideo) {
			AVIStreamRelease(m_strmVideo); 
			m_strmVideo	= NULL;
		}

		if (m_strmAudio) {
			AVIStreamRelease(m_strmAudio);
			m_strmAudio = NULL;
		}
		
		if (m_aviFile) {
			AVIFileRelease(m_aviFile); 
			m_aviFile = NULL;
		}

		m_bOpened = FALSE;
	}
}

BOOL CAVIRec::WriteVideo(DWORD frameNo, LPVOID buf, LONG sizeImage, BOOL bKeyFrame)
{
	if (!m_bOpened) {
		return FALSE;
	}
	

	HRESULT hr; 
	hr = AVIStreamWrite(m_strmVideo, frameNo, 1, buf, sizeImage, bKeyFrame ? AVIIF_KEYFRAME : 0, NULL, NULL);

	return hr ? FALSE : TRUE;
}

BOOL CAVIRec::WriteAudio(DWORD frameNo, LPVOID buf, LONG size)
{
	if (!m_bOpened) {
		return FALSE;
	}

	HRESULT hr; 
	hr = AVIStreamWrite(m_strmAudio, frameNo, 1, buf, size, AVIIF_KEYFRAME, NULL, NULL);

	return hr ? FALSE : TRUE;
}

BOOL CAVIRec::WriteCaption(DWORD frameNo, LPVOID buf, LONG sizeCaption, BOOL bKeyFrame)
{
	if (!m_bOpened) {
		return FALSE;
	}

	HRESULT hr; 
	hr = AVIStreamWrite(m_strmCaption, frameNo, 1, buf, sizeCaption, bKeyFrame ? AVIIF_KEYFRAME : 0, NULL, NULL);

	return hr ? FALSE : TRUE;
}

//___CAVIFolderRec --

CAVIFolderRec::CAVIFolderRec()
{
	m_bOpened	= FALSE;
}


BOOL CAVIFolderRec::CreateAVIFolder(SYSTEMTIME* pST, TCHAR *szFolderPath)
{
	TCHAR sPath[MAX_PATH];
	TCHAR szFolderName[MAX_PATH];

	_swprintf(szFolderName, _T("\\Record_%04d-%02d-%02d-%02d_%02d_%02d"), 
		pST->wYear, pST->wMonth, pST->wDay, pST->wHour, pST->wMinute, pST->wSecond);
	_tcscpy(sPath, m_szFolder);
	_tcscat(sPath, szFolderName);

	_tcscpy(szFolderPath, sPath);
	return CreateDirectory(sPath, NULL);
}


BOOL CAVIFolderRec::GetCurFileName(SYSTEMTIME* pST, TCHAR *szFilePath)
{
	TCHAR sPath[MAX_PATH];
	TCHAR szFileName[MAX_PATH];

	if(m_szAppendName[0])
		_swprintf(szFileName, _T("\\%04d-%02d-%02d %02d_%02d_%02d_%s.avi"), 
			pST->wYear, pST->wMonth, pST->wDay, pST->wHour, pST->wMinute, pST->wSecond, m_szAppendName);
	else
		_swprintf(szFileName, _T("\\%04d-%02d-%02d %02d_%02d_%02d.avi"), 
			pST->wYear, pST->wMonth, pST->wDay, pST->wHour, pST->wMinute, pST->wSecond, m_szAppendName);

	_tcscpy(sPath, m_szCurFolder);
	_tcscat(sPath, szFileName);
	
	_tcscpy(szFilePath, sPath);

	return TRUE;
}

void CAVIFolderRec::SetAppendName(TCHAR* szAppendName)
{
	if(szAppendName){	
		_tcscpy(m_szAppendName, szAppendName);
	}else{
		m_szAppendName[0] = 0;
	}
}

BOOL CAVIFolderRec::Open(TCHAR* szFolder, BITMAPINFOHEADER* pbih, DWORD fccHandler, WAVEFORMATEX *pwfx, ULONG uRate, TCHAR* szAppendName, ULONG ChunkTime)
{
	TCHAR szFilePath[MAX_PATH];
	
	SYSTEMTIME st;
	GetLocalTime(&st);
	_tcscpy(m_szFolder, szFolder);
		
	//1. CreateFolder 
	if(!CreateAVIFolder(&st, m_szCurFolder)){
		return FALSE;
	}
	if(szAppendName){	
		_tcscpy(m_szAppendName, szAppendName);
	}else{
		m_szAppendName[0] = 0;
	}

	//2. Open File
	GetCurFileName(&st, szFilePath);
	if(!m_AVIRec.Open(szFilePath, pbih, fccHandler, pwfx, uRate)){
		return FALSE;
	}

	m_CurSystemTime	= st;
	m_FileChunkTime	= ChunkTime;
	m_StartTick		= GetTickCount(); 

	memcpy(&m_BitmpaInfoHeader, pbih, sizeof(BITMAPINFOHEADER));
	m_fccHandler	= fccHandler;
	if(pwfx){
		memcpy(&m_WaveFormat, pwfx, sizeof(WAVEFORMATEX));
	}else{
		memset(&m_WaveFormat, 0, sizeof(WAVEFORMATEX));
	}

	m_uRate		= uRate;

	m_bOpened	= TRUE;
	return TRUE;
}


BOOL CAVIFolderRec::WriteVideo(DWORD frameNo, LPVOID buf, LONG sizeImage, BOOL bKeyFrame, BOOL* bReopen)
{
	if(!m_bOpened) return FALSE;
	
	//1. Check change of day
	BOOL	bReOpen = FALSE;

	SYSTEMTIME st;
	GetLocalTime(&st);
	*bReopen	= FALSE;

	if(m_CurSystemTime.wDay != st.wDay){
		if(!CreateAVIFolder(&st, m_szCurFolder)){
			return FALSE;
		}

		m_CurSystemTime	= st;
		bReOpen			= TRUE;
	}

	//2. Check over chunk size
	ULONG CurTick = GetTickCount(); 
	if(bReOpen || ((CurTick-m_StartTick) > (m_FileChunkTime*1000))){
		TCHAR szFilePath[MAX_PATH];

		GetCurFileName(&st, szFilePath);
		WAVEFORMATEX	*pWaveFormat;
		if(m_WaveFormat.cbSize == 0) pWaveFormat = NULL;
		else pWaveFormat = &m_WaveFormat;

		m_AVIRec.Close();
		if(!m_AVIRec.Open(szFilePath, &m_BitmpaInfoHeader, m_fccHandler, pWaveFormat, m_uRate)){
			return FALSE;
		}

		//TRACE("Record File Opened [%s]\n ", szFilePath);	
		m_StartTick	= CurTick;
		frameNo		= 0;
		*bReopen	= TRUE;
	}

	//3. Write packet
	return m_AVIRec.WriteVideo(frameNo, buf, sizeImage, bKeyFrame);
}