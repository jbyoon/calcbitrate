#ifndef	__G711_H__
#define	__G711_H__

#ifdef	__cplusplus
extern "C" {
#endif

unsigned char	linear2alaw(short pcm_val);
unsigned char	linear2ulaw(short pcm_val);

short			alaw2linear(unsigned char a_val);
short			ulaw2linear(unsigned char u_val);

unsigned char	alaw2ulaw(unsigned char aval);
unsigned char	ulaw2alaw(unsigned char uval);

#ifdef	__cplusplus
}
#endif

#endif