
#include "StdAfx.h"

#define INITGUID
#include <Guiddef.h>

#ifdef _LIB
#define USING_UUCORE_LIB
#endif

#include "UMFUtil.h"
#include "UUCoreDec.h"


#define CHECK_UUCORE_READY()\
	if(NULL == m_pIUucore){\
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("UUCore not initialized\n"));\
		return FALSE;\
	}\

#define CHECK_VIDEODEC_READY()\
	if(NULL == m_pIUucore || NULL == m_hVideoCodec){\
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("UUCore Video decoder not Ready\n"));\
		return FALSE;\
	}\

#define CHECK_AUDIODEC_READY()\
	if(NULL == m_pIUucore || NULL == m_hAudioCodec){\
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("UUCore Audio decoder not Ready\n"));\
		return FALSE;\
	}\


CUUCoreDec::CUUCoreDec()
{
	m_bInit			= FALSE;
	m_hDLLUucore	= NULL;
	m_pIUucore		= NULL;

	m_hVideoCodec	= NULL;
	m_hAudioCodec	= NULL;

	m_VideoCodeId	= UU_CODEC_NONE;
	m_AudioCodeId	= UU_CODEC_NONE;
}


CUUCoreDec::~CUUCoreDec()
{
	if(m_bInit){
		CloseVideoCodec();
		CloseAudioCodec();

		if( m_pIUucore ) m_pIUucore->Release();
		m_pIUucore = NULL;

		if( m_hDLLUucore ) FreeLibrary( m_hDLLUucore );
		m_hDLLUucore = NULL;

		m_bInit = FALSE;
	}
}

BOOL CUUCoreDec::Init()
{
	if( m_bInit ){
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("UUCore already initialized\n"));
		return TRUE;
	}

	m_pIUucore = CreateUUCoreInstance();
	if( !m_pIUucore ){
		UMFTrace(UMF_TRACE_LEVEL_ERROR, _T("Failed to Create UUCore instance\n"));
		return FALSE;
	}

	m_bInit = TRUE;
	return TRUE;
}

BOOL	CUUCoreDec::GetVersion(ULONG Version[4])
{
	CHECK_UUCORE_READY();
	return m_pIUucore->UucGetVersion(Version);
}


BOOL	CUUCoreDec::GetModulePath(TCHAR* szModulePath)
{
	CHECK_UUCORE_READY();

	TCHAR szModulFilename[128];
	GetModuleFileName(m_hDLLUucore, szModulFilename, 128);

	lstrcpy(szModulePath, szModulFilename);
	return TRUE;
}


BOOL	CUUCoreDec::OpenVideoDecompressor(ULONG ulCodecType)
{
	CHECK_UUCORE_READY();
	if(ulCodecType == m_VideoCodeId) return TRUE;

	if(m_hVideoCodec)	CloseVideoCodec();
	m_hVideoCodec = m_pIUucore->UucOpenCodec(ulCodecType, FALSE);
	if(!m_hVideoCodec){
		UMFTrace(UMF_TRACE_LEVEL_ERROR, _T("Failed to call UucOpenCodec with Video Codec 0x%08\n"), ulCodecType);	
		return FALSE;
	}
	SetVideoDecompressParam(0, 0, UU_CSP_YUY2 );

	m_VideoCodeId = ulCodecType;
	return TRUE;
}


BOOL	CUUCoreDec::OpenAudioDecompressor(ULONG ulCodecType)
{
	CHECK_UUCORE_READY();
	if(ulCodecType == m_AudioCodeId) return TRUE;

	if(m_hAudioCodec)	CloseAudioCodec();
	m_hAudioCodec = m_pIUucore->UucOpenCodec(ulCodecType, FALSE);
	if(!m_hAudioCodec){
		UMFTrace(UMF_TRACE_LEVEL_ERROR, _T("Failed to call UucOpenCodec with Audio Codec 0x%08\n"), ulCodecType);	
		return FALSE;
	}

	return TRUE;
}


void	CUUCoreDec::CloseVideoCodec()
{
	if(m_pIUucore && m_hVideoCodec){
		m_pIUucore->UucCloseCodec(m_hVideoCodec);
		m_hVideoCodec = NULL;
	}
}


void	CUUCoreDec::CloseAudioCodec()
{
	if(m_pIUucore && m_hAudioCodec){
		m_pIUucore->UucCloseCodec(m_hAudioCodec);
		m_hAudioCodec = NULL;
	}
}


BOOL	CUUCoreDec::SetVideoDecompressParam(ULONG ulWidth, ULONG ulHeight, ULONG ulColorSpace)
{
	CHECK_VIDEODEC_READY();

	CodecProperty	tCodecProp;
	ZeroMemory( &tCodecProp, sizeof(tCodecProp) );

	tCodecProp.video.uWidth			= ulWidth;
	tCodecProp.video.uHeight		= ulHeight;
	tCodecProp.video.uColorSpace	= ulColorSpace;
	if(FALSE == m_pIUucore->UucSetParam(m_hVideoCodec, tCodecProp)){
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Failed to call SetVideoDecompressParam W[%d] H[%d] Format[%d]\n"),
			ulWidth, ulHeight, ulColorSpace);
		return FALSE;
	}
	return TRUE;
}


BOOL	CUUCoreDec::SetAudioDecompressParam(ULONG ulBitrate, ULONG ulSamplerate, ULONG ulChannels)
{
	CHECK_AUDIODEC_READY();

	CodecProperty	tCodecProp;
	ZeroMemory( &tCodecProp, sizeof(tCodecProp) );

	tCodecProp.audio.uBitrate		= ulBitrate;
	tCodecProp.audio.uSamplerate	= ulSamplerate;
	tCodecProp.audio.uChannels		= ulChannels;
	if(FALSE == m_pIUucore->UucSetParam( m_hAudioCodec, tCodecProp)){
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Failed to call SetAudioDecompressParam rate[%d] sample[%d] channel[%d]\n"),
			ulBitrate, ulSamplerate, ulChannels);
		return FALSE;
	}
	return TRUE;
}


BOOL	CUUCoreDec::VideoDecompress(DecodedVideo* pDecodedData, int* pDecodedLength, 
									BYTE* pCompressedData, int nCompressedLength, BOOL	bDeinterlaced)
{
	
	CHECK_VIDEODEC_READY();
	if( FALSE == m_pIUucore->UucDecodeVideo( m_hVideoCodec, pDecodedData,pDecodedLength,
										     pCompressedData,nCompressedLength, bDeinterlaced)){
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Failed to Decompress Video\n"));
		return FALSE;
	}
	return TRUE;
}


BOOL	CUUCoreDec::AudioDecompress(SHORT* pDecodedData, int* pDecodedDataBufLength, int* pDecodedLength,
									BYTE* pCompressedData, int nCompressedLength)
{
	CHECK_AUDIODEC_READY();
	if( FALSE == m_pIUucore->UucDecodeAudio(m_hAudioCodec, pDecodedData, pDecodedDataBufLength, pDecodedLength,
											pCompressedData, nCompressedLength)){
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Failed to Decompress Audio\n"));
		return FALSE;
	}
	return TRUE;
}


#ifdef _LIB
	#ifdef _DEBUG
		#pragma comment( lib, "../../LIB/UUCore/sUUCore_d" )
	#else
		#pragma comment( lib, "../../LIB/UUCore/sUUCore" )
	#endif
#else
//#pragma comment( lib, "../../LIB/UUCore/UUCore" )
#endif

IUucore*	CUUCoreDec::CreateUUCoreInstance()
{
	HRESULT hr;
	IUucore* pIUucore = NULL;
	
	HMODULE	hModule = LoadUMFLibrary(_T("UUCore.dll"));
	if(!hModule){
		UMFTrace(UMF_TRACE_LEVEL_ERROR, _T("Failed to Load UUCore.dll %0x08\n"), GetLastError());
		return NULL;
	}

	BOOL (FAR WINAPI*_UucCreateInstance)(void **pUnknown);
	FARPROC test_proc=GetProcAddress(hModule, "UucCreateInstance");
	if (test_proc) {
		*(FARPROC*)&_UucCreateInstance=test_proc;
	}

	IUnknown*	pUnknown = NULL;
	BOOL fRet = _UucCreateInstance( (PVOID*)&pUnknown );
	if( fRet == FALSE ){
		UMFTrace(UMF_TRACE_LEVEL_ERROR, _T("Failed to create UuCreateInstance %0x08\n"), GetLastError());
		return NULL;
	}

	//
	hr = pUnknown->QueryInterface( IID_IUucore, (VOID**)&pIUucore );
	if( !SUCCEEDED(hr) ){
		UMFTrace(UMF_TRACE_LEVEL_ERROR, _T("Failed to Queryinterface for UUCORE %0x08\n"), GetLastError());
		return NULL;
	}

	return pIUucore;	


}

/*
#else
typedef BOOL(WINAPI* UUCREATEINSTANCE)( VOID** );
IUucore*	CUUCoreDec::CreateUUCoreInstanc()
{
	HRESULT hr;
	IUucore* pIUucore = NULL;
	m_hDLLUucore = LoadLibrary( _T("Uucore.DLL") );
	if( m_hDLLUucore == NULL ){
		UMFTrace(UMF_TRACE_LEVEL_ERROR, _T("Can not load UUCore.DLL Error 0x%08X\n"), GetLastError());
		return NULL;
	}

	UUCREATEINSTANCE	fnUuCreateInstance = 
			(UUCREATEINSTANCE)GetProcAddress( m_hDLLUucore, "UuCreateInstance" );
	if( fnUuCreateInstance == NULL ){
		UMFTrace(UMF_TRACE_LEVEL_ERROR, _T("Can not Get UuCreateInstance 0x%08X\n"), GetLastError());
		return NULL;
	}

	//
	IUnknown*	pUnknown = NULL;
	BOOL fRet = fnUuCreateInstance( (PVOID*)&pUnknown );
	if( fRet == FALSE ){
		UMFTrace(UMF_TRACE_LEVEL_ERROR, _T("Can not create UuCreateInstance 0x%08X\n"), GetLastError());
		return NULL;
	}

	//
	hr = pUnknown->QueryInterface( IID_IUucore, (VOID**)&pIUucore );
	if( !SUCCEEDED(hr) ){
		UMFTrace(UMF_TRACE_LEVEL_ERROR, _T("Fail Queryinterface for UUCORE 0x%08X\n"), GetLastError());
		return NULL;
	}

	return pIUucore;
}
#endif
*/