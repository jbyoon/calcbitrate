#include "stdafx.h"
#include "UMFXMLUtil.h"
#include "XMLUtils.h"
#include <atlstr.h> 

#ifndef TRACE
#include <atlbase.h>
#define TRACE AtlTrace
#endif



#define CHECK_XML_RETURN_AND_ASSIGN(VAL)	\
	if(!m_pXML){	\
		TRACE(_T("XML not assigned before\n"));\
		return VAL;\
	}\
	IXMLDOMNode* pXML = (IXMLDOMNode*)m_pXML;\



CUMFXMLUtil::CUMFXMLUtil(PXML pXML)
{
	CoInitialize(NULL);
	m_pXMLDoc	= NULL;
	m_pXML		= pXML;
}


CUMFXMLUtil::CUMFXMLUtil()
{
	CoInitialize(NULL);
	m_pXMLDoc	= NULL;
	m_pXML		= NULL;
}


CUMFXMLUtil::~CUMFXMLUtil()
{
	CoUninitialize();
}


BOOL	CUMFXMLUtil::Load(TCHAR *szFileName, TCHAR *szRootTag)
{
	IXMLDOMDocument2*	pXMLDoc;// = new IXMLDOMDocument2;

	HRESULT hr;
	hr = CoCreateInstance(__uuidof(DOMDocument) , NULL, CLSCTX_INPROC_SERVER, __uuidof(IXMLDOMDocument2), (void**)&pXMLDoc); 
	if(S_OK != hr){
		TRACE(_T("Can not Create XMLDOMDocument Install MSXML4.0"));
		return FALSE;
	}

	m_pXMLDoc = pXMLDoc;

	pXMLDoc->resolveExternals = VARIANT_TRUE;
	pXMLDoc->async = VARIANT_FALSE;
	pXMLDoc->validateOnParse = VARIANT_TRUE;

	VARIANT_BOOL varRet = VARIANT_FALSE;

	varRet = pXMLDoc->load(szFileName);
	if (varRet == VARIANT_FALSE){
		return FALSE;
	}

	IXMLDOMNodeListPtr	pRootXMLList;
	bool bRet = true;

	try{
		pRootXMLList	= pXMLDoc->documentElement->getElementsByTagName(szRootTag);
		IXMLDOMNode* pNode;
		pRootXMLList->get_item(0, &pNode);

		m_pXML = (PXML)pNode;
		
	}catch(...)
	{
		bRet = false;
	}

	_tcscpy_s(m_szXMLPath, szFileName);
	return TRUE;
}


BOOL	CUMFXMLUtil::LoadXML(char *szXML, int nLength, TCHAR *szRootTag)
{
	if(!m_pXMLDoc){
		HRESULT hr;
		IXMLDOMDocument2*	pXMLDoc;
		hr = CoCreateInstance(__uuidof(DOMDocument) , NULL, CLSCTX_INPROC_SERVER, __uuidof(IXMLDOMDocument2), (void**)&pXMLDoc); 
		if(S_OK != hr){
			TRACE(_T("Can not Create XMLDOMDocument Install MSXML4.0"));
			return FALSE;
		}
		m_pXMLDoc = pXMLDoc;
	}

	VARIANT_BOOL vb;
	USES_CONVERSION;
	IXMLDOMDocument2Ptr	XMLDocPtr;
	BOOL bRet = TRUE;

	XMLDocPtr = (IXMLDOMDocument2*)m_pXMLDoc;
	szXML[nLength] = '\0';
	BSTR bstr = SysAllocString( A2W( (char *)szXML ) );

	vb = XMLDocPtr->loadXML( bstr);

	if( VARIANT_TRUE == vb ){
		IXMLDOMNodeListPtr	pRootXMLList;
		
		try{
			pRootXMLList	= XMLDocPtr->documentElement->getElementsByTagName(szRootTag);
			IXMLDOMNode* pNode;
			pRootXMLList->get_item(0, &pNode);

			m_pXML = (PXML)pNode;
			
		}catch(...)
		{
			bRet = FALSE;
		}
	}else {
		MSXML2::IXMLDOMParseErrorPtr pErr = XMLDocPtr->parseError;

		CString strLine, sResult;
		strLine.Format(_T(" ( line %u, column %u )"), pErr->Getline(), pErr->Getlinepos());
		// Return validation results in message to the user.
		if (pErr->errorCode != S_OK)
		{
			sResult = CString("Validation failed on ") +
			 CString ("\n=====================") +
			 CString("\nReason: ") + CString( (char*)(pErr->Getreason())) +
			 CString("\nSource: ") + CString( (char*)(pErr->GetsrcText())) +
			 strLine + CString("\n");

			ATLTRACE(sResult);
		}
		bRet = FALSE;
	}

	SysFreeString( bstr );
	return bRet;
}


BOOL	CUMFXMLUtil::Save(TCHAR *szFileName)
{
	if(!m_pXMLDoc){
		TRACE(_T("IXMLDOMDocument2 not loaded before\n"));
		return FALSE;
	}

	HRESULT hr;
	IXMLDOMDocument2*	pXMLDoc = (IXMLDOMDocument2*)m_pXMLDoc;

	if (NULL == szFileName) {
		hr = pXMLDoc->save(m_szXMLPath);
	} else {
		hr = pXMLDoc->save(szFileName);
	}

	if(S_OK != hr){
		return FALSE;
	}
	return TRUE;
}


BOOL	CUMFXMLUtil::SetXML(PXML pXmlElement)
{
	if(NULL == pXmlElement) return FALSE;
	m_pXML = pXmlElement;
	return TRUE;
}

	
TCHAR*	CUMFXMLUtil::GetNodeName()
{
	CHECK_XML_RETURN_AND_ASSIGN(NULL);

	IXMLDOMElementPtr XMLElement;
	XMLElement = (IXMLDOMNodePtr)pXML;

	return XMLElement->GetnodeName();
}


TCHAR*	CUMFXMLUtil::GetNodeValue()
{
	CHECK_XML_RETURN_AND_ASSIGN(NULL);

	IXMLDOMElementPtr XMLElement;
	XMLElement = (IXMLDOMNodePtr)pXML;
	
	//return XMLElement->GetnodeValue();
	return XMLElement->Gettext();
}


PXML	CUMFXMLUtil::GetChildXML(TCHAR* szItem)
{
	CHECK_XML_RETURN_AND_ASSIGN(NULL);
	
	IXMLDOMElementPtr XMLElement;
	XMLElement = (IXMLDOMNodePtr)pXML;

	try{
		IXMLDOMNodeListPtr	XMLNodeList;
		XMLNodeList		= XMLElement->getElementsByTagName(szItem);
		IXMLDOMNode* pXMLNode;
		XMLNodeList->get_item(0, &pXMLNode);
		return (PXML)pXMLNode;
	}catch(...){
		TRACE(_T("XML Fail Get Child XML %s\n"), szItem);
	}

	return NULL;
}

PXML	CUMFXMLUtil::GetFirstChild()
{
	CHECK_XML_RETURN_AND_ASSIGN(NULL);
	
	IXMLDOMElementPtr XMLElement;
	//XMLElement = (IXMLDOMElementPtr)pXML;
	
	XMLElement = pXML->firstChild;
	if(XMLElement){
		IXMLDOMElement* pXMLElement;
		XMLElement->QueryInterface(__uuidof(IXMLDOMElement), (void**)&pXMLElement);
		return pXMLElement;
	}

	return NULL;
}

PXML	CUMFXMLUtil::GetNextChild(PXML pPrevChild)
{
	if(!pPrevChild){
		TRACE(_T("PrevChild not assigned\n"));
		return NULL;
	}

	IXMLDOMNode* pXML = (IXMLDOMElement*)pPrevChild;

	IXMLDOMElementPtr XMLElement;
	XMLElement = pXML->nextSibling;
	if(XMLElement){
		IXMLDOMElement* pXMLElement;
		XMLElement->QueryInterface(__uuidof(IXMLDOMElement), (void**)&pXMLElement);
		return pXMLElement;
	}

	return NULL;
}


TCHAR*	CUMFXMLUtil::GetXMLData()
{
	CHECK_XML_RETURN_AND_ASSIGN(NULL);
	return pXML->xml;
}



DWORD	CUMFXMLUtil::GetAttrValueHex(TCHAR* attrName)
{
	CHECK_XML_RETURN_AND_ASSIGN(0);
	return ::GetAttrValueHex(pXML, attrName);
}

long	CUMFXMLUtil::GetAttrValueNum(TCHAR* attrName)
{
	CHECK_XML_RETURN_AND_ASSIGN(0);
	return ::GetAttrValueNum(pXML, attrName);
}

double	CUMFXMLUtil::GetAttrValueFloat(TCHAR* attrName)
{
	CHECK_XML_RETURN_AND_ASSIGN(0);
	return ::GetAttrValueFloat(pXML, attrName);
}

BOOL	CUMFXMLUtil::GetAttrValueString(TCHAR* attrName, TCHAR* buffer, DWORD count)
{
	CHECK_XML_RETURN_AND_ASSIGN(0);
	return ::GetAttrValueString(pXML, attrName, buffer, count);
}

BOOL	CUMFXMLUtil::GetAttrValueBinData(TCHAR* attrName, int count, TCHAR* data)
{
	CHECK_XML_RETURN_AND_ASSIGN(0);
	::GetAttrValueBinData(pXML, attrName, count, data);
	return TRUE;
}

BOOL	CUMFXMLUtil::SetAttrValueHex(TCHAR* attrName,DWORD v)
{
	CHECK_XML_RETURN_AND_ASSIGN(0);
	::SetAttrValueHex(pXML, attrName, v);
	return TRUE;
}

BOOL	CUMFXMLUtil::SetAttrValueNum(TCHAR* attrName,long v)
{
	CHECK_XML_RETURN_AND_ASSIGN(0);
	::SetAttrValueNum(pXML, attrName, v);
	return TRUE;
}

BOOL	CUMFXMLUtil::SetAttrValueFloat(TCHAR* attrName,double v)
{
	CHECK_XML_RETURN_AND_ASSIGN(0);
	::SetAttrValueFloat(pXML, attrName, v);
	return TRUE;
}

BOOL	CUMFXMLUtil::SetAttrValueString(TCHAR* attrName,TCHAR* buffer)
{
	CHECK_XML_RETURN_AND_ASSIGN(0);
	::SetAttrValueString(pXML, attrName, buffer);
	return TRUE;
}

BOOL	CUMFXMLUtil::SetAttrValueBinData(TCHAR* attrName,int count, TCHAR* data)
{
	CHECK_XML_RETURN_AND_ASSIGN(0);
	::SetAttrValueBinData(pXML, attrName, count, data);
	return TRUE;
}


PXML	CUMFXMLUtil::CreateElement(TCHAR* szTagName)
{
	IXMLDOMDocument2Ptr	XMLDocPtr;
	IXMLDOMElementPtr ChildXML;

	if(m_pXMLDoc){
		XMLDocPtr = (IXMLDOMDocument2 *)m_pXMLDoc;
	}else{
		CHECK_XML_RETURN_AND_ASSIGN(0);
		IXMLDOMElementPtr XMLElement;
		XMLElement = (IXMLDOMNodePtr)pXML;
		XMLDocPtr = XMLElement->GetownerDocument();
	}

	if(!XMLDocPtr) return NULL;

	ChildXML = XMLDocPtr->createElement(szTagName);

	IXMLDOMElement* pNewElement;
	ChildXML->QueryInterface(__uuidof(IXMLDOMElement), (void**)&pNewElement);
	return pNewElement;
}


BOOL	CUMFXMLUtil::AppendChild(PXML pChildXML)
{
	CHECK_XML_RETURN_AND_ASSIGN(0);
	pXML->appendChild((IXMLDOMNode*)pChildXML);

	return TRUE;
}

