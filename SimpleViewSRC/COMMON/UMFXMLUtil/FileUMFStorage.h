#ifndef __FILE_UMSF_STORAGE_H__
#define __FILE_UMSF_STORAGE_H__

#include "IBaseUMFStorage.h"


//Base UMF Stroage for XML storage
class CFileUMFStorage : public IBaseUMFStorage
{
public:
	CFileUMFStorage():m_pXMLDoc(NULL),m_pXML(NULL){}
	~CFileUMFStorage();

	eUMF_STORAGE_TYPE 	GetType(){return UMF_STORAGE_FILE;}
	PXML GetXML(){return m_pXML;}
	
	HRESULT		Load(TCHAR * szFilename, TCHAR* RootTag = _T("VASetting"));
	HRESULT		Save(TCHAR * szFilename = NULL);


private:

	PXML	m_pXMLDoc;
	PXML	m_pXML;

	TCHAR	m_szConfFileName[MAX_PATH];
};




#endif
