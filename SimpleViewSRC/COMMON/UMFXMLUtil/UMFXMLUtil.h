#ifndef __UMF_XML_UTIL_H__
#define __UMF_XML_UTIL_H__

#ifndef __PXML__

typedef void*	PXML;
#define __PXML__

#endif

class CUMFXMLUtil
{
public:
	CUMFXMLUtil(PXML pXML);
	CUMFXMLUtil();

	~CUMFXMLUtil();
	
	BOOL	Load(TCHAR *sz, TCHAR *szRootTag = _T("APPSetting"));
	BOOL	Save(TCHAR *sz = NULL);

	BOOL	LoadXML(char *szXML, int size, TCHAR *szRootTag = _T("APPSetting"));

	BOOL	SetXML(PXML pXmlElement);
	PXML	GetXML() {return (PXML)m_pXML;}

	TCHAR*	GetNodeName();
	TCHAR*	GetNodeValue();

	PXML	GetChildXML(TCHAR* szItem);
	PXML	GetFirstChild();
	PXML	GetNextChild(PXML pPrevChild);
	
	DWORD	GetAttrValueHex(TCHAR* attrName);
	long	GetAttrValueNum(TCHAR* attrName);
	double	GetAttrValueFloat(TCHAR* attrName);
	//count : is not buffer length 
	BOOL	GetAttrValueString(TCHAR* attrName, TCHAR* buffer, DWORD count);
	BOOL	GetAttrValueBinData(TCHAR* attrName, int count, TCHAR* data);
	
	BOOL	SetAttrValueHex(TCHAR* attrName,DWORD v);
	BOOL	SetAttrValueNum(TCHAR* attrName,long v);
	BOOL	SetAttrValueFloat(TCHAR* attrName,double v);
	BOOL	SetAttrValueString(TCHAR* attrName,TCHAR* buffer);
	BOOL	SetAttrValueBinData(TCHAR* attrName,int count, TCHAR* data);

	PXML	CreateElement(TCHAR* szTagName);
	BOOL	AppendChild(PXML pChildXML);

	//Show XML content.
	TCHAR*	GetXMLData();

private:

	PXML	m_pXMLDoc;
	TCHAR	m_szXMLPath[MAX_PATH];
	PXML	m_pXML;
};


#endif