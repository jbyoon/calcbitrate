#include "stdafx.h"
#include "XMLUtils.h"
#include "FileUMFStorage.h"




CFileUMFStorage::~CFileUMFStorage()
{
	if(m_pXMLDoc){
		IXMLDOMDocument2* pXMLDoc = (IXMLDOMDocument2*)m_pXMLDoc;
		pXMLDoc->Release();
		m_pXMLDoc = NULL;

	}
}


HRESULT		CFileUMFStorage::Load(TCHAR * szFilename, TCHAR* RootTag)
{
	IXMLDOMDocument2*	pXMLDoc;

	HRESULT hr;
	hr = CoCreateInstance(__uuidof(DOMDocument) , NULL, CLSCTX_INPROC_SERVER, __uuidof(IXMLDOMDocument2), (void**)&pXMLDoc); 
	if(S_OK != hr){
		TRACE(_T("Can not Create XMLDOMDocument Install MSXML2.0"));
		return S_FALSE;
	}

	m_pXMLDoc = pXMLDoc;

	pXMLDoc->resolveExternals = VARIANT_TRUE;
	pXMLDoc->async = VARIANT_FALSE;
	pXMLDoc->validateOnParse = VARIANT_TRUE;


	_variant_t varXml(szFilename);
	_variant_t varOut((bool)TRUE);

	varOut = pXMLDoc->load(szFilename);
	if ((bool)varOut == FALSE){
		TRACE(_T("Can not Read XML File %s\n"), szFilename);
		return S_FALSE;
	}

	IXMLDOMNodeListPtr	pRootXMLList;
	IXMLDOMElementPtr	pRootXML, pEngineXML;

	DWORD EngCnt = 0;
	bool bRet = true;
	try{
		pRootXMLList	= pXMLDoc->documentElement->getElementsByTagName(RootTag);
		IXMLDOMNode* pNode;
		pRootXMLList->get_item(0, &pNode);
		m_pXML = (PXML)pNode;
	}catch(...)
	{
		bRet = false;
	}
    
	if(bRet){
		_tcscpy_s(m_szConfFileName,szFilename);
	}

	return bRet?S_OK:S_FALSE;

}



HRESULT		CFileUMFStorage::Save(TCHAR * szFilename)
{
	IXMLDOMDocument2*	pXMLDoc;
	HRESULT hr;

	if(!m_pXMLDoc){
		TRACE(_T("CFileUMFStorage::Save Opened Before\n"));
		return S_FALSE;
	}

	if(!szFilename) szFilename = m_szConfFileName;
	
	pXMLDoc = (IXMLDOMDocument2*)m_pXMLDoc;
	hr = pXMLDoc->save(szFilename);
	if(S_OK != hr){
		TRACE(_T("Can not Read XML File %s\n"), szFilename);
		return hr;
	}
	return S_OK;
}

