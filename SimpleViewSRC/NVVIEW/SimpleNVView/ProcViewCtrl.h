#pragma once

#include "MultiViewRender.h"
#include "resource.h"


class CProcViewCtrl : public CDialog
{
	DECLARE_DYNAMIC(CProcViewCtrl)
	enum {MAX_UI_VIEW = 1};

public:
	CProcViewCtrl();
	virtual ~CProcViewCtrl();

	BOOL Setup();
	void Endup();
	void ClearAll();
		
	void OnChangeClientRect();

	enum { IDD = IDD_DIALOG_ZONESETTING };

//IVCAConfigureObserver	
//	void FireOnEvent(DWORD uiEvent, DWORD uiContext){}

	void ProcessImageNVAMetaData(DWORD EngId, BYTE *pImage, BITMAPINFOHEADER *pbm, BYTE *pMetaData, int iMateDataLen);
	CMultiViewRender *GetMultiViewRender(){return &m_MultiViewRender;}

	BITMAPINFOHEADER*	GetSourceBIH(){return &m_SourceBIH;}

protected:
	DECLARE_MESSAGE_MAP()
	
	virtual void	OnSize(UINT nType, int cx, int cy);
	virtual void	OnPaint();


private:
	
	BITMAPINFOHEADER	m_SourceBIH;
	
		
};


