// SimpleNVView.h : main header file for the SimpleNVView DLL
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols

// CSimpleNVView
// See SimpleNVView.cpp for the implementation of this class
//
class CSimpleNVView : public CWinApp
{
public:
	CSimpleNVView();
// Overrides
public:
	virtual BOOL InitInstance();
	DECLARE_MESSAGE_MAP()
};



