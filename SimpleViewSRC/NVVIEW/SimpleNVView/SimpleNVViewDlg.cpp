// SimpleNVView.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
//#include "Resource.h"
#include "SimpleNVViewDlg.h"
#include "UMFXMLUtil/UMFXMLUTil.h"
#include "UMFUtil.h"
#include "RTSPClientAPI/IRTSPClientAPI.h"
#include "yuv2rgb2.h"
#include "ccvt/ccvt.h"
#include <math.h>
#include "version.h"

//#define SAVE_BMP_MAX_IMG_TO_SAVE	100
//#define SAVE_BMP_ROOT_PATH	_T("d:\\temp\\DecodedBmp")

#define SAVE_BMP_INFO_FILENAME	_T("info.txt")
#define GOP_SIZE			30
#define ONE_MINUTE			60
#define NUM_BARS_TO_GRAPH	(GOP_SIZE * 2 + 3)
#define NUM_FRAMES_TO_HOLD	(GOP_SIZE * (ONE_MINUTE + 5))

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

struct BMP_SAVE_INFO
{
	BOOL		bSaveToBmp;
	TCHAR		PathToSaveBmp[MAX_PATH];
	UINT		TotalFramesToSave;
	UINT		bSaveYuv420p;
};

struct STATISTICS_SAVE_INFO
{
	BOOL		bSaveStatistics;
	UINT		timeWarmup;	// in sec
	UINT		timeCollecting;	// in sec
	TCHAR		filenameToSaveStatistics[MAX_PATH];
	TCHAR		filenameToSaveStreamInfo[MAX_PATH];
	TCHAR		filenameToSaveStatisticsBmp[MAX_PATH];
	double		AccumulatedMotionRate; // 1.0
};

struct IMATEST_SAVE_INFO
{
	BOOL		bSaveImatestImage;
	UINT		snapshotInterval;	// in sec
	TCHAR		pathToSaveImatestBmp[MAX_PATH];
};


STATISTICS_SAVE_INFO gStatisticsSaveInfo;
BMP_SAVE_INFO gBmpSaveInfo;
IMATEST_SAVE_INFO gImatestSaveInfo;


//IMPLEMENT_DYNAMIC(CSimpleNVViewDlg, CDialog)
CSimpleNVViewDlg::CSimpleNVViewDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSimpleNVViewDlg::IDD, pParent)
{
	m_hWndParent	= NULL;
	m_bCreated		= FALSE;
	m_Status		= INIT_STATUS;
	m_Pause			= FALSE;
	m_pBufDecodedImg = NULL;
	m_pBufJpegImg = NULL;
	m_bIsBmpPathCreated = FALSE;
	m_pFrameInfo = new FRAME_INFO[NUM_FRAMES_TO_HOLD];
	ZeroMemory(m_pFrameInfo, sizeof(FRAME_INFO)*NUM_FRAMES_TO_HOLD);
	ZeroMemory(&gStatisticsSaveInfo, sizeof(gStatisticsSaveInfo));
	ZeroMemory(&gBmpSaveInfo, sizeof(gBmpSaveInfo));
	ZeroMemory(&gImatestSaveInfo, sizeof(gImatestSaveInfo));
	m_bGotStatisticsSourceInfo = FALSE;
	m_hStreamFile = INVALID_HANDLE_VALUE;
	m_hStatFile = INVALID_HANDLE_VALUE;;
	m_nStatCount = 0;
	m_bCheckComplexity = FALSE;
	m_bCheckMotionrate = FALSE;
	InitializeCriticalSection(&m_CS); 
}

CSimpleNVViewDlg::~CSimpleNVViewDlg()
{
	DeleteCriticalSection(&m_CS);

	delete m_pFrameInfo;
}

void CSimpleNVViewDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CSimpleNVViewDlg, CDialog)
	ON_WM_SIZE()
	ON_WM_LBUTTONDOWN()
	ON_WM_DRAWITEM()
	ON_WM_PAINT()
	ON_WM_CLOSE()
END_MESSAGE_MAP()

BOOL CSimpleNVViewDlg::OnInitDialog()
{
	__super::OnInitDialog();
	OnChangeClientRect();
	//::SetWindowText(m_hWndParent, _T("I am a"));
	//CWnd* pWnd = (CWnd*)m_pNVFrame;
	//pWnd->SetWindowText(_T("I am "));

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}


BOOL CSimpleNVViewDlg::Create(HWND hWndParent, IBaseNVFrame *pNVFrame)
{
//	if(NULL == pConfSet) return NULL;
//	if(pConfSet->GetType() != IBaseConfSet::VCA_CONFSET)return NULL;

	BOOL bResult;

	m_pBufDecodedImg = new char[SAVE_BMP_BUF_SIZE];
	memset(m_pBufDecodedImg, 0x7f, SAVE_BMP_BUF_SIZE);	// fill in grey color in YUY2
	InitComplexity();

	m_pNVFrame	= pNVFrame;
	m_hWndParent	= hWndParent;

	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	//1. Create Dialog
	bResult = CDialog::Create(CSimpleNVViewDlg::IDD, CWnd::FromHandle(hWndParent));
	if(!bResult){
		TRACE(_T("Can not create CSimpleNVViewDlg \n"));
		return NULL;
	}
	m_pNVFrame	= pNVFrame;
	m_hWndParent	= hWndParent;

	if(!m_MultiViewRender.Setup(m_hWnd, MAX_UI_VIEW)){
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Can not Setup Viewer \n"));
		return FALSE;
	}

	if(!m_UUCoreLib.Init()){
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Can not Setup UUCore\n"));
		return FALSE;
	}

	m_UMFDrawUtil.Init(m_MultiViewRender.GetUMFRender());
	m_UMFDrawUtil.InitFont(0,NULL,9,UMF_FONT_BOLD);
	m_UMFDrawUtil.InitFont(1,NULL,9,UMF_FONT_BOLD);

	m_MultiViewRender.SetViewMode(CMultiViewRender::VIEW_1);
	m_bCreated		= TRUE;

	
	return TRUE;
}

void CSimpleNVViewDlg::CloseOpenFiles()
{
	if(m_bGotStatisticsSourceInfo){
		if(m_hStreamFile != INVALID_HANDLE_VALUE){
			CloseHandle(m_hStreamFile);
		}
		if(m_hStatFile != INVALID_HANDLE_VALUE){
			WriteStatFile();
			CloseHandle(m_hStatFile);
		}
	}
}

void CSimpleNVViewDlg::Destroy()
{
	CloseComplexity();
	if (m_pBufDecodedImg) {
		delete [] m_pBufDecodedImg;
		m_pBufDecodedImg = NULL;
	}

	//여기서 stream파일을 close한다.
	CloseOpenFiles();

	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	if(m_bCreated){
		m_MultiViewRender.Endup();
		m_bCreated = FALSE;
		DestroyWindow();
				
		delete this;
	}

}

BOOL CSimpleNVViewDlg::Show(ULONG EngId, BOOL bShow, DISPLAY_STYLE Style)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if(bShow){
		if(IBaseNVView::CHILD & Style) {
			//ModifyStyle(WS_POPUP|WS_BORDER|WS_CAPTION|DS_MODALFRAME, WS_CHILD);
			ModifyStyle(WS_POPUP|WS_BORDER|WS_CAPTION|WS_DLGFRAME, WS_CHILD);
			ModifyStyleEx(0, WS_EX_DLGMODALFRAME );
			SetParent(CWnd::FromHandle(m_hWndParent));
			CRect rcParent;
			::GetClientRect(m_hWndParent, &rcParent);
			MoveWindow(0,0,rcParent.Width(),rcParent.Height());
		}else if(IBaseNVView::POPUP & Style){
			ModifyStyle(WS_CHILD, WS_POPUP|WS_BORDER|WS_CAPTION|WS_DLGFRAME);
			ModifyStyleEx(WS_EX_DLGMODALFRAME , 0);
			SetParent(NULL);
		}

	
		ShowWindow(SW_SHOW);
		OnChangeClientRect();

		m_Status	= RUN_STATUS;
	}else{
		ShowWindow(SW_HIDE);
		m_Status	= INIT_STATUS;
	}

	m_EngId = EngId;
	return  bShow;
}



struct VIDEO_SOURCE_INFO
{
	UINT		SourceType;
	eUMF_COLORFORMAT ColorFormat;
	TCHAR		VideoCodec[8];
	UINT		ImageSizeW;
	UINT		ImageSizeH;
	TCHAR		RtspUrl[MAX_PATH];
	TCHAR		AudioCodec[8];
	UINT		AudioSampleRate;
	UINT		AudioSampleBits;
	BOOL		bNoVideoDisplay;
};

VIDEO_SOURCE_INFO s_sourceInfo = {0,};

BOOL	GetSourceInfoFromXml(PXML pSrcXML, VIDEO_SOURCE_INFO* pSourceInfo)
{
	CUMFXMLUtil	SourceInfoXML(pSrcXML);

	try{
//		UINT len = _countof(pSourceInfo->VideoCodec);
		pSourceInfo->ColorFormat	= (eUMF_COLORFORMAT)SourceInfoXML.GetAttrValueHex(_T("ColorFormat"));
		SourceInfoXML.GetAttrValueString(_T("VideoCodec"), pSourceInfo->VideoCodec, _countof(pSourceInfo->VideoCodec));
		SourceInfoXML.GetAttrValueString(_T("RTSPURL"), pSourceInfo->RtspUrl, _countof(pSourceInfo->RtspUrl));
		pSourceInfo->ImageSizeW		= SourceInfoXML.GetAttrValueNum(_T("ImageSizeW"));
		pSourceInfo->ImageSizeH		= SourceInfoXML.GetAttrValueNum(_T("ImageSizeH"));
		SourceInfoXML.GetAttrValueString(_T("AudioCodec"), pSourceInfo->AudioCodec, _countof(pSourceInfo->AudioCodec));
		pSourceInfo->AudioSampleRate		= SourceInfoXML.GetAttrValueNum(_T("AudioSampleRate"));
		pSourceInfo->AudioSampleBits		= SourceInfoXML.GetAttrValueNum(_T("AudioSampleBits"));
		pSourceInfo->bNoVideoDisplay		= SourceInfoXML.GetAttrValueNum(_T("NoVideoDisplay"));	// 1=No diaplay, 0=Display
	}catch(...)
	{
		return FALSE;
	}
	return TRUE;
}

// JSS: Hacked into here. This needs to be placed somewhere it reads from XML. But I don't know where I put in.
BOOL GetBmpSaveInfoFromXml(PXML pSrcXML, BMP_SAVE_INFO* pSourceInfo)
{
	CUMFXMLUtil	SourceInfoXML(pSrcXML);
	int testValue = -1;

	try{
		pSourceInfo->bSaveToBmp = SourceInfoXML.GetAttrValueNum(_T("bSaveToBmp"));
		pSourceInfo->TotalFramesToSave	= SourceInfoXML.GetAttrValueNum(_T("TotalFramesToSave"));
		pSourceInfo->bSaveYuv420p = SourceInfoXML.GetAttrValueNum(_T("bSaveYuv420p"));
		SourceInfoXML.GetAttrValueString(_T("PathToSaveBmp"), pSourceInfo->PathToSaveBmp, MAX_PATH-1);
	}catch(...)
	{
		return FALSE;
	}
	return TRUE;
}

BOOL GetStatisticesSaveInfoFromXml(PXML pSrcXML, STATISTICS_SAVE_INFO* pSourceInfo)
{
	CUMFXMLUtil	SourceInfoXML(pSrcXML);
	int testValue = -1;

	try{
		pSourceInfo->bSaveStatistics = SourceInfoXML.GetAttrValueNum(_T("bSaveStatistics"));
		pSourceInfo->timeWarmup	= SourceInfoXML.GetAttrValueNum(_T("timeWarmup"));
		pSourceInfo->timeCollecting	= SourceInfoXML.GetAttrValueNum(_T("timeCollecting"));
		SourceInfoXML.GetAttrValueString(_T("filenameToSaveStatistics"), pSourceInfo->filenameToSaveStatistics, MAX_PATH-1);
		SourceInfoXML.GetAttrValueString(_T("filenameToSaveStreamInfo"), pSourceInfo->filenameToSaveStreamInfo, MAX_PATH-1);
		SourceInfoXML.GetAttrValueString(_T("filenameToSaveStatisticsBmp"), pSourceInfo->filenameToSaveStatisticsBmp, MAX_PATH-1);
		TCHAR		str[MAX_PATH]=_T("");
		SourceInfoXML.GetAttrValueString(_T("AccumulatedMotionRate"), str, MAX_PATH-1);
		pSourceInfo->AccumulatedMotionRate = _wtof(str);
		if(pSourceInfo->AccumulatedMotionRate<=0) pSourceInfo->AccumulatedMotionRate=1.0; // fps
	}catch(...)
	{
		return FALSE;
	}
	return TRUE;
}

BOOL GetImatestSaveInfoFromXml(PXML pSrcXML, IMATEST_SAVE_INFO* pSourceInfo)
{
	CUMFXMLUtil	SourceInfoXML(pSrcXML);
	int testValue = -1;

	try{
		pSourceInfo->bSaveImatestImage = SourceInfoXML.GetAttrValueNum(_T("bSaveImatestImage"));
		pSourceInfo->snapshotInterval	= SourceInfoXML.GetAttrValueNum(_T("snapshotInterval"));
		SourceInfoXML.GetAttrValueString(_T("pathToSaveImatestBmp"), pSourceInfo->pathToSaveImatestBmp, MAX_PATH-1);

	}catch(...)
	{
		return FALSE;
	}
	return TRUE;
}

void CSimpleNVViewDlg::ChangeVideoSource(DWORD EngId, PXML pSoruceInfoXML)
{
	if(!pSoruceInfoXML) return;

	VIDEO_SOURCE_INFO SourceInfo;
	GetSourceInfoFromXml(pSoruceInfoXML, &SourceInfo);

	// We refers SourceInfo almost every frame in several routines. We saves in the global variable, which is not a good idea though.
	CopyMemory(&s_sourceInfo, &SourceInfo, sizeof(SourceInfo));

	m_MultiViewRender.ChangeSourceInfo(EngId, SourceInfo.ImageSizeW, SourceInfo.ImageSizeH, SourceInfo.ColorFormat);

	CString title;
	title.Format(_T("%s %s %s"), PROGRAM_NAME, VERSION, SourceInfo.RtspUrl);

	::SetWindowText(m_hWndParent, title);
}


BOOL CSimpleNVViewDlg::ChangeMMSourceInfo(DWORD EngId, PXML pSoruceInfoXML)
{
	EnterCriticalSection(&m_CS); 
	ChangeVideoSource(EngId, pSoruceInfoXML);
	LeaveCriticalSection(&m_CS);
	return TRUE;
}


void CSimpleNVViewDlg::ProcessMMData(ULONG EngId, SRCTYPE srcType, BYTE *pMMData, UINT MMDataLen, FILETIME *pTimestamp)
{
	if(!m_bCreated) return;
	if(!IsVideoSource(srcType)) return;
	if(m_Pause) return;

	SYSTEMTIME systemTime;
	wchar_t wcsText[MAX_PATH];
	POINT pt = {0,0};
	POINT pt2= {0,10};
	RECT rc = {100,100,200,200};

	static FILETIME Timestamp_pre;
	static FILETIME Timestamp_pre_ForFrame;
	static ULONG freezing_cnt;

	// time check

	if ( !((Timestamp_pre_ForFrame.dwHighDateTime == 0) && (Timestamp_pre_ForFrame.dwLowDateTime == 0)) )
	{
		ULONGLONG qwResult_pre_ForFrame = (((ULONGLONG) Timestamp_pre_ForFrame.dwHighDateTime) << 32) + Timestamp_pre_ForFrame.dwLowDateTime;
		ULONGLONG qwResult_cur_ForFrame = (((ULONGLONG) pTimestamp->dwHighDateTime) << 32) + pTimestamp->dwLowDateTime;
		ULONGLONG timediff_pre_cur_ForFrame = qwResult_cur_ForFrame - qwResult_pre_ForFrame;

		Timestamp_pre_ForFrame.dwHighDateTime = pTimestamp->dwHighDateTime;
		Timestamp_pre_ForFrame.dwLowDateTime = pTimestamp->dwLowDateTime;
		
		if( timediff_pre_cur_ForFrame > 10000000 ) // frame간 간격이 1초 이상인 경우 즉, fps 1 이하인 경우.
		{
			TRACE("[Time Over] Frame Diff :%f s. \n", (double)(timediff_pre_cur_ForFrame/10000000.0) );
			freezing_cnt = freezing_cnt + 1;

		}
	}
	else
	{
		Timestamp_pre_ForFrame.dwHighDateTime = pTimestamp->dwHighDateTime;
		Timestamp_pre_ForFrame.dwLowDateTime = pTimestamp->dwLowDateTime;
	}


	VIEWENGINFO* pViewEngInfo = m_MultiViewRender.GetViewEngineInfo(EngId);
	if(!pViewEngInfo){
		return;
	}

	//if not set buffer 
	if(pViewEngInfo->BufIndex == CMultiViewRender::UNDEFINED_INDEX){
		ChangeVideoSource(EngId, m_pNVFrame->GetSourceInfo(EngId));
	}

	/*{
	IXMLDOMNode *pxml = (IXMLDOMNode*)m_pNVFrame->GetSourceInfo(EngId);
	BSTR str;
	pxml->get_text(&str)
	}*/

	EnterCriticalSection(&m_CS); 

	BYTE* pImage = pMMData;
	ULONG pictType;
	if(UMF_ST_RAW_RTSP&srcType){
		if(!DecodeRTSPVideo(EngId, pMMData, MMDataLen, &pImage, &pictType, pTimestamp)){
			goto EXIT;
		}
	}else{
		goto EXIT;
	}

	BOOL bChangeDevice=FALSE;
	if(!m_MultiViewRender.CheckChangeViewMonitor(&bChangeDevice)){
		goto EXIT;
	}
	if(bChangeDevice){
		m_MultiViewRender.ChangeViewMonitor();
		m_UMFDrawUtil.ReInit(m_MultiViewRender.GetUMFRender());
	}
	if(pImage)
	m_MultiViewRender.RenderImage(EngId, pImage);

	VIEWINFO*	pViewInfo = m_MultiViewRender.GetViewInfoByEngId(EngId);

	CalcFrameStatistics(MMDataLen, pictType, pTimestamp);

	if( (Timestamp_pre.dwHighDateTime == 0) && (Timestamp_pre.dwLowDateTime == 0) )
	{

		// set now time
		Timestamp_pre.dwHighDateTime = pTimestamp->dwHighDateTime;
		Timestamp_pre.dwLowDateTime = pTimestamp->dwLowDateTime;

		// sean.
		SaveStatistics(EngId, m_fps, m_mbpsSizeShortTerm, freezing_cnt);
		TRACE("\n[FPS] First :%lf\n", m_fps);
	}
	else
	{
		ULONGLONG qwResult_pre = (((ULONGLONG) Timestamp_pre.dwHighDateTime) << 32) + Timestamp_pre.dwLowDateTime;
		ULONGLONG qwResult_cur = (((ULONGLONG) pTimestamp->dwHighDateTime) << 32) + pTimestamp->dwLowDateTime;
		
		ULONGLONG timediff_pre_cur = qwResult_cur - qwResult_pre;
		
		if( timediff_pre_cur >= 10000000.0 ) //1초 이상 마다 한번씩 현재 fps 출력.
		{
			// set now time
			Timestamp_pre.dwHighDateTime = pTimestamp->dwHighDateTime;
			Timestamp_pre.dwLowDateTime = pTimestamp->dwLowDateTime;

			// sean.
			SaveStatistics(EngId, m_fps, m_mbpsSizeShortTerm, freezing_cnt);
			TRACE("[FPS] :%lf\n", m_fps);
		}
	}

	//Draw Rect Text
	m_UMFDrawUtil.SetCanvasRect(pViewInfo->TargetRect);

	//	m_UMFDrawUtil.DrawRect(&rc, RGB(0,0,255), 50, UMF_BRT_SOLID);

	FileTimeToSystemTime(pTimestamp, &systemTime);
	swprintf(wcsText, sizeof(wcsText), _T("%04d-%02d-%02d %02d:%02d:%02d.%03d"), systemTime.wYear, systemTime.wMonth, systemTime.wDay, systemTime.wHour, systemTime.wMinute, systemTime.wSecond, systemTime.wMilliseconds);
	m_UMFDrawUtil.DrawText(0, pt, wcsText, RGB(255,255,0), 255);

	if (s_sourceInfo.bNoVideoDisplay) {
		swprintf(wcsText, sizeof(wcsText), _T("Video is intentionally not drawn for reducing CPU load"));
		m_UMFDrawUtil.DrawText(0, pt2, wcsText, RGB(255,255,0), 255);
	}
	// Draw Complexity 
	if (m_bCheckComplexity) {
		POINT pt3= {0,20};
		swprintf(wcsText, sizeof(wcsText), _T("Complexity %dx%d : %3d %s"), m_Width,m_Height,m_CurComplexity,
			(m_Width!=1280)?_T("Please Change Resolution into 1280x720!!!"):_T(""));
		m_UMFDrawUtil.DrawText(0, pt3, wcsText, m_Width!=1280?RGB(255,0,0):RGB(255,255,0), 255);
	}
	if (m_bCheckMotionrate) {
		DrawMotionRate();
	}

	m_UMFDrawUtil.Commit();

	DrawFrameSizeStatistics(EngId);

	m_MultiViewRender.Present(EngId);

EXIT:
	LeaveCriticalSection(&m_CS);
}



#include "UMFCmdDef.h"
LONG	CSimpleNVViewDlg::OnNVViewCommand(ULONG nEngId, wchar_t* szCmd, wchar_t* szSubCmd, wchar_t* szValue, void* pParam)
{
	if(wcscmp(szCmd, L"CONTROL")==0 && wcscmp(szSubCmd, L"CHANGESIZE")==0) {
		CRect rect;
		if(m_hWndParent){
			::GetClientRect(m_hWndParent, &rect);
			MoveWindow(&rect);
		}
	}
	if(wcscmp(szCmd, L"CONTROL")==0 && wcscmp(szSubCmd, L"COMPLEXITY")==0) {
		m_bCheckComplexity = !m_bCheckComplexity;
	} else if(wcscmp(szCmd, L"CONTROL")==0 && wcscmp(szSubCmd, L"MOTIONRATE")==0) {
		m_bCheckMotionrate = !m_bCheckMotionrate;
	}
	return TRUE;
}


void	CSimpleNVViewDlg::OnChangeClientRect()
{
	m_MultiViewRender.OnChangeClientRect();
}


void	CSimpleNVViewDlg::OnSize(UINT nType, int cx, int cy)
{
	__super::OnSize(nType, cx, cy);
	OnChangeClientRect();
}


void CSimpleNVViewDlg::GetStatisticsSourceInfo(DWORD EngId)
{
	if (!m_bGotStatisticsSourceInfo) {
		GetBmpSaveInfoFromXml(m_pNVFrame->GetSourceInfo(EngId), &gBmpSaveInfo);
		GetStatisticesSaveInfoFromXml(m_pNVFrame->GetSourceInfo(EngId), &gStatisticsSaveInfo);
		GetImatestSaveInfoFromXml(m_pNVFrame->GetSourceInfo(EngId), &gImatestSaveInfo);
		m_bGotStatisticsSourceInfo = TRUE;

		//여기서 stream파일을 open한다.
		TCHAR filename[MAX_PATH];
		_stprintf_s(filename,_T("%s.h264"),gStatisticsSaveInfo.filenameToSaveStatistics);		
		m_hStreamFile = CreateFile(filename,GENERIC_WRITE,0,NULL,CREATE_ALWAYS,FILE_ATTRIBUTE_NORMAL|FILE_FLAG_SEQUENTIAL_SCAN,NULL);
		_stprintf_s(filename,_T("%s.txt"),gStatisticsSaveInfo.filenameToSaveStatistics);		
		m_hStatFile = CreateFile(filename,GENERIC_WRITE,0,NULL,CREATE_ALWAYS,FILE_ATTRIBUTE_NORMAL|FILE_FLAG_SEQUENTIAL_SCAN,NULL);
	}
}

void	CSimpleNVViewDlg::WriteStatFile()
{
	#define MAXOUT 6400
	DWORD written;
	//formatting해서 저장한다.
	char * buf=(char*)malloc(MAXOUT);
	char * buforg = buf;
	for(UINT i = 0; i< m_nStatCount;i++){
//		int len = sprintf_s(buf,MAXOUT-(buf-buforg),"(%I64d,%I64d),\r\n",m_aFrameTimeLen[i][0],m_aFrameTimeLen[i][1]);
		int len = sprintf_s(buf,MAXOUT-(buf-buforg),"(%I64d, %8I64d, %I64d),\r\n",
			m_aFrameTimeLen[i][0],m_aFrameTimeLen[i][1],m_aFrameTimeLen[i][2]-116444736000000000I64);
		if(len>0)buf+=len;
	}
	WriteFile(m_hStatFile,buforg,buf-buforg,&written,NULL);
	
	TRACE("[WriteStatFile] :%d\n", m_nStatCount);
	m_nStatCount = 0;
	free(buforg);
}

BOOL	CSimpleNVViewDlg::DecodeRTSPVideo(DWORD EngId, BYTE *pMMData, UINT MMDataLen, BYTE **pImage, ULONG* pPictType, FILETIME *pTimestamp)
{
	GetStatisticsSourceInfo(EngId);
	//여기서 stream을 저장한다.
	if(m_hStreamFile!=INVALID_HANDLE_VALUE){
		DWORD written;
		WriteFile(m_hStreamFile,pMMData,MMDataLen,&written,NULL);
	}

	if(m_hStatFile!=INVALID_HANDLE_VALUE){
		m_aFrameTimeLen[m_nStatCount][0] = *(__int64*)pTimestamp;
		m_aFrameTimeLen[m_nStatCount][1] = MMDataLen;
		SYSTEMTIME sysTime;
		FILETIME fileTime;
		GetSystemTime(&sysTime);
		SystemTimeToFileTime(&sysTime,&fileTime);		
		m_aFrameTimeLen[m_nStatCount][2] = *(__int64*)&fileTime;
		m_nStatCount++;
		// check 2second
		{
#define kSaveTime_mSec 2000
			ASSERT(MAXTIMEFRAEME > 30*((kSaveTime_mSec/1000)+1));
			static DWORD oldtick = 0;
			if (oldtick==0) {
				oldtick = GetTickCount();
			}
			DWORD newtick = GetTickCount();
			if (newtick - oldtick >= kSaveTime_mSec) {
				oldtick = newtick;
				TRACE("\n[SaveStatistics] timeWarmup End :%d sec\n", gStatisticsSaveInfo.timeWarmup);
				WriteStatFile();
			}
		}
	}

	DWORD CodecID = GetCodecIdFromStreamType(s_sourceInfo.VideoCodec);

	if(CodecID == UU_CODEC_NONE) return FALSE;
	if(!m_UUCoreLib.OpenVideoDecompressor(CodecID)) return FALSE;

	DecodedVideo	tDecoded = {0,};
	INT				nDecodedLength = 0;
	BYTE*			pcbCompressedData = pMMData;
	INT				nCompressedLength = MMDataLen;
	BOOL			fDeinterlaced = FALSE;
	ZeroMemory( &tDecoded, sizeof(tDecoded) );					

	VIEWENGINFO* pViewEngInfo = m_MultiViewRender.GetViewEngineInfo(EngId);

	BOOL bImageDecoded = FALSE;
	if (!s_sourceInfo.bNoVideoDisplay) {
		bImageDecoded = m_UUCoreLib.VideoDecompress(&tDecoded, &nDecodedLength, pcbCompressedData, nCompressedLength, fDeinterlaced);
	} else {
		// We have to fill the tDecoded, if we do not display
		tDecoded.pData[0] = (BYTE*)m_pBufDecodedImg;
		tDecoded.uWidth = pViewEngInfo->Width;
		tDecoded.uHeight = pViewEngInfo->Height;
	}
	
	if (bImageDecoded) {
		*pPictType = tDecoded.uPictType;
		*pImage = tDecoded.pData[0];
// check complexity
		if (m_bCheckComplexity) {
			CheckComplexity(tDecoded.uWidth, tDecoded.uHeight, tDecoded.pData[0]);
		}
		if (m_bCheckMotionrate) {
			CheckMotionRate(tDecoded.uWidth, tDecoded.uHeight, tDecoded.pData[0],gStatisticsSaveInfo.AccumulatedMotionRate);
		}

		SaveDecodedImgToBmp(EngId, &tDecoded, nCompressedLength, pTimestamp);
		SaveStreamInfo(CodecID, tDecoded.uWidth, tDecoded.uHeight);
		SaveStatisticsBmpFile(tDecoded.uWidth, tDecoded.uHeight, (unsigned char*)tDecoded.pData[0]);
		SaveImatestBmpFile(tDecoded.uWidth, tDecoded.uHeight, (unsigned char*)tDecoded.pData[0]);
	}

	//If change source information when decoding change source info 
	if(pViewEngInfo->Width != tDecoded.uWidth || (pViewEngInfo->Height != tDecoded.uHeight) ){
		m_MultiViewRender.ChangeSourceInfo(EngId, tDecoded.uWidth, tDecoded.uHeight, 
			UUCoreColor2UMFColorFormat(tDecoded.uColorSpace));
	}

	*pImage = tDecoded.pData[0];
	return TRUE;
}

void CSimpleNVViewDlg::SaveStreamInfo(DWORD CodecID, int width, int height)
{
	static BOOL bSavedStreamInfo = FALSE;

	if (!bSavedStreamInfo && gStatisticsSaveInfo.bSaveStatistics) {
		CStdioFile fileInfo;
		if (fileInfo.Open(gStatisticsSaveInfo.filenameToSaveStreamInfo, CFile::modeCreate | CFile::modeNoTruncate | CFile::modeReadWrite | CFile::typeText)) {
			CString str;
			if (CodecID == UU_CODEC_H264 || CodecID == UU_CODEC_X264) {
				str.Format(_T("H.264"));
			} else if (CodecID == UU_CODEC_MJPEG) {
				str.Format(_T("MJPEG"));
			} else {
				str.Format(_T("Unknown"));
			}
			fileInfo.WriteString(str);

			str.Format(_T("\t%dx%d"), width, height);
			fileInfo.WriteString(str);

			fileInfo.Close();

			bSavedStreamInfo = TRUE;
		}
	}
}

void CSimpleNVViewDlg::SaveStatisticsBmpFile(int width, int height, unsigned char* pYuv)
{
	static BOOL bSavedStatisticsBmp = FALSE;

	if (!bSavedStatisticsBmp && gStatisticsSaveInfo.bSaveStatistics) {
		SaveYuvBmpFile(gStatisticsSaveInfo.filenameToSaveStatisticsBmp, width, height, pYuv);

		bSavedStatisticsBmp = TRUE;
	}
}

void CSimpleNVViewDlg::SaveImatestBmpFile(int width, int height, unsigned char* pYuv)
{
	static DWORD oldTickCount = 0;
	DWORD newTickCount = 0;

	if (gImatestSaveInfo.bSaveImatestImage) {
		newTickCount = GetTickCount();

		if (newTickCount - oldTickCount > gImatestSaveInfo.snapshotInterval * 1000) {
			oldTickCount = newTickCount;
			CString strFilename;
			strFilename.Format(_T("%s\\Imatest00.bmp"), gImatestSaveInfo.pathToSaveImatestBmp);
			SaveYuvBmpFile(strFilename, width, height, pYuv);
		}
	}
}

void CSimpleNVViewDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
#if 0
	if(m_Pause){
		m_Pause = FALSE;
	}else{
		m_Pause = TRUE;
	}
#endif

	__super::OnLButtonDown(nFlags, point);
}

void CSimpleNVViewDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	// TODO: Add your message handler code here and/or call default
//	m_MultiViewRender.OnEraseBackGround();
	__super::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

void CSimpleNVViewDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: Add your message handler code here
	// Do not call __super::OnPaint() for painting messages
	m_MultiViewRender.OnEraseBackGround();
}

BOOL CSimpleNVViewDlg::CreateSaveDirectory(TCHAR* rootPath)
{
	if (m_bIsBmpPathCreated) {
		return TRUE;
	}

	if (NULL==rootPath[0]) {
		// When XML does not specify path, rootPath contains NULL. However, CreateDirectory considers it is the root path of the current execution path.
		// This prevents from creating paths in the root path.
		return FALSE;
	}

	SYSTEMTIME localTime;
	GetLocalTime(&localTime);

	CString path;
	path.Format(_T("%s\\%02d_%02d_%02d_%02d"), rootPath, localTime.wMonth, localTime.wDay, localTime.wHour, localTime.wMinute);

	if (!CreateDirectory(path, NULL)) {
		return FALSE;
	}

	m_bmpSavePath = path;
	m_bIsBmpPathCreated = TRUE;

	return TRUE;
}

// incoming yuv is yuy2
void CSimpleNVViewDlg::SaveDecodedImgToBmp(DWORD EngId, DecodedVideo* ptDecoded, INT nCompressedLength, FILETIME *pTimestamp)
{

	static UINT cntSaved = 0;

	if (FALSE==gBmpSaveInfo.bSaveToBmp) {
		return;
	}

	cntSaved++;
	if (cntSaved>gBmpSaveInfo.TotalFramesToSave) {
		return;
	}

	if (2!=ptDecoded->uColorSpace || ptDecoded->uWidth>SAVE_BMP_MAX_WIDTH || ptDecoded->uHeight>SAVE_BMP_MAX_HEIGHT) {
		return;
	}

	if (!CreateSaveDirectory(gBmpSaveInfo.PathToSaveBmp)) {
		return;
	}

	CString strFileName;
	strFileName.Format(_T("%s\\img%03d.bmp"), m_bmpSavePath, cntSaved);

	SaveYuvBmpFile(strFileName, ptDecoded->uWidth, ptDecoded->uHeight, (unsigned char*)ptDecoded->pData[0]);

	// saving info file

	strFileName.Format(_T("%s\\%s"), m_bmpSavePath, SAVE_BMP_INFO_FILENAME);

	CStdioFile fileInfo;
	if (!fileInfo.Open(strFileName, CFile::modeCreate | CFile::modeNoTruncate | CFile::modeReadWrite | CFile::typeText)) {
		return;
	}
	fileInfo.SeekToEnd();

	CString strInfo;
	SYSTEMTIME systemTime;
	FileTimeToSystemTime(pTimestamp, &systemTime);
	strInfo.Format(_T("%03d\t%04d-%02d-%02d_%02d:%02d:%02d.%03d\t%d\t%d\n"), cntSaved, systemTime.wYear, systemTime.wMonth, systemTime.wDay, systemTime.wHour, systemTime.wMinute, systemTime.wSecond, systemTime.wMilliseconds, ptDecoded->uPictType, nCompressedLength);

	fileInfo.WriteString(strInfo);

	fileInfo.Close();

	if (gBmpSaveInfo.bSaveYuv420p) {
		// saving yuv420p file 

		int width = ptDecoded->uWidth;
		int height = ptDecoded->uHeight;

		UINT8 * dsty, * dstu, * dstv;
		dsty = (UINT8*)m_pBufDecodedImg;
		dstu = dsty + width*height;
		dstv = dstu + width*height/4;
		LONG lYuvSize = width*height + width*height/2;	//420p is 1.5 times
		
		ccvt_yuyv_420p(width, height, ptDecoded->pData[0], dsty, dstu, dstv);

		strFileName.Format(_T("%s\\img%03d.yuv420p"), m_bmpSavePath, cntSaved);

		CFile fileImg;
		if (!fileImg.Open(strFileName, CFile::modeCreate | CFile::modeWrite | CFile::typeBinary)) {
			return;
		}

		fileImg.Write(m_pBufDecodedImg, lYuvSize);

		fileImg.Close();
	}

}

void CSimpleNVViewDlg::SaveYuvBmpFile(CString strFileName, int width, int height, unsigned char* pImgYuv)
{
	int offsetH;
	int i;

	VidFrame yuv;
	VidFrame rgb;
	yuv.size.width = width;
	yuv.size.height = height;
	yuv.data = pImgYuv;

	rgb.buflen = SAVE_BMP_BUF_SIZE;
	rgb.data = (unsigned char*)m_pBufDecodedImg;

	yuyv_to_bgr24(&yuv, &rgb);

	BITMAPINFOHEADER BMIH = {0,};
	BMIH.biSize = sizeof(BITMAPINFOHEADER);
	BMIH.biBitCount = 24;
	BMIH.biPlanes = 1;
	BMIH.biCompression = BI_RGB;
	BMIH.biWidth = width;
	BMIH.biHeight = height;
	BMIH.biSizeImage = ((((BMIH.biWidth * BMIH.biBitCount) + 31) & ~31) >> 3) * BMIH.biHeight;

	BITMAPFILEHEADER bmfh = {0,};
	int nBitsOffset = sizeof(BITMAPFILEHEADER) + BMIH.biSize; 
	LONG lImageSize = BMIH.biSizeImage;
	LONG lFileSize = nBitsOffset + lImageSize;
	bmfh.bfType = 'B'+('M'<<8);
	bmfh.bfOffBits = nBitsOffset;
	bmfh.bfSize = lFileSize;
	bmfh.bfReserved1 = bmfh.bfReserved2 = 0;

	CFile fileImg;
	if (!fileImg.Open(strFileName, CFile::modeCreate | CFile::modeWrite | CFile::typeBinary)) {
		return;
	}

	//Write the bitmap file header
	fileImg.Write(&bmfh, sizeof(BITMAPFILEHEADER));
	//And then the bitmap info header
	fileImg.Write(&BMIH, sizeof(BITMAPINFOHEADER));
	//Finally, write the image data itself 
	//-- the data represents our drawing
	offsetH = BMIH.biWidth * BMIH.biBitCount / 8;
	// bitmap upside down
	for (i=BMIH.biHeight-1; i>=0; i--) {
		fileImg.Write(m_pBufDecodedImg+offsetH*i, offsetH);
	}

	fileImg.Close();
}

#define ONE_SECOND_IN_100NS	(10000000)					// in 100ns 
#define ONE_MINUTE_IN_100NS	(ONE_SECOND_IN_100NS*(ULONG)60)	// in 100ns
// FPS 계산시 1초 동안만 데이타를 살펴본다면, 프레임이 나오다가 나오지 않다가를 반복하면 FPS가 0으로 떨어졌다가 30으로 올라갔다를 반복한다.
// 이 편차를 줄이기 위해 TIME_TO_MONITOR 동안의 평균을 가지고 계산한다.
#define TIME_TO_MONITOR		(5)		// in seconds

void CSimpleNVViewDlg::CalcFrameStatistics(UINT MMDataLen, ULONG pictType, FILETIME *pTimestamp)
{
	// latest sample goes into the last slot
	for (int i=0; i<NUM_FRAMES_TO_HOLD-1; i++) {
		m_pFrameInfo[i] = m_pFrameInfo[i+1];
	}
	m_pFrameInfo[NUM_FRAMES_TO_HOLD-1].type = pictType;
	m_pFrameInfo[NUM_FRAMES_TO_HOLD-1].size = MMDataLen;
	m_pFrameInfo[NUM_FRAMES_TO_HOLD-1].timestamp.LowPart = pTimestamp->dwLowDateTime;
	m_pFrameInfo[NUM_FRAMES_TO_HOLD-1].timestamp.HighPart = pTimestamp->dwHighDateTime;

	SYSTEMTIME testTime;
	FileTimeToSystemTime(pTimestamp, &testTime);
	//TRACE("Time: %02d:%02d:%02d %02d.%02d.%02d.%03d\n", testTime.wYear, testTime.wMonth, testTime.wDay, testTime.wHour, testTime.wMinute, testTime.wSecond, testTime.wMilliseconds);

	ULARGE_INTEGER latestTimestamp;
	latestTimestamp = m_pFrameInfo[NUM_FRAMES_TO_HOLD-1].timestamp;

	m_maxFrameSize = 0;
	m_sumFrameSizeShortTerm = 0;
	m_sumFrameSizeLongTerm = 0;
	
	for (int i=NUM_FRAMES_TO_HOLD-1; i>=0; i--) {
		if ( (latestTimestamp.QuadPart - m_pFrameInfo[i].timestamp.QuadPart) <= ONE_SECOND_IN_100NS * TIME_TO_MONITOR) {
			m_sumFrameSizeShortTerm += m_pFrameInfo[i].size;
		}
		if ( (latestTimestamp.QuadPart - m_pFrameInfo[i].timestamp.QuadPart) <= ONE_MINUTE_IN_100NS * TIME_TO_MONITOR) {
			m_sumFrameSizeLongTerm += m_pFrameInfo[i].size;
		}
	}

	ULARGE_INTEGER oldestTimestamp;
	m_numFramesInOneSec = 0;
	for (int i=NUM_FRAMES_TO_HOLD-1; i>=0; i--) {
		if ( (latestTimestamp.QuadPart - m_pFrameInfo[i].timestamp.QuadPart) <= ONE_SECOND_IN_100NS * TIME_TO_MONITOR) {
			m_numFramesInOneSec++;
			oldestTimestamp.QuadPart = m_pFrameInfo[i].timestamp.QuadPart;
		} else {
			ULARGE_INTEGER diff;
			diff.QuadPart = latestTimestamp.QuadPart - oldestTimestamp.QuadPart;
			m_fps = (double)m_numFramesInOneSec * ONE_SECOND_IN_100NS / diff.QuadPart / TIME_TO_MONITOR;
			if (m_fps>30.1) {
				int a=0;
				a = a+1;
			}
			m_fps = (double)m_numFramesInOneSec / TIME_TO_MONITOR;
			break;
		}
	}

	for (int i=NUM_FRAMES_TO_HOLD-1; i>NUM_FRAMES_TO_HOLD-NUM_BARS_TO_GRAPH-1; i--) {
		if (m_maxFrameSize < m_pFrameInfo[i].size) {
			m_maxFrameSize = m_pFrameInfo[i].size;
		}
	}

	m_mbpsSizeShortTerm = m_sumFrameSizeShortTerm * 8. / (1024 * 1024) / TIME_TO_MONITOR;
	m_mbpsSizeLongTerm = m_sumFrameSizeLongTerm * 8. / (1024 * 1024 * 60) / TIME_TO_MONITOR;
}

#define BAR_WIDTH	5	// including gaps between bars
#define GRAPH_HEIGHT	75
#define GRAPH_MARGIN_LEFT	10
#define GRAPH_MARGIN_BOTTOM	10
#define ALPHA_GRAPH	180
#define CLR_GRAPH	RGB(255,255,255)
#define CLR_BAR_I	RGB(255,0,0)
#define CLR_BAR_P	RGB(255,255,0)
#define CLR_GRAPH_TEXT	RGB(255,255,0)

void CSimpleNVViewDlg::DrawFrameSizeStatistics(ULONG EngId)
{
	SIZE canvasSize = m_UMFDrawUtil.GetCanvasSize();

	POINT ptGr[] = {
		{GRAPH_MARGIN_LEFT, canvasSize.cy - GRAPH_MARGIN_BOTTOM - GRAPH_HEIGHT},
		{GRAPH_MARGIN_LEFT, canvasSize.cy - GRAPH_MARGIN_BOTTOM},
		{GRAPH_MARGIN_LEFT + BAR_WIDTH * NUM_BARS_TO_GRAPH, canvasSize.cy - GRAPH_MARGIN_BOTTOM}
	};

	m_UMFDrawUtil.DrawLineList(sizeof(ptGr)/sizeof(POINT), ptGr, CLR_GRAPH, ALPHA_GRAPH);

	int exponent = 1;
	while (m_maxFrameSize > (UINT)pow(2., exponent)) {
		exponent++;
	}
	UINT graphMaxY = (UINT)pow(2., exponent);

	wchar_t wcsStrMax[32];
	swprintf(wcsStrMax, sizeof(wcsStrMax), _T("%d KB"), graphMaxY / 1024);
	POINT pt = {GRAPH_MARGIN_LEFT, canvasSize.cy - GRAPH_MARGIN_BOTTOM - GRAPH_HEIGHT - 10};
	m_UMFDrawUtil.DrawText(1, pt, wcsStrMax, CLR_GRAPH_TEXT, ALPHA_GRAPH);

	wchar_t wcsStrShort[32];
	swprintf(wcsStrShort, sizeof(wcsStrShort), _T("%3.1f Mb (/sec)"), m_mbpsSizeShortTerm);
	POINT ptShort = {GRAPH_MARGIN_LEFT + BAR_WIDTH * NUM_BARS_TO_GRAPH, canvasSize.cy - GRAPH_MARGIN_BOTTOM - 70};
	m_UMFDrawUtil.DrawText(1, ptShort, wcsStrShort, CLR_GRAPH_TEXT, ALPHA_GRAPH);

	wchar_t wcsStrLong[32];
	swprintf(wcsStrLong, sizeof(wcsStrLong), _T("%3.1f Mb (/min)"), m_mbpsSizeLongTerm);
	POINT ptLong = {GRAPH_MARGIN_LEFT + BAR_WIDTH * NUM_BARS_TO_GRAPH, canvasSize.cy - GRAPH_MARGIN_BOTTOM - 50};
	m_UMFDrawUtil.DrawText(1, ptLong, wcsStrLong, CLR_GRAPH_TEXT, ALPHA_GRAPH);

	wchar_t wcsStrFps[32];
	swprintf(wcsStrFps, sizeof(wcsStrFps), _T("%4.1f FPS (for %d sec)"), m_fps, TIME_TO_MONITOR);
	POINT ptFps = {GRAPH_MARGIN_LEFT + BAR_WIDTH * NUM_BARS_TO_GRAPH, canvasSize.cy - GRAPH_MARGIN_BOTTOM - 20};
	m_UMFDrawUtil.DrawText(1, ptFps, wcsStrFps, CLR_GRAPH_TEXT, ALPHA_GRAPH);

	RECT rtBar[NUM_BARS_TO_GRAPH];
	for (int i = 0; i < NUM_BARS_TO_GRAPH; i++) {
		int barHeight = m_pFrameInfo[NUM_FRAMES_TO_HOLD-1-NUM_BARS_TO_GRAPH+i].size * GRAPH_HEIGHT / graphMaxY;
		rtBar[i].left	= GRAPH_MARGIN_LEFT + BAR_WIDTH * i + 1;
		rtBar[i].right	= GRAPH_MARGIN_LEFT + BAR_WIDTH * (i + 1) - 1;
		rtBar[i].top	= canvasSize.cy - GRAPH_MARGIN_BOTTOM - barHeight;
		rtBar[i].bottom	= canvasSize.cy - GRAPH_MARGIN_BOTTOM;
		m_UMFDrawUtil.DrawRect(rtBar[i], (m_pFrameInfo[NUM_FRAMES_TO_HOLD-1-NUM_BARS_TO_GRAPH+i].type == 1) ? CLR_BAR_I : CLR_BAR_P, ALPHA_GRAPH, UMF_BRT_SOLID);
	}

	m_UMFDrawUtil.Commit();

	//sean.
	//SaveStatistics(EngId, m_fps, m_mbpsSizeShortTerm);
}

void CSimpleNVViewDlg::SaveStatistics(ULONG EngId, double fps, double mbpsShortTerm, ULONG freezing_cnt)
{
	return;

	
	static int cnt = 0;
	static DWORD oldTickCount = 0;
	DWORD newTickCount = 0;
	
	TRACE("[Now] SaveStatistics Function\n");

	if (gStatisticsSaveInfo.bSaveStatistics == 0) {
		return;
	}

	if (cnt == 0) {
		cnt++;
		oldTickCount = GetTickCount();
	}

	newTickCount = GetTickCount();

	if (newTickCount - oldTickCount < gStatisticsSaveInfo.timeWarmup * 1000) {
		TRACE("\n[SaveStatistics] timeWarmup End :%d sec\n", gStatisticsSaveInfo.timeWarmup);
		return;
	}

	if (newTickCount - oldTickCount > (gStatisticsSaveInfo.timeWarmup + gStatisticsSaveInfo.timeCollecting) * 1000) {
		TRACE("\n[SaveStatistics] timeCollecting End :%d sec\n", gStatisticsSaveInfo.timeCollecting);
		return;
	}
	
	CStdioFile fileInfo;
	if (!fileInfo.Open(gStatisticsSaveInfo.filenameToSaveStatistics, CFile::modeCreate | CFile::modeNoTruncate | CFile::modeReadWrite | CFile::typeText)) {
		TRACE("FileOpen Fail :%s\n", gStatisticsSaveInfo.filenameToSaveStatistics);
		return;
	}
	TRACE("FileOpen Success :%s\n", gStatisticsSaveInfo.filenameToSaveStatistics);
	
	fileInfo.SeekToEnd();

	CString strInfo;
	strInfo.Format(_T("%4.1f\t%3.1f\t%d\n"), fps, mbpsShortTerm, freezing_cnt);
	fileInfo.WriteString(strInfo);

	fileInfo.Close();
}
