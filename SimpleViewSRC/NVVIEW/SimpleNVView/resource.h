//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by SimpleNVView.rc
//
#define IDD_DIALOG_ZONE                 2000
#define IDC_BUTTON_APPLY                2001
#define IDC_COMBO_POS                   2002
#define IDC_EDIT_ALARM_PERCENT          2003
#define IDC_EDIT_PROC_DURATION          2004
#define IDC_EDIT3                       2005
#define IDC_EDIT_ALARM_DURATION         2005
#define ID_ADDZONE                      32771
#define ID_ADDLINE                      32772
#define ID_REMOVEZONE                   32773
#define ID_REMOVEALL                    32774
#define ID_ADDNODE                      32775
#define ID_REMOVENODE                   32776
#define ID_ADDCOUNTER                   32777
#define ID_REMOVECOUNTER                32778
#define ID_SAVETOFILE                   32781
#define ID_LOADFROMFILE                 32782
#define ID_PRESENCE                     32791
#define ID_ENTER                        32792
#define ID_EXIT                         32793
#define ID_APPEAR                       32794
#define ID_DISAPPEAR                    32795
#define ID_EDIT                         32795
#define ID_STOP                         32796
#define ID_CONTROL_STOP                 32796
#define ID_DWELL                        32797
#define ID_DIRECTION                    32798
#define ID_SPEED                        32799

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        2006
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         2005
#define _APS_NEXT_SYMED_VALUE           2000
#endif
#endif
