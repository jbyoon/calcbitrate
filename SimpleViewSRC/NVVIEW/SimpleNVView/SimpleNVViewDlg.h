// SimpleNVView.h : main header file for the SimpleNVView DLL
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include <list>
#include "resource.h"		// main symbols
#include "CommonInterface/IBaseNVView.h"
#include "MultiViewRender.h"
#include "UMFDrawUtil.h"
#include "UUCoreDec.h"

#define MODULE_NAME	"SimpleNVView"

#define SAVE_BMP_MAX_WIDTH	1920
#define SAVE_BMP_MAX_HEIGHT	1080
#define SAVE_BMP_BUF_SIZE (SAVE_BMP_MAX_WIDTH*SAVE_BMP_MAX_HEIGHT*3)
#define SAVE_JPEG_BUF_SIZE (512000)

typedef struct {
	ULONG type;
	UINT size;
	ULARGE_INTEGER timestamp;
} FRAME_INFO;

class CSimpleNVViewDlg : public CDialog, public IBaseNVView
{
//	DECLARE_DYNAMIC(CSimpleNVViewDlg)

public:
	CSimpleNVViewDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSimpleNVViewDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG_ZONE, MAX_UI_VIEW = 4 };

	char*	GetModuleName(){return MODULE_NAME;}
	ULONG	GetType(){return VIEW_MODULE;}

	BOOL	Create(HWND hWndParent, IBaseNVFrame *pNVFrame);
	void	CloseOpenFiles();
	void	Destroy();

	LONG	OnNVViewCommand(ULONG nEngId, wchar_t* szCmd, wchar_t* szSubCmd, wchar_t* szValue, void* pParam);
		
	BOOL	Start(BOOL bStart){return FALSE;}
	NVVIEW_STATUS GetStatus(){return m_Status;}
	
	DWORD	GetSupportSrcTypes(){ return 0xFFFFFFFF;}

	BOOL	Show(ULONG EngId, BOOL bShow, DISPLAY_STYLE Style);
	BOOL	ChangeMMSourceInfo(DWORD EngId, PXML pSoruceInfoXML);
	void	ProcessMMData(ULONG EngId, SRCTYPE Type, BYTE *pMMData, UINT MMDataLen, FILETIME *pTimestamp);
	BOOL	CreateSaveDirectory(TCHAR* rootPath);
	void	SaveDecodedImgToBmp(DWORD EngId, DecodedVideo* ptDecoded, INT nCompressedLength, FILETIME *pTimestamp);
	void	CalcFrameStatistics(UINT MMDataLen, ULONG pictType, FILETIME *pTimestamp);
	void	DrawFrameSizeStatistics(ULONG EngId);
	void	SaveStatistics(ULONG EngId, double fps, double mbpsShortTerm, ULONG freezing_cnt);
	void	SaveYuvBmpFile(CString strFileName, int width, int height, unsigned char* pImgYuv);
	void	GetStatisticsSourceInfo(DWORD EngId);
	void	SaveStreamInfo(DWORD CodecID, int width, int height);
	void	SaveStatisticsBmpFile(int width, int height, unsigned char* pYuv);
	void	SaveImatestBmpFile(int width, int height, unsigned char* pYuv);
	void	WriteStatFile();

	// Complexity
	void InitComplexity();
	void CloseComplexity();
	void ConvertYuvtoBmp(int width, int height, unsigned char* pImgYuv);
	BOOL CheckComplexity(int width, int height, BYTE *pYUV);
	BOOL CheckMotionRate(int width, int height, BYTE *pYUV, double mrate=1.0);
	void DrawMotionRate();
	DWORD ComplexityThread();
	static	DWORD	WINAPI ComplexityThreadWrapper(LPVOID pParam);


protected:
	
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//virtual BOOL OnWndMsg(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pResult);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()

private:
	HWND				m_hWndParent;
	DWORD				m_EngId;

	BOOL				m_bCreated;
	NVVIEW_STATUS		m_Status;
	void				OnChangeClientRect();

	BOOL				m_bIsBmpPathCreated;
	CString				m_bmpSavePath;
	char*				m_pBufDecodedImg;
	

	CRITICAL_SECTION	m_CS;
	CMultiViewRender	m_MultiViewRender;
	CUMFDrawUtil		m_UMFDrawUtil;
	IUMFDrawUtil*		m_pDrawUtil;
	IBaseNVFrame*		m_pNVFrame;

	CUUCoreDec			m_UUCoreLib;

	FRAME_INFO			*m_pFrameInfo;
	UINT				m_maxFrameSize;
	UINT				m_sumFrameSizeShortTerm;
	UINT				m_sumFrameSizeLongTerm;
	double				m_mbpsSizeShortTerm;
	double				m_mbpsSizeLongTerm;
	int					m_numFramesInOneSec;
	double				m_fps;
// Complexity
	BOOL				m_bCheckComplexity;
	BOOL				m_bCheckMotionrate;
	UINT				m_CurComplexity;
	double				m_CurMotionRate;
	BYTE*				m_pBufJpegImg;
	BYTE*				m_pRawL	;
	BYTE*				m_pL8Old;
	std::list<BYTE*>	m_qDiff;
	UINT				m_MotionAcumulateSize;
	BOOL				m_bRundComplexityThread;
	HANDLE				m_hComplexityThread;
	HANDLE				m_hComplexityEvent;
	int					m_Width;
	int					m_Height;
	
	void				ChangeVideoSource(DWORD EngId, PXML pSoruceInfoXML);
	BOOL				DecodeRTSPVideo(DWORD EngId, BYTE *pMMData, UINT MMDataLen, BYTE **pImage, ULONG* pPictType, FILETIME *pTimestamp);
	BOOL				m_Pause;
	BOOL				m_bGotStatisticsSourceInfo;
	HANDLE				m_hStreamFile;
	HANDLE				m_hStatFile;
	UINT				m_nStatCount;
#define MAXTIMEFRAEME  1000
	__int64				m_aFrameTimeLen[MAXTIMEFRAEME][3];

public:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnPaint();
};

