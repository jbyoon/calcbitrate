// Complexity.cpp : 
//

#include "stdafx.h"
#define _USE_MATH_DEFINES
#include <math.h>

#include "SimpleNVViewDlg.h"
#include "yuv2rgb2.h"
#include "jpeglib.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define RESIZE_PIXEL 8
#define ACCUMULATE_MOTION 10
#define L8_BUFFER_SIZE (SAVE_BMP_MAX_WIDTH/8*SAVE_BMP_MAX_WIDTH/8)
#define NEW_L8_BUFFER() new BYTE[L8_BUFFER_SIZE]
#define CLEAR_L8_BUFFER(b)  do { memset(b,0,L8_BUFFER_SIZE);} while(0)
#define SAVE_RELEASE_BUFFER(b) do { if (b) {delete [] b; b = NULL;}} while(0)

void CSimpleNVViewDlg::InitComplexity() {
	m_pBufJpegImg = new BYTE[SAVE_JPEG_BUF_SIZE];
	m_pRawL = new BYTE[SAVE_BMP_MAX_WIDTH*SAVE_BMP_MAX_WIDTH];
	m_pL8Old = NULL;
	m_MotionAcumulateSize = ACCUMULATE_MOTION;
	m_CurMotionRate = 0;
	m_CurComplexity = 0;
	//
	m_hComplexityEvent = CreateEvent(NULL, TRUE, FALSE, NULL);

	DWORD threadID;
	m_hComplexityThread = CreateThread(NULL, 0, ComplexityThreadWrapper, this, NULL, &threadID);
	if (NULL != m_hComplexityThread) {
		m_bRundComplexityThread=TRUE;
	}
}

void CSimpleNVViewDlg::CloseComplexity() {
	if (NULL != m_hComplexityThread) {
		m_bRundComplexityThread=FALSE;
		DWORD ret = WaitForSingleObject(m_hComplexityThread, 3000);
		if(ret!=WAIT_OBJECT_0){
			TRACE(_T("Timeout :: Terminate ComplexityThread\n"));
			TerminateThread(m_hComplexityThread, 0);
		}
		CloseHandle(m_hComplexityThread);
		m_hComplexityThread = NULL;
	}
	if( m_hComplexityEvent ) {
		CloseHandle( m_hComplexityEvent );
		m_hComplexityEvent = NULL;
	}
	SAVE_RELEASE_BUFFER(m_pBufJpegImg);
	SAVE_RELEASE_BUFFER(m_pRawL);
	SAVE_RELEASE_BUFFER(m_pL8Old);
	for (std::list<BYTE*>::iterator it=m_qDiff.begin(); it != m_qDiff.end(); ++it) {
		SAVE_RELEASE_BUFFER(*it);
	}
	m_qDiff.clear();
}

/////////////////////////////////////////////////////////////////////////////
// Complexity
void CSimpleNVViewDlg::ConvertYuvtoBmp(int width, int height, unsigned char* pImgYuv)
{
	VidFrame yuv;
	VidFrame rgb;
	yuv.size.width = width;
	yuv.size.height = height;
	yuv.data = pImgYuv;
	rgb.buflen = SAVE_BMP_BUF_SIZE;
	rgb.data = (BYTE*)m_pBufDecodedImg;

	yuyv_to_bgr24(&yuv, &rgb);
}


int CompressJpeg(BYTE *pRGB, BYTE* pJPEG, ULONG *pjpglen, int width, int height) {
	struct jpeg_compress_struct cinfo;
	struct jpeg_error_mgr jerr;

	JSAMPROW row_pointer[1];	/* pointer to JSAMPLE row[s] */
	int row_stride;		/* physical row width in image buffer */

	cinfo.err = jpeg_std_error(&jerr);
	jpeg_create_compress(&cinfo);
	jpeg_mem_dest(&cinfo, &pJPEG, pjpglen);

	cinfo.image_width = width; 	/* image width and height, in pixels */
	cinfo.image_height = height;
	cinfo.input_components = 3;		/* # of color components per pixel */
	cinfo.in_color_space = JCS_RGB; 	/* colorspace of input image */

	jpeg_set_defaults(&cinfo);
	//  jpeg_set_quality(&cinfo, quality, TRUE /* limit to baseline-JPEG values */);

	jpeg_start_compress(&cinfo, TRUE);

	row_stride = width * 3;	/* JSAMPLEs per row in image_buffer */

	while (cinfo.next_scanline < cinfo.image_height) {
	row_pointer[0] = & pRGB[cinfo.next_scanline * row_stride];
	(void) jpeg_write_scanlines(&cinfo, row_pointer, 1);
	}

	jpeg_finish_compress(&cinfo);
	jpeg_destroy_compress(&cinfo);
	return *pjpglen;
}


BOOL CSimpleNVViewDlg::CheckComplexity(int width, int height, BYTE *pYUV)
{
	// check time
	if(WaitForSingleObject(m_hComplexityEvent,0) == WAIT_OBJECT_0) {
		return FALSE;
	}
	m_Width = width, m_Height = height;
	ConvertYuvtoBmp(width,height,pYUV);
	// call thread
	SetEvent(m_hComplexityEvent);
	return TRUE;
}


DWORD CSimpleNVViewDlg::ComplexityThread() {
	while(m_bRundComplexityThread) {
		if (WaitForSingleObject(m_hComplexityEvent,200) == WAIT_OBJECT_0) {
			unsigned long jpglen = SAVE_JPEG_BUF_SIZE;
			ULONG len = CompressJpeg((BYTE*)m_pBufDecodedImg,(BYTE*)m_pBufJpegImg,&jpglen,m_Width,m_Height);
			m_CurComplexity = int(len/sqrt(float(m_Width*m_Height)));
			WaitForSingleObject(m_hComplexityThread,200); // sleep 200ms
			ResetEvent(m_hComplexityEvent);
		} else {
		}
	}
	return 0;
}

DWORD	CSimpleNVViewDlg::ComplexityThreadWrapper(LPVOID pParam)
{
	return ((CSimpleNVViewDlg *) pParam)->ComplexityThread();
}


/////////////////////////////////////////////////////////////////////////////
// Motion Rate
static void ConvertYUVtoL(int width, int height, BYTE *pYUV, BYTE *pL) {
	int i,j;
	for (i=0;i<height;i++) {
		for (j=0;j<width;j++) {
			*pL = *pYUV;
			pL++;
			pYUV+=2;
		}
	}
}

static void Resizex8(int width, int height, BYTE *pSRC, BYTE *pDST, int *rw, int *rh)
{
	int i,j,ii,jj;
	int hxb = *rh = (height+RESIZE_PIXEL-1)/RESIZE_PIXEL;
	int wxb = *rw = (width+RESIZE_PIXEL-1)/RESIZE_PIXEL;
	int htail = height%RESIZE_PIXEL;
	int wtail = width%RESIZE_PIXEL;
	for (i=0;i<hxb;i++) {
		for (j=0;j<wxb;j++) {
			int total = 0;
			int hbase = i*RESIZE_PIXEL;
			int wbase = j*RESIZE_PIXEL;
			int iend = (htail&&(i==(hxb-1)))?htail:RESIZE_PIXEL;
			int jend = (wtail&&(j==(wxb-1)))?wtail:RESIZE_PIXEL;
			for (ii=0;ii<iend;ii++) {
				BYTE *pL = &pSRC[hbase*width+wbase];
				for (jj=0;jj<jend;jj++) {
					total += *pL++;
				}
			}
			pDST[i*wxb+j] = total/(iend*jend);
		}
	}
}

static void MotionDifference(int width, int height, BYTE *pSRC, BYTE *pDST, BYTE *pDiff, int delta)
{
	if(pSRC&&pDST) {
		for (int i=0;i<height;i++) {
			for (int j=0;j<width;j++) {
				int d= abs(*pSRC++ - *pDST++);
				*pDiff++ = d>delta?255:0;
			} 
		}
//	} else if(!pSRC&&pDST) {
//		memcpy(pDiff,pDST,width*height);
//	} else if (pSRC&&!pDST) {
//		memcpy(pDiff,pSRC,width*height);
//	} else if (!pSRC&&!pDST) {
	}
}

static void MaxImage(int width, int height, BYTE *pSRC, BYTE *pDST, BYTE *pMax)
{
	for (int i=0;i<height;i++) {
		for (int j=0;j<width;j++) {
			int d = *pSRC++ + *pDST++;
			*pMax++ = min(d,255);
		} 
	}
}

static double CalcPixel(int width, int height, BYTE *pSRC)
{
	ULONG total =0;
	for (int i=0;i<height;i++) {
		for (int j=0;j<width;j++) {
			total += *pSRC++;
		} 
	}
	return 100*(total/255.0)/(width*height);
}

static void DrawBox(int x, int y, int xx, int yy, int width, int height, BYTE *pYUV, BYTE value) {
	for (int i=y;i<=yy;i++) {
		for (int j=x;j<=xx;j++) {
//			pYUV[2*(i*width+j)] = value;
//			pYUV[2*(i*width+j)] = (value+pYUV[2*(i*width+j)])>>1;
			pYUV[2*(i*width+j)+1] = 0;
		}
	}
}

static void DrawMotionBox(int rw, int rh, BYTE *pL8, int width, int height, BYTE *pYUV)
{
	for (int i=0;i<rh;i++) {
		for (int j=0;j<rw;j++) {
			BYTE value = pL8[i*rw+j];
			if(value>0) {
				DrawBox(j*RESIZE_PIXEL, i*RESIZE_PIXEL,min(width,(j+1)*RESIZE_PIXEL-1),min(height,(i+1)*RESIZE_PIXEL-1), 
					width,height, pYUV, value);
			}
		} 
	}
}

BOOL CSimpleNVViewDlg::CheckMotionRate(int width, int height, BYTE *pYUV, double mrate)
{
	// YUV -> L
	ConvertYUVtoL(width,height,pYUV,m_pRawL);
	// 8x8
	int rw=0;
	int rh=0;
	BYTE *pL8 = NEW_L8_BUFFER();
	Resizex8(width,height,m_pRawL,pL8,&rw,&rh);
	if (!m_pL8Old) {
		m_pL8Old = pL8;
		return TRUE;
	}
	// comp
	BYTE *pDiff = NEW_L8_BUFFER();
#define MOTION_DELTA 10
	MotionDifference(rw,rh,m_pL8Old,pL8,pDiff,MOTION_DELTA);
	m_qDiff.push_back(pDiff);
	m_MotionAcumulateSize = max(int(m_fps*mrate),2);
	if (m_qDiff.size()>m_MotionAcumulateSize) {
		BYTE *d = m_qDiff.front();
		SAVE_RELEASE_BUFFER(d);
		m_qDiff.pop_front();
	}
	// max(a,b)
	BYTE *pMax = NEW_L8_BUFFER();
	CLEAR_L8_BUFFER(pMax);
	for (std::list<BYTE*>::iterator it=m_qDiff.begin(); it != m_qDiff.end(); ++it) {
		MaxImage(rw,rh,pMax,*it,pMax);
	}
	// calc motion rate 0~100%
	m_CurMotionRate = CalcPixel(rw,rh,pMax);
	// draw motion
	DrawMotionBox(rw,rh,pMax, width,height,pYUV);

	SAVE_RELEASE_BUFFER(m_pL8Old);
	m_pL8Old = pL8;
	SAVE_RELEASE_BUFFER(pMax);

	return TRUE;
}

void CSimpleNVViewDlg::DrawMotionRate() {
#if 1
	POINT pt4= {0,90};
	wchar_t wcsText[MAX_PATH];
	swprintf(wcsText, sizeof(wcsText), _T("Motion %2d fps : %4.1f%%"), m_MotionAcumulateSize,m_CurMotionRate);
	m_UMFDrawUtil.DrawText(0, pt4, wcsText, RGB(255,255,0), 255);
	POINT pt={100,100};
#define R (2*M_PI) // M_PI_2
	m_UMFDrawUtil.DrawCircularArc(pt,40,40,0,R,RGB(255,255,0),64);
	m_UMFDrawUtil.DrawCircularArc(pt,40,30,0,(m_CurMotionRate/100.0)*R,RGB(255,0,0),128);
#else
	POINT pt4= {0,40};
	wchar_t wcsText[MAX_PATH];
	swprintf(wcsText, sizeof(wcsText), _T("Motion %2d fps : %4.1f%%"), m_MotionAcumulateSize,m_CurMotionRate);
	m_UMFDrawUtil.DrawText(0, pt4, wcsText, RGB(255,255,0), 255);
#define WIDTH 200
#define HEIGHT 16
#define X  130
#define Y  40
#define XX (X+WIDTH)
#define YY (Y+HEIGHT)

	RECT rect_b ={X,YY-2,XX,YY};
	int l = int(WIDTH*(m_CurMotionRate/100));
	RECT rect={X+0,Y+2,X+l,YY-2};
//	m_UMFDrawUtil.DrawBoundingBox(rect_b,RGB(255,255,0), 128);
	m_UMFDrawUtil.DrawRect(rect_b,RGB(255,255,0), 128, UMF_BRT_SOLID);
	m_UMFDrawUtil.DrawRect(rect,RGB(255,0,0), 128, UMF_BRT_SOLID);
#endif
}