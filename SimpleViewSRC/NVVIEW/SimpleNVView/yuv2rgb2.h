#ifndef __YUV2RGB_H_
#define __YUV2RGB_H_

typedef struct {
	/// The width 
	int width;
	/// The height
	int height;
} VidSize;

/// Video Frame
typedef struct {
	/// The size of frame
	VidSize size;	

	/// The size of data/buffer allocated to hold image. It should be larger than or equal to imagesize
	int buflen; 

	/// Pointer to image data.
	unsigned char *data;

	unsigned char* u;
} VidFrame;

int yuv420_to_rgb24(VidFrame *src,VidFrame *dest);
int yuv420_to_bgr24(VidFrame *src,VidFrame *dest);
int yuyv_to_rgb24(VidFrame *src,VidFrame *dest);
int yuyv_to_bgr24(VidFrame *src,VidFrame *dest);

#endif
