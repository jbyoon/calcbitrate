// ProcViewCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "ProcViewCtrl.h"
#include <math.h>
#include "resource.h"

// CProcViewCtrl
IMPLEMENT_DYNAMIC(CProcViewCtrl, CDialog)

CProcViewCtrl::CProcViewCtrl()
{
	memset(&m_SourceBIH, 0, sizeof(m_SourceBIH));
}

CProcViewCtrl::~CProcViewCtrl()
{
	Endup();
}


BEGIN_MESSAGE_MAP(CProcViewCtrl, CStatic)
	ON_WM_SIZE()
	ON_WM_PAINT()
END_MESSAGE_MAP()


// CProcViewCtrl message handlers

BOOL CProcViewCtrl::Setup()
{
	if(!m_MultiViewRender.Setup(m_hWnd, 1)){
		MessageBox(_T("Can not Setup Viewer"), _T("ERROR"), MB_OK);
		return FALSE;
	}

	m_MultiViewRender.SetViewMode(CMultiViewRender::VIEW_1);
	return TRUE;
}


void CProcViewCtrl::Endup()
{
	m_MultiViewRender.Endup();
}



void CProcViewCtrl::ProcessImageNVAMetaData(DWORD EngId, BYTE *pImage, BITMAPINFOHEADER *pbm, BYTE *pMetaData, int iMateDataLen)
{
	VIEWINFO* pViewInfo = m_MultiViewRender.GetViewInfoByEngId(EngId);
	if(!pViewInfo) return ;

	if(m_SourceBIH.biWidth != pbm->biWidth || m_SourceBIH.biHeight != pbm->biHeight || 
		m_SourceBIH.biCompression != pbm->biCompression){
		m_MultiViewRender.ChangeVCASourceInfo(EngId, pbm);
		memcpy(&m_SourceBIH, pbm, sizeof(BITMAPINFOHEADER));
	}
	
	m_MultiViewRender.RenderImage(EngId, pImage);
	m_MultiViewRender.Present(EngId);
}


void CProcViewCtrl::OnSize(UINT nType, int cx, int cy)
{
	__super::OnSize(nType, cx, cy);
	m_MultiViewRender.OnChangeClientRect();
	// TODO: Add your message handler code here
}

void CProcViewCtrl::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: Add your message handler code here
	// Do not call __super::OnPaint() for painting messages
	m_MultiViewRender.OnEraseBackGround();
}


