#ifndef __RTSP_CLIENT_API_H__
#define __RTSP_CLIENT_API_H__

enum {
	RTSP_MT_VIDEO = 1,
	RTSP_MT_AUDIO,
	RTSP_MT_META
};

#define IDPWMAXLENGTH			32
#define OFD_MODULE_ID			0x004F4644	// "OFD"
#define VCA_MODULE_ID			0x00564341	// "VCA"

typedef struct 
{
	int nsize;			/* size of structure		   */
	int customid;		/* custom module id			   */
	int moduleid;		/* module id - ordering number */
	int datalen;		/* size of whole data		   */

	char reserved[16];
}CustomMetaHdr;


class IRTSPClientAPI  
{
public:
	virtual BOOL	Init() = 0;
	virtual VOID	Release() = 0;
	virtual BOOL	Connect(CHAR *pszUrl, CHAR *pszSession, CHAR *pszUsername, CHAR *pszPassword) = 0;
	virtual VOID	Disconnect() = 0;
	virtual HANDLE	GetEvent(UINT uType) = 0;
	virtual BOOL	GetMediaData(UINT uType, BYTE **ppData, UINT *puFrameSize, struct timeval *pTimeStamp) = 0;
	virtual BOOL	GetParam(CHAR *pszCmd, VOID *pParam, UINT uLen) = 0;
	virtual BOOL	SetParam(CHAR *pszCmd, VOID *pParam) = 0;

	virtual VOID	DestroySelf() = 0;
};

extern "C" IRTSPClientAPI* WINAPI CreateRTSPClientAPI();

#endif
