#ifndef __UUCOREAPI_H
#define __UUCOREAPI_H

#pragma pack( push, __UUCOREAPI_H )
#pragma pack(8)


// How to use
//	- Select "UUCore Build Type" and "DLL Import, Export"
//		#define USING_UUCORE_LIB
//		#define USING_UUCORE_DLL	// default
//	- Add preprocessors to use
//		if USING_UUCORE_LIB isn't defined, UUCore.dll will be used
//		if UUCOREDLL_EXPORTS isn't defined, UUCoreApi can be used for calling
#if defined(USING_UUCORE_LIB)
#		define UUC_API __stdcall
#else	// USING_UUCORE_DLL
#	if defined(UUCOREDLL_EXPORTS)
#		define UUC_API  __declspec(dllexport) __stdcall
#	else
#		define UUC_API  __declspec(dllimport) __stdcall
#	endif
#endif


extern "C" const IID IID_IUucore;

// Codec ID
#define UU_CODEC_NONE		0
#define UU_CODEC_MPJV		1				/* MPEG4 Video Codec for MPJVS */
#define UU_CODEC_MP2		2				/* MPEG1 Layer2 Audio Codec */
#define UU_CODEC_ULAW		mmioFOURCC('U','L','A','W')	/* G.711 uLaw */
#define UU_CODEC_MPJ4		mmioFOURCC('M','P','J','4')	/* MPEG4 Video Codec for MPJVS */
#define UU_CODEC_MPJ2		mmioFOURCC('M','P','J','2')	/* MPEG2 Video Codec for MPJVS */
#define UU_CODEC_MJPEG		mmioFOURCC('M','J','P','G')	/* MJPEG Video Codec */
#define UU_CODEC_H264		mmioFOURCC('H','2','6','4')	/* H.264 Video Codec */
#define UU_CODEC_X264		mmioFOURCC('X','2','6','4')	/* H.264 Video Codec */
#define UU_CODEC_MPEG2		mmioFOURCC('M','P','2','V')	/* MPEG2 Video Codec for UUCore V1.100.0 or higher version */
#define UU_CODEC_MPEG4		mmioFOURCC('M','P','G','4')	/* MPEG4 Video Codec for UUCore V1.100.0 or higher version */

// color space
#define UU_CSP_NONE			0
#define UU_CSP_YVU420		1  /* Planar YUV 4:2:0 (1 Cr & Cb sample per 2x2 Y samples (note V,U order!)) */
#define UU_CSP_YV12			UU_CSP_YVU420  /* Same as UU_CSP_YVU420 */
#define UU_CSP_YUY2			2  /* Packed YUV 4:2:2 */
#define UU_CSP_RGB24		3  /* Packed 24-bit RGB */
#define UU_CSP_RGB32		4  /* Packed 32-bit RGB */
#define UU_CSP_RGB565		5  /* Packed RGB 5:6:5, 16bpp, (msb)   5R 6G 5B(lsb), in cpu endianness */
#define UU_CSP_YUV420		6  /* Planar YUV 4:2:0 (1 Cb & Cr sample per 2x2 Y samples (note U, V order!)) */
#define UU_CSP_I420			UU_CSP_YUV420  /* Same as UU_CSP_YUV420 */
#define UU_CSP_IYUV			UU_CSP_YUV420  /* Same as UU_CSP_YUV420 */



// Codec Property Flags
#define UU_CPF_LOW_DELAY	0x00000001	/* The decoded output will not be delayed */

// Parameter for DecodeVideo to get buffered frames in decoder
// for UUCore V1.100.7 or higher version
#define	UU_GET_BUFFERED_FRAME_SIZE		1	/* it can be used for 5th parameter of UucDecodeVideo(), or 4th parameter of DecodeVideo() */


// Deinterlacer
#define UU_DEINT_LOWPASS5		0  /* 5-tap lowpass deinterlacer */
#define UU_DEINT_MEDIAN			1  /* median deinterlacer */
#define UU_DEINT_LINEAR_BLEND	2  /* linear blend deinterlacer */
#define UU_DEINT_LINEAR_IPOL	3  /* linear interpolating deinterlacer */
#define UU_DEINT_CUBIC_IPOL		4  /* cubic interpolating deinterlacer */
// Deinterlacer (for UUCore V1.100.0 or higher version)
#define UU_DEINTER_DUPLICATE			5  
#define UU_DEINTER_BLEND				6  
#define UU_DEINTER_MEDIAN				7  
#define UU_DEINTER_EDGE_DETECT			8  
#define UU_DEINTER_MEDIAN_THRESHOLD		9  
#define UU_DEINTER_CAVT					10 

// Interpolation Method for Resizing (for UUCore V1.100.0 or higher version)
#define UU_INTER_NN						1
#define UU_INTER_LINEAR					2
#define UU_INTER_CUBIC					4
#define UU_INTER_CUBIC2P_BSPLINE		5	/* two-parameter cubic filter (B=1, C=0) */
#define UU_INTER_CUBIC2P_CATMULLROM		6	/* two-parameter cubic filter (B=0, C=1/2) */
#define UU_INTER_B05C03					7	/* two-parameter cubic filter (B=1/2, C=3/10) */
#define UU_INTER_SUPER					8
#define UU_INTER_LANCZOS				16
#define UU_INTER_SMOOTH_EDGE			(1 << 31)


/* in bytes */
#define UU_MAX_AUDIO_FRAME_SIZE 192000

/**
 * Required number of additionally allocated bytes at the end of the input bitstream for decoding.
 * this is mainly needed because some optimized bitstream readers read 
 * 32 or 64 bit at once and could read over the end<br>
 * Note, if the first 23 bits of the additional bytes are not 0 then damaged
 * MPEG bitstreams could cause overread and segfault
 */
#define UU_INPUT_BUFFER_PADDING_SIZE 8

//IID for IUUCORE
DEFINE_GUID(IID_IUucore, 0x8bac9015, 0xad7a, 0x42dd, 0xa2, 0xc5, 0x29, 0x30, 0x41, 0xf0, 0x5f, 0xa2);

typedef struct {
	union {
		ULONG reserved[16];
		struct {
			ULONG	uBitrate;			// IN: not used
			ULONG	uFramerate;			// IN: not used
			ULONG	uGopSize;			// IN: not used
			ULONG	uWidth;				// IN: horizontal resolution of input image
			ULONG	uHeight;			// IN: vertical resolution of input image
			ULONG	uColorSpace;		// IN: colorspace of output image
			ULONG	uFlags;				// IN: UU_CSP_NONE or UU_CPF_LOW_DELAY
			ULONG	uSrcColorSpace;		// IN: colorspace of input image. This parameter is only required if you use UUCore library as deinterlacing library.
			ULONG	uDeinterlacer;		// IN: UU_DEINT_LOWPASS5 or UU_DEINT_MEDIAN
			ULONG	uResizingMethod;		// IN: interpolation method for resizing
			ULONG	uDstWidth;			// IN: horizontal resolution of output image
			ULONG	uDstHeight;			// IN: vertical resolution of output image
		} video;
	};
	union {
		ULONG reserved[16];
		struct {
			ULONG	uBitrate;
			ULONG	uSamplerate;
			ULONG	uChannels;
		} audio;
	};
} CodecProperty;

typedef struct {
	union {
		ULONG reserved[32];
		struct {
			BYTE*	pData[4];
			ULONG	uBytesPerLine[4];
			ULONG	uWidth;
			ULONG	uHeight;
			ULONG	uPictType;
			ULONG	uColorSpace;
			ULONG	uGotPicture;
			unsigned __int64 uTimeStamp;
			ULONG	uFieldNumber; 
		};
	};
} DecodedVideo;

#ifdef __cplusplus
extern "C"
{
#endif
//////////////////////////////////////////////////////////////////////////////////
// APIs
BOOL UUC_API UucGetVersion(ULONG pVersion[4]);
HANDLE UUC_API UucOpenCodec(ULONG  codecId, BOOL bEncoder);
BOOL UUC_API UucCloseCodec(HANDLE hCodec);
BOOL UUC_API UucSetParam(HANDLE hCodec, CodecProperty CodecProp);
BOOL UUC_API UucEncodeAudio(HANDLE hCodec, BYTE* pEncodedBuf, int nEncodedBufSize, int* pEncodedSize, short* pPcmData);
BOOL UUC_API UucDecodeAudio(HANDLE hCodec, short* pDecodedBuf, int* pDecodedFrameSize, int* pDecodedSize, BYTE* pEncodedBuf, int nEncodedSize);
BOOL UUC_API UucDecodeVideo(HANDLE hCodec, DecodedVideo* pDecodedBuf, int* pDecodedSize, BYTE* pEncodedBuf, int nEncodedSize, BOOL bDeinterlaced);
BOOL UUC_API UucFlushBuffer(HANDLE hCodec);
BOOL UUC_API UucDeinterlace(HANDLE hCodec, DecodedVideo* pDecodedBuf, BOOL bDeinterlace);
BOOL UUC_API UucCreateInstance(void **pUnknown);
#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
// Interface definition for IUUCORE
interface IUucore : IUnknown
{
	STDMETHOD_(BOOL,   UucGetVersion) (ULONG pVersion[4]) PURE;
	STDMETHOD_(HANDLE, UucOpenCodec)  (ULONG  codecId, BOOL bEncoder) PURE;
	STDMETHOD_(BOOL,   UucCloseCodec) (HANDLE hCodec) PURE;
	STDMETHOD_(BOOL,   UucSetParam)   (HANDLE hCodec, CodecProperty CodecProp) PURE;
	STDMETHOD_(BOOL,   UucEncodeAudio)(HANDLE hCodec, BYTE* pEncodedBuf, int nEncodedBufSize, int* pEncodedSize, short* pPcmData) PURE;
	STDMETHOD_(BOOL,   UucDecodeAudio)(HANDLE hCodec, short* pDecodedBuf, int* pDecodedFrameSize, int* pDecodedSize, BYTE* pEncodedBuf, int nEncodedSize) PURE;
	STDMETHOD_(BOOL,   UucDecodeVideo)(HANDLE hCodec, DecodedVideo* pDecodedBuf, int* pDecodedSize, BYTE* pEncodedBuf, int nEncodedSize, BOOL bDeinterlaced) PURE;
	STDMETHOD_(BOOL,   UucFlushBuffer)(HANDLE hCodec) PURE;
	STDMETHOD_(BOOL,   UucDeinterlace)(HANDLE hCodec, DecodedVideo* pDecodedBuf, BOOL bDeinterlace) PURE;
};
#endif

#pragma pack( pop, __UUCOREAPI_H )

#endif // __UUCOREAPI_H

