REM ==================================================================================================
REM [Parameters]
REM		%1 => OutputSubDir	EX) Source
REM		Note: OutputDir is implicitly set according to the "UDPREL" (environment variables)
REM			UDPREL exists => %UDPREL%/UMF/
REM			else	      => ../../DEMO/		
REM ==================================================================================================

@echo off

rem if "%1" == ""   goto USAGE

echo %TargetPath%

set UDPPATH=%UDPREL%
set CopyTargetFile="%TargetPath%"
set OutputSubDir=%1
set CopyTargetImportLib="%TargetDir%%TargetName%.lib"
set LibSubDir=%ProjectName%

echo .
echo "===== Start Postbuild Process ===== "


if "%UDPPATH%"=="" (goto MODE2) else (goto MODE1)

:MODE1
echo * UDPREL exists...
set OutputDir=%UDPPATH%\UMF

echo * CopyTargetFile=%CopyTargetFile%
echo * OutputSubDir=%OutputSubDir%
set CopyDstDir=%OutputDir%\%OutputSubDir%
if not exist %CopyDstDir% (mkdir %CopyDstDir%)

:COPY
if "%TargetExt%"==".lib" (goto COPYLIB)

copy %CopyTargetFile% %CopyDstDir%
echo * %CopyTargetFile% was copied to %CopyDstDir%


:MODE2
echo * Copy Demo Foloder ...
set OutputDir=..\..\DEMO
goto MAKEDSTDIR


:MAKEDSTDIR
echo * CopyTargetFile=%CopyTargetFile%
echo * OutputSubDir=%OutputSubDir%
set CopyDstDir=%OutputDir%\%OutputSubDir%
if not exist %CopyDstDir% (mkdir %CopyDstDir%)

:COPY
if "%TargetExt%"==".lib" (goto COPYLIB)

copy %CopyTargetFile% %CopyDstDir%
echo * %CopyTargetFile% was copied to %CopyDstDir%

if "%TargetExt%"==".dll" (goto COPYLIB)
goto EXIT

:COPYLIB
if "%OutputSubDir%"=="" (set CopyDstDir=..\..\LIB\%LibSubDir%) else (set CopyDstDir=..\..\LIB\%OutputSubDir%)

if not exist %CopyDstDir% (mkdir %CopyDstDir%)
copy %CopyTargetImportLib% %CopyDstDir%
echo * %CopyTargetImportLib% was copied to %CopyDstDir%
goto EXIT:

:COPYSTATICLIB
goto EXIT:


:EXIT
echo "===== Finish Postbuild Process ===== "
echo .
goto :eof

:USAGE
echo Error in script usage. The correct usage is:
echo     %0 CopyTargetFile OutputSubDir
echo:
echo For example:
echo     usage1 %0 subfolder (EX: SDKPostCopy.bat Source)
goto :eof

