// ErrCmnError.h: interface for the CErrCmnError class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ERRCMNERROR_H__E345B5E3_AFF1_49AB_997F_490545820CC2__INCLUDED_)
#define AFX_ERRCMNERROR_H__E345B5E3_AFF1_49AB_997F_490545820CC2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#include "DRVnDLLTypes.h"
#include "CapQueue.h"

#define MAX_ERRCMN_ERROR_QUEUE_SIZE				128

typedef enum {	ERR_LEVEL_NONE = 0,
				ERR_LEVEL_KERNEL,
				ERR_LEVEL_DLL,
				ERR_LEVEL_APP,
} ERR_LEVEL;

BOOL PushErrorCodes(ULONG code, ULONG minorCode=0, char* auxMsg="", char* session=NULL);
BOOL PushErrorCodesV(ULONG code, LPCSTR szFmt , ... );
BOOL PushErrorCodesV(LPCSTR szFmt , ... );

// forward decl.
class CErrCmnLog;

class CErrCmnError  
{
public:
	virtual BOOL	PushErrorCode(ULONG code, ULONG minorCode=0, char* auxMsg="", char* session=NULL) = 0;
	virtual BOOL	PushErrorCodeItem(ErrorCodeItem* pItem) = 0;
	virtual BOOL	PopErrorCodeItem(ErrorCodeItem* pItem) = 0;
	virtual void	EmptyErrorCode() = 0;

	void	MakeErrorCodeItem(ErrorCodeItem* pItem, ULONG code, ULONG minorCode, char* auxMsg, char* session);

	void	SetCurSession(char* session);
	char*	GetCurSession();

	char*	GetStrSeverity(ULONG code);
	char*	GetStrLevel(ULONG code);
	char*	GetStrHandling(ULONG code);

	void	SetLogger(CErrCmnLog* logger) {m_pLogger = logger;}

			CErrCmnError(ERR_LEVEL level);
	virtual ~CErrCmnError();

protected:
	void	CopyMsgString(char* dest, char* source, unsigned count);
	BOOL	PushErrorCode_i(ErrorCodeItem* pItem);
	BOOL	PopErrorCode_i(ErrorCodeItem* pItem);
	void	EmptyErrorCode_i();
	void	GetTimeStamp(LARGE_INTEGER* t);

	CCapQueue<ErrorCodeItem, MAX_ERRCMN_ERROR_QUEUE_SIZE>	m_Queue;
	char				m_curSessionName[MAX_ERRCMN_SESSION_LENGTH + 1];
	ERR_LEVEL			m_myLevel;
	CErrCmnLog*			m_pLogger;
};

class CErrCmnSessionGuard  
{
public:
	CErrCmnSessionGuard(CErrCmnError* pErr, char* session, ULONG bEmptyError = TRUE) : m_pErr(pErr)
	{
		if (m_pErr) {
			if (bEmptyError) {
				m_pErr->EmptyErrorCode();
			}
			m_pErr->SetCurSession(session);
		}
	}

	virtual ~CErrCmnSessionGuard()
	{
		if (m_pErr) {
			m_pErr->SetCurSession(NULL);
		}
	}
protected:
	CErrCmnError*	m_pErr;
};

#define ERR_SESSION(session) \
	CErrCmnSessionGuard sessionGuard(g_pCapError, NULL)
//	CErrCmnSessionGuard sessionGuard(g_pCapError, session)

#define ERR_SESSION_NO_EMPTY(session) \
	CErrCmnSessionGuard sessionGuard(g_pCapError, NULL, FALSE)
//	CErrCmnSessionGuard sessionGuard(g_pCapError, session, FALSE)

// _____________________________________________________________________________________________
// _____________________________________________________________________________________________
// _____________________________________________________________________________________________
// _____________________________________________________________________________________________

/*
 Severity	: [31..29]
			0 = Operation Success
			1 = Warning
			2 = Operarion Fail
			3 = Implementation Error

 Level		: [28..26]
			0 = None
			1 = Kernel
			2 = DLL
			3 = APP

 Handling	: [25..23]		// discarded field
			0 = None
			1 = xxx Driver Reinit
			2 = xxx Reboot
			3 = xxx Report manufacturer

 Reserved	: [22..20]

 Major Code	: [19..8]
	[19..17] : 0(0-511): common
	[19..17] : 1(511-1023): Video
	[19..17] : 2(1024-1535): Audio
	[19..17] : 3(1536-2047): Real-time display
	[19..17] : 4(?-?): Reserved
	[19..17] : 5(?-?): Reserved
	[19..17] : 6(?-?): Reserved
	[19..17] : 7(?-?): Reserved

 Minor Code	: [7..0]

*/

typedef enum {	
				ERR_SEVERITY_OPERATION_SUCCESS = 0,

				// 동작은 되나, 사용자가 의도한 바 대로 동작하지 않을 수 있다. (예: CapSetChannelSequence에서 loss된 채널을 넣었을때)
				ERR_SEVERITY_WARNING,

				// 일반적인 수행 실패이다. SUCCESS의 complementary라 볼 수 있다.
				ERR_SEVERITY_OPERATION_FAIL,

				// 절대로 수행되어서는 안되는 코드 또는 path가 수행되었을 때
				ERR_SEVERITY_IMPLEMENTATION_ERROR,
} ERR_SEVERITY;

typedef enum {	ERR_HANDLING_NONE = 0,
				// ERR_HANDLING_REINIT,
				// ERR_HANDLING_REBOOT,
				// ERR_HANDLING_REPORT_MANUFACTURER,
} ERR_HANDLING;

#define ERR_SEVERITY_FIELD_SIZE			3
#define ERR_SEVERITY_FIELD_POS			29

#define ERR_LEVEL_FIELD_SIZE			3
#define ERR_LEVEL_FIELD_POS				26

#define ERR_HANDLING_FIELD_SIZE			3
#define ERR_HANDLING_FIELD_POS			23

#define ERR_MAJOR_CODE_FIELD_SIZE		12
#define ERR_MAJOR_CODE_FIELD_POS		8

#define ERR_MINOR_CODE_FIELD_SIZE		8
#define ERR_MINOR_CODE_FIELD_POS		0

#define DEFINE_ERRCODE(severity, level, handling, majorcode)	(ULONG)(	\
	((severity & ((1 << ERR_SEVERITY_FIELD_SIZE) - 1)) << ERR_SEVERITY_FIELD_POS) | \
	((level & ((1 << ERR_LEVEL_FIELD_SIZE) - 1)) << ERR_LEVEL_FIELD_POS) | \
	((handling & ((1 << ERR_HANDLING_FIELD_SIZE) - 1)) << ERR_HANDLING_FIELD_POS) | \
	((majorcode & ((1 << ERR_MAJOR_CODE_FIELD_SIZE) - 1)) << ERR_MAJOR_CODE_FIELD_POS) \
	)

#define DEFINE_ERRCODE2(severity, majorcode)					(ULONG)(	\
	((severity & ((1 << ERR_SEVERITY_FIELD_SIZE) - 1)) << ERR_SEVERITY_FIELD_POS) | \
	((majorcode & ((1 << ERR_MAJOR_CODE_FIELD_SIZE) - 1)) << ERR_MAJOR_CODE_FIELD_POS) \
	)

#define EXTRACT_ERRCODE2(code)	(ULONG)( \
	(code & (((1 << ERR_SEVERITY_FIELD_SIZE) - 1) << ERR_SEVERITY_FIELD_POS)) | \
	(code & (((1 << ERR_MAJOR_CODE_FIELD_SIZE) - 1) << ERR_MAJOR_CODE_FIELD_POS)) \
	)

#define ADD_MINOR_ERRCODE(code, minorCode)	(	\
	((minorCode & ((1 << ERR_MINOR_CODE_FIELD_SIZE) - 1)) << ERR_MINOR_CODE_FIELD_POS) | \
	code	\
	)

#define REMOVE_MINOR_ERRCODE(code) ( \
	code & (~((((1 << ERR_MINOR_CODE_FIELD_SIZE) - 1)) << ERR_MINOR_CODE_FIELD_POS)) \
	)

#define ADD_LEVEL_TO_ERRCODE(code, level) (	\
	((level & ((1 << ERR_LEVEL_FIELD_SIZE) - 1)) << ERR_LEVEL_FIELD_POS) | \
	code	\
	)

#define ERRCODE_SEVERITY(code) ( \
	(code >> ERR_SEVERITY_FIELD_POS) & ((1 << ERR_SEVERITY_FIELD_SIZE) - 1) \
	)

#define ERRCODE_LEVEL(code) ( \
	(code >> ERR_LEVEL_FIELD_POS) & ((1 << ERR_LEVEL_FIELD_SIZE) - 1) \
	)

#define ERRCODE_HANDLING(code) ( \
	(code >> ERR_HANDLING_FIELD_POS) & ((1 << ERR_HANDLING_FIELD_SIZE) - 1) \
	)

#define ERRCODE_MAJOR_CODE(code) ( \
	(code >> ERR_MAJOR_CODE_FIELD_POS) & ((1 << ERR_MAJOR_CODE_FIELD_SIZE) - 1) \
	)

#define ERRCODE_MINOR_CODE(code) ( \
	(code >> ERR_MINOR_CODE_FIELD_POS) & ((1 << ERR_MINOR_CODE_FIELD_SIZE) - 1) \
	)

//################################################################################################
// ERR_SEVERITY_SUCCESS
//################################################################################################
// ERRCMN_S_OK
// OK
#define ERRCMN_S_OK							DEFINE_ERRCODE2(ERR_SEVERITY_OPERATION_SUCCESS, 0)
// ERRCMN_S_FALSE
// 논리적인 FALSE를 리턴할 경우 (COM 참고)
#define ERRCMN_S_FALSE						DEFINE_ERRCODE2(ERR_SEVERITY_OPERATION_SUCCESS, 1)
// ERRCMN_S_NO_MORE_ERROR
// 더 이상 에러가 없다
#define ERRCMN_S_NO_MORE_ERROR				DEFINE_ERRCODE2(ERR_SEVERITY_OPERATION_SUCCESS, 2)

//################################################################################################
// ERR_SEVERITY_WARNING
//################################################################################################

//################################################################################################
// ERR_SEVERITY_OPERATION_FAIL
//################################################################################################

//------------------------------------------------------------------------------------------------
// 	[19..17] : 0(0-511): common
//------------------------------------------------------------------------------------------------
// ERRCMN_F_INSUFFICIENT_RESOURCES
// 리소스 생성에 실패했을 때 (메모리를 제외한 모든 자원)
#define ERRCMN_F_INSUFFICIENT_RESOURCES		DEFINE_ERRCODE2(ERR_SEVERITY_OPERATION_FAIL, 0)

// ERRCMN_F_OUT_OF_MEMORY
// 메모리가 부족하다
#define ERRCMN_F_OUT_OF_MEMORY				DEFINE_ERRCODE2(ERR_SEVERITY_OPERATION_FAIL, 1)

// ERRCMN_F_OPERATION_UNSUCCESSFUL
// 일반적인 operation이 실패했을 때
#define ERRCMN_F_OPERATION_UNSUCCESSFUL		DEFINE_ERRCODE2(ERR_SEVERITY_OPERATION_FAIL, 2)

// ERRCMN_F_CANNOT_GET_ERROR_CODE
// error code를 가져오는 중 에러가 발생하였다
#define ERRCMN_F_CANNOT_GET_ERROR_CODE		DEFINE_ERRCODE2(ERR_SEVERITY_OPERATION_FAIL, 3)

// ERRCMN_F_NOT_IMPLEMENTED
// 구현되어 있지 않다.
#define ERRCMN_F_NOT_IMPLEMENTED			DEFINE_ERRCODE2(ERR_SEVERITY_OPERATION_FAIL, 4)

// ERRCMN_F_DRIVER_OPEN_FAIL
// 디바이스 드라이버 열기에 실패
#define ERRCMN_F_DRIVER_OPEN_FAIL			DEFINE_ERRCODE2(ERR_SEVERITY_OPERATION_FAIL, 5)

// ERRCMN_F_INVALID_PARAMETER
// 잘못된 파라메터
#define ERRCMN_F_INVALID_PARAMETER			DEFINE_ERRCODE2(ERR_SEVERITY_OPERATION_FAIL, 6)

// ERRCMN_F_IOCTL_INCORRECT_BUFFER_SIZE
// IOCTL 버퍼 크기가 잘못되었다.
#define ERRCMN_F_IOCTL_INCORRECT_BUFFER_SIZE	DEFINE_ERRCODE2(ERR_SEVERITY_OPERATION_FAIL, 7)

// ERRCMN_F_EXCEPTION_IN_CALLBACK
// callback function 함수 호출에서 exception이 일어났다.
#define ERRCMN_F_EXCEPTION_IN_CALLBACK			DEFINE_ERRCODE2(ERR_SEVERITY_OPERATION_FAIL, 8)

// ERRCMN_F_INVALID_IOCTL_CODE
// 잘못된 IOCTL
#define ERRCMN_F_INVALID_IOCTL_CODE			DEFINE_ERRCODE2(ERR_SEVERITY_OPERATION_FAIL, 9)

// ERRCMN_F_INVALID_CALLING_LEVEL
// 허용되지 않은 Process control call API를 불렀다.
#define ERRCMN_F_INVALID_CALLING_LEVEL		DEFINE_ERRCODE2(ERR_SEVERITY_OPERATION_FAIL, 10)

// ERRCMN_F_INVALID_POINTER
// 파라메터의 pointer 액세스가 실패
#define ERRCMN_F_INVALID_POINTER			DEFINE_ERRCODE2(ERR_SEVERITY_OPERATION_FAIL, 11)

// ERRCMN_F_INVALID_FUNCTION_POINTER
// function pointer의 액세스가 실패
#define ERRCMN_F_INVALID_FUNCTION_POINTER		DEFINE_ERRCODE2(ERR_SEVERITY_OPERATION_FAIL, 12)

// ERRCMN_F_OBSOLETE_FUNCTION
// Obsolete된 함수를 불렀다
#define ERRCMN_F_OBSOLETE_FUNCTION			DEFINE_ERRCODE2(ERR_SEVERITY_OPERATION_FAIL, 13)

// ERRCMN_F_INVALID_KEY_CODE
// Invalid key-code
#define ERRCMN_F_INVALID_KEY_CODE			DEFINE_ERRCODE2(ERR_SEVERITY_OPERATION_FAIL, 14)

//------------------------------------------------------------------------------------------------
//	[19..17] : 1(511-1023): Video
//------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------------
//	[19..17] : 2(1024-1535): Audio
//------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------------
//	[19..17] : 3(1536-2047): Real-time display
//------------------------------------------------------------------------------------------------

// ERRRT_F_DIRECTDRAW_SURFACE
// DirectDraw Overlay surface가 잘못되었다.
#define ERRRT_F_DIRECTDRAW_SURFACE			DEFINE_ERRCODE2(ERR_SEVERITY_OPERATION_FAIL, 1536)

// ERRRT_F_DIRECTDRAW_SURFACE_LOCK
// DirectDraw Overlay surface Lock 실패
#define ERRRT_F_DIRECTDRAW_SURFACE_LOCK		DEFINE_ERRCODE2(ERR_SEVERITY_OPERATION_FAIL, 1537)

//################################################################################################
// ERR_SEVERITY_IMPLEMENTATION_ERROR
//################################################################################################

// ERRCMN_E_INVALID_CODE_PATH
// 실행되저는 안되는 코드 패스가 수행되었다.
#define ERRCMN_E_INVALID_CODE_PATH			DEFINE_ERRCODE2(ERR_SEVERITY_IMPLEMENTATION_ERROR, 0)

// _____________________________________________________________________________________________
// _____________________________________________________________________________________________
// _____________________________________________________________________________________________
// _____________________________________________________________________________________________

#define MAX_ERRCMN_LOG_MSG_LENGTH		256

class CErrCmnLog  
{
public:
	enum {NO_LOGGING = 0, ERROR_LOGGING};

	virtual ULONG Log(char* logMsg) = 0;

	char*	ErrorToMsg(ErrorCodeItem* err);

			CErrCmnLog(int logLevel = 0);
	virtual ~CErrCmnLog();
protected:
	char	GetStrSeverity(ErrorCodeItem* err);
	char	GetStrLevel(ErrorCodeItem* err);
	char*	GetSession(ErrorCodeItem* err);
	ULONG	GetMajorCode(ErrorCodeItem* err);
	ULONG	GetMinorCode(ErrorCodeItem* err);
	char*	GetAuxMsg(ErrorCodeItem* err);
	char*	GetStrTimeStamp(ErrorCodeItem* err);

	int		m_logLevel;
	char	m_msg[MAX_ERRCMN_LOG_MSG_LENGTH + 1];
	char	m_strMajorCode[16];
	char	m_strTimeStamp[32];
};


// _____________________________________________________________________________________________
// _____________________________________________________________________________________________
// _____________________________________________________________________________________________
// _____________________________________________________________________________________________

class CErrCmnLogTrace : public CErrCmnLog  
{
public:
	virtual ULONG Log(char* logMsg);

	CErrCmnLogTrace(int logLevel = 0);
	virtual ~CErrCmnLogTrace();

};

#endif // !defined(AFX_ERRCMNERROR_H__E345B5E3_AFF1_49AB_997F_490545820CC2__INCLUDED_)
