#include "stdafx.h"

#include "inilib.h"
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
#define	INI_SECTION_CONFIG		"Config"
#define INI_SECTION_COMPANY		"Companies"
#define INI_SECTION_MODELS		"\\Models"

#define INI_KEY_SELECTED		"Selected"

#define INI_KEY_COMPANYNAME		"CompanyName"
#define INI_KEY_DLLNAME			"DllName"
#define INI_KEY_EXPORTFOLDER	"ExportFolder"
#define INI_KEY_EXPORTFILE		"ExportFile"
#define INI_KEY_DRIVERREG		"DriverReg"

#define INI_KEY_MODELID			"ModelID"
#define INI_KEY_MODELNAME		"ModelName"
#define INI_KEY_SYSNAME			"SysName"
#define INI_KEY_ACTIVATIONCODE	"ActivationCode"


void RemoveFilename(char * fn)
{
	int len=lstrlen(fn);
	while(len>0){
		len--;
		if(fn[len]=='\\'){
			fn[len+1]=0;
			break;
		}
	}
}


static BOOL FindFirstFile(char * wildcard_name,char * name_found)
{
	WIN32_FIND_DATA find={0,};
	HANDLE ff_handle=FindFirstFile(wildcard_name,&find);
	if(ff_handle!=INVALID_HANDLE_VALUE){
		do{
			if(!(find.dwFileAttributes&(FILE_ATTRIBUTE_DIRECTORY|FILE_ATTRIBUTE_COMPRESSED|FILE_ATTRIBUTE_HIDDEN))){
				lstrcpy(name_found,wildcard_name);
				RemoveFilename(name_found);
				lstrcat(name_found,find.cFileName);

				FindClose(ff_handle);
				return TRUE;
			}
		}while(FindNextFile(ff_handle,&find));
		FindClose(ff_handle);
	}
	return FALSE;

}

BOOL CINIModelInfoAPP::Init(char * f_name,BOOL f_find,BOOL f_fullpath)
{
	opened=FALSE;//invalidate...
	if(f_find){
		if(f_fullpath){
			opened = FindFirstFile(f_name,ini_name);
		}else{
			//1. current path..
			char temp_path[MAX_PATH];
			GetCurrentDirectory(MAX_PATH,temp_path);
			lstrcat(temp_path,"\\");
			lstrcat(temp_path,f_name);
			if(FindFirstFile(temp_path, ini_name)){
				opened=TRUE;
			}else{
				//2. excute file's path
				//jb GetModuleFileName(AfxGetInstanceHandle(),temp_path,MAX_PATH);
				GetModuleFileName(NULL,temp_path,MAX_PATH);
				RemoveFilename(temp_path);
				lstrcat(temp_path, f_name);
				if(FindFirstFile(temp_path, ini_name)){
					opened=TRUE;
				}else{
					//2. excute file's parent path..
					RemoveFilename(temp_path);
					temp_path[ lstrlen(temp_path)-1 ]=0;
					RemoveFilename(temp_path);
					lstrcat(temp_path,f_name);
					opened = FindFirstFile(temp_path,ini_name);
				}
			}
		}
		if(opened){
			if(SelectCompany()){
				return TRUE;
			}else{
				return FALSE;
			}
		}
	}else{
		if(SelectIniFile(f_name,f_fullpath)){
			opened = FALSE;
			if(SelectCompany()){
				return TRUE;
			}
		}
	}
	return FALSE;
}

BOOL CINIModelInfoAPP::GetCompanyData(char* key,char* buf,DWORD size)
{
	DWORD copied = GetPrivateProfileString(company_section,key,"",buf,size,ini_name);	
	
	if(copied>0)return TRUE;

	return FALSE;
}

BOOL CINIModelInfoAPP::GetCompanyDLL(char* buf,DWORD size)
{
	return GetCompanyData(INI_KEY_DLLNAME ,buf,size);
}
BOOL CINIModelInfoAPP::GetCompanyName(char* buf,DWORD size)
{
	return GetCompanyData(INI_KEY_COMPANYNAME,buf,size);
}

BOOL CINIModelInfoAPP::GetDriverReg(char* buf,DWORD size)
{
	return GetCompanyData(INI_KEY_DRIVERREG,buf,size);
}


BOOL CINIModelInfoAPP::SelectCompany()
{
	char buf[64];
	DWORD len = GetString(INI_SECTION_CONFIG,INI_KEY_SELECTED,buf,64);
	if(len){
		if(GetString(INI_SECTION_COMPANY,buf,company_section,64)){
			lstrcpy(company_section_models,company_section);
			lstrcat(company_section_models,INI_SECTION_MODELS);
			selected=TRUE;
			return TRUE;
		}
	}
	return FALSE;
}


BOOL CINIModelInfoAPP::SearchModelID(DWORD id)
{
	if(!selected)return FALSE;
	if(id==recent_model_id)return TRUE;

	
	models.SelectIniFile(ini_name,TRUE);
	int count=models.EnumSection_SelectSection(company_section_models);
	for(int i=0;i<count;i++){
		char model_section[256];
		if(models.EnumSection_GetString(i,model_section)){
			char buf[256];
			if(GetString(model_section,INI_KEY_MODELID,buf,256)>0){
				DWORD id_val;
				sscanf(buf,"%x",&id_val);
				if(id_val==id){
					recent_model_id=id;
					lstrcpy(searched_model_section,model_section);
					break;
				}
			}
		}
	}

	if(id==recent_model_id)
		return TRUE;
	else
		return FALSE;
}


BOOL CINIModelInfoAPP::GetSYSName(DWORD id,char* buf,DWORD size)
{
	if(SearchModelID(id)){
		DWORD copied = GetString(searched_model_section,INI_KEY_SYSNAME,buf,size);
		if(copied){
			return TRUE;
		}
	}
	return FALSE;
}


BOOL CINIModelInfoAPP::GetActivationCode(DWORD id,unsigned char* buf)
{
	if(SearchModelID(id)){
		char buff[256];
		DWORD ac[16];
		DWORD copied = GetString(searched_model_section,INI_KEY_ACTIVATIONCODE,buff,256);
		if(copied){
			if(sscanf(buff,"%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x",
				ac+ 0,ac+ 1,ac+ 2,ac+ 3,ac+ 4,ac+ 5,ac+ 6,ac+ 7,
				ac+ 8,ac+ 9,ac+10,ac+11,ac+12,ac+13,ac+14,ac+15)==16){
				for(int i=0;i<16;i++){
					buf[i]=(unsigned char)ac[i];
				}
				return TRUE;
			}
		}
	}
	return FALSE;
}

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////

DWORD CINIBase::GetString(char *sectoin_name,char* key_name,char * buf,DWORD buf_size)
{
	return GetPrivateProfileString(sectoin_name,key_name,"",buf,buf_size,ini_name);
}

DWORD CINIBase::WriteString(char *sectoin_name,char* key_name,char * buf)
{
	return WritePrivateProfileString(sectoin_name,key_name,buf,ini_name);
}



BOOL CINIBase::SelectIniFile(char* name,BOOL full_path)
{
	if(full_path==FALSE){
		//full path should be used.
		DWORD pre_defined=(DWORD)name;
		if(pre_defined<10){
			switch(pre_defined){
			case 1:	name="aud3.ini";break;
			case 2:	name="suez.ini";break;
			case 3:	name="kama.ini";break;
			case 4:	name="mizar.ini";break;
			}
		}
		GetCurrentDirectory(MAX_PATH,ini_name);
		lstrcat(ini_name,"\\");
		//GetModuleFileName(AfxGetInstanceHandle(),ini_name,MAX_PATH);
		//RemoveFilename(ini_name);
		lstrcat(ini_name,name);

		return TRUE;
	}else{
		lstrcpy(ini_name,name);
		return TRUE;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////


int CINISectionEnum::EnumSection_SelectSection(char * section_name)
{
	//allocate... use dynamic allocation...
	//for buffer invoke
	DWORD copied = GetPrivateProfileSection(section_name,buffer,ini_buffer_size ,ini_name);
	if(copied==0){
		idx=0;
		return 0;
	}
	char * buf = buffer;
	indexs[0]=0;
	idx=1;
	while(1){
		if(*buf==0){
			if(*(buf+1)==0)break;
			indexs[idx++]=buf-buffer+1;
		}else if(*buf=='='){
			*buf=0;
			indexs[idx++]=buf-buffer+1;
		}
		buf++;
	}
	return idx/2;
}

BOOL CINISectionEnum::EnumSection_GetString(int index,char * buf)
{
	lstrcpy(buf, &buffer[indexs[index*2+1]]);
	return TRUE;
}

BOOL CINISectionEnum::EnumSection_GetKey(int index,char * buf)
{
	lstrcpy(buf, &buffer[indexs[index*2]]);
	return TRUE;
}

BOOL CINISectionEnum::EnumSection_GetKeyAndString(int index,char * key,char* string)
{
	EnumSection_GetKey(index,key);
	EnumSection_GetString(index,string);
	return TRUE;
}


//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////

BOOL CINIModelInfoApplet::Init(char * f_name)
{
	lstrcpy(ini_name_short,f_name);
	return SelectIniFile(f_name);
}



DWORD CINIModelInfoApplet::EnumCompanies()
{
	companies.SelectIniFile(ini_name,TRUE);
	companies.EnumSection_SelectSection(INI_SECTION_COMPANY);
	return companies.EnumSection_GetCount();
}

BOOL CINIModelInfoApplet::GetCompanyName(int company_index,char* company_name,DWORD buf_size)
{
	if(company_index>=companies.EnumSection_GetCount())return FALSE;


	char company_section[256];
	companies.EnumSection_GetString(company_index,company_section);
	return GetString(company_section,INI_KEY_COMPANYNAME,company_name,buf_size);
}


DWORD CINIModelInfoApplet::EnumModels(int company_index)
{
	if(company_index>=companies.EnumSection_GetCount())return FALSE;
	char company_model_section[256];
	companies.EnumSection_GetString(company_index,company_model_section);
	lstrcat(company_model_section,INI_SECTION_MODELS);

	models.SelectIniFile(ini_name,TRUE);
	models.EnumSection_SelectSection(company_model_section);

	return models.EnumSection_GetCount();
}



BOOL CINIModelInfoApplet::GetModelName(int model_index,char * model_name,DWORD buf_size)
{
	if(model_index >= models.EnumSection_GetCount())return FALSE;


	char model_section[256];
	//models.EnumSection_GetString(model_index,mode_section);
	models.EnumSection_GetString(model_index,model_section);
	return GetString(model_section,INI_KEY_MODELNAME,model_name,buf_size);
}


BOOL CINIModelInfoApplet::SetSelectedCompany(int company_index)
{
	char buf[256];
	if(companies.EnumSection_GetKey(company_index,buf)){
		return WritePrivateProfileString(INI_SECTION_CONFIG,INI_KEY_SELECTED,buf,ini_name);
	}
	return FALSE;
}

#define	INI_SECTION_CONFIG		"Config"
#define INI_SECTION_COMPANY		"Companies"
#define INI_SECTION_MODELS		"\\Models"
#define INI_KEY_SELECTED		"Selected"

#define INI_KEY_COMPANYNAME		"CompanyName"
#define INI_KEY_DLLNAME			"DllName"
#define INI_KEY_EXPORTFOLDER	"ExportFolder"
#define INI_KEY_EXPORTFILE		"ExportFile"

#define INI_KEY_MODELID			"ModelID"
#define INI_KEY_MODELNAME		"ModelName"
#define INI_KEY_SYSNAME			"SysName"
#define INI_KEY_ACTIVATIONCODE	"ActivationCode"

////Most Comples Function in this file....	
BOOL CINIModelInfoApplet::ExportCompany(int company_index)
{
	//Set Folder
	//Set Name..
	BOOL specific_folder_exist=FALSE;
	BOOL specific_file_exist=FALSE;
	char export_folder[256];
	char export_file_append[256];
	
	//First find the names..
	if(company_index>=companies.EnumSection_GetCount())return FALSE;


	char company_section[256];
	companies.EnumSection_GetString(company_index,company_section);

	if(GetString(company_section,INI_KEY_EXPORTFOLDER,export_folder,256))
		specific_folder_exist=TRUE;

	if(GetString(company_section,INI_KEY_EXPORTFILE,export_file_append,256))
		specific_file_exist=TRUE;


	if(!specific_folder_exist) companies.EnumSection_GetKey(company_index,export_folder);
	if(!specific_file_exist)   companies.EnumSection_GetKey(company_index,export_file_append);

	//make the folder...
//	CreateDirectory(export_folder,NULL);

	char module_path[MAX_PATH];
	//GetModuleFileName(AfxGetInstanceHandle(),module_path,MAX_PATH);
	//RemoveFilename(module_path);

	GetCurrentDirectory(MAX_PATH,module_path);
	lstrcat(module_path,"\\");

	//lstrcat(module_path,"\\");
	lstrcat(module_path,export_folder);

	CreateDirectory(module_path,NULL);

	lstrcat(module_path,"\\");

	int len=lstrlen(ini_name_short);
	char * add=module_path+lstrlen(module_path);
	for(int i=0;i<=len;i++){
		if(ini_name_short[i]=='.'){
			lstrcat(add,export_file_append);
			add+=lstrlen(add);
		}
		*add++=ini_name_short[i];
		*add=0;
	}

	export.SelectIniFile(module_path,TRUE);

	char key[256],company[256],value[256],model[256];
	
	companies.EnumSection_GetKeyAndString(company_index,key,company);

	export.WriteString(INI_SECTION_CONFIG,INI_KEY_SELECTED,key);
	export.WriteString(INI_SECTION_COMPANY,key,company);

	GetString(company,INI_KEY_COMPANYNAME,value,256);	export.WriteString(company,INI_KEY_COMPANYNAME,value);
	GetString(company,INI_KEY_DLLNAME,value,256);		export.WriteString(company,INI_KEY_DLLNAME,value);

	lstrcat(company,INI_SECTION_MODELS);
	for(i=0;i<models.EnumSection_GetCount();i++){
		models.EnumSection_GetKeyAndString(i,key,model);
		export.WriteString(company,key,model);

		GetString(model,INI_KEY_MODELID,value,256);			export.WriteString(model,INI_KEY_MODELID,value);
		GetString(model,INI_KEY_MODELNAME,value,256);		export.WriteString(model,INI_KEY_MODELNAME,value);
		GetString(model,INI_KEY_SYSNAME,value,256);			export.WriteString(model,INI_KEY_SYSNAME,value);
		GetString(model,INI_KEY_ACTIVATIONCODE,value,256);	export.WriteString(model,INI_KEY_ACTIVATIONCODE,value);
	}

	return TRUE;
}


