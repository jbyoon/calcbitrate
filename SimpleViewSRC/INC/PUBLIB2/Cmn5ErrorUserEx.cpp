#include "..\stdafx.h"
#include "Cmn5ErrorUserEx.h"

extern CCmn5Error* g_pCapError;

BOOL PushErrorCodes(DWORD code, DWORD minorCode, char* auxMsg, char* session)
{
	TRACE((" - Error! code[%08X] [%s][%s]\n ", code, session,auxMsg));
	if (g_pCapError) {
		return g_pCapError->PushErrorCode(code,minorCode,auxMsg,session);
	} else {
		return FALSE;
	}
}

BOOL PushErrorCodesV(ULONG code, LPCSTR szFmt , ... )
{
    char szOutBuff [ CMN5_MAX_ERROR_CODE_MSG_LENGTH ] ;

    va_list  args ;
    va_start ( args , szFmt ) ;
    _vsnprintf ( szOutBuff , CMN5_MAX_ERROR_CODE_MSG_LENGTH-1, szFmt , args ) ;
    va_end ( args ) ;
	szOutBuff[CMN5_MAX_ERROR_CODE_MSG_LENGTH-1] = NULL;

	BOOL rs = FALSE;
	if (g_pCapError) {
		rs = g_pCapError->PushErrorCode(code,0,szOutBuff,g_pCapError->GetCurSession());
	}

	return rs;
}

BOOL PushErrorCodesV2(LPCSTR szFmt , ... )
{
    char szOutBuff [ CMN5_MAX_ERROR_CODE_MSG_LENGTH ] ;

    va_list  args ;
    va_start ( args , szFmt ) ;
    _vsnprintf ( szOutBuff , CMN5_MAX_ERROR_CODE_MSG_LENGTH-1, szFmt , args ) ;
    va_end ( args ) ;
	szOutBuff[CMN5_MAX_ERROR_CODE_MSG_LENGTH-1] = NULL;

	BOOL rs = FALSE;
	if (g_pCapError) {
		rs = g_pCapError->PushErrorCode(ERRCMN_F_OPERATION_UNSUCCESSFUL,0,szOutBuff,g_pCapError->GetCurSession());
	}

	return rs;
}

#if 1 // metalbrain
BOOL CCmn5ErrorUser::PushErrorCodes(DWORD code, DWORD minorCode, char* auxMsg, char* session)
{
	TRACE((" - Error! code[%08X] [%s][%s]\n ", code, session,auxMsg));
	return PushErrorCode(code,minorCode,auxMsg,session);
}

BOOL CCmn5ErrorUser::PushErrorCodesV(ULONG code, LPCSTR szFmt , ... )
{
    char szOutBuff [ CMN5_MAX_ERROR_CODE_MSG_LENGTH ] ;

    va_list  args ;
    va_start ( args , szFmt ) ;
    _vsnprintf ( szOutBuff , CMN5_MAX_ERROR_CODE_MSG_LENGTH-1, szFmt , args ) ;
    va_end ( args ) ;
	szOutBuff[CMN5_MAX_ERROR_CODE_MSG_LENGTH-1] = NULL;

	return PushErrorCode(code,0,szOutBuff,GetCurSession());
}

BOOL CCmn5ErrorUser::PushErrorCodesV2(LPCSTR szFmt , ... )
{
    char szOutBuff [ CMN5_MAX_ERROR_CODE_MSG_LENGTH ] ;

    va_list  args ;
    va_start ( args , szFmt ) ;
    _vsnprintf ( szOutBuff , CMN5_MAX_ERROR_CODE_MSG_LENGTH-1, szFmt , args ) ;
    va_end ( args ) ;
	szOutBuff[CMN5_MAX_ERROR_CODE_MSG_LENGTH-1] = NULL;
	
	return PushErrorCode(ERRCMN_F_OPERATION_UNSUCCESSFUL,0,szOutBuff,GetCurSession());
}
#endif

BOOL CCmn5ErrorUser::PushErrorCode(ULONG code, ULONG minorCode, char* auxMsg, char* session)
{
	CCmn5ErrGuardLock<CCmn5ErrCriticalSection> guard(m_lock);
	
	CMN5_ERROR_CODE_ITEM item;
	ZeroMemory(&item, sizeof(item));
	
	MakeErrorCodeItem(&item, code, minorCode, auxMsg, session);

	return PushErrorCode_i(&item);
}

BOOL CCmn5ErrorUser::PushErrorCodeItem(CMN5_ERROR_CODE_ITEM* pItem)
{
	CCmn5ErrGuardLock<CCmn5ErrCriticalSection> guard(m_lock);

	return PushErrorCode_i(pItem);
}

BOOL CCmn5ErrorUser::PopErrorCodeItem(CMN5_ERROR_CODE_ITEM* pItem)
{
	CCmn5ErrGuardLock<CCmn5ErrCriticalSection> guard(m_lock);

	RtlZeroMemory(pItem, sizeof(CMN5_ERROR_CODE_ITEM));
	return PopErrorCode_i(pItem);
}

void CCmn5ErrorUser::EmptyErrorCode()
{
	CCmn5ErrGuardLock<CCmn5ErrCriticalSection> guard(m_lock);

	EmptyErrorCode_i();
}

CCmn5ErrorUser::CCmn5ErrorUser(CMN5_ERR_LEVEL level) : CCmn5Error(level)
{

}

CCmn5ErrorUser::~CCmn5ErrorUser()
{

}
