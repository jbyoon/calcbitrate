// ErrCmnError.cpp: implementation of the CErrCmnError class.
//
//////////////////////////////////////////////////////////////////////

#include "..\stdafx.h"

#include "ErrCmnError.h"

void CErrCmnError::MakeErrorCodeItem(ErrorCodeItem* pItem, ULONG code, ULONG minorCode, char* auxMsg, char* session)
{
	RtlZeroMemory(pItem, sizeof(ErrorCodeItem));
	pItem->ErrorCode = ADD_MINOR_ERRCODE(code, minorCode);
	pItem->ErrorCode = ADD_LEVEL_TO_ERRCODE(pItem->ErrorCode, m_myLevel);
	GetTimeStamp(&pItem->TimeStamp);
	CopyMsgString(pItem->AuxMsg, auxMsg, MAX_ERRCMN_ERROR_CODE_MSG_LENGTH);
	if(session) {
		CopyMsgString(pItem->SessionName, session, MAX_ERRCMN_SESSION_LENGTH);
	} else {
		CopyMsgString(pItem->SessionName, GetCurSession(), MAX_ERRCMN_SESSION_LENGTH);
	}
}

BOOL CErrCmnError::PushErrorCode_i(ErrorCodeItem* pItem)
{
	if (m_pLogger) {
		m_pLogger->Log(m_pLogger->ErrorToMsg(pItem));
	}

	return m_Queue.Push(pItem);
}

BOOL CErrCmnError::PopErrorCode_i(ErrorCodeItem* pItem)
{
	return m_Queue.Pop(pItem);
}

void CErrCmnError::CopyMsgString(char* dest, char* source, unsigned count)
{
	if (NULL == dest || NULL == source)
		return;

	while (count && (*dest++ = *source++)) {
		count--;
	}

	if (count) {
		while (--count) {
			*dest++ = '\0';
		}
	}
}

char* CErrCmnError::GetStrSeverity(ULONG code)
{
	switch(ERRCODE_SEVERITY(code)) {
	case ERR_SEVERITY_OPERATION_SUCCESS:
		return "SUCCESS";
		break;
	case ERR_SEVERITY_WARNING:
		return "WARNING";
		break;
	case ERR_SEVERITY_OPERATION_FAIL:
		return "FAIL";
		break;
	case ERR_SEVERITY_IMPLEMENTATION_ERROR:
		return "IMPLEMENTATION_ERROR";
		break;
	default:
		return "Unspecified";
		break;
	}
}

char* CErrCmnError::GetStrLevel(ULONG code)
{
	switch(ERRCODE_LEVEL(code)) {
	case ERR_LEVEL_NONE:
		return "NONE";
		break;
	case ERR_LEVEL_KERNEL:
		return "KERNEL";
		break;
	case ERR_LEVEL_DLL:
		return "DLL";
		break;
	case ERR_LEVEL_APP:
		return "APP";
		break;
	default:
		return "Unspecified";
		break;
	}
}

char* CErrCmnError::GetStrHandling(ULONG code)
{
	switch(ERRCODE_HANDLING(code)) {
	case ERR_HANDLING_NONE:
		return "NONE";
		break;
	default:
		return "Unspecified";
		break;
	}
}

void CErrCmnError::SetCurSession(char* session)
{
	if(session) {
		CopyMsgString(m_curSessionName, session, MAX_ERRCMN_SESSION_LENGTH);
#if defined(USER_MODE_CODE)
//		TRACE(session);
//		TRACE(" SESSION\n");
#endif
	} else {
		CopyMsgString(m_curSessionName, "", MAX_ERRCMN_SESSION_LENGTH);
	}
}

char* CErrCmnError::GetCurSession()
{
	return m_curSessionName;
}

void CErrCmnError::EmptyErrorCode_i()
{
	m_Queue.RemoveAll();
}

void CErrCmnError::GetTimeStamp(LARGE_INTEGER* t)
{
#if defined(KERNEL_MODE_CODE)
	KeQuerySystemTime(t);
#else
	SYSTEMTIME systemTime;
	GetSystemTime(&systemTime);
	SystemTimeToFileTime(&systemTime, (FILETIME*)t);
#endif
}

CErrCmnError::CErrCmnError(ERR_LEVEL level) : m_myLevel(level)
{
	m_pLogger = NULL;
	RtlZeroMemory(m_curSessionName, sizeof(m_curSessionName));
	EmptyErrorCode_i();
}

CErrCmnError::~CErrCmnError()
{

}

// _____________________________________________________________________________________________
// _____________________________________________________________________________________________
// _____________________________________________________________________________________________
// _____________________________________________________________________________________________

char* CErrCmnLog::ErrorToMsg(ErrorCodeItem* err)
{
	RtlZeroMemory(m_msg, sizeof(m_msg));
	// Ex.
	// ERR: S/F/W/E level session major-code minor-code aux-msg time-stamp
	//_snprintf(m_msg, MAX_ERRCMN_LOG_MSG_LENGTH, "ERR: %c %c %s 0x%08x 0x%08x %s %s\n", 
	//	GetStrSeverity(err), GetStrLevel(err), GetSession(err), GetMajorCode(err), GetMinorCode(err),
	//	GetAuxMsg(err), GetStrTimeStamp(err));
	_snprintf(m_msg, MAX_ERRCMN_LOG_MSG_LENGTH, "ERR: %c %c %s %s %s\n", 
		GetStrSeverity(err), GetStrLevel(err), GetSession(err), GetAuxMsg(err), GetStrTimeStamp(err));

	return m_msg;
}

char CErrCmnLog::GetStrSeverity(ErrorCodeItem* err)
{
	switch(ERRCODE_SEVERITY(err->ErrorCode)) {
	case ERR_SEVERITY_OPERATION_SUCCESS:
		return 'S';
		break;
	case ERR_SEVERITY_WARNING:
		return 'W';
		break;
	case ERR_SEVERITY_OPERATION_FAIL:
		return 'F';
		break;
	case ERR_SEVERITY_IMPLEMENTATION_ERROR:
		return 'E';
		break;
	default:
		return 'X';
		break;
	}
}

char CErrCmnLog::GetStrLevel(ErrorCodeItem* err)
{
	switch(ERRCODE_LEVEL(err->ErrorCode)) {
	case ERR_LEVEL_NONE:
		return 'N';
		break;
	case ERR_LEVEL_KERNEL:
		return 'K';
		break;
	case ERR_LEVEL_DLL:
		return 'D';
		break;
	case ERR_LEVEL_APP:
		return 'A';
		break;
	default:
		return 'X';
		break;
	}
}

char* CErrCmnLog::GetSession(ErrorCodeItem* err)
{
	return err->SessionName;
}

ULONG CErrCmnLog::GetMajorCode(ErrorCodeItem* err)
{
	return ERRCODE_MAJOR_CODE(err->ErrorCode);
}

ULONG CErrCmnLog::GetMinorCode(ErrorCodeItem* err)
{
	return ERRCODE_MINOR_CODE(err->ErrorCode);
}

char* CErrCmnLog::GetAuxMsg(ErrorCodeItem* err)
{
	return err->AuxMsg;
}

char* CErrCmnLog::GetStrTimeStamp(ErrorCodeItem* err)
{
#if defined(KERNEL_MODE_CODE)
	_snprintf(m_strTimeStamp, 32, "0x%08x%08x", err->TimeStamp.HighPart, err->TimeStamp.LowPart);
#else
	FILETIME localTime;
	SYSTEMTIME systemTime;
	FileTimeToLocalFileTime((FILETIME*)&err->TimeStamp, &localTime);
	FileTimeToSystemTime(&localTime, &systemTime);
	_snprintf(m_strTimeStamp, 32, "%d-%d %d:%d:%d.%d", 
		systemTime.wMonth, systemTime.wDay, systemTime.wHour, systemTime.wMinute, systemTime.wSecond,
		systemTime.wMilliseconds);
#endif

	return m_strTimeStamp;
}

CErrCmnLog::CErrCmnLog(int logLevel) : m_logLevel(logLevel)
{

}

CErrCmnLog::~CErrCmnLog()
{

}

// _____________________________________________________________________________________________
// _____________________________________________________________________________________________
// _____________________________________________________________________________________________
// _____________________________________________________________________________________________

ULONG CErrCmnLogTrace::Log(char* logMsg)
{
#if defined(KERNEL_MODE_CODE)
	if (m_logLevel > NO_LOGGING) {
		DbgPrint("%s", logMsg);
	}
#else
	TRACE("%s", logMsg);
#endif

	return TRUE;
}

CErrCmnLogTrace::CErrCmnLogTrace(int logLevel) : CErrCmnLog(logLevel)
{

}

CErrCmnLogTrace::~CErrCmnLogTrace()
{

}
