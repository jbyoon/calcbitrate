#include "stdafx.h"
#include "CommonInterface.h"

int GetMediaTypeSize(MEDIATYPE_MULTICHANNEL& mc)
{
	int size=0;
	switch(mc.majorType){
	case MMEDIATYPE_VIDEO:	size=sizeof(MEDIATYPE_VIDEO);	break;
	case MMEDIATYPE_AUDIO:	size=sizeof(MEDIATYPE_AUDIO);	break;
	case MMEDIATYPE_STREAM:	size=sizeof(MEDIATYPE_STREAM);	break;
	case MMEDIATYPE_TEXT:	size=sizeof(MEDIATYPE_TEXT);	break;
	case MMEDIATYPE_AUX:	size=sizeof(MEDIATYPE_AUX);	break;
	}
	size*=mc.chCount;
	return size;
}

BOOL CopyMediaTypeMultiChannel(MEDIATYPE_MULTICHANNEL& dst,MEDIATYPE_MULTICHANNEL& src)
{
	int size=GetMediaTypeSize(src);
	dst=src;
	dst.chMediaTypes=(MEDIATYPE_BASE*)malloc(size);
	CopyMemory(dst.chMediaTypes,src.chMediaTypes,size);
	return TRUE;
}

BOOL UpdateMediaTypeMultiChannel(MEDIATYPE_MULTICHANNEL& dst,MEDIATYPE_MULTICHANNEL& src)
{
	int size=GetMediaTypeSize(src);
	MEDIATYPE_BASE* dstT = dst.chMediaTypes;
	dst=src;
	dst.chMediaTypes = dstT;
	CopyMemory(dst.chMediaTypes,src.chMediaTypes,size);
	return TRUE;
}

BOOL InitMultiChannel(MEDIATYPE_MULTICHANNEL & mc,PrismIIMediaType majorType,DWORD minorType,char* mediaName,DWORD mediaID,LONG chCount)
{
	mc.majorType=majorType;
	mc.minorType=minorType;
	lstrcpy(mc.mediaName,mediaName);
	mc.mediaID=mediaID;
	mc.chCount=chCount;
	int i;
	switch(majorType){
	case MMEDIATYPE_VIDEO:
	{
		mc.chVideoTypes=(MEDIATYPE_VIDEO*)calloc(chCount,sizeof(MEDIATYPE_VIDEO));
		for(i=0;i<chCount;i++){
			mc.chVideoTypes[i].majorType=majorType;
			mc.chVideoTypes[i].minorType=minorType;
		}
		return TRUE;
	}
	case MMEDIATYPE_AUDIO:
		mc.chAudioTypes=(MEDIATYPE_AUDIO*)calloc(chCount,sizeof(MEDIATYPE_AUDIO));
		for(i=0;i<chCount;i++){
			mc.chAudioTypes[i].majorType=majorType;
			mc.chAudioTypes[i].minorType=minorType;
		}
		return TRUE;
	case MMEDIATYPE_STREAM:
		mc.chStreamTypes=(MEDIATYPE_STREAM*)calloc(chCount,sizeof(MEDIATYPE_STREAM));
		for(i=0;i<chCount;i++){
			mc.chStreamTypes[i].majorType=majorType;
			mc.chStreamTypes[i].minorType=minorType;
		}
		return TRUE;
	case MMEDIATYPE_TEXT:
		mc.chTextTypes=(MEDIATYPE_TEXT*)calloc(chCount,sizeof(MEDIATYPE_TEXT));
		for(i=0;i<chCount;i++){
			mc.chTextTypes[i].majorType=majorType;
			mc.chTextTypes[i].minorType=minorType;
		}
		return TRUE;
	case MMEDIATYPE_AUX:
		mc.chAuxTypes=(MEDIATYPE_AUX*)calloc(chCount,sizeof(MEDIATYPE_AUX));
		for(i=0;i<chCount;i++){
			mc.chAuxTypes[i].majorType=majorType;
			mc.chAuxTypes[i].minorType=minorType;
		}
		return TRUE;
	}
	return FALSE;
}

static int g_mdTypeSize[]={
	sizeof(MEDIATYPE_VIDEO),
	sizeof(MEDIATYPE_AUDIO),
	sizeof(MEDIATYPE_STREAM),
	sizeof(MEDIATYPE_TEXT),
	sizeof(MEDIATYPE_AUX),
};

MEDIATYPE_BASE * GetChannelMediaType(MEDIATYPE_MULTICHANNEL* pmt,ULONG chIdx)
{
	return (MEDIATYPE_BASE *)(((DWORD)pmt->chMediaTypes)+g_mdTypeSize[pmt->majorType]*chIdx);
}


BOOL GetMajorTypeName(PrismIIMediaType majorType,char* strTypeName)
{
	char * name=NULL;
	switch(majorType){
	case MMEDIATYPE_VIDEO:name="Video";break;
	case MMEDIATYPE_AUDIO:name="Audio";break;
	case MMEDIATYPE_STREAM:name="Stream";break;
	case MMEDIATYPE_TEXT:name="Text";break;
	case MMEDIATYPE_AUX:name="Aux";break;
	}

	if(name){
		strcpy_s(strTypeName,strlen(name),name);
		return TRUE;
	}

	return FALSE;
}

BOOL GetMinorTypeName(PrismIIMediaType majorType,DWORD minorType,char* strTypeName)
{
	char* name=NULL;
	switch(majorType){
	case MMEDIATYPE_VIDEO:
		switch(minorType){
		case MEDIATYPE_VIDEO_RGB32:name="RGB32";break;
		case MEDIATYPE_VIDEO_RGB24:name="RGB24";break;
		case MEDIATYPE_VIDEO_RGB16:name="RGB16";break;
		case MEDIATYPE_VIDEO_RGB15:name="RGB15";break;
		case MEDIATYPE_VIDEO_YUY2:name="YUY2";break;
		case MEDIATYPE_VIDEO_YV12:name="YV12";break;
		case MEDIATYPE_VIDEO_YUYV:name="YUYV";break;
		case MEDIATYPE_VIDEO_DIVX:name="DIVX";break;
		case MEDIATYPE_VIDEO_MPEG2:name="MPEG2";break;
		case MEDIATYPE_VIDEO_MPEG4:name="MPEG4";break;
		case MEDIATYPE_VIDEO_H263:name="H263";break;
		case MEDIATYPE_VIDEO_DCP4:name="DCP4";break;
		case MEDIATYPE_VIDEO_NVS1:name="NVS1";break;
		case MEDIATYPE_VIDEO_MPJV:name="MPJV";break;
		case MEDIATYPE_VIDEO_MPJ4:name="MPJ4";break;
		case MEDIATYPE_VIDEO_MPJ2:name="MPJ2";break;
		case MEDIATYPE_VIDEO_MJPEG:name="MJPEG";break;
		case MEDIATYPE_VIDEO_H264:name="H264";break;
		case MEDIATYPE_VIDEO_H264QBOX:name="H264QBOX";break;
		case MEDIATYPE_VIDEO_MP4S:name="MP4S";break;
		}
		break;
	case MMEDIATYPE_AUDIO:
		switch(minorType){
		case MEDIATYPE_AUDIO_PCM:name="PCM";break;
		case MEDIATYPE_AUDIO_G723:name="G723";break;
		case MEDIATYPE_AUDIO_MPEG1_LAYER1:name="MPEG1_L1";break;
		case MEDIATYPE_AUDIO_MPEG1_LAYER2:name="MPEG1_L2";break;
		case MEDIATYPE_AUDIO_MPEG1_LAYER3:name="MPEG1_L3";break;
		case MEDIATYPE_AUDIO_MPEG1_LAYER2QBOX:name="MPEG1_L2QBOX";break;
		}
		break;
	case MMEDIATYPE_STREAM:
		switch(minorType){
		case MEDIATYPE_STREAM_MPEG2PROGRAM:name="MPG";break;
		}
		break;
	case MMEDIATYPE_TEXT:
		switch(minorType){
		case MEDIATYPE_TEXT_ANSI:name="ANSI";break;
		}
		break;
	case MMEDIATYPE_AUX:
		switch(minorType){
		case MEDIATYPE_AUX_SENSOR:name="SENSOR";break;
		case MEDIATYPE_AUX_MOTIONDETECTION:name="MD";break;
		}
		break;
	}
	if(name){
		strcpy(strTypeName,name);
		return TRUE;
	}

	return FALSE;
}


BOOL IsUncompressedVideo(DWORD vidFormat)
{
	if(
		vidFormat==MEDIATYPE_VIDEO_RGB32||
		vidFormat==MEDIATYPE_VIDEO_RGB24||
		vidFormat==MEDIATYPE_VIDEO_RGB16||
		vidFormat==MEDIATYPE_VIDEO_RGB15||
		vidFormat==MEDIATYPE_VIDEO_YUY2||
		vidFormat==MEDIATYPE_VIDEO_YV12||
		vidFormat==MEDIATYPE_VIDEO_YUYV
		)
		return TRUE;
	else
		return FALSE;
}
