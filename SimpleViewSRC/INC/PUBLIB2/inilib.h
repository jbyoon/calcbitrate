#ifndef _INILIB_H_
#define _INILIB_H_
// CIniDlg dialog
const ini_buffer_size = 2048;
const max_key_count = 128;
#define INI_AUD3	1
#define INI_SUEZ	2
#define INI_KAMA	3
#define INI_MIZAR	4

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////

class CINIBase{
public:
	CINIBase():opened(0){}

	//mothods...
	BOOL   SelectIniFile(char*,BOOL full_path=FALSE);
	char * GetIniName(){return ini_name;}
	DWORD  GetString(char *sectoin_name,char* key_name,char * buf,DWORD buf_size);
	DWORD WriteString(char *sectoin_name,char* key_name,char * buf);
	//DWORD GetInt(char *sectoin_name,char* key_name,char * buf,DWORD buf_size);
protected: 
	BOOL opened;// file name assigned..
	char ini_name[MAX_PATH];
};

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////

class CINISectionEnum:public CINIBase{
public:
	CINISectionEnum():CINIBase(),idx(0){}

	int  EnumSection_SelectSection(char * section_name);
	BOOL EnumSection_GetKey(int index,char * buf);
	BOOL EnumSection_GetString(int index,char * buf);
	BOOL EnumSection_GetKeyAndString(int index,char * key,char* string);
	int  EnumSection_GetCount(){return idx/2;}
protected: 
	int  idx;
	char buffer[ini_buffer_size];
	int  indexs[max_key_count];
};

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////


class CINIModelInfoAPP:public CINIBase{
//methods for 
public:
	CINIModelInfoAPP():CINIBase(),selected(FALSE),recent_model_id(0){}

	BOOL Init(char * f_name,BOOL f_use_wildcard=FALSE,BOOL f_fullpath=FALSE);
	BOOL IsInited(){return (opened&&selected);}


	BOOL GetCompanyDLL(char* dll_name,DWORD buf_size);
	BOOL GetCompanyName(char* company_name,DWORD buf_size);
	BOOL GetDriverReg(char* reg_name,DWORD buf_size);

	BOOL GetSYSName(DWORD model_id,char* sys_name,DWORD buf_size);
	BOOL GetActivationCode(DWORD model_id,unsigned char* code);

private:
	BOOL SearchModelID(DWORD model_id);
	BOOL GetCompanyData(char* key,char* buf,DWORD size);
	CINISectionEnum models;
	
	BOOL SelectCompany();

	BOOL selected;
	char company_section[64];
	char company_section_models[64];

	DWORD recent_model_id;
	char searched_model_section[64];
};

class CINIModelInfoApplet:private CINIBase{
public:
	CINIModelInfoApplet():CINIBase(),companies(),models(),export(){}
	BOOL Init(char * f_name);
	DWORD EnumCompanies();//returns the number of companies
	BOOL GetCompanyName(int company_index,char* company_name,DWORD buf_size);
	DWORD EnumModels(int company_index);//returns the number of models for the company.
	BOOL GetModelName(int model_index,char * model_name,DWORD buf_size);
	BOOL SetSelectedCompany(int company_index);//write selected company to the ini file.
	
	BOOL ExportCompany(int company_index);
private:
	CINISectionEnum companies;
	CINISectionEnum models;
	CINIBase export;
	char ini_name_short[MAX_PATH];
};

void RemoveFilename(char * fn);

#endif