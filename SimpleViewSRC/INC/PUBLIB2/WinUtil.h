#ifndef _WINUTIL_H_
#define _WINUTIL_H_

#ifndef	ARRAYSIZE
#define ARRAYSIZE(c) (sizeof(c)/sizeof(c[0]))
#endif

#ifndef ZERO_VARIABLE
#define ZERO_VARIABLE(c) ZeroMemory(&c,sizeof(c))
#endif

#ifndef ZERO_ARRAY
#define ZERO_ARRAY(c)	ZeroMemory(c,sizeof(c))
#endif

#ifndef SAFE_RELEASE
#define SAFE_RELEASE(c) if(c){c->Release();c=NULL;}else{}
#endif

#ifndef INIT_dwSize_STRUCT
#define INIT_dwSize_STRUCT(x) (ZeroMemory(&(x), sizeof(x)), (x).dwSize=sizeof(x))
#endif

#ifndef SAFE_DELETE
#define SAFE_DELETE(p)       if(p) { delete (p);     (p)=NULL; }else{}
#endif

#ifndef SAFE_DELETE_ARRAY
#define SAFE_DELETE_ARRAY(p) if(p) { delete[] (p);   (p)=NULL; }else{}
#endif

#ifndef TRACE
#ifdef _DEBUG
#define TRACE              OutputDebugStringF
#else
#define TRACE              1 ? (void)0 : OutputDebugStringF
#endif
#endif

#ifndef GetTimeStampStart
#if !defined(_WIN64)
	#define GetTimeStampStart(x) { _asm push eax  _asm push edx _asm rdtsc __asm mov dword ptr x,eax __asm mov dword ptr x+4,edx _asm pop edx _asm pop eax}
#else
	#define GetTimeStampStart(x) { x = __rdtsc();}
#endif
#endif

#ifndef GetTimeStampEnd
#if !defined(_WIN64)
	#define GetTimeStampEnd(x) {  _asm push eax _asm push edx _asm rdtsc __asm sub eax,dword ptr x __asm sbb edx,dword ptr x+4 __asm mov dword ptr x,eax __asm mov dword ptr x+4,edx _asm pop edx _asm pop eax}
#else 
	#define GetTimeStampEnd(x) { x = __rdtsc() - x;}
#endif
#endif

#define TBSTYLE_FREESIZE	(TBSTYLE_FLAT|TBSTYLE_TRANSPARENT|TBSTYLE_TOOLTIPS|WS_CHILD |CCS_NODIVIDER|CCS_NOPARENTALIGN|CCS_NORESIZE)
#define TB_SEPERATOR		{-1,  -1,TBSTATE_ENABLED   ,TBSTYLE_SEP ,0,0}
#define TBSTATE_DISABLED	0


#define ARRANGEFLAG_HORZ		0x01
#define ARRANGEFLAG_VERT		0x02
#define ARRANGEFLAG_ABSOLUTE	0x04
#define ARRANGEFLAG_RELATIVE	0x08
#define ARRANGEFLAG_DEFAULT		(ARRANGEFLAG_HORZ|ARRANGEFLAG_RELATIVE)

#define AWTD_LEFT		0x01
#define AWTD_RIGHT		0x02
#define AWTD_HCENTER	0x04
#define AWTD_TOP		0x08
#define AWTD_BOTTOM		0x10
#define AWTD_VCENTER	0x20

#define APP_REG_PATH	"Software\\UDP\\PrismII"

BOOL	WUSetAppInstanceHandle(HINSTANCE hInst);
void	MoveWindowToCenterRect(HWND hwnd,RECT& rc,POINT& offset,UINT flags=0,HWND hWndInsertAfter=NULL);
BOOL	AnchorWindowToDesktop(HWND hwnd,DWORD pos);
BOOL	GetDlgItemProperties(HWND hDlg,UINT id,HFONT&hfont,DWORD&style,DWORD &exstyle,RECT &rc,HWND* phwnd=NULL,char* classname=NULL);
HWND	MyCreateWindow(LPCSTR clsname,LPCSTR title,DWORD style,DWORD exstyle,RECT&rc,HWND parent,UINT id,HFONT* hfont=NULL);
BOOL	DuplicateUnnamedControl(HWND hdlg,UINT id,int count,DWORD flags=ARRANGEFLAG_DEFAULT,int cxy=1);
BOOL	DuplicateNamedControl(HWND hdlg,UINT id,int count,char* title, DWORD flags=ARRANGEFLAG_DEFAULT,int cxy=1);
BOOL	CreateParametricNamedStaticControlAlign(HWND hdlg,UINT id,int count,char *title_format,DWORD align_where);
BOOL	DuplicateParametricNamedStaticControl(HWND hdlg,UINT id,int count,char* title_format, DWORD flags=ARRANGEFLAG_DEFAULT,int cxy=1);
BOOL	GetChildWindowRect(HWND hwnd,RECT* pRect);
BOOL	GetDlgItemRect(HWND hDlg,UINT id,RECT* prc);
BOOL	GetClientRectInScreen(HWND hwnd,RECT* prc);
BOOL	ShowDlgItem(HWND hdlg,UINT id,int nCmdShow);
BOOL	ShowDlgItems(HWND hdlg,int nCmdShow,int count,...);
BOOL	EnableDlgItem(HWND hdlg,UINT id,BOOL bEnable);
BOOL	EnableDlgItems(HWND hdlg,BOOL bEnable,int count,...);
HMENU	CreateBitmapMenu2(UINT uID, HBITMAP *paHbm,char **names,int indexs[],int cItems);
HBITMAP CreateDeviceCompatibleBitmap(int width,int height);
BOOL	GetFolder(char * strSelectedFolder,const char* lpszTitle,const HWND hwndOwner, const char* strRootFolder, const char* strStartFolder);
BOOL	GetFileVersionString(HINSTANCE hInst,char *szResult);
BOOL	DeleteFolder(const char * folderName,__int64* deletedSize = NULL);

#ifdef _INC_COMMCTRL
BOOL	ListView_InsertColumns(HWND hwnd,int count,...);
BOOL	ListView_InsertItemWithSubItems(HWND hwnd,UINT index,char* itemText,int subItemCount,...);
int		ListView_GetSelectedItem(HWND hwnd);
BOOL	GetBitmapHandles(HIMAGELIST himl,HBITMAP bmps[],int count);
BOOL	DropDownToolbarMenu(HWND hWnd,LPNMTOOLBAR lpnmTB,HMENU hMenuLoaded);
#endif


__int64 GetFrequency();

BOOL AppQueryRegSetBinary(LPCTSTR lpszValue, BYTE *pV,ULONG nSize,LPCSTR appRegPath=APP_REG_PATH);
BOOL AppQueryRegGetBinary(LPCTSTR lpszValue, BYTE *pV,ULONG nSize,LPCSTR appRegPath=APP_REG_PATH);

const char* GetFilename(const char * fn);
void RemoveExtension(char * fn);
void RemoveFilename(char * fn);

void OutputDebugStringF(char * format,...);

#ifdef __cplusplus
//inline functions..
void inline SetBit(DWORD& v,int index,DWORD bit)
{
	DWORD mask=1<<index;
	DWORD mask2=bit<<index;
	v=(v&(~mask))|mask2;
}

class CFindFile{
public:
	CFindFile(const char * folderName,const char * fileName,DWORD excludeAttr=FILE_ATTRIBUTE_DIRECTORY|FILE_ATTRIBUTE_COMPRESSED|FILE_ATTRIBUTE_HIDDEN);
	BOOL FindNext(char* fileName,WIN32_FIND_DATA* pFound=NULL);
	~CFindFile();
private:
	WIN32_FIND_DATA m_find;
	DWORD m_stage;
	HANDLE m_handle;
	DWORD m_exclAttr;
	char m_pathName[MAX_PATH];
};


class CMultiChannelFPS{
	int* chFPS;	// fequence per second
	int* chFPM;	// fequence per minute
	int* chKBPS;
	int* chKBPM;
	int secCount;
	LARGE_INTEGER freq, startPoint, freqMin, startPointMin;
	LARGE_INTEGER bpsStartPoint, bpsStartPointMin;
	CRITICAL_SECTION m_Crit;
public:	

	int* chFPS_copy;	// fequence per second
	int* chFPM_copy;	// fequence per second
	int* chKBPS_copy;
	int* chKBPM_copy;

	CMultiChannelFPS(ULONG n);
	~CMultiChannelFPS();
	ULONG m_nCh;
	void AddFrame(int channel, int frame=1);
	void AddLength(int channel, int kbytes);
	void Reset();
};
#endif //__cplusplus

#endif