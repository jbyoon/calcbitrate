// ErrCmnErrorUser.cpp: implementation of the CErrCmnErrorUser class.
//
//////////////////////////////////////////////////////////////////////

#include "..\stdafx.h"
#include "ErrCmnErrorUser.h"

CErrCmnError* g_pCapError = NULL;

BOOL PushErrorCodes(DWORD code, DWORD minorCode, char* auxMsg, char* session)
{
	TRACE((" - Error! code[%08X] [%s][%s]\n ", code, session,auxMsg));
	if (g_pCapError) {
		return g_pCapError->PushErrorCode(code,minorCode,auxMsg,session);
	} else {
		return FALSE;
	}
}

BOOL PushErrorCodesV(ULONG code, LPCSTR szFmt , ... )
{
    char szOutBuff [ MAX_ERRCMN_ERROR_CODE_MSG_LENGTH ] ;

    va_list  args ;
    va_start ( args , szFmt ) ;
    _vsnprintf ( szOutBuff , MAX_ERRCMN_ERROR_CODE_MSG_LENGTH-1, szFmt , args ) ;
    va_end ( args ) ;
	szOutBuff[MAX_ERRCMN_ERROR_CODE_MSG_LENGTH-1] = NULL;

	BOOL rs = FALSE;
	if (g_pCapError) {
		rs = g_pCapError->PushErrorCode(code,0,szOutBuff,g_pCapError->GetCurSession());
	}

	return rs;
}

BOOL PushErrorCodesV(LPCSTR szFmt , ... )
{
    char szOutBuff [ MAX_ERRCMN_ERROR_CODE_MSG_LENGTH ] ;

    va_list  args ;
    va_start ( args , szFmt ) ;
    _vsnprintf ( szOutBuff , MAX_ERRCMN_ERROR_CODE_MSG_LENGTH-1, szFmt , args ) ;
    va_end ( args ) ;
	szOutBuff[MAX_ERRCMN_ERROR_CODE_MSG_LENGTH-1] = NULL;

	BOOL rs = FALSE;
	if (g_pCapError) {
		rs = g_pCapError->PushErrorCode(ERRCMN_F_OPERATION_UNSUCCESSFUL,0,szOutBuff,g_pCapError->GetCurSession());
	}

	return rs;
}

BOOL CErrCmnErrorUser::PushErrorCode(ULONG code, ULONG minorCode, char* auxMsg, char* session)
{
	CErrCmnGuardLock<CErrCmnCriticalSection> guard(m_lock);
	
	ErrorCodeItem item;
	ZeroMemory(&item, sizeof(item));
	
	MakeErrorCodeItem(&item, code, minorCode, auxMsg, session);

	return PushErrorCode_i(&item);
}

BOOL CErrCmnErrorUser::PushErrorCodeItem(ErrorCodeItem* pItem)
{
	CErrCmnGuardLock<CErrCmnCriticalSection> guard(m_lock);

	return PushErrorCode_i(pItem);
}

BOOL CErrCmnErrorUser::PopErrorCodeItem(ErrorCodeItem* pItem)
{
	CErrCmnGuardLock<CErrCmnCriticalSection> guard(m_lock);

	RtlZeroMemory(pItem, sizeof(ErrorCodeItem));
	return PopErrorCode_i(pItem);
}

void CErrCmnErrorUser::EmptyErrorCode()
{
	CErrCmnGuardLock<CErrCmnCriticalSection> guard(m_lock);

	EmptyErrorCode_i();
}

CErrCmnErrorUser::CErrCmnErrorUser(ERR_LEVEL level) : CErrCmnError(level)
{

}

CErrCmnErrorUser::~CErrCmnErrorUser()
{

}
