#ifndef _PRISMII_COMMONINTERFACE_H_
#define _PRISMII_COMMONINTERFACE_H_
#include <windows.h>
#include <Mmreg.h>
#include <Mmsystem.h>
 
#define MAX_MEDIA	10


//#define PRISMII_MEDIATYPE_
// bit-position idx
#define CAPAPI_DLL_INDEX	0
#define AUDAPI_DLL_INDEX	1
#define OVRAPI_DLL_INDEX	2
#define CODAPI_DLL_INDEX	3
#define NETAPI_DLL_INDEX	4
#define DECAPI_DLL_INDEX	5

#define PRISMEXTDLL_CATEGORY_CARDMANAGER	1
#define PRISMEXTDLL_CATEGORY_FILTER			2
#define PRISMEXTDLL_CATEGORY_PLUGIN			3
#define PRISMEXTDLL_CATEGORY_EVENTMANAGER	4


typedef struct{
	DWORD extCategory;
	char extName[32];
	char extDesc[128];
	char extFile[MAX_PATH];
	HMODULE hLib;
}PRISMEXTDLLINFO;

//common interfaces...
MIDL_INTERFACE("02AD1585-B94B-4ce9-A68E-01BE7C2D7702")
IProcessCtrl: public IUnknown
{
public:
    virtual HRESULT STDMETHODCALLTYPE ProcessSetup() = 0;
    virtual HRESULT STDMETHODCALLTYPE ProcessEndup() = 0;
    virtual HRESULT STDMETHODCALLTYPE ProcessRun() = 0;			//apply changed setting values.
    virtual HRESULT STDMETHODCALLTYPE ProcessStop() = 0;
    virtual HRESULT STDMETHODCALLTYPE SaveSetting() = 0;
    virtual HRESULT STDMETHODCALLTYPE RestoreSetting() = 0;
    virtual HRESULT STDMETHODCALLTYPE ResetSetting() = 0;//default..
	virtual BOOL    STDMETHODCALLTYPE ShowPropertyPage(HWND hWndSite,RECT rcPage,DWORD v1,DWORD v2,DWORD v3) = 0;
	virtual BOOL    STDMETHODCALLTYPE HidePropertyPage() = 0;
	virtual HRESULT STDMETHODCALLTYPE GetProcessState(UINT* pProcessState) = 0;//default..
	virtual HRESULT STDMETHODCALLTYPE ProcessCommand(UINT uCmd,BOOL *bCheckState) = 0;
	virtual HRESULT STDMETHODCALLTYPE ProcessTimer(UINT nTimerID) = 0;
};

MIDL_INTERFACE("FAE6E67E-B140-45a8-86BF-BFB77DBB0E75")
//IProcessCtrlV2 :public IProcessCtrl
IProcessCtrlV2 :public IUnknown
{	
public:
	virtual HRESULT STDMETHODCALLTYPE ProcessAfterSetup() {return S_OK;}	//
    virtual HRESULT STDMETHODCALLTYPE ProcessAfterRun() {return S_OK;};	//2009.11.03 jbyoon to reflesh proptery page by process state
	virtual HRESULT STDMETHODCALLTYPE ProcessAfterStop() {return S_OK;};	//2009.11.03 jbyoon to reflesh proptery page by process state
	virtual HRESULT STDMETHODCALLTYPE ProcessAfterEndup() {return S_OK;};	//
};

MIDL_INTERFACE("5B1717D9-40F3-4d0e-9997-93ADB39994B4")
IPrismIIWindowManage: public IUnknown
{
public:
	virtual DWORD STDMETHODCALLTYPE GetWindowCount() = 0;
	virtual BOOL STDMETHODCALLTYPE IsResizable(ULONG idx)=0;
	virtual BOOL STDMETHODCALLTYPE GetFixedSize(ULONG idx,RECT* prc)=0;
	virtual BOOL STDMETHODCALLTYPE AttachWindow(ULONG idx,HWND hWnd,RECT rc) = 0;
	virtual BOOL STDMETHODCALLTYPE DetachWindow(ULONG idx) = 0;
	virtual BOOL STDMETHODCALLTYPE MoveWindow(ULONG idx,RECT rc) = 0;
	virtual BOOL STDMETHODCALLTYPE GetWindowText(ULONG idx,char name[256])=0;
	virtual BOOL STDMETHODCALLTYPE SetVisible(ULONG idx,BOOL)=0;
	virtual HWND STDMETHODCALLTYPE GetWindowHandle(ULONG idx)=0;

};

enum SSVType{
SSVT_DWORD= 1,
SSVT_BIN  = 2,
SSVT_TEXT = 3,
};

MIDL_INTERFACE("5B1717D9-40F3-4d0e-9997-93ADB39994B5")
ISettingStorage: public IUnknown
{
    virtual HRESULT STDMETHODCALLTYPE Read(LPCSTR name,SSVType* pvType,DWORD* len,void* data) = 0;
    virtual HRESULT STDMETHODCALLTYPE Write(LPCSTR name,SSVType vType,DWORD len,void* data) = 0;
    virtual HRESULT STDMETHODCALLTYPE MakeStorage(LPCSTR name,ISettingStorage** pStorage) = 0;
    virtual HRESULT STDMETHODCALLTYPE GetStorage(LPCSTR name,ISettingStorage** pStorage) = 0;
    virtual HRESULT STDMETHODCALLTYPE Reset() = 0;
};

MIDL_INTERFACE("5B1717D9-40F3-4d0e-9997-93ADB39994B6")
IMessageManager: public IUnknown
{
    virtual HRESULT STDMETHODCALLTYPE Show(BOOL bShow)= 0;
	virtual HRESULT STDMETHODCALLTYPE PushErrorCodeF(const char* format,...)= 0;
	virtual HRESULT STDMETHODCALLTYPE PushErrorCodeItem(void* errItem)= 0;		// CMN5_ERROR_CODE_ITEM
	virtual HRESULT STDMETHODCALLTYPE PushErrorCode(ULONG code, ULONG minorCode, const char* auxMsg,const  char* session)= 0;
};


#define LOG_ERR		0
#define LOG_WARN	1
#define LOG_INFO	2


MIDL_INTERFACE("B237D4DE-7E77-4c25-943D-ACF4C84FD2E5")
ILogManager: public IUnknown
{
	virtual HRESULT STDMETHODCALLTYPE LogMessageF(IProcessCtrl* pComp,ULONG code, const char * logMsg,...)= 0;
	virtual HRESULT STDMETHODCALLTYPE LogMessageF2(IProcessCtrl* pComp,ULONG code, BOOL bPrefix, BOOL bLinefeed, const char * logMsg,...)= 0;
};


////////////////////////////////////////////////////////////////////////
//filter////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

MIDL_INTERFACE("CE2BEB10-12E3-4615-9551-F8AB0DD00D45")
IPrismIIDataCallback : public IUnknown
{
public:
    virtual HRESULT STDMETHODCALLTYPE GetMediaCategory() = 0;
    virtual HRESULT STDMETHODCALLTYPE SetReleaseMethod() = 0;
    virtual HRESULT STDMETHODCALLTYPE Receive() = 0;
};

//FOR ime...

////////////////
enum PrismIIMediaType{
	MMEDIATYPE_VIDEO=0,
	MMEDIATYPE_AUDIO=1,
	MMEDIATYPE_STREAM=2,
	MMEDIATYPE_TEXT=3,
	MMEDIATYPE_AUX=4,
};


//VIDEO
#define MEDIATYPE_VIDEO_RGB32				0x01
#define MEDIATYPE_VIDEO_RGB24				0x02
#define MEDIATYPE_VIDEO_RGB16				0x03
#define MEDIATYPE_VIDEO_RGB15				0x04
#define MEDIATYPE_VIDEO_YUY2				mmioFOURCC('Y','U','Y','2')
#define MEDIATYPE_VIDEO_YV12				mmioFOURCC('Y','V','1','2')
#define MEDIATYPE_VIDEO_YUYV				mmioFOURCC('Y','U','Y','V')
#define MEDIATYPE_VIDEO_DIVX				mmioFOURCC('D','I','V','X')
#define MEDIATYPE_VIDEO_MPEG2				mmioFOURCC('M','P','G','2')
#define MEDIATYPE_VIDEO_MPEG4				mmioFOURCC('M','P','G','4')
#define MEDIATYPE_VIDEO_H263				mmioFOURCC('H','2','6','3')
#define MEDIATYPE_VIDEO_DCP4				mmioFOURCC('D','C','P','4')
#define MEDIATYPE_VIDEO_NVS1				mmioFOURCC('N','V','S','1')
#define MEDIATYPE_VIDEO_MPJV				mmioFOURCC('M','P','J','V')
#define MEDIATYPE_VIDEO_MPJ4				mmioFOURCC('M','P','J','4')
#define MEDIATYPE_VIDEO_MPJ2				mmioFOURCC('M','P','J','2')
#define MEDIATYPE_VIDEO_MJPEG				mmioFOURCC('M','J','P','G')
#define MEDIATYPE_VIDEO_H264				mmioFOURCC('H','2','6','4')
#define MEDIATYPE_VIDEO_H264QBOX			mmioFOURCC('H','Q','B','X')
#define MEDIATYPE_VIDEO_MP4S				mmioFOURCC('M','P','4','S')

//AUDIO
#define MEDIATYPE_AUDIO_PCM					WAVE_FORMAT_PCM
#define MEDIATYPE_AUDIO_G723				0x8000
#define MEDIATYPE_AUDIO_MPEG1_LAYER1		0x8001
#define MEDIATYPE_AUDIO_MPEG1_LAYER2		0x8002
#define MEDIATYPE_AUDIO_MPEG1_LAYER3		0x8003
#define MEDIATYPE_AUDIO_MPEG1_LAYER2QBOX	0x8004
#define MEDIATYPE_AUDIO_G711_ULAW			0x8005	// HP4K
#define MEDIATYPE_AUDIO_G711_ALAW			0x8006	

//STREAM
#define MEDIATYPE_STREAM_MPEG2PROGRAM		0x01

//TEXT
#define MEDIATYPE_TEXT_ANSI					1
//AUX
#define MEDIATYPE_AUX_SENSOR				0x01
#define MEDIATYPE_AUX_MOTIONDETECTION		0x02
#define MEDIATYPE_AUX_D3DFILTER_NOTIFY		0x03
#define MEDIATYPE_AUX_MOTIONDETECTION_EVENT	0x04

////////////////
struct MEDIATYPE_BASE{
	union {
		DWORD reserved[32];
		struct {
			PrismIIMediaType majorType;
			DWORD minorType;
			char channelName[32];
			DWORD channelID;
		};
	};
};

#define MTV_USE_ROI	0x04

struct MEDIATYPE_VIDEO:public MEDIATYPE_BASE{
	union {
		DWORD reserved1[32];
		struct {
			SIZE	imgSize;
			DWORD	frameRate;
			DWORD	imgFlag;
			RECT	roi;//rect of interest..
		};
	};
};

struct MEDIATYPE_STREAM:public MEDIATYPE_BASE{
	union {
		DWORD reserved1[32];
//		struct {
//		};
	};
};

struct MEDIATYPE_AUDIO:public MEDIATYPE_BASE{
	union {
		DWORD reserved1[32];
		struct {
			WAVEFORMATEX wfx;
			BYTE reserved2[32];
		};
	};
};

struct MEDIATYPE_TEXT:public MEDIATYPE_BASE{
	union {
		DWORD reserved1[32];
		struct {
			LONG	maxLength;
		};
	};
};

struct MEDIATYPE_AUX:public MEDIATYPE_BASE{
	union {
		DWORD reserved1[32];
		struct {
			LONG length;
		};
	};
};

struct MEDIATYPE_AUX_MDEVENT:public MEDIATYPE_BASE{
	union {
		DWORD reserved1[32];
		struct {
			LONG length;
			DWORD *pMDROIInfo[16];
		};
	};
};

////////////////
struct MEDIATYPE_MULTICHANNEL{
	PrismIIMediaType majorType;
	DWORD minorType;
	char mediaName[32];
	DWORD mediaID;
	LONG chCount;
	union{
		MEDIATYPE_BASE   * chMediaTypes;
		MEDIATYPE_VIDEO  * chVideoTypes;
		MEDIATYPE_AUDIO  * chAudioTypes;
		MEDIATYPE_STREAM * chStreamTypes;
		MEDIATYPE_TEXT   * chTextTypes;
		MEDIATYPE_AUX    * chAuxTypes;
	};
};
//methodes....
MEDIATYPE_BASE * GetChannelMediaType(MEDIATYPE_MULTICHANNEL* pmt,ULONG chIdx);

template<class T> class MTypeMC:public MEDIATYPE_MULTICHANNEL{
public:
	T& operator[](int index);
};

template<class T> inline T& MTypeMC<T>::operator[](int index)
{
	//assert(level>0);
	return ((T*) chMediaTypes)[index];
}


////////////////
struct MEDIADATA_BASE{
	union {
		DWORD reserved[32];
		struct {
		//	PrismIIMediaType mediaType;
			LARGE_INTEGER time;	//100 nanosec..
			BYTE *  pData;
			LONG	len;
		};
	};
};

struct MEDIADATA_VIDEO:public MEDIADATA_BASE{
	union {
		DWORD reserved1[32];
		struct {
		//	SIZE	imgSize;
		//	DWORD	fourCC;
			BOOL	bKeyFrame;
			DWORD	vState;//
			DWORD	dwFrameType;
			LARGE_INTEGER PTS; // Presentation Time Stamp
			DWORD	dwMode;       // Additional Information
		};
	};
};

struct MEDIADATA_AUDIO:public MEDIADATA_BASE{
	union {
		DWORD reserved1[32];
		struct {
			LONG nSamples;	
		};
	};
};

struct MEDIADATA_AUX:public MEDIADATA_BASE{
	union {
		DWORD reserved1[32];
		struct {
			DWORD dwWidth;
			DWORD dwHeight;
			DWORD dwMotionCounter;
			DWORD dwBitmapSize;
		};
	};
};

struct MEDIADATA_AUX_MDEVENT:public MEDIADATA_BASE{
	union {
		DWORD reserved1[32];
		struct {
			DWORD index;
			DWORD transit;
		};
	};
};

struct MEDIADATA_TEXT:public MEDIADATA_BASE{
	union {
		DWORD reserved1[32];
//		struct {
		//	DWORD textType;
//		};
	};
};

struct MEDIADATA_STREAM:public MEDIADATA_BASE{
	union {
		DWORD reserved1[32];
//		struct {
//		};
	};
};

int GetMediaTypeSize(MEDIATYPE_MULTICHANNEL& mc);
BOOL CopyMediaTypeMultiChannel(MEDIATYPE_MULTICHANNEL& dst,MEDIATYPE_MULTICHANNEL& src);
BOOL UpdateMediaTypeMultiChannel(MEDIATYPE_MULTICHANNEL& dst,MEDIATYPE_MULTICHANNEL& src);
BOOL InitMultiChannel(MEDIATYPE_MULTICHANNEL & mc,PrismIIMediaType majorType,DWORD minorType,char* mediaName,DWORD mediaID,LONG chCount);

BOOL GetMajorTypeName(PrismIIMediaType majorType,char* strTypeName);
BOOL GetMinorTypeName(PrismIIMediaType majorType,DWORD minorType,char* strTypeName);
BOOL IsUncompressedVideo(DWORD vidFormat);


typedef BOOL (WINAPI *ReleaseMediaFunc)(MEDIADATA_BASE*,BOOL bRelease);

interface IInstanceManager;

MIDL_INTERFACE("5BB4DD5C-8520-4323-9063-8E3840CDF84A")
IPrismIIFilter: public IProcessCtrl
{
public:
	virtual HRESULT STDMETHODCALLTYPE Load(IInstanceManager* pFrameApp,ISettingStorage* pStorage,IMessageManager* pMsgMng,LONG mediaCount, MEDIATYPE_MULTICHANNEL* mediaTypes[]) = 0;
	virtual BOOL STDMETHODCALLTYPE SetMediaTypes(LONG mediaCount, MEDIATYPE_MULTICHANNEL* mediaTypes[],BOOL bCurrent)= 0;
	virtual BOOL STDMETHODCALLTYPE SetMediaTypeDetail(LONG mediaIndex, LONG chIndex,MEDIATYPE_BASE* mediaType,BOOL bCurrent)= 0;
	virtual HRESULT STDMETHODCALLTYPE ProcessMediaData(LONG mediaIndex, LONG chIndex,MEDIADATA_BASE* mediaData,ReleaseMediaFunc relFunc)= 0;

//CONTEXT..
//	virtual HRESULT STDMETHODCALLTYPE InitializeContext()=0;
//	virtual HRESULT STDMETHODCALLTYPE UninitizlizeContext()=0;

	//static�� filter�� Ư��..
/*
    virtual HRESULT STDMETHODCALLTYPE ProcessSetup() = 0;
    virtual HRESULT STDMETHODCALLTYPE ProcessEndup() = 0;
    virtual HRESULT STDMETHODCALLTYPE ProcessRun() = 0;	//apply changed setting values.
    virtual HRESULT STDMETHODCALLTYPE ProcessStop() = 0;
*/
};

////////////////////////////////////////////////////////////////////////
//card manager//////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
typedef struct {
	DWORD hwId;
	DWORD hwIdCapAudio;
	BYTE activationCode[16];
}DVRAUTHINFO;


typedef struct{
	DWORD dwApiVer;
	DWORD dwApiMask;
	VOID* pCapApi;
	VOID* pAudApi;
	VOID* pOvrApi;
	VOID* pCodApi;
	VOID* pNetApi;
}DVRAPIINFO;

#define MAKE_VERSION(major,minor) ( major|(minor << 16) )
#define GET_MAJOR_VERSION(ver)		((ver)&0xffff)
#define GET_MINOR_VERSION(ver)		((ver)>>16)

#if 0
typedef struct{
	DWORD dwApiVer;
	DWORD dwApiMask;
	ICap5* pCapApi;
	IAud5* pAudApi;
	IOvr5* pOvrApi;
	ICod5* pCodApi;
	INet5* pNetApi;
}DVRAPI5INFO;
#endif

MIDL_INTERFACE("230C716D-5AF1-44a6-9B12-B6CF8D615C7A")
IPrismIICardManager : public IProcessCtrl
{
public:
	virtual HRESULT STDMETHODCALLTYPE Load(IInstanceManager* pFrameApp,char *Dlls[],DVRAUTHINFO authInfo[],int authCount,ISettingStorage* pStorage,IMessageManager* pMsgMng) = 0;
	virtual BOOL STDMETHODCALLTYPE GetMediaTypes(LONG* mediaCount, MEDIATYPE_MULTICHANNEL** mediaTypes) = 0;
////api query functions...
	virtual HRESULT STDMETHODCALLTYPE GetApiCount(UINT* pCount)=0;
	virtual HRESULT STDMETHODCALLTYPE GetApiInfo(UINT apiIndex,DVRAPIINFO* pApiInfo)=0;
	//virtual BOOL STDMETHODCALLTYPE IsMediaInfoChanged(DWORD info) = 0;
};



////////////////////////////////////////////////////////////////////////
//frame app/////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

struct WINDOWPLACEINFO{
	HWND hParent;
	RECT rc;
	BOOL bShow;

};

//used int IInstanceManager::NotifyWindowInfo
enum WindowNotify{
	NOTIFY_WND_CREATED=1,
	NOTIFY_WND_DESTROYED=2,
	NOTIFY_WND_SIZED=3,
	NOTIFY_WND_ACTIVATED=4,

};

MIDL_INTERFACE("43E021BF-EC04-4cfd-AAE1-175E6049FB4C")
IInstanceManager : public IUnknown
{
public:
    virtual HRESULT STDMETHODCALLTYPE GetMainWindowHandle() = 0;
//cardmanager calls...
	virtual BOOL STDMETHODCALLTYPE SetMediaTypes(LONG mediaCount, MEDIATYPE_MULTICHANNEL* mediaTypes[],BOOL bCurrent)= 0;
	virtual BOOL STDMETHODCALLTYPE SetMediaTypeDetail(LONG mediaIndex, LONG chIndex,MEDIATYPE_BASE* mediaType,BOOL bCurrent)= 0;
	virtual HRESULT STDMETHODCALLTYPE ProcessMediaData(ULONG mediaIndex,ULONG linCh,MEDIADATA_BASE * MediaData,ReleaseMediaFunc relFunc) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetDriverDLL() = 0;
    virtual HRESULT STDMETHODCALLTYPE GetActivationCode() = 0;
//Initialize UI ... methods..
	virtual BOOL STDMETHODCALLTYPE AddTreeViewItem(IProcessCtrl * pProcess,DWORD IconID,char* text,DWORD v1,DWORD v2,DWORD v3) = 0;
	virtual BOOL STDMETHODCALLTYPE AddTreeViewItemFormatted(IProcessCtrl * pProcess,DWORD IconID,DWORD v1,DWORD v2,DWORD v3,char *format,...) = 0;
	virtual BOOL STDMETHODCALLTYPE SelectTreeViewItem(IProcessCtrl * pProcess,BOOL bSelect,DWORD v1,DWORD v2,DWORD v3)=0;
	virtual BOOL STDMETHODCALLTYPE SetTreeViewAddPosition(IProcessCtrl * pProcess,DWORD v1,DWORD v2,DWORD v3)=0;
	virtual BOOL STDMETHODCALLTYPE DeleteTreeViewItem(IProcessCtrl * pProcess,DWORD v1,DWORD v2,DWORD v3)=0;
	virtual BOOL STDMETHODCALLTYPE EmptyTreeView(IProcessCtrl * pProcess)=0;
	virtual HRESULT STDMETHODCALLTYPE AdjustTreeViewLevel(BOOL bIncrease) = 0;

//Window Management methods..
	virtual HRESULT STDMETHODCALLTYPE NotifyWindowInfo(IPrismIIWindowManage* pWndMng,UINT idx,WindowNotify uMsg,ULONG param) = 0;
	virtual HRESULT STDMETHODCALLTYPE AddToolbarButton(IProcessCtrl * pProcess,UINT uCmd,UINT iIcon,LPCSTR pText,BOOL bCheck,BOOL bOn) = 0;
//CONTEXT..
	virtual HRESULT STDMETHODCALLTYPE InitializeContext()=0;
	virtual HRESULT STDMETHODCALLTYPE UninitizlizeContext()=0;

	virtual HRESULT STDMETHODCALLTYPE SetTimer(IProcessCtrl* pProc,UINT id,UINT uElapse)=0;
	virtual HRESULT STDMETHODCALLTYPE KillTimer(IProcessCtrl* pProc,UINT id)=0;

//plugin support..
	virtual HRESULT STDMETHODCALLTYPE ProcessMediaCallback(DWORD callbackParamType,DWORD apiIndex,DWORD param1,DWORD param2,DWORD param3)=0;

};


////////////////////////////////////////////////////////////////////////
//plug - in/////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

//Media Callback Parameter Type

//native api callbacks ,,, datas....
#define CPT_CAP_CH_ADDR				1
#define CPT_CAP_CAP_IMAGE_INFO		2
#define CPT_CMN5_DATA_INFO_DATA		3
#define CPT_CAP_FRAMERATE			4

///#define 
typedef struct{
	BOOL bCallback;
	DWORD reserved[63];	
}PRISMIIPLUGININFO;

MIDL_INTERFACE("30286FDF-D4E6-409e-8E7B-4AFF71B69B62")
IPrismIIPlugIn: public IProcessCtrl
{
public:
	virtual HRESULT STDMETHODCALLTYPE Load(IInstanceManager* pFrameApp,IPrismIICardManager* pCardManager,ISettingStorage* pStorage,IMessageManager* pMsgMng) = 0;
	virtual HRESULT STDMETHODCALLTYPE GetPlugInInfo(PRISMIIPLUGININFO* pPlugInInfo)=0;
	virtual HRESULT STDMETHODCALLTYPE MediaCallback(DWORD callbackParamType,DWORD apiIndex,DWORD param1,DWORD param2,DWORD param3)=0;
};
////////////////////////////////////////////////////////////////////////
//Event Manager/////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

MIDL_INTERFACE("FF4A374D-1F62-4dac-ACB0-228FA5950E50")
IPrismIIEventManager: public IProcessCtrl
{
public:
	virtual HRESULT STDMETHODCALLTYPE Load(IInstanceManager* pFrameApp,ISettingStorage* pStorage,IMessageManager* pMsgMng) = 0;
};

DEFINE_GUID (IID_IInstanceManager,		0x43E021BF,0xEC04,0x4cfd,0xAA,0xE1,0x17,0x5E,0x60,0x49,0xFB,0x4C);
DEFINE_GUID (IID_IPrismIICardManager,	0x230C716D,0x5AF1,0x44a6,0x9B,0x12,0xB6,0xCF,0x8D,0x61,0x5C,0x7A);
DEFINE_GUID (IID_IProcessCtrl,			0x02AD1585,0xB94B,0x4ce9,0xA6,0x8E,0x01,0xBE,0x7C,0x2D,0x77,0x02);
DEFINE_GUID (IID_IPrismIIFilter,		0x5BB4DD5C,0x8520,0x4323,0x90,0x63,0x8E,0x38,0x40,0xCD,0xF8,0x4A);
DEFINE_GUID (IID_IPrismIIWindowManage,	0x5B1717D9,0x40F3,0x4d0e,0x99,0x97,0x93,0xAD,0xB3,0x99,0x94,0xB4);
DEFINE_GUID (IID_ISettingStorage,		0x5B1727D9,0x40F3,0x4d0e,0x99,0x97,0x93,0xAD,0xB3,0x99,0x94,0xB4);
DEFINE_GUID (IID_IMessageManager,		0x529530b5,0xe105,0x43e2,0xb1,0x7c,0x77,0xc1,0x80,0x25,0x5b,0x66);
DEFINE_GUID (IID_IPrismIIPlugIn,		0x36e0b78b,0xbde6,0x4cba,0xb6,0x11,0xa1,0x6d,0x94,0x81,0xf8,0x8c);
DEFINE_GUID (IID_ILogManager,			0xb237d4de,0x7e77,0x4c25,0x94,0x3d,0xac,0xf4,0xc8,0x4f,0xd2,0xe5);
DEFINE_GUID(IID_IProcessCtrlV2,			0xfae6e67e, 0xb140, 0x45a8, 0x86, 0xbf, 0xbf, 0xb7, 0x7d, 0xbb, 0xe, 0x75);	// jbyoon add AfterRun

#endif// _PRISMII_COMMONINTERFACE_H_


