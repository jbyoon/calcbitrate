#if !defined(AFX_CMN5ERRORUSEREX_H__DFDEBAD7_E351_421B_A5BC_1083ABFA2E5F__INCLUDED_)
#define AFX_CMN5ERRORUSEREX_H__DFDEBAD7_E351_421B_A5BC_1083ABFA2E5F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Cmn5ErrorEx.h"

class CCmn5ErrLock  
{
public:
	virtual void Acquire() = 0;
	virtual void Release() = 0;

			CCmn5ErrLock() {}
	virtual ~CCmn5ErrLock() {}
};

template <class LOCK>
class CCmn5ErrGuardLock
{
public:
	CCmn5ErrGuardLock(LOCK& lock) : m_lock(&lock), m_owner (FALSE)
	{ 
		m_lock->Acquire(); 
		m_owner = TRUE; 
	}
	virtual ~CCmn5ErrGuardLock() 
	{
		if (m_owner) 
			m_lock->Release();
	}
private:
	LOCK*	m_lock;
	BOOL	m_owner;
};

class CCmn5ErrNullLock
{
public:
	virtual void Acquire() {}
	virtual void Release() {}

			CCmn5ErrNullLock() {}
	virtual ~CCmn5ErrNullLock() {}
};

class CCmn5ErrCriticalSection  
{
public:
	virtual void Acquire()				{EnterCriticalSection(&m_cs);}
	virtual void Release()				{LeaveCriticalSection(&m_cs);}
			CCmn5ErrCriticalSection()	{InitializeCriticalSection(&m_cs);}
	virtual ~CCmn5ErrCriticalSection()	{DeleteCriticalSection(&m_cs);}
private:
	CRITICAL_SECTION	m_cs;
};

class CCmn5ErrorUser : public CCmn5Error  
{
typedef CCmn5Error SuperClass;   // work around VC++ call to super class method.
public:
#if 1 // metalbrain
	virtual BOOL PushErrorCodes(ULONG code, ULONG minorCode, char* auxMsg, char* session);
	virtual BOOL PushErrorCodesV(ULONG code, LPCSTR szFmt , ... );
	virtual BOOL PushErrorCodesV2(LPCSTR szFmt , ... );
#endif
	
	virtual BOOL	PushErrorCode(ULONG code, ULONG minorCode, char* auxMsg, char* session);
	virtual BOOL	PushErrorCodeItem(CMN5_ERROR_CODE_ITEM* pItem);
	virtual BOOL	PopErrorCodeItem(CMN5_ERROR_CODE_ITEM* pItem);
	virtual void	EmptyErrorCode();

	CCmn5ErrorUser(CMN5_ERR_LEVEL level);
	virtual ~CCmn5ErrorUser();

private:
	CCmn5ErrCriticalSection	m_lock;
};

#endif // !defined(AFX_CMN5ERRORUSEREX_H__DFDEBAD7_E351_421B_A5BC_1083ABFA2E5F__INCLUDED_)
