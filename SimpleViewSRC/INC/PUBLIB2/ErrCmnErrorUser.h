// ErrCmnErrorUser.h: interface for the CErrCmnErrorUser class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ERRCMNERRORUSER_H__DFDEBAD7_E351_421B_A5BC_1083ABFA2E5F__INCLUDED_)
#define AFX_ERRCMNERRORUSER_H__DFDEBAD7_E351_421B_A5BC_1083ABFA2E5F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ErrCmnError.h"

class CErrCmnLock  
{
public:
	virtual void Acquire() = 0;
	virtual void Release() = 0;

			CErrCmnLock() {}
	virtual ~CErrCmnLock() {}

};

template <class LOCK>
class CErrCmnGuardLock
{
public:
	CErrCmnGuardLock(LOCK& lock) : m_lock(&lock), m_owner (FALSE)
	{ 
		m_lock->Acquire(); 
		m_owner = TRUE; 
	}
	virtual ~CErrCmnGuardLock() 
	{
		if (m_owner) 
			m_lock->Release();
	}
private:
	LOCK*	m_lock;
	BOOL	m_owner;
};

class CErrCmnNullLock
{
public:
	virtual void Acquire() {}
	virtual void Release() {}

			CErrCmnNullLock() {}
	virtual ~CErrCmnNullLock() {}
};

class CErrCmnCriticalSection  
{
public:
	virtual void Acquire()				{EnterCriticalSection(&m_cs);}
	virtual void Release()				{LeaveCriticalSection(&m_cs);}
			CErrCmnCriticalSection()	{InitializeCriticalSection(&m_cs);}
	virtual ~CErrCmnCriticalSection()	{DeleteCriticalSection(&m_cs);}
private:
	CRITICAL_SECTION	m_cs;
};

class CErrCmnErrorUser : public CErrCmnError  
{
typedef CErrCmnError SuperClass;   // work around VC++ call to super class method.
public:
	virtual BOOL	PushErrorCode(ULONG code, ULONG minorCode, char* auxMsg, char* session);
	virtual BOOL	PushErrorCodeItem(ErrorCodeItem* pItem);
	virtual BOOL	PopErrorCodeItem(ErrorCodeItem* pItem);
	virtual void	EmptyErrorCode();


			CErrCmnErrorUser(ERR_LEVEL level);
	virtual ~CErrCmnErrorUser();

private:
	CErrCmnCriticalSection	m_lock;
};

extern CErrCmnError* g_pCapError;

#endif // !defined(AFX_ERRCMNERRORUSER_H__DFDEBAD7_E351_421B_A5BC_1083ABFA2E5F__INCLUDED_)
