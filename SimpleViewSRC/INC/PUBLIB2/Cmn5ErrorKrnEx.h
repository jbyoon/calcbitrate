// Cmn5ErrorKrnEx.h: interface for the CCmn5ErrorKrn class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ERRCMNERRORKRN_H__A09DEB95_3D72_4500_9548_28FC98C773FE__INCLUDED_)
#define AFX_ERRCMNERRORKRN_H__A09DEB95_3D72_4500_9548_28FC98C773FE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Cmn5ErrorEx.h"

class CCmn5Error;

typedef struct {
	ULONG ErrorCode;
	ULONG MinorCode;
	ULONG Reserved;
	char* AuxMsg;
	char* SessionName;
} ERROR_CODE_PARMS;

class CCmn5ErrorKrn : public CCmn5Error  
{
typedef CCmn5Error SuperClass;   // work around VC++ call to super class method.
public:
#if 1 // metalbrain
	virtual BOOL PushErrorCodes(ULONG code, ULONG minorCode, char* auxMsg, char* session);
	virtual BOOL PushErrorCodesV(ULONG code, LPCSTR szFmt , ... );
	virtual BOOL PushErrorCodesV2(LPCSTR szFmt , ... );
#endif
	virtual BOOL	PushErrorCode(ULONG ErrorCode, ULONG MinorCode=0, char* AuxMsg="", char* SessionName=NULL);
	virtual BOOL	PushErrorCodeItem(CMN5_ERROR_CODE_ITEM* pItem);
	virtual BOOL	PopErrorCodeItem(CMN5_ERROR_CODE_ITEM* pItem);
	virtual void	EmptyErrorCode();

	void	SetInterruptObject(PKINTERRUPT* i) {m_InterruptObject = i;}

	CCmn5ErrorKrn(CMN5_ERR_LEVEL level);
	virtual ~CCmn5ErrorKrn();

private:
	BOOL	SynchPushErrorCode(ERROR_CODE_PARMS* pParms);
	BOOL	SynchPushErrorCodeItem(CMN5_ERROR_CODE_ITEM* pItem);
	BOOL	SynchPopErrorCodeItem(CMN5_ERROR_CODE_ITEM* pItem);
	void	SynchEmptyErrorCode();

	CMN5_ERROR_CODE_ITEM			m_PushItem;
	CMN5_ERROR_CODE_ITEM			m_PopItemForDispatchLevel;
	CMN5_ERROR_CODE_ITEM			m_PopItemForDIRQL;

	PKINTERRUPT*				m_InterruptObject;

	friend BOOLEAN SYNCH_PushErrorCode(PVOID pContext);
	friend BOOLEAN SYNCH_PushErrorCodeItem(PVOID pContext);
	friend BOOLEAN SYNCH_PopErrorCodeItem(PVOID pContext);
	friend BOOLEAN SYNCH_EmptyErrorCode(PVOID);
};

#endif // !defined(AFX_ERRCMNERRORKRN_H__A09DEB95_3D72_4500_9548_28FC98C773FE__INCLUDED_)
