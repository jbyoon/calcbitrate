// CapQueue.h: interface for the CapQueue class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(CCapQueue__INCLUDED_)
#define CCapQueue__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// This queue does not use locking strategy. If you need it, implement outside of queue.

template<class T,int MaxQueueSize>
class CCapQueue
{
public:
	ULONG GetCount()
	{
		return m_numItems;
	}

	BOOL Push(T* item)
	{
		if(IsFull()) 
			return FALSE;
		m_queue[m_head] = *item;
		m_head = (m_head + 1) % MaxQueueSize;
		m_numItems++;
		return TRUE;
	}

	BOOL Pop(T* item)
	{
		if (IsEmpty())
			return FALSE;
		*item = m_queue[m_tail];
		m_tail = (m_tail + 1) % MaxQueueSize;
		m_numItems--;
		return TRUE;
	}

	BOOL PopPtr(T** item)
	{
		if (IsEmpty())
			return FALSE;
		*item = &m_queue[m_tail];
		m_tail = (m_tail + 1) % MaxQueueSize;
		m_numItems--;
		return TRUE;
	}

	BOOL Peek(T* item)
	{
		if (IsEmpty())
			return FALSE;
		*item = m_queue[m_tail];
		return TRUE;
	}

	BOOL PeekSpecific(T* item, ULONG Num)
	{
		if (IsEmpty())
			return FALSE;
		if(Num > m_tail) return FALSE;

		*item = m_queue[Num];
		return TRUE;
	}

	void RemoveAll()
	{
		m_head = m_tail = m_numItems = 0;
	}

	void SaveIndex()
	{
		m_headTemp = m_head;
		m_tailTemp = m_tail;
		m_numItemsTemp = m_numItems;
	}

	void RestoreIndex()
	{
		m_head = m_headTemp;
		m_tail = m_tailTemp;
		m_numItems = m_numItemsTemp;
	}

	BOOL HasIt()	{return (0 != m_numItems);}
	BOOL IsEmpty()	{return (0 == m_numItems);}
	BOOL IsFull()	{return (MaxQueueSize == m_numItems);}

	void ShowInternal()
	{
		XdPrint(DEBUG_QUEUE,(" - Queue Info m_numItems = [%d]\n",m_numItems));
	}

	CCapQueue() { RemoveAll();	};
	virtual ~CCapQueue(){};

	 T& operator[](int index){
		return 	m_queue[(m_tail+index)% MaxQueueSize];
	}

private:
	ULONG	m_head;
	ULONG	m_tail;
	ULONG	m_numItems;
	T		m_queue[MaxQueueSize];
	ULONG	m_headTemp;
	ULONG	m_tailTemp;
	ULONG	m_numItemsTemp;
};

#endif
