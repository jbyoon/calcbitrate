// Cmn5ErrorEx.cpp: implementation of the CCmn5Error class.
//
//////////////////////////////////////////////////////////////////////

#include "..\stdafx.h"

#include "Cmn5ErrorEx.h"

void CCmn5Error::MakeErrorCodeItem(CMN5_ERROR_CODE_ITEM* pItem, ULONG code, ULONG minorCode, char* auxMsg, char* session)
{
	RtlZeroMemory(pItem, sizeof(CMN5_ERROR_CODE_ITEM));
	pItem->ErrorCode = ADD_MINOR_ERRCODE(code, minorCode);
	pItem->ErrorCode = ADD_LEVEL_TO_ERRCODE(pItem->ErrorCode, m_myLevel);
#if _MSC_VER >= 1300
	LARGE_INTEGER TimeStamp;
	GetTimeStamp(&TimeStamp);
	pItem->TimeStamp.QuadPart = TimeStamp.QuadPart;
#else
	GetTimeStamp(&pItem->TimeStamp);
#endif
	CopyMsgString(pItem->AuxMsg, auxMsg, CMN5_MAX_ERROR_CODE_MSG_LENGTH);
	pItem->AuxMsg[CMN5_MAX_ERROR_CODE_MSG_LENGTH-1]=NULL;
	if(session) {
		CopyMsgString(pItem->SessionName, session, CMN5_MAX_SESSION_LENGTH);
		pItem->SessionName[CMN5_MAX_SESSION_LENGTH-1]=NULL;
	} else {
		CopyMsgString(pItem->SessionName, GetCurSession(), CMN5_MAX_SESSION_LENGTH);
		pItem->SessionName[CMN5_MAX_SESSION_LENGTH-1]=NULL;
	}
}

BOOL CCmn5Error::PushErrorCode_i(CMN5_ERROR_CODE_ITEM* pItem)
{
	if (m_pLogger) {
		m_pLogger->Log(m_pLogger->ErrorToMsg(pItem));
	}

	return m_Queue.Push(pItem);
}

BOOL CCmn5Error::PopErrorCode_i(CMN5_ERROR_CODE_ITEM* pItem)
{
	return m_Queue.Pop(pItem);
}

void CCmn5Error::CopyMsgString(char* dest, char* source, unsigned count)
{
	if (NULL == dest || NULL == source)
		return;

	while (count && (*dest++ = *source++)) {
		count--;
	}

	if (count) {
		while (--count) {
			*dest++ = '\0';
		}
	}
}

char* CCmn5Error::GetStrSeverity(ULONG code)
{
	switch(ERRCODE_SEVERITY(code)) {
	case ERR_SEVERITY_OPERATION_SUCCESS:
		return "SUCCESS";
		break;
	case ERR_SEVERITY_WARNING:
		return "WARNING";
		break;
	case ERR_SEVERITY_OPERATION_FAIL:
		return "FAIL";
		break;
	case ERR_SEVERITY_IMPLEMENTATION_ERROR:
		return "IMPLEMENTATION_ERROR";
		break;
	default:
		return "Unspecified";
		break;
	}
}

char* CCmn5Error::GetStrLevel(ULONG code)
{
	switch(ERRCODE_LEVEL(code)) {
	case CMN5_ERR_LEVEL_NONE:
		return "NONE";
		break;
	case CMN5_ERR_LEVEL_KERNEL:
		return "KERNEL";
		break;
	case CMN5_ERR_LEVEL_DLL:
		return "DLL";
		break;
	case CMN5_ERR_LEVEL_APP:
		return "APP";
		break;
	default:
		return "Unspecified";
		break;
	}
}

char* CCmn5Error::GetStrHandling(ULONG code)
{
	switch(ERRCODE_HANDLING(code)) {
	case ERR_HANDLING_NONE:
		return "NONE";
		break;
	default:
		return "Unspecified";
		break;
	}
}

void CCmn5Error::SetCurSession(char* session)
{
	if(session) {
		CopyMsgString(m_curSessionName, session, CMN5_MAX_SESSION_LENGTH);
		m_curSessionName[CMN5_MAX_SESSION_LENGTH-1]=NULL;
#if defined(USER_MODE_CODE)
//		TRACE(session);
//		TRACE(" SESSION\n");
#endif
	} else {
		CopyMsgString(m_curSessionName, "", CMN5_MAX_SESSION_LENGTH);
		m_curSessionName[CMN5_MAX_SESSION_LENGTH-1]=NULL;
	}
}

char* CCmn5Error::GetCurSession()
{
	return m_curSessionName;
}

void CCmn5Error::EmptyErrorCode_i()
{
	m_Queue.RemoveAll();
}

void CCmn5Error::GetTimeStamp(LARGE_INTEGER* t)
{
#if defined(KERNEL_MODE_CODE)
	KeQuerySystemTime(t);
#else
	SYSTEMTIME systemTime;
	GetSystemTime(&systemTime);
	SystemTimeToFileTime(&systemTime, (FILETIME*)t);
#endif
}

CCmn5Error::CCmn5Error(CMN5_ERR_LEVEL level) : m_myLevel(level)
{
	m_pLogger = NULL;
	RtlZeroMemory(m_curSessionName, sizeof(m_curSessionName));
	EmptyErrorCode_i();
}

CCmn5Error::~CCmn5Error()
{

}

// _____________________________________________________________________________________________
// _____________________________________________________________________________________________
// _____________________________________________________________________________________________
// _____________________________________________________________________________________________

char* CCmn5ErrLog::ErrorToMsg(CMN5_ERROR_CODE_ITEM* err)
{
	RtlZeroMemory(m_msg, sizeof(m_msg));
	// Ex.
	// ERR: S/F/W/E level session major-code minor-code aux-msg time-stamp
	//_snprintf(m_msg, MAX_ERRCMN_LOG_MSG_LENGTH, "ERR: %c %c %s 0x%08x 0x%08x %s %s\n", 
	//	GetStrSeverity(err), GetStrLevel(err), GetSession(err), GetMajorCode(err), GetMinorCode(err),
	//	GetAuxMsg(err), GetStrTimeStamp(err));
	_snprintf(m_msg, CMN5_MAX_ERRCMN_LOG_MSG_LENGTH, "ERR: %c %c %s %s %s", 
		GetStrSeverity(err), GetStrLevel(err), GetSession(err), GetAuxMsg(err), GetStrTimeStamp(err));
	m_msg[CMN5_MAX_ERRCMN_LOG_MSG_LENGTH-1] = NULL;

	return m_msg;
}

char CCmn5ErrLog::GetStrSeverity(CMN5_ERROR_CODE_ITEM* err)
{
	switch(ERRCODE_SEVERITY(err->ErrorCode)) {
	case ERR_SEVERITY_OPERATION_SUCCESS:
		return 'S';
		break;
	case ERR_SEVERITY_WARNING:
		return 'W';
		break;
	case ERR_SEVERITY_OPERATION_FAIL:
		return 'F';
		break;
	case ERR_SEVERITY_IMPLEMENTATION_ERROR:
		return 'E';
		break;
	default:
		return 'X';
		break;
	}
}

char CCmn5ErrLog::GetStrLevel(CMN5_ERROR_CODE_ITEM* err)
{
	switch(ERRCODE_LEVEL(err->ErrorCode)) {
	case CMN5_ERR_LEVEL_NONE:
		return 'N';
		break;
	case CMN5_ERR_LEVEL_KERNEL:
		return 'K';
		break;
	case CMN5_ERR_LEVEL_DLL:
		return 'D';
		break;
	case CMN5_ERR_LEVEL_APP:
		return 'A';
		break;
	default:
		return 'X';
		break;
	}
}

char* CCmn5ErrLog::GetSession(CMN5_ERROR_CODE_ITEM* err)
{
	return err->SessionName;
}

ULONG CCmn5ErrLog::GetMajorCode(CMN5_ERROR_CODE_ITEM* err)
{
	return ERRCODE_MAJOR_CODE(err->ErrorCode);
}

ULONG CCmn5ErrLog::GetMinorCode(CMN5_ERROR_CODE_ITEM* err)
{
	return ERRCODE_MINOR_CODE(err->ErrorCode);
}

char* CCmn5ErrLog::GetAuxMsg(CMN5_ERROR_CODE_ITEM* err)
{
	return err->AuxMsg;
}

char* CCmn5ErrLog::GetStrTimeStamp(CMN5_ERROR_CODE_ITEM* err)
{
#if defined(KERNEL_MODE_CODE)
	_snprintf(m_strTimeStamp, 32, "0x%08x%08x", err->TimeStamp.HighPart, err->TimeStamp.LowPart);
#else
	FILETIME localTime;
	SYSTEMTIME systemTime;
	FileTimeToLocalFileTime((FILETIME*)&err->TimeStamp, &localTime);
	FileTimeToSystemTime(&localTime, &systemTime);
	_snprintf(m_strTimeStamp, 32, "%d-%d %d:%d:%d.%d", 
		systemTime.wMonth, systemTime.wDay, systemTime.wHour, systemTime.wMinute, systemTime.wSecond,
		systemTime.wMilliseconds);
#endif

	return m_strTimeStamp;
}

CCmn5ErrLog::CCmn5ErrLog(int logLevel) : m_logLevel(logLevel)
{

}

CCmn5ErrLog::~CCmn5ErrLog()
{

}

// _____________________________________________________________________________________________
// _____________________________________________________________________________________________
// _____________________________________________________________________________________________
// _____________________________________________________________________________________________

ULONG CCmn5ErrLogTrace::Log(char* logMsg)
{
#if defined(KERNEL_MODE_CODE)
	if (m_logLevel > NO_LOGGING) {
		DbgPrint("%s", logMsg);
	}
#else
	TRACE("%s", logMsg);
#endif

	return TRUE;
}

CCmn5ErrLogTrace::CCmn5ErrLogTrace(int logLevel) : CCmn5ErrLog(logLevel)
{

}

CCmn5ErrLogTrace::~CCmn5ErrLogTrace()
{

}
