#include "stdafx.h"
#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
//#include <commctrl.h>
//#include <shlobj.h>
//#include <stdarg.h>

//#include <varargs.h>

#include "winutil.h"
#pragma comment(lib,"version.lib")
#pragma comment(lib,"shell32.lib")

static HINSTANCE g_hInst;
BOOL WUSetAppInstanceHandle(HINSTANCE hInst)
{
	g_hInst=hInst;
	return TRUE;
}

void MoveWindowToCenterRect(HWND hwnd,RECT& rc,POINT& offset,UINT flags,HWND hWndInsertAfter)
{
	RECT rect;
	GetWindowRect(hwnd,&rect);
	int x=offset.x+(rc.left+rc.right-rect.right+rect.left)/2;
	int y=offset.y+(rc.top+rc.bottom-rect.bottom+rect.top)/2;
	SetWindowPos(hwnd,hWndInsertAfter,x,y,0,0,SWP_NOSIZE|flags);
}

BOOL AnchorWindowToDesktop(HWND hwnd,DWORD pos)
{
	//GetSystemMetrics(SM_CXFULLSCREEN);
	//GetSystemMetrics(SM_CXFULLSCREEN);
	RECT rc_screen;
	SystemParametersInfo(SPI_GETWORKAREA,0,&rc_screen,0);
	RECT rc_window;
	GetWindowRect(hwnd,&rc_window);
	int left_x=rc_screen.left;
	int hcenter_x=(rc_screen.left+rc_screen.right)/2 - (rc_window.right-rc_window.left)/2;
	int right_x=rc_screen.right-(rc_window.right-rc_window.left);
	int bottom_y=(rc_screen.bottom-(rc_window.bottom-rc_window.top));
	int vcenter_y=(rc_screen.top+rc_screen.bottom)/2 - (rc_window.bottom-rc_window.top)/2;;
	int top_y=rc_screen.top;
	int x=0,y=0;

	if(pos&AWTD_LEFT)			x = left_x;
	else if(pos&AWTD_RIGHT)		x = right_x;
	else if(pos&AWTD_HCENTER)	x = hcenter_x;

	if(pos&AWTD_TOP)			y = top_y;
	else if(pos&AWTD_BOTTOM)	y = bottom_y;
	else if(pos&AWTD_VCENTER)	y = vcenter_y;

	return SetWindowPos(hwnd,NULL,x,y,0,0,SWP_NOZORDER|SWP_NOSIZE);
}





BOOL GetDlgItemProperties(HWND hDlg,UINT id,HFONT&hfont,DWORD&style,DWORD&exstyle,RECT &rc,HWND* phwnd,char* classname)
{
	HWND hwnd=GetDlgItem(hDlg,id);
	hfont=(HFONT)SendMessage(hwnd,WM_GETFONT,0,0);
	GetWindowRect(hwnd,&rc);
	MapWindowPoints(NULL,hDlg,(LPPOINT)&rc,2);
	ShowWindow(hwnd,SW_SHOW);
	style=GetWindowLong(hwnd,GWL_STYLE);
	exstyle=GetWindowLong(hwnd,GWL_EXSTYLE);
	
	if(phwnd)*phwnd=hwnd;
	if(classname){
		GetClassName(hwnd,classname,80);
	}
	return TRUE;
}

HWND MyCreateWindow(LPCSTR clsname,LPCSTR title,DWORD style, DWORD exstyle,RECT&rc,HWND parent,UINT id,HFONT* hfont)
{
	if (id!=-1) {
		HWND hOldWnd = GetDlgItem(parent, id);
		if (hOldWnd) {
			DestroyWindow(hOldWnd);
		}
	}
	HWND hwnd=CreateWindowEx(exstyle,clsname,title,
					style,rc.left,rc.top,rc.right-rc.left,rc.bottom-rc.top,
					parent,(HMENU)(id),g_hInst,NULL);
	if(hfont)SendMessage(hwnd,WM_SETFONT,(WPARAM)*hfont,0);
	return hwnd;
}

BOOL DuplicateUnnamedControl(HWND hdlg,UINT id,int count,DWORD flags,int cxy)
{
	HWND hwnd;
	HFONT hfont;
	DWORD style,exstyle;
	RECT rc;
	char classname[80];
	if(count<0) return FALSE;
	if(GetDlgItemProperties(hdlg,id,hfont,style,exstyle,rc,&hwnd,classname)){
		ShowWindow(hwnd,SW_SHOW);
		style|=WS_VISIBLE;
		for(int i=0;i<count;i++){

			int x=0,y=0;
			if(flags&ARRANGEFLAG_HORZ)x=(flags&ARRANGEFLAG_RELATIVE)?rc.right-rc.left+cxy : cxy;
			if(flags&ARRANGEFLAG_VERT)y=(flags&ARRANGEFLAG_RELATIVE)?rc.bottom-rc.top+cxy : cxy;

			OffsetRect(&rc,x,y);

			UINT id_new = id+i+1;

			MyCreateWindow(classname, "", style,exstyle, rc, hdlg, id_new, &hfont);
		}
	}
	return TRUE;	
}

BOOL DuplicateNamedControl(HWND hdlg,UINT id,int count,char* title, DWORD flags,int cxy)
{
	HWND hwnd;
	HFONT hfont;
	DWORD style,exstyle;
	RECT rc;
	char classname[80];
	if(count<0) return FALSE;
	if(GetDlgItemProperties(hdlg,id,hfont,style,exstyle,rc,&hwnd,classname)){
		ShowWindow(hwnd,SW_SHOW);
		style|=WS_VISIBLE;
		for(int i=0;i<count;i++){
			
			int x=0,y=0;
			if(flags&ARRANGEFLAG_HORZ)x=(flags&ARRANGEFLAG_RELATIVE)?rc.right-rc.left+cxy : cxy;
			if(flags&ARRANGEFLAG_VERT)y=(flags&ARRANGEFLAG_RELATIVE)?rc.bottom-rc.top+cxy : cxy;
			OffsetRect(&rc,x,y);

			UINT id_new = id+i+1;

			MyCreateWindow(classname, title, style, exstyle, rc, hdlg, id_new, &hfont);
		}
	}
	return TRUE;	
}

BOOL CreateParametricNamedStaticControlAlign(HWND hdlg,UINT id,int count,char *title_format,DWORD align_where)
{
	for(int i=0;i<count;i++){
		HFONT hfont;
		DWORD style,exstyle;
		RECT rc;
		GetDlgItemProperties(hdlg,id+i,hfont,style,exstyle,rc);

		char title[128];
		wsprintf(title,title_format,i);
		RECT rc_new={rc.left,rc.top-(rc.bottom-rc.top),rc.right,rc.top};
		MyCreateWindow("Static", title, WS_VISIBLE|WS_CHILD,exstyle, rc_new, hdlg, (DWORD)-1, &hfont);
	}
	return TRUE;
}

BOOL DuplicateParametricNamedStaticControl(HWND hdlg,UINT id,int count,char* title_format, DWORD flags,int cxy)
{
	HWND hwnd;
	HFONT hfont;
	DWORD style,exstyle;
	RECT rc;
	if(count<0) return FALSE;
	if(GetDlgItemProperties(hdlg,id,hfont,style,exstyle,rc,&hwnd)){
		ShowWindow(hwnd,SW_SHOW);
		style|=WS_VISIBLE;
		for(int i=0;i<count;i++){
			
			int x=0,y=0;
			if(flags&ARRANGEFLAG_HORZ)x=(flags&ARRANGEFLAG_RELATIVE)?rc.right-rc.left+cxy : cxy;
			if(flags&ARRANGEFLAG_VERT)y=(flags&ARRANGEFLAG_RELATIVE)?rc.bottom-rc.top+cxy : cxy;
			OffsetRect(&rc,x,y);

			char title[128];
			wsprintf(title,title_format,i+1);

			MyCreateWindow("Static", title, style, exstyle,rc, hdlg, (DWORD)-1, &hfont);
		}
	}
	return TRUE;	
}

BOOL GetChildWindowRect(HWND hwnd,RECT* pRect)
{
	RECT rc;
	GetWindowRect(hwnd,&rc);
	MapWindowPoints(NULL,GetParent(hwnd),(LPPOINT)&rc,2);
	*pRect=rc;
	return TRUE;
}


BOOL	GetClientRectInScreen(HWND hwnd,RECT* prc)
{
	GetClientRect(hwnd,prc);
	MapWindowPoints(hwnd,NULL,(POINT*)prc,2);
	return TRUE;
}


BOOL GetDlgItemRect(HWND hDlg,UINT id,RECT* prc)
{
	HWND hwnd=GetDlgItem(hDlg,id);
	GetWindowRect(hwnd,prc);
	MapWindowPoints(NULL,hDlg,(LPPOINT)prc,2);
	return TRUE;
}


BOOL ShowDlgItem(HWND hdlg,UINT id,int nCmdShow)
{
	return ShowWindow(GetDlgItem(hdlg,id),nCmdShow);
}

BOOL ShowDlgItems(HWND hdlg,int nCmdShow,int count,...)
{
   va_list marker;

   va_start( marker, count);     /* Initialize variable arguments. */
   for(int i=0;i<count;i++){
	   UINT id= va_arg( marker, UINT);
	   ShowDlgItem(hdlg, id, nCmdShow);
   }
   va_end( marker );              /* Reset variable arguments.      */
   return TRUE;
}

BOOL EnableDlgItem(HWND hdlg,UINT id,BOOL bEnable)
{
	return EnableWindow(GetDlgItem(hdlg,id),bEnable);
}


BOOL EnableDlgItems(HWND hdlg,BOOL bEnable,int count,...)
{
   va_list marker;

   va_start( marker, count);     /* Initialize variable arguments. */
   for(int i=0;i<count;i++){
	   UINT id= va_arg( marker, UINT);
	   EnableDlgItem(hdlg, id, bEnable);
   }
   va_end( marker );              /* Reset variable arguments.      */
   return TRUE;
}
HBITMAP CreateDeviceCompatibleBitmap(int width,int height)
{
	HDC hdc=GetDC(NULL);
	HBITMAP  hbmp=CreateCompatibleBitmap(hdc,width,height);
	ReleaseDC(NULL,hdc);
	return hbmp;
}

#ifdef _INC_COMMCTRL
BOOL GetBitmapHandles(HIMAGELIST himl,HBITMAP bmps[],int count)
{
	HDC hdc=GetDC(NULL);
	HDC hdcmem=CreateCompatibleDC(hdc);
	//HBITMAP hbmp_old;
	IMAGEINFO imageinfo;
	for(int i=0;i<count;i++){
		ImageList_GetImageInfo(himl,i,&imageinfo);
		RECT rc=imageinfo.rcImage;
		HBITMAP hbmp=CreateCompatibleBitmap(hdc,rc.right-rc.left,rc.bottom-rc.top);
		SelectObject(hdcmem,hbmp);
		ImageList_Draw(himl,i,hdcmem,0,0,ILD_NORMAL );
		bmps[i]=hbmp;
	}
	DeleteDC(hdcmem);
	ReleaseDC(NULL,hdc);
	return TRUE;
}




static char strTmpPath[MAX_PATH];

int CALLBACK BrowseCallbackProc(HWND hwnd, UINT uMsg, LPARAM lParam, LPARAM lpData)
{
	TCHAR szDir[MAX_PATH];
	switch(uMsg){
	case BFFM_INITIALIZED:
		if (lpData){
			strcpy_s(szDir, strTmpPath);
			SendMessage(hwnd,BFFM_SETSELECTION,TRUE,(LPARAM)szDir);
		}
		break;
	case BFFM_SELCHANGED: {
	   if (SHGetPathFromIDList((LPITEMIDLIST) lParam ,szDir)){
		  SendMessage(hwnd,BFFM_SETSTATUSTEXT,0,(LPARAM)szDir);
	   }
	   break;
	}
	default:
	   break;
	}
         
	return 0;
}



BOOL GetFolder(char * strSelectedFolder,const char* lpszTitle,const HWND hwndOwner, const char* strRootFolder, const char* strStartFolder)
{
	char pszDisplayName[MAX_PATH];
	LPITEMIDLIST lpID;
	BROWSEINFOA bi;
	
	bi.hwndOwner = hwndOwner; 
/*
	if (strRootFolder == NULL){
		bi.pidlRoot = NULL;
	}else{
	   LPITEMIDLIST  pIdl = NULL;
	   IShellFolder* pDesktopFolder;
	   char          szPath[MAX_PATH];
	   OLECHAR       olePath[MAX_PATH];
	   ULONG         chEaten;
	   ULONG         dwAttributes;

	   strcpy(szPath, (LPCTSTR)strRootFolder);
	   if (SUCCEEDED(SHGetDesktopFolder(&pDesktopFolder)))
	   {
		   MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, szPath, -1, olePath, MAX_PATH);
		   pDesktopFolder->ParseDisplayName(NULL, NULL, olePath, &chEaten, &pIdl, &dwAttributes);
		   pDesktopFolder->Release();
	   }
	   bi.pidlRoot = pIdl;
	}
*/ 
	LPITEMIDLIST pidl;

#if 0 
	SHGetFolderLocation(NULL,CSIDL_DRIVES,NULL,0,&pidl);
#endif 
    bi.pidlRoot = pidl;
	bi.pszDisplayName = pszDisplayName;
	bi.lpszTitle = lpszTitle;
	bi.ulFlags = BIF_RETURNONLYFSDIRS | BIF_STATUSTEXT|BIF_NEWDIALOGSTYLE;
	bi.lpfn = BrowseCallbackProc;
	if (strStartFolder == NULL){
		bi.lParam = FALSE;
	}else{
		lstrcpy(strTmpPath, strStartFolder);
		bi.lParam = TRUE;
	}
	bi.iImage = NULL;
	lpID = SHBrowseForFolder(&bi);
	if (lpID != NULL){
		BOOL b = SHGetPathFromIDList(lpID, pszDisplayName);
		if (b == TRUE){
			lstrcpy(strSelectedFolder,pszDisplayName);
			return TRUE;
		}
	}else{
		strSelectedFolder[0]=0;
	}
	return FALSE;
}
#endif

BOOL GetFileVersionString(HINSTANCE hInst,char *szResult)
{
	DWORD dwVerHnd;
	char szFullPath[MAX_PATH];
	GetModuleFileName(hInst, szFullPath, sizeof(szFullPath));
    DWORD dwVerInfoSize = GetFileVersionInfoSize(szFullPath, &dwVerHnd);
    if (dwVerInfoSize){
        // If we were able to get the information, process it:
        LPVOID  lpvMem;
        char    szGetName[256];
		
        lpvMem = malloc(dwVerInfoSize);
        GetFileVersionInfo(szFullPath, dwVerHnd, dwVerInfoSize, lpvMem);
		
		unsigned int cbTranslate;
		WORD* lpLang;// {language,codepage}...
		
		
		VerQueryValue(lpvMem, TEXT("\\VarFileInfo\\Translation"),(LPVOID*)&lpLang,&cbTranslate);
		
		wsprintf( szGetName, TEXT("\\StringFileInfo\\%04x%04x\\FileVersion"),lpLang[0],lpLang[1]);
		
        BOOL  fRet;
        UINT  cchVer = 0;
        LPSTR lszVer = NULL;
		
        fRet = VerQueryValue(lpvMem, szGetName, (void**)&lszVer, &cchVer);
		
        if (fRet && cchVer && lszVer){
            lstrcpy(szResult, lszVer);
	        free(lpvMem);
			return TRUE;
        }
        free(lpvMem);
    }
    return FALSE;
}

BOOL DeleteFolder(const char * folderName,__int64* deletedSize)
{
	WIN32_FIND_DATA findFileData;
	BOOL bNoError=TRUE;
	char searchFileName[MAX_PATH];
	wsprintf(searchFileName, "%s\\*.*", folderName);

	HANDLE hFindFile = FindFirstFile(searchFileName, &findFileData);
	if (INVALID_HANDLE_VALUE != hFindFile) {
		do {
			if(findFileData.cFileName[0]!='.'){// current folder or parent folder.. ignore it.
				char fileName[MAX_PATH];
				wsprintf(fileName,"%s\\%s",folderName,findFileData.cFileName);
				if(findFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY){
					if(!DeleteFolder(fileName,deletedSize)){
						bNoError=FALSE;
						break;
					}
				}else{
					if(findFileData.dwFileAttributes !=FILE_ATTRIBUTE_NORMAL){
						SetFileAttributes(fileName,FILE_ATTRIBUTE_NORMAL);
					}
					if(!DeleteFile(fileName)){
						//failed to delete a file..
						bNoError=FALSE;
						break;
					}
					if(deletedSize){
						__int64 filesize=findFileData.nFileSizeHigh;
						filesize<<=32;
						filesize+=findFileData.nFileSizeLow;
						*deletedSize+=filesize;
					}
				}
			}
		}while(FindNextFile(hFindFile, &findFileData));
		FindClose(hFindFile);
	}
	if(bNoError){
		return RemoveDirectory(folderName);
	}
	return FALSE;
}
#ifdef _INC_COMMCTRL
BOOL DropDownToolbarMenu(HWND hWnd,LPNMTOOLBAR lpnmTB,HMENU hMenuLoaded)
{
	RECT rc;
	::SendMessage(lpnmTB->hdr.hwndFrom, TB_GETRECT,(WPARAM)lpnmTB->iItem, (LPARAM)&rc);

	::MapWindowPoints(lpnmTB->hdr.hwndFrom,HWND_DESKTOP,(LPPOINT)&rc, 2);                         

	TPMPARAMS tpm;

	tpm.cbSize = sizeof(TPMPARAMS);

	tpm.rcExclude = rc;

	return TrackPopupMenuEx(hMenuLoaded,TPM_LEFTALIGN|TPM_LEFTBUTTON|TPM_VERTICAL,rc.left, rc.bottom, hWnd, &tpm); 
}
#endif
CFindFile::CFindFile(const char * folderName,const char * fileName,DWORD excludeAttr)
{
	m_exclAttr=excludeAttr;
	ZeroMemory(&m_find,sizeof(m_find));

	char wildcard_name[MAX_PATH];
	GetModuleFileName(NULL,wildcard_name,MAX_PATH);
	RemoveFilename(wildcard_name);
	if(folderName && folderName[0]){
		lstrcat(wildcard_name, folderName);
		lstrcat(wildcard_name, "\\");
	}
	lstrcpy(m_pathName,wildcard_name);
	lstrcat(wildcard_name, fileName);
	
	m_stage= 0;//FIRST

	m_handle = FindFirstFile(wildcard_name, &m_find);
}


BOOL CFindFile::FindNext(char* fileName,WIN32_FIND_DATA* pFound)
{
	if(m_handle!=INVALID_HANDLE_VALUE){
		if(m_stage==1){
			if(!FindNextFile(m_handle,&m_find)){
				FindClose(m_handle);
				m_handle=INVALID_HANDLE_VALUE;
				return FALSE;
			}
		}
		m_stage=1;
		do{
			if(!(m_find.dwFileAttributes& m_exclAttr)){
				if(pFound){
					*pFound=m_find;
				}
				if(fileName){
					lstrcpy(fileName,m_pathName);
					lstrcat(fileName,m_find.cFileName);
				}
				return TRUE;
			}
		}while(FindNextFile(m_handle,&m_find));

		FindClose(m_handle);
		m_handle=INVALID_HANDLE_VALUE;
		return FALSE;
	}
	return FALSE;
}

CFindFile::~CFindFile()
{
	if(m_handle != INVALID_HANDLE_VALUE){
		FindClose(m_handle);
	}
}




BOOL AppQueryRegSetBinary(LPCTSTR lpszValue, BYTE *pV,ULONG nSize,LPCSTR appRegPath)
{
	HKEY hKey=NULL;
	DWORD dw;
	DWORD rs;
	rs = RegCreateKeyEx(HKEY_CURRENT_USER, appRegPath, 0, REG_NONE,REG_OPTION_NON_VOLATILE, KEY_WRITE|KEY_READ, NULL,&hKey, &dw);
	if(rs != ERROR_SUCCESS) return FALSE;

	LONG lResult;
	lResult = RegSetValueEx(hKey, lpszValue, NULL, REG_BINARY,(LPBYTE)pV, nSize);
	RegCloseKey(hKey);

	if (lResult != ERROR_SUCCESS) {
		return FALSE;
	}

	return TRUE;
}

BOOL AppQueryRegGetBinary(LPCTSTR lpszValue, BYTE *pV,ULONG nSize,LPCSTR appRegPath)
{
	HKEY hKey=NULL;
	DWORD dw;
	DWORD rs;
	rs = RegCreateKeyEx(HKEY_CURRENT_USER, appRegPath, 0, REG_NONE,REG_OPTION_NON_VOLATILE, KEY_WRITE|KEY_READ, NULL,&hKey, &dw);
	if(rs != ERROR_SUCCESS) return FALSE;

	DWORD dwType;
	LONG lResult;
	DWORD cbData=nSize;
	lResult = RegQueryValueEx(hKey, lpszValue, NULL, &dwType,(LPBYTE)pV, &cbData);
	RegCloseKey(hKey);

	if (lResult != ERROR_SUCCESS || cbData!=nSize || dwType!=REG_BINARY) {
		return FALSE;
	}

	return TRUE;
}


void RemoveFilename(char * fn)
{
	int len=lstrlen(fn);
	while(len>0){
		len--;
		if(fn[len]=='\\'){
			fn[len+1]=0;
			break;
		}
	}
}

void RemoveExtension(char * fn)
{
	int len=lstrlen(fn);
	while(len>0){
		len--;
		if(fn[len]=='.'){
			fn[len+1]=0;
			break;
		}
	}
}

const char* GetFilename(const char * fn)
{
	int len=lstrlen(fn);
	while(len>0){
		len--;
		if(fn[len]=='\\'){
			return &fn[len+1];
		}
	}
	return fn;
}

__int64 GetFrequency()
{
	__int64 time,qpc,compare;
	static __int64 freq=0;
	if(freq==0){

		// Set to the realtime mode.-------------------
		HANDLE hThread=GetCurrentThread();
		HANDLE hProcess=GetCurrentProcess();

		DWORD OldClass=GetPriorityClass(hProcess);
		long OldPriority=GetThreadPriority(hThread);

		Sleep(0);

		SetPriorityClass(hProcess,REALTIME_PRIORITY_CLASS);
		SetThreadPriority(hThread,THREAD_PRIORITY_TIME_CRITICAL);	// Run code at realtime priority

		Sleep(0);
		// Set to the realtime mode.-------------------

		QueryPerformanceFrequency((LARGE_INTEGER*)&qpc);
		QueryPerformanceCounter((LARGE_INTEGER*)&compare);
		compare+=qpc/10;												// Find time .1 seconds ahead
		
		GetTimeStampStart(time);

		do{
			QueryPerformanceCounter((LARGE_INTEGER*)&qpc);			// Spin until time equal
		}while( compare>qpc );

		GetTimeStampEnd(time);
		

		SetThreadPriority(hThread,OldPriority);
		SetPriorityClass(hProcess,OldClass);

		freq=time*10;//set the value..
	}
	return freq;
}

void OutputDebugStringF(char * format,...)
{
	va_list args;
	va_start(args, format);

	int nBuf;
	TCHAR szBuffer[512];

	nBuf = _vsnprintf_s(szBuffer, sizeof(szBuffer), format, args);

	OutputDebugString(szBuffer);	

	va_end(args);
}
#ifdef _INC_COMMCTRL
BOOL	ListView_InsertColumns(HWND hwnd,int count,...)
{
   va_list marker;

   va_start( marker, count);     /* Initialize variable arguments. */
   for(int i=0;i<count;i++){
		char *columnName= va_arg( marker, char*);
		UINT columnWidth= va_arg( marker, UINT);

		LVCOLUMN lvcolumn={LVCF_TEXT|LVCF_WIDTH,0,100,"HEADER",0,0,};

		lvcolumn.pszText=columnName;
		lvcolumn.cx=columnWidth;
		ListView_InsertColumn(hwnd,i,&lvcolumn);
   }
   va_end( marker );              /* Reset variable arguments.      */
   return TRUE;
}

BOOL	ListView_InsertItemWithSubItems(HWND hwnd,UINT index,char* itemText,int subItemCount,...)
{
	LVITEM item;
	
	item.mask=LVIF_TEXT;
	item.iItem=index;
	item.iSubItem=0;			
	item.pszText =itemText;
	ListView_InsertItem(hwnd,&item);
	
	va_list marker;
	
	va_start( marker, subItemCount);     /* Initialize variable arguments. */
	for(int i=0;i<subItemCount;i++){
		char *subItemText = va_arg( marker, char*);
		item.iSubItem=i+1;			
		item.pszText =subItemText;
		ListView_SetItem(hwnd,&item);
	}	
	return TRUE;
}

int ListView_GetSelectedItem(HWND hwnd)
{
#if 1
	return ListView_GetNextItem(hwnd,-1,LVNI_ALL|LVNI_SELECTED);
#else
	int itemCount=ListView_GetItemCount(hwnd);
	for(int i=0;i<itemCount;i++){
		if(ListView_GetItemState(hwnd,i,LVIS_SELECTED))
			return i;
	}
	return -1;
#endif
}

#endif

CMultiChannelFPS::CMultiChannelFPS(ULONG n)
{
	m_nCh=n;
	secCount=0;
	QueryPerformanceFrequency(&freq);
	QueryPerformanceCounter(&startPoint);
	startPointMin.QuadPart = startPoint.QuadPart;
	bpsStartPoint.QuadPart = startPoint.QuadPart;
	bpsStartPointMin.QuadPart = startPoint.QuadPart;
	

	freqMin.QuadPart = freq.QuadPart*60;

	chFPS	  = new int[n];
	chFPM	  = new int[n];
	chFPS_copy= new int[n];
	chFPM_copy= new int[n];
	chKBPS	= new int[n];
	chKBPM	= new int[n];
	chKBPS_copy = new int[n];
	chKBPM_copy = new int[n];

	memset(chFPS,		0, sizeof(int)*n);
	memset(chFPM,		0, sizeof(int)*n);
	memset(chFPS_copy,	0, sizeof(int)*n);
	memset(chFPM_copy,	0, sizeof(int)*n);
	memset(chKBPS,		0, sizeof(int)*n);
	memset(chKBPM,		0, sizeof(int)*n);
	
	InitializeCriticalSection(&m_Crit);
}

CMultiChannelFPS::~CMultiChannelFPS()
{
	delete [] chFPS;
	delete [] chFPM;
	delete [] chFPS_copy;
	delete [] chFPM_copy;
	delete [] chKBPS;
	delete [] chKBPM;
	delete [] chKBPS_copy;
	delete [] chKBPM_copy;

	DeleteCriticalSection(&m_Crit);
}

void CMultiChannelFPS::AddFrame(int channel, int frame)
{
	LARGE_INTEGER curPoint;
	EnterCriticalSection(&m_Crit);
	QueryPerformanceCounter(&curPoint);

	chFPS[channel]+=frame;

	double elapseTime = ((double)curPoint.QuadPart - startPoint.QuadPart);

	if ( elapseTime >= ((double) freq.QuadPart) ) { // 1s

		CopyMemory(chFPS_copy,chFPS,sizeof(int)*m_nCh);

		memset(chFPS,0,sizeof(int)*m_nCh);
		startPoint = curPoint;

		
		double eTimeMin = ((double)curPoint.QuadPart - startPointMin.QuadPart);

		for(ULONG i=0;i<m_nCh;i++) {
			chFPM[i] += chFPS_copy[i];
		}
	
		if ( eTimeMin >= ((double) freq.QuadPart*60) ) { // 1M

			CopyMemory(chFPM_copy,chFPM,sizeof(int)*m_nCh);

			memset(chFPM,0,sizeof(int)*m_nCh);
			startPointMin = curPoint;
		}
	}
	LeaveCriticalSection(&m_Crit);
}

void CMultiChannelFPS::AddLength(int channel, int kbytes)
{
	LARGE_INTEGER curPoint;
	QueryPerformanceCounter(&curPoint);

	EnterCriticalSection(&m_Crit);
	chKBPS[channel]+=kbytes;

	double elapseTime = ((double)curPoint.QuadPart - bpsStartPoint.QuadPart);

	if ( elapseTime >= ((double) freq.QuadPart) ) { // 1s
		CopyMemory(chKBPS_copy,chKBPS,sizeof(int)*m_nCh);

		memset(chKBPS,0,sizeof(int)*m_nCh);
		bpsStartPoint = curPoint;

		double eTimeMin = ((double)curPoint.QuadPart - bpsStartPointMin.QuadPart);

		for(ULONG i=0;i<m_nCh;i++) {
			chKBPM[i] += chKBPS_copy[i];
		}
	
		if ( eTimeMin >= ((double) freq.QuadPart*60) ) { // 1M

			CopyMemory(chKBPM_copy,chKBPM,sizeof(int)*m_nCh);

			memset(chKBPM,0,sizeof(int)*m_nCh);
			bpsStartPointMin = curPoint;
		}
	}
	LeaveCriticalSection(&m_Crit);
}

void CMultiChannelFPS::Reset()
{
	EnterCriticalSection(&m_Crit);
	ZeroMemory(chKBPS_copy, sizeof(int)*m_nCh);
	ZeroMemory(chFPS_copy, sizeof(int)*m_nCh);
	LeaveCriticalSection(&m_Crit);
}

char *LoadFile(const char *name, int *pSize)
{
	char				*ptr = 0;

	HANDLE hFile = CreateFile(name, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
	if(hFile == INVALID_HANDLE_VALUE) {
		return NULL;
	}

	*pSize = ::GetFileSize(hFile, 0);
	
	ptr = (char*)malloc(*pSize);
	
	DWORD d = 0;
	if(!ReadFile(hFile, ptr, *pSize, &d, NULL)) {
		d = GetLastError();
//		TRACE( "error :%d\n", d);
		free(ptr);
		ptr = NULL;
		CloseHandle(hFile);
		return NULL;
	}

	CloseHandle(hFile);
	return ptr;
}
