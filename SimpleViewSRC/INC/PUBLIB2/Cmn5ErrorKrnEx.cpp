// Cmn5ErrorKrnEx.cpp: implementation of the Cmn5ErrorKrn class.
//
//////////////////////////////////////////////////////////////////////

#include "..\stdafx.h"

#include "Cmn5ErrorKrnEx.h"
#include <stdarg.h>

// FIXME: extern?
CCmn5Error* g_pCapError = NULL;

#ifdef __cplusplus
extern "C" {
#endif

BOOL PushErrorCodes(ULONG code, ULONG minorCode, char* auxMsg, char* session)
{
	BOOL rs = FALSE;

	KdPrint((DRIVERNAME " - Error! code[%08X] [%s][%s]\n",code,(NULL==session)?"_":session,(NULL==auxMsg)?"_":auxMsg));

	if (g_pCapError) {
		rs = g_pCapError->PushErrorCode(code,minorCode,auxMsg,session);
	}

	return rs;
}

BOOL PushErrorCodesV(ULONG code, LPCSTR szFmt , ... )
{
    char szOutBuff [ CMN5_MAX_ERROR_CODE_MSG_LENGTH ] ;

	if (szFmt) {
		va_list  args ;
		va_start ( args , szFmt ) ;
		_vsnprintf ( szOutBuff , CMN5_MAX_ERROR_CODE_MSG_LENGTH-1, szFmt , args ) ;
		va_end ( args ) ;
	} else {
		_snprintf ( szOutBuff , CMN5_MAX_ERROR_CODE_MSG_LENGTH-1, "NULL format string") ;
	}
	szOutBuff[CMN5_MAX_ERROR_CODE_MSG_LENGTH-1] = NULL;

	BOOL rs = FALSE;
	if (g_pCapError) {
		rs = g_pCapError->PushErrorCode(code,0,szOutBuff,g_pCapError->GetCurSession());
	}

	return rs;
}

BOOL PushErrorCodesV2(LPCSTR szFmt , ... )
{
    char szOutBuff [ CMN5_MAX_ERROR_CODE_MSG_LENGTH ] ;

    va_list  args ;
    va_start ( args , szFmt ) ;
    _vsnprintf ( szOutBuff , CMN5_MAX_ERROR_CODE_MSG_LENGTH-1, szFmt , args ) ;
    va_end ( args ) ;
	szOutBuff[CMN5_MAX_ERROR_CODE_MSG_LENGTH-1] = NULL;

	BOOL rs = FALSE;
	if (g_pCapError) {
		rs = g_pCapError->PushErrorCode(ERRCMN_F_OPERATION_UNSUCCESSFUL,0,szOutBuff,g_pCapError->GetCurSession());
	}

	return rs;
}

#ifdef __cplusplus
}
#endif

#if 1 // metalbrain
BOOL CCmn5ErrorKrn::PushErrorCodes(DWORD code, DWORD minorCode, char* auxMsg, char* session)
{
	KdPrint((DRIVERNAME " - Error! code[%08X] [%s][%s]\n ", code, session,auxMsg));
	return PushErrorCode(code,minorCode,auxMsg,session);
}

BOOL CCmn5ErrorKrn::PushErrorCodesV(ULONG code, LPCSTR szFmt , ... )
{
    char szOutBuff [ CMN5_MAX_ERROR_CODE_MSG_LENGTH ] ;

    va_list  args ;
    va_start ( args , szFmt ) ;
    _vsnprintf ( szOutBuff , CMN5_MAX_ERROR_CODE_MSG_LENGTH-1, szFmt , args ) ;
    va_end ( args ) ;
	szOutBuff[CMN5_MAX_ERROR_CODE_MSG_LENGTH-1] = NULL;

	return PushErrorCode(code,0,szOutBuff,GetCurSession());
}

BOOL CCmn5ErrorKrn::PushErrorCodesV2(LPCSTR szFmt , ... )
{
    char szOutBuff [ CMN5_MAX_ERROR_CODE_MSG_LENGTH ] ;

    va_list  args ;
    va_start ( args , szFmt ) ;
    _vsnprintf ( szOutBuff , CMN5_MAX_ERROR_CODE_MSG_LENGTH-1, szFmt , args ) ;
    va_end ( args ) ;
	szOutBuff[CMN5_MAX_ERROR_CODE_MSG_LENGTH-1] = NULL;

	return PushErrorCode(ERRCMN_F_OPERATION_UNSUCCESSFUL,0,szOutBuff,GetCurSession());
}
#endif

BOOL CCmn5ErrorKrn::SynchPushErrorCode(ERROR_CODE_PARMS* pParms)
{
	MakeErrorCodeItem(&m_PushItem, pParms->ErrorCode, pParms->MinorCode, pParms->AuxMsg, pParms->SessionName);

	KdPrint((DRIVERNAME " - Error! code[%08X] [%s][%s]\n",m_PushItem.ErrorCode,
		(NULL==m_PushItem.SessionName)?"_":m_PushItem.SessionName,
		(NULL==m_PushItem.AuxMsg)?"_":m_PushItem.AuxMsg));

	return PushErrorCode_i(&m_PushItem);
}

BOOLEAN SYNCH_PushErrorCode(PVOID pContext)
{
	BOOLEAN rs = FALSE;
	CCmn5ErrorKrn* pError = static_cast<CCmn5ErrorKrn*>(g_pCapError);
	if (pError) {
		rs = (BOOLEAN)pError->SynchPushErrorCode((ERROR_CODE_PARMS*)pContext);
	}

	return rs;
}

BOOL CCmn5ErrorKrn::PushErrorCode(ULONG ErrorCode, ULONG MinorCode, char* AuxMsg, char* SessionName)
{
	ULONG rs = FALSE;

	ERROR_CODE_PARMS parms;
	parms.ErrorCode = ErrorCode;
	parms.MinorCode = MinorCode;
	parms.AuxMsg = AuxMsg;
	parms.SessionName = SessionName;

	if (m_InterruptObject && KeGetCurrentIrql() <= DISPATCH_LEVEL) {
		rs = KeSynchronizeExecution(*m_InterruptObject, SYNCH_PushErrorCode, &parms);
	} else {
		rs = SynchPushErrorCode(&parms);
	}

	return rs;
}

BOOL CCmn5ErrorKrn::SynchPushErrorCodeItem(CMN5_ERROR_CODE_ITEM* pItem)
{
	return PushErrorCode_i(pItem);
}

BOOLEAN SYNCH_PushErrorCodeItem(PVOID pContext)
{
	BOOLEAN rs = FALSE;
	CCmn5ErrorKrn* pError = static_cast<CCmn5ErrorKrn*>(g_pCapError);
	if (pError) {
		rs = (BOOLEAN)pError->SynchPushErrorCodeItem((CMN5_ERROR_CODE_ITEM*)pContext);
	}

	return rs;
}

BOOL CCmn5ErrorKrn::PushErrorCodeItem(CMN5_ERROR_CODE_ITEM* pItem)
{
	ULONG rs = FALSE;
	
	if (m_InterruptObject && KeGetCurrentIrql() <= DISPATCH_LEVEL) {
		rs = KeSynchronizeExecution(*m_InterruptObject, SYNCH_PushErrorCodeItem, pItem);
	} else {
		rs = SynchPushErrorCodeItem(pItem);
	}

	return rs;
}

BOOL CCmn5ErrorKrn::SynchPopErrorCodeItem(CMN5_ERROR_CODE_ITEM* pItem)
{
	ULONG rs = FALSE;

	rs = PopErrorCode_i(pItem);
//	KdPrint((DRIVERNAME " - CCmn5ErrorKrn::PopErrorCodeForSynchronizeExec rs = %d\n", rs));

	return rs;
}

BOOLEAN SYNCH_PopErrorCodeItem(PVOID pContext)
{
	BOOLEAN rs = FALSE;
	CCmn5ErrorKrn* pError = static_cast<CCmn5ErrorKrn*>(g_pCapError);
	if (pError) {
		rs = (BOOLEAN)pError->SynchPopErrorCodeItem((CMN5_ERROR_CODE_ITEM*)pContext);
	}

	return rs;
}

BOOL CCmn5ErrorKrn::PopErrorCodeItem(CMN5_ERROR_CODE_ITEM* pItem)
{
	ULONG rs = FALSE;

	RtlZeroMemory(pItem, sizeof(CMN5_ERROR_CODE_ITEM));
	
	if (m_InterruptObject && KeGetCurrentIrql() <= DISPATCH_LEVEL) {
		rs = KeSynchronizeExecution(*m_InterruptObject, SYNCH_PopErrorCodeItem, pItem);
	} else {
		rs = SynchPopErrorCodeItem(pItem);
	}

	return rs;
}

BOOLEAN SYNCH_EmptyErrorCode(PVOID)
{
	CCmn5ErrorKrn* pError = static_cast<CCmn5ErrorKrn*>(g_pCapError);
	if (pError) {
		pError->SynchEmptyErrorCode();
	}

	return TRUE;
}

void CCmn5ErrorKrn::SynchEmptyErrorCode()
{
	EmptyErrorCode_i();
}

void CCmn5ErrorKrn::EmptyErrorCode()
{
	if (m_InterruptObject && KeGetCurrentIrql() <= DISPATCH_LEVEL) {
		KeSynchronizeExecution(*m_InterruptObject, SYNCH_EmptyErrorCode, NULL);
	} else {
		SynchEmptyErrorCode();
	}
}


/*
ULONG CCmn5ErrorKrn::PushErrorCode(ULONG code, ULONG minorCode, char* auxMsg)
{
	// It will rely on the implementation of PushErrorCode in base class.
	// Do not consider locking strategy at this very function.
	return SuperClass::PushErrorCode(code, minorCode, auxMsg);
}
*/


CCmn5ErrorKrn::CCmn5ErrorKrn(CMN5_ERR_LEVEL level) : CCmn5Error(level)
{
	m_InterruptObject = NULL;
}

CCmn5ErrorKrn::~CCmn5ErrorKrn()
{

}
