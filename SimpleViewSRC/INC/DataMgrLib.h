#ifndef _DATAMGRLIB_H_
#define _DATAMGRLIB_H_

#include "string.h"
#include "vca_commondef.h"
#include "vca_generaldef.h"
#include "vca_zonedef.h"
#include "vca_eventdef.h"
#include "vca_eventmacros.h"
#include "vca_countingdef.h"
#include "vca_calibdef.h"
#include "vca_clsobjectdef.h"
#include "vca_biadef.h"
#include "vca_tamperdef.h"
#include "vca_ptztrackingdef.h"
#include <vector>
#include <list>

// Constants
#define VCA_CALIB_GRID_PICK			0x00000001
#define VCA_CALIB_GRID_DRAG			0x00000002
#define VCA_CALIB_GRID_ENDDRAG		0x00000004
#define VCA_CALIB_GRID_NODRAW		0x00000008
#define VCA_CALIB_GRID_MOUSEWHEEL   0x00000010
#define VCA_CALIB_MOUSEOVER			0x00000020
#define VCA_CALIB_RULER_BEGIN		0x00000040
#define VCA_CALIB_RULER_END			0x00000080
#define	VCA_CALIB_DRAW_HORIZON		0x00000100
#define	VCA_CALIB_INSERT_OBJ		0x00000200
#define	VCA_CALIB_TRANS_GRID		0x00000400

#define VCA_PTZ_PANTILT				0x00000001
#define VCA_PTZ_ZOOM				0x00000002
#define VCA_PTZ_ATBOXPICK			0x00000004

// Enums
typedef enum
{
	VCA_TRKMODE_TRACKER,
	VCA_TRKMODE_AUTOTRACKER,
}
VCA_TRKMODE_E;

typedef enum
{
	VCA_PTZCFGMODE_CPANTILT		= 0x0001,
	VCA_PTZCFGMODE_CPANTILTZOOM,
	VCA_PTZCFGMODE_ATBOXPICK
}
VCA_PTZCFGMODE_E;

typedef enum
{
	VCA_OP_ZONE		=	0x0001,
	VCA_OP_COUNTER,
	VCA_OP_NONE
}
VCA_OP_E;

typedef enum
{
	fZonesInfo		=	0x00000001,
	fEventsInfo		=	0x00000002,
	fCountersInfo	=	0x00000004,
	fClsObjectsInfo	=	0x00000008,
	fCalibInfo		=	0x00000010,
	fChannelInfo	=	0x00000020,
	fLicenseInfo	=	0x00000040,
	fBIAsInfo		=	0x00000080,
	fTamperInfo		=	0x00000100,
	fXMLInfo		=	0x00000200,
	fPTZTInfo		=	0x00000400,
	fStatus			=	0x10000000,
}
VCA_DATAMGRINFO_FLAG_E;

typedef struct
{
	VCA_ZONES_T			*pZones;
	VCA_EVENTS_T		*pEvents;
	VCA_COUNTERS_T		*pCounters;
	VCA_CLSOBJECTS_T	*pClsobjects;
	VCA_CALIB_ALL_INFO	*pCalibInfo;
	VCA_CHANNEL_INFO_T	*pChannelInfo;
	VCA_LICENSE_INFO_T	*pLicenseInfo;
	VCA_BIAS_T			*pBIAs;
	VCA_TAMPER_CONFIG	*pTamperInfo;
	VCA_XML_INFO_T		*pXMLInfo;
	VCA_PTZT_T			*pPTZTInfo;

	int					nStatus;
	int					nFlags;
}
VCA_DATAMGRINFO_T;

struct CNT_LINE_EVENTHIST
{
	VCA_COUNTLINE_EVENT info;
	VCA_ZONE_T *pZone;
	int age;
};

// CDataMgr Class
class CDataMgr
{
public:
	CDataMgr( void );
	CDataMgr( VCA_DATAMGRINFO_T *pDataMgrInfo );
	virtual ~CDataMgr( );

public:
	void				ClearAllInfo();
	void				ClearXMLInfo();

	VCA_DATAMGRINFO_T*	GetDataMgrInfo();
	VCA_ZONES_T*		GetZonesPtr() { return m_pZones; };
	VCA_EVENTS_T*		GetEventsPtr() { return m_pEvents; };
	VCA_COUNTERS_T*		GetCountersPtr() { return m_pCounters; };
	VCA_CLSOBJECTS_T*	GetClsobjectsPtr() { return m_pClsobjects; };
	VCA_CALIB_ALL_INFO*	GetCalibInfoPtr() { return m_pCalibInfo; };
	VCA_CHANNEL_INFO_T*	GetChannelInfoPtr() { return m_pChannelInfo; };
	VCA_LICENSE_INFO_T*	GetLicenseInfoPtr()	{ return m_pLicenseInfo; };
	VCA_BIAS_T*			GetBIAsPtr() { return m_pBIAs; };
	VCA_TAMPER_CONFIG*	GetTamperInfoPtr() { return m_pTamperInfo; };
	VCA_XML_INFO_T*		GetXMLInfoPtr() { return m_pXMLInfo; };
	VCA_PTZT_T*			GetPTZTInfoPtr() { return m_pPTZTInfo; };

	//void		UpdateAllZones( VCA_ZONES_T *pZones ) { m_pZones = pZones; ClearStatus(); };
	//void		UpdateAllEvents( VCA_EVENTS_T *pEvents ) { m_pEvents = pEvents; ClearStatus(); };
	//void		UpdateAllCounters( VCA_COUNTERS_T *pCounters ) { m_pCounters = pCounters; ClearStatus(); };
	//void		UpdateAllClsobjects( VCA_CLSOBJECTS_T *pClsobjects ) { m_pClsobjects = pClsobjects; ClearStatus(); };
	//void		UpdateCalibInfo( VCA_CALIB_ALL_INFO *pCalibInfo ) { m_pCalibInfo = pCalibInfo; ClearStatus(); };
	//void		UpdateChannelInfo( VCA_CHANNEL_INFO_T *pChannelInfo ) { m_pChannelInfo = pChannelInfo; ClearStatus(); };
	//void		UpdateLicenseInfo( VCA_LICENSE_INFO_T *pLicenseInfo ) { m_pLicenseInfo = pLicenseInfo; ClearStatus(); };
	//void		UpdateBIAs( VCA_BIAS_T *pBIAs ) { m_pBIAs = pBIAs; ClearStatus(); };
	//void		UpdateTamperInfo( VCA_TAMPER_CONFIG *pTamperInfo ) { m_pTamperInfo = pTamperInfo; ClearStatus(); };
	//void		UpdateXMLInfo( VCA_XML_INFO_T *pXMLInfo ) { m_pXMLInfo = pXMLInfo; ClearStatus(); };
	//void		UpdatePTZTInfo( VCA_PTZT_T *pPTZTInfo ) { m_pPTZTInfo = pPTZTInfo; ClearStatus(); };
	//void		UpdateAll( VCA_DATAMGRINFO_T *pDataMgrInfo )
	//{
	//	if ( pDataMgrInfo->nFlags & fZonesInfo )
	//		UpdateAllZones( pDataMgrInfo->pZones );

	//	if ( pDataMgrInfo->nFlags & fEventsInfo )
	//		UpdateAllEvents( pDataMgrInfo->pEvents );

	//	if ( pDataMgrInfo->nFlags & fCountersInfo )
	//		UpdateAllCounters( pDataMgrInfo->pCounters );

	//	if ( pDataMgrInfo->nFlags & fClsObjectsInfo )
	//		UpdateAllClsobjects( pDataMgrInfo->pClsobjects );

	//	if ( pDataMgrInfo->nFlags & fCalibInfo )
	//		UpdateCalibInfo( pDataMgrInfo->pCalibInfo );

	//	if ( pDataMgrInfo->nFlags & fChannelInfo )
	//		UpdateChannelInfo( pDataMgrInfo->pChannelInfo );

	//	if ( pDataMgrInfo->nFlags & fLicenseInfo )
	//		UpdateLicenseInfo( pDataMgrInfo->pLicenseInfo );

	//	if ( pDataMgrInfo->nFlags & fBIAsInfo )
	//		UpdateBIAs( pDataMgrInfo->pBIAs );

	//	if ( pDataMgrInfo->nFlags & fTamperInfo )
	//		UpdateTamperInfo( pDataMgrInfo->pTamperInfo );

	//	if ( pDataMgrInfo->nFlags & fXMLInfo )
	//		UpdateXMLInfo( pDataMgrInfo->pXMLInfo );
	//
	//	if ( pDataMgrInfo->nFlags & fPTZTInfo )
	//		UpdatePTZTInfo( pDataMgrInfo->pPTZTInfo );

	//	ClearStatus();
	//}

	int		GetStatus()	{ return m_nStatus; };
	void	ClearStatus() { m_nStatus = VCA_NO_CHANGE; };
	void	SetStatus( unsigned int newStatus ) { m_nStatus = m_nStatus | newStatus; };

	void	SetNativeSize( long width, long height );

// Mode
	void	SetMode( int iMode )	{ m_iMode = iMode; };
	int		GetMode( )				{ return m_iMode; };

// UID
	void	SetUID( unsigned int uiUID )	{ m_uiNextUID = uiUID; };
	unsigned int GetUID( )	{ return m_uiNextUID; };
	unsigned int IncrementUID( ) 
	{ 
		unsigned int uiUID = m_uiNextUID++;
		m_uiNextUID = (m_uiNextUID == UID_RESERVED) ? UID_DEFAULT : m_uiNextUID;
		return uiUID; 
	}

// Check supported features
	BOOL	CheckFeature( VCA_SYS_FEATURES_E eFeature, BOOL bAlsoEnabled );
	BOOL	CheckEventSupported( VCA_EVENT_T *pEvent, BOOL bAlsoEnabled );
	int		ApplyMaxLimits();

// Reset
	void	ResetAll();
	void	ResetZones();
	void	ResetEvents();
	void	ResetCounters();
	void	ResetClsobjects();

// Get RGB color
	unsigned int GetColor( unsigned int iIndex ) { return m_colorLUT[ iIndex % MAXZONECOLORS ]; };
	char*	GetColorName( unsigned int iIndex );

// All zone public methods
	int		AddZone( VCA_ZONE_T *pZone, int IsAddStockEvents );
	BOOL	ChangeZone( int iZoneIdx, VCA_ZONE_T *pZone );
	BOOL	RemoveZone( int iZoneIdx );
	VCA_ZONE_T *GetZone( int iZoneIdx );

// Dealing with the nodes
	BOOL	AddNode( unsigned int position, VCA_ZONE_T *pZone, unsigned int x, unsigned int y );
	int		ChangeNode( int iZoneIdx, int iNodeIdx, unsigned int x, unsigned int y );
	BOOL	RemoveNode( int iZoneIdx, int iRemoveNodeIdx );
	VCA_POINT_T	*GetNode( int iZoneIdx, int iNodeIdx );

// All clsobject public methods
	int		AddClsobject( VCA_CLSOBJECT_T *pClsobject );
	BOOL	ChangeClsobject( int iClsobjectIdx, VCA_CLSOBJECT_T *pClsobject );
	int		RemoveClsobject( int iClsobjectIdx );
	VCA_CLSOBJECT_T *GetClsobject( int iClsobjectIdx );

// Set selected/notified zone
	void	SetArea( int iAreaIdx, VCA_OP_E eType, VCA_AREA_STATUS_E eStatus );
	void	ClearArea( int iAreaIdx, VCA_OP_E eType, VCA_AREA_STATUS_E eStatus );
	void 	UpdateSelectAreaIdx( );
	void	UpdateNotifyAreaIdx( );
	void	ClearAllAreaFlags( );
	void	ClearAllSelectArea( );
	void	ClearAllNotifyArea( );

	BOOL	SetSelectArea( int iAreaIdx, VCA_OP_E eType );
	BOOL	SetNotifyArea( int iAreaIdx, VCA_OP_E eType );
	BOOL	SetActiveNode( int iActiveNodeIdx );
	BOOL	SetNotifyNode( int iNotifyNodeIdx );

	void	GetSelectAreaIdx( int *piAreaIdx, VCA_OP_E *pType )	{ *piAreaIdx = m_iSelectAreaIdx; *pType = m_eSelectAreaType; };
	void	GetNotifyAreaIdx( int *piAreaIdx, VCA_OP_E *pType )	{ *piAreaIdx = m_iNotifyAreaIdx; *pType = m_eNotifyAreaType; };
	void	GetOldNotifyAreaIdx( int *piAreaIdx, VCA_OP_E *pType )	{ *piAreaIdx = m_iOldNotifyAreaIdx; *pType = m_eOldNotifyAreaType; };
	int		GetActiveNodeIdx()		{ return m_iActiveNodeIdx;	};
	int		GetNotifyNodeIdx()		{ return m_iNotifyNodeIdx;	};

	int		GetLastZoneIdx() { return m_iLastZoneIdx; };
	int		GetLastCounterIdx() { return m_iLastCounterIdx; };
	void	SetLastZoneIdx( int iZoneIdx ) { m_iLastZoneIdx = iZoneIdx; };
	void	SetLastCounterIdx( int iCounterIdx ) { m_iLastCounterIdx = iCounterIdx; };

// Set/get stream number
	int		GetStreamNo() { return m_nStreamNo; };
	void	SetStreamNo( int nStreamNo ) { m_nStreamNo = nStreamNo; };

// Set/get channel number
	int		GetChannelNo() { return m_nChannelNo; };
	void	SetChannelNo( int nChannelNo ) { m_nChannelNo = nChannelNo; };

// Set/get display flags
	unsigned int	GetDisplayFlags( )	{ return m_uiDisplayFlags; };
	void	SetDisplayFlags( unsigned int uiDisplayFlags )	{ m_uiDisplayFlags = uiDisplayFlags; };

	BOOL	GetSelectFlag()		{ BOOL retVal = m_bSelectAreaChanged; m_bSelectAreaChanged = FALSE; return retVal;	};
	BOOL	GetNotifyFlag()		{ BOOL retVal = m_bNotifyAreaChanged; m_bNotifyAreaChanged = FALSE; return retVal;	};

	long	GetNumZones();
	void	GetZoneCentre( VCA_ZONE_T *pZone, VCA_POINT_T *pt );
	void	GetCounterCentre( VCA_COUNTER_T *pCounter, VCA_POINT_T *pt );

// All event/rules public methods
	void			PreLoadEvents();
	int				AddEvent( VCA_EVENT_T *pEvent );
	VCA_EVENT_T*	GetEvent( int iEventIdx );
	VCA_RULE_T*		GetRule( int iRuleIdx, VCA_EVENT_T *pEvent );

	int		AddDefaultZoneEvent( int iZoneid, BOOL isStock, BOOL setDefault, VCA_ZONE_STYLE_E zoneStyle );
	void	AssignDefaultEventToUndefinedZones( );
	BOOL	IsZoneAssignedToEvent( int iZoneid );
	void	ClearZoneEvents( int iZoneid );
	long	GetNumEvents();
	int		FindAvailableRuleIdx( VCA_EVENT_T *pEvent );

	BOOL	SetPresence( int iZoneIdx, BOOL isStockRule, BOOL isOverWrite );
	BOOL	RemovePresence( int iZoneIdx );
	BOOL	SetEnter( int iZoneIdx, BOOL isStockRule, BOOL isOverWrite );
	BOOL	RemoveEnter( int iZoneIdx );
	BOOL	SetExit( int iZoneIdx, BOOL isStockRule, BOOL isOverWrite );
	BOOL	RemoveExit( int iZoneIdx );
	BOOL	SetAppear( int iZoneIdx, BOOL isStockRule, BOOL isOverWrite );
	BOOL	RemoveAppear( int iZoneIdx );
	BOOL	SetDisappear( int iZoneIdx, BOOL isStockRule, BOOL isOverWrite );
	BOOL	RemoveDisappear( int iZoneIdx );
	BOOL	SetStop( int iZoneIdx, int iTime, BOOL isStockRule, BOOL isOverWrite );
	BOOL	RemoveStop( int iZoneIdx );
	BOOL	SetLoitering( int iZoneIdx, int iTime, BOOL isStockRule, BOOL isOverWrite );
	BOOL	RemoveLoitering( int iZoneIdx );
	BOOL	SetDirection( int iZoneIdx, int startangle, int finishangle, BOOL isStockRule, BOOL isOverWrite );
	BOOL	RemoveDirection( int iZoneIdx );
	BOOL	SetSpeed( int iZoneIdx, double fSpeedLo, double fSpeedUp, BOOL isStockRule, BOOL isOverWrite );
	BOOL	RemoveSpeed( int iZoneIdx );
	BOOL	SetAbObj( int iZoneIdx, int iTime, BOOL isStockRule, BOOL isOverWrite );
	BOOL	RemoveAbObj( int iZoneIdx );
	BOOL	SetRmObj( int iZoneIdx, int iTime, BOOL isStockRule, BOOL isOverWrite );
	BOOL	RemoveRmObj( int iZoneIdx );
	BOOL	SetTailgating( int iZoneIdx, int iTime, int iTriggerType, BOOL isStockRule, BOOL isOverWrite );
	BOOL	RemoveTailgating( int iZoneIdx );
	BOOL	SetLineCounterA( int iZoneIdx, int iCalibrationWidth, BOOL isStockRule, BOOL isOverWrite, BOOL tick = TRUE );
	BOOL	RemoveLineCounterA( int iZoneIdx );
	BOOL	SetLineCounterB( int iZoneIdx, int iCalibrationWidth, BOOL isStockRule, BOOL isOverWrite, BOOL tick = TRUE );
	BOOL	RemoveLineCounterB( int iZoneIdx );
	BOOL	SetColFilter( int iZoneIdx, unsigned short usColBin, unsigned short usThres, BOOL isStockRule, BOOL isOverWrite );
	BOOL	RemoveColFilter( int iZoneIdx );
	BOOL	SetSmoke( int iZoneIdx, int iTime, BOOL isStockRule, BOOL isOverWrite );
	BOOL	RemoveSmoke( int iZoneIdx );
	BOOL	SetFire( int iZoneIdx, int iTime, BOOL isStockRule, BOOL isOverWrite );
	BOOL	RemoveFire( int iZoneIdx );
	int		GetLineCounterA( int iZoneIdx ) ;
	int		GetLineCounterB( int iZoneIdx ) ;
	
	ANGLE_STATUS CheckZoneAngle( int iZoneIdx, int *iEventIdx, int  *iRuleIdx );
	int		FindZoneMatchedEvent( int iZoneIdx, VCA_RULE_E eType, int *eventIdx, int *ruleIdx );
	int		FindAvailableEventIdx( );
	int		FindRuleRelatedZone( VCA_RULE_T *pRule );

// FIXME, the following three functions need to be removed, use FindZoneMatchedEvent instead
	BOOL	IsLineCounterA( int iZoneId, int *iEventIdx, int  *iRuleIdx );
	BOOL	IsLineCounterB( int iZoneId, int *iEventIdx, int  *iRuleIdx );
	BOOL	IsItACountingLine( int iZoneId );

// All counter public methods
	int					AddCounter( VCA_COUNTER_T *pCounter );
	BOOL				ChangeCounter( int iCounterIdx, VCA_COUNTER_T *pCounter );
	BOOL				RemoveCounter( int iCounterIdx );
	VCA_COUNTER_T*		GetCounter( int iCounterIdx );
	VCA_SUBCOUNTER_T*	GetSubCounter( int iSubCounterIdx, VCA_COUNTER_T *pCounter );

	int		FindNextCounterIdx();
	int		FindNextSubCounterIdx( VCA_COUNTER_T *pCounter );
	int		GetNumCounters();
	void	ValidateAllZones();
	void	ValidateAllCounters();
	int		ValidateSingleCounter( VCA_COUNTER_T *pCounter );

// HTTP-API related
	void	SaveNativeInfo();
	void	SaveNativeCalibInfo();
	void	ResetAllInfoStatus();
	VCA_ZONES_T*		GetNativeZonesPtr() { return m_pNativeZones; };
	VCA_EVENTS_T*		GetNativeEventsPtr() { return m_pNativeEvents; };
	VCA_COUNTERS_T*		GetNativeCountersPtr() { return m_pNativeCounters; };
	VCA_CLSOBJECTS_T*	GetNativeClsobjectsPtr() { return m_pNativeClsobjects; };
	VCA_CALIB_ALL_INFO* GetNativeCalibInfoPtr()	{ return m_pNativeCalibInfo; };
	VCA_CHANNEL_INFO_T* GetNativeChannelInfoPtr() { return m_pNativeChannelInfo; };
	VCA_LICENSE_INFO_T* GetNativeLicenseInfoPtr() { return m_pNativeLicenseInfo; };
	VCA_BIAS_T*			GetNativeBIAsPtr() { return m_pNativeBIAs; };
	VCA_TAMPER_CONFIG*	GetNativeTamperInfoPtr() { return m_pNativeTamperInfo; };
	VCA_PTZT_T*			GetNativePTZTInfoPtr() { return m_pNativePTZTInfo; };

	int		FindUnTickedEventIdx();
	void	UpdateAllSourceTrig( int iEventId, int iEventIdNew );
	void	RemoveDefaultEvents();

	void	SetCalibInfo(VCA_CALIB_INFO_T_D calibInfo, bool bCentre);
	VCA_CALIB_INFO_T_D	GetCalibInfo();

	int		GetZoneIdFromRule( VCA_RULE_T *pRule );

// Countingline related
	void UpdateCountingLineEvents(std::vector<VCA_COUNTLINE_EVENT>& countLineEvents);
	std::list<CNT_LINE_EVENTHIST>* GetCountingLineEventsA() { return m_countLineEventsA;}
	std::list<CNT_LINE_EVENTHIST>* GetCountingLineEventsB() { return m_countLineEventsB;}
	void ClearCountingLineEvents();
	BOOL ShowLineCounterHelp() {return m_bShowLineCounterHelp;}
	VOID ShowLineCounterHelp(BOOL b) {m_bShowLineCounterHelp = b;}

// PTZ config related
	void	SetATTargetId( int iATTargetId )	{ m_iATTargetId = iATTargetId; };
	int		GetATTargetId( )	{ return m_iATTargetId; };
	void	SetATTargetBox( RECT *pRect )	{ m_rcATBox = *pRect; };
	RECT	GetATTargetBox( )	{ return m_rcATBox; };

	void			SetTrkMode( VCA_TRKMODE_E eTrkMode )	{ m_eTrkMode = eTrkMode; };
	VCA_TRKMODE_E	GetTrkMode( )	{ return m_eTrkMode; };
	void			SetVCAInhibit( BOOL bInhibit )	{ m_bInhibit = bInhibit; };
	BOOL			GetVCAInhibit()	{ return m_bInhibit; };
	
	BOOL	IsZoneDisabled( )
	{
		if ( ( VCA_TRKMODE_AUTOTRACKER == m_eTrkMode ) || ( m_bInhibit ) )
			return TRUE;
		else
			return FALSE;
	}

	BOOL	SetPanTiltZoomSpeed( int iPanSpeed, int iTiltSpeed, int iZoomSpeed )
	{
		if ( ( m_iPanSpeed != iPanSpeed ) || ( m_iTiltSpeed != iTiltSpeed ) || ( m_iZoomSpeed != iZoomSpeed ) )
		{
			m_iPanSpeed	= iPanSpeed;
			m_iTiltSpeed= iTiltSpeed;
			m_iZoomSpeed= iZoomSpeed;
			return TRUE;
		}
		else
			return FALSE;
	}

	void	GetPanTiltZoomSpeed( int *piPanSpeed, int *piTiltSpeed, int *piZoomSpeed )
	{
		*piPanSpeed	= m_iPanSpeed;
		*piTiltSpeed= m_iTiltSpeed;
		*piZoomSpeed= m_iZoomSpeed;
	}

	void				SetPTZCFGMode( VCA_PTZCFGMODE_E ePTZCFGMode )	{ m_ePTZCFGMode = ePTZCFGMode; };
	VCA_PTZCFGMODE_E	GetPTZCFGMode()	{ return m_ePTZCFGMode; };

	void				SetPTZCFGOption( unsigned int uiOption )	{ m_uiPTZCFGOption = uiOption; };
	unsigned int		GetPTZCFGOption()	{ return m_uiPTZCFGOption; };

protected:
// All zone protected methods
	int		FindNextZoneIdx( );
	int		FindNextClsobjectIdx( );

// All event protected methods
protected:
	int		GetRule_LINECOUNTERT( int iZoneIdx, VCA_RULE_E eRule );
	BOOL    SetRule_LINECOUNTERT( int iZoneIdx, VCA_RULE_E eRule, int iCalibrationWidth, BOOL isStockRule, BOOL isOverWrite, BOOL tick );
	BOOL	SetRule_BASICT( int iZoneIdx, VCA_RULE_E eRule, BOOL isStockRule, BOOL isOverWrite );
	BOOL	SetRule_TIMERT( int iZoneIdx, VCA_RULE_E eRule, int iTime, int iFlags, BOOL isStockRule, BOOL isOverWrite );
	BOOL	RemoveRule( int iZoneIdx, VCA_RULE_E eRule );

	int		GetSubCounterTypeNum( VCA_COUNTER_T *pCounter, unsigned short usSubCounterType, unsigned short *pusSubCounterId );
	int		ClearSubCounterType( VCA_COUNTER_T *pCounter, unsigned short usSubCounterType ); 

protected:
	bool m_bMemIsInternal;
	VCA_DATAMGRINFO_T	m_tDataMgrInfo;	// Wrapper for all info
	VCA_ZONES_T*		m_pZones;
	VCA_EVENTS_T*		m_pEvents;
	VCA_COUNTERS_T*		m_pCounters;
	VCA_CLSOBJECTS_T*	m_pClsobjects;
	VCA_CALIB_ALL_INFO*	m_pCalibInfo;
	VCA_CHANNEL_INFO_T*	m_pChannelInfo;
	VCA_LICENSE_INFO_T*	m_pLicenseInfo;
	VCA_BIAS_T*			m_pBIAs;
	VCA_TAMPER_CONFIG*	m_pTamperInfo;
	VCA_XML_INFO_T*		m_pXMLInfo;
	VCA_PTZT_T*			m_pPTZTInfo;
	int					m_nStatus;
	int					m_nStreamNo;
	int					m_nChannelNo;
	int					m_iMode;

	VCA_ZONES_T*		m_pNativeZones;
	VCA_EVENTS_T*		m_pNativeEvents;
	VCA_COUNTERS_T*		m_pNativeCounters;
	VCA_CLSOBJECTS_T*	m_pNativeClsobjects;
	VCA_CALIB_ALL_INFO*	m_pNativeCalibInfo;
	VCA_CHANNEL_INFO_T*	m_pNativeChannelInfo;
	VCA_LICENSE_INFO_T*	m_pNativeLicenseInfo;
	VCA_BIAS_T*			m_pNativeBIAs;
	VCA_TAMPER_CONFIG*	m_pNativeTamperInfo;
	VCA_PTZT_T*			m_pNativePTZTInfo;

	unsigned int		m_uiMaxNumZones;
	unsigned int		m_uiMaxNumEvents;
	unsigned int		m_uiMaxNumCounters;
	unsigned int		m_uiMaxNumChannels;

	unsigned int		m_uiNextUID;

public:
// All calibration stuff
	POINT			m_LastMousePos, m_LastClickedMousePos;
	long			m_zDelta;
	bool			m_bSetCameraParams;
	bool			m_bCentreCalibModels;
	unsigned		m_uiCalibDrawOptions;

protected:
	static unsigned int	m_colorLUT[ MAXZONECOLORS ];
	static char		*strColorName[MAXZONECOLORS];
	long			m_lNativeWidth;
	long			m_lNativeHeight;

	int				m_iSelectAreaIdx;
	int				m_iOldSelectAreaIdx;
	VCA_OP_E		m_eSelectAreaType;
	VCA_OP_E		m_eOldSelectAreaType;

	int				m_iNotifyAreaIdx;
	int				m_iOldNotifyAreaIdx;
	VCA_OP_E		m_eNotifyAreaType;
	VCA_OP_E		m_eOldNotifyAreaType;

	int				m_iActiveNodeIdx;
	int				m_iOldActiveNodeIdx;
	int				m_iNotifyNodeIdx;
	int				m_iOldNotifyNodeIdx;
	unsigned int	m_uiDisplayFlags;
	
	BOOL			m_bSelectAreaChanged;
	BOOL			m_bNotifyAreaChanged;

	int				m_iLastZoneIdx;
	int				m_iLastCounterIdx;

	int				m_iATTargetId;
	RECT			m_rcATBox;

	VCA_TRKMODE_E	m_eTrkMode;
	BOOL			m_bInhibit;

	VCA_PTZCFGMODE_E m_ePTZCFGMode;
	unsigned int	m_uiPTZCFGOption;
	int				m_iPanSpeed;
	int				m_iTiltSpeed;
	int				m_iZoomSpeed;

	std::list< CNT_LINE_EVENTHIST> m_countLineEventsA[MAX_NUM_ZONES], m_countLineEventsB[MAX_NUM_ZONES];
	BOOL			m_bShowLineCounterHelp;

//Debug stuff
public:
	void ToggleDebug() {isDebug = !isDebug;};
	bool IsDebug() {return isDebug;};
protected:
	bool isDebug;
};

#endif // _DATAMGRLIB_H_