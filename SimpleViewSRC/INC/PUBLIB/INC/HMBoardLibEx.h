//=============================================================================
//- HMBoardLibEx.h: Advanced API commands
//
// The optimal codec configurations are usually done in SDK and guarantee the best
// video quality. But if there are special needs for the quality even in some poor 
// conditions, there can be likelihoods for more enhancement using the advanced 
// API commands.
// The best uses for these commands are the responsibility for the user because
// we cannot provide the optimal settings for all circumstances.
//
//
#ifndef HMBOARDLIB_EX_H
#define HMBOARDLIB_EX_H



//-----------------------------------------------------------------------------
//- Additional information for COD5_CAC_BITRATE
//
// cmd: COD5_CAC_BITRATE
// param0: COD5_BM_VBRR
// param1: bitrate
// param2: peakbitrate
// param3: QP_RANGE_MIN(0 is internal default)
//
// cmd: COD5_CAC_BITRATE
// param0: COD5_BM_CBR
// param1: bitrate
// param2: COD5_BMCBR_BUFFER_CBR_ZERO_PADDING_xxx
// param3: COD5_BMCBR_BUFFER_LOW_DELAY_MODE_xxx
//
// definitions for param{n}
#define COD5_BMCBR_BUFFER_CBR_ZERO_PADDING_ENABLE	1 
#define COD5_BMCBR_BUFFER_CBR_ZERO_PADDING_DISABLE	2
#define COD5_BMCBR_BUFFER_LOW_DELAY_MODE_ENABLE		1
#define COD5_BMCBR_BUFFER_LOW_DELAY_MODE_DISABLE	2



//-----------------------------------------------------------------------------
//- Additional API commands directly to use the codec chip objects
//
// These commands are just used for the test of the adjustable features in codec 
// So the UDA5 API commands will be added if we think that the specific 
// features are permanently used to be adjustable for some cases.
// All commands for the codec chip are described in firmware document provided by
// chip vendor.
//
// cmd: COD5_CAC_AVCENC_PARAM
// param0: Q_AVCENC command
// param1-3: arguments for the command
#define COD5_CAC_AVCENC_PARAM			 0x0108000F
//
// cmd: COD5_CAC_LVPP_PARAM
// param0: Q_LVPP command
// param1-3: arguments for the command
#define COD5_CAC_LVPP_PARAM				 0x01080010
//
// cmd: COD5_CAC_NVPP_PARAM
// param0: Q_NVPP command
// param1-3: arguments for the command
#define COD5_CAC_NVPP_PARAM				 0x01080011
//
// cmd: COD5_CAC_AUDENC_PARAM
// param0: Q_AUDENC command
// param1-3: arguments for the command
#define COD5_CAC_AUDENC_PARAM			 0x01080012



// cmd: COD5_CAC_ADAPTIVE_FRAMERATE_ENABLE
// param0: 1 to enable. 0 is internal default.
#define COD5_CAC_ADAPTIVE_FRAMERATE_ENABLE		0x01081001



// cmd: COD5_CAC_ADAPTIVE_FRAMERATE_PARAM
// param0: MIN_QP 
// param1: MAX_QP 
// param2: SCALING_DENOMINATOR 
// param3: SCALING_MIN_NUMERATOR
#define COD5_CAC_ADAPTIVE_FRAMERATE_PARAM		0x01081002


#define COD5_CAC_IP_FIELD_CODING				0x01081003



// Controls optional intra refresh of P frames, useful for extremely long GOPs and infinite GOPs. Selects a
// length for a row of macroblocks that will be forced to intra coding, moving through the image by the length
// of the row each P frame. This means that a larger row size means a faster refresh. For example, with a
// size of 20, macroblocks 0-19 would be forced to intra coding in the first P frame, then macroblocks 20-39
// would be forced to intra coding in the next P frame.
// Parameters:
// rows Number of macroblocks per row, 0 to disable.
#define COD5_CAC_ROLLING_REFRESH_MB_ROWS		0x01081004



// cmd: COD5_CAC_SCENE_CHANGE
// param0: Q_AVCENC_CMP_SCENE_CHANGE_ENABLE
// param1: Q_AVCENC_CMP_SCENE_CHANGE_I_SLICE
// param2: Q_AVCENC_CMP_SCENE_CHANGE_NEW_GOP
#define COD5_CAC_SCENE_CHANGE					0x01081005



//-----------------------------------------------------------------------------
//- Configures the Temporal Noise Reducing Filter in the VPP.
//
// This method configures the recursive Temporal Noise Reducing Filter which is used to get rid.
// of the Gaussian noise in the luma and chroma planes. There are seperate parameters to control
// the luma and chroma filtering. The strength of the filter can be altered by changing the ygain
// and cgain registers from 0 to 7. A value of 0 for the gain's results in no-filtering performed while
// a value of 7 results in maximum filtering. <br/>
// <br>
// By using the ylowThr and yhighThr in conjunction with ygainIndex0 and ygainIndex1 different
// levels of filtering can be performed on regions with low and high luminance. The filter strength
// ygainIndex0 is applied to pixels which satisfy the condition ylowThr <= luma <= yhighThr
// and a filter strength of ygainIndex1 is applied otherwise. <br/>
// Similarly the chroma filtering can be adjusted by using the colorMask in conjuction with cgainIndex0 and cgainIndex1.
// The gain cgainIndex0 is applied to pixels that meet the color combination configured using colorMask and a filter strength
// of cgainIndex1 is applied otherwise. The colorMask selects a combination of color regions. <br/>
// Bit 0: Enables Color0 <br/>
// Bit 1: Enables Color1 <br/> 
// Bit 2: Enables Color2 <br/>
// Bit 3: Enables Color3 <br/>
// <br>
// @param vpp The instance of the vpp (lvpp/nvpp) on which the filter is invoked
// @param colorMask 0-15
// @param ycorrIndex     0-7
// @param ylowThr        0-255
// @param yhighThr       0-255
// @param ygainIndex0    0-7
// @param ygainIndex1    0-7
// @param cgainIndex0    0-7
// @param cgainIndex1    0-7
// @param vMotFiltBypass 0-1
// @return nil (function will assert if there is a problem)
//
// @usage filterTemporalMotion( lvpp0, 0, 0, 0, 255, 7, 5, 5, 5, 0)
//<table class="code_sample">
//<tr>
//<td><pre>
// <b>Code Sample</b>:<br> 
// <br>
// <b>lvpp0:setParam({param=Q_LVPP_CMP_HW_BYPASS_TFILTY,        value=0})</b><br>
// <b>lvpp0:setParam({param=Q_LVPP_CMP_HW_BYPASS_TFILTC,        value=0})</b><br>
// The luma and the chroma filtering is enabled by setting the BYPASS_TFILTY and
// BYPASS_TFILTC to zero. <br>
// If the bypass bit is set to 1, the temporal statistics
// will not be created and will affect the encoding quality. <br>
// <br> 
// <b>filterTemporalMotion(lvpp0, 0, 0, 16, 236, 5, 7, 5, 5, 0) </b><br>
// Luma filtering with a gain of 5 will be applied for pixels between 16 amd 236, while a gain of 7 will be applied to other pixels.<br>
// <br> 
// <b>filterTemporalMotion(lvpp0, 2, 0, 16, 236, 5, 7, 5, 7, 0) </b><br>
// If the color detection is on, all chroma pixels that belong to color region 2 will be filtered with a gain of 7, <br>
// while a gain of 5 will be applied elsewhere.<br>
//</pre><td>
//<tr>
// </table>
//
// cmd: COD5_CAC_TEMPORAL_FILTER
// param0: pointer to COD5_TEMPORAL_FILTER structure
#define COD5_CAC_TEMPORAL_FILTER				0x01081006
//
// COD5_TEMPORAL_FILTER structure
typedef struct _COD5_TEMPORAL_FILTER {
	int colorMask;
	int ycorrIndex;
	int ylowThr;
	int yhighThr;
	int ygainIndex0;
	int ygainIndex1;
	int cgainIndex0;
	int cgainIndex1;
	int vMotFiltBypass;
} COD5_TEMPORAL_FILTER;



//-----------------------------------------------------------------------------
//- Configures the PAFF thresold/offset in AVCENC.
//
// Default value in firmware
// Q_AVCENC_CMP_PAFF_LOW_THRESH = 78
// Q_AVCENC_CMP_PAFF_HIGH_THRESH = 128
// Q_AVCENC_CMP_PAFF_OFFSET_MBAFF = 50
// Q_AVCENC_CMP_PAFF_OFFSET_I = 0
// Q_AVCENC_CMP_PAFF_OFFSET_P = 10
// Q_AVCENC_CMP_PAFF_OFFSET_B = 20
// Q_AVCENC_CMP_PAFF_OFFSET_BR = 10
//  
// All the values are used by dividing the thresholds by 100 inside the firmware.
// eg. 128 is used as 1.28 inside the firmware.
//  
// Here is a brief description of the algorithm. For each pair of macroblock in
// top/bottom field we compute a ratio of the variances. If the variances are too close
// the pair of macroblocks should be coded as frame or else should be coded as field.
//  
// The ratio is threholded against Q_AVCENC_CMP_PAFF_LOW_THRESH and
// Q_AVCENC_CMP_PAFF_HIGH_THRESH. If the ratio is less than HIGH_THRESH and greater than
// LOW_THRESH it should be coded as frame else field. We compute the total number of
// macroblocks that should be coded as fields and coded as frame in each frame and take
// their ratio.
// 
// The ratio field2frame = num of mbs coded as field/ num of mbs coded as frames is
// computed.
//  
// The ratio field2rame < Threshold(T) then the frame is coded as frame else as field.
//  
// T = 1 + Q_AVCENC_CMP_PAFF_OFFSET_MBAFF + Q_AVCENC_CMP_PAFF_OFFSET_{I/P/B/Br}
//  
// You do not use B frames and hence Q_AVCENC_CMP_PAFF_OFFSET_B and 
// Q_AVCENC_CMP_PAFF_OFFSET_BR do not matter.
//  
// NOTES
// 1) Typically set Q_AVCENC_CMP_PAFF_HIGH_THRESH as the inverse of Q_AVCENC_CMP_PAFF_LOW_THRESH.
// 2) Lowering the value of Q_AVCENC_CMP_PAFF_LOW_THRESH will biasing towards frame
// coding while increasing it will bias towards field coding.
// 3) The offsets are values which are used to bias frame/field coding with a particular
// encoding type.
//
//param0: Low Threshold to determine frame coding range. Q_AVCENC_CMP_PAFF_LOW_THRESH.
//param1: High Threshold to determine frame coding range. Q_AVCENC_CMP_PAFF_HIGH_THRESH.
#define COD5_CAC_PAFF_THRESHOLD_PARAM	0x01081007
//
//param0: PAFF Picture offset for MBAFF pictures. Q_AVCENC_CMP_PAFF_OFFSET_MBAFF.
//param1: PAFF Picture offset for I-pictures. Q_AVCENC_CMP_PAFF_OFFSET_I.
//param2: PAFF Picture offset for P-pictures. Q_AVCENC_CMP_PAFF_OFFSET_P.
//param3: PAFF Picture offset for B-pictures. Q_AVCENC_CMP_PAFF_OFFSET_B.
#define COD5_CAC_PAFF_OFFSET_PARAM		0x01081008



#define COD5_CAC_CODEC_OBJECT_STATUS	0x01080001// exclusively used only at HM Series
#define COD5_CAC_CODEC_CHANNEL_CONFIG_STATUS	0x01081009
#define COD5_CAC_CODEC_QHALEM_RW	0x0108100A
#define COD5_CAC_CODEC_QHALQCC_RW	0x0108100B
#define COD5_CAC_CODEC_QHALEM_RW_BYTE	0x0108100C
#define COD5_CAC_CODEC_VERBOSELEVEL	0x0108100D



//-----------------------------------------------------------------------------
//- Reserved API commands
//
#define COD5_CAC_NUM_REFERENCE_FRAMES	0x01080004// not implemented
#define COD5_CAC_QS_MATRIX						0x01080003// not implemented
#define COD5_CAC_SUBPEL_ACCURACY			0x01080009// not implemented




#endif





