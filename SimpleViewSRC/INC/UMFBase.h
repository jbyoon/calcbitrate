#ifndef __UMF_BASE_H__
#define __UMF_BASE_H__

// macro to create FOURCC
#ifndef UMF_DO_FOURCC
#define UMF_DO_FOURCC(ch0, ch1, ch2, ch3) \
    ( (unsigned int)(unsigned char)(ch0) | ( (unsigned int)(unsigned char)(ch1) << 8 ) | \
    ( (unsigned int)(unsigned char)(ch2) << 16 ) | ( (unsigned int)(unsigned char)(ch3) << 24 ) )
#endif // UMF_DO_FOURCC

/***************************************************************************/

#define UMF_CHECK(EXPRESSION, ERR_CODE) { \
  if (!(EXPRESSION)) { \
    TRACE(TEXT("[%s] FAILED (%s)"), TEXT(#ERR_CODE), TEXT(#EXPRESSION)); \
    return ERR_CODE; \
  } else { \
    TRACE(TEXT("SUCCESSED (%s)"), TEXT(#EXPRESSION)); \
  } \
}

#define UMF_ARRAY_SIZE(ARR) (sizeof(ARR)/sizeof(ARR[0]))

#define UMF_SET_ZERO(VAR)   memset(&(VAR), 0, sizeof(VAR))

/***************************************************************************/

enum
{
	UMF_ID_NONE					= -1,
	UMF_MAXIMUM_SOURCE_FACTORY	= 32,
	UMF_MAXIMUM_SOURCE			= 128,
	UMF_MAXIMUM_VIEW			= 32,
	UMF_MAXIMUM_ENGINE			= 128,
};


typedef enum	//Define based DirectX 9
{
    UMF_NONE    = 0,
    UMF_YV12    = 1,    // Planar Y, V, U (4:2:0) (note V,U order!)
    UMF_NV12    ,       // Planar Y, merged U->V (4:2:0)
    UMF_YUY2    ,       // Composite Y->U->Y->V (4:2:2)
    UMF_UYVY    ,       // Composite U->Y->V->Y (4:2:2)
    UMF_YUV411  ,       // Planar Y, U, V (4:1:1)
    UMF_YUV420  ,       // Planar Y, U, V (4:2:0)
    UMF_YUV422  ,       // Planar Y, U, V (4:2:2)
    UMF_YUV444  ,       // Planar Y, U, V (4:4:4)
    UMF_Y411    ,       // Composite Y, U, V (4:1:1)
    UMF_Y41P    ,       // Composite Y, U, V (4:1:1)
    UMF_RGBA32  ,       // Composite B->G->R->A
	UMF_RGBX32  ,       // Composite B->G->R->X
    UMF_RGB24   ,       // Composite B->G->R
    UMF_RGB565  ,       // Composite B->G->R, 5 bit per B & R, 6 bit per G
    UMF_RGBA555 ,       // Composite B->G->R->A, 5 bit per component, 1 bit per A
	UMF_RGB555  ,       // Composite B->G->R, 5 bit per component
    UMF_RGB444  ,       // Composite B->G->R->A, 4 bit per component
    UMF_GRAY    ,       // Luminance component only.
    UMF_YUV420A ,       // Planar Y, U, V, Alpha
    UMF_YUV422A ,       // Planar Y, U, V, Alpha
    UMF_YUV444A ,       // Planar Y, U, V, Alpha
    UMF_YVU9    ,       // Planar Y, U, V
    UMF_GRAYA   ,       // Luminance with Alpha
    UMF_D3D_SURFACE,    // Pointer to D3D surface
}eUMF_COLORFORMAT ;


// negative : Error
// 0		: Success
// positive : Warning
typedef enum 
{
	UMF_CHNAGE_SOURCE           = 1,	// return when change source
    UMF_OK                      = 0,	// no error
    UMF_ERR_FAILED              = -999,
    UMF_ERR_NOT_INITIALIZED     = -998,
    UMF_ERR_TIMEOUT             = -987,
    UMF_ERR_NOT_ENOUGH_DATA     = -996,	// not enough input data
    UMF_ERR_NULL_PTR            = -995, // null pointer in input parameters
	UMF_ERR_INIT                = -899,
    UMF_ERR_SYNC                = -897, // can't find sync word in buffer
    UMF_ERR_NOT_ENOUGH_BUFFER   = -896, // not enough buffer to put output data
    UMF_ERR_END_OF_STREAM       = -895,
    UMF_ERR_OPEN_FAILED         = -894,	// failed to open file/device
    UMF_ERR_ALLOC               = -883, // failed to allocate memory
    UMF_ERR_INVALID_STREAM      = -882,
    UMF_ERR_UNSUPPORTED         = -879,
    UMF_ERR_NOT_IMPLEMENTED     = -878,
    UMF_ERR_INVALID_PARAMS      = -876,
	UMF_ERR_XML_PARSING			= -875,
    UMF_ERR_NOT_READY			= -874,
	UMF_ERR_COMMAND_NOT_FOUND	= -873
}eUMF_STATUS;


#define UMF_SUCCEEDED(Status) ((eUMF_STATUS)(Status) == 0)
#define UMF_FAILED(Status) ((eUMF_STATUS)(Status) < 0)


static BOOL GetBitMapInfoHeaderFromUMFColor(DWORD width, DWORD height, eUMF_COLORFORMAT format, BITMAPINFOHEADER *info)
{
	memset(info, 0 ,sizeof(BITMAPINFOHEADER));
	info->biSize	= sizeof(BITMAPINFOHEADER);
	info->biWidth	= width;
	info->biHeight	= height;

	switch(format){
		case UMF_YV12:
			info->biBitCount	= 12;
			info->biSizeImage	= (width*height*3)/2;
			info->biCompression = UMF_DO_FOURCC('Y','V','1','2');
			break;

		case UMF_YUY2:
			info->biBitCount	= 16;
			info->biSizeImage	= width*height*2;
			info->biCompression = UMF_DO_FOURCC('Y','U','Y','2');
			break;

		case UMF_UYVY:
			info->biBitCount	= 16;
			info->biSizeImage	= width*height*2;
			info->biCompression = UMF_DO_FOURCC('U','Y','V','Y');
			break;
		
		case UMF_RGBA32:
		case UMF_RGBX32:
			info->biBitCount	= 32;
			info->biSizeImage	= width*height*4;
			break;

		case UMF_RGB24:
			info->biBitCount	= 24;
			info->biSizeImage	= width*height*3;
			break;

		case UMF_RGB565:
			info->biBitCount	= 16;
			info->biSizeImage	= width*height*2;
			break;

		case UMF_RGB555:
			info->biBitCount	= 15;
			info->biSizeImage	= width*height*2;
			break;

		default :
			return FALSE;
	}

	return TRUE;
}


static eUMF_COLORFORMAT GetUMFColorFromBitmapInfo(BITMAPINFOHEADER *info)
{
	if(UMF_DO_FOURCC('Y','V','1','2') == info->biCompression){
		return UMF_YV12;
	}else if(UMF_DO_FOURCC('Y','U','Y','2') == info->biCompression){
		return UMF_YUY2;
	}else if(UMF_DO_FOURCC('U','Y','V','Y') == info->biCompression){
		return UMF_UYVY;
	}else if(0 == info->biCompression){ //RGB
		if(info->biBitCount == 32) return UMF_RGBA32;
		else if(info->biBitCount == 24) return UMF_RGB24;
		else if(info->biBitCount == 16) return UMF_RGB565;
		else if(info->biBitCount == 15) return UMF_RGB555;
	}

	return UMF_NONE;
}

static float GetUMFColorByte(eUMF_COLORFORMAT format)
{
	switch(format){
		case UMF_YUY2:
		case UMF_UYVY:
		case UMF_RGB565:
		case UMF_RGBA555:
		case UMF_RGB555:
			return 2;

		case UMF_YV12:
		case UMF_NV12:
		case UMF_Y411:
		case UMF_Y41P:
		case UMF_RGB444:
			return 1.5;

		case UMF_RGBA32:
		case UMF_RGBX32:
			return 4;

		case UMF_RGB24:
			return 3;

		case UMF_GRAY:
			return 1;
	}

	return 4;
}

#endif /* __UMF_STRUCTURES_H__ */
