#ifndef __BASE_UMF_SORUCE_INTERFACE_H__
#define __BASE_UMF_SORUCE_INTERFACE_H__

#include "IBaseUMFStorage.h"

/************************************************************************
- SRCTYPE : 4bytes (32bits)
 Express as hexadecimal digit : [H7,H6,H5,H4,H3,H2,H1,H0]
 Video 			TYPE : H1,H0 (8bits)
 Audio 			TYPE : H3,H2 (8bits)
 Ext1(Meta)  	TYPE : H4 (4bits)
 Ext2 			TYPE : H5 (4bits)
 Ext3 			TYPE : H6 (4bits) 
 Ext4 			TYPE : H7 (4bits)
*************************************************************************/
enum  {
	UMF_ST_NONE				= 0x00000000, 

// Video	
	UMF_ST_VID_RAWFILE		= 0x00000001,
	UMF_ST_VID_CAP5			= 0x00000002,
	UMF_ST_VID_STREAM		= 0x00000003,
	UMF_ST_VID_COMPFILE		= 0x00000004,
	UMF_ST_VID_RTSP			= 0x00000005,

	UMF_ST_VID_LINKLOAD_JPEG= 0x00000006,
	UMF_ST_VID_LINKLOAD_H264= 0x00000007,
	UMF_ST_VID_RAWFILE_H264	= 0x00000008,	//NAL Unit Packet
	UMF_ST_VID_INNODEPVMS	= 0x00000009,	
	
// Audio	
	UMF_ST_AUD_RTSP			= 0x00000100,
	
// Etc1	(Meta type)
	UMF_ST_META_VCA			= 0x00010000,
	UMF_ST_META_MD			= 0x00020000,
	
// Etc2 
	UMF_ST_RAW_RTSP			= 0x00100000,
};
typedef UINT SRCTYPE;


#define	IsVideoSource(st)	((st)&(0x000000ff))
#define	IsAudioSource(st)	((st)&(0x0000ff00))
#define	IsMetaSource(st)	((st)&(0x000f0000))

#define IsRawRTSP(st)		(st&UMF_ST_RAW_RTSP)

//<JL>
#define	IsMetaDataSource(st)	((st)&(UMF_ST_META_VCA))
//</JL>

class IBaseUMFSource
{
public:	

	typedef enum {INIT, OPEN, RUN } eStatus;

	virtual SRCTYPE		GetSourceType()	= 0;
	virtual eStatus		GetStatus()		= 0;

	virtual eUMF_STATUS	Open(UINT SourceId, IBaseUMFStorage *pXmlStorage) = 0;
	virtual void		Close()		= 0;
	virtual eUMF_STATUS	Start()		= 0;
	virtual void		Stop()		= 0;

	virtual UINT		GetSourceId()	= 0;
	virtual PXML		GetSourceInfo()	= 0;

	virtual	HANDLE		GetEvent() 		= 0;
	virtual eUMF_STATUS	GetMMData(SRCTYPE* pType, BYTE **pMMData, UINT* pMMDataLen, FILETIME *timestamp)	= 0;
	virtual eUMF_STATUS	ReleaseMMData() = 0;
	
	virtual void		DestroySelf() 	= 0;
};


class IBaseUMFSourceFactory
{
public:	
	virtual SRCTYPE		GetSourceType()	= 0;

	virtual	eUMF_STATUS		Open(UINT SrcFactoryId, IBaseUMFStorage *pXmlStorage)	= 0;
	virtual	void			Close()		= 0;

	virtual IBaseUMFSource* CreateUMFSource() = 0;

	virtual	HWND			ShowConfigDlg(UINT SrcId, HWND hParentWnd, IBaseUMFStorage *pXmlStorage)	= 0;
	virtual	eUMF_STATUS		ApplyCurSetting(IBaseUMFStorage *pXmlStorage)	= 0;

	virtual void			DestroySelf()	= 0;
};

extern "C" IBaseUMFSourceFactory*	WINAPI CreateSourceFactory();
extern "C" SRCTYPE	WINAPI GetSourceType();

#endif	// __BASE_UMF_SORUCE_INTERFACE_H__
