#ifndef __BASE_UMF_FRAME_INTERFACE_H__
#define __BASE_UMF_FRAME_INTERFACE_H__

/************************************************************************
Comment
* 1. Container can has only one frame. (2010.04.12)
* 2. View can contain multiple sources.
* 3. View Windows pops up when Open() function is called.
*************************************************************************/
#include "IBaseUMFContainer.h"
#include "CommonInterface/IBaseMMProc.h"

//#define USING_MMPROC

typedef enum { 
	UMF_VT_NONE				= 0,
	UMF_VT_SIMPLEFRAME		= 1,
	UMF_VT_VAFRAME			= 2,
	UMF_VT_NVFRAME			= 3,
	UMF_VT_VCA5FRAME		= 4,
	UMF_VT_RTSPTESTFRAME	= 5,
	UMF_VT_VASFRAME			= 6,
	UMF_VT_VAFRAMEWORK		= 7,

} eFRAMETYPE;


typedef enum { 
	UMF_VF_POPUP		= 0x00000001,
	UMF_VF_CHILD		= 0x00000002,
} eFRAMEFLAG;


class IBaseUMFrame
{
public:

	virtual	HWND	Open(ULONG ViewFlag, IBaseUMFStorage *pViewMgrXmlStrg, IBaseUMFContainer *pContainer) = 0;
	virtual void	Close()			= 0;
	virtual void	DestroySelf() 	= 0;

	virtual LONG	SendUMFCommand(ULONG nEngId, wchar_t* szCmd, wchar_t* szSubCmd, wchar_t* szValue, void* pParam)	= 0;
	virtual BOOL	Show(BOOL bShow)= 0;
//<JL>
	//virtual LONG CreateMMProc(LPCTSTR szName, LPCTSTR szDLLFile){return -1;};
	//virtual LONG DestroyMMProc(LONG iMMProcID){return -1;};
	//virtual LONG ConnectMMProcs(LONG iSourceID, LONG iDestID){return -1;};
	//virtual LONG DisconnectMMProcs(LONG iSourceID, LONG iDestID){return -1;};
	//virtual LONG PositionMMProc(LONG iMMProcID, double dRelPosX, double dRelPosY, double dRelWidth, double dRelHeight){return -1;};
	//virtual IBaseMMProc* GetMMProc(LONG MMProcID){return NULL;};
//</JL>
};


extern "C" IBaseUMFrame* WINAPI	CreateUMFrame();
extern "C" eFRAMETYPE	WINAPI	GetFrameType();

#endif //__BASE_UMF_VIEW_INTERFACE_H__