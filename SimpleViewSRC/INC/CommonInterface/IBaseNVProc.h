#ifndef __INTERFACE_BASE_NV_PROCESS_H__
#define __INTERFACE_BASE_NV_PROCESS_H__

#include "IBaseUMFStorage.h"

typedef enum	//
{
	NV_PROC_NONE				=	0,
}eNV_PROC_TYPE;


class IBaseNVProc
{
public:	
	virtual eNV_PROC_TYPE	GetType()	= 0;
	
	virtual BOOL		Open(UINT ProcId, IBaseUMFStorage *pStorage) = 0;
	virtual void		Close()		= 0;
	virtual void		DestroySelf() 	= 0;
	
	virtual UINT		GetAlgoId()	= 0;
	virtual PXML		GetAlgoInfo()	= 0;
	
	virtual DWORD		GetMaxResultBufSize()	= 0;
	virtual BOOL		ProcessMMData(BITMAPINFOHEADER &bih, FILETIME timestamp, UINT *uiLengthResult, BYTE *pResult) = 0;
};


class IBaseNVProcFactory
{
public:	
	virtual eNV_PROC_TYPE		GetType()	= 0;

	virtual	BOOL			Open(UINT FactoryId, IBaseUMFStorage *pStorage)	= 0;
	virtual	void			Close()		= 0;

	virtual IBaseNVProc*	CreateNVProc() = 0;
	virtual UINT			GetFacId()	= 0;

	virtual	HWND			ShowConfigDlg(UINT SrcId, HWND hParentWnd, IBaseUMFStorage *pStorage)	= 0;
	virtual	BOOL			ApplyCurSetting(IBaseUMFStorage *pStorage)	= 0;

	virtual void			DestroySelf()	= 0;
};

extern "C" IBaseNVProcFactory*	WINAPI	CreateNVProcFactory();
extern "C" eNV_PROC_TYPE	WINAPI	GetNVProcType();


#endif