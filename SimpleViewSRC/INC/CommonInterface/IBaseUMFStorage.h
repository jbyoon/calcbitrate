#ifndef __BASE_UMF_STORAGE_INTERFACE_H__
#define __BASE_UMF_STORAGE_INTERFACE_H__

#include "UMFBase.h"

typedef enum //
{
	UMF_STORAGE_BASE		= 0,
	UMF_STORAGE_FILE		= 1,
	UMF_STORAGE_VCA5		= 2,
	UMF_STORAGE_WATERLEVEL	= 3,
	UMF_STORAGE_SNOWDETECT	= 4,
}eUMF_STORAGE_TYPE;


#ifndef __PXML__
typedef void*	PXML;
#define __PXML__
#endif


class IBaseUMFStorage
{
public:
	virtual PXML 				GetXML()	= 0;
	virtual eUMF_STORAGE_TYPE 	GetType()	= 0;
};


class IFileUMFStorage : public IBaseUMFStorage
{
public:
	virtual HRESULT		Load(TCHAR * szFilename)	= 0;
	virtual HRESULT		Save(TCHAR * szFilename)	= 0;
};


//Base UMF Stroage for XML storage
class CBaseUMFStorage : public IBaseUMFStorage
{
public:
	CBaseUMFStorage(){m_XML = NULL;}
	CBaseUMFStorage(PXML pXML){SetXML(pXML);}
	
	eUMF_STORAGE_TYPE 	GetType(){return UMF_STORAGE_BASE;}
	
	PXML GetXML(){return m_XML;}
	void SetXML(PXML pXML){m_XML = pXML;}

private:
	PXML m_XML;
};

#endif	// __BASE_UMF_STORAGE_INTERFACE_H__
