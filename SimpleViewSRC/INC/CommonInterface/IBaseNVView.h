#ifndef __VA_NV_VIEW_INTERFACE_H__
#define __VA_NV_VIEW_INTERFACE_H__

#include "IBaseUMFSource.h"
//#include "CommonInterface/IBaseMMProc.h"

class	IBaseNVFrame
{
public:	
	//Communicate with container
	virtual	void	FireEvent(ULONG nEngId, wchar_t* szCmd, wchar_t* szSubCmd, wchar_t* szValue) = 0;
	
	//Send CGI Command 
	virtual	BOOL	RequestCGICommand(ULONG EngId, char* szCmd, BYTE* pResponse, DWORD* pLength) = 0;
	virtual	BOOL	RequestCGICommandFast(ULONG EngId, char* szCmd, BYTE* pResponse, DWORD* pLength) = 0;
	
	virtual PXML	GetSourceInfo(ULONG EngId) = 0;
	virtual PXML	GetFrameInfo()	= 0;
};


class IBaseNVView
{
public:
	typedef enum{
		BASE_MODULE = 0, 
		VIEW_MODULE = 1
	} NVVIEW_TYPE;

	typedef enum{
		INIT_STATUS = 0, 
		RUN_STATUS 	= 1
	} NVVIEW_STATUS;

	typedef enum{
		POPUP		= 0x00000001,
		CHILD		= 0x00000002,
	}DISPLAY_STYLE;
	
	virtual char* GetModuleName()	= 0;
	virtual ULONG GetType()			= 0;
	
	virtual BOOL Create(HWND hWndParent, IBaseNVFrame *pNVFramework) = 0;
	virtual void Destroy() = 0;

	//Container Send command to view 
	virtual LONG OnNVViewCommand(ULONG nEngId, wchar_t* szCmd, wchar_t* szSubCmd, wchar_t* szValue, void* pParam) = 0 ;
	
	virtual BOOL Start(BOOL bStart) 	= 0;
	virtual NVVIEW_STATUS GetStatus()	= 0;
	
	virtual DWORD GetSupportSrcTypes()	= 0;
	
	virtual BOOL Show(ULONG EngId, BOOL bShow, DISPLAY_STYLE Style) = 0;

	virtual BOOL ChangeMMSourceInfo(DWORD EngId, PXML pSoruceInfoXML) = 0;
	virtual void ProcessMMData(ULONG EngId, SRCTYPE Type, BYTE *pMMData, UINT MMDataLen, FILETIME *pTimestamp) = 0;

//<JL>
	//virtual LONG CreateMMProc(LPCTSTR szName, LPCTSTR szDLLFile){return -1;};
	//virtual LONG DestroyMMProc(LONG iMMProcID){return -1;};
	//virtual LONG ConnectMMProcs(LONG iSourceID, LONG iDestID){return -1;};
	//virtual LONG DisconnectMMProcs(LONG iSourceID, LONG iDestID){return -1;};
	//virtual LONG PositionMMProc(LONG iMMProcID, double dRelPosX, double dRelPosY, double dRelWidth, double dRelHeight){return -1;};
	//virtual IBaseMMProc* GetMMProc(LONG MMProcID){return NULL;};
//</JL>
};

extern "C" IBaseNVView*  WINAPI CreateNVViewModule();

#endif