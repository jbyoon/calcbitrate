#ifndef __INTERFACE_BASE_VA_ALGORISM_H__
#define __INTERFACE_BASE_VA_ALGORISM_H__

#include "IBaseUMFStorage.h"

typedef enum	//
{
	VA_ALGO_NONE				=	0,
	VA_ALGO_DUMMY				=	1,
	VA_ALGO_VCA5				=	2,
	VA_ALGO_WATERLEVEL			=	3,
	VA_ALGO_SNOWDETECT			=	4,
	VA_ALGO_WATERLEVEL2			=	5,
}eVA_ALGO_TYPE;


class IBaseVAAlgo
{
public:	
	virtual eVA_ALGO_TYPE	GetType()	= 0;
	
	virtual BOOL		Open(UINT AlgoId, IBaseUMFStorage *pStorage) = 0;
	virtual void		Close()		= 0;
	virtual void		DestroySelf() 	= 0;
	
	virtual UINT		GetAlgoId()	= 0;
	virtual PXML		GetAlgoInfo()	= 0;
	
	virtual DWORD		GetMaxResultBufSize()	= 0;
	virtual	BOOL		ApplyConfig(IBaseUMFStorage *pStorage)	= 0;
	virtual BOOL		ProcessFrame(BITMAPINFOHEADER &bih, BYTE* pBits, FILETIME timestamp, UINT *uiLengthResult, BYTE *pResult) = 0;
};


class IBaseVAAlgoFactory
{
public:	
	virtual eVA_ALGO_TYPE		GetType()	= 0;

	virtual	BOOL			Open(UINT SrcFactoryId, IBaseUMFStorage *pStorage)	= 0;
	virtual	void			Close()		= 0;

	virtual IBaseVAAlgo*	CreateVAAlgo() = 0;
	virtual UINT			GetFacId()	= 0;

	virtual	HWND			ShowConfigDlg(UINT SrcId, HWND hParentWnd, IBaseUMFStorage *pStorage)	= 0;
	virtual	BOOL			ApplyCurSetting(IBaseUMFStorage *pStorage)	= 0;

	virtual void			DestroySelf()	= 0;
};

extern "C" IBaseVAAlgoFactory*		CreateVAAlgoFactory();
extern "C" eVA_ALGO_TYPE			GetVAAlgoType();


#endif