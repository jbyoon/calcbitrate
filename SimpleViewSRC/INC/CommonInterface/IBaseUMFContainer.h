#ifndef __BASE_UMF_CONTAINER_INTERFACE_H__
#define __BASE_UMF_CONTAINER_INTERFACE_H__

#include "IBaseUMFSourceMgr.h"
#include "CommonInterface/IBaseMMProc.h"

typedef enum { 
	UMF_CT_NONE				= 0,
	UMF_CT_NVCONTAINER		= 1,
} eCONTAINERTYPE;

class	IBaseUMFrame;

typedef enum { 
	UMFEVENT
} eUMFMESSAGE;
struct sEventParams {
	ULONG EngId;
	std::wstring szEvent;
	std::wstring szSubEvent;
	std::wstring szValue;
};

class	IBaseUMFApp
{
public:
	virtual	HWND				GetAppHWnd() = 0;
	virtual	void				OnUMFEvent(ULONG EngId, const wchar_t* szEvent, const wchar_t* szSubEvent, const wchar_t* szValue) = 0;
};

class	IBaseUMFContainer
{
public:
	virtual	BOOL				Open(IBaseUMFApp* pUMFApp) = 0;
	virtual	void				Close() = 0;
	virtual void				DestorySelf() = 0;

	virtual	IBaseUMFSourceMgr*	GetSourceMgr() = 0;
	virtual	IBaseUMFrame*		GetCurUMFrame() = 0;

	virtual LONG				SendUMFCommand(ULONG nEngId, wchar_t* szCmd, wchar_t* szSubCmd, wchar_t* szValue, void* pParam) = 0;
	virtual	void				OnUMFEvent(ULONG nEngId, wchar_t* szEvent, wchar_t* szSubEvent, wchar_t* szValue) = 0;
	virtual	void				OnInternalUMFEvent() = 0;
	
	virtual	HWND				GetContainerHWnd() = 0;

	virtual	BOOL				Show(BOOL bShow) = 0;
//<JL>
	//virtual LONG CreateMMProc(LPCTSTR szName, LPCTSTR szDLLFile) {return -1;};
	//virtual LONG DestroyMMProc(LONG iMMProcID) {return -1;};
	//virtual LONG ConnectMMProcs(LONG iSourceID, LONG iDestID) {return -1;};
	//virtual LONG DisconnectMMProcs(LONG iSourceID, LONG iDestID) {return -1;};
	//virtual LONG PositionMMProc(LONG iMMProcID, double dRelPosX, double dRelPosY, double dRelWidth, double dRelHeight) {return -1;};
	//virtual IBaseMMProc* GetMMProc(LONG iMMProcID) {return NULL;};
//</JL>
};

extern "C" IBaseUMFContainer*	WINAPI CreateUMFContainer();
extern "C" eCONTAINERTYPE		WINAPI GetContainerType();

#endif	// __BASE_UMF_CONTAINER_INTERFACE_H__
