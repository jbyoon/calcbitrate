#ifndef __BASE_CONF_MODULE_INTERFACE_H__
#define __BASE_CONF_MODULE_INTERFACE_H__

#include "IBaseUMFStorage.h"

#define BASE_UMF_CONFMODULE_EVENT	0
#define VCA_CONFMODULE_EVENT		0x1000

#define	BASE_UMF_CONFMODULE_PARAM	0
#define	VCA_CONFSET_PARAM			0x1000

//COMMON CONF MODULE EVENT
#define	UMF_CM_SHOW_DLG_EVENT		(BASE_UMF_CONFMODULE_EVENT+1)
#define	UMF_CM_APPLY_DLG_EVENT		(BASE_UMF_CONFMODULE_EVENT+2)

//First Param1(in) : Engine , Param2(Out) : RenderFlags
#define	CM_VCACONF_RENDERINGFLAGS	(VCA_CONFSET_PARAM+1)

class IBaseVAConfSet
{
public:
	typedef enum {
		BASE_CONFSET	= 0, 
		VCA_CONFSET		= 1
	}CONFSET_Type;
		
	virtual CONFSET_Type 		GetType()				= 0;
	virtual IBaseUMFStorage*	GetStorage(ULONG EngId) = 0;

	virtual BOOL				GetConfSetParam(ULONG EngId, ULONG Cmd, ULONG* pParam1, ULONG* pParam2) = 0;
	virtual void				OnConfModuleEvent(ULONG EngId, ULONG EventType, ULONG* Param1, ULONG* Param2) = 0;
};	


class IBaseVAConfModule
{
public:
	typedef enum{
		BASE_MODULE		= 0, 
		VIEW_MODULE		= 1
	}CONFMODULE_TYPE;

	typedef enum{
		POPUP			= 0,
		CHILD			= 1,
	}DISPLAY_STYLE;


	virtual char*	GetModuleName()		= 0;
	virtual ULONG	GetType()			= 0;
	virtual BOOL	IsSupportStorage(eUMF_STORAGE_TYPE type)	= 0;

	virtual HWND Create(HWND hWndParent, IBaseVAConfSet *pVAConfSet) = 0;
	virtual void Destroy() = 0;
	virtual BOOL Show(BOOL bShow, ULONG EngId, DISPLAY_STYLE Style) = 0;
};

class IMetadataParser;
class IViewVAConfModule : public IBaseVAConfModule
{
public:
	virtual void ProcessImageNVAMetaData(ULONG EngId, BYTE *pImage, BITMAPINFOHEADER *pbm, 
					BYTE *pMetaData, int iMateDataLen, IMetadataParser *pParser) = 0;
};


#endif