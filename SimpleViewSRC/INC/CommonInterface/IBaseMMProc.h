/**
 * \file
 *
 * \brief This file describes and implements the MMProc interface and associated auxiliary functionality.
 *
 */

#pragma once

#include "IBaseUMFSource.h"
#include <vector>
#include <algorithm>
#include <assert.h>

/**
 * \class IMMProcContainer
 *
 * \brief This interface describes the minimum functionality required by a class that intends to contain and manage MMProc objects.
 *
 * \details At the present moment, the container class serves as intermediary between the MMProc objects and the activeX control
 * facilitating the firing of events to the external application. 
 */
class IMMProcContainer
{
public:
	/**
	 * \brief Sends an event to higher hierarchy abstraction levels with the objective of triggering an event in the external application.
	 *
	 * \param[in] szCmd The command description string.
	 * \param[in] szSubCmd The sub-command description string.
	 * \param[in] szValue A string containing command specific data.
	 *
	 * \details Typically, an MMProc object will call this method when it needs to send information to the external application 
	 * (e.g. website).
	 * \details The IMMProcContainer is responsible for relaying this message to a higher hierarchy level (e.g. NVFrame).
	 * \details The command and sub-command descriptors are meant to be interpreted by the external application before processing the szValue data.
	 */
	virtual	void FireEvent(wchar_t* szCmd, wchar_t* szSubCmd, wchar_t* szValue) = 0;
};

/**
 * \class IMMProc
 * \brief This interface describes the functionality required by a MMProc class.
 *
 * \details A MMProc is a MultiMedia Processor object.
 * \details The main features are (Note that all are implementation dependent):
 *		- The ability to connect and disconnect to other MMProc objects. This allows communication between MMProc objects;
 *		- The ability to process some type of MultiMedia processing of binary data;
 *		- The ability of translation and scaling within a parent window (for visible MMProc objects);
 *		- The ability to receive and send information to the external application.
 */
class IMMProc
{
public:

	/**
	 * \brief Provides the MMProc with information regarding parent window and parent container.
	 *
	 * \param[in] hWndParent The handle of the parent window.
	 * \param[in] pContainer The reference of the MMProc container.
	 */
	virtual BOOL Create(HWND hWndParent, IMMProcContainer& pContainer) = 0;
	/**
	 * \brief Allows the MMProc object to clean up resourses and delete itself.
	 *
	 * \details Typically, each MMProc is implemented as an individual dll and this method is required to permit the IMMProcContainer to destroy the MMProc object.
	 */
	virtual void deleteSelf() = 0;
	/**
	 * \brief Connects this MMproc object to another MMProc object.
	 *
	 * \param[in] pConnectingMMProc The MMProc object to connect to.
	 * \param[in] szConnDescription A description of the type of connection to establish.
	 * \return TRUE on a successful connection, FALSE otherwise.
	 *
	 * \details A connection between MMproc objects has underlying properties which depend on the resources that are shared and communication messages.
	 * \details The szConnDescription parameter controls the decision of which type of connection to attempt.
	 *
	 * \details Typically, this method should call #MMPROC_CONNECT macro.
	 */
	virtual BOOL Connect(IMMProc* pConnectingMMProc, LPCTSTR szConnDescription) = 0;
	/**
	 * \brief Disconnects this MMProc object from another MMProc object.
	 *
	 * \param[in] pConnectingMMProc The MMProc object to disconnect from.
	 * \param[in] szConnDescription A description of the type of the existing connection.
	 * \return TRUE on a successful disconnection, FALSE otherwise.
	 *
	 * \details Typically, this method should call #MMPROC_DISCONNECT macro.
	 */
	virtual BOOL Disconnect(IMMProc* pConnectingMMProc, LPCTSTR szConnDescription) = 0;
	/**
	 * \brief Process MultiMedia binary data.
	 *
	 * \param[in] EngId MultiMedia source engine ID.
	 * \param[in] SourceInfo MultiMedia source information.
	 * \param[in] Type MultiMedia data type.
	 * \param[in] pMMData Binary data buffer.
	 * \param[in] MMDataLen Data buffer length.
	 * \param[in] pTimestamp Time associated with the data frame.
	 * \return TRUE on successful processing of the data or when the packet is ignored. Returns FALSE when there is the processing fails for any reason 
	 * (usually due to data corruption or wrong data format).
	 *
	 * \details Provided with a packet of binary data, and depending on its Type and SourceInfo, the MMProc can ignore or decide how to process said data.
	 * \details NOTE: See UMF Framework references and documentation for a better understanding of the contents and intended fucntionality of the input parameters.
	 */
	virtual BOOL ProcessMultimedia(ULONG EngId, PXML SourceInfo, SRCTYPE Type, BYTE *pMMData, UINT MMDataLen, FILETIME *pTimestamp) = 0;
	/**
	 * \brief Translate and resize a windowed MMProc.
	 *
	 * \param[in] dRelPosX Relative left horizontal coordinate with respect to the parent window.
	 * \param[in] dRelPosY Relative top vertical coordinate with respect to the parent window.
	 * \param[in] dRelWidth Relative width with respect to the parent window.
	 * \param[in] dRelHeight Relative height with respect to the parent window.
	 * \return TRUE on successful positioning or if the MMProc is not viewable, FALSE otherwise.
	 *
	 * \details The range of all parameters is [0-1].
	 */
	virtual BOOL Position(double dRelPosX, double dRelPosY, double dRelWidth, double dRelHeight) = 0;
	/**
	 * \brief Pass a data string to the MMProc.
	 *
	 * \param[in] szData The data string.
	 * \return TRUE on successful data parsing or if the data is ignored, FALSE otherwise.
	 *
	 * \details The format of the string provided depends on specific MMProc requirements. 
	 */
	virtual BOOL ProvideData(LPCTSTR szData) = 0;
	/**
	 * \brief Request data from the MMProc.
	 *
	 * \return TRUE on successful request, FALSE otherwise.
	 *
	 * \details The returned data contents depend on the specific MMProc. 
	 * The data is sent via the IMMProcContainer FireEvent function call. 
	 */
	virtual BOOL RequestData() = 0;
};

/**
 * \struct MMProc_Relative_Position
 * \brief This structure describes the relative position and and size of a viewable MMProc in respect to its parent window.
 */
struct MMProc_Relative_Position
{
	double x;///< Relative left horizontal coordinate with respect to the parent window.
	double y;///< Relative top vertical coordinate with respect to the parent window.
	double w;///< Relative width with respect to the parent window.
	double h;///< Relative height with respect to the parent window.
	MMProc_Relative_Position() : x(0), y(0), w(0), h(0) {}///< Default constructor. All members set to 0.
};

/**
 * \struct MMProc_Message
 * \brief This structure describes a message sent between connected MMProc objects.
 */
struct MMProc_Message
{
	int sourceStamp; ///< A unique identifier related to the source of the message. This is generated automatically when the message is sent.
	int type; ///< The message type which serves as payload as well.
	MMProc_Message(int _type) : sourceStamp(0), type(_type) {}  ///< Default constructor. The message type is required.
};

template <class T> class MMProcConnector;

/**
 * \class IMMProcHasConnector
 * \brief This class describes a connectivity point to a MMProcConnector.
 *
 * \details A class that contains a MMProcConnector<T> instance should implement this interface to allow a point of contact to other MMProc objects with the same MMProcConnector.
 */
template <class T> class IMMProcHasConnector
{
public:
	/**
	 * \brief Pass a data string to the MMProc.
	 *
	 * \param[in] pConn A pointer to the requesting MMProcConnector object.
	 * \return A pointer to the MMProcConnector object requested.
	 *
	 * \details It is the responsibility of the classes that implement this interface to return the right pointer.
	 * \details The input parameter is in place to ensure function overloading in classes with multiple connectors.
	 */
	virtual MMProcConnector<T>* GetConnector(MMProcConnector<T>* pConn) = 0;
	/**
	 * \brief Send a message to the MMProc via this connector.
	 *
	 * \param[in] msg The message being sent.
	 * \param[in] pConnector A pointer to the message source MMProcConnector.
	 * \return TRUE if message received and acted upon, FALSE otherwise.
	 *
	 * \details Possible message types depend on the specific connector implementation.
	 */
	virtual BOOL Notify(MMProc_Message msg, MMProcConnector<T>* pConnector) = 0;
};

/**
 * \class MMProcConnector
 * \brief This class represents a connection manager responsible for establishing and maintaining a connection to other MMProcs with similar connectors.
 *
 * \details Type T establishes the nature of the connection and is the type of the data shared between MMProcs.
 * \details The MMProcConnector contains an item of type T which can be used accross the connection path.
 */
template <class T> class MMProcConnector
{
protected:
	IMMProcHasConnector<T>* m_pMMProcHolder; ///< The MMProc object that contains this connector.
	BOOL m_bIsSource; ///< Whether the this connector's MMProc holder is the source of the data item.
	MMProcConnector<T>* m_pProvider; ///< The MMProc that provides access to the data item (NOTE: If m_pMMProcHolder is the source of the data item, this is the same as m_pMMProcHolder, otherwise they are different).
	std::vector<MMProcConnector<T>*> m_lClients; ///< A collection of client connectors.
	T* m_pItem; ///< The data item shared by the connection.

public:
	static const unsigned int MAX_CONNECTORS = 100; ///< The maximum number of connections allowed

	/**
	 * \brief Constructor.
	 *
	 * \details reserves memory for the maximum number of client connections.
	 */
	MMProcConnector()
	{
		m_pProvider = NULL;
		m_pItem = NULL;
		m_lClients.reserve(MAX_CONNECTORS); //preallocate to prevent cross-DLL allocation and deallocation... TODO: fix this properly
	}

	/**
	 * \brief Initialise the connector.
	 *
	 * \param[in] _pMMProcHolder The MMProc object that contains this connector.
	 * \param[in] _item A pointer to the data/object to be shared within the connection (Can be NULL).
	 *
	 * \details The MMProc holder provides the means to access this connector object.
	 * \details In the case when _item is NULL, the intention is that the MMProc holder will connect to another MMProc which provides the _item to be used.
	 */
	void Setup(IMMProcHasConnector<T>* _pMMProcHolder, T* _item)
	{
		m_pMMProcHolder = _pMMProcHolder;
		m_pItem = _item;
		m_bIsSource = _item != NULL;
		if(m_bIsSource) m_pProvider = this;
	}

	/**
	 * \brief Destructor.
	 *
	 * \details disconnects from the provider connection and from the client connections.
	 */
	~MMProcConnector()
	{
		if(m_pProvider != NULL)
		{
			Disconnect(m_pProvider);
		}
		std::vector<MMProcConnector<T>*> clientsCopy = m_lClients;
		std::vector<MMProcConnector<T>*>::iterator it;
		for(it = clientsCopy.begin(); it != clientsCopy.end(); it++)
		{
			Disconnect(*it);
		}
	}

	/**
	 * \brief Access the data item.
	 *
	 * \return A pointer to the data item.
	 */
	T* GetItem() {return m_pItem;}

	/**
	 * \brief Is this connector a provider of the data?.
	 *
	 * \return TRUE if m_pItem is not NULL, false otherwise.
	 */
	BOOL IsProvider() {return !(m_pItem == NULL);}

	/**
	 * \brief Is this connector's MMProc holder the source of the data?.
	 *
	 * \return m_bIsSource.
	 */
	BOOL IsSource() {return m_bIsSource;}

	/**
	 * \brief Connect to another MMProc Connector.
	 *
	 * \param[in] pConnector The MMProc Connector to establish a connection with.
	 *
	 * \details The connection process requires the registration of pConnector with this and the registration of this with pConnector.
	 * \details See Register.
	 */
	BOOL Connect(MMProcConnector<T>* pConnector)
	{
		if(!pConnector->Register(this)) return FALSE;
		if(!this->Register(pConnector))
		{
			pConnector->Unregister(this);
			return false;
		}
		return TRUE;
	}

	/**
	 * \brief Register a MMProc Connector with this connector.
	 *
	 * \param[in] pConnector The MMProc Connector to register.
	 *
	 * \details The registration process establishes the connection hierarchy between 2 connectors.
	 * \details The valid combinations are as follows:
	 * - pConnector is the source of the data item and this connector is not the source:
	 *		- Establish pConnector as the provider for this connection;
	 *		- This connector gains access to the data item.
	 * - This connector is the source:
	 *		- Add pConnector as a client.
	 * - Neither are source and pConnector is a provider of the data item and this connector is not a provider.
	 *		- Establish pConnector as the provider for this connection;
	 *		- This connector gains access to the data item.
	 * - Neither are source and this connector is a provider of the data:
	 *		- Add pConnector as a client.
	 * \details All other combinations cause the registration process to fail.
	 * \details The following image illustrates a typical connection hierarchy between 3 MMProcs.
	 * \image html MMProcConnectorHierarchy.png
	 */
	BOOL Register(MMProcConnector<T>* pConnector)
	{
		if(pConnector == NULL) return FALSE; //pConnection does not implement the right interface

		if(pConnector->IsSource() && this->IsSource()) return FALSE; //They can't both be sources

		if(pConnector->IsSource() && !this->IsSource()) //If the connection is source then get its item and save a pointer to it
		{
			this->m_pProvider = pConnector;
			m_pItem = pConnector->GetItem();
			return TRUE;
		}
		if(!pConnector->IsSource() && this->IsSource()) //If this is the source, then the connection is a client
		{
			this->AddClient(pConnector);
			return TRUE;
		}
		else //Neither are source
		{
			if(pConnector->IsProvider() && this->IsProvider()) return FALSE; //They can't both be providers

			if(pConnector->IsProvider() && !this->IsProvider()) //If the connection is provider then get its item and save a pointer to it
			{
				this->m_pProvider = pConnector;
				m_pItem = pConnector->GetItem();
				return TRUE;
			}
			if(!pConnector->IsProvider() && this->IsProvider()) //If this is the provider, then the connection is a client
			{
				this->AddClient(pConnector);
				return TRUE;
			}
			else //Neither are providers
			{
				return FALSE; //We don't know which one is the client.
			}
		}

	};

	/**
	 * \brief Add a MMProc Connector to the list of clients of this connector.
	 *
	 * \param[in] newClient The client to add.
	 */
	BOOL AddClient(MMProcConnector<T>* newClient)
	{
		BOOL clientPresent = FALSE;
		if(!m_lClients.empty())
		{
			std::vector<MMProcConnector<T>*>::iterator foundObject;
			foundObject = std::find(m_lClients.begin(), m_lClients.end(), newClient);
			if(foundObject != m_lClients.end()) 
			{
				clientPresent = TRUE;
			}
		}
		if(!clientPresent)
		{
			if(m_lClients.size() < MAX_CONNECTORS)
			{
				m_lClients.push_back(newClient);
				return TRUE;
			}
			else
			{
				assert(false); // Do you really need that many connectors?  If so then either increase the maximum or fix the cross-DLL allocation/deallocation problem
				return FALSE;
			}
		}
		return FALSE;
	};

	/**
	 * \brief Disconnect from another MMProc Connector.
	 *
	 * \param[in] pConnector The MMProc Connector to disconnect from.
	 *
	 * \details The connection process requires the unregistration of pConnector with this and the unregistration of this with pConnector.
	 * \details See Unregister.
	 */
	BOOL Disconnect(MMProcConnector<T>* pConnector)
	{
		if(!pConnector->Unregister(this)) return FALSE;
		if(!this->Unregister(pConnector))
		{
			return FALSE;
		}
		return TRUE;
	}

	/**
	 * \brief Unregister a MMProc Connector from this connector.
	 *
	 * \param[in] pConnector The MMProc Connector to unregister.
	 *
	 * \details The unregistration process removes the connection between 2 connectors.
	 * \details If pConnector is the provider to this connector, remove links to it and the data.
	 * \details If pConnector a client of this connector, remove remove it from the client list.
	 */
	BOOL Unregister(MMProcConnector<T>* pConnector)
	{
		if(this->m_pProvider == pConnector) // if the connection is the provider, remove pointers to it and the item
		{
			this->m_pProvider = NULL;
			m_pItem = NULL;
			//TODO: disconnect to clients as well?
			return TRUE;
		}

		return RemoveClient(pConnector);// if the connection is a client, just remove it.
	}

	/**
	 * \brief Remove a MMProc Connector client from the client list.
	 *
	 * \param[in] oldClient The MMProc Connector client to remove.
	 */
	BOOL RemoveClient(MMProcConnector<T>* oldClient)
	{
		BOOL clientPresent = FALSE;
		if(!m_lClients.empty())
		{
			std::vector<MMProcConnector<T>*>::iterator foundObject;
			foundObject = std::find(m_lClients.begin(), m_lClients.end(), oldClient);
			if(foundObject != m_lClients.end())
			{
				m_lClients.erase(foundObject);
				return TRUE;
			}
		}
		return FALSE;
	};

	/**
	 * \brief Send a message to the MMProc Connector that serves as the provider of this connection.
	 *
	 * \param[in] msg The message to send.
	 *
	 * \details The message details are MMProc type dependent.
	 * \details To understand the concept of provider look at MMProcConnector::Register.
	 */
	BOOL NotifyProvider(MMProc_Message msg)
	{
		if(m_pProvider)
		{
			if(msg.sourceStamp == 0) msg.sourceStamp = (int)this;
			return m_pProvider->NotifyHolder(msg);
		}
		return FALSE;
	}

	/**
	 * \brief Send a message to all client MMProc Connectors.
	 *
	 * \param[in] msg The message to send.
	 *
	 * \details The message details are MMProc type dependent.
	 * \details To understand the concept of client look at MMProcConnector::Register.
	 */
	BOOL NotifyClients(MMProc_Message msg)
	{
		if(msg.sourceStamp == 0) msg.sourceStamp = (int)this;
		BOOL success = TRUE;
		std::vector<MMProcConnector<T>*>::iterator itClient;
		for(itClient = m_lClients.begin(); itClient != m_lClients.end(); itClient++)
		{
			if(!(*itClient)->NotifyHolder(msg)) success = FALSE;
		}
		return success;
	}

	/**
	 * \brief Send a message to the MMProc Connector that contains this connector.
	 *
	 * \param[in] msg The message to send.
	 *
	 * \details The message details are MMProc type dependent.
	 * \details NotifyHolder is used by MMProcConnector::NotifyProvider and MMProcConnector::NotifyClients as the final stage of the message passing mechanism.
	 */
	BOOL NotifyHolder(MMProc_Message msg)
	{
		if(m_pMMProcHolder && (int)this != msg.sourceStamp)
		{
			return m_pMMProcHolder->Notify(msg, this);
		}
		return FALSE;
	}
};

//JL: Connector macros

/** 
 * \def MMPROC_CONNECT( SUCCESS, PCONNECTION_MMPROC, MMPROC_ITEM )
 * \brief A macro that aids with establishing a connection between two MMProc objects.
 *  
 * \param[out] SUCCESS A boolean outcome of the connection process (TRUE if successful and FALSE if not).
 * \param[in] PCONNECTION_MMPROC An IMMProc pointer to the object to connect to. 
 * \param[in] MMPROC_ITEM The type of the data item to be shared within the connection.
 *
 * \details This macro should be called from within the IMMProc::Connect method.
 * \details The macro first establishes compatibility between the MMProc objects to be connected.
 * \details If both MMProc objects are compatible, the macro retrieves the corresponding and type dependent connector objects.
 * \details Finally, the macro calls MMProcConnector::Connect using both connectors.
*/
#define MMPROC_CONNECT( SUCCESS, PCONNECTION_MMPROC, MMPROC_ITEM ) \
	IMMProcHasConnector< MMPROC_ITEM >* pIHasItemConnThis = \
		dynamic_cast<IMMProcHasConnector< MMPROC_ITEM >*> ( this ); \
	IMMProcHasConnector< MMPROC_ITEM >* pIHasItemConnOther = \
		dynamic_cast<IMMProcHasConnector< MMPROC_ITEM >*> ( PCONNECTION_MMPROC ); \
	if(pIHasItemConnThis != NULL && pIHasItemConnOther != NULL) \
	{ \
		MMProcConnector<MMPROC_ITEM>* pIItemConnThis = NULL; \
		pIItemConnThis = pIHasItemConnThis->GetConnector(pIItemConnThis); \
		MMProcConnector<MMPROC_ITEM>* pIItemConnOther = NULL; \
		pIItemConnOther = pIHasItemConnOther->GetConnector(pIItemConnOther); \
		if(pIItemConnThis != NULL && pIItemConnOther != NULL) SUCCESS = pIItemConnThis->Connect(pIItemConnOther); \
		else SUCCESS = FALSE; \
	} \
	else SUCCESS = FALSE; 

/** 
 * \def MMPROC_DISCONNECT( SUCCESS, PCONNECTION_MMPROC, MMPROC_ITEM )
 * \brief A macro that aids with disconnecting two MMProc objects.
 *  
 * \param[out] SUCCESS A boolean outcome of the connection process (TRUE if successful and FALSE if not).
 * \param[in] PCONNECTION_MMPROC An IMMProc pointer to the object to disconnect from. 
 * \param[in] MMPROC_ITEM The type of the data item that is shared within the connection.
 *
 * \details This macro should be called from within the IMMProc::Disconnect method.
 * \details The macro first establishes compatibility between the MMProc objects to be disconnected.
 * \details If both MMProc objects are compatible, the macro retrieves the corresponding and type dependent connector objects.
 * \details Finally, the macro calls MMProcConnector::Disconnect using both connectors.
*/
#define MMPROC_DISCONNECT( SUCCESS, PCONNECTION_MMPROC, MMPROC_ITEM ) \
	IMMProcHasConnector< MMPROC_ITEM >* pIHasItemConnThis = \
		dynamic_cast<IMMProcHasConnector< MMPROC_ITEM >*> ( this ); \
	IMMProcHasConnector< MMPROC_ITEM >* pIHasItemConnOther = \
		dynamic_cast<IMMProcHasConnector< MMPROC_ITEM >*> ( PCONNECTION_MMPROC ); \
	if(pIHasItemConnThis != NULL && pIHasItemConnOther != NULL) \
	{ \
		MMProcConnector<MMPROC_ITEM>* pIItemConnThis = NULL; \
		pIItemConnThis = pIHasItemConnThis->GetConnector(pIItemConnThis); \
		MMProcConnector<MMPROC_ITEM>* pIItemConnOther = NULL; \
		pIItemConnOther = pIHasItemConnOther->GetConnector(pIItemConnOther); \
		if(pIItemConnThis != NULL && pIItemConnOther != NULL) SUCCESS = pIItemConnThis->Disconnect(pIItemConnOther); \
		else SUCCESS = FALSE; \
	} \
	else SUCCESS = FALSE; 

/** 
 * \mainpage MMProc Documentation
 *
 * \section intro_sec Introduction
 * This document introduces the concept of MMProc and describes its implementation details based on the content of IBaseMMProc.h.
 *
 * MMProcs are \"Multimedia Processor\" objects and are part of the UMF Framework. 
 *
 * Each MMProc is implemented in its own dll and provides a specific functionality that contributes to the overall application behaviour. 
 *
 * By creating and connecting MMProcs, it should be possible to build a variety of user interfaces with minimal code re-write.
 *
 *
 * \section objective_sec Objectives
 * The MMProc concept is an evolution of the UMF architecture. In the UMF (see AxUMF-RFD_V1.0.docx), a container object (e.g. NVContainer) is responsible for managing the loading and connections between Source elements (e.g. RTSP source) and Sink elements (e.g. NVViewModuleMgr).
 *
 * The overall objective of the MMProc architecture is to streamline the management of the Sinks and Sources by the introduction of the concept of MultiMedia Processor (MMProc). When using this architecture a container manages MMProc objects instead of managing Viewers.
 *
 * Also, the usage of MMProcs enables generalising the creation and use of entities that interact with data generated from UDP's camera systems and facilitates the implementation of platform independent GUI applications.
 *
 *
 * \section details_sec Details
 * The IMMProc class is the Interface to an MMProc. MMProcs exist within a container (IMMProcContainer) and each module does a different task (e.g. metadata decode, video preview, video rendering, ...). MMProcs can be connected together to form a kind of filter chain. The modules in the container and how they are connected is completelly driven by the user of the container (the webpage in the activeX case).
 *
 * The most relevant properties of an MMProc are: (1) the ability to connect to other MMProcs; and (2) a method or thread that processes MultiMedia data. These are realised in the pure abstract methods IMMProc::Connect / IMMProc::Disconnect and IMMProc::ProcessMultimedia. IMMProc includes other auxilary methods for management and interaction.
 *
 * The following graph represents the classes defined in the MMProc architecture. Note that only MMProcConnector is not an abstract class. T is the type of the data that will be shared withing a MMProc connection.
 *
 * \dot
 * graph class_diag 
 * {
 *		node [shape=record, fontname=Helvetica, fontsize=10];
 *		
 *		immproccont 
 *		[
 *			label = "{IMMProcContainer||FireEvent()\l}";
 *			URL = "\ref IMMProcContainer";
 *		];
 *
 *		immproc 
 *		[
 *			label = "{IMMProc||Create()\lDeleteSelf()\lConnect()\lDisconnect()\lProcessMultimedia()\lPosition()\lProvideData()\lRequestData()\l}";
 *			URL = "\ref IMMProc";
 *		];
 *
 *		mmprocconnector 
 *		[
 *			label = "{MMProcConnector\<T\>|T* m_pItem\l|MMProcConnector()\lSetup()\l~MMProcConnector()\lGetItem()\lIsProvider()\lIsSource()\lConnect()\lRegister()\lAddClient()\lDisconnect()\lUnregister()\lRemoveClient()\lNotifyProvider()\lNotifyClients()\lNotifyHolder()\l}";
 *			URL = "\ref MMProcConnector";
 *		];
 *
 *		immprochasconnector 
 *		[
 *			label = "{IMMProcHasConnector\<T\>||GetConnector()\lNotify()\l}";
 *			URL = "\ref IMMProcHasConnector";
 *		];
 * }
 * \enddot
 *
 * The next diagram represents an example instance of the MMProc architecture. At the highest level, there is a Container, which is responsible for responding to instructions for loading, connecting and activating MMProcs. 
 *
 * As an example, we have two MMProcs. 
 *
 * MMProc1 contains an object of type Ta (Oa) which it can share with other IMMProc objectss. It also contains a connector object of type MMProcConnector \<Ta\> that allows the access to Oa.
 *
 * MMProc1 is required to implement the abstract class IMMProcHasConnector \<Ta\>, so that other MMProc objects can determine whether this specific type of connection is present and permit the establishment of connections to it.
 *
 * MMProc2 intends to make use of Oa included in another IMMProc. For that, it contains a connector object of type MMProcConnector \<Ta\> that allows the connection to another MMProc that provides such data (i.e. MMProc1 in this case) and implement IMMProcHasConnector \<Ta\>.
 *
 * To establish a connection between two IMMProc objects, the protocol requires the confirmation that both implement the same type of IMMProcHasConnector and then call the MMProcConnector::Connect method. For simplicity, a macro is offered (#MMPROC_CONNECT) that obeys the correct protocol.
 *
 * \image html MMProcConnectionExample.png
 *
 * A typical application requests the container to load/create a specific set of IMMProc objects/modules and to establish appropriate connections between them. The flow of MultiMedia across IMMProc chains data drives the process cycle that allows data decoding, internal states updating and interacting with the user.
 *
 * The following diagram is represents an actual application where the use of the MMProc architecture is used extensively. It is part of the AxUMF activeX project which is used in the Zones and Rules VCA configuration page of IPN/IPX cameras.
 * 
 * The process is as follows:
 *		-# The web page requests the loading of 6 MMProcs from associated DLLs:
 *			-# VCADataManager - Keeps state information and a copy of database settings. It coordinates the availability of this data with othe MMProcs connected to it;
 *			-# AVMMProc - Decodes video RTSP stream and, when connected to a Render provider, draws frames onto the DX surface;
 *			-# VCAPreview - Decodes Metadata RTSP stream and, when connected to a Render provider, draws Zones, Targets and Calibration structures;
 *			-# RenderMMProc - Provides a MultiViewRenderer for other MMProcs to draw on and presents the scene whenever a new frame is available;
 *			-# VCAAlarmlog - Displays a list of triggered alarms. This data is read from a MetaData object provided by another MMProc;
 *			-# VCAEventSettings - Provides a user interface settings tree for configuring of zone events. It requires access to a VCADataManager;
 *		-# The web page requests connections between MMProcs:
 *			-# VCAPreview - RenderMMProc (RENDER connection) - Allows the VCAPreview to have access to the DX surface and thus draw zones, targets and calibration grid.
 *			-# AVMMProc - VCAPreview (RENDER connection) - Allows the AVMMProc to have access to the DX surface through VCAPreview. As the surface is the same, once the AVMMProc renders a video frame, the VCAPreview can overlay its rendering.
 *			-# VCAPreview - VCADataManager (DATAMANAGER connection) - Allows the VCAPreview to have access to the Data Manager state and thus control the rendering.
 *			-# VCAAlarmlog - VCAPreview (METADATA connection) - Allows the VCAAlarmlog to have access to a VCAMetaData and keep the alarm list up-to-date.
 *			-# VCAEventSettings - VCADataManager (DATAMANAGER connection) - Allows the VCAEventSettings to have access to the Data Manager event settings information and keep edit it according to the user's instructions.
 *		-# Once the connections are in place, the application may perform further settings such as positioning viewable MMProcs and providing control and initialisation data.
 *		-# The system runs through the pushing of new RTSP data (as part of the UMF architecture) and the interaction with the user.
 *		-# NOTE: Other applications might or UI settings will use different connection setups. This is the main strength of the MMProc approach.
 *
 * \dot
 * digraph IPX_Example 
 * {
 *		node [shape=record, fontname=Helvetica, fontsize=10];
 *		
 *		datamgrmmproc
 *		[
 *			label = "{VCADataManager|VCADataManager}";
 *		];
 *
 *		avmmproc
 *		[
 *			label = "AVMMProc";
 *		];
 *
 *		viewmmproc
 *		[
 *			label = "{Viewer|VCAMetaData}";
 *		];
 *
 *		rendermmproc
 *		[
 *			label = "{RenderMMProc|MultiViewRender}";
 *		];
 *
 *		alarmmmproc
 *		[
 *			label = "VCAAlarmlog";
 *		];
 *
 *		eventsetmmproc
 *		[
 *			label = "VCAEventSettings";
 *		];
 *
 *		viewmmproc -> rendermmproc;
 *		avmmproc -> viewmmproc;
 *		viewmmproc -> datamgrmmproc;
 *		alarmmmproc -> viewmmproc;
 *		eventsetmmproc -> datamgrmmproc;
 *		
 *		edge [style = dashed];
 *		avmmproc -> rendermmproc;
 * }
 * \enddot
 */

