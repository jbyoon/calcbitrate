#ifndef __BASE_UMF_SORUCE_MGR_INTERFACE_H__
#define __BASE_UMF_SORUCE_MGR_INTERFACE_H__

/************************************************************************
Role of Source Manager
 * 1. enumerate usable source factories
 * 2. create source
 * 3. configure source

Comment
 * 1. How to use GetSrcFacList()
		for(i=0; i< GetSrcFacCnt(); i++) {
			GetSrcFacList(i, ...)
		}
 * 2. GetSrcFacList parameters
	- uSrcFacIdx : Range = [0, GetSrcFacCnt())
	- ccDescStrMax : count of characters (_countof(szDescStr))
 * 3. Call ReleaseSource after using source
*************************************************************************/
#include "IBaseUMFSource.h"

class IBaseUMFSourceMgr
{
public:	
	typedef enum {CREATED, READY} eStatus;

	virtual	eUMF_STATUS	GetSource(UINT uSrcFacId, UINT uSrcId, IBaseUMFSource** ppSource) = 0;
	virtual	eUMF_STATUS	ReleaseSource(IBaseUMFSource* pSource) = 0;
	virtual	HWND		ShowConfigDlg(UINT uSrcId, HWND hParentWnd)	= 0;
		
	virtual eStatus		GetStatus()		= 0;
	virtual UINT		GetSrcFacCnt()	= 0;
	virtual eUMF_STATUS	GetSrcFacList(UINT uSrcFacIdx, UINT* uSrcFacId, SRCTYPE* SrcType, TCHAR* szDescStr, UINT ccDescStrMax)	= 0;

	virtual	eUMF_STATUS	GetSourceSettings(UINT uSrcId, IBaseUMFStorage **ppXmlStorage) = 0;
};

#endif	// __BASE_UMF_SORUCE_MGR_INTERFACE_H__
