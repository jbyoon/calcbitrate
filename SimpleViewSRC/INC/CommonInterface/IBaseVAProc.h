#ifndef __VA_BASE_PROC_INTERFACE_H__
#define __VA_BASE_PROC_INTERFACE_H__

#include "IBaseUMFStorage.h"

class IMetadataParser;
class IBaseVAProc
{
public:
	typedef enum{
		BASE_MODULE = 0, 
		VIEW_MODULE = 1
	} VAPROC_TYPE;

	typedef enum{
		INIT_STATUS = 0, 
		RUN_STATUS 	= 1
	} VAPROC_STATUS;

	typedef enum{
		POPUP		= 0x00000001,
		CHILD		= 0x00000002,
	}DISPLAY_STYLE;


	virtual char* GetModuleName()	= 0;
	virtual ULONG GetType()			= 0;
	
	virtual BOOL Create(HWND hWndParent, IBaseUMFStorage *pStorage) = 0;
	virtual void Destroy() = 0;
	virtual BOOL ShowProperty(BOOL bShow) = 0;
	
	virtual BOOL Start(BOOL bStart) 	= 0;
	virtual VAPROC_STATUS GetStatus()	= 0;
	
	virtual BOOL	IsSupportStorage(eUMF_STORAGE_TYPE type) = 0;

	virtual BOOL ShowProc(BOOL bShow, DWORD EngId, DISPLAY_STYLE Style) = 0;
	virtual void ProcessVAData(ULONG EngId, BYTE *pImage, BITMAPINFOHEADER *pbm, FILETIME timestamp, 
		BYTE *pMetaData, int iMateDataLen, IMetadataParser *pParser, PXML pSourceXML) = 0;
};



#endif