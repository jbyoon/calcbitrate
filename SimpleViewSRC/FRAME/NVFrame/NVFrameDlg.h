#pragma once

#include "resource.h"		// main symbols
#include "CommonInterface/IBaseUMFrame.h"	//Frame Dialog
#include "NVViewModuleMgr.h"
#include ".\\NVSystem\\NVSystem.h"
#include ".\\NVSystem\\NVCGICommander.h"

#ifndef NOT_IN_USE
#define NOT_IN_USE -1
#endif

// CNVFrameDlg dialog
class CNVFrameDlg : public CDialog, public IBaseNVFrame, public IBaseUMFrame
{
	DECLARE_DYNAMIC(CNVFrameDlg)

public:
	CNVFrameDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CNVFrameDlg();

	// Dialog Data
	enum { IDD = IDD_NVFRAME_DIALOG };

	// Implementation of IBaseUMFView
	HWND	Open(ULONG ViewFlag, IBaseUMFStorage *pViewMgrXmlStrg, IBaseUMFContainer *pContainer);
	void	Close();
	void	DestroySelf();
	
	LONG	SendUMFCommand(ULONG nEngId, wchar_t* szCmd, wchar_t* szSubCmd, wchar_t* szValue, void* pParam);
	BOOL	Show(INT iShow);

//<JL>
	//LONG CreateMMProc(LPCTSTR szName, LPCTSTR szDLLFile);
	//LONG DestroyMMProc(LONG iMMProcID);
	//LONG ConnectMMProcs(LONG iSourceID, LONG iDestID);
	//LONG DisconnectMMProcs(LONG iSourceID, LONG iDestID);
	//LONG PositionMMProc(LONG iMMProcID, double dRelPosX, double dRelPosY, double dRelWidth, double dRelHeight);
	//IBaseMMProc* GetMMProc(LONG MMProcID);
//</JL>
	
	// Implementation of IBaseNVFrame
	void	FireEvent(ULONG nEngId, wchar_t* szCmd, wchar_t* szSubCmd, wchar_t* szValue);

	BOOL	RequestCGICommand(ULONG EngId, char* szCmd, BYTE* pResponse, DWORD* pLength){
		return m_NVSystem.RequestCGICommand(EngId, szCmd, pResponse, pLength);
	}
	BOOL	RequestCGICommandFast(ULONG EngId, char* szCmd, BYTE* pResponse, DWORD* pLength){
		return m_NVSystem.RequestCGICommand(EngId, szCmd, pResponse, pLength, TRUE);
	}

	PXML	GetSourceInfo(ULONG EngId){
		return m_NVSystem.GetSourceInfo(EngId);
	}

	PXML	GetFrameInfo(){
		return m_pViewStorage?m_pViewStorage->GetXML(): NULL;
	}

protected:

	DECLARE_MESSAGE_MAP()

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);

private:

	void	OnSize();

	BOOL				m_bOpen;
	ULONG				m_uVIewFlag;

	HWND				m_hParentWnd;
	IBaseUMFContainer*	m_pUMFContainer;	
	IBaseUMFStorage*	m_pViewStorage;

	CNVSystem			m_NVSystem;
	CNVViewModuleMgr	m_NVViewModuleMgr;
	CNVCGICommander		m_NVCGICommander;
public:
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnDestroy();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
};

BOOL	GetRtspSourceInfo(PXML pRtspSourceInfoXMLElement, RTSPSOURCEINFO *pRtspSourceInfo);
BOOL	SetRtspSourceInfo(PXML pRtspSourceInfoXMLElement, RTSPSOURCEINFO *pRtspSourceInfo);