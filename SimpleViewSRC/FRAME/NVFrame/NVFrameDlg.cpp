// SimpleViewDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NVFrame.h"
#include "NVFrameDlg.h"
#include "UMFXMLUtil\UMFXMLUtil.h"

//////////////////////////////////////////////////////////////////////
// Helper Function
//////////////////////////////////////////////////////////////////////
// CNVFrameDlg dialog

IMPLEMENT_DYNAMIC(CNVFrameDlg, CDialog)

#define IDM_COMPLEXITY 206
#define IDM_MOTIONRATE 207

CNVFrameDlg::CNVFrameDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CNVFrameDlg::IDD, pParent)
{
	m_pViewStorage	= NULL;

	m_uVIewFlag		= 0;
	m_hParentWnd	= NULL;
	m_pUMFContainer	= NULL;
	m_bOpen			= FALSE;
}

CNVFrameDlg::~CNVFrameDlg()
{
	Close();
}

void CNVFrameDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CNVFrameDlg, CDialog)
	ON_WM_SIZE()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_DESTROY()
	ON_WM_SYSCOMMAND()
END_MESSAGE_MAP()


// CNVFrameDlg message handlers

BOOL CNVFrameDlg::OnInitDialog()
{
	__super::OnInitDialog();

	HICON hIcon = LoadIcon(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_VASCLIENT_ICON));
	this->SetIcon(hIcon, FALSE);
	
	CUMFXMLUtil		ViewerXML(m_pViewStorage->GetXML());

	CBaseUMFStorage	NVViewXMLStorage(ViewerXML.GetChildXML(_T("NVViewManager")));
	/*
	HWND hWnd;
	if(UMF_VF_CHILD == m_uVIewFlag){
		hWnd = m_hParentWnd;
	}else{
		hWnd = m_hWnd;
	}
	*/
	if( !m_NVViewModuleMgr.Setup(m_hWnd, this, &NVViewXMLStorage) ) {
		EndDialog(0);
		return FALSE;
	}

	CMenu* pSysMenu = GetSystemMenu(FALSE); 
    if (pSysMenu != NULL)
    {
        pSysMenu->AppendMenu(MF_SEPARATOR);
        pSysMenu->AppendMenu(MF_STRING, IDM_COMPLEXITY, CString("Complexity"));
        pSysMenu->AppendMenu(MF_STRING, IDM_MOTIONRATE, CString("MotionRate"));
    }

	m_NVViewModuleMgr.Show(m_NVViewModuleMgr.GetDefaultViewIndex(), 0xFFFFFFFF, TRUE, IBaseNVView::CHILD);

	m_NVSystem.Start();
	return TRUE;  // return TRUE unless you set the focus to a control
}


HWND	CNVFrameDlg::Open(ULONG ViewFlag, IBaseUMFStorage *pViewStorage, IBaseUMFContainer *pContainer)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	m_bOpen	= FALSE;

	if(!pViewStorage || !pContainer) {
		TRACE(_T("Can not pViewMgrXmlStrg[%p], pSourceMgr[%p] not Assigned \n"));
		return NULL;
	}
	
	IBaseUMFSourceMgr*	pSourceMgr	= pContainer->GetSourceMgr();
	if(!m_NVSystem.Open(pViewStorage, pSourceMgr)){
		TRACE(_T("Can not Init VASystem. \n"));
		return NULL;
	}
	
	m_NVSystem.RegisterNVEventSink(&m_NVViewModuleMgr);
	m_pViewStorage	= pViewStorage;
	m_hParentWnd	= pContainer->GetContainerHWnd();
	m_pUMFContainer	= pContainer;
	if(0 == ViewFlag){
		m_uVIewFlag	= m_hParentWnd?UMF_VF_CHILD:UMF_VF_POPUP;
	}else{
		m_uVIewFlag	= ViewFlag;
	}
	
	m_bOpen	= TRUE;
	return (HWND)TRUE;
}


void	CNVFrameDlg::Close()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	m_NVSystem.Stop();
	m_NVSystem.Close();
	m_NVViewModuleMgr.Endup();
	m_bOpen	= FALSE;
}


void	CNVFrameDlg::DestroySelf()
{ 
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	Close();
	delete this; 
}


BOOL	CNVFrameDlg::Show(INT iShow)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	
	if(!m_bOpen){
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("CNVFrameDlg::Show open before \n"));
		return FALSE;
	}

	if(m_uVIewFlag&UMF_VF_POPUP){
		if(iShow){
			DoModal();
		}else{
			EndDialog(0);
		}		
	}else{
		if(iShow){
			if(Create(CNVFrameDlg::IDD, CWnd::FromHandle(m_hParentWnd))){
				ModifyStyle(WS_OVERLAPPED|WS_POPUP|WS_CAPTION|WS_BORDER|WS_DLGFRAME|WS_THICKFRAME|WS_TABSTOP, WS_CHILD);
				SetParent(CWnd::FromHandle(m_hParentWnd));
				CRect rcParent;
				::GetClientRect(m_hParentWnd, &rcParent);
				MoveWindow(0,0,rcParent.Width(),rcParent.Height());
				ShowWindow(SW_SHOW);
			}
		}else{
			EndDialog(0);
		}
	}
	
	return TRUE;
}

#include "UMFCmdDef.h"

#define CMD_TYPE_NAME		NVFrameCmdType
#define	CMD_TYPE_END		NVFrameEndCmd
#define CMD_STRING_NAME		NVFrameCmdString
#define	CMD_FUNCTION_NAME	FindNVFrameControlCmd
#define CMD_HEADER			"UMFCmdDef.h"
#define CMD_TYPE			UMFNVFRAME_CONTROL_CMD
#include "UMFCmdCommon.h"


LONG CNVFrameDlg::SendUMFCommand(ULONG nEngId, wchar_t* szCmd, wchar_t* szSubCmd, wchar_t* szValue, void* pParam)
{
	int		nControlCmd;
	if(_wcsicmp(_T("CONTROL"), szCmd) == 0){
		nControlCmd = FindNVFrameControlCmd(szSubCmd);
		if(CHANGESIZE == nControlCmd){
			if(m_hParentWnd != NULL)
			{
				CRect rcParent;
				::GetClientRect(m_hParentWnd, &rcParent);
				MoveWindow(0, 0, rcParent.Width(), rcParent.Height());
			}
			return UMF_OK;
		}else if(CHANGENVVIEW == nControlCmd){
			int iRunning = m_NVViewModuleMgr.GetRunningViewIndex();
			if(iRunning >= 0) m_NVViewModuleMgr.Show(iRunning, nEngId, FALSE, IBaseNVView::CHILD);

			int iToRun = _ttoi(szValue);
			m_NVViewModuleMgr.Show(iToRun, nEngId, TRUE, IBaseNVView::CHILD);
			return UMF_OK;
		}
	}
	return m_NVViewModuleMgr.SendCommand(nEngId, szCmd, szSubCmd, szValue, pParam);
}


void CNVFrameDlg::FireEvent(ULONG nEngId, wchar_t* szCmd, wchar_t* szSubCmd, wchar_t* szValue)
{
	TRACE("***** Call FireEvent() "); //sean
	USES_CONVERSION;
	if( _wcsicmp(L"CONTROL", szCmd) == 0 && _wcsicmp(L"CHANGESOURCEINFO", szSubCmd) == 0 ) {

		IBaseUMFStorage *pXmlStorage = NULL;
		IBaseUMFSourceMgr*	pSourceMgr	= m_pUMFContainer->GetSourceMgr();

		NVEngineInfo* pNVEngInfo = m_NVSystem.GetNVEngineInfo(nEngId);
		ASSERT( NULL != pNVEngInfo );
		pSourceMgr->GetSourceSettings(pNVEngInfo->SourceId, &pXmlStorage);

//Need to extand other source type
		RTSPSOURCEINFO		rtspInfoSource;
		GetRtspSourceInfo(pXmlStorage->GetXML(), &rtspInfoSource);
		memcpy(rtspInfoSource.szURL, W2T(szValue), sizeof(rtspInfoSource.szURL));
		SetRtspSourceInfo(pXmlStorage->GetXML(), &rtspInfoSource);

		m_NVSystem.Stop();

		CNVEngine* pNVEngine = m_NVSystem.GetEngine(nEngId);
		ASSERT( NULL != pNVEngine );
		pSourceMgr->ReleaseSource(pNVEngine->GetSource());

		IBaseUMFSource* pUMFSource = NULL;
		if(UMF_OK != pSourceMgr->GetSource(UMF_ID_NONE, pNVEngInfo->SourceId, &pUMFSource)) {
			TRACE(_T("Can not get source for engine [%d] Source[%d] \n"), nEngId, pNVEngInfo->SourceId);
		}
		pNVEngine->SetSource(pUMFSource);

		m_NVSystem.Start();

		return;
	}

	if(m_pUMFContainer) m_pUMFContainer->OnUMFEvent(nEngId, szCmd, szSubCmd, szValue);
}

void CNVFrameDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);
	OnSize();
}

void CNVFrameDlg::OnSize()
{
	m_NVViewModuleMgr.SendCommand(0, L"CONTROL", L"CHANGESIZE", NULL, NULL);
}

void CNVFrameDlg::OnDestroy()
{
	__super::OnDestroy();

	Close();
}

static BOOL	GetRtspSourceInfo(PXML pRtspSourceInfoXMLElement, RTSPSOURCEINFO *pRtspSourceInfo)
{

	IXMLDOMNode*		pSourceXML = (IXMLDOMNode*)pRtspSourceInfoXMLElement;
	try {
		TCHAR* pXml = pSourceXML->xml;

		GetAttrValueString(pSourceXML, _T("RTSPURL"), pRtspSourceInfo->szURL, _countof(pRtspSourceInfo->szURL));
		GetAttrValueString(pSourceXML, _T("SESSION"), pRtspSourceInfo->szSession, _countof(pRtspSourceInfo->szSession));
		GetAttrValueString(pSourceXML, _T("ID"), pRtspSourceInfo->szID, _countof(pRtspSourceInfo->szID));
		GetAttrValueString(pSourceXML, _T("PW"), pRtspSourceInfo->szPW, _countof(pRtspSourceInfo->szPW));
		pRtspSourceInfo->uAliveCheckTime = GetAttrValueNum(pSourceXML, _T("AliveCheckTime"));
		if(pRtspSourceInfo->uAliveCheckTime == 0) pRtspSourceInfo->uAliveCheckTime = 10;
	} catch (...) {
		return FALSE;
	}
	return TRUE;
}

static BOOL	SetRtspSourceInfo(PXML pRtspSourceInfoXMLElement, RTSPSOURCEINFO *pRtspSourceInfo)
{
	IXMLDOMElementPtr	SourceXML;
	IXMLDOMNode*		pSourceXML = (IXMLDOMNode*)pRtspSourceInfoXMLElement;
	try	{	
		SourceXML = (IXMLDOMElementPtr)pSourceXML;
		SetAttrValueString(SourceXML, _T("RTSPURL"), pRtspSourceInfo->szURL);
		SetAttrValueString(SourceXML, _T("WEBURL"), pRtspSourceInfo->szWEBURL);// sean
/*
		SetAttrValueString(SourceXML, _T("SESSION"), pRtspSourceInfo->szSession);
		SetAttrValueString(SourceXML, _T("ID"), pRtspSourceInfo->szID);
		SetAttrValueString(SourceXML, _T("PW"), pRtspSourceInfo->szPW);
		SetAttrValueNum(SourceXML, _T("AliveCheckTime"), pRtspSourceInfo->uAliveCheckTime);
*/
	} catch (...) {
		return FALSE;	
	}
	return TRUE;
}

void CNVFrameDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	// TODO: Add your message handler code here and/or call default
	TRACE(_T("OnSysCommand %X\n"),nID);
	if(nID == IDM_COMPLEXITY) {
		SendUMFCommand(0,L"CONTROL",L"COMPLEXITY",0,0);
	} else if(nID == IDM_MOTIONRATE) {
		SendUMFCommand(0,L"CONTROL",L"MOTIONRATE",0,0);
	}
	__super::OnSysCommand(nID, lParam);
}
