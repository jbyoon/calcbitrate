#ifndef __NV_VIEW_STATIC_MODULE_DEF_H__
#define __NV_VIEW_STATIC_MODULE_DEF_H__


#define MAXNVVIEWODULE_CNT	1

#pragma comment( lib, "../../LIB/NVViews/sSimpleNVView" )

extern "C" IBaseNVView*  WINAPI CreateNVSimpleViewModule();

FARPROC pNVVIewCreateFuncList[MAXNVVIEWODULE_CNT] = {
	(FARPROC)CreateNVSimpleViewModule
};


#endif
