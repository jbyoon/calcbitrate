#include "StdAfx.h"
#include "NVViewModuleMgr.h"
#include "UMFXMLUtil\UMFXMLUtil.h"
#include "UMFUtil.h"

CNVViewModuleMgr::CNVViewModuleMgr(void)
{
	m_hWnd			= NULL;			
	m_MaxModuleCnt	= 0;
	memset(m_pNVViewModules, 0, sizeof(m_pNVViewModules));
}


CNVViewModuleMgr::~CNVViewModuleMgr(void)
{
	Endup();
}


BOOL	CNVViewModuleMgr::Setup(HWND hWnd, IBaseNVFrame *pNVFrame, IBaseUMFStorage *pStorage)
{
	TCHAR	szModulePath[MAX_PATH];
	
	if(!pStorage || !pStorage->GetXML()){
		UMFTrace(UMF_TRACE_LEVEL_ERROR, _T("Failed to Setup NVFrame.\n"),);
		return FALSE;
	}

	CUMFXMLUtil UMFUtil(pStorage->GetXML());
	if(UMFUtil.GetAttrValueString(_T("Path"), szModulePath, MAX_PATH)){
		if(!LoadNVViewModules(hWnd, pNVFrame, szModulePath)){
			UMFTrace(UMF_TRACE_LEVEL_ERROR, _T("Failed to Load %s.\n"), szModulePath);
			return FALSE;
		}
	}else{
		if(!LoadNVViewModulesByStorage(hWnd, pNVFrame, pStorage)){
			UMFTrace(UMF_TRACE_LEVEL_ERROR, _T("Failed to Create NVViewModule[%s].\n"), szModulePath);
			return FALSE;
		}
	}

	memset(m_szDefaultModule, 0, sizeof(m_szDefaultModule));
	UMFUtil.GetAttrValueString(_T("DefaultModule"), m_szDefaultModule, MAX_PATH);
	m_hWnd	= hWnd;	

	return TRUE;
}


void	CNVViewModuleMgr::Endup()
{
	DWORD i;
	for(i = 0 ; i < m_MaxModuleCnt ; i++){
		m_pNVViewModules[i]->Destroy();
		m_pNVViewModules[i]= 0;	
	}
	m_MaxModuleCnt = 0;
}


BOOL	CNVViewModuleMgr::Show(DWORD index, DWORD EngId, BOOL bShow, IBaseNVView::DISPLAY_STYLE DisplayType)
{
	if(index >= m_MaxModuleCnt) return FALSE;
	m_pNVViewModules[index]->Show(EngId, bShow, DisplayType);
	
	//Change Frame name 
	if(m_hWnd && (DisplayType&IBaseNVView::CHILD)){
		SetWindowTextA(m_hWnd, m_pNVViewModules[index]->GetModuleName());
	}

	return TRUE;
}


LONG	CNVViewModuleMgr::SendCommand(ULONG nEngId, wchar_t* szCmd, wchar_t* szSubCmd, wchar_t* szValue, void* pParam)
{
	LONG res = -1;
	DWORD	i;	
	for(i = 0 ; i < m_MaxModuleCnt ; i++){
		if(m_pNVViewModules[i]->GetStatus() == IBaseNVView::RUN_STATUS){
//			if(m_pNVViewModules[i]->OnNVViewCommand(nEngId, szCmd, szSubCmd, szValue, pParam))
//				res = TRUE;
			return m_pNVViewModules[i]->OnNVViewCommand(nEngId, szCmd, szSubCmd, szValue, pParam);
		}
	}
	return res;
}


BOOL	CNVViewModuleMgr::ChangeMMSourceInfo(DWORD EngId, PXML pSoruceInfoXML)
{
	DWORD	i;	
	for(i = 0 ; i < m_MaxModuleCnt ; i++){
		if(m_pNVViewModules[i]->GetStatus() == IBaseNVView::RUN_STATUS){
			m_pNVViewModules[i]->ChangeMMSourceInfo(EngId, pSoruceInfoXML);
		}
	}
	return TRUE;
}


BOOL	CNVViewModuleMgr::ProcessMMData(DWORD EngId, DWORD Type, BYTE *pMMData, UINT MMDataLen, FILETIME *pTimestamp)
{
	DWORD	i;	
	for(i = 0 ; i < m_MaxModuleCnt ; i++){
		if(m_pNVViewModules[i]->GetStatus() == IBaseNVView::RUN_STATUS){
			m_pNVViewModules[i]->ProcessMMData(EngId, Type, pMMData, MMDataLen, pTimestamp);
		}
	}
	return TRUE;
}


DWORD	CNVViewModuleMgr::GetDefaultViewIndex()
{
	DWORD	i;	
	USES_CONVERSION;
	for(i = 0 ; i < m_MaxModuleCnt ; i++){
		if(strcmp(T2A(m_szDefaultModule), m_pNVViewModules[i]->GetModuleName()) == 0){
			return i;
		}
	}

	return 0;		
}

DWORD CNVViewModuleMgr::GetRunningViewIndex()
{
	DWORD	i;	
	USES_CONVERSION;
	for(i = 0 ; i < m_MaxModuleCnt ; i++){
		if(m_pNVViewModules[i]->GetStatus() == IBaseNVView::RUN_STATUS){
			return i;
		}
	}
	return -1;		
}



#ifndef _LIB

IBaseNVView*	CreateNVViewModule(TCHAR *szModuleName, BOOL bDirectLoad)
{
	if(NULL == _tcsstr(szModuleName, _T(".dll"))){
		UMFTrace(UMF_TRACE_LEVEL_VERBOSE, _T("%s is not dll.\n"), szModuleName);
		return NULL;
	}

	HMODULE hLib=NULL;
	hLib=LoadUMFLibrary(szModuleName, bDirectLoad);
	if(NULL == hLib){		
		UMFTrace(UMF_TRACE_LEVEL_VERBOSE, _T("Failed to Load %s. GetLastError [%d]\n"), szModuleName, GetLastError());
		return NULL;
	}

	IBaseNVView* pNVViewModule = NULL;
	UMF_CREATE_INSTANCE(pNVViewModule, hLib, IBaseNVView, CreateNVViewModule);
	return pNVViewModule;
}


BOOL	CNVViewModuleMgr::LoadNVViewModules(HWND hWnd, IBaseNVFrame *pNVFrame, TCHAR *szModulePath)
{
	WIN32_FIND_DATA FindFileData;
	HANDLE hFind = INVALID_HANDLE_VALUE;

	TCHAR szFindFileFilter[MAX_PATH];
	_tcscpy_s(szFindFileFilter, szModulePath);
	_tcscat_s(szFindFileFilter, _T("\\*"));

	hFind = FindFirstFile(szFindFileFilter, &FindFileData);
	if (hFind == INVALID_HANDLE_VALUE){
	  UMFTrace(UMF_TRACE_LEVEL_VERBOSE, _T("Failed to find modules in [%s]. GetLastError [%d]\n"), szFindFileFilter, GetLastError());
      return FALSE;
	} 

	IBaseNVView* pModule;
	while(1){
		_stprintf_s(szFindFileFilter, _T("%s\\%s"),szModulePath, FindFileData.cFileName);
		pModule = CreateNVViewModule(szFindFileFilter, TRUE);
		if(pModule){
			if(pModule->Create(hWnd, pNVFrame)){
				m_pNVViewModules[m_MaxModuleCnt] = pModule;
				m_MaxModuleCnt++;
			} else {
				pModule->Destroy();
			}
		}

		if(m_MaxModuleCnt >= MAX_MODULE){
			UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Too many NVViewModules loaded. The max number of NVViewModule[%d]\n"), MAX_MODULE);
			break;
		}
		if(FindNextFile(hFind, &FindFileData) == 0)break;
	}

	return (m_MaxModuleCnt == 0)? FALSE:TRUE;
}


BOOL	CNVViewModuleMgr::LoadNVViewModulesByStorage(HWND hWnd, IBaseNVFrame *pNVFrame, IBaseUMFStorage *pStorage)
{
	PXML pXML = pStorage->GetXML();
	CUMFXMLUtil		XMLUtil;
	IBaseNVView*	pModule;
	TCHAR			szViewName[MAX_PATH];

	XMLUtil.SetXML(pXML);
	pXML = XMLUtil.GetFirstChild();

	while(pXML){
		XMLUtil.SetXML(pXML);
		
		XMLUtil.GetAttrValueString(_T("Name") ,szViewName, MAX_PATH);
		pModule = CreateNVViewModule(szViewName, FALSE);
		if(pModule){
			if(pModule->Create(hWnd, pNVFrame)){
				m_pNVViewModules[m_MaxModuleCnt] = pModule;
				m_MaxModuleCnt++;
			}
		}

		if(m_MaxModuleCnt >= MAX_MODULE){
			UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Too many NVViewModules loaded. The max number of NVViewModule[%d]\n"), MAX_MODULE);
			break;
		}
		
		pXML = XMLUtil.GetNextChild(pXML);
	}
	return (m_MaxModuleCnt == 0)? FALSE:TRUE;
}

#else

#include "NVViewStaticModuleDef.h"

BOOL	CNVViewModuleMgr::LoadNVViewModules(HWND hWnd, IBaseNVFrame *pNVFrame, TCHAR *szModulePath)
{
	int i;
	IBaseNVView* pModule;

	for( i = 0 ; i < MAXNVVIEWODULE_CNT ;  i++){
		pModule	= (IBaseNVView*)pNVVIewCreateFuncList[i]();
		if(pModule){
			if(pModule->Create(hWnd, pNVFrame)){
				m_pNVViewModules[m_MaxModuleCnt] = pModule;
				m_MaxModuleCnt++;
			}
		}
	}
	return TRUE;
}



#endif

