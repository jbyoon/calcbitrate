#include "StdAfx.h"
#include "NVCGICommander.h"

CNVCGICommander::CNVCGICommander()
{
	m_bSetup	= FALSE;
	
	//defaul auto login on and ssl on
	m_bAutoLogin= TRUE;
	m_bSSL		= TRUE;
}


CNVCGICommander::~CNVCGICommander()
{
}


BOOL	CNVCGICommander::Setup(TCHAR* szServerIP, int nPort, TCHAR *szUserName, TCHAR *szPassword)
{
	if(m_bSetup){
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("CNVCGICommander Setup before reopen \n"));
		Endup();
	}

	if(S_OK != m_HTTPClient.Init()){
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("CNVCGICommander Setup before reopen \n"));
		return FALSE;
	}

	if(0 == nPort){
		nPort = (m_bSSL)?DEFAULT_SSL_PORT:DEFAULT_PORT;
	}

	//Test connect
	HANDLE hSession;
	if((hSession = m_HTTPClient.Connect(szServerIP, nPort, szUserName, szPassword)) == NULL){
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("CNVCGICommander Fail Connect [%s:%d] UserName[%s] Password[%s] \n"), 
			szServerIP, nPort, szUserName, szPassword);
		m_HTTPClient.Deinit();
		return FALSE;
	}

	//Store parameters
	m_HTTPClient.Disconnect(hSession);
	_tcscpy_s(m_szServerIP, szServerIP);
	m_Port	= nPort;
	_tcscpy_s(m_szUserName, szUserName);
	_tcscpy_s(m_szPassword, szPassword);

	m_bSetup = TRUE;

	return m_bSetup;
}


void	CNVCGICommander::Endup()
{
	if(m_bSetup){
		m_HTTPClient.Deinit();				
		m_bSetup = FALSE;
	}
}


void	CNVCGICommander::SetFlag(DWORD Flag, BOOL bSet)
{
	if(AUTO_LOGIN == Flag) m_bAutoLogin = bSet;
	else if(USE_SSL == Flag) m_bSSL = bSet;
}


#pragma warning(disable: 4996)
static int	parsing_gettokenvalue( IN	  char*  _psz,
						   IN	  char*  _pszdelimit, 
						   OUT	  char   _aszvalue[][128],
						   IN OUT int*	 _pncount )
{
	int		nret = 0;

	char	sz[1024] = {0,};
	strcpy( sz, _psz );

	int ncount = 0;
	char* psztoken = strtok( sz, _pszdelimit );
	while( psztoken ) {
		if( *_pncount < (ncount+1) ) {
			nret = -1;
			break;
		}

		if( 128-1 < strlen( psztoken ) ) {
			nret = -2;
			break;
		}

		strcpy( &_aszvalue[ncount][0], psztoken );
		ncount += 1;

		psztoken = strtok( NULL, _pszdelimit );
	}

	*_pncount = ncount;
	//__E(DBG_TRA, "libnvc_gettokenvalue ncount %d, ret %d", ncount, nret);

	return nret;
}



BOOL	CNVCGICommander::RequestCGICommand(char* szCmd, BYTE* pResponse, DWORD* pLength)
{
	if(!m_bSetup){
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("CNVCGICommander Setup before \n"));
		return FALSE;
	}

	HANDLE	hSession	= NULL;
	HANDLE	hRequest	= NULL;

	BOOL bRet = TRUE;
	char	asz[2][128] = {{0,}};
	INT		naszcount = 2;


	USES_CONVERSION;
	UMFTrace(UMF_TRACE_LEVEL_VERBOSE, _T("RequestCGICommand nip %s, port %d, user %s, pass %s\n CMD : %s\n"), 
				m_szServerIP, m_Port, m_szUserName, m_szPassword, A2T(szCmd));

	hSession = m_HTTPClient.Connect( m_szServerIP,
							   m_Port, 
							   m_bAutoLogin ? m_szUserName : NULL,
							   m_bAutoLogin ? m_szPassword : NULL );
	if(!hSession){
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Fail RequestCGICommand Connect nip %s, port %d, user %s, pass %s\nCMD : %s\n"), 
				m_szServerIP, m_Port, m_szUserName, m_szPassword, A2T(szCmd));
		bRet = FALSE;
		goto EXIT;
	}


	parsing_gettokenvalue( szCmd, "?", asz, &naszcount );

	
	hRequest = m_HTTPClient.OpenRequest(hSession, m_bSSL, TRUE/*fIgnoreCA*/, A2T(asz[0]));
	if(!hRequest){
		UMFTrace(UMF_TRACE_LEVEL_VERBOSE, _T("Fail RequestCGICommand OpenRequest nip %s, port %d, user %s, pass %s\nCMD : %s\n"), 
				m_szServerIP, m_Port, m_szUserName, m_szPassword, A2T(szCmd));
		bRet = FALSE;
		goto EXIT;
	}

	bRet = m_HTTPClient.SendRequest( hRequest, asz[1], pResponse, pLength);
	if(!bRet){
		UMFTrace(UMF_TRACE_LEVEL_VERBOSE, _T("Fail RequestCGICommand SendRequest nip %s, port %d, user %s, pass %s\nCMD : %s\n"), 
				m_szServerIP, m_Port, m_szUserName, m_szPassword, A2T(szCmd));
		bRet = FALSE;
		goto EXIT;
	}
	
	bRet = TRUE;

EXIT:
	if(hRequest) m_HTTPClient.CloseRequest(hRequest);
	if(hSession) m_HTTPClient.Disconnect(hSession);

	return bRet;
}

