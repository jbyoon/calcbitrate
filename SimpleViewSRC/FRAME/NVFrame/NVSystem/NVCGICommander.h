#pragma once

#include "HTTPClientBase.h"

class CNVCGICommander 
{
public:
	enum {DEFAULT_CGI_CMD_LENGTH = 512};
	enum {DEFAULT_PORT = 80, DEFAULT_SSL_PORT = 443};

	typedef enum{
		AUTO_LOGIN	= 1,
		USE_SSL		= 2,
	}eNVCGIFLAG;

	CNVCGICommander(void);
	~CNVCGICommander(void);
	
	BOOL	Setup(TCHAR* szServerIP, int Port, TCHAR *szUserName = NULL, TCHAR *szPassword = NULL);
	void	Endup();

	BOOL	IsSameServer(TCHAR* szServerIP){
		return (0 == _tcscmp(szServerIP, m_szServerIP));
	}
	
	void	SetFlag(DWORD Flag, BOOL bSet);
	
	BOOL	RequestCGICommand( char* szCmd, BYTE* pResponse, DWORD* pLength);
	BOOL	RequestCGICommandFast( char* szCmd, BYTE* pResponse, DWORD* pLength){
		return RequestCGICommand(szCmd, pResponse, pLength);
	}

private:

	BOOL			m_bSetup;
	
	CHttpClientBase	m_HTTPClient;

	BOOL			m_bAutoLogin;
	BOOL			m_bSSL;
	
	TCHAR			m_szServerIP[MAX_PATH];
	INT				m_Port;
	TCHAR			m_szUserName[MAX_PATH];
	TCHAR			m_szPassword[MAX_PATH];
};
