#ifndef __VA_CONF_STORAGE_H__
#define __VA_CONF_STORAGE_H__

#include "CommonInterface\IBaseUMFStorage.h"
#include "IBaseVAAlgo.h"

IFileUMFStorage* CreateConfStorage(eVA_ALGO_TYPE type);
void	DestroyConfStorage(IFileUMFStorage* pStorage);



#endif