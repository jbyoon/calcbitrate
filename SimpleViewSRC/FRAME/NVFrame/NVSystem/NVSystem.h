#pragma once

#include "CommonInterface\IBaseUMFSource.h"
#include "CommonInterface\IBaseUMFSourceMgr.h"

#include "NVEngine.h"
#include "../../SOURCE/RtspMediaSource/RTSPSource.h"

typedef struct{
	DWORD	Id;
	DWORD	SourceId;
	DWORD	NVProcFacId;
	TCHAR	szConfPath[MAX_PATH];
}NVEngineInfo;


class CNVSystem
{
public:
	enum {MAX_VA_ENGINE = 32};

public:
	CNVSystem();
	~CNVSystem();

	BOOL		Open(IBaseUMFStorage *pViewStorage, IBaseUMFSourceMgr* pSourceMgr);
	void		Close();

	DWORD			GetTotalEngCnt(){return m_nTotalEng;}
	CNVEngine*		GetEngine(DWORD index){return (m_nTotalEng>index)?m_pNVEngine[index]:NULL;}
	NVEngineInfo*	GetNVEngineInfo(DWORD index){return (m_nTotalEng>index)?&(m_NVEngineInfo[index]):NULL;}
	IBaseUMFStorage* GetConfStorage(DWORD index){return (m_nTotalEng>index)?m_pConfStorage[index]:NULL;}

	BOOL		RequestCGICommand(ULONG EngId, char* szCmd, BYTE* pResponse, DWORD* pLength, BOOL bFast = FALSE);
	PXML		GetSourceInfo(ULONG EngId);

	BOOL		RegisterNVEventSink(INVEventSink *pNVEventSink);
	void		UnRegisterNVEventSinkAll();

	BOOL		Start();
	void		Stop();

private:
	BOOL				m_bSetup;

	DWORD				m_nTotalEng;
	NVEngineInfo		m_NVEngineInfo[MAX_VA_ENGINE];
	IFileUMFStorage*	m_pConfStorage[MAX_VA_ENGINE];

	CNVEngine*			m_pNVEngine[MAX_VA_ENGINE];

	IBaseUMFSourceMgr*	m_pSourceMgr;
	
	ULONG				m_nCGICommanderCnt;
	CNVCGICommander*	m_pCGICommander[MAX_VA_ENGINE];
	CNVCGICommander*	GetCGICommander(IBaseUMFSource* pUMFSource);

	BOOL				CreateNVEngine(ULONG EngId);
	BOOL				GetNVEngineInfo(IBaseUMFStorage *pViewStorage);

	
};