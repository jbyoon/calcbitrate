#include "StdAfx.h"
#include "NVSystem.h"
#include "UMFXMLUtil\UMFXMLUtil.h"
//#include "VAConfStorage.h"
#include "NVCGICommander.h"

#define CHECK_SETUP_RETURN_FALSE	\
	if(!m_bSetup){\
		TRACE(_T("CNVSystem does not opened before\n"));\
		return FALSE;\
	}\

#define CHECK_SETUP_RETURN	\
	if(!m_bSetup){\
		TRACE(_T("CNVSystem does not opened before\n"));\
		return;\
	}\




CNVSystem::CNVSystem()
{
	m_nTotalEng	= 0;
	memset(m_pNVEngine, 0, sizeof(m_pNVEngine));
	memset(m_NVEngineInfo, 0, sizeof(m_NVEngineInfo));
	memset(m_pConfStorage, 0, sizeof(m_pConfStorage));

	m_nCGICommanderCnt	= 0;
	memset(m_pCGICommander, 0, sizeof(m_pCGICommander));

	m_pSourceMgr	= NULL;
	m_bSetup		= FALSE;
}

CNVSystem::~CNVSystem()
{
	Close();
}


BOOL	CNVSystem::Open(IBaseUMFStorage *pViewStorage, IBaseUMFSourceMgr* pSourceMgr)
{
	if(m_bSetup){
		TRACE(_T("CNVSystem::Opened before \n"));
		return TRUE;
	}

	if(!GetNVEngineInfo(pViewStorage)){
		TRACE(_T("CNVSystem::Can not get NVEngineInfo\n"));
		return FALSE;
	}
	
	m_pSourceMgr	= pSourceMgr;
	for(UINT i=0 ; i< m_nTotalEng ; i++){
		if(!CreateNVEngine(i)){
			TRACE(_T("CNVSystem::Fail CreateNVEngine [%d]\n"), i);
			return FALSE;
		}
	}

	m_bSetup		= TRUE;
	return TRUE;
}


void	CNVSystem::Close()
{
	if(m_bSetup){
		UINT i;
		for(i=0 ; i< m_nTotalEng ; i++){
			if(m_pNVEngine[i]){
				delete m_pNVEngine[i];
				m_pNVEngine[i] = NULL;
			}
		}

		for(i=0 ; i< m_nCGICommanderCnt ; i++){
			if(m_pCGICommander[i]){
				delete m_pCGICommander[i];
				m_pCGICommander[i] = NULL;
			}
		}

		m_bSetup = FALSE;
	}
}


BOOL	CNVSystem::Start()
{
	CHECK_SETUP_RETURN_FALSE;

	for(UINT i=0 ; i< m_nTotalEng ; i++)
		if(m_pNVEngine[i])m_pNVEngine[i]->Start();

	
	//for test CGI Command
#if 0

	#define	_MAXLEN_SYNTAXBUFF		(256 * 1024)
	BYTE szResponse[_MAXLEN_SYNTAXBUFF];
	char szRequest[4096];
	ULONG Length = _MAXLEN_SYNTAXBUFF;
	memset(szResponse, 0, sizeof(szResponse));
	strcpy((char *)szRequest, "/nvc-cgi/admin/param.fcgi?action=query");
	RequestCGICommand(0, szRequest, szResponse, &Length);

#endif

	return TRUE;
}


void	CNVSystem::Stop()
{
	CHECK_SETUP_RETURN;

	for(UINT i=0 ; i< m_nTotalEng ; i++)
		if(m_pNVEngine[i])m_pNVEngine[i]->Stop();
}


BOOL	CNVSystem::RegisterNVEventSink(INVEventSink *pNVEventSink)
{
	CHECK_SETUP_RETURN_FALSE;

	for(UINT i=0 ; i< m_nTotalEng ; i++)
		if(m_pNVEngine[i])m_pNVEngine[i]->RegisterNVEventSink(pNVEventSink);

	return TRUE;
}


void	CNVSystem::UnRegisterNVEventSinkAll()
{
	CHECK_SETUP_RETURN;
	for(UINT i=0 ; i< m_nTotalEng ; i++)
		if(m_pNVEngine[i])m_pNVEngine[i]->UnRegisterNVEventSinkAll();
}



BOOL	CNVSystem::GetNVEngineInfo(IBaseUMFStorage *pViewStorage)
{
	CUMFXMLUtil XMLUtil, XMLChildUtil;
	PXML ChildXML;
	int i;

	XMLUtil.SetXML(pViewStorage->GetXML());

	ChildXML = XMLUtil.GetChildXML(_T("EningSetting"));
	if(!ChildXML){
		return FALSE;
	}

	XMLUtil.SetXML(ChildXML);
	ChildXML = XMLUtil.GetFirstChild();
	i = 0;
	while(ChildXML){
		XMLChildUtil.SetXML(ChildXML);

		m_NVEngineInfo[i].Id			= XMLChildUtil.GetAttrValueNum(_T("Id"));
		m_NVEngineInfo[i].SourceId		= XMLChildUtil.GetAttrValueNum(_T("SourceId"));
		m_NVEngineInfo[i].NVProcFacId	= XMLChildUtil.GetAttrValueNum(_T("NVProcFacId"));
		XMLChildUtil.GetAttrValueString(_T("ConfXML"), m_NVEngineInfo[i].szConfPath, MAX_PATH);


		ChildXML = XMLChildUtil.GetNextChild(ChildXML);
		i++;
	}
	
	if(i == 0){
		return FALSE;
	}

	m_nTotalEng = i;
	return TRUE;
}


BOOL	CNVSystem::CreateNVEngine(ULONG EngId)
{
	CNVEngine*		pEngine			= NULL;
	IBaseUMFSource* pUMFSource		= NULL;
	CNVCGICommander* pCGICommander	= NULL;
	IFileUMFStorage* pConfStorage	= NULL;
	
	BOOL	bSuccess = FALSE;	

	pEngine		= new CNVEngine();
	do{
		if(UMF_OK != m_pSourceMgr->GetSource(UMF_ID_NONE, m_NVEngineInfo[EngId].SourceId, &pUMFSource)) {
			TRACE(_T("Can not get source for engine [%d] Source[%d] \n"), EngId, m_NVEngineInfo[EngId].SourceId);
			break;	
		}
			
		pCGICommander = GetCGICommander(pUMFSource);
		pConfStorage = NULL;

		if(!pEngine->Setup(EngId, pUMFSource, pCGICommander, pConfStorage)){
			TRACE(_T("Can not create NVProc for engine [%d] NVProc[%d] \n"), EngId, m_NVEngineInfo[EngId].SourceId);
			break;
		}

		bSuccess = TRUE;	
	}while(0);


	if(bSuccess){
		m_pNVEngine[EngId]		= pEngine;
		m_pConfStorage[EngId]	= pConfStorage;
	}else{
		if(pEngine) delete pEngine;
		if(pUMFSource) m_pSourceMgr->ReleaseSource(pUMFSource);
		//if(pConfStorage) DestroyConfStorage(pConfStorage);

		m_pNVEngine[EngId]		= NULL;
		m_pConfStorage[EngId]	= NULL;
	}

	return bSuccess;
}


PXML	CNVSystem::GetSourceInfo(ULONG EngId)
{
	if(m_nTotalEng <= EngId){
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("GetSourceInfo Exceed Max[%d] Engid[%d]\n "), m_nTotalEng, EngId);
		return NULL;
	}

	IBaseUMFSource* pSource = m_pNVEngine[EngId]->GetSource();
	if(!pSource){
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("GetSourceInfo Can not Get Source Engid[%d]\n "), EngId);
		return NULL;
	}

	return pSource->GetSourceInfo();
}


BOOL	CNVSystem::RequestCGICommand(ULONG EngId, char* szCmd, BYTE* pResponse, DWORD* pLength, BOOL bFast)
{
	if(m_nTotalEng <= EngId){
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("RequestCGICommand Exceed Max[%d] Engid[%d]\n "), m_nTotalEng, EngId);
		return FALSE;
	}

	CNVCGICommander* pCGICommander = m_pNVEngine[EngId]->GetCGICommander();
	if(!pCGICommander){
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("RequestCGICommand Can not Get CGICommander Engid[%d]\n "), EngId);
		return NULL;
	}

	if(bFast)return	pCGICommander->RequestCGICommandFast(szCmd, pResponse, pLength);
	return	pCGICommander->RequestCGICommand(szCmd, pResponse, pLength);
}


static BOOL	GetServerAddreFromURL(TCHAR *szURL, TCHAR *szAddr, BOOL *bSecure)
{
	ULONG i, start, end;

	if( 0 == _tcsicmp(_T("https"), szURL/*szprotocol*/ ) ) {
		*bSecure = TRUE;
	}
	else
		if( 0 == _tcsicmp(_T("http"), szURL/*szprotocol*/ ) ) {
			*bSecure = FALSE;
		}
		else {
			*bSecure = FALSE;
		}


		start = end =0;
		for(i = 0 ; i < _tcslen(szURL) ; i++){
			if(szURL[i] == '/' && szURL[i+1] == '/'){
				i = start = i+2;
			}
			if(szURL[i] == '/' && szURL[i+1] != '/'){
				end = i;
				break;
			}
			if(szURL[i] == ':' && szURL[i+1] != '/'){
				end = i;
				break;
			}
		}
		if(0 == end)return FALSE;

		for(i = 0 ; i < end-start ; i++)szAddr[i] = szURL[i+start];
		szAddr[i] = _T('\0');

		return TRUE;
}


CNVCGICommander*	CNVSystem::GetCGICommander(IBaseUMFSource* pXmlSourceInfo)
{
	CUMFXMLUtil XMLUtil;
	XMLUtil.SetXML(pXmlSourceInfo->GetSourceInfo());
	TCHAR	szURL[MAX_PATH];
	TCHAR	szAddr[MAX_PATH];
	TCHAR	szID[MAX_PATH];
	TCHAR	szPassword[MAX_PATH];
	BOOL	bSecure = TRUE;
	CNVCGICommander* pNVCGICommander = NULL;

	memset(szURL, 0, sizeof(szURL));
	memset(szID, 0, sizeof(szID));
	memset(szPassword, 0, sizeof(szPassword));

	XMLUtil.GetAttrValueString(_T("WEBURL"), szURL, MAX_PATH);

	XMLUtil.GetAttrValueString(_T("ID"), szID, MAX_PATH);
	XMLUtil.GetAttrValueString(_T("PW"), szPassword, MAX_PATH);

		
	if(!GetServerAddreFromURL(szURL, szAddr, &bSecure)){
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("GetCGICommander Can not Get ServerAddress From URL [%s]\n "), szURL);
		return NULL;
	}

	/*
	//1. find exist cgi commander 
	for(i = 0 ; i < m_nCGICommanderCnt ; i++){
		if(m_pCGICommander[i]->IsSameServer(szAddr)){
			pNVCGICommander = m_pCGICommander[i];
		}
	}

	if(!pNVCGICommander){
*/		pNVCGICommander = new CNVCGICommander();
		if(!pNVCGICommander->Setup(szAddr, 0, szID, szPassword)){
			UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("GetCGICommander can not connect server [%s] [%s:%s]\n "), 
				szAddr, szID, szPassword);
			delete pNVCGICommander;
			return NULL;
		}
//	}

	pNVCGICommander->SetFlag(CNVCGICommander::USE_SSL, bSecure);

	m_pCGICommander[m_nCGICommanderCnt++] = pNVCGICommander;
	return pNVCGICommander;
}
