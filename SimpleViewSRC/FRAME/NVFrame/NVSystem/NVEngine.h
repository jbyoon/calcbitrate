#pragma once

#include "UMFXMLUtil\XMLUtils.h"

class CVCSystem;	

class IBaseUMFSource;
class IBaseUMFStorage;
class CNVCGICommander;

class INVEventSink
{
public:
	virtual BOOL	ChangeMMSourceInfo(DWORD EngId, PXML pSoruceInfoXML) = 0;
	virtual BOOL	ProcessMMData(DWORD EngId, DWORD SrcType, BYTE *pMMData, UINT MMDataLen, FILETIME *pTimestamp)	= 0;
};

class CNVEngine
{
public:
	enum {MAX_VA_EVENTSINK_CNT = 4};
	enum {VA_ENGINE_FREE, VA_ENGINE_READY, VA_ENGINE_WORK};

public:
	
	CNVEngine();
	~CNVEngine();

	BOOL	Setup(DWORD id, IBaseUMFSource* pUMFSource = NULL, CNVCGICommander* pCGICommander = NULL, IBaseUMFStorage *pStorage = NULL);
	void	Endup();

	DWORD	GetId(){return m_Id;}
	void	SetCGICommander(CNVCGICommander* pCGICommander){m_pCGICommander = pCGICommander;}
	CNVCGICommander* GetCGICommander(){return m_pCGICommander;}
	void			SetSource(IBaseUMFSource* pUMFSource){ m_pUMFSource = pUMFSource;}
	IBaseUMFSource* GetSource(){return m_pUMFSource;}
	
	BOOL	RegisterNVEventSink(INVEventSink *pNVEventSink) {
		if (pNVEventSink){
			if(m_EventInstCnt >= MAX_VA_EVENTSINK_CNT) return FALSE;
			m_pNVEventSink[m_EventInstCnt++] = (pNVEventSink);
			return TRUE;
		}
		return FALSE;
	}
	
	void	UnRegisterNVEventSinkAll(){
		m_EventInstCnt = 0;
	}

	BOOL	Start();
	void	Stop();

	void	UpdateAlogConf();

	static	DWORD	WINAPI EngineThreadWrapper(LPVOID pParam);

	
private:

	DWORD			m_Id;
	DWORD			m_State;

	IBaseUMFSource* m_pUMFSource;
	CNVCGICommander* m_pCGICommander;
	

	HANDLE			m_hThread;
	HANDLE			m_hKillEvent;
	DWORD			EngineThread();

	DWORD			m_EventInstCnt;
	INVEventSink*	m_pNVEventSink[MAX_VA_EVENTSINK_CNT];	
};
