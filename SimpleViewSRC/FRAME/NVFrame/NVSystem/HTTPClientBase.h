#ifndef __CHttpClient__
#define __CHttpClient__

#include <Wininet.h>
#pragma comment(lib, "Wininet.lib")

/*
	Windows Internet API (Wininet) for Client Side
*/

class CHttpClientBase
{
public: 
	CHttpClientBase();
	~CHttpClientBase();

	HRESULT Init();
	void	Deinit();
	
	HANDLE	Connect(TCHAR* szIP, int	nPort, TCHAR* szUsername = NULL, TCHAR*	szPassword = NULL);
	void	Disconnect(HANDLE hSession);

	/*
		CGI request "POST" method
		_lpszObject : "/nvc-cgi/admin/param.cgi"
	*/
	HANDLE	OpenRequest(HANDLE hSession, BOOL bHTTPS, BOOL bIgnoreCA, TCHAR* szObject, BOOL bUseCache = TRUE);

	/*
		CGI request "POST" method
		_lpszOption : "action=xxx&group=yyy&..."
	*/
	BOOL	SendRequest(HANDLE hOpenRequest, char* szOption, BYTE* pResponse, DWORD* pdwResponseLength);
	void	CloseRequest(HANDLE hOpenRequest);

private:

	HINTERNET	m_hInet;
};


#endif //#ifndef __CHttpClient__
