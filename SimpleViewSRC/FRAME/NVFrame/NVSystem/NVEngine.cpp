#include "StdAfx.h"
#include "CommonInterface\IBaseUMFSource.h"
#include "NVEngine.h"
#include "CommonInterface\IBaseNVProc.h"
#include "UMFXMLUtil\UMFXMLUtil.h"

#define CHECK_ENGINE_READY()\
	if(m_State < VA_ENGINE_READY){\
		TRACE("Engine is not ready for Work, Current Status [%d]\n",m_State);\
		return FALSE;\
	}\

static BOOL waitWithMessageLoop(HANDLE hEvent, DWORD dwTimeout)
{
  DWORD dwRet;
  MSG msg;
  BOOL ret = FALSE;
    
  while(true)
  {
    dwRet = MsgWaitForMultipleObjects(1, &hEvent, FALSE, dwTimeout, QS_ALLINPUT);
    if (dwRet == WAIT_OBJECT_0)
	{
		ret = TRUE;
		break;
	}
    if (dwRet != WAIT_OBJECT_0 + 1)
	{
		ret = FALSE;
		break;
	}
    while(PeekMessage(&msg,NULL,NULL,NULL,PM_REMOVE))
    {
      TranslateMessage(&msg);
      DispatchMessage(&msg);
      if (WaitForSingleObject(hEvent, 0) == WAIT_OBJECT_0)
	  {
         ret = TRUE;
		 break;
	  }
	  else
	  {
	  }
    }

  }
  return ret;
}

CNVEngine::CNVEngine()
{
	m_Id		= -1;
	m_State		= VA_ENGINE_FREE;

	m_pUMFSource= NULL;
	m_pCGICommander	= NULL;

	m_hThread	= NULL;
	m_hKillEvent= NULL;
	

	m_EventInstCnt	= 0;
	memset(m_pNVEventSink, 0 ,sizeof(m_pNVEventSink));
}

CNVEngine::~CNVEngine()
{
	Endup();
}

BOOL	CNVEngine::Setup(DWORD id, IBaseUMFSource* pUMFSource, CNVCGICommander* pCGICommander, IBaseUMFStorage *pStorage)
{
	if (VA_ENGINE_WORK == m_State) {
		return FALSE;
	}

	m_Id			= id;
	m_pUMFSource	= pUMFSource;
	m_pCGICommander	= pCGICommander;
	m_EventInstCnt	= 0;

	memset(m_pNVEventSink, 0, sizeof(m_pNVEventSink));

	m_State		= VA_ENGINE_READY;
	return TRUE;
}

void	CNVEngine::Endup()
{
	Stop();
}

BOOL	CNVEngine::Start()
{
	if (VA_ENGINE_READY != m_State) {
		return FALSE;
	}

	if(NULL == m_pUMFSource){
		TRACE(_T("UMFSource not assigned before\n"));
		return FALSE;
	}

	if( m_hKillEvent ) CloseHandle( m_hKillEvent );
	if( m_hThread ) CloseHandle( m_hThread );

	m_hKillEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	if (NULL == m_hKillEvent) {
		return FALSE;
	}

	DWORD threadID;
	m_hThread = CreateThread(NULL, 0, EngineThreadWrapper, this, NULL, &threadID);
	if (NULL == m_hThread) {
		CloseHandle(m_hKillEvent);
		return FALSE;
	}

	m_State = VA_ENGINE_WORK;
	return TRUE;
}


VOID	CNVEngine::Stop()
{
	if (VA_ENGINE_WORK != m_State) {
		return;
	}

	TRACE(_T("CNVEngine::Stop Engie Id[%d] \n"), m_Id);
	SetEvent(m_hKillEvent);

	if(!waitWithMessageLoop(m_hThread, 5000)){
		TRACE(_T("Timeout :: Terminate thread Engie Id[%d] \n"), m_Id);
		TerminateThread(m_hThread, 0);
	}

	CloseHandle(m_hThread);
	CloseHandle(m_hKillEvent);

	m_hThread = 0;
	m_hKillEvent = 0;

	m_State = VA_ENGINE_READY;
}



DWORD	CNVEngine::EngineThreadWrapper(LPVOID pParam)
{
	return ((CNVEngine *) pParam)->EngineThread();
}


DWORD	CNVEngine::EngineThread()
{
	DWORD				uTimeOut;
	BYTE				*pMMData = NULL;
	UINT				uMMDataLen = 0;
	FILETIME			timestamp, timestamp1;
	static FILETIME		timestamp_pre;
	DWORD				i,dwImageSize = 0;
	int	count;
	ULONG timediff;
	ULONGLONG timediff_pre_cur;
	PXML pSourceInfo;
	
	pSourceInfo = m_pUMFSource->GetSourceInfo();
	if( !pSourceInfo ) {
		TRACE("Engine Can't get source info from Source Module\n");
		return 0;
	}

	for(i = 0 ; i < m_EventInstCnt ; i++)
		m_pNVEventSink[i]->ChangeMMSourceInfo(m_Id, pSourceInfo);
		
	uTimeOut	= 25; // 30ms
	timestamp1.dwLowDateTime = 0;
	count		= 0;
	timediff	= 0;
	timediff_pre_cur = 0;

	m_pUMFSource->Start();

	int tick = GetTickCount();

	HANDLE	EventArray[2];
	EventArray[0] =	m_hKillEvent;
	EventArray[1] =	m_pUMFSource->GetEvent();

	int iFrames = 0;
	while (TRUE) {
		DWORD obj = WaitForMultipleObjects(2, EventArray, FALSE, uTimeOut);
		if (WAIT_OBJECT_0 == obj) {
			break;
		} else {
			SRCTYPE srcType;
			eUMF_STATUS Result;

			while (TRUE) {
				//JL: do a wait for single object here to look for the kill event      
				if (WaitForSingleObject(m_hKillEvent, 0) == WAIT_OBJECT_0)
					break;

				Result = m_pUMFSource->GetMMData(&srcType, &pMMData, &uMMDataLen, &timestamp);

				//Check source inforamtion change.
				if (UMF_CHNAGE_SOURCE ==  Result) {
					pSourceInfo = m_pUMFSource->GetSourceInfo();
					for(i = 0 ; i < m_EventInstCnt ; i++)
						m_pNVEventSink[i]->ChangeMMSourceInfo(m_Id, pSourceInfo);
					TRACE("Source inforamation changed \n");
				}else if(UMF_OK !=  Result){
	//				TRACE("Engine Can not GetMMData \n");
					break;
				}

				iFrames++;

				if(count == 30){
					//TRACE("Engine[%d] Avr Frame Diff [%d] \n", m_uEngineID, timediff/count);
					//TRACE("Avr Frame Diff [%d] \n", timediff/count);
					timediff  = 0;
					count = 0;

				}else{
					timediff += (timestamp.dwLowDateTime - timestamp1.dwLowDateTime);
					count++;

				}

				/*
				//TRACE("Engine[%d] Frame Diff [%d] \n", m_uEngineID, timestamp.dwLowDateTime - timestamp1.dwLowDateTime);
				//TRACE("Frame Diff :%d \n", timediff);

				if( (UMF_ST_VID_RAWFILE & srcType) | 
					(UMF_ST_VID_CAP5 & srcType) |  
					(UMF_ST_VID_STREAM & srcType) | 
					(UMF_ST_VID_COMPFILE & srcType) | 
					(UMF_ST_VID_RTSP & srcType) | 
					(UMF_ST_VID_LINKLOAD_JPEG & srcType) | 
					(UMF_ST_VID_LINKLOAD_H264 & srcType) | 
					(UMF_ST_VID_RAWFILE_H264 & srcType) | 
					(UMF_ST_VID_INNODEPVMS & srcType) 
				  )
				{
					//TRACE("Now Time Stamp :%ld %ld\n", timestamp_pre.dwLowDateTime, timestamp.dwLowDateTime);
					
					ULONGLONG qwResult_pre = (((ULONGLONG) timestamp_pre.dwHighDateTime) << 32) + timestamp_pre.dwLowDateTime;
					ULONGLONG qwResult_cur = (((ULONGLONG) timestamp.dwHighDateTime) << 32) + timestamp.dwLowDateTime;
					timediff_pre_cur = qwResult_cur - qwResult_pre;

					timestamp_pre = timestamp;
					
					if( timediff_pre_cur > 10000000 ) //500000) ) // 25 fps 일 경우 0.04 --> so, 2 frame 이상 놓칠경우 경고.
					{
						TRACE("[Time Over] Frame Diff :%f s. \n", (double)(timediff_pre_cur/10000000.0) );
					}
					
				}
				*/


				timestamp1 = timestamp;
				for(i = 0 ; i < m_EventInstCnt ; i++)
					m_pNVEventSink[i]->ProcessMMData(m_Id, srcType, pMMData, uMMDataLen, &timestamp);
		
				m_pUMFSource->ReleaseMMData();
			}
		}
	}

	// Stop the video source
	m_pUMFSource->Stop();
	
	return 0;
}
