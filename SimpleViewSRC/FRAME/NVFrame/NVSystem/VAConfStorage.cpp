#include "StdAfx.h"
#include "VAConfStorage.h"


#include "..\\VCA5Lib\\VCA5Storage.h"
#include "..\\WaterLevelDetect\\WaterLevelStorage.h"


IFileUMFStorage* CreateConfStorage(eVA_ALGO_TYPE type)
{
	IFileUMFStorage* pStorage = NULL;
	if(VA_ALGO_VCA5 == type){
		pStorage = new CVCA5Storage();
	}else if(VA_ALGO_WATERLEVEL == type){
		pStorage = new CWaterLevelStorage();
	}
	return pStorage;
}

void	DestroyConfStorage(IFileUMFStorage* pStorage)
{
	if(pStorage){
		if(pStorage->GetType() == VA_ALGO_VCA5){
			delete (CVCA5Storage*)pStorage;
		}else if(pStorage->GetType() == VA_ALGO_WATERLEVEL){
			delete (CWaterLevelStorage*)pStorage;
		}
	}	
}


