
#include "StdAfx.h"
#include "HttpClientBase.h"

#define NVC_HTTE_AGENT	_T("CLIENT_NVC")

CHttpClientBase::CHttpClientBase()
{
	m_hInet = NULL;
}


CHttpClientBase::~CHttpClientBase()
{
	Deinit();
}


HRESULT	CHttpClientBase::Init()
{
	HRESULT	hRet = NO_ERROR;

	if(m_hInet){
		UMFTrace(UMF_TRACE_LEVEL_WARNING ,_T("CHttpClientBase Init before \n"));
		return NO_ERROR;
	}

	HINTERNET	hInet = NULL;
	hInet = InternetOpen( NVC_HTTE_AGENT, INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, 0 );
	if( NULL == hInet ) {
		UMFTrace(UMF_TRACE_LEVEL_ERROR ,_T("Fail InternetOpen [%s] Error [%d] \n"), NVC_HTTE_AGENT, GetLastError());
		return -1;
	}

	m_hInet = hInet;
	return hRet;
}


void	CHttpClientBase::Deinit()
{
	if( m_hInet ) {
		InternetCloseHandle(m_hInet);
		m_hInet = NULL;
	}
}


HANDLE	CHttpClientBase::Connect(TCHAR* szIP, int nPort, TCHAR* szUsername, TCHAR* szPassword)
{
	if( NULL == m_hInet ){
		UMFTrace(UMF_TRACE_LEVEL_WARNING ,_T("CHttpClientBase::Connect does not Init before \n"));
		return NULL;
	}

	HRESULT		hRet = NO_ERROR;
	HINTERNET	hSession = NULL;
	hSession = InternetConnect(m_hInet, szIP, nPort, szUsername, szPassword,INTERNET_SERVICE_HTTP, 0,NULL);
	if( NULL == hSession ) {
		UMFTrace(UMF_TRACE_LEVEL_ERROR ,_T("Fail InternetConnect [%s:%d] Error [%d] \n"), szIP, nPort, GetLastError());
		return NULL;
	}

	return hSession;
}


void	CHttpClientBase::Disconnect(HANDLE hSession)
{
	if(hSession) {
		InternetCloseHandle(hSession);
	}
}


HANDLE	CHttpClientBase::OpenRequest(HANDLE hSession, BOOL bHTTPS, BOOL bIgnoreCA, TCHAR* szObject, BOOL bUseCache)
{
	if((NULL == hSession) || (NULL == szObject)){
		UMFTrace(UMF_TRACE_LEVEL_WARNING ,_T("CHttpClientBase::OpenRequest does not open[%p] before Object[%p]\n"),
			hSession, szObject);
		return NULL;
	}

	
	BOOL		fRet	= TRUE;
	HINTERNET	hRequest= NULL;
	TCHAR*		apszMIME[] = { _T("text/*"), NULL };

	DWORD	dwFlag = 0;

	if( bHTTPS ) {
		dwFlag = INTERNET_FLAG_SECURE;
		if(bIgnoreCA) {
			dwFlag |= INTERNET_FLAG_IGNORE_CERT_CN_INVALID;
		}
	}
	if(!bUseCache){
		// VT: Sometimes things are cached (e.g. like snapshot!)
		dwFlag |= INTERNET_FLAG_DONT_CACHE; 
	}

	hRequest = HttpOpenRequest(hSession, _T("POST"), szObject, NULL, NULL, (LPCTSTR*)apszMIME, dwFlag,NULL );
	if( NULL == hRequest ) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING, _T("Fail HttpOpenRequest [%s] Error [%d] \n"), szObject, GetLastError());
		return NULL;
	}

	DWORD	adwFlag[1] = {0,};
	DWORD	dwFlagBufferLength;
	if( bIgnoreCA ) {
		dwFlagBufferLength = sizeof(adwFlag);
		fRet = InternetQueryOption( hRequest, 
									INTERNET_OPTION_SECURITY_FLAGS,
									adwFlag, 
									&dwFlagBufferLength );
		if( TRUE == fRet ) {
			adwFlag[0] |= INTERNET_OPTION_SECURITY_FLAGS | SECURITY_FLAG_IGNORE_UNKNOWN_CA;
			dwFlagBufferLength = 4;
			fRet = InternetSetOption( hRequest, 
									  INTERNET_OPTION_SECURITY_FLAGS,
									  adwFlag, 
									  dwFlagBufferLength );
		}
	}

	// limited connections
	adwFlag[0] = 10;
	dwFlagBufferLength = sizeof(adwFlag);
	fRet = InternetSetOption( hRequest, 
							  INTERNET_OPTION_MAX_CONNS_PER_1_0_SERVER,
							  adwFlag, 
							  dwFlagBufferLength );

	adwFlag[0] = 10;
	dwFlagBufferLength = sizeof(adwFlag);
	fRet = InternetSetOption( hRequest, 
							  INTERNET_OPTION_MAX_CONNS_PER_SERVER,
							  adwFlag, 
							  dwFlagBufferLength );

	return hRequest;
}


void	CHttpClientBase::CloseRequest(HANDLE hOpenRequest)
{
	if(hOpenRequest) {
		InternetCloseHandle(hOpenRequest);
	}
}


BOOL CHttpClientBase::SendRequest(HANDLE hOpenRequest, char* szOption, BYTE* pResponse, DWORD* pdwResponseLength)
{
	if(NULL == hOpenRequest || (NULL == pResponse) || (NULL == pdwResponseLength)){
		UMFTrace(UMF_TRACE_LEVEL_WARNING ,
			_T("CHttpClientBase::SendRequest does not open hRequest [%p] before, pResponse[%d], pdwResponseLength [%d] \n"),
			hOpenRequest, pResponse, pdwResponseLength);
		return NULL;
	}
	
	BOOL	bRet = TRUE;
	DWORD	dwOptionLength	= 0;
	BYTE*   pTempResponse	= pResponse;

	if(szOption) {
		dwOptionLength = (DWORD)strlen(szOption);
	}

	bRet = HttpSendRequest(hOpenRequest, NULL, 0, szOption, dwOptionLength );
	if( FALSE == bRet ) {
		UMFTrace(UMF_TRACE_LEVEL_WARNING ,_T("Fail HttpSendRequest Option [%s] Error {%d] \n"), szOption, GetLastError());
		return FALSE;
	}

	
	DWORD	dwTotalReadedBytes = 0;
	DWORD	dwTotalQueryLength = 0;

	do {
		DWORD	dwQueryLength;
		DWORD	dwReadedBytes;

		InternetQueryDataAvailable(hOpenRequest, &dwQueryLength, 0, 0 );
		dwTotalQueryLength += dwQueryLength;

		if( *pdwResponseLength < dwTotalQueryLength ) {
			*pdwResponseLength = dwTotalQueryLength;
			bRet = FALSE;
			break;
		}

		if( 0 != dwQueryLength ) {
			pTempResponse = pResponse + dwTotalReadedBytes;
			InternetReadFile( hOpenRequest, pTempResponse, dwQueryLength, &dwReadedBytes );
			dwTotalReadedBytes += dwReadedBytes;
			if( 0 == dwReadedBytes ) {
				break;
			}
		}else {
			break;
		}
	}while(1);

	*pdwResponseLength = dwTotalReadedBytes;
	return bRet;
}
