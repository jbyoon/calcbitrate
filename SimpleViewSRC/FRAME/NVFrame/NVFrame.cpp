// NVFrame.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "NVFrame.h"
#include "NVFrameDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//
//TODO: If this DLL is dynamically linked against the MFC DLLs,
//		any functions exported from this DLL which call into
//		MFC must have the AFX_MANAGE_STATE macro added at the
//		very beginning of the function.
//
//		For example:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// normal function body here
//		}
//
//		It is very important that this macro appear in each
//		function, prior to any calls into MFC.  This means that
//		it must appear as the first statement within the 
//		function, even before any object variable declarations
//		as their constructors may generate calls into the MFC
//		DLL.
//
//		Please see MFC Technical Notes 33 and 58 for additional
//		details.
//


// CNVFrame

BEGIN_MESSAGE_MAP(CNVFrame, CWinApp)
END_MESSAGE_MAP()


// CNVFrame construction

CNVFrame::CNVFrame()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CNVFrame object
#ifdef _LIB
	#include "../../BuildConfigure.h"
	#ifdef USE_MFC_CONTAINER
	CNVFrame theApp;
	#endif
#else
	CNVFrame theApp;
#endif


// CNVFrame initialization

BOOL CNVFrame::InitInstance()
{
	CWinApp::InitInstance();

	return TRUE;
}

#ifdef _LIB
static	BOOL	g_bSetInstance	= FALSE;
HINSTANCE GetUMFStaticInstance();
#endif


extern "C" IBaseUMFrame*	WINAPI CreateUMFrame()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

#ifdef _LIB
	if(!g_bSetInstance){
		HINSTANCE hInst = GetUMFStaticInstance();

		afxCurrentInstanceHandle	= hInst;
		afxCurrentResourceHandle	= hInst;	
#ifdef USE_MFC_CONTAINER
		theApp.m_hInstance			= hInst;	
#endif
		g_bSetInstance				= TRUE;
	}
#endif
	return new CNVFrameDlg();
}

extern "C" eFRAMETYPE	WINAPI	GetFrameType()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	return UMF_VT_NVFRAME;
}