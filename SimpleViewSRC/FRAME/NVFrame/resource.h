//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by NVFrame.rc
//
#define VS_VERSION_INFO                 1
#define IDC_STATIC_DIALOG               5000
#define IDC_STATIC_ALARM                5001
#define IDC_CUSTOM1                     5002
#define IDI_ICON1                       5002
#define IDI_VASCLIENT_ICON              5002
#define IDD_NVFRAME_DIALOG              6000
#define ID_CONF_MODULE_BASE             10000
#define ID_PROC_MODULE_BASE             11000
#define ID_OPTION_RECORDING             32830

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        5003
#define _APS_NEXT_COMMAND_VALUE         32774
#define _APS_NEXT_CONTROL_VALUE         5003
#define _APS_NEXT_SYMED_VALUE           5000
#endif
#endif
