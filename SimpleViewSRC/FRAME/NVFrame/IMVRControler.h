#ifndef __MVR_CONTROLER_INTERFACE_H__
#define __MVR_CONTROLER_INTERFACE_H__

class IMVRControler
{
public:
	virtual	BOOL	GetViewRect(DWORD EngId, RECT* pRect) = 0;
	virtual BOOL	ChangeSourceInfo(DWORD EngId, DWORD width, DWORD height, UINT uColorformat) = 0;
	virtual BOOL	OnChangeClientRect(RECT *pRect = NULL)  = 0;
	virtual BOOL	ProcessMMData(DWORD EngId, SRCTYPE srcType, BYTE *pMMData, UINT iMMDataLen) = 0;
};

#endif //__MVR_CONTROLER_INTERFACE_H__