// NVFrame.h : main header file for the NVFrame DLL
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CNVFrame
// See NVFrame.cpp for the implementation of this class
//

class CNVFrame : public CWinApp
{
public:
	CNVFrame();

// Overrides
public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
};
