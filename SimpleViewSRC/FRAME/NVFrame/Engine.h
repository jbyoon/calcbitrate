#pragma once

#include "IBaseUMFSource.h"
#include "IMVRControler.h"

class CEngine
{
public:
	CEngine(void);
	~CEngine(void);

	BOOL		Setup(UINT uEngineID, IBaseUMFSource* pUMFSource, IMVRControler* pMVR);
	void		Endup();
	BOOL		Start();
	void		Stop();

	UINT		GetEngineId();
	UINT		GetSourceId();
	BOOL		IsInMyRect(CPoint point);

	DWORD		EngineThread();
	static DWORD WINAPI EngineThreadWrapper(LPVOID pParam);


public:
	enum {STATE_CREATED=0, STATE_READY=1, STATE_RUNNING=2};

private:
	UINT				m_uEngID;
	UINT				m_uStatus;

	HANDLE				m_hThread;
	HANDLE				m_hKillEvent;

	IBaseUMFSource*		m_pUMFSource;
	IMVRControler*		m_pMVR;
};
