#pragma once

#include "UMFBase.h"
#include "CommonInterface\IBaseUMFrame.h"
#include "CommonInterface\IBaseNVView.h"
#include "CommonInterface\IBaseMMProc.h"
#include ".\NVSystem\NVSystem.h"

class CNVViewModuleMgr : public INVEventSink
{
public:
	enum {MAX_MODULE = 32};
	CNVViewModuleMgr();
	~CNVViewModuleMgr();

	BOOL	Setup(HWND hWnd, IBaseNVFrame *pNVFrame, IBaseUMFStorage *pStorage);
	void	Endup();

	DWORD	GetMaxModuleCnt() {return m_MaxModuleCnt;}
	IBaseNVView* GetModule(DWORD index){return m_pNVViewModules[index];}
	
	BOOL	Show(DWORD index, DWORD EngId, BOOL bShow, IBaseNVView::DISPLAY_STYLE DisplayType);
	LONG	SendCommand(ULONG nEngId, wchar_t* szCmd, wchar_t* szSubCmd, wchar_t* szValue, void* pParam);

	// Implementattion of IVAEvenSink
	BOOL	ChangeMMSourceInfo(DWORD EngId, PXML pSoruceInfoXML);
	BOOL	ProcessMMData(DWORD EngId, DWORD SrcType, BYTE *pMMData, UINT MMDataLen, FILETIME *pTimestamp);
	
	DWORD	GetDefaultViewIndex();
	DWORD	GetRunningViewIndex();


private:
	HWND	m_hWnd;
	DWORD	m_MaxModuleCnt;
	PXML	m_pXMLStroage;
	TCHAR	m_szDefaultModule[MAX_PATH];

	BOOL			LoadNVViewModules(HWND hWnd, IBaseNVFrame *pNVFrame, TCHAR *szModulePath);
	BOOL			LoadNVViewModulesByStorage(HWND hWnd, IBaseNVFrame *pNVFrame, IBaseUMFStorage *pStorage);
	IBaseNVView*	m_pNVViewModules[MAX_MODULE];
//	IBaseNVView*	LoadNVViewModule(HWND hWnd, IBaseNVFrame *pNVFrame, TCHAR *szModuleName);

//<JL>
//	LONG			LoadNVViewModule(HWND hWnd, IBaseNVFrame *pNVFrame, TCHAR *szModulePath);

	//LONG CreateMMProc(LPCTSTR szName, LPCTSTR szDLLFile);
	//LONG DestroyMMProc(LONG iMMProcID);
	//LONG ConnectMMProcs(LONG iSourceID, LONG iDestID);
	//LONG DisconnectMMProcs(LONG iSourceID, LONG iDestID);
	//LONG PositionMMProc(LONG iMMProcID, double dRelPosX, double dRelPosY, double dRelWidth, double dRelHeight);
	//IBaseMMProc* GetMMProc(LONG MMProcID);
//</JL>
};
