#include "StdAfx.h"
#include "Engine.h"
#include "UMFXMLUtil.h"

struct SOURCE_INFO
{
	UINT	SourceId;
	INT		SrcFacId;
	UINT	SourceType;
	union {
		struct {
			UINT	Bd;
			UINT	Ch;
		};
		TCHAR	szSourcePath[MAX_PATH];
	}tSourceData;
	UINT		ColorFormat;
	UINT		ImageSizeW;
	UINT		ImageSizeH;
	UINT		FrameRate;
};

BOOL	GetSourceInfoFromXml(void* pSrcXML, SOURCE_INFO* pSourceInfo)
{
	CUMFXMLUtil XMLUtil;
	XMLUtil.SetXML(pSrcXML);

	try{
		pSourceInfo->SourceId		= XMLUtil.GetAttrValueNum( _T("Id"));
		pSourceInfo->SourceType		= XMLUtil.GetAttrValueNum( _T("SourceType"));
		if(pSourceInfo->SourceType & UMF_ST_VID_CAP5 ) {
			pSourceInfo->tSourceData.Bd	= XMLUtil.GetAttrValueNum( _T("Bd"));
			pSourceInfo->tSourceData.Ch	= XMLUtil.GetAttrValueNum(_T("Ch"));
		} else {
			XMLUtil.GetAttrValueString(_T("Path"), pSourceInfo->tSourceData.szSourcePath, 
				_countof(pSourceInfo->tSourceData.szSourcePath));
		}
		pSourceInfo->ColorFormat	= XMLUtil.GetAttrValueHex(_T("ColorFormat"));
		pSourceInfo->ImageSizeW		= XMLUtil.GetAttrValueNum(_T("ImageSizeW"));
		pSourceInfo->ImageSizeH		= XMLUtil.GetAttrValueNum(_T("ImageSizeH"));
		pSourceInfo->FrameRate		= XMLUtil.GetAttrValueNum(_T("FrameRate"));
	}catch(...)
	{
		return FALSE;
	}

	return TRUE;
}

CEngine::CEngine(void)
{
	m_uEngID		= UMF_ID_NONE;
	m_uStatus		= STATE_CREATED;
	m_hThread		= NULL;
	m_hKillEvent	= NULL;
	m_pUMFSource	= NULL;
	m_pMVR			= NULL;
}

CEngine::~CEngine(void)
{
	Endup();
}

BOOL	CEngine::Setup(UINT uEngineID, IBaseUMFSource* pUMFSource,	IMVRControler* pMVR)
{
	if (STATE_RUNNING == m_uStatus) {
		return FALSE;
	}

	if((uEngineID == UMF_ID_NONE) || (!pUMFSource) || (!pMVR)) {
		return FALSE;
	}

	m_pUMFSource = pUMFSource;
	m_pMVR	= pMVR;
	m_uEngID = uEngineID;
	m_uStatus = STATE_READY;

	return TRUE;
}

void	CEngine::Endup()
{
	m_uEngID  = UMF_ID_NONE;
	m_uStatus = STATE_CREATED;
	m_hThread		= NULL;
	m_hKillEvent	= NULL;
	m_pUMFSource	= NULL;
	m_pMVR			= NULL;
}

BOOL	CEngine::Start()
{
	if (STATE_READY != m_uStatus) {
		return UMF_ERR_NOT_READY;
	}

	if( m_hKillEvent ) CloseHandle( m_hKillEvent );
	if( m_hThread ) CloseHandle( m_hThread );

	m_hKillEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	if (NULL == m_hKillEvent) {
		return UMF_ERR_INIT;
	}
	DWORD threadID;
	m_hThread = CreateThread(NULL,0,EngineThreadWrapper,this,NULL,&threadID);
	if (NULL == m_hThread) {
		CloseHandle(m_hKillEvent);
		return UMF_ERR_INIT;
	}

	m_uStatus = STATE_RUNNING;

	return UMF_OK;
}

void	CEngine::Stop()
{
	if (STATE_RUNNING != m_uStatus) {
		return;
	}

	SetEvent(m_hKillEvent);

	DWORD obj = WaitForSingleObject(m_hThread, 5000);
	if(WAIT_TIMEOUT == obj){
		OutputDebugString(_T("Timeout :: Terminate thread\n"));
		TerminateThread(m_hThread, 0);
	}

	CloseHandle(m_hThread);
	CloseHandle(m_hKillEvent);

	m_hThread = NULL;
	m_hKillEvent = NULL;

	m_uStatus = STATE_READY;
}

UINT	CEngine::GetEngineId() 
{
	return m_uEngID;
}

UINT	CEngine::GetSourceId() 
{
	if(!m_pUMFSource) return UMF_ID_NONE;
	return m_pUMFSource->GetSourceId();
}

BOOL	CEngine::IsInMyRect(CPoint point)
{
	if (STATE_CREATED == m_uStatus) {
		return FALSE;
	}

	RECT myRect;
	if(!m_pMVR->GetViewRect(m_uEngID, &myRect)) {
		return FALSE;
	}

	return PtInRect(&myRect, point);
}

DWORD	CEngine::EngineThreadWrapper(LPVOID pParam)
{
	return ((CEngine *) pParam)->EngineThread();
}

DWORD	CEngine::EngineThread()
{
	DWORD				uTimeOut;
	BYTE				*pMMData = NULL;
	UINT				uMMDataLen = 0;
	FILETIME			timestamp, timestamp1;
	DWORD				dwImageSize = 0;
	int	count;
	ULONG timediff;

//	CoInitialize(NULL);

	PXML pSourceInfo = m_pUMFSource->GetSourceInfo();
	if( !pSourceInfo ) {
		TRACE("Engine Can't get source info from Source Module\n");
		return 0;
	}

	SOURCE_INFO	SourceInfo;
	if(!GetSourceInfoFromXml(pSourceInfo, &SourceInfo)) {
		TRACE("Engine Can't parse source info xml");
		return 0;
	}

	DWORD dwWidth	= SourceInfo.ImageSizeW;
	DWORD dwHeight	= SourceInfo.ImageSizeH;
	eUMF_COLORFORMAT colorFormat = (eUMF_COLORFORMAT)SourceInfo.ColorFormat;
	if(0 == SourceInfo.FrameRate) {
		SourceInfo.FrameRate = 3000;
	}

	m_pMVR->ChangeSourceInfo(m_uEngID, dwWidth, dwHeight, colorFormat);
	m_pMVR->OnChangeClientRect();

	uTimeOut	= (100000/(SourceInfo.FrameRate));


	timestamp1.dwLowDateTime = 0;
	count		= 0;
	timediff	= 0;
	m_pUMFSource->Start();

	int tick = GetTickCount();

	HANDLE	EventArray[2];
	EventArray[0] =	m_hKillEvent;
	EventArray[1] =	m_pUMFSource->GetEvent();

	int iFrames = 0;
	while (TRUE) {
		DWORD obj = WaitForMultipleObjects(2, EventArray, FALSE, uTimeOut);
		if (WAIT_OBJECT_0 == obj) {
			//			m_pSource->ReleaseMMData();
			break;
		} else {
			SRCTYPE srcType;
			if (UMF_OK != m_pUMFSource->GetMMData(&srcType, &pMMData, &uMMDataLen, &timestamp)) {
				TRACE("Engine Can not GetMMData \n");

				continue;
			}
			iFrames++;

			if(count == 30){
				//		TRACE("Engine[%d] Avr Frame Diff [%d] \n", m_uEngineID, timediff/count);
				timediff  = 0;
				count = 0;

			}else{
				timediff += (timestamp.dwLowDateTime - timestamp1.dwLowDateTime);
				count++;
			}
			//TRACE("Engine[%d] Frame Diff [%d] \n", m_uEngineID, timestamp.dwLowDateTime - timestamp1.dwLowDateTime);

			timestamp1 = timestamp;

			m_pMVR->ProcessMMData(m_uEngID, srcType, pMMData, uMMDataLen);

			m_pUMFSource->ReleaseMMData();
		}
	}

	// Stop the video source
	m_pUMFSource->Stop();

	TRACE("End of Engine \n");
//	CoUninitialize();

	return 0;
}