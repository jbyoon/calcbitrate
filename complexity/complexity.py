#! /usr/bin/env python
#
import Image
import sys,os,math,operator
import tempfile
import fnmatch

def complexity_rawfile(im):
  tfile = tempfile.mkstemp(prefix='xtmp')
  nfname = tfile[1]
  im.save(nfname,'JPEG')
  filesize = os.stat(nfname).st_size
  os.remove(nfname)
  return filesize

def complexity(files):
  data=[]
  filesize = 0
  for fname in files:
    im=Image.open(fname)
    orgsize = im.size
    sz = (1280,720)
    if im.size != sz:
      im = im.resize(sz)
    if im.format == 'JPEG':
      nfname = fname
      filesize = os.stat(nfname).st_size
    else:
      filesize = complexity_rawfile(im)

    area=im.size[0]*im.size[1]
    area_2 = int(area**0.5)
#    d= fname,filesize,im.size,area,area_2,int(filesize/area_2)
    d= fname,orgsize,im.size,filesize,int(filesize/area_2)
    data.append(d)
#  sd = sorted(data,key=operator.itemgetter(len(data[0])-1),reverse=True)
    sd = data
  for d in sd:
    print ','.join(str(x) for x in d)

#complexity(sys.argv[1:])

def findfiles(pattern, path):
  result = []
  for root, dirs, files in os.walk(path):
    for d in dirs:
      rs = findfiles(pattern, d)
      if len(rs) > 0:
        result.append(rs)
    for name in files:
      if fnmatch.fnmatch(name, pattern):
          result.append(os.path.join(root, name))
  return result

def complexity_video(fname):
  prefix = os.path.basename(fname)
  nfname = '/tmp/'+prefix+'-%04d.bmp'
  cmd = 'ffmpeg -i '+fname+' -r 30 '+nfname
  os.system(cmd)
  files = findfiles(prefix+'-*.bmp','/tmp')
  complexity(files)

#complexity_video(sys.argv[1])

def complexity_rtsp(url):
  prefix = url.split('/')[2] # ip
  videoname = '/tmp/'+prefix+'.mpg'
#  vlc -vvv rtsp://192.168.96.203/ufirststream --sout t1.mpg --run-time 10 vlc://quit
  cmd = 'vlc -vvv '+url+' --sout '+ videoname + ' --run-time 3 vlc://quit'
  os.system(cmd)
  complexity_video(videoname)

complexity_rtsp(sys.argv[1])
