#! /usr/bin/env python
#
import Image
import urllib2
import sys,os,time

ROOT_USERNAME = 'admin'
ROOT_PASSWORD = 'admin'

def MakeHttpReq(ipaddr, shortUrl):
    theurl = 'http://%s%s' % (ipaddr, shortUrl)
    passman = urllib2.HTTPPasswordMgrWithDefaultRealm()
    passman.add_password(None, theurl, ROOT_USERNAME, ROOT_PASSWORD)
    authhandler = urllib2.HTTPBasicAuthHandler(passman)
    opener = urllib2.build_opener(authhandler)
    urllib2.install_opener(opener)
    pagehandle = urllib2.urlopen(theurl)
    return pagehandle

def ReadParamCGI(ipaddr,group):
    data=[]
    error=False
    theurl = '/uapi-cgi/param.cgi?action=list&group='+group
    try:
        pagehandle = MakeHttpReq(ipaddr, theurl)
        page = pagehandle.read()
        error=(page.find(group) != -1)
        print page
        for d in page.split():
            data.append(d.split('='))
        print data
    except:
        error=True
        page=''
    return data,error

def WriteParamCGI(ipaddr,group_value):
    data=[]
    error=False
    theurl = '/uapi-cgi/param.cgi?action=update&'+group_value
    try:
        pagehandle = MakeHttpReq(ipaddr, theurl)
        page = pagehandle.read()
        error=(page.find('#200|OK') != -1)
        print page
    except:
        pass
#    logtxt+= loghead + "CGI command-" +theurl + " " + page
    return error

################################
##
ipaddr = sys.argv[1] # 192.168.101.97
fname = sys.argv[2] # 1080p
orgfname = fname+".org.jpeg"
jpgfname = fname+".jpeg"

#r,e = ReadParamCGI(ipaddr,"System")
#e = WriteParamCGI(ipaddr,'ENCODER.Ch0.Snapshot.quality','75')


WriteParamCGI(ipaddr,'ENCODER.Ch0.Videocodec.St0.standard=mjpeg')
WriteParamCGI(ipaddr,'ENCODER.Ch0.Videocodec.St0.Mjpeg.maxfps=5')
WriteParamCGI(ipaddr,'ENCODER.Ch0.Videocodec.St0.Mjpeg.quality=100')
# ENCODER.Ch0.Snapshot.enable=no
# ENCODER.Ch0.Snapshot.maxfps=4
# ENCODER.Ch0.Snapshot.name=snapshot
# ENCODER.Ch0.Snapshot.quality=75
# ENCODER.Ch0.Snapshot.resolution=480x270
for res in ["1920x1080","1280x720","800x450","640x360","480x270","320x180"]
    WriteParamCGI(ipaddr,'ENCODER.Ch0.Videocodec.St0.Mjpeg.resolution='+res)
    time.sleep(3)

    cmd='wget http://admin:admin@'+ipaddr+'/uapi-cgi/snapshot.fcgi -O ' + orgfname
    os.system(cmd)
    im=Image.open(orgfname)
    im.save(jpgfname)

    filesize = os.stat(jpgfname).st_size
    im2=Image.open(jpgfname)
    d = im2.size[0]*im2.size[1]

    print fname,filesize,im2.size,d, float(filesize)/d
