#! /usr/bin/env python
#
import Image
import urllib2
import sys,os,time,math,operator

ROOT_USERNAME = 'admin'
ROOT_PASSWORD = 'admin'

def MakeHttpReq(ipaddr, shortUrl):
    theurl = 'http://%s%s' % (ipaddr, shortUrl)
    passman = urllib2.HTTPPasswordMgrWithDefaultRealm()
    passman.add_password(None, theurl, ROOT_USERNAME, ROOT_PASSWORD)
    authhandler = urllib2.HTTPBasicAuthHandler(passman)
    opener = urllib2.build_opener(authhandler)
    urllib2.install_opener(opener)
    pagehandle = urllib2.urlopen(theurl)
    return pagehandle

def ReadParamCGI(ipaddr,group):
    data=[]
    error=False
    theurl = '/uapi-cgi/param.cgi?action=list&group='+group
    try:
        pagehandle = MakeHttpReq(ipaddr, theurl)
        page = pagehandle.read()
        error=(page.find(group) != -1)
        print page
        for d in page.split():
            data.append(d.split('='))
        print data
    except:
        error=True
        page=''
    return data,error

def WriteParamCGI(ipaddr,group_value):
    page=''
    data=[]
    error=False
    theurl = '/uapi-cgi/param.cgi?action=update&'+group_value
    try:
        pagehandle = MakeHttpReq(ipaddr, theurl)
        page = pagehandle.read()
        error=(page.find('#200|OK') != -1)
#        print page
    except:
        pass
    print theurl + " " + page
    return error

################################
##
def gathercomplexcity(ipaddr,postfix):
    #cameratype="tyco"
    cameratype="ipx"
    data=[]
    if cameratype == "tyco":
        WriteParamCGI(ipaddr,'ENCODER.Ch0.Videocodec.St0.standard=mjpeg')
        WriteParamCGI(ipaddr,'ENCODER.Ch0.Videocodec.St0.Mjpeg.maxfps=5')
        WriteParamCGI(ipaddr,'ENCODER.Ch0.Videocodec.St0.Mjpeg.quality=100')
    else:
        WriteParamCGI(ipaddr,'ENCODER.Ch0.Videocodec.St0.standard=h264')
        WriteParamCGI(ipaddr,'ENCODER.Ch0.Videocodec.St0.h264.maxfps=5')
        WriteParamCGI(ipaddr,'ENCODER.Ch0.Snapshot.enable=yes')
        WriteParamCGI(ipaddr,'ENCODER.Ch0.Snapshot.maxfps=5')
        WriteParamCGI(ipaddr,'ENCODER.Ch0.Snapshot.quality=100')
    #
    for res in ["1920x1080","1280x720","800x450","640x360","480x270","320x180"]:
        WriteParamCGI(ipaddr,'ENCODER.Ch0.Videocodec.St0.Mjpeg.resolution='+res)
        if cameratype == "tyco":
            pass
        else:
            WriteParamCGI(ipaddr,'ENCODER.Ch0.Snapshot.resolution='+res)

        time.sleep(1)
        orgfname = res+postfix+'.org.jpeg'
        jpgfname = res+postfix+'.jpeg'
        randomstr = '_='+str(int(time.time()))
        cmd='wget http://'+ROOT_USERNAME+':'+ROOT_PASSWORD+'@'+ipaddr+'/uapi-cgi/snapshot.fcgi?'+randomstr+' -O ' + orgfname
        os.system(cmd)
        im=Image.open(orgfname)
        im.save(jpgfname)

        filesize = os.stat(jpgfname).st_size
        im2=Image.open(jpgfname)
        area = im2.size[0]*im2.size[1]
        area_2 = int(area**0.5)

#        d= filesize,im2.size,area,round(math.log(area),3),round(float(filesize)/area,3)
        d= filesize,im2.size,area,area_2,int(length/area_2)
        data.append(d)
    return  sorted(data,key=operator.itemgetter(0),reverse=True)

################################
## mamin
print gathercomplexcity(sys.argv[1],sys.argv[2])


