#! /usr/bin/env python
#
import Image,ImageChops,ImageFilter,ImageOps,ImageMath,ImageStat
import sys,os
import glob
import operator,math

def compareimage(old,new):
  print old,new
  imA=Image.open(old)
  sz=imA.size
#  sz=(imA.size[0]/8,imA.size[1]/8)
  A = imA.resize(sz).convert("L")
  imB=Image.open(new)
  B = imB.resize(sz).convert("L")
#  imC = ImageChops.difference(imA, imB)
  imC = ImageChops.difference(A, B)
#  imD = imC.filter(ImageFilter.RankFilter(9,8))
  imD = ImageOps.posterize(imC,4)
  return imD.convert("1")
#  return imD.convert("1").resize((100,50))
#  return imC.convert("L")

#compareimage("m1.bmp","m2.bmp").show()

def calcmotion(im):
  im = im.filter(ImageFilter.MedianFilter(size=9))
  sz=(im.size[0]/8,im.size[1]/8)
  im = im.resize(sz)
  im = im.filter(ImageFilter.MaxFilter(size=3))
  im.show()
  stat = ImageStat.Stat(im)
  print stat.count[0],stat.sum[0]/255, 100*(stat.sum[0]/255)/stat.count[0]

def compareimages(files):
  im = ''
  old = files[0]
  for fname in files[1:]:
    nim = compareimage(old,fname)
    if im == '':
      im = nim
    else:
      im = ImageMath.eval("convert(max(a, b), 'L')", a=im, b=nim)
    old =fname
  calcmotion(im)
#  im.show()

compareimages(sys.argv[1:])