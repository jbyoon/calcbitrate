#! /usr/bin/env python
#

import Image
import sys,os
import glob
import operator,math

def downscale(fname,scale):
  print fname
  im=Image.open(fname)
  sz=(int(im.size[0]*scale),int(im.size[1]*scale))
  nim = im.resize(sz)
  nfname = fname.split('.')[0] +'-'+str(sz[0])+'x'+str(sz[1])+'.jpg'
  nim.save(nfname,"JPEG")
  return nfname,os.stat(nfname).st_size,sz

def info():
  for fname in glob.glob('*.jpg'):
    fname,os.stat(fname).st_size

def main(fname):
  data=[]
  for scale in [1.0,0.6667,0.5,0.3334,0.25,0.1,0.02]:
    nf,length,sz = downscale(fname,scale)
    area=float(sz[0]*sz[1])
    area_2 = area**0.5
#    data.append([nf,length,sz,area,round(length/area,3),round(math.log(area),2)])
    data.append([nf,length,area,area_2,round(length/area_2,2)])
  data = sorted(data,key=operator.itemgetter(1),reverse=True)
  for d in data:
    print ','.join(str(x) for x in d)

main(sys.argv[1])
